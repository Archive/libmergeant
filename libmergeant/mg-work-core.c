/* mg-work-core.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-base.h"
#include "mg-server.h"
#include "mg-work-core.h"
#include "mg-query.h"
#include "mg-context.h"
#include "mg-target.h"
#include "mg-entity.h"
#include "mg-field.h"
#include "mg-qf-field.h"
#include "mg-qf-value.h"
#include "mg-qf-all.h"
#include "mg-parameter.h"
#include "mg-db-field.h"
#include "mg-join.h"
#include "mg-condition.h"
#include "mg-db-constraint.h"
#include "mg-database.h"
#include "mg-db-table.h"
#include "mg-graphviz.h"

static void mg_work_core_class_init (MgWorkCoreClass *class);
static void mg_work_core_init (MgWorkCore *obj);
static void mg_work_core_dispose (GObject *object);
static void mg_work_core_initialize (MgWorkCore *core);

/* 
 * this structure represents a dependency resulting from a join between 
 * 'this->dependant->target' and 'this->target',
 * the MgJoin for this dependency being 'this->join'
 */
typedef struct _TargetDep{
	MgTarget          *target;
	GSList            *depend_on; /* list of TargetDep structures */

	struct _TargetDep *dependant;
	MgJoin            *join;      /* join between 'this->dependant->target' and 'this->target' */ 
} TargetDep;
#define TARGET_DEP(x) ((TargetDep *) x)

static void nullified_query_cb (MgQuery *query, MgWorkCore *core);
static void nullified_target_cb (MgTarget *target, MgWorkCore *core);
static void nullified_context_cb (MgContext *context, MgWorkCore *core);

MgContext *make_work_context_target (MgWorkCore *core);
MgContext *make_work_context_no_target (MgWorkCore *core);
TargetDep *make_target_deps_recurs (MgWorkCore *core, MgTarget *on_target, 
				    const GSList *joins, GHashTable *joinsdep);
TargetDep *make_target_deps (MgWorkCore *core);
void       make_target_deps_free (TargetDep *dep);
gboolean   make_target_select_queries_improved (MgWorkCore *core, TargetDep *dep, GHashTable *target_query_h, 
						GHashTable *replacements, GError **error);
gboolean   modif_target_depends_on (MgWorkCore *core,  TargetDep *modif_target_dep, MgTarget *target);

static GSList *get_query_target_pkey (MgQuery *query, MgTarget *target);
static void make_query_update (MgWorkCore *core);
static void make_query_delete (MgWorkCore *core);
static void make_query_insert (MgWorkCore *core);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

guint
mg_work_core_get_type (void)
{
        static GType type = 0;

        if (!type) {
                static const GTypeInfo info = {
                        sizeof (MgWorkCoreClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) mg_work_core_class_init,
                        NULL,
                        NULL,
                        sizeof (MgWorkCore),
                        0,
                        (GInstanceInitFunc) mg_work_core_init
                };

                type = g_type_register_static (MG_BASE_TYPE, "MgWorkCore", &info, 0);
        }

        return type;
}

static void
mg_work_core_class_init (MgWorkCoreClass * class)
{
        GObjectClass   *object_class = G_OBJECT_CLASS (class);

        parent_class = g_type_class_peek_parent (class);
        object_class->dispose = mg_work_core_dispose;
}

static void
mg_work_core_init (MgWorkCore * core)
{
	core->args_context = NULL;
	core->work_context = NULL;
	core->modif_target = NULL;

	core->query_select = NULL;
	core->query_select_improved = NULL;
	core->query_update = NULL;
	core->query_delete = NULL;
	core->query_insert = NULL;

	core->query_select_repl = NULL;

	core->nodes = NULL;
}

/**
 * mg_work_core_new
 * @query: a #MgQuery object
 * @modified: a #MgTarget object, or %NULL
 *
 * Creates a new #MgWorkCore object and fills it with appropriate data
 *
 * @query must be a SELECT query (no union, etc selection query)
 *
 * The @modified target must belong to @query and represent
 * modifiable entity (a #MgDbTable for example). If @modified is %NULL then
 * no modification will be allowed.
 *
 * Returns: the new object
 */
GObject *
mg_work_core_new (MgQuery *query, MgTarget *modified)
{
	GObject *obj;
	MgWorkCore *core;
	GHashTable *replacements;

	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (mg_query_get_query_type (query) == MG_QUERY_TYPE_SELECT, NULL);
	
	if (modified) {
		g_return_val_if_fail (IS_MG_TARGET (modified), NULL);
		g_return_val_if_fail (mg_target_get_query (modified) == query, NULL);
		g_return_val_if_fail (mg_entity_is_writable (mg_target_get_represented_entity (modified)), NULL);
	}

	obj = g_object_new (MG_WORK_CORE_TYPE, "conf", mg_base_get_conf (MG_BASE (query)), NULL);
	core = MG_WORK_CORE (obj);

	replacements = g_hash_table_new (NULL, NULL);
	core->query_select = MG_QUERY (mg_query_new_copy (query, replacements));
	g_signal_connect (G_OBJECT (core->query_select), "nullified",
			  G_CALLBACK (nullified_query_cb), core);

	if (modified) {
		core->modif_target = g_hash_table_lookup (replacements, modified);
		g_signal_connect (G_OBJECT (core->modif_target), "nullified",
				  G_CALLBACK (nullified_target_cb), core);
	}

	core->query_select_repl = replacements;

	mg_work_core_initialize (core);

	return obj;
}

static void
nullified_query_cb (MgQuery *query, MgWorkCore *core)
{
	g_signal_handlers_disconnect_by_func (G_OBJECT (query),
					      G_CALLBACK (nullified_query_cb), core);

	if (query == core->query_select) 
		core->query_select = NULL;

	if (query == core->query_select_improved) 
		core->query_select_improved = NULL;

	if (query == core->query_update) 
		core->query_update = NULL;
	
	if (query == core->query_delete) 
		core->query_delete = NULL;

	if (query == core->query_insert) 
		core->query_insert = NULL;
	
	g_object_unref (G_OBJECT (query));
}

static void
nullified_target_cb (MgTarget *target, MgWorkCore *core)
{
	g_signal_handlers_disconnect_by_func (G_OBJECT (target),
					      G_CALLBACK (nullified_target_cb), core);
	core->modif_target = NULL;
}

static void
nullified_context_cb (MgContext *context, MgWorkCore *core)
{
	g_signal_handlers_disconnect_by_func (G_OBJECT (context),
					      G_CALLBACK (nullified_context_cb), core);
       
	if (context == core->args_context) 
		core->args_context = NULL;

	if (context == core->work_context) 
		core->work_context = NULL;

	g_object_unref (G_OBJECT (context));
}


static void
mg_work_core_dispose (GObject *object)
{
        MgWorkCore *core;

        g_return_if_fail (object != NULL);
        g_return_if_fail (IS_MG_WORK_CORE (object));
        core = MG_WORK_CORE (object);

	/* hash tables */
	if (core->query_select_repl) {
		g_hash_table_destroy (core->query_select_repl);
		core->query_select_repl = NULL;
	}
	
	/* nodes */
	if (core->nodes) {
		GSList *list = core->nodes;
		while (list) {
			g_free (list->data);
			list = g_slist_next (list);
		}
		g_slist_free (core->nodes);
		core->nodes = NULL;
	}
	
	/* contexts */
	if (core->args_context)
		nullified_context_cb (core->args_context, core);
	
	if (core->work_context) 
		nullified_context_cb (core->work_context, core);
	
	/* queries */
	if (core->query_select)
		nullified_query_cb (core->query_select, core);
	if (core->query_select_improved)
		nullified_query_cb (core->query_select_improved, core);
	if (core->query_update)
		nullified_query_cb (core->query_update, core);
	if (core->query_delete)
		nullified_query_cb (core->query_delete, core);
	if (core->query_insert)
		nullified_query_cb (core->query_insert, core);
	
	/* mofifiable target */
	if (core->modif_target)
		nullified_target_cb (core->modif_target, core);
	
	/* for the parent class */
        parent_class->dispose (object);
}

/*
 * Real initialization
 */
static void 
mg_work_core_initialize (MgWorkCore *core)
{
	MgQuery *top_query = NULL;
	GSList *list;
	GSList *fields;

	/*
	 * Making modifications to the SELECT query to:
	 * -> expand any '*' into all the corresponding fields 
	 * -> add PK fields for the entity which will be modified ? -> not done
	 */
	/* expand any '*' into all the corresponding fields */
	fields = mg_entity_get_visible_fields (MG_ENTITY (core->query_select));
	list = fields;
	while (list) {
		if (IS_MG_QF_ALL (list->data)) {
			MgTarget *target;
			GSList *entfields, *list2;
			MgField *newfield;
			
			target = mg_qf_all_get_target (MG_QF_ALL (list->data));
			entfields = mg_entity_get_visible_fields (mg_target_get_represented_entity (target));
			list2 = entfields;
			while (list2) {
				newfield = MG_FIELD (mg_qf_field_new_with_objects (core->query_select,
										   target, MG_FIELD (list2->data)));
				mg_entity_add_field_before (MG_ENTITY (core->query_select), newfield,
							    MG_FIELD (list->data));
				mg_query_set_order_by_field (core->query_select, MG_QFIELD (newfield),
							     G_MAXINT, TRUE);
				mg_base_set_name (MG_BASE (newfield), mg_base_get_name (MG_BASE (list2->data)));
				mg_base_set_description (MG_BASE (newfield), 
							 mg_base_get_description (MG_BASE (list2->data)));
				g_object_unref (G_OBJECT (newfield));
				list2 = g_slist_next (list2);
			}
			g_slist_free (entfields);
			mg_qfield_set_visible (MG_QFIELD (list->data), FALSE);
		}
		list = g_slist_next (list);
	}
	g_slist_free (fields);

	/* add PK fields for the entity which will be modified */
	if (core->modif_target) {		
		if (!get_query_target_pkey (core->query_select, core->modif_target)) {
			/* TODO */
		}
	}
 
	/*
	 * args context 
	 */
	if (!core->args_context) {
		core->args_context = mg_entity_get_exec_context (MG_ENTITY (core->query_select));
		g_signal_connect (G_OBJECT (core->args_context), "nullified",
				  G_CALLBACK (nullified_context_cb), core);
	}

	/*
	 * work context 
	 */
	if (core->modif_target) {
		core->work_context = make_work_context_target (core);
		top_query = core->query_select_improved;
	}
	else 
		core->work_context = make_work_context_no_target (core);
	g_signal_connect (G_OBJECT (core->work_context), "nullified",
			  G_CALLBACK (nullified_context_cb), core);
	
	/*
	 * making modification queries
	 */
	if (core->modif_target) {
		MgGraphviz *graph;

		make_query_update (core);
		make_query_delete (core);
		make_query_insert (core);

#ifdef debug
		/*
		 * Graph output
		 */
		graph = MG_GRAPHVIZ (mg_graphviz_new (mg_base_get_conf (MG_BASE (core->query_select))));
		if (top_query)
			mg_graphviz_add_to_graph (graph, G_OBJECT (top_query));
		mg_graphviz_add_to_graph (graph, G_OBJECT (core->query_update));
		mg_graphviz_add_to_graph (graph, G_OBJECT (core->query_delete));
		mg_graphviz_add_to_graph (graph, G_OBJECT (core->query_insert));
		mg_graphviz_add_to_graph (graph, G_OBJECT (core->work_context));
		mg_graphviz_save_file (graph, "_work_context.dot", NULL);
		g_object_unref (G_OBJECT (graph));
#endif
	}
	

}

static GSList *
get_query_target_pkey (MgQuery *query, MgTarget *target)
{
	MgEntity *entity;
	GSList *pk_fields = NULL;

	entity = mg_target_get_represented_entity (target);
	if (IS_MG_DB_TABLE (entity)) {
		GSList *constraints, *list;

		constraints = mg_db_table_get_constraints (MG_DB_TABLE (entity));
		list = constraints;
		while (list && !pk_fields) {
			if (mg_db_constraint_get_constraint_type (MG_DB_CONSTRAINT (list->data)) == CONSTRAINT_PRIMARY_KEY) {
				MgDbConstraint *pkcons;
				gboolean allthere = TRUE;
				GSList *cons_pk_fields, *flist;

				pkcons = MG_DB_CONSTRAINT (list->data);
				cons_pk_fields = mg_db_constraint_pkey_get_fields (pkcons);
				flist = cons_pk_fields;
				while (flist && allthere) {
					MgQfield *field;

					field = mg_query_get_field_by_ref_field (query, flist->data);
					if (field)
						pk_fields = g_slist_append (pk_fields, field);
					else
						allthere = FALSE;
					flist = g_slist_next (flist);
				}
				g_slist_free (cons_pk_fields);
				if (!allthere) {
					g_slist_free (pk_fields);
					pk_fields = NULL;
				}
			}
			list = g_slist_next (list);
		}
		g_slist_free (constraints);
	}
	else {
		/* not yet possible at the moment... */
		TO_IMPLEMENT;
	}

	return pk_fields;
}

static void
make_query_update (MgWorkCore *core)
{
	GSList *list;
	MgTarget *target;
	MgQuery *query;
	MgConf *conf = mg_base_get_conf (MG_BASE (core->query_select));
	MgCondition *cond = NULL;
	GHashTable *replacements;
	MgQuery *top_query = core->query_select_improved;

	/* Query object */
	query = MG_QUERY (mg_query_new (conf));
	mg_query_set_query_type (query, MG_QUERY_TYPE_UPDATE);
	target = MG_TARGET (mg_target_new_with_entity (query, mg_target_get_represented_entity (core->modif_target)));
	mg_query_add_target (query, target, NULL);
	replacements = g_hash_table_new (NULL, NULL);

	/* Fields */
	g_object_get (G_OBJECT (top_query), "really_all_fields", &list, NULL);
	while (list) {
		if (IS_MG_QF_FIELD (list->data)) {
			if (mg_qf_field_get_target (MG_QF_FIELD (list->data)) == 
			    g_object_get_data (G_OBJECT (top_query), "my_target")) {
				MgField *field, *value_prov, *new_value_prov;
				MgParameter *param;
				
				field = MG_FIELD (mg_qf_field_new_with_objects (query, target,
										mg_qf_field_get_ref_field (MG_QF_FIELD (list->data))));
				mg_base_set_name (MG_BASE (field), mg_base_get_name (MG_BASE (list->data)));
				mg_entity_add_field (MG_ENTITY (query), field);
				g_object_unref (G_OBJECT (field));
				g_object_get (G_OBJECT (list->data), "value_provider", &value_prov, NULL);
				g_assert (value_prov);
				g_assert (IS_MG_QF_VALUE (value_prov));
				g_hash_table_insert (replacements, list->data, field);

				param = mg_context_find_parameter_for_field (core->work_context, MG_QFIELD (value_prov));
				new_value_prov = MG_FIELD (mg_qf_value_new (query, 
									mg_qf_value_get_server_data_type (MG_QF_VALUE (value_prov))));
				mg_base_set_name (MG_BASE (new_value_prov), mg_base_get_name (MG_BASE (value_prov)));
				mg_entity_add_field (MG_ENTITY (query), new_value_prov);
				mg_qfield_set_visible (MG_QFIELD (new_value_prov), FALSE);
				g_object_unref (G_OBJECT (new_value_prov));
				g_object_set (G_OBJECT (field), "value_provider", new_value_prov, NULL);
				
				mg_parameter_add_dest_field (param, MG_QFIELD (new_value_prov));
			}
		}

		list = g_slist_next (list);
	}
	
	/* Condition */
	g_object_get (G_OBJECT (top_query), "really_all_fields", &list, NULL);
	while (list) {
		if (IS_MG_QF_VALUE (list->data)) {
			if (g_object_get_data (G_OBJECT (list->data), "for_cond")) {
				MgQfield *field, *value;
				MgCondition *subcond;
				MgParameter *param;

				if (!cond) {
					cond = MG_CONDITION (mg_condition_new (query, MG_CONDITION_NODE_AND));
					mg_query_set_condition (query, cond);
					g_object_unref (G_OBJECT (cond));
				}

				field = g_hash_table_lookup (replacements, g_object_get_data (G_OBJECT (list->data), "for_cond"));
				g_assert (field);
				g_assert (MG_QF_FIELD (field));
				
				param = mg_context_find_parameter_for_field (core->work_context, MG_QFIELD (list->data));
				g_assert (param);
				value = MG_QFIELD (mg_qf_value_new (query, 
							    mg_field_get_data_type (mg_qf_field_get_ref_field (MG_QF_FIELD (field)))));
				mg_base_set_name (MG_BASE (value), mg_base_get_name (MG_BASE (list->data)));
				mg_entity_add_field (MG_ENTITY (query), MG_FIELD (value));
				mg_qfield_set_visible (MG_QFIELD (value), FALSE);
				g_object_unref (G_OBJECT (value));
				mg_parameter_add_dest_field (param, MG_QFIELD (value));

				subcond = MG_CONDITION (mg_condition_new (query, MG_CONDITION_LEAF_EQUAL));
				mg_condition_leaf_set_operator (subcond, MG_CONDITION_OP_LEFT, field);
				mg_condition_leaf_set_operator (subcond, MG_CONDITION_OP_RIGHT, value);
				mg_condition_node_add_child (cond, subcond, NULL);
				g_object_unref (G_OBJECT (subcond));
			}
		}
		list = g_slist_next (list);
	}

	
	core->query_update = query;
	g_signal_connect (G_OBJECT (core->query_update), "nullified",
			  G_CALLBACK (nullified_query_cb), core);



	/* Free memory */
	g_hash_table_destroy (replacements);
}

static void
make_query_delete (MgWorkCore *core)
{
	GSList *list;
	MgTarget *target;
	MgQuery *query;
	MgConf *conf = mg_base_get_conf (MG_BASE (core->query_select));
	MgCondition *cond = NULL;
	MgQuery *top_query = core->query_select_improved;

	/* Query object */
	query = MG_QUERY (mg_query_new (conf));
	mg_query_set_query_type (query, MG_QUERY_TYPE_DELETE);
	target = MG_TARGET (mg_target_new_with_entity (query, mg_target_get_represented_entity (core->modif_target)));
	mg_query_add_target (query, target, NULL);
	
	/* Condition */
	g_object_get (G_OBJECT (top_query), "really_all_fields", &list, NULL);
	while (list) {
		if (IS_MG_QF_VALUE (list->data)) {
			if (g_object_get_data (G_OBJECT (list->data), "for_cond")) {
				MgQfield *fieldv, *fieldf;
				MgCondition *subcond;
				MgParameter *param;

				if (!cond) {
					cond = MG_CONDITION (mg_condition_new (query, MG_CONDITION_NODE_AND));
					mg_query_set_condition (query, cond);
					g_object_unref (G_OBJECT (cond));
				}

				fieldf = g_object_get_data (G_OBJECT (list->data), "for_cond");
				fieldf = MG_QFIELD (mg_qf_field_new_with_objects (query, target,
										 mg_qf_field_get_ref_field (MG_QF_FIELD (fieldf))));
				mg_entity_add_field (MG_ENTITY (query), MG_FIELD (fieldf));
				g_object_unref (G_OBJECT (fieldf));
				mg_qfield_set_visible (fieldf, FALSE);

				param = mg_context_find_parameter_for_field (core->work_context, MG_QFIELD (list->data));
				g_assert (param);
				fieldv = MG_QFIELD (mg_qf_value_new (query, 
							   mg_field_get_data_type (mg_qf_field_get_ref_field (MG_QF_FIELD (fieldf)))));
				mg_entity_add_field (MG_ENTITY (query), MG_FIELD (fieldv));
				mg_qfield_set_visible (MG_QFIELD (fieldv), FALSE);
				g_object_unref (G_OBJECT (fieldv));
				mg_parameter_add_dest_field (param, MG_QFIELD (fieldv));

				subcond = MG_CONDITION (mg_condition_new (query, MG_CONDITION_LEAF_EQUAL));
				mg_condition_leaf_set_operator (subcond, MG_CONDITION_OP_LEFT, fieldf);
				mg_condition_leaf_set_operator (subcond, MG_CONDITION_OP_RIGHT, fieldv);
				mg_condition_node_add_child (cond, subcond, NULL);
				g_object_unref (G_OBJECT (subcond));
			}
		}
		list = g_slist_next (list);
	}

	
	core->query_delete = query;
	g_signal_connect (G_OBJECT (core->query_delete), "nullified",
			  G_CALLBACK (nullified_query_cb), core);
}

static void
make_query_insert (MgWorkCore *core)
{
	GSList *list;
	MgTarget *target;
	MgQuery *query;
	MgConf *conf = mg_base_get_conf (MG_BASE (core->query_select));
	MgQuery *top_query = core->query_select_improved;

	/* Query object */
	query = MG_QUERY (mg_query_new (conf));
	mg_query_set_query_type (query, MG_QUERY_TYPE_INSERT);
	target = MG_TARGET (mg_target_new_with_entity (query, mg_target_get_represented_entity (core->modif_target)));
	mg_query_add_target (query, target, NULL);

	/* Fields */
	g_object_get (G_OBJECT (top_query), "really_all_fields", &list, NULL);
	while (list) {
		if (IS_MG_QF_FIELD (list->data)) {
			if (mg_qf_field_get_target (MG_QF_FIELD (list->data)) == 
			    g_object_get_data (G_OBJECT (top_query), "my_target")) {
				MgField *field, *value_prov;
				MgParameter *param;
				
				field = MG_FIELD (mg_qf_field_new_with_objects (query, target,
										mg_qf_field_get_ref_field (MG_QF_FIELD (list->data))));
				mg_entity_add_field (MG_ENTITY (query), field);
				g_object_unref (G_OBJECT (field));
				g_object_get (G_OBJECT (list->data), "value_provider", &value_prov, NULL);
				g_assert (value_prov);
				g_assert (IS_MG_QF_VALUE (value_prov));

				param = mg_context_find_parameter_for_field (core->work_context, MG_QFIELD (value_prov));
				g_assert (param);
				value_prov = MG_FIELD (mg_qf_value_new (query, 
									mg_qf_value_get_server_data_type (MG_QF_VALUE (value_prov))));
				mg_entity_add_field (MG_ENTITY (query), value_prov);
				mg_qfield_set_visible (MG_QFIELD (value_prov), FALSE);
				g_object_unref (G_OBJECT (value_prov));
				g_object_set (G_OBJECT (field), "value_provider", value_prov, NULL);
				
				mg_parameter_add_dest_field (param, MG_QFIELD (value_prov));
			}
		}

		list = g_slist_next (list);
	}
	
	core->query_insert = query;
	g_signal_connect (G_OBJECT (core->query_insert), "nullified",
			  G_CALLBACK (nullified_query_cb), core);
}




/*
 * Computing the MgContext and the WorkCoreNodes
 * when core->modif_target is NULL
 */
MgContext *
make_work_context_no_target (MgWorkCore *core)
{
	GSList *fields, *list;
	GSList *params = NULL;
	MgContext *context;

        fields = mg_entity_get_visible_fields (MG_ENTITY (core->query_select));
        list = fields;
        while (list) {
                MgQfield *field = MG_QFIELD (list->data);
		
		if (! IS_MG_QF_ALL (field)) {
			MgWorkCoreNode *node = g_new0 (MgWorkCoreNode, 1);

			node->param = MG_PARAMETER (mg_parameter_new_with_field (field, mg_field_get_data_type (MG_FIELD (field))));
			node->position = mg_entity_get_field_index (MG_ENTITY (core->query_select), MG_FIELD (field));
			core->nodes = g_slist_append (core->nodes, node);
			params = g_slist_append (params, node->param);
			
			/* set the parameter properties */
			mg_base_set_name (MG_BASE (node->param), mg_base_get_name (MG_BASE (field)));
			mg_base_set_description (MG_BASE (node->param), mg_base_get_description (MG_BASE (field)));
			if (G_OBJECT_TYPE (field) == MG_QF_FIELD_TYPE) {
				MgField *realfield = mg_qf_field_get_ref_field (MG_QF_FIELD (field));
				if (G_OBJECT_TYPE (realfield) == MG_DB_FIELD_TYPE)
					mg_parameter_set_not_null (node->param,
								   !mg_db_field_is_null_allowed (MG_DB_FIELD (realfield)));
			}
		}

                list = g_slist_next (list);
        }

	context = MG_CONTEXT (mg_context_new (mg_base_get_conf (MG_BASE (core->query_select)), params));

	/* get rid of the params list since we don't use them anymore */
	list = params;
        while (list) { 
                g_object_unref (G_OBJECT (list->data));
                list = g_slist_next (list);
        }
        g_slist_free (params);

	return context;
}


/*
 * Computing the MgContext and the MgWorkCoreNodes
 * when core->modif_target is NOT NULL
 */
MgContext *
make_work_context_target (MgWorkCore *core)
{
	GHashTable *targets_h; /* (key, value) = (target, SELECT query for that target) */
	TargetDep *targets_d;
	GSList *fields, *list;
	GSList *sel_queries = NULL;
	MgQuery *top_query;
	MgContext *context;
	GHashTable *replacements; /* objects replacements in the copy of 'core->query_select' */
	GSList *cond_fields;

	g_return_val_if_fail (core->query_select, NULL);


	/*
	 * Create a tree of all the target dependencies
	 */
	targets_d = make_target_deps (core);

	targets_h = g_hash_table_new (NULL, NULL);

	/*
	 * make a copy of 'core->query_select'
	 */
	replacements = g_hash_table_new (NULL, NULL);
	top_query = MG_QUERY (mg_query_new_copy (core->query_select, replacements));
	g_object_set_data (G_OBJECT (top_query), "my_target", g_hash_table_lookup (replacements, core->modif_target));
	sel_queries = g_slist_append (sel_queries, top_query);
	g_hash_table_insert (targets_h, core->modif_target, top_query);

	/*
	 * For each MgQfValue in 'top_query', we need to remove the "value_provider" property if it exists because
	 * these parameters are only aliases for parameters we have in the 'core->args_context' and we don't 
	 * want to have a MgEntryCombo created for them.
	 */
	g_object_get (G_OBJECT (top_query), "really_all_fields", &list, NULL);
	while (list) {
		if (IS_MG_QF_VALUE (list->data))
			g_object_set (G_OBJECT (list->data), "value_provider", NULL, NULL);
		list = g_slist_next (list);
	}

	/*
	 * make a list ('sel_queries') of SELECT queries: one for each target in the 'core->query_select'
	 * query. Only the MgQfield of type MG_QF_FIELD are inserted into the new queries.
	 *
	 * For the 'core->modif_target', we don't start from scratch, but from the query created just above
	 */
	fields = mg_entity_get_visible_fields (MG_ENTITY (core->query_select));
	list = fields;
	while (list) {
		if (IS_MG_QF_FIELD (list->data)) {
			MgTarget *target = mg_qf_field_get_target (MG_QF_FIELD (list->data));
			MgQuery *query;
			MgField *realfield;
			MgTarget *newtarget;

			if (target != core->modif_target) {
				query = g_hash_table_lookup (targets_h, target);
				if (query) {
					/* add a field */
					realfield = mg_qf_field_get_ref_field (MG_QF_FIELD (list->data));
					newtarget = g_object_get_data (G_OBJECT (query), "my_target");
					realfield = MG_FIELD (mg_qf_field_new_with_objects (query, newtarget, realfield));
					mg_entity_add_field (MG_ENTITY (query), realfield);
					g_object_unref (G_OBJECT (realfield));

					/* set it to appear in the ORDER BY list */
					mg_query_set_order_by_field (query, MG_QFIELD (realfield), G_MAXINT, TRUE);
				}
				else {
					/* create a new query and insert in into targets_h */
					query = MG_QUERY (mg_query_new (mg_base_get_conf (MG_BASE (core->query_select))));
					mg_query_set_query_type (query, MG_QUERY_TYPE_SELECT);
					
					mg_base_set_name (MG_BASE (query), mg_base_get_name (MG_BASE (list->data)));
					mg_base_set_description (MG_BASE (query), mg_base_get_description (MG_BASE (list->data)));
					
					realfield = mg_qf_field_get_ref_field (MG_QF_FIELD (list->data));
					newtarget = MG_TARGET (mg_target_new_with_entity (query, 
											  mg_field_get_entity (realfield)));
					g_assert (mg_query_add_target (query, newtarget, NULL));
					g_object_set_data (G_OBJECT (query), "my_target", newtarget);
					
					realfield = MG_FIELD (mg_qf_field_new_with_objects (query, newtarget, realfield));
					mg_entity_add_field (MG_ENTITY (query), realfield);
					g_object_unref (G_OBJECT (newtarget));
					g_object_unref (G_OBJECT (realfield));
					
					g_hash_table_insert (targets_h, target, query);
					sel_queries = g_slist_append (sel_queries, query);
				}
			
				mg_base_set_name (MG_BASE (realfield), mg_base_get_name (MG_BASE (list->data)));
				mg_base_set_description (MG_BASE (realfield), mg_base_get_description (MG_BASE (list->data)));
			}
			else {
				/* MgQfValue for that field if necessary */
				MgQfValue *vfield;
				MgField *reffield;
				gchar *plugin;
				MgDataHandler *default_dh = NULL;

				query = top_query;
				realfield = g_hash_table_lookup (replacements, list->data);


				vfield = MG_QF_VALUE (mg_qf_value_new (query, mg_field_get_data_type (realfield)));
				mg_entity_add_field (MG_ENTITY (query), MG_FIELD (vfield));
				g_object_unref (G_OBJECT (vfield));
				mg_qf_value_set_is_parameter (vfield, TRUE);
				mg_qfield_set_visible (MG_QFIELD (vfield), FALSE);
				g_object_set (G_OBJECT (realfield), "value_provider", vfield, NULL);
				mg_qfield_set_internal (MG_QFIELD (vfield), TRUE);
				
				/* MgQfValue properties */
				mg_base_set_name (MG_BASE (vfield), mg_base_get_name (MG_BASE (realfield)));
				mg_base_set_description (MG_BASE (vfield), mg_base_get_description (MG_BASE (realfield)));
				if (IS_MG_QF_FIELD (realfield) && 
				    IS_MG_DB_FIELD (mg_qf_field_get_ref_field (MG_QF_FIELD (realfield)))) {
					MgDbField *dbfield = MG_DB_FIELD (mg_qf_field_get_ref_field (MG_QF_FIELD (realfield)));
					MgServer *srv;
					
					mg_qf_value_set_not_null (vfield, !mg_db_field_is_null_allowed (dbfield));
					if (mg_db_field_get_default_value (dbfield))
						mg_qf_value_set_default_value (vfield, mg_db_field_get_default_value (dbfield));
					
					srv = mg_conf_get_server (mg_base_get_conf (MG_BASE (core->query_select)));
					default_dh = mg_server_get_object_handler (srv, G_OBJECT (dbfield));
				}
				else {
					TO_IMPLEMENT;
				}
				
				/* Entry plugin if available */
				g_object_get (G_OBJECT (list->data), "handler_plugin", &plugin, NULL);
				if (plugin) 
					g_object_set (G_OBJECT (vfield), "handler_plugin", plugin, NULL);
				else {
					if (default_dh) {
						plugin = mg_base_get_name (MG_BASE (default_dh));
						g_object_set (G_OBJECT (vfield), "handler_plugin", plugin, NULL);
					}
				}
				
				reffield = mg_qf_field_get_ref_field (MG_QF_FIELD (realfield));
				if (IS_MG_DB_FIELD (reffield)) 
					mg_qf_value_set_not_null (vfield,
								  !mg_db_field_is_null_allowed (MG_DB_FIELD (reffield)));
				
				/* keep a reference to the field in 'core->query_select' which we are now treating */
				g_object_set_data (G_OBJECT (vfield), "position", list->data);
			}
		}
		list = g_slist_next (list);
	}

	g_slist_free (fields);

	/* For each target dependency, from the corresponding join, add the missing fields participating in the
	 * join to the SELECT queries associated with each target;
	 *
	 * And create some MgParameter objects on the 'FK' side and make them have a value provider on the 'PK' side.
	 */
	make_target_select_queries_improved (core, targets_d, targets_h, replacements, NULL);
	top_query = g_hash_table_lookup (targets_h, core->modif_target);


	/* Preparing the WHERE condition values for modification queries (UPDATE and DELETE) */
	cond_fields = get_query_target_pkey (core->query_select, core->modif_target);
	if (!cond_fields) {
		/* we don't have a primary key in the list of fields, so we just add all the fields
		 * and hope we don't modify too many tuples in the database.
		 * (there is no JOIN in the UPDATE and DELETE, so we only have fields visible
		 * from the original SELECT query to make criteria of).
		 */
		GSList *tmplist = mg_entity_get_visible_fields (MG_ENTITY (core->query_select));
		list = tmplist;
		while (list) {
			if (IS_MG_QF_FIELD (list->data) && 
			    (mg_qf_field_get_target (MG_QF_FIELD (list->data)) == core->modif_target)) {
				cond_fields = g_slist_append (cond_fields, list->data);
			}
			list = g_slist_next (list);
		}
		g_slist_free (tmplist);
	}
	list = cond_fields;
	while (list) {
		MgQfValue *vfield;
		
		vfield = MG_QF_VALUE (mg_qf_value_new (top_query, mg_field_get_data_type (MG_FIELD (list->data))));
		mg_entity_add_field (MG_ENTITY (top_query), MG_FIELD (vfield));
		g_object_unref (G_OBJECT (vfield));
		mg_qf_value_set_is_parameter (vfield, TRUE);
		mg_qfield_set_visible (MG_QFIELD (vfield), FALSE);
		mg_qfield_set_internal (MG_QFIELD (vfield), TRUE);
		
		/* MgQfValue Properties */
		if (IS_MG_QF_FIELD (list->data) && 
		    IS_MG_DB_FIELD (mg_qf_field_get_ref_field (MG_QF_FIELD (list->data)))) {
			MgDbField *dbfield = MG_DB_FIELD (mg_qf_field_get_ref_field (MG_QF_FIELD (list->data)));
			
			mg_qf_value_set_not_null (vfield, !mg_db_field_is_null_allowed (dbfield));
			if (mg_db_field_get_default_value (dbfield))
				mg_qf_value_set_default_value (vfield, mg_db_field_get_default_value (dbfield));
		}
		else {
			TO_IMPLEMENT;
		}


		/* keep a reference to the field in 'core->query_select' which we are now treating */
		g_object_set_data (G_OBJECT (vfield), "position", list->data);
		g_object_set_data (G_OBJECT (vfield), "for_cond", g_hash_table_lookup (replacements, list->data));

		mg_base_set_name (MG_BASE (vfield), "For Cond");
		
		list = g_slist_next (list);
	}
	g_slist_free (cond_fields);

	/*
	 * create context, and for each parameter, see if it is an alias for a parameter in 'core->args_context'
	 */
	context = mg_entity_get_exec_context (MG_ENTITY (top_query));
	if (core->args_context)
		list = core->args_context->parameters;
	else
		list = NULL;
	while (list) {
		GSList *ff = mg_parameter_get_dest_fields (MG_PARAMETER (list->data));
		MgQfield *alias_qf = NULL;

		while (ff && !alias_qf) {
			alias_qf = g_hash_table_lookup (replacements, ff->data);
			ff = g_slist_next (ff);
		}

		if (alias_qf) { /* find the param. in 'context' which is for 'alias_qf' */
			MgParameter *alias = mg_context_find_parameter_for_field (context, alias_qf);
			
			if (alias)
				mg_parameter_set_alias_of (alias, MG_PARAMETER (list->data));
		}
			
		list = g_slist_next (list);
	}
	g_hash_table_destroy (replacements);

	/* create MgWorkCoreNode nodes */
	list = context->parameters;
	while (list) {
		gint pos = -1;
		GSList *dest = mg_parameter_get_dest_fields (MG_PARAMETER (list->data));
		MgField *field;

		while (dest && (pos==-1)) {
			field = g_object_get_data (G_OBJECT (dest->data), "position");
			if (field)
				pos = mg_entity_get_field_index (MG_ENTITY (core->query_select), field);
			dest = g_slist_next (dest);
		}

		if (pos > -1) {
			MgWorkCoreNode *node = g_new0 (MgWorkCoreNode, 1);
			node->param = MG_PARAMETER (list->data);
			node->position = pos;
			core->nodes = g_slist_append (core->nodes, node);
		}
		
		list = g_slist_next (list);
	}

	/*
	 * Free allocated memory
	 */
#ifdef debug
	list = sel_queries;
	while (list) {
		gchar *gname = g_strdup_printf ("_sel_query%d.dot", g_slist_position (sel_queries, list));
		MgGraphviz *graph = MG_GRAPHVIZ (mg_graphviz_new (mg_base_get_conf (MG_BASE (core->query_select))));
		mg_graphviz_add_to_graph (graph, G_OBJECT (top_query));
		mg_graphviz_save_file (graph, gname, NULL);
		g_object_unref (G_OBJECT (graph));
		g_print ("Written file %s\n", gname);
		g_free (gname);

		list = g_slist_next (list);
	}
#endif
	list = sel_queries;
	while (list) {
		if (list->data != top_query)
			g_object_unref (G_OBJECT (list->data));
		else {
			core->query_select_improved = top_query;
			g_signal_connect (G_OBJECT (core->query_select_improved), "nullified",
					  G_CALLBACK (nullified_query_cb), core);
		}
		list = g_slist_next (list);
	}
	g_slist_free (sel_queries);
	make_target_deps_free (targets_d);
	g_hash_table_destroy (targets_h);

	return context;
}

static MgDbConstraint *find_fk_constraint_from_entities (MgEntity *ent_fk, MgEntity *ent_ref_pk);
static void            improve_queries_with_fk_constraint (MgWorkCore *core, TargetDep *dep, 
							   MgQuery *query_fk, MgQuery *query_ref_pk, MgDbConstraint *fkcons,
							   GHashTable *replacements);


gboolean
make_target_select_queries_improved (MgWorkCore *core, TargetDep *dep, GHashTable *target_query_h, GHashTable *replacements, GError **error)
{
	MgQuery *sel_query1, *sel_query2;
	MgJoin *join = dep->join;
	MgTarget *target1, *target2;
	MgEntity *ent1, *ent2;
	MgCondition *cond;
	GSList *list;

	/* recursive behaviour */
	list = dep->depend_on;
	while (list) {
		if (!make_target_select_queries_improved (core, TARGET_DEP (list->data), target_query_h, replacements, error)) {
			TO_IMPLEMENT; /* set error msg */
			return FALSE;
		}
		list = g_slist_next (list);
	}

	/* this TargetDep */
	if (!join)
		return TRUE;

	cond = mg_join_get_condition (join);

	target1 = dep->dependant->target;
	target2 = dep->target;
	ent1 = mg_target_get_represented_entity (target1);
	ent2 = mg_target_get_represented_entity (target2);

	sel_query1 = g_hash_table_lookup (target_query_h, target1);
	sel_query2 = g_hash_table_lookup (target_query_h, target2);
	if (!sel_query1 || !sel_query2)
		return TRUE;
	g_assert (sel_query1);
	g_assert (sel_query2);

	if (target1 == core->modif_target) {
		if (IS_MG_DB_TABLE (ent1)) {
			if (cond) {
				TO_IMPLEMENT;
			}
			else {
				/* we need to have a FK constraint here */
				if (IS_MG_DB_TABLE (ent2)) {
					MgDbConstraint *fkcons = find_fk_constraint_from_entities (ent1, ent2);
					if (fkcons) 
						improve_queries_with_fk_constraint (core, dep, sel_query1, sel_query2, fkcons, replacements);
					else {
						/* error: no FK constraint found for the join */;
						TO_IMPLEMENT; /* set error msg */
						return FALSE;
					}

				}
				else {
					TO_IMPLEMENT; /* set error msg */
					return FALSE;
				}
			}
		}
		else {
			TO_IMPLEMENT; /* set error msg */
			return FALSE;
		}
	}
	else {
		/* REM: target2 cannot be equal to 'core->modif_target' */
		if (cond) {
			/* use join condition */
			TO_IMPLEMENT;
		}
		else {
			if (IS_MG_DB_TABLE (ent1) && IS_MG_DB_TABLE (ent2)) {
				MgDbConstraint *fkcons = find_fk_constraint_from_entities (ent1, ent2);
				if (fkcons)
					improve_queries_with_fk_constraint (core, dep, sel_query1, sel_query2, fkcons, replacements);
				else {
					/* error: no FK constraint found for the join */;
					TO_IMPLEMENT; /* set error msg */
					return FALSE;
				}
			}
			else {
				TO_IMPLEMENT; /* set error msg */
				return FALSE;
			}
		}
	}

	return TRUE;
}

static void
improve_queries_with_fk_constraint (MgWorkCore *core, TargetDep *dep, MgQuery *query_fk, MgQuery *query_ref_pk, MgDbConstraint *fkcons,
				    GHashTable *replacements)
{
	GSList *pairs, *list;
	const GSList *clist;
	
	pairs = mg_db_constraint_fkey_get_fields (fkcons);
	list = pairs;
	while (list) {
		MgField *field;
		MgQfield *tmp;
		MgQfField *qfield, *qfield_pk, *query_sel_field;
		MgQfValue *vfield;
		MgCondition *cond, *newcond;
		
		/* adding PK fields if necessary to query_ref_pk */
		field = MG_FIELD (MG_DB_CONSTRAINT_FK_PAIR (list->data)->ref_pkey);
		tmp = mg_query_get_field_by_ref_field (query_ref_pk, field);
		if (!tmp) {
			MgTarget *newtarget;
			newtarget = g_object_get_data (G_OBJECT (query_ref_pk), "my_target");
			qfield = MG_QF_FIELD (mg_qf_field_new_with_objects (query_ref_pk,
									    newtarget,
									    field));
			mg_entity_add_field (MG_ENTITY (query_ref_pk), MG_FIELD (qfield));
			mg_qfield_set_internal (MG_QFIELD (qfield), TRUE);
			g_object_set_data (G_OBJECT (qfield), "ref_int_added", GINT_TO_POINTER (TRUE));
			g_object_unref (G_OBJECT (qfield));
		}
		else
			qfield = MG_QF_FIELD (tmp);
		qfield_pk = qfield;
		
		/* adding FK fields if necessary to 'core->query_select'*/
		field = MG_FIELD (MG_DB_CONSTRAINT_FK_PAIR (list->data)->fkey);
		tmp = mg_query_get_field_by_ref_field (core->query_select, field);
		if (!tmp) {
			MgTarget *newtarget;
			newtarget = dep->dependant->target;
			qfield = MG_QF_FIELD (mg_qf_field_new_with_objects (core->query_select,
									    newtarget,
									    field));
			query_sel_field = qfield;
			mg_entity_add_field (MG_ENTITY (core->query_select), MG_FIELD (qfield));
			mg_qfield_set_internal (MG_QFIELD (qfield), TRUE);
			g_object_set_data (G_OBJECT (qfield), "ref_int_added", GINT_TO_POINTER (TRUE));
			g_object_unref (G_OBJECT (qfield));
		}
		else
			query_sel_field = MG_QF_FIELD (tmp);
		
		
		/* adding FK fields if necessary to query_fk */
		field = MG_FIELD (MG_DB_CONSTRAINT_FK_PAIR (list->data)->fkey);
		tmp = mg_query_get_field_by_ref_field (query_fk, field);
		if (!tmp) {
			MgTarget *newtarget;
			newtarget = g_object_get_data (G_OBJECT (query_fk), "my_target");
			qfield = MG_QF_FIELD (mg_qf_field_new_with_objects (query_fk,
									    newtarget,
									    field));
			mg_entity_add_field (MG_ENTITY (query_fk), MG_FIELD (qfield));
			mg_qfield_set_internal (MG_QFIELD (qfield), TRUE);
			g_object_set_data (G_OBJECT (qfield), "ref_int_added", GINT_TO_POINTER (TRUE));
			g_object_unref (G_OBJECT (qfield));
		}
		else
			qfield = MG_QF_FIELD (tmp);

		if (replacements && !g_hash_table_lookup (replacements, query_sel_field))
			g_hash_table_insert (replacements, query_sel_field, qfield);
			
		
		/* add MgQfValue for this field */
		vfield = MG_QF_VALUE (mg_qf_value_new (query_fk, 
						       mg_field_get_data_type (field)));
		mg_entity_add_field (MG_ENTITY (query_fk), MG_FIELD (vfield));
		g_object_unref (G_OBJECT (vfield));
		mg_qf_value_set_is_parameter (vfield, TRUE);
		mg_qfield_set_visible (MG_QFIELD (vfield), FALSE);
		mg_qfield_set_internal (MG_QFIELD (vfield), TRUE);

		g_object_set (G_OBJECT (vfield), "value_provider", qfield_pk, NULL); /* when PK changes, so does this MgQfValue */

		g_object_set (G_OBJECT (qfield), "value_provider", vfield, NULL); /* FK is linked to the MgQfValue */
		cond = mg_query_get_condition (query_fk);
		if (!cond) {
			cond = MG_CONDITION (mg_condition_new (query_fk, MG_CONDITION_NODE_AND));
			mg_query_set_condition (query_fk, cond);
			g_object_unref (G_OBJECT (cond));
		}
		else {
			if (mg_condition_is_leaf (cond)) {
				g_object_ref (G_OBJECT (cond));
				newcond = MG_CONDITION (mg_condition_new (query_fk, MG_CONDITION_NODE_AND));
				mg_query_set_condition (query_fk, newcond);
				g_assert (mg_condition_node_add_child (newcond, cond, NULL));
				g_object_unref (G_OBJECT (cond));
				g_object_unref (G_OBJECT (newcond));
				cond = newcond;
			}
		}
		newcond = MG_CONDITION (mg_condition_new (query_fk, MG_CONDITION_LEAF_EQUAL));
		g_assert (mg_condition_node_add_child (cond, newcond, NULL));
		mg_condition_leaf_set_operator (newcond, MG_CONDITION_OP_LEFT, MG_QFIELD (qfield));
		mg_condition_leaf_set_operator (newcond, MG_CONDITION_OP_RIGHT, MG_QFIELD (vfield));
		g_object_unref (G_OBJECT (newcond));
		
		/* MgQfValue properties */
		mg_base_set_name (MG_BASE (vfield), mg_base_get_name (MG_BASE (field)));
		mg_base_set_description (MG_BASE (vfield), mg_base_get_description (MG_BASE (field)));
		mg_qf_value_set_not_null (vfield,
					  !mg_db_field_is_null_allowed (MG_DB_FIELD (field)));
		if (mg_db_field_get_default_value (MG_DB_FIELD (field)))
			mg_qf_value_set_default_value (vfield, mg_db_field_get_default_value (MG_DB_FIELD (field)));
		
		/* keep a reference to the field in 'core->query_select' which we 
		   are now treating */
		g_object_set_data (G_OBJECT (vfield), "position", query_sel_field);
		
		list = g_slist_next (list);
	}
	g_slist_free (pairs);


	/* setting query_ref_pk to be a 'param_source' for query_fk */
	clist = mg_query_get_param_sources (query_fk);
	if (!g_slist_find (clist, query_ref_pk))
		mg_query_add_param_source (query_fk, query_ref_pk);
}


/* find a FK constraint for the MgEntities couple (ent_fk, ent_ref_pk) */
static MgDbConstraint *
find_fk_constraint_from_entities (MgEntity *ent_fk, MgEntity *ent_ref_pk)
{
	MgDbConstraint *fkcons = NULL, *fkptr;
	GSList *fklist, *fk_constraints;
	MgConf *conf = mg_base_get_conf (MG_BASE (ent_fk));

	fk_constraints = mg_database_get_all_constraints (mg_conf_get_database (conf));
	fklist = fk_constraints;
	while (fklist && !fkcons) {
		fkptr = MG_DB_CONSTRAINT (fklist->data);
		if ((mg_db_constraint_get_constraint_type (fkptr) == CONSTRAINT_FOREIGN_KEY) &&
		    (((mg_db_constraint_get_table (fkptr) == MG_DB_TABLE (ent_fk)) &&
		      (mg_db_constraint_fkey_get_ref_table (fkptr) == MG_DB_TABLE (ent_ref_pk)))))
			fkcons = fkptr;
		fklist = g_slist_next (fklist);
	}
	g_slist_free (fk_constraints);

	/*g_print ("CONSTRAINT FIND (%s -> %s) -> %p\n", mg_base_get_name (MG_BASE (ent_fk)),
		 mg_base_get_name (MG_BASE (ent_ref_pk)), fkcons); */
	return fkcons;
}

gboolean
modif_target_depends_on (MgWorkCore *core, TargetDep *modif_target_dep, MgTarget *target)
{
	GSList *list;

	if (target == core->modif_target)
		return TRUE;

	if (modif_target_dep->target == target)
		return TRUE;

	list = modif_target_dep->depend_on;
	while (list) {
		if (modif_target_depends_on (core, TARGET_DEP (list->data), target))
			return TRUE;
		list = g_slist_next (list);
	}

	return FALSE;
}

#ifdef debug
static void
target_dep_dump (TargetDep *dep, gint offset)
{
	gchar *str;
        guint i;
	GSList *list;

	/* string for the offset */
        str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;

	/*g_print ("%sDEP target=%p (%s) dependant:%p\n", str, dep->target, mg_base_get_name (MG_BASE (dep->target)), 
		 dep->dependant ? dep->dependant->target : NULL); */
	list = dep->depend_on;
	while (list) {
		target_dep_dump (TARGET_DEP (list->data), offset+5);
		list = g_slist_next (list);
	}
	g_free (str);
}
#endif


/*
 * make all the required work to call make_target_deps_recurs
 */
TargetDep *
make_target_deps (MgWorkCore *core)
{
	TargetDep *dep;
	GSList *joins;
	GHashTable *hash;
	
	g_return_val_if_fail (core->query_select, NULL);
	g_return_val_if_fail (core->modif_target, NULL);

	joins = mg_query_get_joins (core->query_select);
	hash = g_hash_table_new (NULL, NULL);

	dep = make_target_deps_recurs (core, core->modif_target, joins, hash);

	g_slist_free (joins);
	g_hash_table_destroy (hash);

#ifdef debug
	g_print ("###################################### target deps\n");
	target_dep_dump (dep, 0);
#endif

	return dep;
}

/*
 * Creates a TargetDep structure to hold all the MgTarget on which 'on_target' depends.
 */
TargetDep *
make_target_deps_recurs (MgWorkCore *core, MgTarget *on_target, const GSList *joins, GHashTable *joinsdep)
{
	TargetDep *dep = NULL;
	const GSList *list;

	dep = g_new0 (TargetDep, 1);
	dep->target = on_target;
	dep->depend_on = NULL;
	dep->join = NULL;

	list = joins;
	while (list) {
		if (!g_hash_table_lookup (joinsdep, list->data)) {
			/* This join has not yet been treaded */
			MgTarget *t1, *t2, *tmp = NULL;
			MgJoinType type;
			gboolean useit = FALSE;

			t1 = mg_join_get_target_1 (MG_JOIN (list->data));
			t2 = mg_join_get_target_2 (MG_JOIN (list->data));
			type = mg_join_get_join_type (MG_JOIN (list->data));
			switch (type) {
			case MG_JOIN_TYPE_INNER:
				if ((t1 == on_target) || (t2 == on_target)) {
					useit = TRUE;
					if (t1 == on_target)
						tmp = t2;
					else
						tmp = t1;
				}
				break;
			case MG_JOIN_TYPE_RIGHT_OUTER:
				tmp = t1;
				t1 = t2;
				t2 = tmp;
			case MG_JOIN_TYPE_LEFT_OUTER:
				if (t1 == on_target) {
					useit = TRUE;
					tmp = t2;
				}
			default:
				/* we can't do anything here */
				break;
			}
			
			if (useit) {
				TargetDep *tdep;

				g_hash_table_insert (joinsdep, list->data, tmp);
				tdep = make_target_deps_recurs (core, tmp, joins, joinsdep);
				dep->depend_on = g_slist_append (dep->depend_on, tdep);
				tdep->join = MG_JOIN (list->data);
				tdep->dependant = dep;
			}
		}
		list = g_slist_next (list);
	}

	return dep;
}

void
make_target_deps_free (TargetDep *dep)
{
	GSList *list = dep->depend_on;
	
	while (list) {
		make_target_deps_free (TARGET_DEP (list->data));
		list = g_slist_next (list);
	}

	if (dep->depend_on)
		g_slist_free (dep->depend_on);
	g_free (dep);
}
