/* mg-referer.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-referer.h"
#include "marshal.h"

/* signals */
enum
{
        ACTIVATED,
        DEACTIVATED,
        LAST_SIGNAL
};

static gint mg_referer_signals[LAST_SIGNAL] = { 0, 0 };

static void mg_referer_iface_init (gpointer g_class);

GType
mg_referer_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgRefererIface),
			(GBaseInitFunc) mg_referer_iface_init,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) NULL,
			NULL,
			NULL,
			0,
			0,
			(GInstanceInitFunc) NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, "MgReferer", &info, 0);
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
	}
	return type;
}


static void
mg_referer_iface_init (gpointer g_class)
{
	static gboolean initialized = FALSE;

	if (! initialized) {
		mg_referer_signals[ACTIVATED] =
                        g_signal_new ("activated",
                                      MG_REFERER_TYPE,
                                      G_SIGNAL_RUN_FIRST,
                                      G_STRUCT_OFFSET (MgRefererIface, activated),
                                      NULL, NULL,
                                      marshal_VOID__VOID, G_TYPE_NONE,
                                      0);
		mg_referer_signals[DEACTIVATED] =
                        g_signal_new ("deactivated",
                                      MG_REFERER_TYPE,
                                      G_SIGNAL_RUN_FIRST,
                                      G_STRUCT_OFFSET (MgRefererIface, deactivated),
                                      NULL, NULL,
                                      marshal_VOID__VOID, G_TYPE_NONE,
                                      0);

		initialized = TRUE;
	}
}

/**
 * mg_referer_activate
 * @iface: an object which implements the #MgReferer interface
 *
 * Tries to activate the object, does nothing if the object is already active.
 *
 * Returns: TRUE if the object is active after the call
 */
gboolean
mg_referer_activate (MgReferer *iface)
{
	g_return_val_if_fail (iface && IS_MG_REFERER (iface), FALSE);
	
	if (MG_REFERER_GET_IFACE (iface)->activate)
		return (MG_REFERER_GET_IFACE (iface)->activate) (iface);
	
	return FALSE;
}

/**
 * mg_referer_deactivate
 * @iface: an object which implements the #MgReferer interface
 *
 * Deactivates the object. This is the opposite to function mg_referer_activate().
 * If the object is already non active, then nothing happens.
 */
void
mg_referer_deactivate (MgReferer *iface)
{
	g_return_if_fail (iface && IS_MG_REFERER (iface));
	
	if (MG_REFERER_GET_IFACE (iface)->deactivate)
		(MG_REFERER_GET_IFACE (iface)->deactivate) (iface);
}

/**
 * mg_referer_is_active
 * @iface: an object which implements the #MgReferer interface
 *
 * Get the status of an object
 *
 * Returns: TRUE if the object is active
 */
gboolean
mg_referer_is_active (MgReferer *iface)
{
	g_return_val_if_fail (iface && IS_MG_REFERER (iface), FALSE);
	
	if (MG_REFERER_GET_IFACE (iface)->is_active)
		return (MG_REFERER_GET_IFACE (iface)->is_active) (iface);
	
	return FALSE;
}

/**
 * mg_referer_get_ref_objects
 * @iface: an object which implements the #MgReferer interface
 *
 * Get the list of objects which are referenced by @iface. The returned list is a
 * new list. If @iface is not active, then the returned list is incomplete.
 *
 * Returns: a new list of referenced objects
 */
GSList *
mg_referer_get_ref_objects (MgReferer *iface)
{
	g_return_val_if_fail (iface && IS_MG_REFERER (iface), NULL);
	
	if (MG_REFERER_GET_IFACE (iface)->get_ref_objects)
		return (MG_REFERER_GET_IFACE (iface)->get_ref_objects) (iface);
	
	return NULL;
}

/**
 * mg_referer_replace_refs
 * @iface: an object which implements the #MgReferer interface
 * @replacements: a #GHashTable
 *
 * Ask @iface to replace references to objects listed as keys in the @replacements hash table
 * with references to objects of the corresponding value.
 *
 * It's up to the caller to make sure each pair of (key, value) objects in @replacements are of the
 * same type, and that it makes sense to procede to the replacement.
 *
 * The object implementing this interface will accept to do any work only if it is
 * already active.
 */
void
mg_referer_replace_refs (MgReferer *iface, GHashTable *replacements)
{
	g_return_if_fail (iface && IS_MG_REFERER (iface));
	
	if (!replacements)
		return;

	if (MG_REFERER_GET_IFACE (iface)->replace_refs)
		(MG_REFERER_GET_IFACE (iface)->replace_refs) (iface, replacements);
}
