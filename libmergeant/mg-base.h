/* mg-base.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


/*
 * This object is the base object for most of Mergeant's objects, it provides
 * basic facilities:
 * - a reference to the MgConf object
 * - a unique id which is used to XML storing procedures
 * - some attributes such as name, description and owner of the object (only used
 *   for DBMS object which are derived from this class.
 */


#ifndef __MG_BASE_H_
#define __MG_BASE_H_

#include "mg-defs.h"
#include <glib-object.h>
#include <libxml/tree.h>
#include "mg-conf.h"

G_BEGIN_DECLS

#define MG_BASE_TYPE          (mg_base_get_type())
#define MG_BASE(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_base_get_type(), MgBase)
#define MG_BASE_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_base_get_type (), MgBaseClass)
#define IS_MG_BASE(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_base_get_type ())


typedef struct _MgBase MgBase;
typedef struct _MgBaseClass MgBaseClass;
typedef struct _MgBasePrivate MgBasePrivate;

/* Properties:
 * name        type          read/write      description
 * ----------------------------------------------------------------
 * conf        pointer       RW              The MgConf object
 */

/* struct for the object's data */
struct _MgBase
{
	GObject            object;
	MgBasePrivate       *priv;
};

/* struct for the object's class */
struct _MgBaseClass
{
	GObjectClass            class;

	/* signals */
	void        (*changed)        (MgBase *base);
	void        (*id_changed)     (MgBase *base);
	void        (*name_changed)   (MgBase *base);
	void        (*descr_changed)  (MgBase *base);
	void        (*owner_changed)  (MgBase *base);
	void        (*nullified)      (MgBase *base);

	/* pure virtual functions */
	void        (*nullify)        (MgBase *base);
#ifdef debug
	void        (*dump)           (MgBase *base, guint offset);
#endif
};

guint        mg_base_get_type        (void);
GObject     *mg_base_new             (void);

void         mg_base_set_conf        (MgBase *base, MgConf *conf);
MgConf      *mg_base_get_conf        (MgBase *base);

void         mg_base_set_id          (MgBase *base, guint id);
void         mg_base_set_name        (MgBase *base, const gchar *name);
void         mg_base_set_description (MgBase *base, const gchar *descr);
void         mg_base_set_owner       (MgBase *base, const gchar *owner);

guint        mg_base_get_id          (MgBase *base);
const gchar *mg_base_get_name        (MgBase *base);
const gchar *mg_base_get_description (MgBase *base);
const gchar *mg_base_get_owner       (MgBase *base);

void         mg_base_nullify         (MgBase *base); /* force the object to completely clean itself */
void         mg_base_nullify_check   (MgBase *base); 

void         mg_base_changed         (MgBase *base);

#ifdef debug
void         mg_base_dump            (MgBase *base, guint offset); /* dump contents on stdout */
#endif

G_END_DECLS

#endif
