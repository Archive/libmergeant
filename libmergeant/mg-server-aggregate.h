/* mg-server-aggregate.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_SERVER_AGGREGATE_H_
#define __MG_SERVER_AGGREGATE_H_

#include "mg-base.h"
#include "mg-defs.h"

G_BEGIN_DECLS

#define MG_SERVER_AGGREGATE_TYPE          (mg_server_aggregate_get_type())
#define MG_SERVER_AGGREGATE(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_server_aggregate_get_type(), MgServerAggregate)
#define MG_SERVER_AGGREGATE_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_server_aggregate_get_type (), MgServerAggregateClass)
#define IS_MG_SERVER_AGGREGATE(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_server_aggregate_get_type ())


/* error reporting */
extern GQuark mg_server_aggregate_error_quark (void);
#define MG_SERVER_AGGREGATE_ERROR mg_server_aggregate_error_quark ()

enum
{
	MG_SERVER_AGGREGATE_XML_LOAD_ERROR
};


/* struct for the object's data */
struct _MgServerAggregate
{
	MgBase                         object;
	MgServerAggregatePrivate       *priv;
};

/* struct for the object's class */
struct _MgServerAggregateClass
{
	MgBaseClass                    class;

	/* signals */
	void   (*templ_signal)        (MgServerAggregate *obj);
};

guint             mg_server_aggregate_get_type      (void);
GObject          *mg_server_aggregate_new           (MgServer *srv);
void              mg_server_aggregate_set_dbms_id   (MgServerAggregate *agg, const gchar *id);
const gchar      *mg_server_aggregate_get_dbms_id   (MgServerAggregate *agg);
void              mg_server_aggregate_set_sqlname   (MgServerAggregate *agg, const gchar *sqlname);
const gchar      *mg_server_aggregate_get_sqlname   (MgServerAggregate *agg);
void              mg_server_aggregate_set_arg_type  (MgServerAggregate *agg, MgServerDataType *dt);
MgServerDataType *mg_server_aggregate_get_arg_type  (MgServerAggregate *agg);
void              mg_server_aggregate_set_ret_type  (MgServerAggregate *agg, MgServerDataType *dt);
MgServerDataType *mg_server_aggregate_get_ret_type  (MgServerAggregate *agg);

G_END_DECLS

#endif
