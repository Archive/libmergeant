/* mg-entity.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_ENTITY_H_
#define __MG_ENTITY_H_

#include <glib-object.h>
#include "mg-defs.h"
#include <libgda/gda-xql-item.h>

G_BEGIN_DECLS

#define MG_ENTITY_TYPE          (mg_entity_get_type())
#define MG_ENTITY(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_entity_get_type(), MgEntity)
#define IS_MG_ENTITY(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_entity_get_type ())
#define MG_ENTITY_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MG_ENTITY_TYPE, MgEntityIface))


/* struct for the interface */
struct _MgEntityIface
{
	GTypeInterface           g_iface;

	/* virtual table */
	GSList     *(*get_all_fields)       (MgEntity *iface);
	GSList     *(*get_visible_fields)   (MgEntity *iface);
	MgField    *(*get_field_by_name)    (MgEntity *iface, const gchar *name);
	MgField    *(*get_field_by_xml_id)  (MgEntity *iface, const gchar *xml_id);
	MgField    *(*get_field_by_index)   (MgEntity *iface, gint index);
	gint        (*get_field_index)      (MgEntity *iface, MgField *field);
	void        (*add_field)            (MgEntity *iface, MgField *field);
	void        (*add_field_before)     (MgEntity *iface, MgField *field, MgField *field_before);
	void        (*swap_fields)          (MgEntity *iface, MgField *field1, MgField *field2);
	void        (*remove_field)         (MgEntity *iface, MgField *field);
	gboolean    (*is_writable)          (MgEntity *iface);
	GSList     *(*get_parameters)       (MgEntity *iface);

	/* signals */
	void        (*field_added)          (MgEntity *iface, MgField *field);
	void        (*field_removed)        (MgEntity *iface, MgField *field);
	void        (*field_updated)        (MgEntity *iface, MgField *field);
	void        (*fields_order_changed) (MgEntity *iface);
};

GType       mg_entity_get_type        (void) G_GNUC_CONST;

GSList     *mg_entity_get_all_fields      (MgEntity *iface);
GSList     *mg_entity_get_visible_fields  (MgEntity *iface);
MgField    *mg_entity_get_field_by_name   (MgEntity *iface, const gchar *name);
MgField    *mg_entity_get_field_by_xml_id (MgEntity *iface, const gchar *xml_id);
MgField    *mg_entity_get_field_by_index  (MgEntity *iface, gint index);
gint        mg_entity_get_field_index     (MgEntity *iface, MgField *field);
void        mg_entity_add_field           (MgEntity *iface, MgField *field);
void        mg_entity_add_field_before    (MgEntity *iface, MgField *field, MgField *field_before);
void        mg_entity_swap_fields         (MgEntity *iface, MgField *field1, MgField *field2);
void        mg_entity_remove_field        (MgEntity *iface, MgField *field);
gboolean    mg_entity_is_writable         (MgEntity *iface);
GSList     *mg_entity_get_parameters      (MgEntity *iface);

MgContext  *mg_entity_get_exec_context    (MgEntity *iface);




G_END_DECLS

#endif
