/* mg-field.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-field.h"
#include "marshal.h"

static void mg_field_iface_init (gpointer g_class);

GType
mg_field_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgFieldIface),
			(GBaseInitFunc) mg_field_iface_init,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) NULL,
			NULL,
			NULL,
			0,
			0,
			(GInstanceInitFunc) NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, "MgField", &info, 0);
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
	}
	return type;
}


static void
mg_field_iface_init (gpointer g_class)
{
	static gboolean initialized = FALSE;

	if (! initialized) {
		initialized = TRUE;
	}
}


/**
 * mg_field_get_entity
 * @iface: an object which implements the #MgField interface
 *
 * Get a reference to the object implementing the #MgEntity interface to which
 * the object implementing the #MgField is attached to.
 *
 * Returns: the object implementing the #MgEntity interface
 */
MgEntity *
mg_field_get_entity (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_FIELD (iface), NULL);

	if (MG_FIELD_GET_IFACE (iface)->get_entity)
		return (MG_FIELD_GET_IFACE (iface)->get_entity) (iface);
	
	return NULL;
}

/**
 * mg_field_get_data_type
 * @iface: an object which implements the #MgField interface
 *
 * Get a data type of the object implementing the #MgField interface
 *
 * Returns: the corresponding #MgServerDataType
 */
MgServerDataType *
mg_field_get_data_type (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_FIELD (iface), NULL);

	if (MG_FIELD_GET_IFACE (iface)->get_data_type)
		return (MG_FIELD_GET_IFACE (iface)->get_data_type) (iface);
	
	return NULL;
}

/**
 * mg_field_get_name
 * @iface: an object which implements the #MgField interface
 *
 * Get the name of the object implementing the #MgField interface
 *
 * Returns: the corresponding #MgServerDataType
 */
const gchar *
mg_field_get_name (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_FIELD (iface), NULL);

	if (MG_FIELD_GET_IFACE (iface)->get_name)
		return (MG_FIELD_GET_IFACE (iface)->get_name) (iface);
	
	return NULL;
}

/**
 * mg_field_get_description
 * @iface: an object which implements the #MgField interface
 *
 * Get the description of the object implementing the #MgField interface
 *
 * Returns: the corresponding #MgServerDataType
 */
const gchar *
mg_field_get_description (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_FIELD (iface), NULL);

	if (MG_FIELD_GET_IFACE (iface)->get_description)
		return (MG_FIELD_GET_IFACE (iface)->get_description) (iface);
	
	return NULL;
}



