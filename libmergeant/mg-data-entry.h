/* mg-data-entry.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_DATA_ENTRY_H_
#define __MG_DATA_ENTRY_H_

#include <glib-object.h>
#include "mg-defs.h"
#include <libgda/libgda.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define MG_DATA_ENTRY_TYPE          (mg_data_entry_get_type())
#define MG_DATA_ENTRY(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_data_entry_get_type(), MgDataEntry)
#define IS_MG_DATA_ENTRY(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_data_entry_get_type ())
#define MG_DATA_ENTRY_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MG_DATA_ENTRY_TYPE, MgDataEntryIface))

typedef enum {
	MG_DATA_ENTRY_IS_NULL        = 1 << 0,
	MG_DATA_ENTRY_CAN_BE_NULL    = 1 << 1,
	MG_DATA_ENTRY_IS_DEFAULT     = 1 << 2,
	MG_DATA_ENTRY_CAN_BE_DEFAULT = 1 << 3,
	MG_DATA_ENTRY_IS_UNCHANGED   = 1 << 4,
	MG_DATA_ENTRY_ACTIONS_SHOWN  = 1 << 5,
	MG_DATA_ENTRY_DATA_NON_VALID = 1 << 6
} MgDataEntryAttribute;

/* struct for the interface */
struct _MgDataEntryIface
{
	GTypeInterface           g_iface;

	/* signals */
	void            (* contents_modified)     (MgDataEntry *de);
	void            (* status_changed)        (MgDataEntry *de);

	/* virtual table */
	void            ( *set_value_type)        (MgDataEntry *de, GdaValueType type);
	GdaValueType    ( *get_value_type)        (MgDataEntry *de);
	void            ( *set_value)             (MgDataEntry *de, const GdaValue * value);
	GdaValue       *( *get_value)             (MgDataEntry *de);
	void            ( *set_value_orig)        (MgDataEntry *de, const GdaValue * value);
	const GdaValue *( *get_value_orig)        (MgDataEntry *de);
	void            ( *set_value_default)     (MgDataEntry *de, const GdaValue * value);
	void            ( *set_attributes)        (MgDataEntry *de, guint attrs, guint mask);
	guint           ( *get_attributes)        (MgDataEntry *de);
	MgDataHandler  *( *get_handler)           (MgDataEntry *de);
	gboolean        ( *expand_in_layout)      (MgDataEntry *de);
};




guint           mg_data_entry_get_type               (void);

void            mg_data_entry_set_value_type         (MgDataEntry *de, GdaValueType type);
GdaValueType    mg_data_entry_get_value_type         (MgDataEntry *de);

void            mg_data_entry_set_value              (MgDataEntry *de, const GdaValue * value);
GdaValue       *mg_data_entry_get_value              (MgDataEntry *de);
void            mg_data_entry_set_value_orig         (MgDataEntry *de, const GdaValue * value);
const GdaValue *mg_data_entry_get_value_orig         (MgDataEntry *de);
void            mg_data_entry_set_value_default      (MgDataEntry *de, const GdaValue * value);

void            mg_data_entry_set_attributes         (MgDataEntry *de, guint attrs, guint mask);
guint           mg_data_entry_get_attributes         (MgDataEntry *de);

MgDataHandler  *mg_data_entry_get_handler            (MgDataEntry *de);
gboolean        mg_data_entry_expand_in_layout       (MgDataEntry *de);

G_END_DECLS

#endif
