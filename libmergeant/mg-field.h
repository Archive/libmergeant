/* mg-field.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_FIELD_H_
#define __MG_FIELD_H_

#include <glib-object.h>
#include "mg-defs.h"

G_BEGIN_DECLS

#define MG_FIELD_TYPE          (mg_field_get_type())
#define MG_FIELD(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_field_get_type(), MgField)
#define IS_MG_FIELD(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_field_get_type ())
#define MG_FIELD_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MG_FIELD_TYPE, MgFieldIface))


/* struct for the interface */
struct _MgFieldIface
{
	GTypeInterface           g_iface;

	/* virtual table */
	MgEntity         *(* get_entity)      (MgField *iface);
	MgServerDataType *(* get_data_type)   (MgField *iface);
	const gchar      *(* get_name)        (MgField *iface);
	const gchar      *(* get_description) (MgField *iface);
	const gchar      *(* get_alias)       (MgField *iface);
};

GType           mg_field_get_type        (void) G_GNUC_CONST;

MgEntity         *mg_field_get_entity      (MgField *iface);
MgServerDataType *mg_field_get_data_type   (MgField *iface);
const gchar      *mg_field_get_name        (MgField *iface);
const gchar      *mg_field_get_description (MgField *iface);

G_END_DECLS

#endif
