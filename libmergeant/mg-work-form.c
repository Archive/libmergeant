/* mg-work-form.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <string.h>
#include "mg-server.h"
#include "mg-work-form.h"
#include "mg-work-core.h"
#include "mg-query.h"
#include "mg-target.h"
#include "mg-entity.h"
#include "mg-renderer.h"
#include "mg-result-set.h"
#include "mg-form.h"
#include "mg-parameter.h"
#include "mg-context.h"
#include "mg-qfield.h"

#ifdef debug
#include "mg-graphviz.h"
#endif

static void mg_work_form_class_init (MgWorkFormClass * class);
static void mg_work_form_init (MgWorkForm * wid);
static void mg_work_form_dispose (GObject   * object);

static void mg_work_form_initialize (MgWorkForm *form);

static void nullified_core_cb (MgWorkCore *core, MgWorkForm *form);

static GtkWidget *actions_arrows_make (MgWorkForm *form);
static void       actions_arrows_update (MgWorkForm *form);

static GtkWidget *modif_buttons_make (MgWorkForm *form);
static void       modif_buttons_update (MgWorkForm *form);

static void       update_simple_form (MgWorkForm *form);
static void       arg_param_changed_cb (MgParameter *param, MgWorkForm *form);
static void       work_param_changed_cb (MgParameter *param, MgWorkForm *form);

static void modif_buttons_clicked_cb (GtkButton *button, MgWorkForm *form);
static void actions_arrows_clicked_cb (GtkButton *button, MgWorkForm *form);

enum {
	SIMPLE_FORM_UPDATE,
	SIMPLE_FORM_INSERT
};


struct _MgWorkFormPriv
{
	MgWorkCore        *core;
	MgResultSet       *data;
	gboolean           has_run;
	guint              cursor;

	GtkWidget         *notebook;
	GtkWidget         *basic_form;
	guint              basic_form_mode;
	
	guint              mode;
	GtkTooltips       *tooltips;

	GtkWidget         *nav_all;
	GtkWidget         *nav_first;
	GtkWidget         *nav_prev;
	GtkWidget         *nav_next;
	GtkWidget         *nav_last;
	GtkWidget         *nav_current;
	GtkWidget         *nav_scale;

	GtkWidget         *modif_all;
	GtkWidget         *modif_commit;
	GtkWidget         *modif_revert;
	GtkWidget         *modif_insert;
	GtkWidget         *modif_delete;
};

#define PAGE_NO_DATA 0
#define PAGE_FORM    1

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

guint
mg_work_form_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgWorkFormClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_work_form_class_init,
			NULL,
			NULL,
			sizeof (MgWorkForm),
			0,
			(GInstanceInitFunc) mg_work_form_init
		};		
		
		type = g_type_register_static (GTK_TYPE_VBOX, "MgWorkForm", &info, 0);
	}

	return type;
}

static void
mg_work_form_class_init (MgWorkFormClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);
	object_class->dispose = mg_work_form_dispose;
}

static void
mg_work_form_init (MgWorkForm * wid)
{
	wid->priv = g_new0 (MgWorkFormPriv, 1);
	wid->priv->core = NULL;
	wid->priv->has_run = FALSE;

	wid->priv->notebook = NULL;
	wid->priv->basic_form = NULL;
	wid->priv->basic_form_mode = SIMPLE_FORM_UPDATE;

	wid->priv->data = NULL;
	wid->priv->cursor = 0;

	wid->priv->mode = 0;
	wid->priv->tooltips = NULL;
}

/**
 * mg_work_form_new
 * @query: a #MgQuery object
 * @modified: a #MgTarget object, or %NULL
 *
 * Creates a new #MgWorkForm widget.
 *
 * @query must be a SELECT query (no union, etc selection query)
 *
 * The @modified target must belong to @query and represent
 * modifiable entity (a #MgDbTable for example). If @modified is %NULL then
 * no modification will be allowed.
 *
 * Returns: the new widget
 */
GtkWidget *
mg_work_form_new (MgQuery *query, MgTarget *modified)
{
	GObject *obj;
	MgWorkForm *form;

	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (mg_query_get_query_type (query) == MG_QUERY_TYPE_SELECT, NULL);
	
	if (modified) {
		g_return_val_if_fail (IS_MG_TARGET (modified), NULL);
		g_return_val_if_fail (mg_target_get_query (modified) == query, NULL);
		g_return_val_if_fail (mg_entity_is_writable (mg_target_get_represented_entity (modified)), NULL);
	}

	obj = g_object_new (MG_WORK_FORM_TYPE, NULL);
	form = MG_WORK_FORM (obj);

	form->priv->core = MG_WORK_CORE (mg_work_core_new (query, modified));
	g_signal_connect (G_OBJECT (form->priv->core), "nullified",
			  G_CALLBACK (nullified_core_cb), form);

	mg_work_form_initialize (form);

	return GTK_WIDGET (obj);
}

static void
nullified_core_cb (MgWorkCore *core, MgWorkForm *form)
{
	g_signal_handlers_disconnect_by_func (G_OBJECT (core),
					      G_CALLBACK (nullified_core_cb), form);
	
	if (form->priv->core->work_context) 
		g_signal_handlers_disconnect_by_func (G_OBJECT (form->priv->core->work_context),
						      G_CALLBACK (work_param_changed_cb), form);

	if (form->priv->has_run && form->priv->core->args_context)
		g_signal_handlers_disconnect_by_func (G_OBJECT (form->priv->core->args_context),
						      G_CALLBACK (arg_param_changed_cb), form);
	
	g_object_unref (G_OBJECT (form->priv->core));
	form->priv->core = NULL;
	gtk_widget_set_sensitive (GTK_WIDGET (form), FALSE);
}

static void
mg_work_form_dispose (GObject *object)
{
	MgWorkForm *form;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_WORK_FORM (object));
	form = MG_WORK_FORM (object);

	if (form->priv) {
		/* core */
		if (form->priv->core)
			nullified_core_cb (form->priv->core, form);

		/* data */
		if (form->priv->data) {
			g_object_unref (G_OBJECT (form->priv->data));
			form->priv->data = NULL;
		}

		/* tooltips */
		if (form->priv->tooltips) {
			gtk_object_destroy (GTK_OBJECT (form->priv->tooltips));
			form->priv->tooltips = NULL;
		}
		
		/* the private area itself */
		g_free (form->priv);
		form->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}


/**
 * mg_work_form_run
 * @form: a #MgWorkForm widget
 *
 * Makes the widget finish its initialization and start its operations
 */
void
mg_work_form_run (MgWorkForm *form, guint mode)
{
	GSList *list;

	g_return_if_fail (form && IS_MG_WORK_FORM (form));
	g_return_if_fail (form->priv);
	g_return_if_fail (form->priv->core->query_select);
	
	/*
	 * Hiding some entries of the form for which correspond to alias parameters
	 */
	list = form->priv->core->work_context->parameters;
	while (list) {
		if (mg_parameter_get_alias_of (MG_PARAMETER (list->data)) ||
		    g_object_get_data (G_OBJECT (list->data), "hide"))
			mg_form_show_param_entry (MG_FORM (form->priv->basic_form), 
						  MG_PARAMETER (list->data), FALSE);
		list = g_slist_next (list);
	}
	
	/*
	 * Hiding parameters which are for internal fields
	 */
	if (form->priv->core->query_select_improved) {
		GSList *tmplist = mg_entity_get_visible_fields (MG_ENTITY (form->priv->core->query_select_improved));
		list = tmplist;
		while (list) {
			if (mg_qfield_is_internal (MG_QFIELD (list->data)) &&
			    !g_object_get_data (G_OBJECT (list->data), "ref_int_added")) {
				/* REM: we need to add a "ref_int_added" propriety to make the difference between the
				 * query fields which were originally set as internal end the ones we added during the PK/FK
				 * queries improvements. This is required since the MgEntryCombo makes use of the internal
				 * attribute of some query field objects */
				MgParameter *param;
				MgQfield *value_prov;
				
				g_object_get (G_OBJECT (list->data), "value_provider", &value_prov, NULL);
				if (value_prov) {
					param = mg_context_find_parameter_for_field (form->priv->core->work_context, value_prov);
					if (param)
						mg_form_show_param_entry (MG_FORM (form->priv->basic_form), param, FALSE);
				}
			}
			list = g_slist_next (list);
		}
		g_slist_free (tmplist);
	}


	/*
	 * Missing parameters?
	 */
	if (!mg_context_is_valid (form->priv->core->args_context)) {
		GtkWidget *dlg;
		gint result;
		GtkWidget *parent;
		MgForm *sform;

		parent = gtk_widget_get_parent (GTK_WIDGET (form));
		while (parent && !GTK_IS_WINDOW (parent)) 
			parent = gtk_widget_get_parent (parent);

		dlg = mg_form_new_in_dialog (mg_base_get_conf (MG_BASE (form->priv->core->query_select)), 
					     form->priv->core->args_context, GTK_WINDOW (parent),
					     NULL, NULL);
		sform = g_object_get_data (G_OBJECT (dlg), "form");
		mg_form_set_entries_auto_default (sform, TRUE);

		gtk_widget_show (dlg);
		result = gtk_dialog_run (GTK_DIALOG (dlg));
		gtk_widget_destroy (dlg);
		switch (result) {
		case GTK_RESPONSE_ACCEPT:
			break;
		default:
			return;
			break;
		}
	}

	/*
	 * Signals connecting
	 */
	if (form->priv->core->args_context)
		g_signal_connect (G_OBJECT (form->priv->core->args_context), "changed",
				  G_CALLBACK (arg_param_changed_cb), form);
	form->priv->has_run = TRUE;

	/*
	 * Actual start
	 */
	form->priv->mode = mode;
	arg_param_changed_cb (NULL, form);
}

/**
 * mg_work_form_set_action_mode
 * @form: & #MgWorkForm widget
 * @mode: an OR'ed value of possible modes
 *
 * Sets the attributes for the operating mode of the widget
 */
void
mg_work_form_set_action_mode (MgWorkForm *form, guint mode)
{
	g_return_if_fail (form && IS_MG_WORK_FORM (form));
	g_return_if_fail (form->priv);

	form->priv->mode = mode;

	/* updating the various possible actions */
	actions_arrows_update (form);
	modif_buttons_update (form);
}

/*
 * this function (re)computes the SELECT query and (re)runs it, resulting
 * in a change in the displayed values
 *
 * It does not use the 'param' argument
 */
static void
arg_param_changed_cb (MgParameter *param, MgWorkForm *form)
{
	gchar *sql;
	GError *error = NULL;

	/* get rid of old data */
	if (form->priv->data) {
		g_object_unref (G_OBJECT (form->priv->data));
		form->priv->data = NULL;
	}

	if (form->priv->core->query_select) {
#ifdef debug
		MgGraphviz *graph = MG_GRAPHVIZ (mg_graphviz_new (mg_base_get_conf (MG_BASE (form->priv->core->query_select))));
		mg_graphviz_add_to_graph (graph, G_OBJECT (form->priv->core->query_select));
		mg_graphviz_save_file (graph, "_new_query.dot", NULL);
		g_object_unref (G_OBJECT (graph));
#endif
		/* Actual running query */
		sql = mg_renderer_render_as_sql (MG_RENDERER (form->priv->core->query_select), 
						 form->priv->core->args_context, &error);
		if (sql) {
			MgConf *conf = mg_base_get_conf (MG_BASE (form->priv->core->query_select));
			
#ifdef debug
			/* g_print ("===========================================================\n"); */
/* 			mg_base_dump (MG_BASE (form->priv->core->work_context), 0); */
/* 			g_print ("===========================================================\n"); */
			g_print ("SQL: %s\n", sql);
#endif
			form->priv->data = mg_server_do_query (mg_conf_get_server (conf), sql, MG_SERVER_QUERY_SQL, NULL);
			if (form->priv->data) {
				if (form->priv->cursor >= mg_resultset_get_nbtuples (form->priv->data))
					form->priv->cursor = mg_resultset_get_nbtuples (form->priv->data) - 1;
				update_simple_form (form);
			}
			g_free (sql);
		}
		else {
			GtkWidget *dlg;
			gchar *message;
			GtkWidget *parent;
			
			/* find the top level window of the form */
			parent = gtk_widget_get_parent (GTK_WIDGET (form));
			while (parent && !GTK_IS_WINDOW (parent)) 
				parent = gtk_widget_get_parent (parent);
			
			if (error) {
				message = g_strdup (error->message);
				g_error_free (error);
			}
			else
				message = g_strdup_printf (_("An unknown error occured while executing the query."));

			dlg = gtk_message_dialog_new (GTK_WINDOW (parent), 0,
						      GTK_MESSAGE_ERROR,
						      GTK_BUTTONS_CLOSE,
						      message);
			g_free (message);
			gtk_dialog_run (GTK_DIALOG (dlg));
			gtk_widget_destroy (dlg);
		}
	}

	/* updating the various possible actions */
	actions_arrows_update (form);
	modif_buttons_update (form);
}

static void
work_param_changed_cb (MgParameter *param, MgWorkForm *form)
{
	modif_buttons_update (form);
}
/*
 * Real initialization
 */
static void 
mg_work_form_initialize (MgWorkForm *form)
{
	MgConf *conf = mg_base_get_conf (MG_BASE (form->priv->core->query_select));
	GtkWidget *realform, *wid, *nb, *group;
	GSList *list;

	/* signals connecting */
	if (form->priv->core->work_context)
		g_signal_connect (G_OBJECT (form->priv->core->work_context), "changed",
				  G_CALLBACK (work_param_changed_cb), form);


	/*
	 * the "No Data" notice
	 */
	nb = gtk_notebook_new ();
	form->priv->notebook = nb;
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (nb), FALSE);
	gtk_notebook_set_show_border (GTK_NOTEBOOK (nb), FALSE);
	gtk_box_pack_start (GTK_BOX (form), nb, TRUE, TRUE, 0);
	gtk_widget_show (nb);
	wid = gtk_label_new (_("No data to be displayed"));
	gtk_widget_show (wid);
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), wid, NULL);

	/*
	 * the actions part
	 */
	wid = gtk_hseparator_new ();
	gtk_box_pack_start (GTK_BOX (form), wid, FALSE, FALSE, 5);
	gtk_widget_show (wid);

	group = actions_arrows_make (form);
	gtk_box_pack_start (GTK_BOX (form), group, FALSE, FALSE, 0);
	gtk_widget_show (group);
	form->priv->nav_all = group;

	group = modif_buttons_make (form);
	gtk_box_pack_start (GTK_BOX (form), group, FALSE, FALSE, 0);
	gtk_widget_show (group);
	form->priv->modif_all = group;

	/* 
	 * the form itself 
	 */
	realform = mg_form_new (conf, form->priv->core->work_context);
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), realform, NULL);
	gtk_widget_show (realform);
	mg_form_show_entries_actions (MG_FORM (realform), form->priv->core->modif_target ? TRUE : FALSE);
	form->priv->basic_form = realform;

	/*
	 * Prepare to hide some unwanted entries in the form
	 * (we can't do it from here since when the widget is displayed everything is shown!)
	 */
	if (form->priv->core->query_select_improved) {
		g_object_get (G_OBJECT (form->priv->core->query_select_improved), 
			      "really_all_fields", &list, NULL);
		while (list) {
			if (g_object_get_data (G_OBJECT (list->data), "for_cond")) {
				MgParameter *param;

				param = mg_context_find_parameter_for_field (form->priv->core->work_context, MG_QFIELD (list->data));
				g_assert (param);
				g_object_set_data (G_OBJECT (param), "hide", GINT_TO_POINTER (TRUE));
			}
			list = g_slist_next (list);
		}
	}

	/* tooltips */
	form->priv->tooltips = gtk_tooltips_new ();
}


/*
 * updates all the parameters in form->priv->core->work_context to the values of form->priv->data
 * pointed by the form->priv->cursor cursor
 */
static void
update_simple_form (MgWorkForm *form)
{
	GSList *list = form->priv->core->nodes;
	const GdaValue *value;

	if (form->priv->basic_form_mode == SIMPLE_FORM_INSERT) {
		/* INSERT mode */
		if (list)
			gtk_notebook_set_current_page (GTK_NOTEBOOK (form->priv->notebook), PAGE_FORM);
		else
			gtk_notebook_set_current_page (GTK_NOTEBOOK (form->priv->notebook), PAGE_NO_DATA);

		while (list) {
			mg_parameter_set_value (MG_WORK_CORE_NODE (list->data)->param, NULL);
			list = g_slist_next (list);
		}
		mg_form_set_entries_default (MG_FORM (form->priv->basic_form));
		mg_form_set_entries_auto_default (MG_FORM (form->priv->basic_form), TRUE);
	}
	else {
		/* UPDATE mode */
		mg_form_set_entries_auto_default (MG_FORM (form->priv->basic_form), FALSE);
		if (!form->priv->data ||
		    (form->priv->data && (mg_resultset_get_nbtuples (form->priv->data) == 0))) {
			/* no data to be displayed */
			gtk_notebook_set_current_page (GTK_NOTEBOOK (form->priv->notebook), PAGE_NO_DATA);
			return;
		}
		
		if (list) {
			gtk_notebook_set_current_page (GTK_NOTEBOOK (form->priv->notebook), PAGE_FORM);
			while (list) {
				value = mg_resultset_get_gdavalue (form->priv->data, form->priv->cursor, 
								   MG_WORK_CORE_NODE (list->data)->position);
				mg_parameter_set_value (MG_WORK_CORE_NODE (list->data)->param, value);
				list = g_slist_next (list);
			}
		}
	}
}



/*
 *
 * Action arrows (First, prev, next, last)
 *
 */
static void actions_arrows_value_changed_cb (GtkRange *range, MgWorkForm *form);
static GtkWidget *
actions_arrows_make (MgWorkForm *form)
{
	GtkWidget *hbox, *button, *img, *wid;

	hbox = gtk_hbox_new (FALSE, 0);

	/* FIRST record */
	img = gtk_image_new_from_stock (GTK_STOCK_GOTO_FIRST, GTK_ICON_SIZE_SMALL_TOOLBAR);
	gtk_widget_show (img);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), img);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);
	form->priv->nav_first = button;
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (actions_arrows_clicked_cb), form);
	g_object_set_data (G_OBJECT (button), "a", GINT_TO_POINTER (-2));

	/* PREV record */
	img = gtk_image_new_from_stock (GTK_STOCK_GO_BACK, GTK_ICON_SIZE_SMALL_TOOLBAR);
	gtk_widget_show (img);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), img);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);
	form->priv->nav_prev = button;
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (actions_arrows_clicked_cb), form);
	g_object_set_data (G_OBJECT (button), "a", GINT_TO_POINTER (-1));


	/* Nav Scale */
	wid = gtk_hscale_new_with_range (0, 1, 1);
	gtk_scale_set_draw_value (GTK_SCALE (wid), FALSE);
	gtk_scale_set_digits (GTK_SCALE (wid), 0);
	gtk_box_pack_start_defaults (GTK_BOX (hbox), wid);
	gtk_widget_show (wid);
	gtk_widget_set_sensitive (wid, FALSE);
	form->priv->nav_scale = wid;
	g_signal_connect (G_OBJECT (wid), "value_changed",
			  G_CALLBACK (actions_arrows_value_changed_cb), form);

	/* NEXT record */
	img = gtk_image_new_from_stock (GTK_STOCK_GO_FORWARD, GTK_ICON_SIZE_SMALL_TOOLBAR);
	gtk_widget_show (img);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), img);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);
	form->priv->nav_next = button;
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (actions_arrows_clicked_cb), form);
	g_object_set_data (G_OBJECT (button), "a", GINT_TO_POINTER (1));

	/* FIRST record */
	img = gtk_image_new_from_stock (GTK_STOCK_GOTO_LAST, GTK_ICON_SIZE_SMALL_TOOLBAR);
	gtk_widget_show (img);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), img);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);
	form->priv->nav_last = button;
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (actions_arrows_clicked_cb), form);
	g_object_set_data (G_OBJECT (button), "a", GINT_TO_POINTER (2));

	/* rows counter */
	wid = gtk_label_new ("? / ?");
	gtk_box_pack_start (GTK_BOX (hbox), wid, FALSE, FALSE, 2);
	gtk_widget_show (wid);
	form->priv->nav_current = wid;

	return hbox;
}

static void
actions_arrows_update (MgWorkForm *form)
{
	/* global mode */
	if (form->priv->mode & MG_WORK_FORM_NAVIGATION_ARROWS) {
		gtk_widget_show (form->priv->nav_all);
		if (form->priv->mode & MG_WORK_FORM_NAVIGATION_SCROLL)
			gtk_widget_show (form->priv->nav_scale);
		else
			gtk_widget_hide (form->priv->nav_scale);
	}
	else
		gtk_widget_hide (form->priv->nav_all);

	/* sensitiveness of the widgets */
	if (form->priv->basic_form_mode == SIMPLE_FORM_INSERT) {
		gtk_widget_set_sensitive (form->priv->nav_first, FALSE);
		gtk_widget_set_sensitive (form->priv->nav_prev, FALSE);
		gtk_widget_set_sensitive (form->priv->nav_next, FALSE);
		gtk_widget_set_sensitive (form->priv->nav_last, FALSE);
		gtk_widget_set_sensitive (form->priv->nav_scale, FALSE);
	}
	else {
		gint nrows = 0;
		
		if (form->priv->data)
			nrows = mg_resultset_get_nbtuples (form->priv->data);
		
		if (form->priv->data && (nrows > 0)) {
			gchar *str;
			
			gtk_widget_set_sensitive (form->priv->nav_first, (form->priv->cursor == 0) ? FALSE : TRUE);
			gtk_widget_set_sensitive (form->priv->nav_prev, (form->priv->cursor == 0) ? FALSE : TRUE);
			gtk_widget_set_sensitive (form->priv->nav_next, (form->priv->cursor == nrows -1) ? FALSE : TRUE);
			gtk_widget_set_sensitive (form->priv->nav_last, (form->priv->cursor == nrows -1) ? FALSE : TRUE);
			
			str = g_strdup_printf ("%d / %d", form->priv->cursor+1, nrows);
			gtk_label_set_text (GTK_LABEL (form->priv->nav_current), str);
			g_free (str);
			
			gtk_widget_set_sensitive (form->priv->nav_scale, nrows == 1 ? FALSE : TRUE);
			if (nrows == 1)
				gtk_range_set_range (GTK_RANGE (form->priv->nav_scale), 1, 2);
			else
				gtk_range_set_range (GTK_RANGE (form->priv->nav_scale), 1, nrows);
			
			if (gtk_range_get_value (GTK_RANGE (form->priv->nav_scale)) != form->priv->cursor+1)
				gtk_range_set_value (GTK_RANGE (form->priv->nav_scale), form->priv->cursor+1);
		}
		else {
			gtk_widget_set_sensitive (form->priv->nav_first, FALSE);
			gtk_widget_set_sensitive (form->priv->nav_prev, FALSE);
			gtk_widget_set_sensitive (form->priv->nav_next, FALSE);
			gtk_widget_set_sensitive (form->priv->nav_last, FALSE);
			gtk_label_set_text (GTK_LABEL (form->priv->nav_current), "? / ?");
			gtk_widget_set_sensitive (form->priv->nav_scale, FALSE);
			gtk_range_set_range (GTK_RANGE (form->priv->nav_scale), 0, 1);
		}
	}
}

static void
actions_arrows_clicked_cb (GtkButton *button, MgWorkForm *form)
{
	gint action;

	if (form->priv->mode & MG_WORK_FORM_MODIF_AUTO_COMMIT) {
		/* see if some data have been modified and need to be written to the DBMS */
		if (mg_form_has_been_changed (MG_FORM (form->priv->basic_form))) 
			/* simulate a clicked of 'SAVE' button */
			modif_buttons_clicked_cb (GTK_BUTTON (form->priv->modif_commit), form);
	}
	
	action = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (button), "a"));
	switch (action) {
	case -2:
		form->priv->cursor = 0;
		break;
	case -1:
		if (form->priv->cursor > 0)
			form->priv->cursor--;
		break;
	case 1:
		if (form->priv->data &&
		    (form->priv->cursor < mg_resultset_get_nbtuples (form->priv->data) - 1 ))
			form->priv->cursor++;
		break;
	case 2:
		if (form->priv->data)
			form->priv->cursor = mg_resultset_get_nbtuples (form->priv->data) - 1;
		break;
	default:
		g_assert_not_reached ();
	}

	/* updates */
	update_simple_form (form);
	actions_arrows_update (form);
}

static void
actions_arrows_value_changed_cb (GtkRange *range, MgWorkForm *form)
{
	gint value = gtk_range_get_value (GTK_RANGE (form->priv->nav_scale));
	if ((value >= 1) &&
	    (value <= mg_resultset_get_nbtuples (form->priv->data)))
		form->priv->cursor = value - 1;

	/* updates */
	update_simple_form (form);
	actions_arrows_update (form);
}



/*
 *
 * Modification buttons (Commit changes, Reset form, New entry, Delete)
 *
 */
static GtkWidget *
modif_buttons_make (MgWorkForm *form)
{
	GtkWidget *hbox, *button, *img;

	hbox = gtk_hbox_new (FALSE, 0);

	/* Commit changes button */
	img = gtk_image_new_from_stock (GTK_STOCK_SAVE, GTK_ICON_SIZE_SMALL_TOOLBAR);
	gtk_widget_show (img);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), img);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);
	form->priv->modif_commit = button;
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (modif_buttons_clicked_cb), form);
	g_object_set_data (G_OBJECT (button), "a", "c");

	/* Reset form */
	img = gtk_image_new_from_stock (GTK_STOCK_REFRESH, GTK_ICON_SIZE_SMALL_TOOLBAR);
	gtk_widget_show (img);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), img);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);
	form->priv->modif_revert = button;
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (modif_buttons_clicked_cb), form);
	g_object_set_data (G_OBJECT (button), "a", "r");


	/* New entry button */
	img = gtk_image_new_from_stock (GTK_STOCK_NEW, GTK_ICON_SIZE_SMALL_TOOLBAR);
	gtk_widget_show (img);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), img);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);
	form->priv->modif_insert = button;
	gtk_widget_set_sensitive (button, TRUE);	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (modif_buttons_clicked_cb), form);
	g_object_set_data (G_OBJECT (button), "a", "i");

	/* Delete entry button */
	img = gtk_image_new_from_stock (GTK_STOCK_DELETE, GTK_ICON_SIZE_SMALL_TOOLBAR);
	gtk_widget_show (img);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), img);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);
	form->priv->modif_delete = button;
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (modif_buttons_clicked_cb), form);
	g_object_set_data (G_OBJECT (button), "a", "d");

	return hbox;
}

static void
modif_buttons_update (MgWorkForm *form)
{
	gint nrows = 0;
	gboolean changed;

	/* global mode */
	if ((form->priv->mode & MG_WORK_FORM_MODIF_BUTTONS) && form->priv->basic_form)
		gtk_widget_show (form->priv->modif_all);
	else
		gtk_widget_hide (form->priv->modif_all);

	/* sensitiveness */
	if (! form->priv->basic_form)
		return;

	changed = mg_form_has_been_changed (MG_FORM (form->priv->basic_form));
	if (form->priv->data)
		nrows = mg_resultset_get_nbtuples (form->priv->data);

	gtk_widget_set_sensitive (form->priv->modif_commit, changed ? TRUE : FALSE);
	gtk_widget_set_sensitive (form->priv->modif_revert, 
				  changed || (form->priv->basic_form_mode == SIMPLE_FORM_INSERT) ? TRUE : FALSE);
	gtk_widget_set_sensitive (form->priv->modif_insert, TRUE);
	gtk_widget_set_sensitive (form->priv->modif_delete, 
			          (form->priv->data && (nrows > 0)) && (form->priv->basic_form_mode == SIMPLE_FORM_UPDATE) ? TRUE : FALSE);

}

static void
modif_buttons_clicked_cb (GtkButton *button, MgWorkForm *form)
{
	gchar *action;
	MgQuery *query = NULL;
	guint initial_mode = form->priv->basic_form_mode;

	action = g_object_get_data (G_OBJECT (button), "a");
	switch (*action) {
	case 'c':
		if (form->priv->basic_form_mode == SIMPLE_FORM_INSERT)
			query = form->priv->core->query_insert;
		else
			query = form->priv->core->query_update;
		form->priv->basic_form_mode = SIMPLE_FORM_UPDATE;
		break;
	case 'r':
		form->priv->basic_form_mode = SIMPLE_FORM_UPDATE;
		mg_form_reset (MG_FORM (form->priv->basic_form));
		break;
	case 'i':
		form->priv->basic_form_mode = SIMPLE_FORM_INSERT;
		break;
	case 'd':
		query = form->priv->core->query_delete;
		form->priv->basic_form_mode = SIMPLE_FORM_UPDATE;
		break;
	default:
		g_assert_not_reached ();
	}

	if (query) {
		gchar *sql = NULL;
		GError *error = NULL;
		MgQueryType qtype;
		gchar *confirm = NULL;
		gboolean do_execute = TRUE;
		GtkWidget *parent;
				
		/* find the top level window of the form */
		parent = gtk_widget_get_parent (GTK_WIDGET (form));
		while (parent && !GTK_IS_WINDOW (parent)) 
			parent = gtk_widget_get_parent (parent);


		sql = mg_renderer_render_as_sql (MG_RENDERER (query), form->priv->core->work_context, &error);
		qtype = mg_query_get_query_type (query);

		switch (qtype) {
		case MG_QUERY_TYPE_INSERT:
			if (form->priv->mode & MG_WORK_FORM_ASK_CONFIRM_INSERT)
				confirm = _("Execute the following insertion query ?\n\n%s");
			break;
		case MG_QUERY_TYPE_UPDATE:
			if (form->priv->mode & MG_WORK_FORM_ASK_CONFIRM_UPDATE)
				confirm = _("Execute the following update query ?\n\n%s");
			break;
		case MG_QUERY_TYPE_DELETE:
			if (form->priv->mode & MG_WORK_FORM_ASK_CONFIRM_DELETE)
				confirm = _("Execute the following deletion query ?\n\n%s");
			break;
		default:
			g_assert_not_reached ();
		}

		if (sql) {
			if (confirm) {
				GtkWidget *dlg;
				gint result;
				
				dlg = gtk_message_dialog_new (GTK_WINDOW (parent), 0,
							      GTK_MESSAGE_QUESTION,
							      GTK_BUTTONS_YES_NO,
							      confirm, sql);
				result = gtk_dialog_run (GTK_DIALOG (dlg));
				gtk_widget_destroy (dlg);
				do_execute = (result == GTK_RESPONSE_YES);
			}
			
			if (do_execute) {
				MgConf *conf = mg_base_get_conf (MG_BASE (form->priv->core->query_select));
			
#ifdef debug
				g_print ("SQL: %s\n", sql);
#endif
				mg_server_do_query (mg_conf_get_server (conf), sql, MG_SERVER_QUERY_SQL, &error);
				if (error) {
					GtkWidget *dlg;
					gchar *message;

					message = g_strdup (error->message);
					g_error_free (error);
					dlg = gtk_message_dialog_new (GTK_WINDOW (parent), 0,
								      GTK_MESSAGE_ERROR,
								      GTK_BUTTONS_CLOSE,
								      message);
					g_free (message);
					gtk_dialog_run (GTK_DIALOG (dlg));
					gtk_widget_destroy (dlg);
				}
				else
					arg_param_changed_cb (NULL, form); /* reload the form's data */
			}
			else
				form->priv->basic_form_mode = initial_mode;

			g_free (sql);
		}
		else {
			GtkWidget *dlg;
			gchar *message;

			if (error) {
				message = g_strdup_printf (_("The following error occured while preparing the query:\n%s"),
							   error->message);
				g_error_free (error);
			}
			else
				message = g_strdup_printf (_("An unknown error occured while preparing the query."));

			dlg = gtk_message_dialog_new (GTK_WINDOW (parent), 0,
						      GTK_MESSAGE_ERROR,
						      GTK_BUTTONS_CLOSE,
						      message);
			g_free (message);
			gtk_dialog_run (GTK_DIALOG (dlg));
			gtk_widget_destroy (dlg);
			form->priv->basic_form_mode = initial_mode;
		}
	}

	/* updates */
	if (form->priv->basic_form_mode != initial_mode)
		update_simple_form (form);
	actions_arrows_update (form);
	modif_buttons_update (form);
}


/**
 * mg_work_form_get_param_for_field
 * @form: a #MgWorkForm widget
 * @field: a #MgQfield object
 *
 * Get a #MgParameter which will be used by @field in @form.
 * @field must belong to the SELECT query given as argument to mg_work_form_new().
 *
 * Returns: a #MgParameter, or %NULL if not found
 */
MgParameter *
mg_work_form_get_param_for_field (MgWorkForm *form, MgQfield *field)
{
	MgParameter *param = NULL;

	g_return_val_if_fail (form && IS_MG_WORK_FORM (form), NULL);
	g_return_val_if_fail (form->priv, NULL);
	g_return_val_if_fail (field && IS_MG_QFIELD (field), NULL);
	
	if (form->priv->core->query_select_repl) {
		MgQfield *sel_query_field;
		sel_query_field = g_hash_table_lookup (form->priv->core->query_select_repl, field);
		if (sel_query_field) 
			param = mg_context_find_parameter_for_field (form->priv->core->args_context, 
								     sel_query_field);
	}
	
	return param;
}
