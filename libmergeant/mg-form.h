/* mg-form.h
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __MG_FORM__
#define __MG_FORM__

#include <gtk/gtk.h>
#include "mg-conf.h"

G_BEGIN_DECLS

#define MG_FORM_TYPE          (mg_form_get_type())
#define MG_FORM(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_form_get_type(), MgForm)
#define MG_FORM_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_form_get_type (), MgFormClass)
#define IS_MG_FORM(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_form_get_type ())


typedef struct _MgForm      MgForm;
typedef struct _MgFormClass MgFormClass;
typedef struct _MgFormPriv  MgFormPriv;

/* struct for the object's data */
struct _MgForm
{
	GtkVBox             object;

	MgFormPriv *priv;
};

/* struct for the object's class */
struct _MgFormClass
{
	GtkVBoxClass parent_class;

	/* signals */
        void       (*param_changed)         (MgForm *form, MgParameter *param);
};

/* 
 * Generic widget's methods 
*/
guint      mg_form_get_type                 (void);
GtkWidget *mg_form_new                      (MgConf *conf, MgContext *context);
GtkWidget *mg_form_new_in_dialog            (MgConf *conf, MgContext *context, GtkWindow *parent,
			 		     const gchar *title, const gchar *header);
gboolean   mg_form_is_valid                 (MgForm *form);
gboolean   mg_form_has_been_changed         (MgForm *form);
void       mg_form_reset                    (MgForm *form);

void       mg_form_show_entries_actions     (MgForm *form, gboolean show_actions);
void       mg_form_show_param_entry         (MgForm *form, MgParameter *param, gboolean show);
void       mg_form_set_entries_auto_default (MgForm *form, gboolean auto_default);
void       mg_form_set_entries_default      (MgForm *form);

G_END_DECLS

#endif



