/* mg-form.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <string.h>
#ifdef has_libgnomedb
#include <libgnomedb/libgnomedb.h>
#endif
#include "mg-form.h"
#include "mg-server.h"
#include "marshal.h"
#include "mg-renderer.h"
#include "mg-parameter.h"
#include "mg-data-handler.h"
#include "mg-data-entry.h"
#include "mg-server-data-type.h"
#include "mg-query.h"
#include "mg-field.h"
#include "mg-context.h"
#include <libmergeant/handlers/mg-entry-combo.h>

static void mg_form_class_init (MgFormClass * class);
static void mg_form_init (MgForm * wid);
static void mg_form_dispose (GObject   * object);

static void entry_contents_modified (MgDataEntry *entry, MgForm *form);
static void parameter_changed_cb (MgParameter *param, MgDataEntry *entry);

enum
{
	PARAM_CHANGED,
	LAST_SIGNAL
};


struct _MgFormPriv
{
	MgConf            *conf;
	MgContext         *context;/* parameters, etc */
	GSList            *entries;/* list of MgDataEntry widgets */
	GtkWidget         *entries_table;
	GSList            *hidden_entries;

	gboolean           forward_param_updates; /* forward them to the MgDataEntry widgets ? */
	GtkTooltips       *tooltips;
};


static gint mg_form_signals[LAST_SIGNAL] = { 0 };

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

guint
mg_form_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgFormClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_form_class_init,
			NULL,
			NULL,
			sizeof (MgForm),
			0,
			(GInstanceInitFunc) mg_form_init
		};		
		
		type = g_type_register_static (GTK_TYPE_VBOX, "MgForm", &info, 0);
	}

	return type;
}

static void
mg_form_class_init (MgFormClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	mg_form_signals[PARAM_CHANGED] =
		g_signal_new ("param_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgFormClass, param_changed),
			      NULL, NULL,
			      marshal_VOID__OBJECT, G_TYPE_NONE, 1,
			      G_TYPE_OBJECT);

	class->param_changed = NULL;
	object_class->dispose = mg_form_dispose;
}

static void
mg_form_init (MgForm * wid)
{
	wid->priv = g_new0 (MgFormPriv, 1);
	wid->priv->conf = NULL;
	wid->priv->context = NULL;
	wid->priv->entries = NULL;
	wid->priv->entries_table = NULL;
	wid->priv->hidden_entries = NULL;

	wid->priv->forward_param_updates = TRUE;
	wid->priv->tooltips = NULL;
}

static void mg_form_initialize (MgForm *form, MgConf *conf);
static void widget_shown_cb (GtkWidget *wid, MgForm *form);
static void mg_conf_weak_notify (MgForm *form, MgConf *conf);
/**
 * mg_form_new
 * @conf: a #MgConf object
 * @context: a #MgContext structure
 *
 * Creates a new #MgForm widget using all the parameters provided in @context.
 * @context is copied in the process.
 *
 * Returns: the new widget
 */
GtkWidget *
mg_form_new (MgConf *conf, MgContext *context)
{
	GObject *obj;
	MgForm *form;

	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);
	if (context) 
		g_return_val_if_fail (mg_context_is_coherent (context, NULL), NULL);

	obj = g_object_new (MG_FORM_TYPE, NULL);
	form = MG_FORM (obj);

	form->priv->conf = conf;
	
	/* context copy */
	if (context)
		form->priv->context = MG_CONTEXT (mg_context_new_copy (context));
	else
		form->priv->context = MG_CONTEXT (mg_context_new (conf, NULL));

	g_object_weak_ref (G_OBJECT (form->priv->conf),
			   (GWeakNotify) mg_conf_weak_notify, form);

	mg_form_initialize (form, conf);

	return GTK_WIDGET (obj);
}

static void
widget_shown_cb (GtkWidget *wid, MgForm *form)
{
	if (g_slist_find (form->priv->hidden_entries, wid)) {
		if (g_slist_find (form->priv->entries, wid)) {
			gint row = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (wid), "row_no"));
			gtk_table_set_row_spacing (GTK_TABLE (form->priv->entries_table), row, 0);
		}
		
		gtk_widget_hide (wid);
	}
}

static void
mg_conf_weak_notify (MgForm *form, MgConf *conf)
{
	GSList *list;

	/* render all the entries non sensitive */
	list = form->priv->entries;
	while (list) {
		gtk_widget_set_sensitive (GTK_WIDGET (list->data), FALSE);
		list = g_slist_next (list);
	}

	/* Tell that we don't need to weak unref the MgConf */
	form->priv->conf = NULL;
}

static void
mg_form_dispose (GObject *object)
{
	MgForm *form;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_FORM (object));
	form = MG_FORM (object);

	if (form->priv) {
		GSList *list;

		/* context */
		if (form->priv->context) {
			list = form->priv->context->parameters;
			while (list) {
				g_signal_handlers_disconnect_by_func (G_OBJECT (list->data),
								      G_CALLBACK (parameter_changed_cb), form);
				list = g_slist_next (list);
			}

			g_object_unref (G_OBJECT (form->priv->context));
			form->priv->context = NULL;
		}

		/* list of entries */
		list = form->priv->hidden_entries;
		if (list) {
			while (list) { 
				g_signal_handlers_disconnect_by_func (G_OBJECT (list->data), 
								      G_CALLBACK (widget_shown_cb), form);
				list = g_slist_next (list);
			}
			g_slist_free (form->priv->hidden_entries);
			form->priv->hidden_entries = NULL;
		}

		list = form->priv->entries;
		if (list) {
			while (list) { 
				g_object_set_data (G_OBJECT (list->data), "param", NULL);
				list = g_slist_next (list);
			}
			g_slist_free (form->priv->entries);
			form->priv->entries = NULL;
		}

		/* Weak unref the MgConf is necessary */
		if (form->priv->conf)
			g_object_weak_unref (G_OBJECT (form->priv->conf),
					     (GWeakNotify) mg_conf_weak_notify, form);
		
		/* tooltips */
		if (form->priv->tooltips) {
			gtk_object_destroy (GTK_OBJECT (form->priv->tooltips));
			form->priv->tooltips = NULL;
		}


		/* the private area itself */
		g_free (form->priv);
		form->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}



/*
 * create the entries in the widget
 */
static void 
mg_form_initialize (MgForm *form, MgConf *conf)
{
	GSList *list;
	MgParameter *param;
	MgContextNode *node;
	GtkWidget *table, *label;
	gint i;
	
	/* tooltips */
	form->priv->tooltips = gtk_tooltips_new ();

	/* context management */
	if (form->priv->context && !form->priv->context->nodes) {
		MgContext *ncontext = MG_CONTEXT (mg_context_new (conf, form->priv->context->parameters));
		g_object_unref (G_OBJECT (form->priv->context));
		form->priv->context = ncontext;
	}

	/* creating all the entries */
	list = form->priv->context->nodes;
	while (list) {
		node = MG_CONTEXT_NODE (list->data);
		if ((param = node->param)) { /* single direct parameter */
			MgServerDataType *type;
			GtkWidget *entry;
			const GdaValue *val, *default_val, *value;
			gboolean nnul;
			MgDataHandler *dh = NULL;
			gchar *plugin = NULL;

			type = mg_parameter_get_data_type (param);
			g_assert (type);

			g_object_get (G_OBJECT (param), "handler_plugin", &plugin, NULL);
			if (plugin) {
				dh = mg_server_get_handler_by_name (mg_conf_get_server (conf), plugin);

				/* test if plugin can handle the parameter's data type */
				if (!mg_data_handler_accepts_gda_type (dh, mg_server_data_type_get_gda_type (type)))
					dh = NULL;
			}
			else
				dh = mg_server_data_type_get_handler (type);
			val = mg_parameter_get_value (param);
			default_val = mg_parameter_get_default_value (param);
			nnul = mg_parameter_get_not_null (param);

			/* determine initial value */
			value = val;
			if (!value && default_val && 
			    (gda_value_get_type (default_val) == mg_server_data_type_get_gda_type (type)))
				value = default_val;
			
			entry = GTK_WIDGET (mg_data_handler_get_entry_from_value 
					    (dh, value,
					     mg_server_data_type_get_gda_type (type)));
			if (!nnul ||
			    (nnul && value && (gda_value_get_type (value) != GDA_VALUE_TYPE_NULL)))
				mg_data_entry_set_value_orig (MG_DATA_ENTRY (entry), value);
			
			if (default_val) {
				mg_data_entry_set_value_default (MG_DATA_ENTRY (entry), default_val);
				mg_data_entry_set_attributes (MG_DATA_ENTRY (entry),
							      MG_DATA_ENTRY_CAN_BE_DEFAULT,
							      MG_DATA_ENTRY_CAN_BE_DEFAULT);
			}

			mg_data_entry_set_attributes (MG_DATA_ENTRY (entry),
						      nnul ? 0 : MG_DATA_ENTRY_CAN_BE_NULL,
						      MG_DATA_ENTRY_CAN_BE_NULL);
			    
			g_object_set_data (G_OBJECT (entry), "param", param);
			g_object_set_data (G_OBJECT (entry), "form", form);
			form->priv->entries = g_slist_append (form->priv->entries, entry);

			/* connect the changes from the parameter */
			g_signal_connect (G_OBJECT (param), "changed",
					  G_CALLBACK (parameter_changed_cb), entry);

		}
		else { /* parameter depending on a sub query */
			GtkWidget *entry;
			GSList *plist;
			gboolean nnul = TRUE;

			entry = mg_entry_combo_new (conf, form->priv->context, node);
			g_object_set_data (G_OBJECT (entry), "node", node);
			g_object_set_data (G_OBJECT (entry), "form", form);
			form->priv->entries = g_slist_append (form->priv->entries, entry);

			/* connect the changes from the parameter */
			plist = node->params;
			while (plist) {
				if (!mg_parameter_get_not_null (plist->data))
					nnul = FALSE;
				g_signal_connect (G_OBJECT (plist->data), "changed",
						  G_CALLBACK (parameter_changed_cb), entry);
				plist = g_slist_next (plist);
			}
			mg_data_entry_set_attributes (MG_DATA_ENTRY (entry),
						      nnul ? 0 : MG_DATA_ENTRY_CAN_BE_NULL,
						      MG_DATA_ENTRY_CAN_BE_NULL);
		}
		list = g_slist_next (list);
	}

	/* creating a table for all the entries */
	table = gtk_table_new (g_slist_length (form->priv->entries), 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), 5);
	gtk_table_set_col_spacings (GTK_TABLE (table), 5);
	form->priv->entries_table = table;
	gtk_box_pack_start (GTK_BOX (form), table,  TRUE, TRUE, 0);
	list = form->priv->entries;
	i = 0;
	while (list) {
		gboolean expand;
		GtkWidget *entry_label;

		/* label for the entry */
		param = g_object_get_data (G_OBJECT (list->data), "param");
		if (param) {
			const gchar *str;
			gchar *str2;
			GtkWidget *evbox;

			str = mg_base_get_name (MG_BASE (param));
			if (!str)
				str = _("Value");
			str2 = g_strdup_printf ("%s:", str);
			evbox = gtk_event_box_new ();
			label = gtk_label_new (str2);
			gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
			g_free (str2);
			gtk_container_add (GTK_CONTAINER (evbox), label);

			gtk_table_attach (GTK_TABLE (table), evbox, 0, 1, i, i+1,
					  GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 0, 0);
			gtk_widget_show (evbox);
			gtk_widget_show (label);
			entry_label = evbox;

			str = mg_base_get_description (MG_BASE (param));
			if (str && *str)
				gtk_tooltips_set_tip (form->priv->tooltips, evbox, str, NULL);
		}
		else {
			/* FIXME: find a better label and tooltip and improve data entry attributes */
			const gchar *str = NULL;
			gchar *str2;
			GtkWidget *evbox;

			node = g_object_get_data (G_OBJECT (list->data), "node");
			str = mg_base_get_name (MG_BASE (node->query));
			if (!str)
				str = _("Value");
			str2 = g_strdup_printf ("%s:", str);
			evbox = gtk_event_box_new ();
			label = gtk_label_new (str2);
			gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
			g_free (str2);
			gtk_container_add (GTK_CONTAINER (evbox), label);

			gtk_table_attach (GTK_TABLE (table), evbox, 0, 1, i, i+1,
					  GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 0, 0);
			gtk_widget_show (evbox);
			gtk_widget_show (label);
			entry_label = evbox;

			str = mg_base_get_description (MG_BASE (node->query));
			if (str && *str)
				gtk_tooltips_set_tip (form->priv->tooltips, evbox, str, NULL);
			
		}

		/* add the entry itself to the table */
		expand = mg_data_entry_expand_in_layout (MG_DATA_ENTRY (list->data));
		gtk_table_attach (GTK_TABLE (table), GTK_WIDGET (list->data), 1, 2, i, i+1,
				  GTK_FILL | GTK_SHRINK | GTK_EXPAND, 
				  GTK_FILL | GTK_SHRINK | expand? GTK_EXPAND : 0, 0, 0);
		gtk_widget_show (GTK_WIDGET (list->data));
		g_object_set_data (G_OBJECT (list->data), "entry_label", entry_label);
		g_object_set_data (G_OBJECT (list->data), "row_no", GINT_TO_POINTER (i));

		/* signals connecting */
		g_signal_connect (G_OBJECT (list->data), "contents_modified",
				  G_CALLBACK (entry_contents_modified), form);
		
		list = g_slist_next (list);
		i++;
	}
}

static void
entry_contents_modified (MgDataEntry *entry, MgForm *form)
{
	MgParameter *param;
	MgContextNode *node;
	guint attr;

	attr = mg_data_entry_get_attributes (entry);
	param = g_object_get_data (G_OBJECT (entry), "param");
	if (param) { /* single parameter */
		GdaValue *value;
		
		form->priv->forward_param_updates = FALSE;

		/* parameter's value */
		value = mg_data_entry_get_value (entry);
		if ((!value || gda_value_is_null (value)) &&
		    (attr & MG_DATA_ENTRY_IS_DEFAULT)) {
			g_object_set (G_OBJECT (param), "use_default_value", TRUE, NULL);
		}
		else
			g_object_set (G_OBJECT (param), "use_default_value", FALSE, NULL);
		mg_parameter_set_value (param, value);

		form->priv->forward_param_updates = TRUE;
		gda_value_free (value);
	}
	else { /* multiple parameters */
		GSList *values, *params, *list;
		node = g_object_get_data (G_OBJECT (entry), "node");
		params = node->params;
		values = mg_entry_combo_get_values (MG_ENTRY_COMBO (entry));
		g_assert (g_slist_length (params) == g_slist_length (values));

		list = values;
		while (list) {
			form->priv->forward_param_updates = FALSE;

			/* parameter's value */
			mg_parameter_set_value (MG_PARAMETER (params->data), (GdaValue *)(list->data));
			form->priv->forward_param_updates = TRUE;;
			gda_value_free (list->data);

			list = g_slist_next (list);
			params = g_slist_next (params);
		}
		g_slist_free (values);
	}
}

static void
parameter_changed_cb (MgParameter *param, MgDataEntry *entry)
{
	MgForm *form = g_object_get_data (G_OBJECT (entry), "form");
	MgContextNode *node = g_object_get_data (G_OBJECT (entry), "node");
	const GdaValue *value = mg_parameter_get_value (param);

	if (form->priv->forward_param_updates) {
		gboolean param_valid;
		gboolean default_if_invalid = FALSE;

		/* There can be a feedback from the entry if the param is invalid and "set_default_if_invalid"
		   exists and is TRUE */
		param_valid = mg_parameter_is_valid (param);
		if (!param_valid) 
			if (g_object_class_find_property (G_OBJECT_GET_CLASS (entry), "set_default_if_invalid"))
				g_object_get (G_OBJECT (entry), "set_default_if_invalid", &default_if_invalid, NULL);

		/* updating the corresponding entry */
		if (! default_if_invalid)
			g_signal_handlers_block_by_func (G_OBJECT (entry),
							 G_CALLBACK (entry_contents_modified), form);
		if (node) {
			GSList *values = NULL;
			GSList *list = node->params;
			gboolean allnull = TRUE;

			while (list) {
				const GdaValue *pvalue = mg_parameter_get_value (MG_PARAMETER (list->data));
				values = g_slist_append (values, pvalue);
				if (allnull && pvalue && (gda_value_get_type (pvalue) != GDA_VALUE_TYPE_NULL))
					allnull = FALSE;

				list = g_slist_next (list);
			}
			
			if (!allnull) 
				mg_entry_combo_set_values_orig (MG_ENTRY_COMBO (entry), values);
			else 
				mg_entry_combo_set_values_orig (MG_ENTRY_COMBO (entry), NULL);

			g_slist_free (values);
		}
		else
			mg_data_entry_set_value_orig (entry, value);

		if (! default_if_invalid)
			g_signal_handlers_unblock_by_func (G_OBJECT (entry),
							   G_CALLBACK (entry_contents_modified), form);
	}

#ifdef debug_signal
	g_print (">> 'PARAM_CHANGED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (form), mg_form_signals[PARAM_CHANGED], 0, param);
#ifdef debug_signal
	g_print ("<< 'PARAM_CHANGED' from %s\n", __FUNCTION__);
#endif

}

/**
 * mg_form_is_valid 
 * @form: a #MgForm widget
 *
 * Tells if the form can be used as-is (if all the parameters do have some valid values)
 *
 * Returns: TRUE if the form is valid
 */
gboolean
mg_form_is_valid (MgForm *form)
{
	g_return_val_if_fail (form && IS_MG_FORM (form), FALSE);
	g_return_val_if_fail (form->priv, FALSE);

	return mg_context_is_valid (form->priv->context);
}

/**
 * mg_form_has_been_changed
 * @form: a #MgForm widget
 *
 * Tells if the form has had at least on entry changed, or not
 *
 * Returns:
 */
gboolean
mg_form_has_been_changed (MgForm *form)
{
	gboolean changed = FALSE;
	GSList *list;

	g_return_val_if_fail (form && IS_MG_FORM (form), FALSE);
	g_return_val_if_fail (form->priv, FALSE);
	
	list = form->priv->entries;
	while (list && !changed) {
		if (! (mg_data_entry_get_attributes (MG_DATA_ENTRY (list->data)) & MG_DATA_ENTRY_IS_UNCHANGED))
			changed = TRUE;
		list = g_slist_next (list);
	}

	return changed;
}

/**
 * mg_form_show_entries_actions
 * @form: a #MgForm widget
 *
 * Show or hide the actions button available at the end of each data entry
 * in the form
 */
void
mg_form_show_entries_actions (MgForm *form, gboolean show_actions)
{
	GSList *entries;
	guint show;
	
	g_return_if_fail (form && IS_MG_FORM (form));
	g_return_if_fail (form->priv);

	show = show_actions ? MG_DATA_ENTRY_ACTIONS_SHOWN : 0;

	entries = form->priv->entries;
	while (entries) {
		mg_data_entry_set_attributes (MG_DATA_ENTRY (entries->data), show, MG_DATA_ENTRY_ACTIONS_SHOWN);
		entries = g_slist_next (entries);
	}
}

/**
 * mg_form_reset
 * @form: a #MgForm widget
 *
 * Resets all the entries in the form to their
 * original values
 */
void
mg_form_reset (MgForm *form)
{
	GSList *list;
	const GdaValue *value;

	g_return_if_fail (form && IS_MG_FORM (form));
	g_return_if_fail (form->priv);
	
	list = form->priv->entries;
	while (list) {
		value = mg_data_entry_get_value_orig (MG_DATA_ENTRY (list->data));
		mg_data_entry_set_value (MG_DATA_ENTRY (list->data), value);
		list = g_slist_next (list);
	}
}


/**
 * mg_form_show_param_entry
 * @form: a #MgForm widget
 * @param: a #MgParameter object
 * @show:
 *
 * Shows or hides the #MgDataEntry in @form which corresponds to the
 * @param parameter
 */
void
mg_form_show_param_entry (MgForm *form, MgParameter *param, gboolean show)
{
	GSList *entries;

	g_return_if_fail (form && IS_MG_FORM (form));
	g_return_if_fail (form->priv);

	entries = form->priv->entries;
	while (entries) {
		GtkWidget *entry = NULL;
		MgParameter *thisparam = g_object_get_data (G_OBJECT (entries->data), "param");

		if (thisparam) {
			if (thisparam == param)
				entry = GTK_WIDGET (entries->data);
		}
		else {
			/* multiple parameters */
			GSList *params;
			MgContextNode *node;

			node = g_object_get_data (G_OBJECT (entries->data), "node");
			params = node->params;
			while (params && !entry) {
				if (params->data == (gpointer) param)
					entry = GTK_WIDGET (entries->data);
				params = g_slist_next (params);
			}
		}

		if (entry) {
			gint row = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (entry), "row_no"));
			GtkWidget *entry_label = g_object_get_data (G_OBJECT (entry), "entry_label");
			if (show) {
				if (g_slist_find (form->priv->hidden_entries, entry)) {
					form->priv->hidden_entries = g_slist_remove (form->priv->hidden_entries, entry);
					g_signal_handlers_disconnect_by_func (G_OBJECT (entry), 
									      G_CALLBACK (widget_shown_cb), form);
				}
				if (g_slist_find (form->priv->hidden_entries, entry_label)) {
					form->priv->hidden_entries = g_slist_remove (form->priv->hidden_entries, entry_label);
					g_signal_handlers_disconnect_by_func (G_OBJECT (entry_label), 
									      G_CALLBACK (widget_shown_cb), form);
				}
				gtk_widget_show (entry);
				gtk_widget_show (entry_label);
				gtk_table_set_row_spacing (GTK_TABLE (form->priv->entries_table), row, 5);
			}
			else {
				if (!g_slist_find (form->priv->hidden_entries, entry)) {
					form->priv->hidden_entries = g_slist_append (form->priv->hidden_entries, entry);
					g_signal_connect_after (G_OBJECT (entry), "show", 
								G_CALLBACK (widget_shown_cb), form);
				}
				if (!g_slist_find (form->priv->hidden_entries, entry_label)) {
					form->priv->hidden_entries = g_slist_append (form->priv->hidden_entries, entry_label);
					g_signal_connect_after (G_OBJECT (entry_label), "show", 
								G_CALLBACK (widget_shown_cb), form);
				}
				
				gtk_widget_hide (entry);
				gtk_widget_hide (entry_label);
				gtk_table_set_row_spacing (GTK_TABLE (form->priv->entries_table), row, 0);
			}
		}

		entries = g_slist_next (entries);
	}
}

/**
 * mg_form_set_entries_auto_default
 * @form: a #MgForm widget
 * @auto_default:
 *
 * Sets weather all the #MgDataEntry entries in the form must default
 * to a default value if they are assigned a non valid value.
 * Depending on the real type of entry, it will provide a default value
 * which the user does not need to modify if it is OK.
 *
 * For example a date entry can by default display the current date).
 */
void
mg_form_set_entries_auto_default (MgForm *form, gboolean auto_default)
{
	GSList *entries;

	g_return_if_fail (form && IS_MG_FORM (form));
	g_return_if_fail (form->priv);

	entries = form->priv->entries;
	while (entries) {
		if (g_object_class_find_property (G_OBJECT_GET_CLASS (entries->data), "set_default_if_invalid"))
			g_object_set (G_OBJECT (entries->data), "set_default_if_invalid", auto_default, NULL);
		entries = g_slist_next (entries);
	}	
}

/**
 * mg_form_set_entries_default
 * @form: a #MgForm widget
 * @auto_default:
 *
 * For each entry in the form, sets it to a default value if it is possible to do so.
 */
void
mg_form_set_entries_default (MgForm *form)
{
	GSList *entries;
	guint attrs;

	g_return_if_fail (form && IS_MG_FORM (form));
	g_return_if_fail (form->priv);

	entries = form->priv->entries;
	while (entries) {
		attrs = mg_data_entry_get_attributes (MG_DATA_ENTRY (entries->data));
		if (attrs & MG_DATA_ENTRY_CAN_BE_DEFAULT)
			mg_data_entry_set_attributes (MG_DATA_ENTRY (entries->data), 
						      MG_DATA_ENTRY_IS_DEFAULT, MG_DATA_ENTRY_IS_DEFAULT);
		entries = g_slist_next (entries);
	}
}

static void form_param_changed (MgForm *form, MgParameter *param, GtkDialog *dlg);

/**
 * mg_form_new_in_dialog
 * @conf: a #MgConf object
 * @context: a #MgContext structure
 * @parent: the parent window for the new dialog, or %NULL
 * @title: the title of the dialog window, or %NULL
 * @header: a helper text displayed at the top of the dialog, or %NULL
 *
 * Creates a new #MgForm widget in the same way as mg_form_new()
 * and puts it into a #GtkDialog widget.
 *
 * The #MgForm widget is attached to the dialog using the user property
 * "form".
 *
 * Returns: the new #GtkDialog widget
 */
GtkWidget *
mg_form_new_in_dialog (MgConf *conf, MgContext *context, GtkWindow *parent,
		       const gchar *title, const gchar *header)
{
	GtkWidget *form;
	GtkWidget *dlg;
	const gchar *rtitle;

	form = mg_form_new (conf, context);
 
	rtitle = title;
	if (!rtitle)
		rtitle = _("Required parameters");
		
	dlg = gtk_dialog_new_with_buttons (rtitle, parent,
					   GTK_DIALOG_MODAL,
					   GTK_STOCK_OK,
					   GTK_RESPONSE_ACCEPT,
					   GTK_STOCK_CANCEL,
					   GTK_RESPONSE_REJECT,
					   NULL);

	if (header && *header) {
		gchar *str;
		GtkWidget *label;

		label = gtk_label_new (NULL);
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
		str = g_strdup_printf ("<b>%s</b>", header);
		gtk_label_set_markup (GTK_LABEL (label), str);
		g_free (str);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), label, FALSE, FALSE, 5);
		gtk_widget_show (label);
	}

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), form, TRUE, TRUE, 5);

	g_signal_connect (G_OBJECT (form), "param_changed",
			  G_CALLBACK (form_param_changed), dlg);
	g_object_set_data (G_OBJECT (dlg), "form", form);

	gtk_widget_show_all (form);
	form_param_changed (MG_FORM (form), NULL, GTK_DIALOG (dlg));

	return dlg;
}

static void
form_param_changed (MgForm *form, MgParameter *param, GtkDialog *dlg)
{
	gboolean valid;

	valid = mg_form_is_valid (form);

	gtk_dialog_set_response_sensitive (dlg, GTK_RESPONSE_ACCEPT, valid);
}
