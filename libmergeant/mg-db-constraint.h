/* mg-db-constraint.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_DB_CONSTRAINT_H_
#define __MG_DB_CONSTRAINT_H_

#include "mg-base.h"
#include "mg-ref-base.h"
#include "mg-defs.h"

G_BEGIN_DECLS

#define MG_DB_CONSTRAINT_TYPE          (mg_db_constraint_get_type())
#define MG_DB_CONSTRAINT(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_db_constraint_get_type(), MgDbConstraint)
#define MG_DB_CONSTRAINT_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_db_constraint_get_type (), MgDbConstraintClass)
#define IS_MG_DB_CONSTRAINT(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_db_constraint_get_type ())


/* error reporting */
extern GQuark mg_db_constraint_error_quark (void);
#define MG_DB_CONSTRAINT_ERROR mg_db_constraint_error_quark ()

enum
{
	MG_DB_CONSTRAINT_XML_SAVE_ERROR,
	MG_DB_CONSTRAINT_XML_LOAD_ERROR
};

typedef enum
{
	CONSTRAINT_PRIMARY_KEY,
	CONSTRAINT_FOREIGN_KEY,
	CONSTRAINT_UNIQUE,
	CONSTRAINT_NOT_NULL,
	CONSTRAINT_CHECK_EXPR,
	CONSTRAINT_UNKNOWN
} MgDbConstraintType;

typedef enum
{
	CONSTRAINT_FK_ACTION_CASCADE,
	CONSTRAINT_FK_ACTION_SET_NULL,
	CONSTRAINT_FK_ACTION_SET_DEFAULT,
	CONSTRAINT_FK_ACTION_SET_VALUE,
	CONSTRAINT_FK_ACTION_NO_ACTION
} MgDbConstraintFkAction;

typedef struct
{
	MgDbField *fkey;
	MgDbField *ref_pkey;
	MgRefBase *ref_pkey_repl; /* can be used instead of ref_pkey, the object will fill ref_pkey itself */
} MgDbConstraintFkeyPair;

#define MG_DB_CONSTRAINT_FK_PAIR(x) ((MgDbConstraintFkeyPair*) x)

/* struct for the object's data */
struct _MgDbConstraint
{
	MgBase                  object;
	MgDbConstraintPrivate  *priv;
};

/* struct for the object's class */
struct _MgDbConstraintClass
{
	MgBaseClass                    class;

	/* signals */
	void   (*templ_signal)        (MgDbConstraint *obj);
};

guint              mg_db_constraint_get_type            (void);
GObject           *mg_db_constraint_new                 (MgDbTable *table, MgDbConstraintType type);
GObject           *mg_db_constraint_new_with_db         (MgDatabase *db);
MgDbConstraintType mg_db_constraint_get_constraint_type (MgDbConstraint *cstr);
gboolean           mg_db_constraint_equal               (MgDbConstraint *cstr1, MgDbConstraint *cstr2);
MgDbTable         *mg_db_constraint_get_table           (MgDbConstraint *cstr);
gboolean           mg_db_constraint_uses_field          (MgDbConstraint *cstr, MgDbField *field);

/* Primary KEY specific */
void               mg_db_constraint_pkey_set_fields     (MgDbConstraint *cstr, const GSList *fields);
GSList            *mg_db_constraint_pkey_get_fields     (MgDbConstraint *cstr);

/* Foreign KEY specific */
void               mg_db_constraint_fkey_set_fields     (MgDbConstraint *cstr, const GSList *pairs);
MgDbTable         *mg_db_constraint_fkey_get_ref_table  (MgDbConstraint *cstr);
GSList            *mg_db_constraint_fkey_get_fields     (MgDbConstraint *cstr);
void               mg_db_constraint_fkey_set_actions    (MgDbConstraint *cstr, 
							 MgDbConstraintFkAction on_update, 
							 MgDbConstraintFkAction on_delete);
void               mg_db_constraint_fkey_get_actions    (MgDbConstraint *cstr, 
							 MgDbConstraintFkAction *on_update, 
							 MgDbConstraintFkAction *on_delete);

/* UNIQUE specific */
void               mg_db_constraint_unique_set_fields   (MgDbConstraint *cstr, const GSList *fields);
GSList            *mg_db_constraint_unique_get_fields   (MgDbConstraint *cstr);

/* NOT NULL specific */
void               mg_db_constraint_not_null_set_field  (MgDbConstraint *cstr, MgDbField *field);
MgDbField         *mg_db_constraint_not_null_get_field  (MgDbConstraint *cstr);

/* Check specific */
/* TODO */

G_END_DECLS

#endif
