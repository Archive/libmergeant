/* mg-server-function.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-server.h"
#include "mg-server-function.h"
#include "mg-server-data-type.h"
#include "mg-xml-storage.h"
#include "mg-data-handler.h"
#include "marshal.h"
#include <libgda/libgda.h>
#include <string.h>

/* 
 * Main static functions 
 */
static void mg_server_function_class_init (MgServerFunctionClass * class);
static void mg_server_function_init (MgServerFunction * srv);
static void mg_server_function_dispose (GObject   * object);
static void mg_server_function_finalize (GObject   * object);

static void mg_server_function_set_property (GObject              *object,
					     guint                 param_id,
					     const GValue         *value,
					     GParamSpec           *pspec);
static void mg_server_function_get_property (GObject              *object,
					     guint                 param_id,
					     GValue               *value,
					     GParamSpec           *pspec);

#ifdef debug
static void mg_server_function_dump (MgServerFunction *func, guint offset);
#endif

static void        mg_function_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_function_get_xml_id (MgXmlStorage *iface);
static xmlNodePtr  mg_function_save_to_xml (MgXmlStorage *iface, GError **error);
static gboolean    mg_function_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	TEMPL_SIGNAL,
	LAST_SIGNAL
};

static gint mg_server_function_signals[LAST_SIGNAL] = { 0 };

/* properties */
enum
{
	PROP_0,
	PROP
};


/* private structure */
struct _MgServerFunctionPrivate
{
	MgServer         *srv;
	gchar            *objectid;       /* unique id for the function */
	MgServerDataType *result_type;
	GSList           *arg_types;           /* list of MgServerDataType pointers */
};


/* module error */
GQuark mg_server_function_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_server_function_error");
	return quark;
}


guint
mg_server_function_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgServerFunctionClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_server_function_class_init,
			NULL,
			NULL,
			sizeof (MgServerFunction),
			0,
			(GInstanceInitFunc) mg_server_function_init
		};

		static const GInterfaceInfo xml_storage_info = {
			(GInterfaceInitFunc) mg_function_xml_storage_init,
			NULL,
			NULL
		};
		
		type = g_type_register_static (MG_BASE_TYPE, "MgServerFunction", &info, 0);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
	}
	return type;
}

static void 
mg_function_xml_storage_init (MgXmlStorageIface *iface)
{
	iface->get_xml_id = mg_function_get_xml_id;
	iface->save_to_xml = mg_function_save_to_xml;
	iface->load_from_xml = mg_function_load_from_xml;
}


static void
mg_server_function_class_init (MgServerFunctionClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_server_function_signals[TEMPL_SIGNAL] =
		g_signal_new ("templ_signal",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgServerFunctionClass, templ_signal),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	class->templ_signal = NULL;

	object_class->dispose = mg_server_function_dispose;
	object_class->finalize = mg_server_function_finalize;

	/* Properties */
	object_class->set_property = mg_server_function_set_property;
	object_class->get_property = mg_server_function_get_property;
	g_object_class_install_property (object_class, PROP,
					 g_param_spec_pointer ("prop", NULL, NULL, (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_server_function_dump;
#endif

}

static void
mg_server_function_init (MgServerFunction * mg_server_function)
{
	mg_server_function->priv = g_new0 (MgServerFunctionPrivate, 1);
	mg_server_function->priv->srv = NULL;
	mg_server_function->priv->objectid = NULL;
	mg_server_function->priv->result_type = NULL;
	mg_server_function->priv->arg_types = NULL;
}


/**
 * mg_server_function_new
 * @srv: a #MgServer object
 *
 * Creates a new MgServerFunction object
 *
 * Returns: the new object
 */
GObject*
mg_server_function_new (MgServer *srv)
{
	GObject   *obj;
	MgServerFunction *mg_server_function;

	g_return_val_if_fail (srv && MG_SERVER (srv), NULL);

	obj = g_object_new (MG_SERVER_FUNCTION_TYPE, "conf",
			    mg_server_get_conf (srv), NULL);
	mg_server_function = MG_SERVER_FUNCTION (obj);
	mg_base_set_id (MG_BASE (mg_server_function), 0);
	g_object_add_weak_pointer (G_OBJECT (srv), (gpointer) &(mg_server_function->priv->srv));
	mg_server_function->priv->srv = srv;

	return obj;
}


static void
mg_server_function_dispose (GObject *object)
{
	MgServerFunction *mg_server_function;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_SERVER_FUNCTION (object));

	mg_server_function = MG_SERVER_FUNCTION (object);
	if (mg_server_function->priv) {
		mg_base_nullify_check (MG_BASE (object));
		g_object_remove_weak_pointer (G_OBJECT (mg_server_function->priv->srv), 
					      (gpointer) &(mg_server_function->priv->srv));

		mg_server_function_set_ret_type (mg_server_function, NULL);
		mg_server_function_set_arg_types (mg_server_function, NULL);
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_server_function_finalize (GObject   * object)
{
	MgServerFunction *mg_server_function;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_SERVER_FUNCTION (object));

	mg_server_function = MG_SERVER_FUNCTION (object);
	if (mg_server_function->priv) {

		g_free (mg_server_function->priv);
		mg_server_function->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_server_function_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	gpointer ptr;
	MgServerFunction *mg_server_function;

	mg_server_function = MG_SERVER_FUNCTION (object);
	if (mg_server_function->priv) {
		switch (param_id) {
		case PROP:
			/* FIXME */
			ptr = g_value_get_pointer (value);
			break;
		}
	}
}

static void
mg_server_function_get_property (GObject              *object,
			guint                 param_id,
			GValue               *value,
			GParamSpec           *pspec)
{
	MgServerFunction *mg_server_function;
	mg_server_function = MG_SERVER_FUNCTION (object);
	
	if (mg_server_function->priv) {
		switch (param_id) {
		case PROP:
			/* FIXME */
			g_value_set_pointer (value, NULL);
			break;
		}	
	}
}

#ifdef debug
static void
mg_server_function_dump (MgServerFunction *func, guint offset)
{
	gchar *str;
	GString *string;
	GSList *list;
	gboolean first = TRUE;
	gint i;

	g_return_if_fail (func && IS_MG_SERVER_FUNCTION (func));
	g_return_if_fail (func->priv);
	
	/* string for the offset */
	str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;

	/* return type */
	if (func->priv->result_type)
		g_print ("%sReturn data type: %s\n", str, mg_server_data_type_get_sqlname (func->priv->result_type));
	else
		g_print ("%s" D_COL_ERR "No return type defined" D_COL_NOR "\n", str);
	
	/* arguments */
	string = g_string_new (" (");
	list = func->priv->arg_types;
	while (list) {
		if (first)
			first = FALSE;
		else
			g_string_append (string, ", ");
		g_string_append_printf (string, "%s", mg_server_data_type_get_sqlname (MG_SERVER_DATA_TYPE (list->data)));
		list = g_slist_next (list);
	}
	g_string_append (string, ")");
	g_print ("%sArguments: %s\n", str, string->str);
	g_string_free (string, TRUE);

	g_free (str);
}
#endif



/* MgXmlStorage interface implementation */
static gchar *
mg_function_get_xml_id (MgXmlStorage *iface)
{
	g_return_val_if_fail (iface && IS_MG_SERVER_FUNCTION (iface), NULL);
	g_return_val_if_fail (MG_SERVER_FUNCTION (iface)->priv, NULL);

	return g_strdup_printf ("PR%s", MG_SERVER_FUNCTION (iface)->priv->objectid);
}

static xmlNodePtr
mg_function_save_to_xml (MgXmlStorage *iface, GError **error)
{
	xmlNodePtr node = NULL, subnode;
	MgServerFunction *func;
	gchar *str;
	GSList *list;

	g_return_val_if_fail (iface && IS_MG_SERVER_FUNCTION (iface), NULL);
	g_return_val_if_fail (MG_SERVER_FUNCTION (iface)->priv, NULL);

	func = MG_SERVER_FUNCTION (iface);

	node = xmlNewNode (NULL, "MG_FUNCTION");

	str = mg_function_get_xml_id (iface);
	xmlSetProp (node, "id", str);
	g_free (str);
	xmlSetProp (node, "name", mg_base_get_name (MG_BASE (func)));
	xmlSetProp (node, "descr", mg_base_get_description (MG_BASE (func)));
	xmlSetProp (node, "owner", mg_base_get_owner (MG_BASE (func)));

	/* return type */
	if (func->priv->result_type) {
		subnode = xmlNewChild (node, NULL, "MG_FUNC_PARAM", NULL);
		
		str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (func->priv->result_type));
		xmlSetProp (subnode, "type", str);
		g_free (str);
		xmlSetProp (subnode, "way", "out");
	}

	/* argument types */
	list = func->priv->arg_types;
	while (list) {
		subnode = xmlNewChild (node, NULL, "MG_FUNC_PARAM", NULL);
		str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
		xmlSetProp (subnode, "type", str);
		xmlSetProp (subnode, "way", "in");
		list = g_slist_next (list);
	}

	return node;
}

static gboolean
mg_function_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgServerFunction *func;
	gchar *prop;
	gboolean pname = FALSE, pid = FALSE;
	xmlNodePtr subnode;

	g_return_val_if_fail (iface && IS_MG_SERVER_FUNCTION (iface), FALSE);
	g_return_val_if_fail (MG_SERVER_FUNCTION (iface)->priv, FALSE);
	g_return_val_if_fail (node, FALSE);

	func = MG_SERVER_FUNCTION (iface);
	if (strcmp (node->name, "MG_FUNCTION")) {
		g_set_error (error,
			     MG_SERVER_FUNCTION_ERROR,
			     MG_SERVER_FUNCTION_XML_LOAD_ERROR,
			     _("XML Tag is not <MG_FUNCTION>"));
		return FALSE;
	}

	/* function's attributes */
	prop = xmlGetProp (node, "id");
	if (prop) {
		if ((*prop == 'P') && (*(prop+1)=='R')) {
			pid = TRUE;
			if (func->priv->objectid)
				g_free (func->priv->objectid);
			func->priv->objectid = g_strdup (prop+2);
		}
		g_free (prop);
	}

	prop = xmlGetProp (node, "name");
	if (prop) {
		pname = TRUE;
		mg_base_set_name (MG_BASE (func), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "descr");
	if (prop) {
		mg_base_set_description (MG_BASE (func), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "owner");
	if (prop) {
		mg_base_set_owner (MG_BASE (func), prop);
		g_free (prop);
	}
	
	/* arguments and return type */
	subnode = node->children;
	while (subnode) {
		if (!strcmp (subnode->name, "MG_FUNC_PARAM")) {
			MgServerDataType *dt = NULL;
			prop = xmlGetProp (subnode, "type");
			if (prop) {
				dt = mg_server_get_data_type_by_xml_id (func->priv->srv, prop);
				g_free (prop);
			}

			if (!dt) {
				g_set_error (error,
					     MG_SERVER_FUNCTION_ERROR,
					     MG_SERVER_FUNCTION_XML_LOAD_ERROR,
					     _("Can't find data type for function '%s'"), 
					     mg_base_get_name (MG_BASE (func)));
				return FALSE;
			}
			
			prop = xmlGetProp (subnode, "way");
			if (prop) {
				if (*prop == 'o') {
					if (func->priv->result_type) {
						g_set_error (error,
							     MG_SERVER_FUNCTION_ERROR,
							     MG_SERVER_FUNCTION_XML_LOAD_ERROR,
							     _("More than one return type for function '%s'"), 
							     mg_base_get_name (MG_BASE (func)));
						return FALSE;
					}
					mg_server_function_set_ret_type (func, dt);
				}
				else {
					GSList *list;
					list = g_slist_copy (mg_server_function_get_arg_types (func));
					list = g_slist_append (list, dt);
					mg_server_function_set_arg_types (func, list);
					g_slist_free (list);
				}
				g_free (prop);
			}
		}
		subnode = subnode->next;
	}

	if (pname && pid)
		return TRUE;
	else {
		g_set_error (error,
			     MG_SERVER_FUNCTION_ERROR,
			     MG_SERVER_FUNCTION_XML_LOAD_ERROR,
			     _("Missing required attributes for <MG_FUNCTION>"));
		return FALSE;
	}
}




/**
 * mg_server_function_set_dbms_id
 * @func: a #MgServerFunction object
 * @id: the DBMS identifier
 *
 * Set the DBMS identifier of the function
 */
void
mg_server_function_set_dbms_id (MgServerFunction *func, const gchar *id)
{
	g_return_if_fail (func && IS_MG_SERVER_FUNCTION (func));
	g_return_if_fail (func->priv);
	g_return_if_fail (id && *id);

	if (func->priv->objectid)
		g_free (func->priv->objectid);
	func->priv->objectid = g_strdup (id);
}


/**
 * mg_server_function_get_dbms_id
 * @func: a #MgServerFunction object
 *
 * Get the DBMS identifier of the function
 *
 * Returns: the function's id
 */
const gchar *
mg_server_function_get_dbms_id (MgServerFunction *func)
{
	g_return_val_if_fail (func && IS_MG_SERVER_FUNCTION (func), NULL);
	g_return_val_if_fail (func->priv, NULL);

	return func->priv->objectid;
}


/**
 * mg_server_function_set_sqlname
 * @func: a #MgServerFunction object
 * @sqlname: 
 *
 * Set the SQL name of the data type.
 */
void
mg_server_function_set_sqlname (MgServerFunction *func, const gchar *sqlname)
{
	g_return_if_fail (func && IS_MG_SERVER_FUNCTION (func));
	g_return_if_fail (func->priv);

	mg_base_set_name (MG_BASE (func), sqlname);
}


/**
 * mg_server_function_get_sqlname
 * @func: a #MgServerFunction object
 *
 * Get the DBMS's name of a data type.
 *
 * Returns: the name of the data type
 */
const gchar *
mg_server_function_get_sqlname (MgServerFunction *func)
{
	g_return_val_if_fail (func && IS_MG_SERVER_FUNCTION (func), NULL);
	g_return_val_if_fail (func->priv, NULL);

	return mg_base_get_name (MG_BASE (func));
}

static void nullified_data_type_cb (MgServerDataType *dt, MgServerFunction *func);

/**
 * mg_server_function_set_arg_types
 * @func: a #MgServerFunction object
 * @arg_types: a list of #MgServerDataType objects or #NULL values ordered to represent the data types
 * of the function's arguments .
 *
 * Set the arguments types of a function
 */
void 
mg_server_function_set_arg_types (MgServerFunction *func, const GSList *arg_types)
{
	GSList *list;

	g_return_if_fail (func && IS_MG_SERVER_FUNCTION (func));
	g_return_if_fail (func->priv);

	if (func->priv->arg_types) {
		list = func->priv->arg_types;
		while (list) {
			if (list->data) {
				g_signal_handlers_disconnect_by_func (G_OBJECT (list->data), 
								      G_CALLBACK (nullified_data_type_cb),
								      func);
				g_object_unref (G_OBJECT (list->data));
			}
			list = g_slist_next (list);
		}
		g_slist_free (func->priv->arg_types);
	}

	func->priv->arg_types = g_slist_copy (arg_types);
	list = func->priv->arg_types;
	while (list) {
		if (list->data) {
			g_signal_connect (G_OBJECT (list->data), "nullified",
					  G_CALLBACK (nullified_data_type_cb), func);
			g_object_ref (G_OBJECT (list->data));
		}
		list = g_slist_next (list);
	}
}

/**
 * mg_server_function_get_arg_types
 * @func: a #MgServerFunction object
 * 
 * To consult the list of arguments types (and number) of a function.
 *
 * Returns: a list of #MgServerDataType objects, the list MUST NOT be modified.
 */
const GSList *
mg_server_function_get_arg_types (MgServerFunction *func)
{
	g_return_val_if_fail (func && IS_MG_SERVER_FUNCTION (func), NULL);
	g_return_val_if_fail (func->priv, NULL);

	return func->priv->arg_types;
}

/**
 * mg_server_function_set_ret_type
 * @func: a #MgServerFunction object
 * @dt: a #MgServerDataType object or #NULL
 *
 * Set the return type of a function
 */
void 
mg_server_function_set_ret_type  (MgServerFunction *func, MgServerDataType *dt)
{
	g_return_if_fail (func && IS_MG_SERVER_FUNCTION (func));
	g_return_if_fail (func->priv);
	if (dt)
		g_return_if_fail (dt && IS_MG_SERVER_DATA_TYPE (dt));
	
	if (func->priv->result_type) { 
		g_signal_handlers_disconnect_by_func (G_OBJECT (func->priv->result_type), 
						      G_CALLBACK (nullified_data_type_cb), func);
		g_object_unref (G_OBJECT (func->priv->result_type));
	}

	func->priv->result_type = dt;
	if (dt) {
		g_signal_connect (G_OBJECT (dt), "nullified",
				  G_CALLBACK (nullified_data_type_cb), func);
		g_object_ref (G_OBJECT (dt));
	}
}

static void
nullified_data_type_cb (MgServerDataType *dt, MgServerFunction *func)
{
	mg_base_nullify (MG_BASE (func));
}

/**
 * mg_server_function_get_ret_type
 * @func: a #MgServerFunction object
 * 
 * To consult the return type of a function.
 *
 * Returns: a #MgServerDataType object.
 */
MgServerDataType *
mg_server_function_get_ret_type  (MgServerFunction *func)
{
	g_return_val_if_fail (func && IS_MG_SERVER_FUNCTION (func), NULL);
	g_return_val_if_fail (func->priv, NULL);

	return func->priv->result_type;
}
