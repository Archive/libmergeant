/* mg-qf-func.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_QF_FUNC_H_
#define __MG_QF_FUNC_H_

#include "mg-base.h"
#include "mg-defs.h"
#include "mg-server-function.h"
#include "mg-qfield.h"

G_BEGIN_DECLS

#define MG_QF_FUNC_TYPE          (mg_qf_func_get_type())
#define MG_QF_FUNC(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_qf_func_get_type(), MgQfFunc)
#define MG_QF_FUNC_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_qf_func_get_type (), MgQfFuncClass)
#define IS_MG_QF_FUNC(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_qf_func_get_type ())


/* error reporting */
extern GQuark mg_qf_func_error_quark (void);
#define MG_QF_FUNC_ERROR mg_qf_func_error_quark ()

enum
{
	MG_QF_FUNC_XML_LOAD_ERROR,
	MG_QF_FUNC_RENDER_ERROR
};


/* struct for the object's data */
struct _MgQfFunc
{
	MgQfield               object;
	MgQfFuncPrivate       *priv;
};

/* struct for the object's class */
struct _MgQfFuncClass
{
	MgQfieldClass                  class;

	/* signals */
	void   (*templ_signal)        (MgQfFunc *obj);
};

guint             mg_qf_func_get_type          (void);
GObject          *mg_qf_func_new_with_func     (MgQuery *query, MgServerFunction *func);
GObject          *mg_qf_func_new_with_xml_id   (MgQuery *query, const gchar *func_xml_id);

MgServerFunction *mg_qf_func_get_ref_func      (MgQfFunc *func);
GSList           *mg_qf_func_get_args          (MgQfFunc *func);

G_END_DECLS

#endif
