/* mg-context.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-context.h"
#include "mg-query.h"
#include "marshal.h"
#include <string.h>
#include "mg-parameter.h"
#include "mg-field.h"
#include "mg-referer.h"

/* 
 * Main static functions 
 */
static void mg_context_class_init (MgContextClass * class);
static void mg_context_init (MgContext * srv);
static void mg_context_dispose (GObject   * object);
static void mg_context_finalize (GObject   * object);

static void mg_context_set_property (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec);
static void mg_context_get_property (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec);


static void nullified_param_cb (MgParameter *param, MgContext *context);
static void changed_param_cb (MgParameter *param, MgContext *context);

#ifdef debug
static void mg_context_dump                     (MgContext *context, guint offset);
#endif

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	LAST_SIGNAL
};

static gint mg_context_signals[LAST_SIGNAL] = { };

/* properties */
enum
{
	PROP_0,
	PROP
};


/* private structure */
struct _MgContextPrivate
{
	gint dummy;
};



/* module error */
GQuark mg_context_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_context_error");
	return quark;
}


guint
mg_context_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgContextClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_context_class_init,
			NULL,
			NULL,
			sizeof (MgContext),
			0,
			(GInstanceInitFunc) mg_context_init
		};
		
		type = g_type_register_static (MG_BASE_TYPE, "MgContext", &info, 0);
	}
	return type;
}


static void
mg_context_class_init (MgContextClass *class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_context_dispose;
	object_class->finalize = mg_context_finalize;

	/* Properties */
	object_class->set_property = mg_context_set_property;
	object_class->get_property = mg_context_get_property;
	g_object_class_install_property (object_class, PROP,
					 g_param_spec_pointer ("prop", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_context_dump;
#endif
}

static void
mg_context_init (MgContext *mg_context)
{
	mg_context->priv = g_new0 (MgContextPrivate, 1);
	mg_context->parameters = NULL;
	mg_context->nodes = NULL;
}


/**
 * mg_context_new
 * @conf: a #MgConf object
 * @params: a list of #MgParameter objects
 *
 * Creates a new #MgContext structure, and populates it with the list given as argument.
 * The list can then be freed as it gets copied. All the parameters in @params are referenced counted
 * and modified, so they should not be used anymore afterwards.
 *
 * Returns: a new #MgContext structure
 */
GObject *
mg_context_new (MgConf *conf, GSList *params)
{
	GObject *obj;
	GHashTable *hash;       /* key = query referenced by a param, value= corresponding node */
	GHashTable *node_hashs; /* key = node, value = corresponding replacements hash */

	gboolean err = FALSE;
	GSList *list;
	MgContext *mg_context;

	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);

	obj = g_object_new (MG_CONTEXT_TYPE, "conf", conf, NULL);
        mg_context = MG_CONTEXT (obj);
        mg_base_set_id (MG_BASE (mg_context), 0);

	if (params)
		mg_context->parameters = g_slist_copy (params);
	mg_context->nodes = NULL;

	/* getting signals from the parameters */
	list = mg_context->parameters;
	while (list) {
		g_signal_connect (G_OBJECT (list->data), "nullified",
				  G_CALLBACK (nullified_param_cb), mg_context);
		g_signal_connect (G_OBJECT (list->data), "changed",
				  G_CALLBACK (changed_param_cb), mg_context);

		g_object_ref (G_OBJECT (list->data));
		list = g_slist_next (list);
	}

	/* creating the MgContextNode entries */
	hash = g_hash_table_new (NULL, NULL);
	node_hashs = g_hash_table_new (NULL, NULL);
	list = mg_context->parameters;
	while (list && !err) {
		MgQfield *ref_field;
		
		ref_field = mg_parameter_get_in_field (MG_PARAMETER (list->data));
		if (ref_field) {
			/* the parameter has a value constrained by a query */
			MgEntity *ent;
			ent = mg_field_get_entity (MG_FIELD (ref_field));
			if (!ent || !IS_MG_QUERY (ent))
				err = TRUE;
			else {
				MgContextNode *enode = g_hash_table_lookup (hash, ent);
				if (!enode) {
					MgContextNode *node;
					GHashTable *repl;       /* replacements for query copying */
					
					repl = g_hash_table_new (NULL, NULL);
					node = g_new0 (MgContextNode, 1);
					node->query = MG_QUERY (mg_query_new_copy (MG_QUERY (ent), repl));
					node->params = g_slist_append (NULL, list->data);
					g_hash_table_insert (hash, ent, node);
					g_hash_table_insert (node_hashs, node, repl);

					mg_context->nodes = g_slist_append (mg_context->nodes, node);
				}
				else 
					enode->params = g_slist_append (enode->params, list->data);
			}
		}
		else {
			/* the parameter is free fill */
			MgContextNode *node;

			node = g_new0 (MgContextNode, 1);
			node->param = list->data;
			mg_context->nodes = g_slist_append (mg_context->nodes, node);
		}

		list = g_slist_next (list);
	}

	/* taking into account all the replacements */
	list = mg_context->nodes;
	while (list) {
		GHashTable *node_repl, *dep_repl;
		MgQfield *ref_field;
		MgContextNode *node = MG_CONTEXT_NODE (list->data);
		GSList *params;

		node_repl = g_hash_table_lookup (node_hashs, node);
		
		if (node->param) {
			/* for field replacements if necessary */
			GSList *dest_fields, *tmplist;

			dest_fields = mg_parameter_get_dest_fields (node->param);
			tmplist = dest_fields;
			while (tmplist) {
				MgContextNode *enode;
				MgEntity *ent;
				ent = mg_field_get_entity (MG_FIELD (tmplist->data));
				enode = g_hash_table_lookup (hash, ent);
				if (enode) {
					dep_repl = g_hash_table_lookup (node_hashs, enode);
					mg_parameter_replace_ref (node->param, dep_repl);
				}
				tmplist = g_slist_next (tmplist);
			}
		}

		params = node->params;
		while (params) {
			GSList *dest_fields, *tmplist;

			/* dependency node's replacements */
			ref_field = mg_parameter_get_in_field (MG_PARAMETER (params->data));
			if (ref_field) {
				MgContextNode *enode;
				MgEntity *ent;
				ent = mg_field_get_entity (MG_FIELD (ref_field));
				enode = g_hash_table_lookup (hash, ent);
				g_assert (enode);
				dep_repl = g_hash_table_lookup (node_hashs, enode);
				mg_parameter_replace_ref (MG_PARAMETER (params->data), dep_repl);
			}
				
			/* this node's replacements */
			if (node_repl)
				mg_parameter_replace_ref (MG_PARAMETER (params->data), node_repl);

			/* for field replacements if necessary */
			dest_fields = mg_parameter_get_dest_fields (MG_PARAMETER (params->data));
			tmplist = dest_fields;
			while (tmplist) {
				MgContextNode *enode;
				MgEntity *ent;
				ent = mg_field_get_entity (MG_FIELD (tmplist->data));
				enode = g_hash_table_lookup (hash, ent);
				if (enode) {
					dep_repl = g_hash_table_lookup (node_hashs, enode);
					mg_parameter_replace_ref (MG_PARAMETER (params->data), dep_repl);
				}
				tmplist = g_slist_next (tmplist);
			}

			params = g_slist_next (params);
		}
		
		if (node->query) {
			/* replacements for the query */
			GSList *sublist = mg_context->nodes;

			while (sublist) {
				if (MG_CONTEXT_NODE (sublist->data)->query)
					mg_referer_replace_refs (MG_REFERER (node->query), g_hash_table_lookup (node_hashs, sublist->data));
				sublist = g_slist_next (sublist);
			}
		}
		
		list = g_slist_next (list);
	}

	/* memory de-allocations */
	list = mg_context->nodes;
	while (list) {
		GHashTable *node_repl;
		node_repl = g_hash_table_lookup (node_hashs, list->data);
		if (node_repl) {
			g_hash_table_remove (node_hashs, list->data);
			g_hash_table_destroy (node_repl);
		}
		list = g_slist_next (list);
	}
	g_hash_table_destroy (hash);
	g_hash_table_destroy (node_hashs);

	return obj;
}


/**
 * mg_context_new_copy
 * @orig: a #MgContext to make a copy of
 * 
 * Copy constructor
 *
 * Returns: a the new copy of @orig
 */
GObject *
mg_context_new_copy (MgContext *orig)
{
	GObject   *obj;
	MgContext *mg_context;
	MgConf *conf;
	GSList *list;

	g_return_val_if_fail (orig && IS_MG_CONTEXT (orig), NULL);

	conf = mg_base_get_conf (MG_BASE (orig));
	obj = g_object_new (MG_CONTEXT_TYPE, "conf", conf, NULL);
	mg_context = MG_CONTEXT (obj);
	mg_base_set_id (MG_BASE (mg_context), 0);

	/* copy parameters */
	if (orig->parameters)
		mg_context->parameters = g_slist_copy (orig->parameters);

	list = mg_context->parameters;
	while (list) {
		g_signal_connect (G_OBJECT (list->data), "nullified",
				  G_CALLBACK (nullified_param_cb), mg_context);
		g_signal_connect (G_OBJECT (list->data), "changed",
				  G_CALLBACK (changed_param_cb), mg_context);
		g_object_ref (G_OBJECT (list->data));
		list = g_slist_next (list);
	}

	/* copy nodes */
	list = orig->nodes;
	while (list) {
		MgContextNode *node, *orig;

		orig = MG_CONTEXT_NODE (list->data);
		node = g_new0 (MgContextNode, 1);
		node->param = orig->param;
		if (orig->query) {
			node->query = orig->query;
			g_object_ref (G_OBJECT (node->query));
		}
		if (orig->params)
			node->params = g_slist_copy (orig->params);
		
		mg_context->nodes = g_slist_append (mg_context->nodes, node);
		list = g_slist_next (list);
	}

	return obj;	
}

static void
nullified_param_cb (MgParameter *param, MgContext *context)
{
	g_assert (g_slist_find (context->parameters, param));
	context->parameters = g_slist_remove (context->parameters, param);
	g_signal_handlers_disconnect_by_func (G_OBJECT (param),
					      G_CALLBACK (nullified_param_cb), context);
	g_signal_handlers_disconnect_by_func (G_OBJECT (param),
					      G_CALLBACK (changed_param_cb), context);

	if (context->nodes) {
		MgContextNode *node;
		gboolean finished = FALSE;
		GSList *list = context->nodes;

		while (list && !finished) {
			node = MG_CONTEXT_NODE (list->data);
			if (node->params && g_slist_find (node->params, param)) {
				node->params = g_slist_remove (node->params, param);
				if (!node->params) {
					g_object_unref (node->query);
					context->nodes = g_slist_remove (context->nodes, node);
					g_free (node);
				}
				finished = TRUE;
			}
			list = g_slist_next (list);
		}
	}
	g_object_unref (G_OBJECT (param));
}

static void
changed_param_cb (MgParameter *param, MgContext *context)
{
	/* signal the parameter change */
	mg_base_changed (MG_BASE (context));
}

static void
mg_context_dispose (GObject *object)
{
	MgContext *mg_context;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_CONTEXT (object));

	mg_context = MG_CONTEXT (object);
	if (mg_context->priv) {
		mg_base_nullify_check (MG_BASE (object));
	}
	
	/* free the nodes if there are some */
	while (mg_context->nodes)
		mg_context_free_node (mg_context, MG_CONTEXT_NODE (mg_context->nodes->data));

	/* free the parameters list */
	while (mg_context->parameters)
		nullified_param_cb (MG_PARAMETER (mg_context->parameters->data), mg_context);


	/* parent class */
	parent_class->dispose (object);
}

static void
mg_context_finalize (GObject *object)
{
	MgContext *mg_context;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_CONTEXT (object));

	mg_context = MG_CONTEXT (object);
	if (mg_context->priv) {
		g_free (mg_context->priv);
		mg_context->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_context_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	gpointer ptr;
	MgContext *mg_context;

	mg_context = MG_CONTEXT (object);
	if (mg_context->priv) {
		switch (param_id) {
		case PROP:
			ptr = g_value_get_pointer (value);
			break;
		}
	}
}

static void
mg_context_get_property (GObject              *object,
			  guint                 param_id,
			  GValue               *value,
			  GParamSpec           *pspec)
{
	MgContext *mg_context;
	mg_context = MG_CONTEXT (object);
	
	if (mg_context->priv) {
		switch (param_id) {
		case PROP:
			break;
		}	
	}
}

/**
 * mg__context_is_coherent
 * @context: a #MgContext object
 * @error:
 *
 * Checks that @context has a coherent structure
 *
 * Returns: TRUE if @context is coherent
 */
gboolean
mg_context_is_coherent (MgContext *context, GError **error)
{
	TO_IMPLEMENT;
	return TRUE;
}


/**
 * mg_context_free_node
 * @context: a #MgContext object
 * @node: a #MgContextNode structure
 *
 * Removes @node from @context and de-allocates any memory in the process.
 */
void
mg_context_free_node (MgContext *context, MgContextNode *node)
{
	g_return_if_fail (context && IS_MG_CONTEXT (context));
	g_return_if_fail (node);
	g_return_if_fail (context->priv);
	g_return_if_fail (g_slist_find (context->nodes, node));

	if (node->param)
		node->param = NULL;
	if (node->query)
		g_object_unref (G_OBJECT (node->query));
	if (node->params)
		g_slist_free (node->params);
	context->nodes = g_slist_remove (context->nodes, node);
	g_free (node);
}

/**
 * mg_context_is_valid
 * @context: a #MgContext object
 *
 * Tells if all the context's parameters have valid data
 *
 * Returns: TRUE if the context is valid
 */
gboolean
mg_context_is_valid (MgContext *context)
{
	GSList *params;
	gboolean retval = TRUE;

	g_return_val_if_fail (context && IS_MG_CONTEXT (context), FALSE);
	g_return_val_if_fail (context->priv, FALSE);

	params = context->parameters;
	while (params && retval) {
		if (!mg_parameter_is_valid (MG_PARAMETER (params->data)))
			retval = FALSE;
		
		/* g_print ("== PARAM %p: valid= %d, value=%s\n", params->data, mg_parameter_is_valid (MG_PARAMETER (params->data)), */
/* 			 mg_parameter_get_value (MG_PARAMETER (params->data)) ?  */
/* 			 gda_value_stringify (mg_parameter_get_value (MG_PARAMETER (params->data))) : "Null"); */
		params = g_slist_next (params);
	}

	return retval;
}

/**
 * mg_context_needs_user_input
 * @context: a #MgContext object
 *
 * Tells if the context needs the user to provide some values for some parameters.
 * This is the case when either the context is invalid, or the context is valid and
 * contains some #MgParameter objects which are valid but are defined to require 
 * a user input.
 *
 * Note: this function may return TRUE even though @context is valid.
 *
 * Returns: TRUE if a user input is required for the context
 */
gboolean
mg_context_needs_user_input (MgContext *context)
{
	GSList *params;
	gboolean retval = FALSE;

	g_return_val_if_fail (context && IS_MG_CONTEXT (context), FALSE);
	g_return_val_if_fail (context->priv, FALSE);

	if (!mg_context_is_valid (context))
		return TRUE;

	params = context->parameters;
	while (params && !retval) {
		if (mg_parameter_requires_user_input (MG_PARAMETER (params->data)))
			retval = TRUE;
		
		params = g_slist_next (params);
	}

	return retval;
}


/**
 * mg_context_find_parameter_for_field
 * @context: a #MgContext object
 * @for_field: a #MgQfield object
 *
 * Finds a #MgParameter which is to be used by @for_field
 *
 * Returns: a #MgParameter or %NULL
 */
MgParameter *
mg_context_find_parameter_for_field (MgContext *context, MgQfield *for_field)
{
	MgParameter *param = NULL;
	GSList *list;

	g_return_val_if_fail (context && IS_MG_CONTEXT (context), NULL);
	g_return_val_if_fail (context->priv, NULL);

	list = context->parameters;
	while (list && !param) {
		GSList *for_fields = mg_parameter_get_dest_fields (MG_PARAMETER (list->data));
		if (for_fields &&  g_slist_find (for_fields, for_field))
			param = MG_PARAMETER (list->data);
		list = g_slist_next (list);
	}

	return param;
}

#ifdef debug
static void
mg_context_dump (MgContext *context, guint offset)
{
	gchar *str;
        guint i;
	
	g_return_if_fail (context && IS_MG_CONTEXT (context));

        /* string for the offset */
        str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;

        /* dump */
        if (context->priv) {
		GSList *list;
                g_print ("%s" D_COL_H1 "MgContext" D_COL_NOR " %p (id=%d)\n",
                         str, context, mg_base_get_id (MG_BASE (context)));

		g_print ("%s     ***** Parameters:\n", str);
		list = context->parameters;
		while (list) {
			mg_base_dump (MG_BASE (list->data), offset+5);
			list = g_slist_next (list);
		}

		g_print ("\n%s     ***** Nodes:\n", str);
		list = context->nodes;
		while (list) {
			MgContextNode *node = MG_CONTEXT_NODE (list->data);
			if (node->param) 
				g_print ("%s     * " D_COL_H1 "MgContextNode" D_COL_NOR " for param %p\n", str, node->param);
			else {
				GSList *params = node->params;
				g_print ("%s     * " D_COL_H1 "MgContextNode" D_COL_NOR "\n", str);
				mg_base_dump (MG_BASE (node->query), offset+10);
				while (params) {
					g_print ("%s       -> param %p\n", str, params->data);
					params = g_slist_next (params);
				}
			}
			list = g_slist_next (list);
		}
	}
        else
                g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, context);
}
#endif

