/* mg-selector.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <string.h>
#include <libgnomedb/libgnomedb.h>
#include "mg-selector.h"
#include "marshal.h"
#include "mg-server.h"
#include "mg-server-data-type.h"
#include "mg-server-function.h"
#include "mg-server-aggregate.h"
#include "mg-database.h"
#include "mg-db-table.h"
#include "mg-entity.h"
#include "mg-db-field.h"
#include "mg-field.h"
#include "mg-db-constraint.h"
#include "mg-query.h"

#include "mg-qf-all.h"
#include "mg-qf-field.h"
#include "mg-qf-value.h"
#include "mg-renderer.h"

static void mg_selector_class_init (MgSelectorClass * class);
static void mg_selector_init (MgSelector * wid);
static void mg_selector_dispose (GObject   * object);


/* 
 * Structure to define a "Module". A module is given a GtkTreeIter and manages to display data below that
 * iterator (the iter is allocated, and may be NULL if the module starts at the root of the GtkTreeModel).
 *
 * Modules are created either when the widget is initialized, or by other modules when objects are created
 * and must be displayed using a module by themselves.
 * 
 * The structure of a module is as follows:
 * - selector: points to the MgSelector widget itself
 * - iter: the top node of the module, or NULL if the module is alone
 * - fill_model: function called just after the module is created, to fill the GtkTreeModel
 * - free: function called right before freeing the module's structure
 * - col_name: returns the module's preferences for a given column's name
 * - obj_manager: function which can be called when an object is created to decide if some sub modules must be created
 * - model_store_data: function called to set specific columns' values (usually the module's contents management is written
 *   in a quite generic way and this function gets called whenever a row is inserted in the GtkTreeModel pr updated).
 * - parent_module: the parent module, or NULL if there is none
 * - sub_modules: the sub modules if applicable, or NULL
 * - mod_data: an extra handle to be used by the module itself, depends on each module.
 *
 * Rem: the obj_manager and model_store_data are not used by all of the modules.
 */
typedef struct _Module {
	MgSelector           *selector;
	GtkTreeIter          *iter;

	void                (*fill_model)       (struct _Module *module);
	void                (*free)             (struct _Module *module);
	const gchar        *(*col_name)         (struct _Module *module, guint colno);
	struct _Module     *(*obj_manager)      (struct _Module *module, GtkTreeIter *iter, GObject *added_obj);
	void                (*model_store_data) (struct _Module *module, GtkTreeIter *iter);

	/* modules hierarchy */
	struct _Module       *parent_module;
	GSList               *sub_modules;

	/* module's own private usage */
	gpointer              mod_data;
} Module;
#define MODULE(x) ((Module *) x)


static gboolean move_iter_to_next_leaf (GtkTreeModel *model, GtkTreeIter *iter);
static gboolean set_iter_position (GtkTreeModel *model, GSList *obj_list, gpointer object, gpointer missing, GtkTreeIter *iter);
static void     model_store_data (Module *module, GtkTreeIter *iter);


/* Modules creation functions */
static Module      *module_data_types_new (MgSelector *mgsel, gboolean insert_header, 
					   GtkTreeIter *iter, gpointer data);
static Module      *module_functions_new (MgSelector *mgsel, gboolean insert_header, 
					  GtkTreeIter *iter, gpointer data);
static Module      *module_aggregates_new (MgSelector *mgsel, gboolean insert_header, 
					   GtkTreeIter *iter, gpointer data);
static Module      *module_tables_new (MgSelector *mgsel, gboolean insert_header, 
				       GtkTreeIter *iter, gpointer data);
static Module      *module_onetable_new (MgSelector *mgsel, gboolean insert_header, 
					 GtkTreeIter *iter, gpointer data);
static Module      *module_queries_new (MgSelector *mgsel, gboolean insert_header, 
					GtkTreeIter *iter, gpointer data);
static Module      *module_onequery_new (MgSelector *mgsel, gboolean insert_header, 
					 GtkTreeIter *iter, gpointer data);


/* columns names for the GtkTreeView */
enum
{
	NAME_COLUMN,
	OWNER_COLUMN, /* associated to MG_SELECTOR_COLUMN_OWNER */
	DESCR_COLUMN, /*               MG_SELECTOR_COLUMN_COMMENTS */
	EXTRA1_COLUMN,/*               MG_SELECTOR_COLUMN_TYPE */
	EXTRA2_COLUMN,/*               MG_SELECTOR_COLUMN_FIELD_LENGTH */
	EXTRA3_COLUMN,/*               MG_SELECTOR_COLUMN_FIELD_NNUL */
	EXTRA4_COLUMN,/*               MG_SELECTOR_COLUMN_FIELD_NNUL to tell if applicable */
	EXTRA5_COLUMN,/*               MG_SELECTOR_COLUMN_FIELD_DEFAULT */
	EXTRA6_COLUMN,/*               MG_SELECTOR_COLUMN_QFIELD_VALUE */
	EXTRA7_COLUMN,/*               MG_SELECTOR_COLUMN_QFIELD_TYPE */
	EXTRA_END_COLUMN,/* marks the end of extra columns, contains nothing */
	OBJ_COLUMN,
	PIXBUF_COLUMN,
	CONTENTS_COLUMN,
	SUB_MODULE_COLUMN,
	NUM_COLUMN,
};

enum
{
	C_SELECTION_CHANGED,
	LAST_SIGNAL
};

enum
{
	CONTENTS_TOP_CATEGORY,
	CONTENTS_GROUP_CATEGORY,
	CONTENTS_OBJECT
};



struct _MgSelectorPriv
{
	MgConf      *conf;
	gulong       mode;
	gulong       columns;

	GtkTreeView *treeview;
	GSList      *modules; /* list of Module structures */
	GObject     *selection;
};


static gint mg_selector_signals[LAST_SIGNAL] = { 0 };

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;


guint
mg_selector_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgSelectorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_selector_class_init,
			NULL,
			NULL,
			sizeof (MgSelector),
			0,
			(GInstanceInitFunc) mg_selector_init
		};		
		
		type = g_type_register_static (GTK_TYPE_VBOX, "MgSelector", &info, 0);
	}

	return type;
}

static void
mg_selector_class_init (MgSelectorClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	mg_selector_signals[C_SELECTION_CHANGED] =
		g_signal_new ("selection_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgSelectorClass, selection_changed),
			      NULL, NULL,
			      marshal_VOID__OBJECT, G_TYPE_NONE, 1,
			      G_TYPE_OBJECT);

	class->selection_changed = NULL;
	object_class->dispose = mg_selector_dispose;
}

static void
mg_selector_init (MgSelector * wid)
{
	wid->priv = g_new0 (MgSelectorPriv, 1);
	wid->priv->conf = NULL;
	wid->priv->mode = 0;
	wid->priv->columns = 0;
	wid->priv->treeview = NULL;
	wid->priv->modules = NULL;
	wid->priv->selection = NULL;
}

static void mg_selector_initialize (MgSelector *mgsel);


static void mg_conf_weak_notify (MgSelector *mgsel, MgConf *conf);
/**
 * mg_selector_new
 * @conf: a #MgConf object
 * @mode: an OR'ed value of the possible items to display in the widget
 * @columns: an OR'ed value describing which columns will be displayed
 *
 * Creates a new #MgSelector widget.
 *
 * Returns: the new widget
 */
GtkWidget *
mg_selector_new (MgConf *conf, gulong mode, gulong columns)
{
	GObject    *obj;
	MgSelector *mgsel;

	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);

	obj = g_object_new (MG_SELECTOR_TYPE, NULL);
	mgsel = MG_SELECTOR (obj);

	mgsel->priv->conf = conf;
	mgsel->priv->mode = mode;
	mgsel->priv->columns = columns;

	g_object_weak_ref (G_OBJECT (mgsel->priv->conf),
			   (GWeakNotify) mg_conf_weak_notify, mgsel);

	mg_selector_initialize (mgsel);

	return GTK_WIDGET (obj);
}

static void
mg_conf_weak_notify (MgSelector *mgsel, MgConf *conf)
{
	GtkTreeModel *model;
	GSList *list;

	/* modules */
	list = mgsel->priv->modules;
	while (list) {
		(MODULE (list->data)->free) (MODULE (list->data));
		g_free (list->data);
		list = g_slist_next (list);
	}
	g_slist_free (mgsel->priv->modules);
	mgsel->priv->modules = NULL;	

	/* Clear the model */
	model = gtk_tree_view_get_model (mgsel->priv->treeview);
	gtk_tree_store_clear (GTK_TREE_STORE (model));

	/* Tell that we don't need to weak unref the MgConf */
	mgsel->priv->conf = NULL;
}

static void tree_selection_changed_cb (GtkTreeSelection *select, MgSelector *mgsel);
static void
mg_selector_dispose (GObject *object)
{
	MgSelector *mgsel;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_SELECTOR (object));
	mgsel = MG_SELECTOR (object);

	if (mgsel->priv) {
		GSList *list;
		GtkTreeSelection *select;

		/* signals, etc */
		select = gtk_tree_view_get_selection (GTK_TREE_VIEW (mgsel->priv->treeview));
		g_signal_handlers_disconnect_by_func (G_OBJECT (select),
						      G_CALLBACK (tree_selection_changed_cb), mgsel);

		/* modules */
		list = mgsel->priv->modules;
		while (list) {
			(MODULE (list->data)->free) (MODULE (list->data));
			g_free (list->data);
			list = g_slist_next (list);
		}
		g_slist_free (mgsel->priv->modules);
		mgsel->priv->modules = NULL;

		/* Weak unref the MgConf is necessary */
		if (mgsel->priv->conf)
			g_object_weak_unref (G_OBJECT (mgsel->priv->conf),
					     (GWeakNotify) mg_conf_weak_notify, mgsel);
		

		/* the private area itself */
		g_free (mgsel->priv);
		mgsel->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

static void tree_value_set_func (GtkTreeViewColumn *tree_column,
				 GtkCellRenderer *cell,
				 GtkTreeModel *model,
				 GtkTreeIter *iter,
				 gpointer user_data);
/*
 * create the model, the columns and the treeview widget itself
 */
static void 
mg_selector_initialize (MgSelector *mgsel)
{
	GtkTreeModel *model;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeSelection *select;
	GtkWidget *sw, *treeview;
	guint nb_modules = 0;
	GSList *list;
	guint i;
	const gchar *str;
	gulong mode = 1;
	gboolean need_header;

	/* counting the number of top level modules */
	for (i = 0 ; i <= 4 ; i++) {
		if (mgsel->priv->mode & mode)
			nb_modules++;	
		mode <<= 1;
	}
	need_header = nb_modules > 1 ? TRUE : FALSE;

	gtk_container_set_border_width (GTK_CONTAINER (mgsel), 0);

	/* model creation */
	model = GTK_TREE_MODEL (gtk_tree_store_new (NUM_COLUMN, 
						    G_TYPE_STRING,    /* NAME_COLUMN */
						    G_TYPE_STRING,    /* OWNER_COLUMN */
						    G_TYPE_STRING,    /* DESCR_COLUMN */
						    G_TYPE_STRING,    /* EXTRA1_COLUMN */
						    G_TYPE_STRING,    /* EXTRA2_COLUMN */
						    G_TYPE_BOOLEAN,   /* EXTRA3_COLUMN */
						    G_TYPE_BOOLEAN,   /* EXTRA4_COLUMN */
						    G_TYPE_STRING,    /* EXTRA5_COLUMN */
						    G_TYPE_STRING,    /* EXTRA6_COLUMN */
						    G_TYPE_STRING,    /* EXTRA7_COLUMN */
						    G_TYPE_BOOLEAN,   /* EXTRA_END_COLUMN */
						    G_TYPE_POINTER,   /* OBJ_COLUMN */
						    GDK_TYPE_PIXBUF,  /* PIXBUF_COLUMN */
						    G_TYPE_INT,       /* CONTENTS_COLUMN */
						    G_TYPE_POINTER)); /* SUB_MODULE_COLUMN */
	/* widget */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (mgsel), sw, TRUE, TRUE, 0);
	treeview = gtk_tree_view_new_with_model (model);
	g_object_unref (G_OBJECT (model));
	gtk_container_add (GTK_CONTAINER (sw), treeview);
	mgsel->priv->treeview = GTK_TREE_VIEW (treeview);

	gtk_widget_set_size_request (sw, 100, 150);
        gtk_widget_show_all (sw); 


	/* modules selection */
	if (mgsel->priv->mode & MG_SELECTOR_DATA_TYPES) {
		Module *module = module_data_types_new (mgsel, need_header, NULL, NULL);
		mgsel->priv->modules = g_slist_append (mgsel->priv->modules, module);
	}

	if (mgsel->priv->mode & MG_SELECTOR_FUNCTIONS) {
		Module *module = module_functions_new (mgsel, need_header, NULL, NULL);
		mgsel->priv->modules = g_slist_append (mgsel->priv->modules, module);
	}

	if (mgsel->priv->mode & MG_SELECTOR_AGGREGATES) {
		Module *module = module_aggregates_new (mgsel, need_header, NULL, NULL);
		mgsel->priv->modules = g_slist_append (mgsel->priv->modules, module);
	}

	if (mgsel->priv->mode & MG_SELECTOR_TABLES) {
		Module *module = module_tables_new (mgsel, need_header, NULL, NULL);
		mgsel->priv->modules = g_slist_append (mgsel->priv->modules, module);
	}

	if (mgsel->priv->mode & MG_SELECTOR_QUERIES) {
		Module *module = module_queries_new (mgsel, need_header, NULL, NULL);
		mgsel->priv->modules = g_slist_append (mgsel->priv->modules, module);
	}

	/* model population */
	list = mgsel->priv->modules;
	while (list) {
		(MODULE (list->data)->fill_model) (MODULE (list->data));
		list = g_slist_next (list);
	}

	/* adding columns */
	/* name column is always here */
	column = gtk_tree_view_column_new ();
	if (nb_modules == 1)
		str = (MODULE (mgsel->priv->modules->data)->col_name)
			(MODULE (mgsel->priv->modules->data), NAME_COLUMN);
	else
		str = _("Name");
	gtk_tree_view_column_set_title (column, str);
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	renderer = gtk_cell_renderer_text_new ();
        gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_cell_data_func (column,
                                                 renderer,
                                                 tree_value_set_func,
                                                 NULL, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);

	/* other columns */
	for (i = OWNER_COLUMN; i < EXTRA_END_COLUMN; i++) {
		MgSelectorColumn selcol = 0;
		switch (i) {
		case OWNER_COLUMN:
			selcol = MG_SELECTOR_COLUMN_OWNER;
			str = _("Owner");
			break;
		case DESCR_COLUMN:
			selcol = MG_SELECTOR_COLUMN_COMMENTS;
			str = _("Description");
			break;
		case EXTRA1_COLUMN:
			selcol = MG_SELECTOR_COLUMN_TYPE;
			str = _("Type");
			break;
		case EXTRA2_COLUMN:
			selcol = MG_SELECTOR_COLUMN_FIELD_LENGTH;
			str = _("Length");
			break;
		case EXTRA3_COLUMN:
			selcol = MG_SELECTOR_COLUMN_FIELD_NNUL;
			str = _("Not NULL?");
			break;
		case EXTRA5_COLUMN:
			selcol = MG_SELECTOR_COLUMN_FIELD_DEFAULT;
			str = _("Default value");
			break;
		case EXTRA6_COLUMN:
			selcol = MG_SELECTOR_COLUMN_QFIELD_VALUE;
			str = _("Value");
			break;		
		case EXTRA7_COLUMN:
			selcol = MG_SELECTOR_COLUMN_QFIELD_TYPE;
			str = _("Kind of field");
			break;		
		default:
			break;
		}

		if (mgsel->priv->columns & selcol) {
			if (nb_modules == 1) {
				const gchar *strtmp = (MODULE (mgsel->priv->modules->data)->col_name)
					(MODULE (mgsel->priv->modules->data), i);
				if (strtmp)
					str = strtmp;
			}
			
			switch (gtk_tree_model_get_column_type (model, i)) {
			case G_TYPE_STRING:
			default:
				renderer = gtk_cell_renderer_text_new ();
				column = gtk_tree_view_column_new_with_attributes (str, renderer, "text", i, NULL);
				break;
			case G_TYPE_BOOLEAN:
				renderer = gtk_cell_renderer_toggle_new ();
				column = gtk_tree_view_column_new_with_attributes (str, renderer, 
										   "active", i, 
										   "visible", i+1, 
										   NULL);
				break;
			}
			
			gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);
		}
	}

	/* selection signal */
	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
	gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (select), "changed",
			  G_CALLBACK (tree_selection_changed_cb), mgsel);
}

static void
tree_value_set_func (GtkTreeViewColumn *tree_column,
                     GtkCellRenderer *cell,
                     GtkTreeModel *model,
                     GtkTreeIter *iter,
                     gpointer user_data)
{
        GdkPixbuf *pixbuf;
        gchar *text;
        GList *renderers;

        gtk_tree_model_get (model, iter, PIXBUF_COLUMN, &pixbuf, NAME_COLUMN, &text, -1);
        renderers = gtk_tree_view_column_get_cell_renderers (tree_column);
        cell = renderers->data;
        g_object_set (G_OBJECT (cell), "pixbuf", pixbuf, NULL);
        cell = renderers->next->data;
        g_object_set (G_OBJECT (cell), "text", text, NULL);
        g_free (text);
        g_list_free (renderers);
}

static void
tree_selection_changed_cb (GtkTreeSelection *select, MgSelector *mgsel) 
{
	GtkTreeIter iter;
	GtkTreeModel *model;
	GObject *sel_obj = NULL;

	if (gtk_tree_selection_get_selected (select, &model, &iter)) {
		gtk_tree_model_get (model, &iter, OBJ_COLUMN, &sel_obj, -1);
		mgsel->priv->selection = sel_obj;
	}
	else 
		mgsel->priv->selection = NULL;

#ifdef debug_signal
	g_print (">> 'SELECTION_CHANGED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (mgsel), mg_selector_signals[C_SELECTION_CHANGED], 0, sel_obj);
#ifdef debug_signal
	g_print ("<< 'SELECTION_CHANGED' from %s\n", __FUNCTION__);
#endif

}

static gchar *get_cut_path_depth (const gchar *path, guint depth);

/**
 * mg_selector_set_selected_object
 * @mgsel:
 * @selection:
 *
 * Force the widget to select a given object, and to display it in its
 * visible area (unfolding nodes on the way if necessary)
 *
 * Returns: TRUE if the specified object was found, and FALSE otherwise
 */
gboolean
mg_selector_set_selected_object (MgSelector *mgsel, GObject *selection)
{
	GtkTreeModel *model;
        GtkTreePath *path = NULL;
        GtkTreeIter iter;
	gpointer obj;
	
        g_return_val_if_fail (mgsel && IS_MG_SELECTOR (mgsel), FALSE);
        g_return_val_if_fail (selection && G_IS_OBJECT (selection), FALSE);

        model = gtk_tree_view_get_model (mgsel->priv->treeview);

	if (!gtk_tree_model_get_iter_first (model, &iter))
		return FALSE;
	
	gtk_tree_model_get (model, &iter, OBJ_COLUMN, &obj, -1);
	if (obj == selection) 
		path = gtk_tree_path_new_first ();
	
	while (!path && move_iter_to_next_leaf (model, &iter)) {
		gtk_tree_model_get (model, &iter, OBJ_COLUMN, &obj, -1);
		if (obj == selection) 
			path = gtk_tree_path_new_first ();
	}

        if (path) {
                gchar *strpath, *partpath;
                guint i = 1;
                GtkTreePath *ppath;
                GtkTreeSelection *tselect;

                strpath = gtk_tree_path_to_string (path);
                partpath = get_cut_path_depth (strpath, i);
                while (partpath) {
                        ppath = gtk_tree_path_new_from_string (partpath);
                        g_free (partpath);
                        gtk_tree_view_expand_row (mgsel->priv->treeview, ppath, FALSE);
                        gtk_tree_path_free (ppath);

                        i++;
                        partpath = get_cut_path_depth (strpath, i);
                }

                g_free (strpath);
                gtk_tree_view_scroll_to_cell (mgsel->priv->treeview, path, NULL, TRUE,
                                              0.5, 0.);
                tselect = gtk_tree_view_get_selection (mgsel->priv->treeview);

                gtk_tree_selection_unselect_all (tselect);
                gtk_tree_selection_select_path (tselect, path);
		
                gtk_tree_view_set_cursor (mgsel->priv->treeview, path, NULL, FALSE);
                gtk_tree_path_free (path);

		return TRUE;
        }
	else
		return FALSE;
}


/* returns an allocated string containing the first part of a path
 * representing a GtkTreePath, depth = 1 for the first ":" 
 */
static gchar *
get_cut_path_depth (const gchar *path, guint depth)
{
	gchar *str, *ptr;
        guint i = 0;

        str = g_strdup (path);
        ptr = str;
        while ((i < depth) && (*ptr != 0)) {
                if (*ptr == ':') 
                        i++;

                if (i == depth)
                        *ptr = 0;

                ptr++;
        }

        if (i != depth) {
                g_free (str);
                str = NULL;
        }

        return str;
}

/**
 * mg_selector_set_headers_visible
 * @mgsel:
 * @visible:
 *
 * Show or hide the headers.
 */
void
mg_selector_set_headers_visible (MgSelector *mgsel, gboolean visible)
{
	g_return_if_fail (mgsel && IS_MG_SELECTOR (mgsel));
	g_return_if_fail (mgsel->priv);

	gtk_tree_view_set_headers_visible (mgsel->priv->treeview, visible);
}

/**
 * mg_selector_set_column_label
 * @mgsel:
 * @column:
 * @label:
 *
 * Sets the label of a column's header.
 */
void
mg_selector_set_column_label (MgSelector *mgsel, guint column, const gchar *label)
{
	GtkTreeViewColumn *tcolumn;

	g_return_if_fail (mgsel && IS_MG_SELECTOR (mgsel));
	g_return_if_fail (mgsel->priv);

	tcolumn = gtk_tree_view_get_column (mgsel->priv->treeview, column);
	gtk_tree_view_column_set_title (tcolumn, label);
}


/**
 * mg_selector_get_selected_object
 * @mgsel:
 *
 * Get the currently selected object.
 *
 * Returns: the selected object or NULL if nothing is selected or the current selection is
 * on a "category" of objects (such as the "Functions" category for example).
 */
GObject *
mg_selector_get_selected_object (MgSelector *mgsel)
{
	GtkTreeIter iter;
	GtkTreeModel *model;
	GtkTreeSelection *select;
	GObject *sel_obj = NULL;

	g_return_val_if_fail (mgsel && IS_MG_SELECTOR (mgsel), NULL);
	g_return_val_if_fail (mgsel->priv, NULL);

	select = gtk_tree_view_get_selection (mgsel->priv->treeview);
	
	if (gtk_tree_selection_get_selected (select, &model, &iter)) 
		gtk_tree_model_get (model, &iter, OBJ_COLUMN, &sel_obj, -1);
	
	return sel_obj;
}


/**
 * mg_selector_get_selected_object_parent
 * @mgsel:
 *
 * FIXME
 *
 * Returns:
 */
GObject *
mg_selector_get_selected_object_parent (MgSelector *mgsel)
{
	GtkTreeIter iter, parent_iter;
	GtkTreeModel *model;
	GtkTreeSelection *select;
	GObject *sel_obj = NULL;

	select = gtk_tree_view_get_selection (mgsel->priv->treeview);
	
	if (gtk_tree_selection_get_selected (select, &model, &iter)) {
		if (gtk_tree_model_iter_parent (model, &parent_iter, &iter)) {
			gtk_tree_model_get (model, &parent_iter, OBJ_COLUMN, &sel_obj, -1);
		}
	}
	
	return sel_obj;
}


/*
 * Moves iter to the next LEAF of the model.
 * Returns: TRUE if successfull and FALSE otherwise (in this case iter is set to
 * invalid)
 */
static gboolean
move_iter_to_next_leaf (GtkTreeModel *model, GtkTreeIter *iter)
{
	GtkTreeIter tmp;
	gboolean down = FALSE;

	/* decide if we can go down in the tree hierarchy */
	if (gtk_tree_model_iter_children (model, &tmp, iter)) {
		gint contents_id;
		
		gtk_tree_model_get (model, iter, CONTENTS_COLUMN, &contents_id, -1);
		if (contents_id != CONTENTS_OBJECT) 
			down = TRUE;
	}
	
	/* action */
	if (down) {
		/* going down in the tree hierarchy */
		*iter = tmp;
		if (gtk_tree_model_iter_has_child (model, iter))
			return move_iter_to_next_leaf (model, iter);
		else
			return TRUE;
	}
	else {
		tmp = *iter;
		if (gtk_tree_model_iter_next (model, &tmp)) {
			/* we have a next sibling */
			*iter = tmp;

			/* can we go down? */
			if (gtk_tree_model_iter_has_child (model, iter)) {
				gint contents_id;
				
				gtk_tree_model_get (model, iter, CONTENTS_COLUMN, &contents_id, -1);
				if (contents_id != CONTENTS_OBJECT) 
					down = TRUE;
			}
			
			if (down)
			        return move_iter_to_next_leaf (model, iter);
			else
				return TRUE;
		}
		else {
			/* dead end, we need to go back in the tree hierarchy */
			tmp = *iter;

			while (gtk_tree_model_iter_parent (model, &tmp, iter)) {
				gint contents_id;
				*iter = tmp;
				
				gtk_tree_model_get (model, iter, CONTENTS_COLUMN, &contents_id, -1);
				if (contents_id == CONTENTS_TOP_CATEGORY)
					return FALSE;
				
				if (gtk_tree_model_iter_next (model, &tmp)) {
					*iter = tmp;
					if (gtk_tree_model_iter_has_child (model, iter))
						return move_iter_to_next_leaf (model, iter);
					else
						return TRUE;
				}
			}
			return FALSE;
		}
	}
}



/*
 * sets iter to the corresponding object. Iter MUST be initialized either at the begining
 * or to another object before calling.
 * The missing parameter must point to an object which appears _before_ 'object' in the list but not in the
 * model (this introduces a step to ignore in the indexes counting to reach 'object'). Usually 'missing' is NULL,
 * except when we need to add an object to the model and we only know its next sibling.
 */ 
static gboolean
set_iter_position (GtkTreeModel *model, GSList *obj_list, gpointer object, gpointer missing, GtkTreeIter *iter)
{
	gint i = 0, objpos;
	gboolean error = FALSE;
	gpointer obj;

	/* object and missing can't be equal */
	g_return_val_if_fail (object != missing, FALSE);

	/* going to the first leaf if we are on a node! */
	if (gtk_tree_model_iter_has_child (model, iter)) {
		gint contents_id;
					
		gtk_tree_model_get (model, iter, CONTENTS_COLUMN, &contents_id, -1);
		if (contents_id != CONTENTS_OBJECT) 
			error = !move_iter_to_next_leaf (model, iter);
	}

	if (error) 
		return FALSE;


	/* if iter already points to another object, we set it as the starting point */
	gtk_tree_model_get (model, iter, OBJ_COLUMN, &obj, -1);
	if (obj) {
		/*g_print ("\tobj %p of type %s\n", obj, G_OBJECT_TYPE_NAME (obj));*/
		i = g_slist_index (obj_list, obj);
		if (i < 0) {
			g_warning ("Iter is invalid!");
			i = 0;
		}
	}
	
	if (obj != object) {
		/* moving forward to the desired position */
		objpos = g_slist_index (obj_list, object) - (missing ? 1 : 0);
		while ((i != objpos) && !error) {
			error = !move_iter_to_next_leaf (model, iter);
			i++;
		}
		
		if (!error) {
			gtk_tree_model_get (model, iter, OBJ_COLUMN, &obj, -1);
			if (obj != object) {
				g_warning ("Obj found %p (%s=%s) != object requested %p (%s=%s)\n", 
					   obj, G_OBJECT_TYPE_NAME (obj), mg_base_get_name (MG_BASE (obj)),
					   object, G_OBJECT_TYPE_NAME (object), mg_base_get_name (MG_BASE (object)));
				error = TRUE;
			}
		}
	}
	
	return !error;
}

/*
 * Fills a model iterator with data, depending on the module
 */
static void
model_store_data (Module *module, GtkTreeIter *iter)
{
	if (module->model_store_data)
		(module->model_store_data) (module, iter);
}






/*
 * Generic module model where objects are grouped by their mg_base_get_name() value
 * The text which gets displayed
 */
typedef struct {
	GSList      *objects;
	GtkTreeIter *iter;
	GObject     *iter_obj;
	GdkPixbuf   *obj_pixbuf;
	
	GObject     *manager;
	gboolean     manager_weak_refed;

	GSList    *(*get_objects_list) (Module *module);
	gchar     *(*get_extended_name) (GObject *obj);
} ModNameGroupData;
#define GROUP_DATA(x) ((ModNameGroupData *)(x->mod_data))

static void name_group_init_model_fill     (Module *module, GtkTreeModel *model);
static void name_group_manager_weak_notify (Module *module, GObject *manager_obj);
static void name_group_obj_added_cb        (GObject *manager_obj, GObject *added_obj, Module *module);
static void name_group_obj_removed_cb      (GObject *manager_obj, GObject *removed_obj, Module *module);
static void name_group_obj_updated_cb      (GObject *manager_obj, GObject *upd_obj, Module *module);
static void name_group_update_started_cb   (GObject *manager_obj, Module *module);
static void name_group_update_finished_cb  (GObject *manager_obj, Module *module);
static void name_group_free_mod_data       (Module *module);

static void name_group_do_add_obj          (Module *module, GObject *added_obj);
static void name_group_do_remove_obj       (Module *module, GObject *removed_obj);
static void name_group_do_update_obj       (Module *module, GObject *updated_obj);

/*
 * Initial filling of the model
 */
static void
name_group_init_model_fill (Module *module, GtkTreeModel *model)
{
	GSList *list, *ptl;
	const gchar *current_name = "";
	GtkTreeIter iter;
	GtkTreeIter cat_iter, *cat_iter_ptr = NULL;
	
	list = GROUP_DATA (module)->get_objects_list (module);
	ptl = list;
	while (ptl) {
		gchar *str1;
		const gchar *str2, *str3;

		if (strcmp (current_name, mg_base_get_name (MG_BASE (ptl->data)))) {
			GSList *next_obj = g_slist_next (ptl);
			/* function with a different name */
			current_name = mg_base_get_name (MG_BASE (ptl->data));
			
			if (next_obj &&
			    !strcmp (mg_base_get_name (MG_BASE (next_obj->data)), current_name)) {
				/* a new category is needed */
				gtk_tree_store_append (GTK_TREE_STORE (model), &cat_iter, module->iter);
				gtk_tree_store_set (GTK_TREE_STORE (model), &cat_iter, 
						    NAME_COLUMN, current_name, 
						    CONTENTS_COLUMN, CONTENTS_GROUP_CATEGORY, 
						    SUB_MODULE_COLUMN, NULL, -1);
				cat_iter_ptr = &cat_iter;
			}
			else
				cat_iter_ptr = NULL;
		}
		
		if (cat_iter_ptr)
			gtk_tree_store_append (GTK_TREE_STORE (model), &iter, cat_iter_ptr);
		else
			gtk_tree_store_append (GTK_TREE_STORE (model), &iter, module->iter);

		str1 = GROUP_DATA (module)->get_extended_name (G_OBJECT (ptl->data));
		str2 = mg_base_get_owner (MG_BASE (ptl->data));
		str3 = mg_base_get_description (MG_BASE (ptl->data));
		gtk_tree_store_set (GTK_TREE_STORE (model), &iter, 
				    NAME_COLUMN, str1,
				    OWNER_COLUMN, str2, 
				    DESCR_COLUMN, str3, 
				    PIXBUF_COLUMN, GROUP_DATA (module)->obj_pixbuf,
				    OBJ_COLUMN, ptl->data, 
				    CONTENTS_COLUMN, CONTENTS_OBJECT, 
				    SUB_MODULE_COLUMN, NULL, -1);
		model_store_data (module, &iter);
		g_free (str1);
		
		/* add sub modules if necessary */
		if (module->obj_manager) {
			Module *sub_module;
			
			sub_module = (module->obj_manager) (module, &iter, G_OBJECT (ptl->data));
			if (sub_module) {
				sub_module->parent_module = module;
				(sub_module->fill_model) (sub_module);
				module->sub_modules = g_slist_append (module->sub_modules, sub_module);
				gtk_tree_store_set (GTK_TREE_STORE (model), &iter, 
						    SUB_MODULE_COLUMN, sub_module, -1);
			}
		}

		ptl = g_slist_next (ptl);
	}

	GROUP_DATA (module)->objects = (gpointer) list;
	GROUP_DATA (module)->iter_obj = NULL;
	GROUP_DATA (module)->iter = NULL;

	/* manager weak ref */
	g_object_weak_ref (G_OBJECT (GROUP_DATA (module)->manager),
			   (GWeakNotify) name_group_manager_weak_notify, module);
	GROUP_DATA (module)->manager_weak_refed = TRUE;
}

/*
 * Remove the module
 */
static void
name_group_manager_weak_notify (Module *module, GObject *manager_obj)
{
	GtkTreeModel *model;

	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	GROUP_DATA (module)->manager_weak_refed = FALSE;

	if (module->iter) {
		gtk_tree_store_remove (GTK_TREE_STORE (model), module->iter);
		gtk_tree_iter_free (module->iter);
		module->iter = NULL;
	}
	else 
		gtk_tree_store_clear (GTK_TREE_STORE (model));

	(module->free) (module);

	if (module->parent_module)
		module->parent_module->sub_modules = g_slist_remove (module->parent_module->sub_modules, module);
	else
		module->selector->priv->modules = g_slist_remove (module->selector->priv->modules, module);
	g_free (module);
}

/*
 * To be connected to the object which signals the creation of a new object which is to be
 * added to the data model
 */
static void
name_group_obj_added_cb (GObject *manager_obj, GObject *added_obj, Module *module)
{
	GSList *tmplist;
	gint objpos;

	/* g_print ("ADDING obj %p of type %s ", added_obj, G_OBJECT_TYPE_NAME (added_obj)); */
	/* 	g_print (" NAME: %s\n", mg_base_get_name (MG_BASE (added_obj))); */
	

	/* module's objects list sync. */
	tmplist = GROUP_DATA (module)->get_objects_list (module);
	objpos = g_slist_index (tmplist, added_obj);
	GROUP_DATA (module)->objects = g_slist_insert (GROUP_DATA (module)->objects, added_obj, objpos);
	g_slist_free (tmplist);

	if (GROUP_DATA (module)->iter) {
		/* need to reset iter ? */
		if (GROUP_DATA (module)->iter_obj) {
			gpointer obj;
			GtkTreeModel *model;
			
			model = gtk_tree_view_get_model (module->selector->priv->treeview);
			gtk_tree_model_get (model, GROUP_DATA (module)->iter, OBJ_COLUMN, &obj, -1);
			if (g_slist_index (GROUP_DATA (module)->objects, obj) >
			    g_slist_index (GROUP_DATA (module)->objects, added_obj)) {
				gtk_tree_model_iter_children (model, GROUP_DATA (module)->iter, module->iter);
			}
		}
	}
	
	name_group_do_add_obj (module, added_obj);
}

/*
 * To be connected to the object which signals the removal of an object which is to be
 * removed the data model
 */
static void
name_group_obj_removed_cb (GObject *manager_obj, GObject *removed_obj, Module *module)
{
	if (GROUP_DATA (module)->iter) {
		/* need to reset iter ? */
		if (GROUP_DATA (module)->iter_obj) {
			gpointer obj;
			GtkTreeModel *model;

			model = gtk_tree_view_get_model (module->selector->priv->treeview);
			gtk_tree_model_get (model, GROUP_DATA (module)->iter, OBJ_COLUMN, &obj, -1);
			if (g_slist_index (GROUP_DATA (module)->objects, obj) > 
			    g_slist_index (GROUP_DATA (module)->objects, removed_obj)) {
				gtk_tree_model_iter_children (model, GROUP_DATA (module)->iter, module->iter);
			}
		}
	}
	
	name_group_do_remove_obj (module, removed_obj);

	/* module's objects list sync. */
	GROUP_DATA (module)->objects = g_slist_remove (GROUP_DATA (module)->objects, removed_obj);

}

/*
 * To be connected to the object which signals the update of an object
 */
static void
name_group_obj_updated_cb (GObject *manager_obj, GObject *upd_obj, Module *module)
{
	if (GROUP_DATA (module)->iter) {
		/* need to reset iter ? */
		if (GROUP_DATA (module)->iter_obj) {
			gpointer obj;
			GtkTreeModel *model;

			model = gtk_tree_view_get_model (module->selector->priv->treeview);
			gtk_tree_model_get (model, GROUP_DATA (module)->iter, OBJ_COLUMN, &obj, -1);
			if (g_slist_index (GROUP_DATA (module)->objects, obj) > 
			    g_slist_index (GROUP_DATA (module)->objects, upd_obj)) {
				gtk_tree_model_iter_children (model, GROUP_DATA (module)->iter, module->iter);
			}
		}
	}
	
	name_group_do_update_obj (module, upd_obj);
}



/*
 * To be used to declare a "Big update" has started. It is used for optimisation
 * and it is not necessary to use it.
 */
static void
name_group_update_started_cb (GObject *manager_obj, Module *module)
{
	GtkTreeModel *model;
	
	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	GROUP_DATA (module)->iter = g_new0 (GtkTreeIter, 1);
	gtk_tree_model_iter_children (model, GROUP_DATA (module)->iter, module->iter);
}

/*
 * To be used to declare a "Big update" has started. It is used for optimisation
 * and it is not necessary to use it.
 */
static void
name_group_update_finished_cb (GObject *manager_obj, Module *module)
{
	gtk_tree_iter_free (GROUP_DATA (module)->iter);
	GROUP_DATA (module)->iter = NULL;
	GROUP_DATA (module)->iter_obj = NULL;
}

/*
 * Real addition of the object to the model
 */
static void
name_group_do_add_obj (Module *module, GObject *added_obj)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	GObject *sibling_before = NULL, *sibling_after = NULL;
	gint objpos;
	const gchar *objname;
	gboolean insert_ok = FALSE;

	objname = mg_base_get_name (MG_BASE (added_obj));
	model = gtk_tree_view_get_model (module->selector->priv->treeview);


	/* finding the siblings of the new object */
	objpos = g_slist_index (GROUP_DATA (module)->objects, added_obj);
	if (objpos > 0) {
		sibling_before = g_slist_nth_data (GROUP_DATA (module)->objects, objpos - 1);
		if (strcmp (mg_base_get_name (MG_BASE (sibling_before)), objname))
			sibling_before = NULL;
	}

	sibling_after = g_slist_nth_data (GROUP_DATA (module)->objects, objpos + 1);
	if (sibling_after) {
		if (strcmp (mg_base_get_name (MG_BASE (sibling_after)), objname))
			sibling_after = NULL;
	}

	if (!sibling_before && !sibling_after) {
		/* NON "polymorphic" object */
		if (objpos == 0) {
			gtk_tree_store_prepend (GTK_TREE_STORE (model), &iter, module->iter);
			if (GROUP_DATA (module)->iter) {
				*GROUP_DATA (module)->iter = iter;
				GROUP_DATA (module)->iter_obj = G_OBJECT (added_obj);
			}
			insert_ok = TRUE;
		}
		else {
			GObject *obj_before;
			
			obj_before = g_slist_nth_data (GROUP_DATA (module)->objects, objpos - 1);
			if (!GROUP_DATA (module)->iter)
				gtk_tree_model_iter_children (model, &iter, module->iter);
			else
				iter = *GROUP_DATA (module)->iter;

			if (set_iter_position (model, GROUP_DATA (module)->objects, obj_before, NULL, &iter)) {
				GtkTreeIter tmpiter;

				/* if the previous object is a polymorphic object, then
				   we need to create a node after its parent and not after
				   the current iterator */
				if (gtk_tree_model_iter_parent (model, &tmpiter, &iter)) {
					gint contents_id;
					
					gtk_tree_model_get (model, &tmpiter, 
							    CONTENTS_COLUMN, &contents_id, -1);
					if (contents_id == CONTENTS_GROUP_CATEGORY) 
						iter = tmpiter;
				}

				gtk_tree_store_insert_after (GTK_TREE_STORE (model), &tmpiter, 
							     module->iter, &iter);
				iter = tmpiter;
				insert_ok = TRUE;
			}
			else
				g_error ("Can't set iter at %s, line %d\n", __FUNCTION__, __LINE__);
		}
	}
	else {
		/* "polymorphic" object */
		if (sibling_before && sibling_after) {
			if (!GROUP_DATA (module)->iter)
				gtk_tree_model_iter_children (model, &iter, module->iter);
			else
				iter = *GROUP_DATA (module)->iter;

			if (set_iter_position (model, GROUP_DATA (module)->objects, 
					       sibling_before, NULL, &iter)) {
				GtkTreeIter tmpiter;
				
				gtk_tree_store_insert_after (GTK_TREE_STORE (model), &tmpiter, 
							     NULL, &iter);
				iter = tmpiter;
				insert_ok = TRUE;
			}
			else
				g_error ("Can't set iter at %s, line %d\n", __FUNCTION__, __LINE__);
		}
		else {
			GObject *sibling_obj;
			gpointer missing = NULL;

			if (sibling_before) 
				sibling_obj = sibling_before;
			else {
				sibling_obj = sibling_after;
				missing = added_obj;
			}
			
			if (!GROUP_DATA (module)->iter)
				gtk_tree_model_iter_children (model, &iter, module->iter);
			else
				iter = *GROUP_DATA (module)->iter;

			/* sets iter to point to sibling_obj */
			if (set_iter_position (model, GROUP_DATA (module)->objects, 
					       sibling_obj, missing, &iter)) {
				GtkTreeIter tmpiter;
				gboolean make_new_cat = FALSE;
				
				/* do we alreday have a object category or do we need one? */
				if (gtk_tree_model_iter_parent (model, &tmpiter, &iter)) {
					gint contents_id;
					
					gtk_tree_model_get (model, &tmpiter, 
							    CONTENTS_COLUMN, &contents_id, -1);
					if (contents_id != CONTENTS_GROUP_CATEGORY)
						make_new_cat = TRUE;
				}
				else
					make_new_cat = TRUE;
				
				if (make_new_cat) { /* a new category is needed */
					GtkTreeIter cat_iter;
					gchar *str1;
					const gchar *str2, *str3;
					
					gtk_tree_store_insert_after (GTK_TREE_STORE (model), &cat_iter, 
								     NULL, &iter);
					tmpiter = cat_iter;
					
					gtk_tree_store_set (GTK_TREE_STORE (model), &cat_iter, NAME_COLUMN, 
							    mg_base_get_name (MG_BASE (added_obj)), 
							    CONTENTS_COLUMN, CONTENTS_GROUP_CATEGORY, 
							    SUB_MODULE_COLUMN, NULL, -1);
					gtk_tree_store_remove (GTK_TREE_STORE (model), &iter);
					gtk_tree_store_append (GTK_TREE_STORE (model), &iter, &cat_iter);
					
					str1 = GROUP_DATA (module)->get_extended_name (sibling_obj);
					str2 = mg_base_get_owner (MG_BASE (sibling_obj));
					str3 = mg_base_get_description (MG_BASE (sibling_obj));
					gtk_tree_store_set (GTK_TREE_STORE (model), &iter,
							    NAME_COLUMN, str1, 
							    OWNER_COLUMN, str2, 
							    DESCR_COLUMN, str3, 
							    PIXBUF_COLUMN, 
							    GROUP_DATA (module)->obj_pixbuf,
							    OBJ_COLUMN, sibling_obj, 
							    CONTENTS_COLUMN, CONTENTS_OBJECT, 
							    SUB_MODULE_COLUMN, NULL, -1);
					model_store_data (module, &iter);
					g_free (str1);
				}
				
				/* here iter points to the sibling and tmpiter to the parent category */
				if (sibling_before)
					gtk_tree_store_insert_after (GTK_TREE_STORE (model), &tmpiter, 
								     &tmpiter, &iter);
				else
					gtk_tree_store_insert_before (GTK_TREE_STORE (model), &tmpiter, 
								      &tmpiter, &iter);
				iter = tmpiter;
				insert_ok = TRUE;
			}
			else
				g_error ("Can't set iter at %s, line %d\n", __FUNCTION__, __LINE__);
		}
	}
	
	if (insert_ok) {
		/* set the columns values */
		gchar *str1;
		const gchar *str2, *str3;

		str1 = GROUP_DATA (module)->get_extended_name (added_obj);
		str2 = mg_base_get_owner (MG_BASE (added_obj));
		str3 = mg_base_get_description (MG_BASE (added_obj));
		gtk_tree_store_set (GTK_TREE_STORE (model), &iter, 
				    NAME_COLUMN, str1,
				    OWNER_COLUMN, str2,
				    DESCR_COLUMN, str3, 
				    PIXBUF_COLUMN, GROUP_DATA (module)->obj_pixbuf,
				    OBJ_COLUMN, added_obj, 
				    CONTENTS_COLUMN, CONTENTS_OBJECT, 
				    SUB_MODULE_COLUMN, NULL, -1);
		model_store_data (module, &iter);
		g_free (str1);
		if (GROUP_DATA (module)->iter) {
			*GROUP_DATA (module)->iter = iter;
			GROUP_DATA (module)->iter_obj = G_OBJECT (added_obj);
		}

		/* add sub modules if necessary */
		if (module->obj_manager) {
			Module *sub_module;

			sub_module = (module->obj_manager) (module, &iter, G_OBJECT (added_obj));
			if (sub_module) {
				sub_module->parent_module = module;
				(sub_module->fill_model) (sub_module);
				module->sub_modules = g_slist_append (module->sub_modules, sub_module);
				gtk_tree_store_set (GTK_TREE_STORE (model), &iter, 
						    SUB_MODULE_COLUMN, sub_module, -1);
			}
		}
	}

}

/*
 * Real deletion from the object to the model
 */
static void
name_group_do_remove_obj (Module *module, GObject *removed_obj)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	model = gtk_tree_view_get_model (module->selector->priv->treeview);

	if (!GROUP_DATA (module)->iter) 
		gtk_tree_model_iter_children (model, &iter, module->iter);
	else 
		iter = *GROUP_DATA (module)->iter;
	
	if (set_iter_position (model, GROUP_DATA (module)->objects, removed_obj, NULL, &iter)) {
		GtkTreeIter tmpiter;
		Module *sub_module;

		/* Remove associated sub module if necessary */
		gtk_tree_model_get (model, &iter, SUB_MODULE_COLUMN, &sub_module, -1);
		if (sub_module) {
			g_assert (g_slist_find (module->sub_modules, sub_module));
			(sub_module->free) (sub_module);
			module->sub_modules = g_slist_remove (module->sub_modules, sub_module);
			g_free (sub_module);
		}

		if (gtk_tree_model_iter_parent (model, &tmpiter, &iter)) {
			gint contents_id;
			gtk_tree_model_get (model, &tmpiter, CONTENTS_COLUMN, &contents_id, -1);
			if ((contents_id == CONTENTS_GROUP_CATEGORY) && 
			    (gtk_tree_model_iter_n_children (model, &tmpiter) == 1))
				iter = tmpiter;
		}

/* FIXME: for GTK+ 2.0.x, gtk_tree_store_remove() does not return any value,
 * and for GTK+ 2.2.x it returns TRUE if the given iter is still valid after returning
 * This is for GTK+ 2.0.x, for 2.2.x, we need to define a GTK22x flag */
#ifdef GTK22x
		if (gtk_tree_store_remove (GTK_TREE_STORE (model), &iter)) {
#else
			gtk_tree_store_remove (GTK_TREE_STORE (model), &iter);
#endif
			if (GROUP_DATA (module)->iter) {
				gpointer obj;

				gtk_tree_model_get (model, &iter, OBJ_COLUMN, &obj, -1);
				if (!obj) {
					if (move_iter_to_next_leaf (model, &iter)) {
						gtk_tree_model_get (model, &iter, OBJ_COLUMN, &obj, -1);
						*GROUP_DATA (module)->iter = iter;
						GROUP_DATA (module)->iter_obj = G_OBJECT (obj);
					}
					else
						GROUP_DATA (module)->iter_obj = NULL;
				}
				else {
					*GROUP_DATA (module)->iter = iter;
					GROUP_DATA (module)->iter_obj = G_OBJECT (obj);
				}
			}
#ifdef GTK22x
		}
		else {
			if (GROUP_DATA (module)->iter) 
				GROUP_DATA (module)->iter_obj = NULL;
		}
#endif

	}
	else
		g_warning ("Can't find right GtkTreeIter for object %p (%s)!",
			   removed_obj, 
			   removed_obj ? mg_base_get_name (MG_BASE (removed_obj)) : "NULL");
}

/*
 * Real update of the object to the model
 */
static void
name_group_do_update_obj (Module *module, GObject *upd_obj)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	if (!GROUP_DATA (module)->iter)
		gtk_tree_model_iter_children (model, &iter, module->iter);
	else
		iter = *GROUP_DATA (module)->iter;
		
	if (set_iter_position (model, GROUP_DATA (module)->objects, upd_obj, NULL, &iter)) {
		/* set the columns values */
		gchar *str1;
		const gchar *str2, *str3;

		str1 = GROUP_DATA (module)->get_extended_name (upd_obj);
		str2 = mg_base_get_owner (MG_BASE (upd_obj));
		str3 = mg_base_get_description (MG_BASE (upd_obj));
		gtk_tree_store_set (GTK_TREE_STORE (model), &iter, 
				    NAME_COLUMN, str1,
				    OWNER_COLUMN, str2,
				    DESCR_COLUMN, str3, 
				    PIXBUF_COLUMN, GROUP_DATA (module)->obj_pixbuf,
				    OBJ_COLUMN, upd_obj, CONTENTS_COLUMN, CONTENTS_OBJECT, -1);
		model_store_data (module, &iter);
		g_free (str1);
		if (GROUP_DATA (module)->iter) {
			*GROUP_DATA (module)->iter = iter;
			GROUP_DATA (module)->iter_obj = G_OBJECT (upd_obj);
		}
	}

}


/*
 * Does memory deallocation for data specific to the group model.
 */
static void
name_group_free_mod_data (Module *module)
{
	if (GROUP_DATA (module)->objects) {
		g_slist_free (GROUP_DATA (module)->objects);
		GROUP_DATA (module)->objects = NULL;
	}

	if (GROUP_DATA (module)->iter) {
		gtk_tree_iter_free (GROUP_DATA (module)->iter);
		GROUP_DATA (module)->iter = NULL;
	}

	if (GROUP_DATA (module)->obj_pixbuf) {
		g_object_unref (G_OBJECT (GROUP_DATA (module)->obj_pixbuf));
		GROUP_DATA (module)->obj_pixbuf = NULL;
	}

	/* manager weak unref */
	if (GROUP_DATA (module)->manager_weak_refed) {
		g_object_weak_unref (G_OBJECT (GROUP_DATA (module)->manager),
				     (GWeakNotify) name_group_manager_weak_notify, module);
		GROUP_DATA (module)->manager_weak_refed = FALSE;
	}
}












/*
 * Generic module model where objects are listed directly in the order of the provided
 * list of objects
 */
typedef struct {
	GSList      *objects;
	GdkPixbuf   *obj_pixbuf;
	
	GObject     *manager;
	gboolean     manager_weak_refed;

	GSList    *(*get_objects_list) (Module *module);
} ModFlatData;
#define FLAT_DATA(x) ((ModFlatData *)(x->mod_data))

static void flat_init_model_fill     (Module *module, GtkTreeModel *model);
static void flat_manager_weak_notify (Module *module, GObject *manager_obj);
static void flat_obj_added_cb        (GObject *manager_obj, GObject *added_obj, Module *module);
static void flat_obj_removed_cb      (GObject *manager_obj, GObject *removed_obj, Module *module);
static void flat_obj_updated_cb      (GObject *manager_obj, GObject *upd_obj, Module *module);
static void flat_free_mod_data       (Module *module);

static void flat_do_add_obj          (Module *module, GObject *added_obj);
static void flat_do_remove_obj       (Module *module, GObject *removed_obj);
static void flat_do_update_obj       (Module *module, GObject *updated_obj);

/*
 * Initial filling of the model
 */
static void
flat_init_model_fill (Module *module, GtkTreeModel *model)
{
	GSList *list, *ptl;
	GtkTreeIter iter;
	
	list = FLAT_DATA (module)->get_objects_list (module);
	ptl = list;
	while (ptl) {
		const gchar *str1, *str2, *str3;

		gtk_tree_store_append (GTK_TREE_STORE (model), &iter, module->iter);

		str1 = mg_base_get_name (MG_BASE (ptl->data));
		str2 = mg_base_get_owner (MG_BASE (ptl->data));
		str3 = mg_base_get_description (MG_BASE (ptl->data));
		gtk_tree_store_set (GTK_TREE_STORE (model), &iter, 
				    NAME_COLUMN, str1,
				    OWNER_COLUMN, str2, 
				    DESCR_COLUMN, str3, 
				    PIXBUF_COLUMN, FLAT_DATA (module)->obj_pixbuf,
				    OBJ_COLUMN, ptl->data, 
				    CONTENTS_COLUMN, CONTENTS_OBJECT, 
				    SUB_MODULE_COLUMN, NULL, -1);
		model_store_data (module, &iter);
		
		/* add sub modules if necessary */
		if (module->obj_manager) {
			Module *sub_module;
			
			sub_module = (module->obj_manager) (module, &iter, G_OBJECT (ptl->data));
			if (sub_module) {
				sub_module->parent_module = module;
				(sub_module->fill_model) (sub_module);
				module->sub_modules = g_slist_append (module->sub_modules, sub_module);
				gtk_tree_store_set (GTK_TREE_STORE (model), &iter, 
						    SUB_MODULE_COLUMN, sub_module, -1);
			}
		}

		ptl = g_slist_next (ptl);
	}

	FLAT_DATA (module)->objects = (gpointer) list;

	/* manager weak ref */
	if (FLAT_DATA (module)->manager) {
		g_object_weak_ref (G_OBJECT (FLAT_DATA (module)->manager),
				   (GWeakNotify) flat_manager_weak_notify, module);
		FLAT_DATA (module)->manager_weak_refed = TRUE;
	}
}

/*
 * Remove the module
 */
static void
flat_manager_weak_notify (Module *module, GObject *manager_obj)
{
	GtkTreeModel *model;

	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	FLAT_DATA (module)->manager_weak_refed = FALSE;

	if (module->iter) {
		gtk_tree_store_remove (GTK_TREE_STORE (model), module->iter);
		gtk_tree_iter_free (module->iter);
		module->iter = NULL;
	}
	else 
		gtk_tree_store_clear (GTK_TREE_STORE (model));

	(module->free) (module);

	if (module->parent_module)
		module->parent_module->sub_modules = g_slist_remove (module->parent_module->sub_modules, module);
	else
		module->selector->priv->modules = g_slist_remove (module->selector->priv->modules, module);
	g_free (module);
}

/*
 * To be connected to the object which signals the creation of a new object which is to be
 * added to the data model
 */
static void
flat_obj_added_cb (GObject *manager_obj, GObject *added_obj, Module *module)
{
	GSList *tmplist;
	gint objpos;

	/* module's objects list sync. */
	tmplist = FLAT_DATA (module)->get_objects_list (module);
	objpos = g_slist_index (tmplist, added_obj);
	FLAT_DATA (module)->objects = g_slist_insert (FLAT_DATA (module)->objects, added_obj, objpos);
	g_slist_free (tmplist);

	flat_do_add_obj (module, added_obj);
}

/*
 * To be connected to the object which signals the removal of an object which is to be
 * removed the data model
 */
static void
flat_obj_removed_cb (GObject *manager_obj, GObject *removed_obj, Module *module)
{
	flat_do_remove_obj (module, removed_obj);

	/* module's objects list sync. */
	FLAT_DATA (module)->objects = g_slist_remove (FLAT_DATA (module)->objects, removed_obj);

}

/*
 * To be connected to the object which signals the update of an object
 */
static void
flat_obj_updated_cb (GObject *manager_obj, GObject *upd_obj, Module *module)
{
	flat_do_update_obj (module, upd_obj);
}


/*
 * Real addition of the object to the model
 */
static void
flat_do_add_obj (Module *module, GObject *added_obj)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gint objpos;
	const gchar *str1, *str2, *str3;

	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	objpos = g_slist_index (FLAT_DATA (module)->objects, added_obj);
	
	/* test if the added object has been found. This is not always the case since the provided list
	   of objects may have filtered out some object. */
	if (objpos < 0) 
		return;

	gtk_tree_store_insert (GTK_TREE_STORE (model), &iter, module->iter, objpos);
	str1 = mg_base_get_name (MG_BASE (added_obj));
	str2 = mg_base_get_owner (MG_BASE (added_obj));
	str3 = mg_base_get_description (MG_BASE (added_obj));
	gtk_tree_store_set (GTK_TREE_STORE (model), &iter, 
			    NAME_COLUMN, str1,
			    OWNER_COLUMN, str2,
			    DESCR_COLUMN, str3, 
			    PIXBUF_COLUMN, FLAT_DATA (module)->obj_pixbuf,
			    OBJ_COLUMN, added_obj, 
			    CONTENTS_COLUMN, CONTENTS_OBJECT, 
			    SUB_MODULE_COLUMN, NULL, -1);
	model_store_data (module, &iter);

	/* add sub modules if necessary */
	if (module->obj_manager) {
		Module *sub_module;
		
		sub_module = (module->obj_manager) (module, &iter, G_OBJECT (added_obj));
		if (sub_module) {
			sub_module->parent_module = module;
			(sub_module->fill_model) (sub_module);
			module->sub_modules = g_slist_append (module->sub_modules, sub_module);
			gtk_tree_store_set (GTK_TREE_STORE (model), &iter, 
					    SUB_MODULE_COLUMN, sub_module, -1);
		}
	}
}

/*
 * Real deletion from the object to the model
 */
static void
flat_do_remove_obj (Module *module, GObject *removed_obj)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gint objpos;

	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	objpos = g_slist_index (FLAT_DATA (module)->objects, removed_obj);

	/* test if the removed object has been found. This is not always the case since the provided list
	   of objects may have filtered out some object. */
	if (objpos < 0) 
		return;

	if (gtk_tree_model_iter_nth_child (model, &iter, module->iter, objpos)) {
		Module *sub_module;

		/* Remove associated sub module if necessary */
		gtk_tree_model_get (model, &iter, SUB_MODULE_COLUMN, &sub_module, -1);
		if (sub_module) {
			g_assert (g_slist_find (module->sub_modules, sub_module));
			(sub_module->free) (sub_module);
			module->sub_modules = g_slist_remove (module->sub_modules, sub_module);
			g_free (sub_module);
		}

		gtk_tree_store_remove (GTK_TREE_STORE (model), &iter);
	}
	else
		g_warning ("Can't find right GtkTreeIter for object %p (%s) at position %d!",
			   removed_obj, 
			   removed_obj ? mg_base_get_name (MG_BASE (removed_obj)) : "NULL", objpos);
}

/*
 * Real update of the object to the model
 */
static void
flat_do_update_obj (Module *module, GObject *upd_obj)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gint objpos;

	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	objpos = g_slist_index (FLAT_DATA (module)->objects, upd_obj);

	/* test if the updated object has been found. This is not always the case since the provided list
	   of objects may have filtered out some object. */
	if (objpos < 0) 
		return;

	if (gtk_tree_model_iter_nth_child (model, &iter, module->iter, objpos)) {
		const gchar *str1, *str2, *str3;
		
		str1 = mg_base_get_name (MG_BASE (upd_obj));
		str2 = mg_base_get_owner (MG_BASE (upd_obj));
		str3 = mg_base_get_description (MG_BASE (upd_obj));
		gtk_tree_store_set (GTK_TREE_STORE (model), &iter, 
				    NAME_COLUMN, str1,
				    OWNER_COLUMN, str2,
				    DESCR_COLUMN, str3, 
				    PIXBUF_COLUMN, FLAT_DATA (module)->obj_pixbuf,
				    OBJ_COLUMN, upd_obj, CONTENTS_COLUMN, CONTENTS_OBJECT, -1);
		model_store_data (module, &iter);
		
	}
	else
		g_warning ("Can't find right GtkTreeIter for object %p (%s) at position %d!",
			   upd_obj, 
			   upd_obj ? mg_base_get_name (MG_BASE (upd_obj)) : "NULL", objpos);
}


/*
 * Does memory deallocation for data specific to the group model.
 */
static void
flat_free_mod_data (Module *module)
{
	if (FLAT_DATA (module)->objects) {
		g_slist_free (FLAT_DATA (module)->objects);
		FLAT_DATA (module)->objects = NULL;
	}

	if (FLAT_DATA (module)->obj_pixbuf) {
		g_object_unref (G_OBJECT (FLAT_DATA (module)->obj_pixbuf));
		FLAT_DATA (module)->obj_pixbuf = NULL;
	}

	/* manager weak unref */
	if (FLAT_DATA (module)->manager_weak_refed) {
		g_object_weak_unref (G_OBJECT (FLAT_DATA (module)->manager),
				     (GWeakNotify) flat_manager_weak_notify, module);
		FLAT_DATA (module)->manager_weak_refed = FALSE;
	}
}











/*
 *
 * Module for data types
 *
 */
static void         module_data_types_fill_model (Module *module);
static void         module_data_types_free (Module *module);
static const gchar *module_data_types_col_name (Module *module, guint colno);

static Module *
module_data_types_new (MgSelector *mgsel, gboolean insert_header, GtkTreeIter *iter, gpointer data)
{
	Module *module;

	module = g_new0 (Module, 1);
	module->selector = mgsel;
	module->fill_model = module_data_types_fill_model;
	module->free = module_data_types_free;
	module->col_name = module_data_types_col_name;
	module->obj_manager = NULL;
	module->model_store_data = NULL;
	module->mod_data = NULL;
	module->iter = NULL;
	module->parent_module = NULL;
	module->sub_modules = NULL;

	if (insert_header) {
		GdkPixbuf *pixbuf = NULL;
		GtkTreeModel *model = gtk_tree_view_get_model (mgsel->priv->treeview);

		pixbuf = gnome_db_stock_get_icon_pixbuf (GNOME_DB_STOCK_TYPES);

		module->iter = g_new0 (GtkTreeIter, 1);
		gtk_tree_store_append (GTK_TREE_STORE (model), module->iter, iter);
		gtk_tree_store_set (GTK_TREE_STORE (model), module->iter, NAME_COLUMN, 
				    _("Data Types"), PIXBUF_COLUMN, pixbuf, 
				    CONTENTS_COLUMN, CONTENTS_TOP_CATEGORY, 
				    SUB_MODULE_COLUMN, NULL, -1);		
	}
	else {
		if (iter)
			module->iter = gtk_tree_iter_copy (iter);
	}

	return module;
}

static GSList *module_data_types_get_objects_list (Module *module);
static gchar  *module_data_types_get_extended_name (GObject *obj);
static void
module_data_types_fill_model (Module *module)
{
	MgServer *srv;
	GtkTreeModel *model;
	GdkPixbuf *pixbuf_func = NULL;

	srv = mg_conf_get_server (module->selector->priv->conf);
	pixbuf_func = gnome_db_stock_get_icon_pixbuf_file ("gnome-db-types_16x16.png");

	/* Module's private data */
	module->mod_data = g_new0 (ModNameGroupData, 1);
	GROUP_DATA (module)->manager = G_OBJECT (srv);
	GROUP_DATA (module)->manager_weak_refed = FALSE;
	GROUP_DATA (module)->obj_pixbuf = pixbuf_func;
	GROUP_DATA (module)->get_objects_list = module_data_types_get_objects_list;
	GROUP_DATA (module)->get_extended_name = module_data_types_get_extended_name;
	
	/* Initial model filling */
	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	name_group_init_model_fill (module, model);

	/* Signals handlers */
	g_signal_connect (G_OBJECT (srv), "data_type_added",
			  G_CALLBACK (name_group_obj_added_cb), module);
	g_signal_connect (G_OBJECT (srv), "data_type_removed",
			  G_CALLBACK (name_group_obj_removed_cb), module);
	g_signal_connect (G_OBJECT (srv), "data_type_updated",
			  G_CALLBACK (name_group_obj_updated_cb), module);
	g_signal_connect (G_OBJECT (srv), "data_update_started",
			  G_CALLBACK (name_group_update_started_cb), module);
	g_signal_connect (G_OBJECT (srv), "data_update_finished",
			  G_CALLBACK (name_group_update_finished_cb), module);
}

static GSList *
module_data_types_get_objects_list (Module *module)
{
	g_return_val_if_fail (GROUP_DATA (module)->manager, NULL);
	g_return_val_if_fail (IS_MG_SERVER (GROUP_DATA (module)->manager), NULL);

	return mg_server_get_data_types (MG_SERVER (GROUP_DATA (module)->manager));
}

static gchar *
module_data_types_get_extended_name (GObject *obj)
{
	MgServerDataType *dt;

	g_return_val_if_fail (obj && IS_MG_SERVER_DATA_TYPE (obj), NULL);

	dt = MG_SERVER_DATA_TYPE (obj);

	return g_strdup (mg_server_data_type_get_sqlname (dt));
}

static void
module_data_types_free (Module *module)
{
	MgServer *srv;
	srv = mg_conf_get_server (module->selector->priv->conf);

	if (srv) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_obj_added_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_obj_removed_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_obj_updated_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_update_started_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_update_finished_cb), module);
	}
	if (module->iter)
		gtk_tree_iter_free (module->iter);
	name_group_free_mod_data (module);
	g_free (module->mod_data);
	module->mod_data = NULL;
}


static const gchar *
module_data_types_col_name (Module *module, guint colno)
{
	switch (colno) {
	case 0:
		return _("Data Type");
		break;
	default:
		return NULL;
		break;
	}
}




/*
 *
 * Module for functions
 *
 */
static void         module_functions_fill_model (Module *module);
static void         module_functions_free (Module *module);
static const gchar *module_functions_col_name (Module *module, guint colno);

static Module *
module_functions_new (MgSelector *mgsel, gboolean insert_header, 
		      GtkTreeIter *iter, gpointer data)
{
	Module *module;

	module = g_new0 (Module, 1);
	module->selector = mgsel;
	module->fill_model = module_functions_fill_model;
	module->free = module_functions_free;
	module->col_name = module_functions_col_name;
	module->obj_manager = NULL;
	module->model_store_data = NULL;
	module->mod_data = NULL;
	module->iter = NULL;
	module->parent_module = NULL;
	module->sub_modules = NULL;

	if (insert_header) {
		GdkPixbuf *pixbuf = NULL;
		GtkTreeModel *model = gtk_tree_view_get_model (mgsel->priv->treeview);

		pixbuf = gnome_db_stock_get_icon_pixbuf (GNOME_DB_STOCK_PROCEDURES);

		module->iter = g_new0 (GtkTreeIter, 1);
		gtk_tree_store_append (GTK_TREE_STORE (model), module->iter, iter);
		gtk_tree_store_set (GTK_TREE_STORE (model), module->iter, NAME_COLUMN, 
				    _("Functions"), PIXBUF_COLUMN, pixbuf, 
				    CONTENTS_COLUMN, CONTENTS_TOP_CATEGORY, 
				    SUB_MODULE_COLUMN, NULL, -1);		
	}
	else {
		if (iter)
			module->iter = gtk_tree_iter_copy (iter);
	}

	return module;	
}


static GSList *module_functions_get_objects_list (Module *module);
static gchar  *module_functions_get_extended_name (GObject *obj);
static void
module_functions_fill_model (Module *module)
{
	MgServer *srv;
	GtkTreeModel *model;
	GdkPixbuf *pixbuf_func = NULL;

	srv = mg_conf_get_server (module->selector->priv->conf);
	pixbuf_func = gnome_db_stock_get_icon_pixbuf_file ("gnome-db-procedures_16x16.png");

	/* Module's private data */
	module->mod_data = g_new0 (ModNameGroupData, 1);
	GROUP_DATA (module)->manager = G_OBJECT (srv);
	GROUP_DATA (module)->manager_weak_refed = FALSE;
	GROUP_DATA (module)->obj_pixbuf = pixbuf_func;
	GROUP_DATA (module)->get_objects_list = module_functions_get_objects_list;
	GROUP_DATA (module)->get_extended_name = module_functions_get_extended_name;
	
	/* Initial model filling */
	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	name_group_init_model_fill (module, model);

	/* Signals handlers */
	g_signal_connect (G_OBJECT (srv), "data_function_added",
			  G_CALLBACK (name_group_obj_added_cb), module);
	g_signal_connect (G_OBJECT (srv), "data_function_removed",
			  G_CALLBACK (name_group_obj_removed_cb), module);
	g_signal_connect (G_OBJECT (srv), "data_function_updated",
			  G_CALLBACK (name_group_obj_updated_cb), module);
	g_signal_connect (G_OBJECT (srv), "data_update_started",
			  G_CALLBACK (name_group_update_started_cb), module);
	g_signal_connect (G_OBJECT (srv), "data_update_finished",
			  G_CALLBACK (name_group_update_finished_cb), module);
}

static GSList *
module_functions_get_objects_list (Module *module)
{
	g_return_val_if_fail (GROUP_DATA (module)->manager, NULL);
	g_return_val_if_fail (IS_MG_SERVER (GROUP_DATA (module)->manager), NULL);

	return mg_server_get_functions (MG_SERVER (GROUP_DATA (module)->manager));
}

static gchar *
module_functions_get_extended_name (GObject *obj)
{
	MgServerFunction *func;
	GString *string;
	const GSList *args;
	gchar *retval;
	gboolean firstarg = TRUE;

	g_return_val_if_fail (obj && IS_MG_SERVER_FUNCTION (obj), NULL);

	func = MG_SERVER_FUNCTION (obj);
	string = g_string_new (mg_server_function_get_sqlname (MG_SERVER_FUNCTION (func)));
	args = mg_server_function_get_arg_types (func);
	g_string_append (string, " (");
	while (args) {
		if (firstarg)
			firstarg = FALSE;
		else
			g_string_append (string, ", ");
		g_string_append (string,
				 mg_server_data_type_get_sqlname (MG_SERVER_DATA_TYPE (args->data)));
		args = g_slist_next (args);
	}
	g_string_append (string, ")");
	retval = string->str;
	g_string_free (string, FALSE);

	return retval;
}

static void
module_functions_free (Module *module)
{
	MgServer *srv;
	srv = mg_conf_get_server (module->selector->priv->conf);

	if (srv) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_obj_added_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_obj_removed_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_obj_updated_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_update_started_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_update_finished_cb), module);
	}
	if (module->iter)
		gtk_tree_iter_free (module->iter);
	name_group_free_mod_data (module);
	g_free (module->mod_data);
	module->mod_data = NULL;
}


static const gchar *
module_functions_col_name (Module *module, guint colno)
{
	switch (colno) {
	case 0:
		return _("Function");
		break;
	default:
		return NULL;
		break;
	}
}




/*
 *
 * Module for aggregates
 *
 */
static void         module_aggregates_fill_model (Module *module);
static void         module_aggregates_free (Module *module);
static const gchar *module_aggregates_col_name (Module *module, guint colno);

static Module *
module_aggregates_new (MgSelector *mgsel, gboolean insert_header, 
		       GtkTreeIter *iter, gpointer data)
{
	Module *module;

	module = g_new0 (Module, 1);
	module->selector = mgsel;
	module->fill_model = module_aggregates_fill_model;
	module->free = module_aggregates_free;
	module->col_name = module_aggregates_col_name;
	module->obj_manager = NULL;
	module->model_store_data = NULL;
	module->mod_data = NULL;
	module->iter = NULL;
	module->parent_module = NULL;
	module->sub_modules = NULL;

	if (insert_header) {
		GdkPixbuf *pixbuf = NULL;
		GtkTreeModel *model = gtk_tree_view_get_model (mgsel->priv->treeview);

		pixbuf = gnome_db_stock_get_icon_pixbuf (GNOME_DB_STOCK_AGGREGATES);

		module->iter = g_new0 (GtkTreeIter, 1);
		gtk_tree_store_append (GTK_TREE_STORE (model), module->iter, iter);
		gtk_tree_store_set (GTK_TREE_STORE (model), module->iter, NAME_COLUMN, 
				    _("Aggregates"), PIXBUF_COLUMN, pixbuf, 
				    CONTENTS_COLUMN, CONTENTS_TOP_CATEGORY, 
				    SUB_MODULE_COLUMN, NULL, -1);		
	}
	else {
		if (iter)
			module->iter = gtk_tree_iter_copy (module->iter);
	}

	return module;
}


static GSList *module_aggregates_get_objects_list (Module *module);
static gchar  *module_aggregates_get_extended_name (GObject *obj);
static void
module_aggregates_fill_model (Module *module)
{
	MgServer *srv;
	GtkTreeModel *model;
	GdkPixbuf *pixbuf_agg = NULL;

	srv = mg_conf_get_server (module->selector->priv->conf);
	pixbuf_agg = gnome_db_stock_get_icon_pixbuf_file ("gnome-db-aggregates_16x16.png");

	/* Module's private data */
	module->mod_data = g_new0 (ModNameGroupData, 1);
	GROUP_DATA (module)->manager = G_OBJECT (srv);
	GROUP_DATA (module)->manager_weak_refed = FALSE;
	GROUP_DATA (module)->obj_pixbuf = pixbuf_agg;
	GROUP_DATA (module)->get_objects_list = module_aggregates_get_objects_list;
	GROUP_DATA (module)->get_extended_name = module_aggregates_get_extended_name;
	
	/* Initial model filling */
	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	name_group_init_model_fill (module, model);

	/* Signals handlers */
	g_signal_connect (G_OBJECT (srv), "data_aggregate_added",
			  G_CALLBACK (name_group_obj_added_cb), module);
	g_signal_connect (G_OBJECT (srv), "data_aggregate_removed",
			  G_CALLBACK (name_group_obj_removed_cb), module);
	g_signal_connect (G_OBJECT (srv), "data_aggregate_updated",
			  G_CALLBACK (name_group_obj_updated_cb), module);
	g_signal_connect (G_OBJECT (srv), "data_update_started",
			  G_CALLBACK (name_group_update_started_cb), module);
	g_signal_connect (G_OBJECT (srv), "data_update_finished",
			  G_CALLBACK (name_group_update_finished_cb), module);
}

static GSList *
module_aggregates_get_objects_list (Module *module)
{
	g_return_val_if_fail (GROUP_DATA (module)->manager, NULL);
	g_return_val_if_fail (IS_MG_SERVER (GROUP_DATA (module)->manager), NULL);

	return mg_server_get_aggregates (MG_SERVER (GROUP_DATA (module)->manager));
}

static gchar *
module_aggregates_get_extended_name (GObject *obj)
{
	MgServerAggregate *agg;
	GString *string;
	MgServerDataType *arg;
	gchar *retval;

	g_return_val_if_fail (obj && IS_MG_SERVER_AGGREGATE (obj), NULL);

	agg = MG_SERVER_AGGREGATE (obj);
	string = g_string_new (mg_server_aggregate_get_sqlname (MG_SERVER_AGGREGATE (agg)));
	arg = mg_server_aggregate_get_arg_type (agg);
	g_string_append (string, " (");
	if (arg)
		g_string_append (string, mg_server_data_type_get_sqlname (arg));
	else
		g_string_append (string, "*");
			
	g_string_append (string, ")");
	retval = string->str;
	g_string_free (string, FALSE);

	return retval;
}

static void
module_aggregates_free (Module *module)
{
	MgServer *srv;
	srv = mg_conf_get_server (module->selector->priv->conf);

	if (srv) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_obj_added_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_obj_removed_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_obj_updated_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_update_started_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
						      G_CALLBACK (name_group_update_finished_cb), module);
	}
	if (module->iter)
		gtk_tree_iter_free (module->iter);
	name_group_free_mod_data (module);
	g_free (module->mod_data);
	module->mod_data = NULL;
}


static const gchar *
module_aggregates_col_name (Module *module, guint colno)
{
	switch (colno) {
	case 0:
		return _("Aggregate");
		break;
	default:
		return NULL;
		break;
	}
}




/*
 *
 * Module for tables
 *
 */
static void         module_tables_fill_model (Module *module);
static void         module_tables_free (Module *module);
static const gchar *module_tables_col_name (Module *module, guint colno);
static Module      *module_tables_obj_manager (Module *module, GtkTreeIter *iter, GObject *object);
static void         module_tables_model_store_data (Module *module, GtkTreeIter *iter);

static Module *module_tables_new (MgSelector *mgsel, gboolean insert_header, 
				  GtkTreeIter *iter, gpointer data)
{
	Module *module;

	module = g_new0 (Module, 1);
	module->selector = mgsel;
	module->fill_model = module_tables_fill_model;
	module->free = module_tables_free;
	module->col_name = module_tables_col_name;
	module->obj_manager = module_tables_obj_manager;
	module->model_store_data = module_tables_model_store_data;
	module->mod_data = NULL;
	module->iter = NULL;
	module->parent_module = NULL;
	module->sub_modules = NULL;

	if (insert_header) {
		GdkPixbuf *pixbuf = NULL;
		GtkTreeModel *model = gtk_tree_view_get_model (mgsel->priv->treeview);

		pixbuf = gnome_db_stock_get_icon_pixbuf (GNOME_DB_STOCK_TABLES);
		module->iter = g_new0 (GtkTreeIter, 1);
		gtk_tree_store_append (GTK_TREE_STORE (model), module->iter, iter);
		gtk_tree_store_set (GTK_TREE_STORE (model), module->iter, NAME_COLUMN, 
				    _("Tables & views"), PIXBUF_COLUMN, pixbuf, 
				    CONTENTS_COLUMN, CONTENTS_TOP_CATEGORY, 
				    SUB_MODULE_COLUMN, NULL, -1);		
	}
	else {
		if (iter)
			module->iter = gtk_tree_iter_copy (iter);
	}

	return module;
}


static GSList *module_tables_get_objects_list (Module *module);
static gchar  *module_tables_get_extended_name (GObject *obj);
static void
module_tables_fill_model (Module *module)
{
	MgDatabase *db;
	GtkTreeModel *model;
	GdkPixbuf *pixbuf_func = NULL;

	db = mg_conf_get_database (module->selector->priv->conf);
	pixbuf_func = gnome_db_stock_get_icon_pixbuf_file ("gnome-db-tables_16x16.png");

	/* Module's private data */
	module->mod_data = g_new0 (ModNameGroupData, 1);
	GROUP_DATA (module)->manager = G_OBJECT (db);
	GROUP_DATA (module)->manager_weak_refed = FALSE;
	GROUP_DATA (module)->obj_pixbuf = pixbuf_func;
	GROUP_DATA (module)->get_objects_list = module_tables_get_objects_list;
	GROUP_DATA (module)->get_extended_name = module_tables_get_extended_name;
	
	/* Initial model filling */
	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	name_group_init_model_fill (module, model);

	/* Signals handlers */
	g_signal_connect (G_OBJECT (db), "table_added",
			  G_CALLBACK (name_group_obj_added_cb), module);
	g_signal_connect (G_OBJECT (db), "table_removed",
			  G_CALLBACK (name_group_obj_removed_cb), module);
	g_signal_connect (G_OBJECT (db), "table_updated",
			  G_CALLBACK (name_group_obj_updated_cb), module);
	g_signal_connect (G_OBJECT (db), "data_update_started",
			  G_CALLBACK (name_group_update_started_cb), module);
	g_signal_connect (G_OBJECT (db), "data_update_finished",
			  G_CALLBACK (name_group_update_finished_cb), module);
}

static GSList *
module_tables_get_objects_list (Module *module)
{
	g_return_val_if_fail (GROUP_DATA (module)->manager, NULL);
	g_return_val_if_fail (IS_MG_DATABASE (GROUP_DATA (module)->manager), NULL);

	return mg_database_get_tables (MG_DATABASE (GROUP_DATA (module)->manager));
}

static gchar *
module_tables_get_extended_name (GObject *obj)
{
	g_return_val_if_fail (obj && IS_MG_DB_TABLE (obj), NULL);

	return g_strdup (mg_base_get_name (MG_BASE (obj)));
}

static void
module_tables_free (Module *module)
{
	gpointer db = GROUP_DATA (module)->manager;
	GSList *list = module->sub_modules;

	/* Free the sub modules for individual tables contents */
	while (list) {
		(MODULE (list->data)->free) (MODULE (list->data));
		g_free (list->data);
		list = g_slist_next (list);
	}
	if (module->sub_modules) {
		g_slist_free (module->sub_modules);
		module->sub_modules = NULL;
	}

	/* free this module */
	if (db) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (db),
						      G_CALLBACK (name_group_obj_added_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (db),
						      G_CALLBACK (name_group_obj_removed_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (db),
						      G_CALLBACK (name_group_obj_updated_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (db),
						      G_CALLBACK (name_group_update_started_cb), module);
		g_signal_handlers_disconnect_by_func (G_OBJECT (db),
						      G_CALLBACK (name_group_update_finished_cb), module);
	}
	if (module->iter)
		gtk_tree_iter_free (module->iter);
	name_group_free_mod_data (module);
	g_free (module->mod_data);
	module->mod_data = NULL;
}


static const gchar *
module_tables_col_name (Module *module, guint colno)
{
	switch (colno) {
	case 0:
		return _("Table or View");
		break;
	case EXTRA1_COLUMN:
		return _("Type");
		break;
	default:
		return NULL;
		break;
	}
}


/*
 * module_tables_obj_manager
 *
 * This function is called when a new MgDbTable is created. It simply creates a new
 * Module (which will be appended to the list of modules of the module for the tables)
 * for the fields of that particular table.
 */
static Module *
module_tables_obj_manager (Module *module, GtkTreeIter *iter, GObject *object)
{
	Module *sub_module = NULL;

	g_assert (object && IS_MG_DB_TABLE (object));

	if (module->selector->priv->mode & MG_SELECTOR_FIELDS) 
		sub_module = module_onetable_new (module->selector, FALSE, iter, object);

	return sub_module;
}

static void
module_tables_model_store_data (Module *module, GtkTreeIter *iter)
{
	GObject *obj;
	GtkTreeModel *model;

	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	gtk_tree_model_get (model, iter, OBJ_COLUMN, &obj, -1);

	if (obj && IS_MG_DB_TABLE (obj)) {
		MgDbTable *table;
		const gchar *str1;

		table = MG_DB_TABLE (obj);

		if (mg_db_table_is_view (table))
			str1 = _("View");
		else
			str1 = _("Table");

		gtk_tree_store_set (GTK_TREE_STORE (model), iter, 
				    EXTRA1_COLUMN, str1,
				    -1);
	}
}







/*
 *
 * Modules for the fields of a given MgDbTable
 *
 */
static void         module_onetable_fill_model (Module *module);
static void         module_onetable_free (Module *module);
static const gchar *module_onetable_col_name (Module *module, guint colno);
static void         module_onetable_model_store_data (Module *module, GtkTreeIter *iter);

static GSList      *module_onetable_get_objects_list (Module *module);
static gchar       *module_onetable_get_extended_name (GObject *obj);

static Module *module_onetable_new (MgSelector *mgsel, gboolean insert_header, 
				    GtkTreeIter *iter, gpointer data)
{
	Module *module;
	MgDbTable *table;
	GdkPixbuf *pixbuf_func = NULL;

	g_assert (data && IS_MG_DB_TABLE (data));
	pixbuf_func = gnome_db_stock_get_icon_pixbuf_file ("gnome-db-onetable_16x16.png");

	/* module structure */
	table = MG_DB_TABLE (data);
	module = g_new0 (Module, 1);
	module->selector = mgsel;
	module->fill_model = module_onetable_fill_model;
	module->free = module_onetable_free;
	module->col_name = module_onetable_col_name;
	module->obj_manager = NULL;
	module->model_store_data = module_onetable_model_store_data;
	module->mod_data = NULL;
	module->iter = NULL;
	module->parent_module = NULL;
	module->sub_modules = NULL;

	/* Module's private data */
	module->mod_data = g_new0 (ModNameGroupData, 1);
	GROUP_DATA (module)->manager = G_OBJECT (table);
	GROUP_DATA (module)->manager_weak_refed = FALSE;
	GROUP_DATA (module)->obj_pixbuf = pixbuf_func;
	GROUP_DATA (module)->get_objects_list = module_onetable_get_objects_list;
	GROUP_DATA (module)->get_extended_name = module_onetable_get_extended_name;
	
	/* model settings */
	if (insert_header) {
		GdkPixbuf *pixbuf = NULL;
		GtkTreeModel *model = gtk_tree_view_get_model (mgsel->priv->treeview);

		module->iter = g_new0 (GtkTreeIter, 1);
		gtk_tree_store_append (GTK_TREE_STORE (model), module->iter, iter);
		gtk_tree_store_set (GTK_TREE_STORE (model), module->iter, 
				    NAME_COLUMN, mg_base_get_name (MG_BASE (table)), 
				    PIXBUF_COLUMN, pixbuf, 
				    CONTENTS_COLUMN, CONTENTS_TOP_CATEGORY, 
				    SUB_MODULE_COLUMN, NULL, -1);
	}
	else {
		if (iter)
			module->iter = gtk_tree_iter_copy (iter);
	}

	return module;	
}

static void module_onetable_constraint_any_cb (MgDatabase *db, MgDbConstraint *cstr, Module *module);
static void
module_onetable_fill_model (Module *module)
{
	GObject *manager, *db;
	GtkTreeModel *model;

	manager = GROUP_DATA (module)->manager;

	/* Initial model filling */
	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	name_group_init_model_fill (module, model);

	/* Signals handlers */
	g_signal_connect (manager, "field_added",
			  G_CALLBACK (name_group_obj_added_cb), module);
	g_signal_connect (manager, "field_removed",
			  G_CALLBACK (name_group_obj_removed_cb), module);
	g_signal_connect (manager, "field_updated",
			  G_CALLBACK (name_group_obj_updated_cb), module);
	/* REM: we don't need to take care of the "fields_order_changed" signal since the MgDbTable DOES NOT
	 emit it. */

	/* signals from the Database object */
	db = (GObject *) mg_db_table_get_database (MG_DB_TABLE (manager));
	g_assert (db && IS_MG_DATABASE (db));
	g_signal_connect (db, "constraint_added",
			  G_CALLBACK (module_onetable_constraint_any_cb), module);
	g_signal_connect (db, "constraint_removed",
			  G_CALLBACK (module_onetable_constraint_any_cb), module);
	g_signal_connect (db, "constraint_updated",
			  G_CALLBACK (module_onetable_constraint_any_cb), module);
}

static void
module_onetable_constraint_any_cb (MgDatabase *db, MgDbConstraint *cstr, Module *module)
{
	if (mg_db_constraint_get_table (cstr) == (MgDbTable *) (GROUP_DATA (module)->manager)) {
		GSList *fields, *list;

		fields = mg_entity_get_all_fields (MG_ENTITY (GROUP_DATA (module)->manager));
		list = fields;
		while (list) {
			if (mg_db_constraint_uses_field (cstr, MG_DB_FIELD (list->data)))
				name_group_obj_updated_cb (GROUP_DATA (module)->manager, G_OBJECT (list->data),
							   module);
			list = g_slist_next (list);
		}
		g_slist_free (fields);
	}
}

static GSList *
module_onetable_get_objects_list (Module *module)
{
	g_return_val_if_fail (GROUP_DATA (module)->manager, NULL);
	g_return_val_if_fail (IS_MG_DB_TABLE (GROUP_DATA (module)->manager), NULL);

	return mg_entity_get_all_fields (MG_ENTITY (GROUP_DATA (module)->manager));
}

static gchar *
module_onetable_get_extended_name (GObject *obj)
{
	g_return_val_if_fail (obj && IS_MG_DB_FIELD (obj), NULL);

	return g_strdup (mg_base_get_name (MG_BASE (obj)));
}

static void
module_onetable_free (Module *module)
{
	GObject *manager = GROUP_DATA (module)->manager;
	GObject *db;

	/* free this module */
	g_assert (manager);
	g_signal_handlers_disconnect_by_func (manager,
					      G_CALLBACK (name_group_obj_added_cb), module);
	g_signal_handlers_disconnect_by_func (manager,
					      G_CALLBACK (name_group_obj_removed_cb), module);
	g_signal_handlers_disconnect_by_func (manager,
					      G_CALLBACK (name_group_obj_updated_cb), module);
	
	db = (GObject *) mg_db_table_get_database (MG_DB_TABLE (manager));
	g_assert (db && IS_MG_DATABASE (db));
	g_signal_handlers_disconnect_by_func (db, 
					      G_CALLBACK (module_onetable_constraint_any_cb), module);
	g_signal_handlers_disconnect_by_func (db, 
					      G_CALLBACK (module_onetable_constraint_any_cb), module);
	g_signal_handlers_disconnect_by_func (db,
					      G_CALLBACK (module_onetable_constraint_any_cb), module);

	if (module->iter)
		gtk_tree_iter_free (module->iter);
	name_group_free_mod_data (module);
	g_free (module->mod_data);
	module->mod_data = NULL;
}


static const gchar *
module_onetable_col_name (Module *module, guint colno)
{
	switch (colno) {
	case 0:
		return _("Field");
		break;
	case EXTRA1_COLUMN:
		return _("Type");
		break;
	case EXTRA2_COLUMN:
		return _("Length");
		break;
	case EXTRA3_COLUMN:
		return _("Not NULL?");
		break;
	case EXTRA5_COLUMN:
		return _("Default value");
		break;
	default:
		return NULL;
		break;
	}
}

static void
module_onetable_model_store_data (Module *module, GtkTreeIter *iter)
{
	GObject *obj;
	GtkTreeModel *model;

	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	gtk_tree_model_get (model, iter, OBJ_COLUMN, &obj, -1);

	if (obj && IS_MG_DB_FIELD (obj)) {
		MgDbField *field;
		const gchar *str1;
		gchar *str2, *str3;
		gboolean bool;
		MgServerDataType *type;
		const GdaValue *value;
		gint length, scale;
		
		field = MG_DB_FIELD (obj);

		/* data type */
		type = mg_field_get_data_type (MG_FIELD (obj));
		if (type)
			str1 = mg_server_data_type_get_sqlname (type);
		else
			str1 = _("Unknown");
			
		/* length */
		length = mg_db_field_get_length (field);
		scale = mg_db_field_get_scale (field);
		if (length != -1) {
			if (scale != 0)
				str2 = g_strdup_printf ("(%d, %d)", length, scale);
			else
				str2 = g_strdup_printf ("%d", length);
		}
		else
			str2 = NULL;
		
		/* NOT NULL */
		bool = mg_db_field_is_null_allowed (field) ? FALSE : TRUE;
		
		/* Default value */
		value = mg_db_field_get_default_value (field);
		if (value)
			str3 = gda_value_stringify (value);
		else
			str3 = g_strdup ("");

		gtk_tree_store_set (GTK_TREE_STORE (model), iter, 
				    EXTRA1_COLUMN, str1,
				    EXTRA2_COLUMN, str2,
				    EXTRA3_COLUMN, bool, 
				    EXTRA4_COLUMN, TRUE, 
				    EXTRA5_COLUMN, str3,
				    -1);
		if (str2) g_free (str2);
		g_free (str3);
	}
}






/*
 *
 * Module for all queries managed by MgConf
 *
 */
static void         module_queries_fill_model (Module *module);
static void         module_queries_free (Module *module);
static const gchar *module_queries_col_name (Module *module, guint colno);
static Module      *module_queries_obj_manager (Module *module, GtkTreeIter *iter, GObject *object);
static void         module_queries_model_store_data (Module *module, GtkTreeIter *iter);

static const gchar *module_queries_render_query_type (MgQuery *query);
static const gchar *module_queries_render_qfield_type (MgQfield *field);

static Module *module_queries_new (MgSelector *mgsel, gboolean insert_header, 
				  GtkTreeIter *iter, gpointer data)
{
	Module *module;

	module = g_new0 (Module, 1);
	module->selector = mgsel;
	module->fill_model = module_queries_fill_model;
	module->free = module_queries_free;
	module->col_name = module_queries_col_name;
	module->obj_manager = module_queries_obj_manager;
	module->model_store_data = module_queries_model_store_data;
	module->mod_data = NULL;
	module->iter = NULL;
	module->parent_module = NULL;
	module->sub_modules = NULL;

	if (insert_header) {
		GdkPixbuf *pixbuf = NULL;
		GtkTreeModel *model = gtk_tree_view_get_model (mgsel->priv->treeview);

		pixbuf = gnome_db_stock_get_icon_pixbuf (GNOME_DB_STOCK_QUERY);
		module->iter = g_new0 (GtkTreeIter, 1);
		gtk_tree_store_append (GTK_TREE_STORE (model), module->iter, iter);
		gtk_tree_store_set (GTK_TREE_STORE (model), module->iter, NAME_COLUMN, 
				    _("Queries"), PIXBUF_COLUMN, pixbuf, 
				    CONTENTS_COLUMN, CONTENTS_TOP_CATEGORY, 
				    SUB_MODULE_COLUMN, NULL, -1);		
	}
	else {
		if (iter)
			module->iter = gtk_tree_iter_copy (iter);
	}

	return module;
}


static GSList *module_queries_get_objects_list (Module *module);
static void
module_queries_fill_model (Module *module)
{
	GtkTreeModel *model;
	GdkPixbuf *pixbuf_query = NULL;

	pixbuf_query = gnome_db_stock_get_icon_pixbuf_file ("gnome-db-query_16x16.png");

	/* Module's private data */
	module->mod_data = g_new0 (ModFlatData, 1);
	FLAT_DATA (module)->manager = NULL;
	FLAT_DATA (module)->manager_weak_refed = FALSE;
	FLAT_DATA (module)->obj_pixbuf = pixbuf_query;
	FLAT_DATA (module)->get_objects_list = module_queries_get_objects_list;

	/* Initial model filling */
	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	flat_init_model_fill (module, model);

	/* Signals handlers */
	g_signal_connect (G_OBJECT (module->selector->priv->conf), "query_added",
			  G_CALLBACK (flat_obj_added_cb), module);
	g_signal_connect (G_OBJECT (module->selector->priv->conf), "query_removed",
			  G_CALLBACK (flat_obj_removed_cb), module);
	/* FIXME: make a query updated signal */
}

static GSList *
module_queries_get_objects_list (Module *module)
{
	return mg_conf_get_queries (module->selector->priv->conf);
}

static void
module_queries_free (Module *module)
{
	GSList *list = module->sub_modules;

	/* Free the sub modules */
	while (list) {
		(MODULE (list->data)->free) (MODULE (list->data));
		g_free (list->data);
		list = g_slist_next (list);
	}
	if (module->sub_modules) {
		g_slist_free (module->sub_modules);
		module->sub_modules = NULL;
	}

	/* free this module */
	g_signal_handlers_disconnect_by_func (G_OBJECT (module->selector->priv->conf),
					      G_CALLBACK (flat_obj_added_cb), module);
	g_signal_handlers_disconnect_by_func (G_OBJECT (module->selector->priv->conf),
					      G_CALLBACK (flat_obj_removed_cb), module);

	if (module->iter)
		gtk_tree_iter_free (module->iter);

	flat_free_mod_data (module);
	g_free (module->mod_data);
	module->mod_data = NULL;
}


static const gchar *
module_queries_col_name (Module *module, guint colno)
{
	switch (colno) {
	case 0:
		return _("Query");
		break;
	case EXTRA1_COLUMN:
		return _("Type");
		break;
	default:
		return NULL;
		break;
	}
}

/*
 * module_queries_obj_manager
 *
 * This function is called when a new object is created (MgQuery). It simply creates a new
 * Module (which will be appended to the list of modules of the module for the queries) for 
 * for the contents of that new query.
 */
static Module *
module_queries_obj_manager (Module *module, GtkTreeIter *iter, GObject *object)
{
	Module *sub_module = NULL;

	g_assert (object && IS_MG_QUERY (object));

	sub_module = module_onequery_new (module->selector, FALSE, iter, object);

	return sub_module;
}

static void
module_queries_model_store_data (Module *module, GtkTreeIter *iter)
{
	GObject *obj;
	GtkTreeModel *model;

	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	gtk_tree_model_get (model, iter, OBJ_COLUMN, &obj, -1);

	if (obj && IS_MG_QUERY (obj)) {
		const gchar *str1;
		gtk_tree_store_set (GTK_TREE_STORE (model), iter, 
				    EXTRA1_COLUMN, module_queries_render_query_type (MG_QUERY (obj)),
				    -1);
		str1 = mg_base_get_name (MG_BASE (obj));
		if (!str1 || !(*str1)) {
			gtk_tree_store_set (GTK_TREE_STORE (model), iter, 
				    NAME_COLUMN, _("Query 2<no name>"),
				    -1);
		}
	}
}

static const gchar *
module_queries_render_query_type (MgQuery *query)
{
	switch (mg_query_get_query_type (query)) {
	case MG_QUERY_TYPE_SELECT:
		return _("Select");
	case MG_QUERY_TYPE_INSERT:
		return _("Insert");
	case MG_QUERY_TYPE_UPDATE:
		return _("Update");
	case MG_QUERY_TYPE_DELETE:
		return _("Delete");
	case MG_QUERY_TYPE_UNION:
		return _("Select (union)");
	case MG_QUERY_TYPE_INTERSECT:
		return _("Select (intersection)");
	case MG_QUERY_TYPE_EXCEPT:
		return _("Select (exception)");
	case MG_QUERY_TYPE_SQL:
		return _("SQL text");
	default:
		g_assert_not_reached ();
	}

	return NULL;
}

static const gchar *
module_queries_render_qfield_type (MgQfield *field)
{
	GType ftype = G_OBJECT_TYPE (field);

	if (ftype == MG_QF_ALL_TYPE)
		return _("entity.*");
	if (ftype == MG_QF_FIELD_TYPE)
		return _("entity.field");
	if (ftype == MG_QF_VALUE_TYPE) {
		if (mg_qf_value_is_parameter (MG_QF_VALUE (field)))
			return _("parameter");
		else
			return _("value");
	}

	/* for other types */
	TO_IMPLEMENT;
	return "???";
}



/*
 *
 * Modules for the fields of a given MgQuery
 *
 */
typedef struct {
	ModFlatData  data;
	GdkPixbuf   *field_pixbuf;
} ModOneQueryData;
#define QUERY_DATA(x) ((ModOneQueryData *)(x->mod_data))

static void         module_onequery_fill_model (Module *module);
static void         module_onequery_free (Module *module);
static const gchar *module_onequery_col_name (Module *module, guint colno);
static Module      *module_onequery_obj_manager (Module *module, GtkTreeIter *iter, GObject *object);
static void         module_onequery_model_store_data (Module *module, GtkTreeIter *iter);
static GSList      *module_onequery_get_objects_list (Module *module);

static Module *module_onequery_new (MgSelector *mgsel, gboolean insert_header, 
				    GtkTreeIter *iter, gpointer data)
{
	Module *module;
	MgQuery *query;
	GdkPixbuf *pixbuf_query = NULL;
	GdkPixbuf *pixbuf_field = NULL;

	g_assert (data && IS_MG_QUERY (data));
	pixbuf_query = gnome_db_stock_get_icon_pixbuf_file ("gnome-db-query_16x16.png");
	pixbuf_field = gnome_db_stock_get_icon_pixbuf_file ("gnome-db-field_16x16.png");

	/* module structure */
	query = MG_QUERY (data);
	module = g_new0 (Module, 1);
	module->selector = mgsel;
	module->fill_model = module_onequery_fill_model;
	module->free = module_onequery_free;
	module->col_name = module_onequery_col_name;
	module->obj_manager = module_onequery_obj_manager;
	module->model_store_data = module_onequery_model_store_data;
	module->mod_data = NULL;
	module->iter = NULL;
	module->parent_module = NULL;
	module->sub_modules = NULL;

	/* Module's private data */
	module->mod_data = g_new0 (ModOneQueryData, 1);
	FLAT_DATA (module)->manager = G_OBJECT (query);
	FLAT_DATA (module)->manager_weak_refed = FALSE;
	FLAT_DATA (module)->obj_pixbuf = pixbuf_query;
	FLAT_DATA (module)->get_objects_list = module_onequery_get_objects_list;
	QUERY_DATA (module)->field_pixbuf = pixbuf_field;

	/* model settings */
	if (insert_header) {
		GdkPixbuf *pixbuf = NULL;
		GtkTreeModel *model = gtk_tree_view_get_model (mgsel->priv->treeview);

		module->iter = g_new0 (GtkTreeIter, 1);
		gtk_tree_store_append (GTK_TREE_STORE (model), module->iter, iter);
		gtk_tree_store_set (GTK_TREE_STORE (model), module->iter, 
				    NAME_COLUMN, mg_base_get_name (MG_BASE (query)), 
				    PIXBUF_COLUMN, pixbuf, 
				    CONTENTS_COLUMN, CONTENTS_TOP_CATEGORY, 
				    SUB_MODULE_COLUMN, NULL, -1);
	}
	else {
		if (iter)
			module->iter = gtk_tree_iter_copy (iter);
	}

	return module;	
}

static void
module_onequery_fill_model (Module *module)
{
	GObject *manager;
	GtkTreeModel *model;

	manager = FLAT_DATA (module)->manager;

	/* Initial model filling */
	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	flat_init_model_fill (module, model);

	/* Signals handlers */
	g_signal_connect (manager, "field_added",
			  G_CALLBACK (flat_obj_added_cb), module);
	g_signal_connect (manager, "field_removed",
			  G_CALLBACK (flat_obj_removed_cb), module);
	g_signal_connect (manager, "field_updated",
			  G_CALLBACK (flat_obj_updated_cb), module);
	/* FIXME: we need to take care of the "fields_order_changed" signal. */

	g_signal_connect (manager, "sub_query_added",
			  G_CALLBACK (flat_obj_added_cb), module);
	g_signal_connect (manager, "sub_query_removed",
			  G_CALLBACK (flat_obj_removed_cb), module);
	g_signal_connect (manager, "sub_query_updated",
			  G_CALLBACK (flat_obj_updated_cb), module);
}

static GSList *
module_onequery_get_objects_list (Module *module)
{
	GSList *retval = NULL;
	g_return_val_if_fail (FLAT_DATA (module)->manager, NULL);
	g_return_val_if_fail (IS_MG_QUERY (FLAT_DATA (module)->manager), NULL);

	if (module->selector->priv->mode & MG_SELECTOR_QVIS_FIELDS) 
		retval = g_slist_concat (retval, mg_entity_get_visible_fields (MG_ENTITY (FLAT_DATA (module)->manager)));
	else
		if (module->selector->priv->mode & MG_SELECTOR_QALL_FIELDS) 
			retval = g_slist_concat (retval, mg_entity_get_all_fields (MG_ENTITY (FLAT_DATA (module)->manager)));

	if (module->selector->priv->mode & MG_SELECTOR_SUB_QUERIES) 
		retval = g_slist_concat (retval, mg_query_get_sub_queries (MG_QUERY (FLAT_DATA (module)->manager)));
	
	return retval;
}

static void
module_onequery_free (Module *module)
{
	GObject *manager = FLAT_DATA (module)->manager;
	GSList *list = module->sub_modules;

	g_assert (manager);

	/* free the extra pixbufs */
	if (QUERY_DATA (module)->field_pixbuf)
		g_object_unref (G_OBJECT (QUERY_DATA (module)->field_pixbuf));

	/* Free the sub modules */
	while (list) {
		(MODULE (list->data)->free) (MODULE (list->data));
		g_free (list->data);
		list = g_slist_next (list);
	}
	if (module->sub_modules) {
		g_slist_free (module->sub_modules);
		module->sub_modules = NULL;
	}


	/* free this module */
	g_signal_handlers_disconnect_by_func (manager,
					      G_CALLBACK (flat_obj_added_cb), module);
	g_signal_handlers_disconnect_by_func (manager,
					      G_CALLBACK (flat_obj_removed_cb), module);
	g_signal_handlers_disconnect_by_func (manager,
					      G_CALLBACK (flat_obj_updated_cb), module);

	if (module->iter)
		gtk_tree_iter_free (module->iter);
	flat_free_mod_data (module);
	g_free (module->mod_data);
	module->mod_data = NULL;
}


static const gchar *
module_onequery_col_name (Module *module, guint colno)
{
	switch (colno) {
	case 0:
		return _("Field");
		break;
	case EXTRA1_COLUMN:
		return _("Type");
		break;
	default:
		return NULL;
		break;
	}
}

/*
 * module_onequery_obj_manager
 *
 * This function is called when a new object is created (MgQuery). It simply creates a new
 * Module (which will be appended to the list of modules of the module for the queries) for 
 * for the contents of that new query.
 */
static Module *
module_onequery_obj_manager (Module *module, GtkTreeIter *iter, GObject *object)
{
	Module *sub_module = NULL;

	g_assert (object);

	if (IS_MG_QUERY (object)) {
		if (module->selector->priv->mode & MG_SELECTOR_QVIS_FIELDS) 
			sub_module = module_onequery_new (module->selector, FALSE, iter, object);
	}

	return sub_module;
}

static void
module_onequery_model_store_data (Module *module, GtkTreeIter *iter)
{
	GObject *obj;
	GtkTreeModel *model;

	model = gtk_tree_view_get_model (module->selector->priv->treeview);
	gtk_tree_model_get (model, iter, OBJ_COLUMN, &obj, -1);

	if (obj && IS_MG_QFIELD (obj)) {
		MgQfield *field;
		const gchar *str1, *str2;
		gchar *str3;
		MgServerDataType *type;
		
		field = MG_QFIELD (obj);

		/* data type */
		type = mg_field_get_data_type (MG_FIELD (obj));
		if (type)
			str1 = mg_server_data_type_get_sqlname (type);
		else
			str1 = _("-");
		/* other */
		str2 = module_queries_render_qfield_type (MG_QFIELD (obj));
		str3 = mg_renderer_render_as_str (MG_RENDERER (obj), NULL);
		
		gtk_tree_store_set (GTK_TREE_STORE (model), iter, 
				    EXTRA1_COLUMN, str1,
				    EXTRA6_COLUMN, str3,
				    EXTRA7_COLUMN, str2,
				    PIXBUF_COLUMN, QUERY_DATA (module)->field_pixbuf,
				    -1);
		if (str3)
			g_free (str3);

		str1 = mg_base_get_name (MG_BASE (field));
		if (!str1 || !(*str1)) {
			gtk_tree_store_set (GTK_TREE_STORE (model), iter, 
				    NAME_COLUMN, _("Field <no name>"),
				    -1);
		}
	}

	if (obj && IS_MG_QUERY (obj)) {
		const gchar *str1;
		gtk_tree_store_set (GTK_TREE_STORE (model), iter, 
				    EXTRA1_COLUMN, module_queries_render_query_type (MG_QUERY (obj)),
				    -1);
		str1 = mg_base_get_name (MG_BASE (obj));
		if (!str1 || !(*str1)) {
			gtk_tree_store_set (GTK_TREE_STORE (model), iter, 
				    NAME_COLUMN, _("Query <no name>"),
				    -1);
		}
	}
}


