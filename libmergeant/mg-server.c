/* mg-server.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-server.h"
#include "mg-xml-storage.h"
#include "marshal.h"
#include <gmodule.h>
#include "mg-result-set.h"
#include "mg-server-data-type.h"
#include "mg-server-function.h"
#include "mg-server-aggregate.h"
#include "handlers/mg-handler-string.h"
#include "handlers/mg-handler-numerical.h"
#include "handlers/mg-handler-boolean.h"
#include "handlers/mg-handler-time.h"
#include "handlers/mg-handler-none.h"
#include <string.h>
#include <dirent.h>

/* 
 * Main static functions 
 */
static void mg_server_class_init (MgServerClass * class);
static void mg_server_init (MgServer * srv);
static void mg_server_dispose (GObject   * object);
static void mg_server_finalize (GObject   * object);

static void mg_server_set_property (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec);
static void mg_server_get_property (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec);

static void        mg_server_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_server_get_xml_id (MgXmlStorage *iface);
static xmlNodePtr  mg_server_save_to_xml (MgXmlStorage *iface, GError **error);
static gboolean    mg_server_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error);

static GSList *handlers_get_initial_list (MgServer *srv);
static GSList *handlers_load_plugins (MgServer *srv, const gchar *path);

static void nullified_handler_cb (MgDataHandler *hdl, MgServer *srv);
static void nullified_data_type_cb (MgServerDataType *dt, MgServer *srv);
static void nullified_function_cb (MgServerFunction *func, MgServer *srv);
static void nullified_aggregate_cb (MgServerAggregate *agg, MgServer *srv);

static void updated_data_type_cb (MgServerDataType *dt, MgServer *srv);
static void updated_function_cb (MgServerFunction *func, MgServer *srv);
static void updated_aggregate_cb (MgServerAggregate *agg, MgServer *srv);


static MgServerFunction *mg_server_get_function_by_name_arg_real (GSList *functions, const gchar *funcname, const GSList *argtypes);
MgServerAggregate *mg_server_get_aggregate_by_name_arg_real (GSList *aggregates, const gchar *aggname, MgServerDataType *argtype);


#define HANDLER_FUNCTION(x) ((MgDataHandler *(*) (MgServer *, GObject *)) x)
static MgDataHandler *func_handler_data_types (MgServer *srv, GObject *obj);


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	CONN_OPENED,
	CONN_TO_CLOSE,
	CONN_CLOSED,
	DATA_TYPE_ADDED,
	DATA_TYPE_REMOVED,
	DATA_TYPE_UPDATED,
	DATA_FUNCTION_ADDED,
	DATA_FUNCTION_REMOVED,
	DATA_FUNCTION_UPDATED,
	DATA_AGGREGATE_ADDED,
	DATA_AGGREGATE_REMOVED,
	DATA_AGGREGATE_UPDATED,
	DATA_UPDATE_STARTED,
	UPDATE_PROGRESS,
	DATA_UPDATE_FINISHED,
	OBJECT_HANDLER_UPDATED,
	LAST_SIGNAL
};

static gint mg_server_signals[LAST_SIGNAL] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

/* properties */
enum
{
	PROP_0,
	PROP_WITH_FUNCTIONS
};

typedef struct _MgServerSupports MgServerSupports;

struct _MgServerSupports 
{
	gboolean sequences;
        gboolean procs;
        gboolean inheritance;
        gboolean xml_queries;
};


/* private structure */
struct _MgServerPrivate
{
	MgConf                 *conf;

	/* server description */
        MgServerSupports        features;
        GdaConnection          *cnc;
	gboolean                with_functions;

	/* DBMS update related information */
	gboolean                update_in_progress;
	gboolean                stop_update; /* TRUE if a DBMS data update must be stopped */

	/* DBMS connectivity */
        GString                *gda_datasource;
        GString                *user_name;
        GString                *password;

	/* DBMS objects */
	GSList                 *users;
        GSList                 *data_types;
        GSList                 *functions;
        GSList                 *aggregates;

	/* Data type handlers and plugins management */
        GSList                 *handlers;            /* list of DataHandler objects */
	MgDataHandler          *fallback_handler;    /* to be used when no other handler can do */
        GHashTable             *types_objects_hash;  /* hash to store the bindings of
							DataHandler objects to several objects */
        GSList                 *handlers_functions;
};


/* module error */
GQuark mg_server_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_server_error");
	return quark;
}


guint
mg_server_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgServerClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_server_class_init,
			NULL,
			NULL,
			sizeof (MgServer),
			0,
			(GInstanceInitFunc) mg_server_init
		};
		

		static const GInterfaceInfo xml_storage_info = {
			(GInterfaceInitFunc) mg_server_xml_storage_init,
			NULL,
			NULL
		};

		type = g_type_register_static (GDA_TYPE_CLIENT, "MgServer", &info, 0);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
	}
	return type;
}

static void 
mg_server_xml_storage_init (MgXmlStorageIface *iface)
{
	iface->get_xml_id = mg_server_get_xml_id;
	iface->save_to_xml = mg_server_save_to_xml;
	iface->load_from_xml = mg_server_load_from_xml;
}

static void
mg_server_class_init (MgServerClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_server_signals[CONN_OPENED] =
                g_signal_new ("conn_opened",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, conn_opened),
                              NULL, NULL,
                              marshal_VOID__VOID,
                              G_TYPE_NONE, 0);
        mg_server_signals[CONN_TO_CLOSE] =
                g_signal_new ("conn_to_close",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, conn_to_close),
                              NULL, NULL,
                              marshal_VOID__VOID,
                              G_TYPE_NONE, 0);
        mg_server_signals[CONN_CLOSED] =    /* runs after user handlers */
                g_signal_new ("conn_closed",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, conn_closed),
                              NULL, NULL,
                              marshal_VOID__VOID,
                              G_TYPE_NONE, 0);
        mg_server_signals[DATA_TYPE_ADDED] =
                g_signal_new ("data_type_added",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, data_type_added),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
	mg_server_signals[DATA_TYPE_REMOVED] =
                g_signal_new ("data_type_removed",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, data_type_removed),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
	mg_server_signals[DATA_TYPE_UPDATED] =
                g_signal_new ("data_type_updated",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, data_type_updated),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_server_signals[DATA_FUNCTION_ADDED] =
                g_signal_new ("data_function_added",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, data_function_added),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_server_signals[DATA_FUNCTION_REMOVED] =
                g_signal_new ("data_function_removed",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, data_function_removed),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_server_signals[DATA_FUNCTION_UPDATED] =
                g_signal_new ("data_function_updated",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, data_function_updated),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_server_signals[DATA_AGGREGATE_ADDED] =
                g_signal_new ("data_aggregate_added",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, data_aggregate_added),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_server_signals[DATA_AGGREGATE_REMOVED] =
                g_signal_new ("data_aggregate_removed",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, data_aggregate_removed),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_server_signals[DATA_AGGREGATE_UPDATED] =
                g_signal_new ("data_aggregate_updated",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, data_aggregate_updated),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
	mg_server_signals[DATA_UPDATE_STARTED] =
                g_signal_new ("data_update_started",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, data_update_started),
                              NULL, NULL,
                              marshal_VOID__VOID,
                              G_TYPE_NONE, 0);
        mg_server_signals[UPDATE_PROGRESS] =
                g_signal_new ("update_progress",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, update_progress),
                              NULL, NULL,
                              marshal_VOID__POINTER_UINT_UINT,
                              G_TYPE_NONE, 3, G_TYPE_POINTER, G_TYPE_UINT, G_TYPE_UINT);
	mg_server_signals[DATA_UPDATE_FINISHED] =
                g_signal_new ("data_update_finished",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, data_update_finished),
                              NULL, NULL,
                              marshal_VOID__VOID,
                              G_TYPE_NONE, 0);
	mg_server_signals[OBJECT_HANDLER_UPDATED] =
                g_signal_new ("object_handler_updated",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgServerClass, object_handler_updated),
                              NULL, NULL,
                              marshal_VOID__VOID,
                              G_TYPE_NONE, 0);

        class->conn_opened = NULL;
        class->conn_to_close = NULL;
        class->conn_closed = NULL;
        class->data_type_added = NULL;
        class->data_type_removed = NULL;
        class->data_type_updated = NULL;
        class->data_function_added = NULL;
        class->data_function_removed = NULL;
        class->data_function_updated = NULL;
        class->data_aggregate_added = NULL;
        class->data_aggregate_removed = NULL;
        class->data_aggregate_updated = NULL;
        class->data_update_started = NULL;
        class->data_update_finished = NULL;
        class->update_progress = NULL;
        class->object_handler_updated = NULL;


	object_class->dispose = mg_server_dispose;
	object_class->finalize = mg_server_finalize;

	/* Properties */
	object_class->set_property = mg_server_set_property;
	object_class->get_property = mg_server_get_property;
	g_object_class_install_property (object_class, PROP_WITH_FUNCTIONS,
					 g_param_spec_boolean ("with_functions", NULL, NULL, FALSE,
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}


static void
mg_server_init (MgServer * mg_server)
{
	GSList *plugins, *list;

	mg_server->priv = g_new0 (MgServerPrivate, 1);
	mg_server->priv->gda_datasource = g_string_new ("");
        mg_server->priv->user_name = g_string_new (getenv ("USER"));
        mg_server->priv->password = g_string_new ("");
        mg_server->priv->cnc = NULL;
	mg_server->priv->conf = NULL;
	mg_server->priv->with_functions = FALSE;

	mg_server->priv->update_in_progress = FALSE;
	mg_server->priv->stop_update = FALSE;

        /*
	 * creation of the data types known by the DBMS 
	 */
        mg_server->priv->data_types = NULL;
        mg_server->priv->functions = NULL;
        mg_server->priv->aggregates = NULL;


	/* 
	 * data handlers
	 */
        mg_server->priv->handlers = handlers_get_initial_list (mg_server);
        /* data handlers plugins */
        if (!g_module_supported ()) {
                g_warning ("GModule is required to use LibMergeant!");
                exit (1);
        }
	/* FIXME: also load plugins from a $HOME/.libmergeant location */
	plugins = handlers_load_plugins (mg_server, LIBMERGEANT_PLUGINSDIR);
	if (plugins) 
		mg_server->priv->handlers = g_slist_concat (mg_server->priv->handlers, plugins);
	list = mg_server->priv->handlers;
	while (list) {
		g_signal_connect (G_OBJECT (list->data), "nullified",
				  G_CALLBACK (nullified_handler_cb), mg_server);
		list = g_slist_next (list);
	}

	mg_server->priv->fallback_handler = MG_DATA_HANDLER (mg_handler_none_new (mg_server));
        mg_server->priv->types_objects_hash = g_hash_table_new (NULL, NULL);
        mg_server->priv->handlers_functions = NULL;

	/* registering functions for the handler finding */
	mg_server_set_object_func_handler (mg_server, func_handler_data_types);
	/* FIXME: Functions, Aggregates */

        /*
	 * init the features 
	 */
        mg_server->priv->features.sequences = FALSE;
        mg_server->priv->features.procs = FALSE;
        mg_server->priv->features.inheritance = FALSE;
        mg_server->priv->features.xml_queries = FALSE;
}

static void
nullified_handler_cb (MgDataHandler *hdl, MgServer *srv)
{
	g_return_if_fail (g_slist_find (srv->priv->handlers, hdl));
	srv->priv->handlers = g_slist_remove (srv->priv->handlers, hdl);
	g_object_unref (G_OBJECT (hdl));
}

static GSList *
handlers_get_initial_list (MgServer *srv)
{
	GSList *list = NULL;
	MgDataHandler *dh;

	/* Strings */
	dh = MG_DATA_HANDLER (mg_handler_string_new (srv));
	list = g_slist_append (list, dh);

	/* Numerical values */
	dh = MG_DATA_HANDLER (mg_handler_numerical_new (srv));
	list = g_slist_append (list, dh);

	/* Booleans */
	dh = MG_DATA_HANDLER (mg_handler_boolean_new (srv));
	list = g_slist_append (list, dh);

	/* Times, dates, ... */
	dh = MG_DATA_HANDLER (mg_handler_time_new (srv));
	list = g_slist_append (list, dh);

	return list;
}

static GSList *
handlers_load_plugins (MgServer *srv, const gchar *path)
{
        DIR *dstream;
	GSList *list = NULL;

        /* FIXME: it appears that the program hangs if we try to access
           not a lib but an executable program. maybe try to use the "standard"
           libtool calls. */
        dstream = opendir (path);
        if (!dstream) {
                g_print ("Cannot open %s\n", path);
        }
        else {
		struct dirent *dir;	

#ifdef debug
		g_print ("Looking for plugins in %s\n", path);
#endif
		dir = readdir (dstream);
                while (dir) {
                        if ((strlen (dir->d_name) > 3) &&       /* only .so files */
                            !strcmp (dir->d_name + strlen (dir->d_name) - 3, ".so")) {
				gchar *str;
                                GModule *lib;
                                MgDataHandler *hdl;
                                MgDataHandler *(*plugin_init) (MgServer *srv, GModule *module);

                                str = g_strdup_printf ("%s/%s", path, dir->d_name);
                                if ((lib = g_module_open (str, G_MODULE_BIND_LAZY))) {
                                        g_module_symbol (lib, "plugin_init", (gpointer *) &plugin_init);
                                        if (plugin_init) {
                                                hdl = (plugin_init) (srv, lib);
                                                list = g_slist_append (list, hdl);

#ifdef debug
						g_print ("\tLoaded '%s' from file %s\n",
							 mg_data_handler_get_plugin_name (hdl),
							 dir->d_name);
#endif

						/* we won't release this module.
						 * REM: we could make a hash table to release a module
						 * when the corresponding MgDataHandler is nullified
						 */
						g_module_make_resident (lib);
                                        }
#ifdef debug
                                        else {
						g_print ("\tFile %s does not have plugin_init()\n",
							 dir->d_name);
                                                g_module_close (lib);
                                        }
#endif
                                }
                                g_free (str);
                        }
                        dir = readdir (dstream);
                }
                closedir (dstream);
	}

	return list;
}

/**
 * mg_server_new
 * @conf: a #MgConf object
 * 
 * Creates a new MgServer object
 *
 * Returns: the new object
 */
GObject   *
mg_server_new (MgConf *conf)
{
	GObject   *obj;
	MgServer *mg_server;

	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);

	obj = g_object_new (MG_SERVER_TYPE, NULL);
	mg_server = MG_SERVER (obj);

	mg_server->priv->conf = conf;

	return obj;
}


static void
mg_server_dispose (GObject   * object)
{
	MgServer *mg_server;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_SERVER (object));

	mg_server = MG_SERVER (object);
	if (mg_server->priv) {
		mg_server_reset (mg_server);
		/* getting rid of the data handlers */
		if (mg_server->priv->fallback_handler) {
			mg_base_nullify (MG_BASE (mg_server->priv->fallback_handler));
			mg_server->priv->fallback_handler = NULL;
		}
		
		while (mg_server->priv->handlers) 
			mg_base_nullify (MG_BASE (mg_server->priv->handlers->data));
		
		/* getting rid of the hash table for handlers */
		if (mg_server->priv->types_objects_hash) {
			g_hash_table_destroy (mg_server->priv->types_objects_hash);
			mg_server->priv->types_objects_hash = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_server_finalize (GObject *object)
{
	MgServer *mg_server;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_SERVER (object));

	mg_server = MG_SERVER (object);
	if (mg_server->priv) {

		g_free (mg_server->priv);
		mg_server->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_server_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	MgServer *mg_server;

	mg_server = MG_SERVER (object);
	if (mg_server->priv) {
		switch (param_id) {
		case PROP_WITH_FUNCTIONS:
			mg_server->priv->with_functions = g_value_get_boolean (value);
			break;
		}
	}
}
static void
mg_server_get_property (GObject              *object,
			guint                 param_id,
			GValue               *value,
			GParamSpec           *pspec)
{
	MgServer *mg_server;
	mg_server = MG_SERVER (object);
	
	if (mg_server->priv) {
		switch (param_id) {
		case PROP_WITH_FUNCTIONS:
			g_value_set_boolean (value, mg_server->priv->with_functions);
			break;
		}	
	}
}


/* MgXmlStorage interface implementation */
static gchar *
mg_server_get_xml_id (MgXmlStorage *iface)
{
	g_return_val_if_fail (iface && IS_MG_SERVER (iface), NULL);
	g_return_val_if_fail (MG_SERVER (iface)->priv, NULL);

	return NULL;
}

static xmlNodePtr
mg_server_save_to_xml (MgXmlStorage *iface, GError **error)
{
	xmlNodePtr toptree, tree;
	MgServer *srv;
	GSList *list;

	g_return_val_if_fail (iface && IS_MG_SERVER (iface), NULL);
	g_return_val_if_fail (MG_SERVER (iface)->priv, NULL);

	srv = MG_SERVER (iface);

	/* main node */
        toptree = xmlNewNode (NULL, "MG_SERVER");
	xmlSetProp (toptree, "with_functions", srv->priv->with_functions ? "t" : "f");

	/* gda_datasource + username */
	tree = xmlNewChild (toptree, NULL, "MG_GDA_DATASOURCE", srv->priv->gda_datasource->str);
	xmlAddChild (toptree, tree);
	tree = xmlNewChild (toptree, NULL, "MG_USERNAME", srv->priv->user_name->str);
	xmlAddChild (toptree, tree);
	
	/* data types */
	tree = xmlNewChild (toptree, NULL, "MG_DATATYPES", NULL);
	list = srv->priv->data_types;
	while (list) {
		xmlNodePtr type;

		type = mg_xml_storage_save_to_xml (MG_XML_STORAGE (list->data), error);
		if (!type) {
			xmlFreeNode (toptree);
			return NULL;
		}
		else {
			if (!xmlAddChild (tree, type)) {
				g_set_error (error, MG_SERVER_ERROR, MG_SERVER_XML_SAVE_ERROR,
					     _("Error saving data type %s"), 
					     mg_server_data_type_get_sqlname 
					     (MG_SERVER_DATA_TYPE (list->data)));
				xmlFreeNode (toptree);
				return NULL;
			}
			list = g_slist_next (list);
		}
	}

	if (srv->priv->with_functions) {
		/* procedures */
		tree = xmlNewChild (toptree, NULL, "MG_PROCEDURES", NULL);
		list = srv->priv->functions;
		while (list) {
			xmlNodePtr type;
			
			type = mg_xml_storage_save_to_xml (MG_XML_STORAGE (list->data), error);
			if (!type) {
				xmlFreeNode (toptree);
				return NULL;
			}
			else {
				if (!xmlAddChild (tree, type)) {
					g_set_error (error, MG_SERVER_ERROR, MG_SERVER_XML_SAVE_ERROR,
						     _("Error saving function %s"), 
						     mg_server_function_get_sqlname 
						     (MG_SERVER_FUNCTION (list->data))); 
					xmlFreeNode (toptree);
					return NULL;
				}
				list = g_slist_next (list);
			}
		}
		
		/* aggregates */
		tree = xmlNewChild (toptree, NULL, "MG_AGGREGATES", NULL);
		list = srv->priv->aggregates;
		while (list) {
			xmlNodePtr type;
			
			type = mg_xml_storage_save_to_xml (MG_XML_STORAGE (list->data), error);
			if (!type) {
				xmlFreeNode (toptree);
				return NULL;
			}
			else {
				if (!xmlAddChild (tree, type)) {
					g_set_error (error, MG_SERVER_ERROR, MG_SERVER_XML_SAVE_ERROR,
						     _("Error saving aggregate %s"), 
						     mg_server_aggregate_get_sqlname 
						     (MG_SERVER_AGGREGATE (list->data))); 
					xmlFreeNode (toptree);
					return NULL;
				}
				list = g_slist_next (list);
			}
		}
	}
	
	return toptree;
}

static gboolean
mg_server_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgServer *srv;
	xmlNodePtr subnode;
	gchar *str;

	g_return_val_if_fail (iface && IS_MG_SERVER (iface), FALSE);
	g_return_val_if_fail (MG_SERVER (iface)->priv, FALSE);
	g_return_val_if_fail (node, FALSE);

	srv = MG_SERVER (iface);

	if (mg_server_conn_is_opened (srv)) {
		g_set_error (error,
			     MG_SERVER_ERROR,
			     MG_SERVER_XML_LOAD_ERROR,
			     _("Connection is already opened"));
		return FALSE;
	}
	if (srv->priv->data_types || srv->priv->functions || srv->priv->aggregates) {
		g_set_error (error,
			     MG_SERVER_ERROR,
			     MG_SERVER_XML_LOAD_ERROR,
			     _("Server already contains data"));
		return FALSE;
	}
	if (strcmp (node->name, "MG_SERVER")) {
		g_set_error (error,
			     MG_SERVER_ERROR,
			     MG_SERVER_XML_LOAD_ERROR,
			     _("XML Tag is not <MG_SERVER>"));
		return FALSE;
	}

	str = xmlGetProp (node, "with_functions");
	if (str) {
		srv->priv->with_functions = (*str && (*str == 't')) ? TRUE : FALSE;
		g_free (str);
	}

	subnode = node->children;
	while (subnode) {
		gboolean done = FALSE;

		if (!strcmp (subnode->name, "MG_GDA_DATASOURCE")) {
			gchar *txt;
			txt = xmlNodeGetContent (subnode);
			mg_server_set_datasource (srv, txt);
			g_free (txt);
			done = TRUE;
		}

		if (!done && !strcmp (subnode->name, "MG_USERNAME")) {
			gchar *txt;
			txt = xmlNodeGetContent (subnode);
			mg_server_set_user_name (srv, txt);
			g_free (txt);
			done = TRUE;
		}

		if (!done && !strcmp (subnode->name, "MG_DATATYPES")) {
			xmlNodePtr type;
			MgServerDataType *dt;

			type = subnode->children;
			while (type) {
				if (! xmlNodeIsText (type)) {
					dt = MG_SERVER_DATA_TYPE (mg_server_data_type_new (srv));
					if (mg_xml_storage_load_from_xml (MG_XML_STORAGE (dt), type, error)) {
						srv->priv->data_types = g_slist_append (srv->priv->data_types, dt);
						g_signal_connect (G_OBJECT (dt), "nullified",
								  G_CALLBACK (nullified_data_type_cb), srv);
						g_signal_connect (G_OBJECT (dt), "changed",
								  G_CALLBACK (updated_data_type_cb), srv);
#ifdef debug_signal
						g_print (">> 'DATA_TYPE_ADDED' from %s\n", __FUNCTION__);
#endif
						g_signal_emit (G_OBJECT (srv), mg_server_signals[DATA_TYPE_ADDED], 0, dt);
#ifdef debug_signal
						g_print ("<< 'DATA_TYPE_ADDED' from %s\n", __FUNCTION__);
#endif
					}
					else {
						return FALSE;
					}
				}
				type = type->next;
			}
			done = TRUE;
		}
		
		if (!done && srv->priv->with_functions && !strcmp (subnode->name, "MG_PROCEDURES")) {
			xmlNodePtr fnode;
			MgServerFunction *func;
			
			fnode = subnode->children;
			while (fnode) {
				if (! xmlNodeIsText (fnode)) {
					func = MG_SERVER_FUNCTION (mg_server_function_new (srv));
					if (mg_xml_storage_load_from_xml (MG_XML_STORAGE (func), fnode, error)) {
						srv->priv->functions = g_slist_append (srv->priv->functions, func);
						g_signal_connect (G_OBJECT (func), "nullified",
								  G_CALLBACK (nullified_function_cb), srv);
						g_signal_connect (G_OBJECT (func), "changed",
								  G_CALLBACK (updated_function_cb), srv);
#ifdef debug_signal
						g_print (">> 'DATA_FUNCTION_ADDED' from %s\n", __FUNCTION__);
#endif
						g_signal_emit_by_name (G_OBJECT (srv), "data_function_added", func);
#ifdef debug_signal
						g_print ("<< 'DATA_FUNCTION_ADDED' from %s\n", __FUNCTION__);
#endif
					}
					else {
						return FALSE;
					}
				}
				fnode = fnode->next;
			}
			done = TRUE;
		}

		if (!done && srv->priv->with_functions && !strcmp (subnode->name, "MG_AGGREGATES")) {
			xmlNodePtr fnode;
			MgServerAggregate *agg;
			
			fnode = subnode->children;
			while (fnode) {
				if (! xmlNodeIsText (fnode)) {
					agg = MG_SERVER_AGGREGATE (mg_server_aggregate_new (srv));
					if (mg_xml_storage_load_from_xml (MG_XML_STORAGE (agg), fnode, error)) {
						srv->priv->aggregates = g_slist_append (srv->priv->aggregates, agg);
						g_signal_connect (G_OBJECT (agg), "nullified",
								  G_CALLBACK (nullified_aggregate_cb), srv);
						g_signal_connect (G_OBJECT (agg), "changed",
								  G_CALLBACK (updated_aggregate_cb), srv);
#ifdef debug_signal
						g_print (">> 'DATA_AGGREGATE_ADDED' from %s\n", __FUCNTION__);
#endif
						g_signal_emit_by_name (G_OBJECT (srv), "data_aggregate_added", agg);
#ifdef debug_signal
						g_print ("<< 'DATA_AGGREGATE_ADDED' from %s\n", __FUCNTION__);
#endif
					}
					else {
						return FALSE;
					}
				}
				fnode = fnode->next;
			}
			done = TRUE;
		}

		subnode = subnode->next;
	}
	
	return TRUE;
}

#ifdef debug
/**
 * mg_server_dump
 * @srv: a #MgServer object
 * @offset: the offset (in caracters) at which the dump will start
 *
 * Writes a textual description of the object to STDOUT. This function only
 * exists if libmergeant is compiled with the "--enable-debug" option.
 */
void 
mg_server_dump (MgServer *srv, gint offset)
{
	gchar *str;
	guint i;
	GSList *list;

	g_return_if_fail (srv && IS_MG_SERVER (srv));
	
	/* string for the offset */
	str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;
	
	/* dump */
	if (srv->priv) 
		g_print ("%s" D_COL_H1 "MgServer %p\n" D_COL_NOR, 
			 str, srv);
	else
		g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, srv);

	/* data types */
	list = srv->priv->data_types;
	if (list) {
		g_print ("%sData types:\n", str);
		while (list) {
			mg_base_dump (MG_BASE (list->data), offset+5);
			list = g_slist_next (list);
		}
	}
	else
		g_print ("%sNo Data type defined\n", str);

	/* functions */
	list = srv->priv->functions;
	if (list) {
		g_print ("%sFunctions:\n", str);
		while (list) {
			mg_base_dump (MG_BASE (list->data), offset+5);
			list = g_slist_next (list);
		}
	}
	else
		g_print ("%sNo Function defined\n", str);


	/* aggregates */
	list = srv->priv->aggregates;
	if (list) {
		g_print ("%sAggregates:\n", str);
		while (list) {
			mg_base_dump (MG_BASE (list->data), offset+5);
			list = g_slist_next (list);
		}
	}
	else
		g_print ("%sNo Aggregate defined\n", str);


	g_free (str);

}
#endif


/**
 * mg_server_get_conf
 * @srv: a #MgServer object
 *
 * Fetch the MgConf object to which the MgServer belongs.
 *
 * Returns: the MgConf object
 */
MgConf *
mg_server_get_conf (MgServer *srv)
{
	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);

	return srv->priv->conf;
}

/**
 * mg_server_set_datasource
 * @srv: a #MgServer object
 * @datasource: a gda datasource
 * 
 * Sets the data source of the server. If the connection is already opened,
 * then no action is performed at all and FALSE is returned.
 *
 * Returns: TRUE on success
 */
gboolean
mg_server_set_datasource (MgServer *srv, const gchar *datasource)
{
	g_return_val_if_fail (srv && IS_MG_SERVER (srv), FALSE);
	g_return_val_if_fail (srv->priv, FALSE);
	g_return_val_if_fail (datasource && *datasource, FALSE);

	if (srv->priv->cnc)
		return FALSE;

	g_string_assign (srv->priv->gda_datasource, datasource);
	return TRUE;
}

/**
 * mg_server_get_datasource
 * @srv: a #MgServer object
 * 
 * Get the data source of the server. 
 *
 * Returns: a new string with the datasource, or NULL
 */
gchar *
mg_server_get_datasource (MgServer *srv)
{
	g_return_val_if_fail (srv && IS_MG_SERVER (srv), FALSE);
	g_return_val_if_fail (srv->priv, FALSE);

	if (srv->priv->gda_datasource && srv->priv->gda_datasource->str && *(srv->priv->gda_datasource->str))
		return g_strdup (srv->priv->gda_datasource->str);
	else
		return NULL;
}

/**
 * mg_server_set_user_name
 * @srv: a #MgServer object
 * @username: 
 * 
 * Sets the user name for the connection to the server. If the connection is already opened,
 * then no action is performed at all and FALSE is returned.
 *
 * Returns: TRUE on success
 */
gboolean
mg_server_set_user_name (MgServer *srv, const gchar *username)
{
	g_return_val_if_fail (srv && IS_MG_SERVER (srv), FALSE);
	g_return_val_if_fail (srv->priv, FALSE);
	g_return_val_if_fail (username, FALSE);

	if (srv->priv->cnc)
		return FALSE;

	g_string_assign (srv->priv->user_name, username);
	return TRUE;	
}

/**
 * mg_server_get_user_name
 * @srv: a #MgServer object
 * 
 * Get the user name for the connection to the server.
 *
 * Returns: a new string with the user name, or NULL
 */
gchar*
mg_server_get_user_name (MgServer *srv)
{
	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);

	if (srv->priv->user_name && srv->priv->user_name->str && *(srv->priv->user_name->str))
		return g_strdup (srv->priv->user_name->str);
	else
		return NULL;
}

/**
 * mg_server_set_user_password
 * @srv: a #MgServer object
 * @password: 
 * 
 * Sets the user password for the connection to the server. If the connection is already opened,
 * then no action is performed at all and FALSE is returned.
 *
 * Returns: TRUE on success
 */
gboolean
mg_server_set_user_password (MgServer *srv, const gchar *password)
{
	g_return_val_if_fail (srv && IS_MG_SERVER (srv), FALSE);
	g_return_val_if_fail (srv->priv, FALSE);
	g_return_val_if_fail (password, FALSE);

	if (srv->priv->cnc)
		return FALSE;

	g_string_assign (srv->priv->password, password);
	return TRUE;	
}

/**
 * mg_server_reset
 * @srv: a #MgServer object
 *
 * Reset the MgServer as it was when created; that is: close the connection if opened,
 * and get rid of any data type, function and aggregate it has.
 */
void
mg_server_reset (MgServer *srv)
{
	g_return_if_fail (srv && IS_MG_SERVER (srv));
	g_return_if_fail (srv->priv);
	
	/* getting rid of the function */
	while (srv->priv->functions)
		mg_base_nullify (MG_BASE (srv->priv->functions->data));
	
	/* getting rid of the aggregates */
	while (srv->priv->aggregates)
		mg_base_nullify (MG_BASE (srv->priv->aggregates->data));
	
	/* getting rid of the data types */
	while (srv->priv->data_types)
		mg_base_nullify (MG_BASE (srv->priv->data_types->data));

	/* close the connection if necessary */
	if (srv->priv->cnc)
		mg_server_close_connect_no_warn (srv);
}

/** 
 * mg_server_open_connect
 * @srv: a #MgServer object
 * @error: location to store error, or %NULL
 *
 * Opens the connection to the DBMS.
 *
 * Returns: TRUE if success and FALSE otherwise (and error is positionned)
 */
gboolean
mg_server_open_connect (MgServer *srv, GError **error)
{
	GdaDataSourceInfo *dsn;
	gboolean retval = FALSE;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), FALSE);
	g_return_val_if_fail (srv->priv, FALSE);

	if (srv->priv->cnc)
		return TRUE;

	dsn = gda_config_find_data_source (srv->priv->gda_datasource->str);
	if (!dsn) {
		GdaError *gdaerror;
		gchar *str;

		gdaerror = gda_error_new ();
		str = g_strdup_printf (_("No datasource '%s' defined in your GDA configuration"),
				       srv->priv->gda_datasource->str);

		g_set_error (error, MG_SERVER_ERROR, MG_SERVER_CONN_OPEN_ERROR, str);
		gda_error_set_description (gdaerror, str);

		g_free (str);
		
		gda_error_set_source (gdaerror, _("[LibMergeant]"));
		gda_connection_add_error (srv->priv->cnc, gdaerror);

		return FALSE;
	}

	srv->priv->cnc = gda_client_open_connection (GDA_CLIENT (srv), dsn->name,
						     srv->priv->user_name->str, 
						     srv->priv->password->str, 0);

	gda_config_free_data_source_info (dsn);
	if (srv->priv->cnc) { 
		/* connection is now opened */
		srv->priv->features.sequences = gda_connection_supports
			(srv->priv->cnc, GDA_CONNECTION_FEATURE_SEQUENCES);
		srv->priv->features.procs = gda_connection_supports
			(srv->priv->cnc, GDA_CONNECTION_FEATURE_PROCEDURES);
		srv->priv->features.inheritance = gda_connection_supports
			(srv->priv->cnc, GDA_CONNECTION_FEATURE_INHERITANCE);
		srv->priv->features.xml_queries = gda_connection_supports
			(srv->priv->cnc, GDA_CONNECTION_FEATURE_XML_QUERIES);		
#ifdef debug_signal
		g_print (">> 'CONN_OPENED' from %s\n", __FUNCTION__);
#endif
		g_signal_emit (G_OBJECT (srv), mg_server_signals[CONN_OPENED], 0);
#ifdef debug_signal
		g_print ("<< 'CONN_OPENED' from %s\n", __FUNCTION__);
#endif

		retval = TRUE;
	}
	else 
		g_set_error (error, MG_SERVER_ERROR, MG_SERVER_CONN_OPEN_ERROR,
			     _("Could not open the connection to the DBMS for datasource '%s'"),
			     srv->priv->gda_datasource->str);

	return retval;
}


/**
 * mg_server_conn_is_opened
 * @srv: a #MgServer object
 *
 * Checks wether the connection to the DBMS is opened or not
 *
 * Returns: TRUE if the connection is opened
 */
gboolean
mg_server_conn_is_opened (MgServer *srv)
{
	g_return_val_if_fail (srv && IS_MG_SERVER (srv), FALSE);
	g_return_val_if_fail (srv->priv, FALSE);

	return srv->priv->cnc ? TRUE : FALSE;
}

/** 
 * mg_server_close_connect
 * @srv: a #MgServer object
 *
 * Closes the connection to the DBMS. First the "conn_to_close" signal is emitted.
 * This function should preferably be called instead of the mg_server_close_connect_no_warn() function.
 */
void
mg_server_close_connect (MgServer *srv)
{
	g_return_if_fail (srv && IS_MG_SERVER (srv));
	g_return_if_fail (srv->priv);

	if (!srv->priv->cnc)
		return;

#ifdef debug_signal
	g_print (">> 'CONN_TO_CLOSE' from %s\n", __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (srv), mg_server_signals[CONN_TO_CLOSE], 0);
#ifdef debug_signal
	g_print ("<< 'CONN_TO_CLOSE' from %s\n", __FUNCTION__);
#endif

	mg_server_close_connect_no_warn (srv);
}


/** 
 * mg_server_close_connect_no_warn
 * @srv: a #MgServer object
 *
 * Closes the connection to the DBMS. Warning: "conn_to_close" signal is NOT emitted.
 */
void
mg_server_close_connect_no_warn (MgServer *srv)
{
	g_return_if_fail (srv && IS_MG_SERVER (srv));
	g_return_if_fail (srv->priv);

	if (!srv->priv->cnc)
		return;
	
	gda_connection_close (srv->priv->cnc);
	srv->priv->cnc = NULL;

#ifdef debug_signal
	g_print (">> 'CONN_CLOSED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (srv), mg_server_signals[CONN_CLOSED], 0);
#ifdef debug_signal
	g_print ("<< 'CONN_CLOSED' from %s\n", __FUNCTION__);
#endif	
}


/**
 * mg_server_do_query
 * @srv: a #MgServer object
 * @query: the query to be executed
 * @type: the query type (SQL or XML)
 * @error: location to store error, or %NULL
 *
 * Sends a query to the DBMS to which the connection is established. If the query is a SELECT
 * one, then a new #MgResultSet is returned (it's up to the caller to unref that object); 
 * otherwise NULL is returned. The error variable contains the error code if an error occured.
 *
 * Returns: a new #MgResultSet object or NULL
 */
MgResultSet *
mg_server_do_query (MgServer *srv, const gchar *query,
		    MgServerQueryType type, GError **error)
{
	MgResultSet *mgres = NULL;
	GdaCommandType gtype;
	GdaDataModel *res;
	GdaCommand *cmd;
	MgServerOpMode mode = MG_SERVER_UNKNOWN_OP;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);

	if (!srv->priv->cnc) {
		g_set_error (error, MG_SERVER_ERROR, MG_SERVER_DO_QUERY_ERROR,
			     _("Connection is not opened"));
		return NULL;
	}
	
	switch (type) {
	case MG_SERVER_QUERY_XML:
		gtype = GDA_COMMAND_TYPE_XML;
		/* FIXME: get the op mode of that query */
		break;
	default:
		gtype = GDA_COMMAND_TYPE_SQL;
		mode = mg_server_get_sql_op_mode (srv, query);
		break;
	}
	
	if (mode == MG_SERVER_UNKNOWN_OP) 
		g_set_error (error, MG_SERVER_ERROR, MG_SERVER_DO_QUERY_ERROR,
			     _("Unknown mode of operation for this query"));
	else {
		cmd = gda_command_new (query, gtype, GDA_COMMAND_OPTION_STOP_ON_ERRORS);
		res = gda_connection_execute_single_command (srv->priv->cnc, cmd, NULL);
		
		if (res) {
			switch (mode) {
			case MG_SERVER_SELECT_OP:
				mgres = MG_RESULTSET (mg_resultset_new (srv, cmd, res));
				break;
			default:
				break;
			}

			g_object_unref (G_OBJECT (res));
		}
		else 
			g_set_error (error, MG_SERVER_ERROR, MG_SERVER_DO_QUERY_ERROR,
				     _("Error during execution of this query (%s)"), query);

	}

	return mgres;
}


/**
 * mg_server_get_sql_op_mode
 * @srv: a #MgServer object
 * @query: an SQL query
 * 
 * Get the operation type (= mode) which is performed by the query given as argument.
 * The query MUST contain only one statement, not several separated by ';'
 *
 * Returns: the query type (mode).
 */
MgServerOpMode
mg_server_get_sql_op_mode (MgServer *srv, const gchar *query)
{
	MgServerOpMode mode = MG_SERVER_UNKNOWN_OP;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), MG_SERVER_UNKNOWN_OP);
	g_return_val_if_fail (srv->priv, MG_SERVER_UNKNOWN_OP);
	g_return_val_if_fail (query && *query, MG_SERVER_UNKNOWN_OP);

	/* FIXME: return an error if there is more than ONE query statement in the query string */
	if (! g_ascii_strncasecmp (query, "SELECT", 6))
		mode = MG_SERVER_SELECT_OP;
	if (! g_ascii_strncasecmp (query, "INSERT", 6))
		mode = MG_SERVER_INSERT_OP;
	if (! g_ascii_strncasecmp (query, "UPDATE", 6))
		mode = MG_SERVER_UPDATE_OP;
	if (! g_ascii_strncasecmp (query, "DELETE", 6))
		mode = MG_SERVER_DELETE_OP;

	return mode;
}


static gboolean server_data_type_update_list (MgServer * srv, GError **error);
static gboolean server_functions_update_list (MgServer * srv, GError **error);
static gboolean server_aggregates_update_list (MgServer * srv, GError **error);
/**
 * mg_server_update_dbms_data
 * @srv: a #MgServer object
 * @error: location to store error, or %NULL
 *
 * Synchronise the list of data types, functions, etc the MgServer object has
 * with what is in the DBMS the connection is opened to. The connection to the DBMS 
 * MUST be opened.
 *
 * Returns: TRUE if no error
 */
gboolean
mg_server_update_dbms_data (MgServer *srv, GError **error)
{
	gboolean retval = TRUE;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), FALSE);
	g_return_val_if_fail (srv->priv, FALSE);
	
	if (srv->priv->update_in_progress) {
		g_set_error (error, MG_SERVER_ERROR, MG_SERVER_META_DATA_UPDATE,
			     _("Update already started!"));
		return FALSE;
	}

	if (!srv->priv->cnc) {
		g_set_error (error, MG_SERVER_ERROR, MG_SERVER_META_DATA_UPDATE,
			     _("Connection is not opened!"));
		return FALSE;
	}

	srv->priv->update_in_progress = TRUE;
	srv->priv->stop_update = FALSE;

#ifdef debug_signal
	g_print (">> 'DATA_UPDATE_STARTED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (srv), mg_server_signals[DATA_UPDATE_STARTED], 0);
#ifdef debug_signal
	g_print ("<< 'DATA_UPDATE_STARTED' from %s\n", __FUNCTION__);
#endif

	retval = server_data_type_update_list (srv, error);
	if (retval && srv->priv->with_functions && !srv->priv->stop_update) 
		retval = server_functions_update_list (srv, error);
	if (retval && srv->priv->with_functions && !srv->priv->stop_update) 
		retval = server_aggregates_update_list (srv, error);

#ifdef debug_signal
	g_print (">> 'DATA_UPDATE_FINISHED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (srv), mg_server_signals[DATA_UPDATE_FINISHED], 0);
#ifdef debug_signal
	g_print ("<< 'DATA_UPDATE_FINISHED' from %s\n", __FUNCTION__);
#endif

	srv->priv->update_in_progress = FALSE;
	if (srv->priv->stop_update) {
		g_set_error (error, MG_SERVER_ERROR, MG_SERVER_META_DATA_UPDATE_USER_STOPPED,
			     _("Update stopped!"));
		return FALSE;
	}

	return retval;
}


/**
 * mg_server_stop_update_dbms_data
 * @srv: a #MgServer object
 *
 * When the server updates its internal lists of DBMS objects, a call to this function will 
 * stop that update process. It has no effect when the server is not updating its DBMS data.
 */
void
mg_server_stop_update_dbms_data (MgServer *srv)
{
	g_return_if_fail (srv && IS_MG_SERVER (srv));
	g_return_if_fail (srv->priv);

	srv->priv->stop_update = TRUE;
}

static gboolean
server_data_type_update_list (MgServer * srv, GError **error)
{
	GSList *dtl = srv->priv->data_types;
	MgServerDataType *dt;
	GdaDataModel *rs;
	gchar *str;
	guint now, total;
	GSList *updated_dt = NULL;

	/* here we get the complete list of types, and for each type, update or
	   create the entry in the list if not yet there. */
	rs = gda_connection_get_schema (GDA_CONNECTION (srv->priv->cnc),
					GDA_CONNECTION_SCHEMA_TYPES, NULL);

	if (!rs) {
		g_set_error (error, MG_SERVER_ERROR, MG_SERVER_DATATYPE_ERROR,
			     _("Can't get list of data types"));
		return FALSE;
	}


	if (!mg_resultset_check_data_model (rs, 4, 
					    GDA_VALUE_TYPE_STRING, 
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_TYPE)) {
		g_set_error (error, MG_SERVER_ERROR, MG_SERVER_DATATYPE_ERROR,
			     _("Schema for list of data types is wrong"));
		g_object_unref (G_OBJECT (rs));
		return FALSE;
	}

	total = gda_data_model_get_n_rows (rs);
	now = 0;		
	while ((now < total) && !srv->priv->stop_update) {
		const GdaValue *value;
		gboolean newdt = FALSE;

		value = gda_data_model_get_value_at (rs, 0, now);
		str = gda_value_stringify (value);
		dt = mg_server_get_data_type_by_name (srv, str);
		if (!dt) {
			gint i = 0;
			GSList *list;
			gboolean found = FALSE;

			/* type name */
			dt = MG_SERVER_DATA_TYPE (mg_server_data_type_new (srv));
			mg_server_data_type_set_sqlname (dt, str);
			newdt = TRUE;
			
			/* finding the right position for the data type */
			list = srv->priv->data_types;
			while (list && !found) {
				if (strcmp (str, 
					    mg_server_data_type_get_sqlname (MG_SERVER_DATA_TYPE (list->data))) < 0)
					found = TRUE;
				else
					i++;
				list = g_slist_next (list);
			}

			srv->priv->data_types = g_slist_insert (srv->priv->data_types, dt, i);
			/* REM: we don't connect the "nullified" and "changed" signals from the new MgServerDataType right
			   now because otherwise the MgServer object will forward the "changed" signal 
			   (into "data_type_updated") before we have had the chance to emit the "data_type_added" signal */
		}
		g_free (str);
		
		updated_dt = g_slist_append (updated_dt, dt);

		/* FIXME: number of params */
		/*dt->numparams = 0;*/

		/* description */
		value = gda_data_model_get_value_at (rs, 2, now);
		if (value && !gda_value_is_null (value) && (* gda_value_get_string(value))) {
			str = gda_value_stringify (value);
			mg_base_set_description (MG_BASE (dt), str);
			g_free (str);
		}
		else 
			mg_base_set_description (MG_BASE (dt), NULL);

		/* owner */
		value = gda_data_model_get_value_at (rs, 1, now);
		if (value && !gda_value_is_null (value) && (* gda_value_get_string(value))) {
			str = gda_value_stringify (value);
			mg_base_set_owner (MG_BASE (dt), str);
			g_free (str);
		}
		else
			mg_base_set_owner (MG_BASE (dt), NULL);
				
		/* gda_type */
		value = gda_data_model_get_value_at (rs, 3, now);
		if (value && !gda_value_is_null (value)) 
			mg_server_data_type_set_gda_type (dt, gda_value_get_vtype (value));
		
		/* signal if the DT is new */
		if (newdt) {
			g_signal_connect (G_OBJECT (dt), "nullified",
					  G_CALLBACK (nullified_data_type_cb), srv);
			g_signal_connect (G_OBJECT (dt), "changed",
					  G_CALLBACK (updated_data_type_cb), srv);
#ifdef debug_signal
			g_print (">> 'DATA_TYPE_ADDED' from %s\n", __FUNCTION__);
#endif
			g_signal_emit (G_OBJECT (srv), mg_server_signals[DATA_TYPE_ADDED], 0, dt);
#ifdef debug_signal
			g_print ("<< 'DATA_TYPE_ADDED' from %s\n", __FUNCTION__);
#endif
		}

		g_signal_emit_by_name (G_OBJECT (srv), "update_progress", "DATA_TYPES",
				       now, total);
		now++;
	}
	
	g_object_unref (G_OBJECT (rs));
	
	/* remove the data types not existing anymore */
	dtl = srv->priv->data_types;
	while (dtl) {
		if (!g_slist_find (updated_dt, dtl->data)) {
			mg_base_nullify (MG_BASE (dtl->data));
			dtl = srv->priv->data_types;
		}
		else
			dtl = g_slist_next (dtl);
	}
	g_slist_free (updated_dt);
	
	g_signal_emit_by_name (G_OBJECT (srv), "update_progress", NULL, 0, 0);
	
	return TRUE;
}


static void
nullified_data_type_cb (MgServerDataType *dt, MgServer *srv)
{
	g_return_if_fail (g_slist_find (srv->priv->data_types, dt));

	srv->priv->data_types = g_slist_remove (srv->priv->data_types, dt);

	g_signal_handlers_disconnect_by_func (G_OBJECT (dt), 
					      G_CALLBACK (nullified_data_type_cb), srv);
	g_signal_handlers_disconnect_by_func (G_OBJECT (dt), 
					      G_CALLBACK (updated_data_type_cb), srv);

#ifdef debug_signal
	g_print (">> 'DATA_TYPE_REMOVED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (srv), "data_type_removed", dt);
#ifdef debug_signal
	g_print ("<< 'DATA_TYPE_REMOVED' from %s\n", __FUNCTION__);
#endif

	g_object_unref (G_OBJECT (dt));
}

static void
updated_data_type_cb (MgServerDataType *dt, MgServer *srv)
{
#ifdef debug_signal
	g_print (">> 'DATA_TYPE_UPDATED' from %s\n", __FUCNTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (srv), "data_type_updated", dt);
#ifdef debug_signal
	g_print ("<< 'DATA_TYPE_UPDATED' from %s\n", __FUCNTION__);
#endif	
}

static gboolean
server_functions_update_list (MgServer *srv, GError **error)
{
	MgServerFunction *func;
	GdaDataModel *rs;
	gchar *str;
	guint now, total;
	GSList *list, *updated_fn = NULL, *todelete_fn = NULL;
	GSList *original_functions;
	gboolean insert;
	gint current_position = 0;

	/* here we get the complete list of functions, and for each function, update or
	   create the entry in the list if not yet there. */
	rs = gda_connection_get_schema (GDA_CONNECTION (srv->priv->cnc),
					GDA_CONNECTION_SCHEMA_PROCEDURES, NULL);

	if (!rs) {
		g_set_error (error, MG_SERVER_ERROR, MG_SERVER_FUNCTIONS_ERROR,
			     _("Can't get list of functions"));
		return FALSE;
	}


	if (!mg_resultset_check_data_model (rs, 8, 
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_INTEGER,
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING)) {
		g_set_error (error, MG_SERVER_ERROR, MG_SERVER_FUNCTIONS_ERROR,
			     _("Schema for list of functions is wrong"));
		g_object_unref (G_OBJECT (rs));
		return FALSE;
	}

	original_functions = mg_server_get_functions (srv);
	total = gda_data_model_get_n_rows (rs);
	now = 0;		
	while ((now < total) && !srv->priv->stop_update) {
		MgServerDataType *rettype = NULL; /* return type for the function */
		GSList *dtl = NULL;     /* list of params for the function */
		const GdaValue *value;
		gchar *ptr;
		gchar *tok;
		
		insert = TRUE;

		/* fetch return type */
		value = gda_data_model_get_value_at (rs, 4, now);
		str = gda_value_stringify (value);
		if (*str != '-') {
			rettype = mg_server_get_data_type_by_name (srv, str);
			if (!rettype)
				insert = FALSE;
		}
		else 
			insert = FALSE;
		g_free (str);

		/* fetch argument types */
		value = gda_data_model_get_value_at (rs, 6, now);
		str = gda_value_stringify (value);
		if (str) {
			ptr = strtok_r (str, " ", &tok);
			while (ptr && *ptr) {
				MgServerDataType *indt;

				if (*ptr == '-')
					dtl = g_slist_append (dtl, NULL); /* any data type will do */
				else {
					indt = mg_server_get_data_type_by_name (srv, ptr);
					if (indt)
						dtl = g_slist_append (dtl, indt);
					else 
						insert = FALSE;
				}
				ptr = strtok_r (NULL, " ", &tok);
			}
			g_free (str);
		}

		/* fetch a func if there is already one with the same id */
		value = gda_data_model_get_value_at (rs, 1, now);
		str = gda_value_stringify (value);
		func = mg_server_get_function_by_dbms_id (srv, str);
		g_free (str);

		if (!func) {
			/* try to find the function using its name, return type and argument type, 
			   and not its DBMS id, this is usefull if the DBMS has changed and the
			   DBMS id have changed */
			value =  gda_data_model_get_value_at (rs, 0, now);
			str = gda_value_stringify (value);
			func = mg_server_get_function_by_name_arg_real (original_functions, str, dtl);
			g_free (str);

			if (func && (mg_server_function_get_ret_type (func) != rettype))
				func = NULL;
		}

		if (!insert) {
			if (func)
				/* If no insertion, then func MUST be updated */
				todelete_fn = g_slist_append (todelete_fn, func);
			func = NULL;
		}
		else {
			if (func) {
				/* does the function we found have the same rettype and params 
				   as the one we have now? */
				gboolean isequal = TRUE;
				GSList *hlist;
				
				list = mg_server_function_get_arg_types (func);
				hlist = dtl;
				while (list && hlist && isequal) {
					if (list->data != hlist->data)
						isequal = FALSE;
					list = g_slist_next (list);
					hlist = g_slist_next (hlist);
				}
				if (isequal && (mg_server_function_get_ret_type (func) != rettype)) 
					isequal = FALSE;
				
				if (isequal) {
					updated_fn = g_slist_append (updated_fn, func);
					current_position = g_slist_index (srv->priv->functions, func) + 1;
					insert = FALSE;
				}
				else {
					todelete_fn = g_slist_append (todelete_fn, func);
					func = NULL;
				}
			}

			if (!func) {
				/* creating new ServerFunction object */
				func = MG_SERVER_FUNCTION (mg_server_function_new (srv));
				mg_server_function_set_ret_type (func, rettype);
				mg_server_function_set_arg_types (func, dtl);

				/* mark function as updated */
				updated_fn = g_slist_append (updated_fn, func);
			}
		}
	
		if (dtl)
			g_slist_free (dtl);
		
		/* function's attributes update */
		if (func) {
			/* unique id */
			value = gda_data_model_get_value_at (rs, 1, now);
			str = gda_value_stringify (value);
			mg_server_function_set_dbms_id (func, str);
			g_free (str);

			/* description */
			value = gda_data_model_get_value_at (rs, 3, now);
			if (value && !gda_value_is_null (value) && (* gda_value_get_string(value))) {
				str = gda_value_stringify (value);
				mg_base_set_description (MG_BASE (func), str);
				g_free (str);
			}
			
			/* sqlname */
			value =  gda_data_model_get_value_at (rs, 0, now);
			str = gda_value_stringify (value);
			mg_server_function_set_sqlname (func, str);
			g_free (str);
			
			/* owner */
			value = gda_data_model_get_value_at (rs, 2, now);
			if (value && !gda_value_is_null (value) && (* gda_value_get_string(value))) {
				str = gda_value_stringify (value);
				mg_base_set_owner (MG_BASE (func), str);
				g_free (str);
			}
			else
				mg_base_set_owner (MG_BASE (func), NULL);
		}

		/* Real insertion */
		if (insert) {
			/* insertion in the list: finding where to insert the function */
			srv->priv->functions = g_slist_insert (srv->priv->functions, func, current_position++);
			g_signal_connect (G_OBJECT (func), "nullified",
					  G_CALLBACK (nullified_function_cb), srv);
			g_signal_connect (G_OBJECT (func), "changed",
					  G_CALLBACK (updated_function_cb), srv);

#ifdef debug_signal
			g_print (">> 'DATA_FUNCTION_ADDED' from %s\n", __FUNCTION__);
#endif
			g_signal_emit_by_name (G_OBJECT (srv), "data_function_added", func);
#ifdef debug_signal
			g_print ("<< 'DATA_FUNCTION_ADDED' from %s\n", __FUNCTION__);
#endif
		}

		g_signal_emit_by_name (G_OBJECT (srv), "update_progress", "FUNCTIONS", now, total);
		now ++;
	}

	g_object_unref (G_OBJECT (rs));
	if (original_functions)
		g_slist_free (original_functions);

	/* cleanup for the functions which do not exist anymore */
        list = srv->priv->functions;
        while (list && !srv->priv->stop_update) {
		if (!g_slist_find (updated_fn, list->data)) 
			todelete_fn = g_slist_append (todelete_fn, list->data);
		list = g_slist_next (list);
        }
	g_slist_free (updated_fn);

	list = todelete_fn;
	while (list) {
		mg_base_nullify (MG_BASE (list->data));
		list = g_slist_next (list);
	}
	g_slist_free (todelete_fn);
	
	g_signal_emit_by_name (G_OBJECT (srv), "update_progress", NULL, 0, 0);
	
	return TRUE;
}

static void
nullified_function_cb (MgServerFunction *func, MgServer *srv)
{
	g_return_if_fail (g_slist_find (srv->priv->functions, func));
	srv->priv->functions = g_slist_remove (srv->priv->functions, func);

	g_signal_handlers_disconnect_by_func (G_OBJECT (func), 
					      G_CALLBACK (nullified_function_cb), srv);
	g_signal_handlers_disconnect_by_func (G_OBJECT (func), 
					      G_CALLBACK (updated_function_cb), srv);

#ifdef debug_signal
	g_print (">> 'DATA_FUNCTION_REMOVED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (srv), "data_function_removed", func);
#ifdef debug_signal
	g_print ("<< 'DATA_FUNCTION_REMOVED' from %s\n", __FUNCTION__);
#endif

	g_object_unref (G_OBJECT (func));
}


static void
updated_function_cb (MgServerFunction *func, MgServer *srv)
{
#ifdef debug_signal
	g_print (">> 'DATA_FUNCTION_UPDATED' from %s\n", __FUCNTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (srv), "data_function_updated", func);
#ifdef debug_signal
	g_print ("<< 'DATA_FUNCTION_UPDATED' from %s\n", __FUCNTION__);
#endif	
}


static gboolean
server_aggregates_update_list (MgServer *srv, GError **error)
{
	MgServerAggregate *agg;
	GdaDataModel *rs;
	gchar *str;
	guint now, total;
	GSList *list, *updated_aggs = NULL, *todelete_aggs = NULL;
	GSList *original_aggregates;
	gboolean insert;
	gint current_position = 0;

	/* here we get the complete list of aggregates, and for each aggregate, update or
	   create the entry in the list if not yet there. */
	rs = gda_connection_get_schema (GDA_CONNECTION (srv->priv->cnc),
					GDA_CONNECTION_SCHEMA_AGGREGATES, NULL);

	if (!rs) {
		g_set_error (error, MG_SERVER_ERROR, MG_SERVER_AGGREGATES_ERROR,
			     _("Can't get list of aggregates"));
		return FALSE;
	}


	if (!mg_resultset_check_data_model (rs, 7, 
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING)) {
		g_set_error (error, MG_SERVER_ERROR, MG_SERVER_AGGREGATES_ERROR,
			     _("Schema for list of aggregates is wrong"));
		g_object_unref (G_OBJECT (rs));
		return FALSE;
	}

	original_aggregates = mg_server_get_aggregates (srv);
	total = gda_data_model_get_n_rows (rs);
	now = 0;		
	while ((now < total) && !srv->priv->stop_update) {
		MgServerDataType *outdt = NULL; /* return type for the aggregate */
		MgServerDataType *indt = NULL;  /* argument for the aggregate */
		const GdaValue *value;
		
		insert = TRUE;

		/* fetch return type */
		value = gda_data_model_get_value_at (rs, 4, now);
		str = gda_value_stringify (value);
		if (*str != '-') {
			outdt = mg_server_get_data_type_by_name (srv, str);
			if (!outdt)
				insert = FALSE;
		}
		else 
			insert = FALSE;
		g_free (str);

		/* fetch argument type */
		value = gda_data_model_get_value_at (rs, 5, now);
		str = gda_value_stringify (value);
		if (str) {
			if (*str != '-') {
				indt = mg_server_get_data_type_by_name (srv, str);
				if (!indt)
					insert = FALSE;
			}
			g_free (str);
		}

		/* fetch a agg if there is already one with the same id */
		value = gda_data_model_get_value_at (rs, 1, now);
		str = gda_value_stringify (value);
		agg = mg_server_get_aggregate_by_dbms_id (srv, str);
		g_free (str);

		if (!agg) {
			/* try to find the aggregate using its name, return type and argument type, 
			   and not its DBMS id, this is usefull if the DBMS has changed and the
			   DBMS id have changed */
			value =  gda_data_model_get_value_at (rs, 0, now);
			str = gda_value_stringify (value);
			agg = mg_server_get_aggregate_by_name_arg_real (original_aggregates, str, indt);
			g_free (str);

			if (agg && (mg_server_aggregate_get_ret_type (agg) != outdt))
				agg = NULL;
		}

		if (!insert) {
			if (agg)
				/* If no insertion, then agg MUST be updated */
				todelete_aggs = g_slist_append (todelete_aggs, agg);
			agg = NULL;
		}
		else {
			if (agg) {
				/* does the aggregate we found have the same outdt and indt
				   as the one we have now? */
				gboolean isequal = TRUE;
				if (mg_server_aggregate_get_arg_type (agg) != indt)
					isequal = FALSE;
				if (isequal && (mg_server_aggregate_get_ret_type (agg) != outdt)) 
					isequal = FALSE;
				
				if (isequal) {
					updated_aggs = g_slist_append (updated_aggs, agg);
					current_position = g_slist_index (srv->priv->aggregates, agg) + 1;
					insert = FALSE;
				}
				else {
					todelete_aggs = g_slist_append (todelete_aggs, agg);
					agg = NULL;
				}
			}

			if (!agg) {
				/* creating new ServerAggregate object */
				agg = MG_SERVER_AGGREGATE (mg_server_aggregate_new (srv));
				mg_server_aggregate_set_ret_type (agg, outdt);
				mg_server_aggregate_set_arg_type (agg, indt);
				
				/* mark aggregate as updated */
				updated_aggs = g_slist_append (updated_aggs, agg);
			}
		}
	
		/* aggregate's attributes update */
		if (agg) {
			/* unique id */
			value = gda_data_model_get_value_at (rs, 1, now);
			str = gda_value_stringify (value);
			mg_server_aggregate_set_dbms_id (agg, str);
			g_free (str);

			/* description */
			value = gda_data_model_get_value_at (rs, 3, now);
			if (value && !gda_value_is_null (value) && (* gda_value_get_string(value))) {
				str = gda_value_stringify (value);
				mg_base_set_description (MG_BASE (agg), str);
				g_free (str);
			}
			
			/* sqlname */
			value =  gda_data_model_get_value_at (rs, 0, now);
			str = gda_value_stringify (value);
			mg_server_aggregate_set_sqlname (agg, str);
			g_free (str);
			
			/* owner */
			value = gda_data_model_get_value_at (rs, 2, now);
			if (value && !gda_value_is_null (value) && (* gda_value_get_string(value))) {
				str = gda_value_stringify (value);
				mg_base_set_owner (MG_BASE (agg), str);
				g_free (str);
			}
			else
				mg_base_set_owner (MG_BASE (agg), NULL);
		}

		/* Real insertion */
		if (insert) {
			/* insertion in the list: finding where to insert the aggregate */
			srv->priv->aggregates = g_slist_insert (srv->priv->aggregates, agg, current_position++);
			g_signal_connect (G_OBJECT (agg), "nullified",
					  G_CALLBACK (nullified_aggregate_cb), srv);
			g_signal_connect (G_OBJECT (agg), "changed",
					  G_CALLBACK (updated_aggregate_cb), srv);

#ifdef debug_signal
			g_print (">> 'DATA_AGGREGATE_ADDED' from %s\n", __FUCNTION__);
#endif
			g_signal_emit_by_name (G_OBJECT (srv), "data_aggregate_added", agg);
#ifdef debug_signal
			g_print ("<< 'DATA_AGGREGATE_ADDED' from %s\n", __FUCNTION__);
#endif
		}

		g_signal_emit_by_name (G_OBJECT (srv), "update_progress", "AGGREGATES", now, total);
		now ++;
	}

	g_object_unref (G_OBJECT (rs));
	if (original_aggregates)
		g_slist_free (original_aggregates);

	/* cleanup for the aggregates which do not exist anymore */
        list = srv->priv->aggregates;
        while (list && !srv->priv->stop_update) {
		if (!g_slist_find (updated_aggs, list->data)) 
			todelete_aggs = g_slist_append (todelete_aggs, list->data);
		list = g_slist_next (list);
        }
	g_slist_free (updated_aggs);

	list = todelete_aggs;
	while (list) {
		mg_base_nullify (MG_BASE (list->data));
		list = g_slist_next (list);
	}
	g_slist_free (todelete_aggs);
		
	g_signal_emit_by_name (G_OBJECT (srv), "update_progress", NULL, 0, 0);
	
	return TRUE;
}


static void
nullified_aggregate_cb (MgServerAggregate *agg, MgServer *srv)
{
	g_return_if_fail (g_slist_find (srv->priv->aggregates, agg));
	srv->priv->aggregates = g_slist_remove (srv->priv->aggregates, agg);

	g_signal_handlers_disconnect_by_func (G_OBJECT (agg), 
					      G_CALLBACK (nullified_aggregate_cb), srv);
	g_signal_handlers_disconnect_by_func (G_OBJECT (agg), 
					      G_CALLBACK (updated_aggregate_cb), srv);

#ifdef debug_signal
	g_print (">> 'DATA_AGGREGATE_REMOVED' from %s\n", __FUCNTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (srv), "data_aggregate_removed", agg);
#ifdef debug_signal
	g_print ("<< 'DATA_AGGREGATE_REMOVED' from %s\n", __FUCNTION__);
#endif
	g_object_unref (G_OBJECT (agg));
}

static void
updated_aggregate_cb (MgServerAggregate *agg, MgServer *srv)
{
#ifdef debug_signal
	g_print (">> 'DATA_AGGREGATE_UPDATED' from %s\n", __FUCNTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (srv), "data_aggregate_updated", agg);
#ifdef debug_signal
	g_print ("<< 'DATA_AGGREGATE_UPDATED' from %s\n", __FUCNTION__);
#endif	
}


/**
 * mg_server_get_handler_by_name
 * @srv: a #MgServer object
 * @name: 
 *
 * Get the MgDataHandler from its name.
 *
 * Returns: the MgDataHandler object
 */
MgDataHandler *
mg_server_get_handler_by_name (MgServer *srv, const gchar *name)
{
	MgDataHandler *dh = NULL;
	GSList *list;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (name && *name, NULL);
	
	list = srv->priv->handlers;
	while (list && !dh) {
		if (!strcmp (mg_base_get_name (MG_BASE (list->data)), name))
			dh = MG_DATA_HANDLER (list->data);
		list = g_slist_next (list);
	}

	if (!dh) {
		if (!strcmp (mg_base_get_name (MG_BASE (srv->priv->fallback_handler)), name))
			dh = srv->priv->fallback_handler;
	}
	
	return dh;
}


/**
 * mg_server_get_handler_by_type
 * @srv: a #MgServer object
 * @type: 
 *
 * Get the MgDataHandler for a data type
 *
 * Returns: the MgDataHandler object
 */
MgDataHandler *
mg_server_get_handler_by_type (MgServer *srv, MgServerDataType *type)
{
	MgDataHandler *dh = NULL;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (type && IS_MG_SERVER_DATA_TYPE (type), NULL);	

	TO_IMPLEMENT;

	return dh;
}


/**
 * mg_server_get_handler_by_gda
 * @srv: a #MgServer object
 * @gda_type: 
 *
 * Get the MgDataHandler for a gda type: the first DataHandler which can handle
 * the requested type is returned.  If no good handler can be found, then a default
 * one will be provided. This function never returns NULL.
 *
 * Returns: the MgDataHandler object
 */
MgDataHandler *
mg_server_get_handler_by_gda (MgServer *srv, GdaValueType gda_type)
{
	GSList *list;
	MgDataHandler *dh = NULL;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (gda_type, NULL);
	
	list = srv->priv->handlers;
	while (list && !dh) {
		if (mg_data_handler_accepts_gda_type (MG_DATA_HANDLER (list->data), gda_type))
			dh = MG_DATA_HANDLER (list->data);
		list = g_slist_next (list);
	}

	if (!dh)
		dh = srv->priv->fallback_handler;

	return dh;
}


/**
 * mg_server_get_plugin_handlers
 * @srv: a #MgServer object
 *
 * Get a list of all the MgDataHandler plugins used by the MgServer.
 *
 * Returns: an allocated list of plugins
 */
GSList *
mg_server_get_plugin_handlers (MgServer *srv)
{
	GSList *list, *retval = NULL;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	
	list = srv->priv->handlers;	
	while (list) {
		if (mg_data_handler_is_plugin (MG_DATA_HANDLER (list->data)))
			retval = g_slist_append (retval, list->data);
		list = g_slist_next (list);
	}

	return retval;
}


/**
 * mg_server_get_user_by_name
 * @srv: a #MgServer object
 * @username: 
 *
 * Find a MgUser from its name.
 *
 * Returns: a pointer to the requested object, or NULL if the object cannot be found.
 */
MgUser *
mg_server_get_user_by_name (MgServer *srv, const gchar *username)
{
	MgUser *user = NULL;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (username && *username, NULL);
	
	TO_IMPLEMENT;

	return user;
}

/**
 * mg_server_get_data_types
 * @srv: a #MgServer object
 *
 * Get the list of data types;
 *
 * Returns: the list (the caller must free the list after usage)
 */
GSList *
mg_server_get_data_types (MgServer *srv)
{
	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);

	if (srv->priv->data_types)
		return g_slist_copy (srv->priv->data_types);
	else
		return NULL;
}

/**
 * mg_server_get_data_type_by_name
 * @srv: a #MgServer object
 * @typename: 
 *
 * Find a data type from its DBMS name
 *
 * Returns: the data type or %NULL if it cannot be found
 */
MgServerDataType  *
mg_server_get_data_type_by_name (MgServer *srv, const gchar *typename)
{
	GSList *list;
	MgServerDataType *datatype = NULL;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (typename && *typename, NULL);
	
	list = srv->priv->data_types;
	while (list && !datatype) {
		if (!strcmp (mg_server_data_type_get_sqlname (MG_SERVER_DATA_TYPE (list->data)),
			     typename))
			datatype = MG_SERVER_DATA_TYPE (list->data);
		list = g_slist_next (list);
	}

	return datatype;
}

/**
 * mg_server_get_data_type_by_xml_id
 * @srv: a #MgServer object
 * @xml_id: the XML identifier of the data type to be found
 *
 * To find a #MgServerDataType using its XML id.
 *
 * Returns: the data type or %NULL if it cannot be found
 */
MgServerDataType  *
mg_server_get_data_type_by_xml_id (MgServer *srv, const gchar *xml_id)
{
	GSList *list;
	MgServerDataType *datatype = NULL;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (xml_id && *xml_id, NULL);
	
	list = srv->priv->data_types;
	while (list && !datatype) {
		gchar *id = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
		if (!strcmp (id, xml_id))
			datatype = MG_SERVER_DATA_TYPE (list->data);
		g_free (id);
		list = g_slist_next (list);
	}

	return datatype;
}

/**
 * mg_server_get_functions
 * @srv: a #MgServer object
 *
 * To get the complete list of functions
 *
 * Returns: the allocated list of functions
 */
GSList *
mg_server_get_functions (MgServer *srv)
{
	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);

	if (srv->priv->functions)
		return g_slist_copy (srv->priv->functions);
	else
		return NULL;
}


/**
 * mg_server_get_functions_by_name
 * @srv: a #MgServer object
 * @funcname: name of the function
 *
 * To get the list of DBMS functions which match the given name.
 *
 * Returns: the allocated list of functions
 */
GSList *
mg_server_get_functions_by_name (MgServer *srv, const gchar *funcname)
{
	GSList *list = NULL;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (funcname && *funcname, NULL);
	TO_IMPLEMENT;

	return list;
}


/**
 * mg_server_get_function_by_name_arg
 * @srv: a #MgServer object
 * @funcname: name of the function
 * @argtypes: a list of #MgServerDataType objects
 *
 * To find a DBMS functions which is uniquely identified by its name and the type(s)
 * of its argument(s).
 *
 * Returns: The function or NULL if not found
 */
MgServerFunction *
mg_server_get_function_by_name_arg (MgServer *srv, const gchar *funcname, const GSList *argtypes)
{
	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (funcname && *funcname, NULL);

	return mg_server_get_function_by_name_arg_real (srv->priv->functions, funcname, argtypes);
}

static MgServerFunction *
mg_server_get_function_by_name_arg_real (GSList *functions, const gchar *funcname, const GSList *argtypes)
{
	MgServerFunction *func = NULL;
	GSList *list;

	list = functions;
	while (list && !func) {
		gboolean argsok = TRUE;
		GSList *funcargs, *list2;

		/* arguments comparison */
		list2 = argtypes;
		funcargs = mg_server_function_get_arg_types (MG_SERVER_FUNCTION (list->data));
		while (funcargs && list2 && argsok) {
			if (funcargs->data != list2->data)
				argsok = FALSE;
			funcargs = g_slist_next (funcargs);
			list2 = g_slist_next (list2);
		}
		if (list2 || funcargs)
			argsok = FALSE;

		/* names comparison */
		if (argsok && 
		    !strcmp (funcname, mg_server_function_get_sqlname (MG_SERVER_FUNCTION (list->data))))
			func = MG_SERVER_FUNCTION (list->data);

		list = g_slist_next (list);
	}

	return func;
}

/**
 * mg_server_get_function_by_dbms_id
 * @srv: a #MgServer object
 * @dbms_id: 
 *
 * To find a DBMS functions which is uniquely identified by its DBMS identifier
 *
 * Returns: The function or NULL if not found
 */
MgServerFunction *
mg_server_get_function_by_dbms_id (MgServer *srv, const gchar *dbms_id)
{
	MgServerFunction *func = NULL;
	GSList *list;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (dbms_id && *dbms_id, NULL);

	list = srv->priv->functions;
	while (list && !func) {
		const gchar *str = mg_server_function_get_dbms_id (MG_SERVER_FUNCTION (list->data));
		if (!str || ! *str) {
			str = mg_server_function_get_sqlname (MG_SERVER_FUNCTION (list->data));
			g_error ("Function %p (%s) has no dbms_id", list->data, str);
		}
		if (str && !strcmp (dbms_id, str))
			func = MG_SERVER_FUNCTION (list->data);
		list = g_slist_next (list);
	}

	return func;
}

/**
 * mg_server_get_function_by_xml_id
 * @srv: a #MgServer object
 * @xml_id: 
 *
 * To find a DBMS functions which is uniquely identified by its XML identifier
 *
 * Returns: The function or NULL if not found
 */
MgServerFunction *
mg_server_get_function_by_xml_id (MgServer *srv, const gchar *xml_id)
{
	MgServerFunction *func = NULL;
	GSList *list;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (xml_id && *xml_id, NULL);

	list = srv->priv->functions;
	while (list && !func) {
		const gchar *str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
		if (!strcmp (xml_id, str))
			func = MG_SERVER_FUNCTION (list->data);
		list = g_slist_next (list);
	}

	return func;
}



/**
 * mg_server_get_aggregates
 * @srv: a #MgServer object
 *
 * To get the complete list of aggregates
 *
 * Returns: the allocated list of aggregates
 */
GSList *
mg_server_get_aggregates (MgServer *srv)
{
	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);

	if (srv->priv->functions)
		return g_slist_copy (srv->priv->aggregates);
	else
		return NULL;
}

/**
 * mg_server_get_aggregates_by_name
 * @srv: a #MgServer object
 * @aggname: the name of the aggregate
 *
 * To get the list of DBMS aggregates which match the given name.
 *
 * Returns: the allocated list of aggregates
 */
GSList *
mg_server_get_aggregates_by_name (MgServer *srv, const gchar *aggname)
{
	GSList *list = NULL;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (aggname && *aggname, NULL);
	TO_IMPLEMENT;

	return list;
}

/**
 * mg_server_get_aggregate_by_name_arg
 * @srv: a #MgServer object
 * @aggname: the name of the aggregate
 * @argtype: the type of argument or %NULL
 *
 * To find a DBMS functions which is uniquely identified by its name and the type
 * of its argument.
 *
 * Returns: The aggregate or NULL if not found
 */
MgServerAggregate *
mg_server_get_aggregate_by_name_arg (MgServer *srv, const gchar *aggname, MgServerDataType *argtype)
{
	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (aggname && *aggname, NULL);
	if (argtype)
		g_return_val_if_fail (IS_MG_SERVER_DATA_TYPE (argtype), NULL);

	return mg_server_get_aggregate_by_name_arg_real (srv->priv->aggregates, aggname, argtype);
}

MgServerAggregate *
mg_server_get_aggregate_by_name_arg_real (GSList *aggregates, const gchar *aggname, 
					  MgServerDataType *argtype)
{
	MgServerAggregate *agg = NULL;
	GSList *list;

	list = aggregates;
	while (list && !agg) {
		if ((mg_server_aggregate_get_arg_type (MG_SERVER_AGGREGATE (list->data)) == argtype) &&
		    !strcmp (aggname, mg_server_aggregate_get_sqlname (MG_SERVER_AGGREGATE (list->data))))
			agg = MG_SERVER_AGGREGATE (list->data);
		
		list = g_slist_next (list);
	}

	return agg;	
}

/**
 * mg_server_get_aggregate_by_dbms_id
 * @srv: a #MgServer object
 * @dbms_id: 
 *
 * To find a DBMS functions which is uniquely identified by its name and the type
 * of its argument.
 *
 * Returns: The aggregate or NULL if not found
 */
MgServerAggregate *
mg_server_get_aggregate_by_dbms_id (MgServer *srv, const gchar *dbms_id)
{
	MgServerAggregate *agg = NULL;
	GSList *list;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (dbms_id && *dbms_id, NULL);

	list = srv->priv->aggregates;
	while (list && !agg) {
		if (!strcmp (dbms_id, mg_server_aggregate_get_dbms_id (MG_SERVER_AGGREGATE (list->data))))
			agg = MG_SERVER_AGGREGATE (list->data);
		list = g_slist_next (list);
	}

	return agg;
}

/**
 * mg_server_get_aggregate_by_xml_id
 * @srv: a #MgServer object
 * @xml_id: 
 *
 * To find a DBMS aggregates which is uniquely identified by its XML identifier
 *
 * Returns: The aggregate or NULL if not found
 */
MgServerAggregate *
mg_server_get_aggregate_by_xml_id (MgServer *srv, const gchar *xml_id)
{
	MgServerAggregate *agg = NULL;
	GSList *list;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (xml_id && *xml_id, NULL);

	list = srv->priv->aggregates;
	while (list && !agg) {
		const gchar *str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
		if (!strcmp (xml_id, str))
			agg = MG_SERVER_AGGREGATE (list->data);
		list = g_slist_next (list);
	}

	return agg;
}


/**
 * mg_server_get_object_handler
 * @srv: a #MgServer object
 * @object: a #GObject object
 *
 * Get the right MgDataHandler object reference to manage data which is
 * "linked" to the object. The object will usually be a MgServerDataType, 
 * a MgServerFunction, or a MgServerAggregate.
 *
 * The returned MgDataHandler depends on the loaded plugins and on the user
 * preferences regarding how these plugins are to be used. If the user has not
 * set any preference for the object, then some default rules are used:
 * <ul>
 * <li> for MgServerDataType, the MgDataHandler corresponding to the gda type
 *    of the MgServerDataType is returned</li></ul>
 * -> for MgServerFunction, the MgDataHandler corresponding to the MgServerDataType
 *    returned by the function is returned
 * -> ...
 *
 * Returns: the MgDataHandler associated to the given object, NEVER returns NULL.
 */
MgDataHandler *
mg_server_get_object_handler (MgServer *srv, GObject *object)
{
        MgDataHandler *dh = NULL;
	GSList *list;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (object && G_IS_OBJECT (object), NULL);
	
	if ((dh = g_hash_table_lookup (srv->priv->types_objects_hash, object)))
		return dh;

	/* going through all the function handlers we have */
	list = srv->priv->handlers_functions;
	while (list && !dh) {
		dh = (HANDLER_FUNCTION (list->data)) (srv, object);
		list = g_slist_next (list);
	}

	if (!dh)
		return srv->priv->fallback_handler;
	else
		return dh;
}

/* called when an object which is a key in the hash table is destroyed */
static void
hash_object_destroyed_n (MgServer *srv, GObject *object) 
{
	g_hash_table_remove (srv->priv->types_objects_hash, object);
}

/**
 * mg_server_set_object_handler
 * @srv: a #MgServer object
 * @object: a #GObject
 * @handler: 
 *
 * This function is the opposite of the mg_server_get_object_handler() function:
 * it "attaches" a MgDataHandler object to any given object, and a subsequent call
 * to mg_server_get_object_handler(object) will return the MgDataHandler object.
 */
void
mg_server_set_object_handler (MgServer *srv, GObject *object, MgDataHandler *handler)
{
	g_return_if_fail (srv && IS_MG_SERVER (srv));
	g_return_if_fail (srv->priv);
	g_return_if_fail (object && G_IS_OBJECT (object));
	if (handler)
		g_return_if_fail (handler && IS_MG_DATA_HANDLER (handler));
	
	if (handler) {
		g_hash_table_insert (srv->priv->types_objects_hash, object, handler);
		g_object_weak_ref (object, (GWeakNotify) hash_object_destroyed_n, srv);
	}
	else {
		g_hash_table_remove (srv->priv->types_objects_hash, object);
		g_object_weak_ref (object, (GWeakNotify) hash_object_destroyed_n, srv);
	}
}

/**
 * mg_server_unset_object_handler
 * @srv: a #MgServer object
 * @object: a #GObject
 *
 * Shortcut to mg_server_set_object_handler() with NULL as "handler" argument.
 */
void
mg_server_unset_object_handler (MgServer *srv, GObject *object)
{
	g_return_if_fail (srv && IS_MG_SERVER (srv));
	g_return_if_fail (srv->priv);
	g_return_if_fail (object && G_IS_OBJECT (object));
	
	mg_server_set_object_handler (srv, object, NULL);	
}

/**
 * mg_server_object_has_handler
 * @srv: a #MgServer object
 * @object: a #GObject
 *
 * Tells if a MgDataHandler object has been assigned to the object given as argument, or
 * if the MgDataHandler which would be returned by the mg_server_get_object_handler()
 * function is a default one.
 *
 * Returns: TRUE if a MgDataHandler object has been assigned to the object.
 */
gboolean
mg_server_object_has_handler (MgServer *srv, GObject *object)
{
	gboolean found = FALSE;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), FALSE);
	g_return_val_if_fail (srv->priv, FALSE);
	g_return_val_if_fail (object && G_IS_OBJECT (object), FALSE);

	if (g_hash_table_lookup (srv->priv->types_objects_hash, object))
		found = TRUE;

	return found;
}

/**
 * mg_server_set_object_func_handler
 * @srv: a #MgServer object
 * @func: a function
 *
 * This function provides a way for the MgServer function to apply rules to
 * find an appropriate MgDataHandler for an object.
 */
void
mg_server_set_object_func_handler (MgServer *srv, MgDataHandler *(*func) (MgServer *, GObject *))
{
	g_return_if_fail (srv && IS_MG_SERVER (srv));
	g_return_if_fail (srv->priv);
	g_return_if_fail (func);
	
	if (g_slist_find (srv->priv->handlers_functions, func))
		return;

	srv->priv->handlers_functions = g_slist_append (srv->priv->handlers_functions, func);
}

static MgDataHandler *
func_handler_data_types (MgServer *srv, GObject *obj)
{
	if (IS_MG_SERVER_DATA_TYPE (obj)) {
		MgServerDataType *dt = MG_SERVER_DATA_TYPE (obj);
		return mg_server_get_handler_by_gda (srv, 
						     mg_server_data_type_get_gda_type (dt));
	}
	else
		return NULL;
}

/**
 * mg_server_stringify_value
 * @srv: a #MgServer object
 * @value: a value to be stringified
 *
 * Renders a value as a string, and ensures the string is UTF-8 encoded.
 *
 * Returns: the new string
 */
gchar *
mg_server_stringify_value (MgServer *srv, const GdaValue * value)
{
	gchar *retval, *str;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	g_return_val_if_fail (value, NULL);

        str = gda_value_stringify (value);
        retval = g_locale_to_utf8 (str, -1, NULL, NULL, NULL);
        g_free (str);

        return retval;
}

/**
 * mg_server_escape_chars
 * @srv: a #MgServer object
 * @string: 
 *
 * Escapes the special caracters from a string. The new string can then safely be sent
 * to the DBMS.
 *
 * Returns: the escaped string
 */
gchar *
mg_server_escape_chars (MgServer *srv, const gchar *string)
{
	gchar *str, *ptr, *ret, *retptr;
	gint size;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);
	if (!string)
		return NULL;
	
	str = g_strdup (string);
	ptr = str;

	/* determination of the new string size */
	size = 1;
	while (*ptr != '\0') {
		if (*ptr == '\'') {
			if (ptr == str)
				size += 2;
			else {
				if (*(ptr - 1) == '\\')
					size += 1;
				else
					size += 2;
			}
		}
		else
			size += 1;
		ptr++;
	}

	ptr = str;
	ret = (gchar *) malloc (sizeof (gchar) * size);
	retptr = ret;
	while (*ptr != '\0') {
		if (*ptr == '\'') {
			if (ptr == str) {
				*retptr = '\\';
				retptr++;
			}
			else if (*(ptr - 1) != '\\') {
				*retptr = '\\';
				retptr++;
			}
		}
		*retptr = *ptr;
		retptr++;
		ptr++;
	}
	*retptr = '\0';
	g_free (str);

	return ret;
}


/**
 * mg_server_get_gda_schema
 * @srv: a #MgServer object
 * @schema: the requested schema
 * @params: some parameters for the requested schema, or NULL
 *
 * Get a direct access to the libgda's function call to get a DBMS schema. It should not directely be used
 * since libmergeant hides any necessary call to it.
 *
 * Returns: the data model, or NULL if an error occured
 */
GdaDataModel *
mg_server_get_gda_schema (MgServer *srv, GdaConnectionSchema schema, GdaParameterList *params)
{
	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (srv->priv, NULL);

	return gda_connection_get_schema (GDA_CONNECTION (srv->priv->cnc), schema, params);
}
