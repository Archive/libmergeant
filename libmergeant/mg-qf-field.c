/* mg-qf-field.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-qf-field.h"
#include "mg-xml-storage.h"
#include "mg-field.h"
#include "mg-entity.h"
#include "mg-renderer.h"
#include "mg-referer.h"
#include "mg-ref-base.h"
#include "marshal.h"
#include "mg-query.h"
#include "mg-target.h"
#include <string.h>

/* 
 * Main static functions 
 */
static void mg_qf_field_class_init (MgQfFieldClass * class);
static void mg_qf_field_init (MgQfField *qf);
static void mg_qf_field_dispose (GObject *object);
static void mg_qf_field_finalize (GObject *object);

static void mg_qf_field_set_property (GObject              *object,
				      guint                 param_id,
				      const GValue         *value,
				      GParamSpec           *pspec);
static void mg_qf_field_get_property (GObject              *object,
				      guint                 param_id,
				      GValue               *value,
				      GParamSpec           *pspec);

/* XML storage interface */
static void        mg_qf_field_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_qf_field_get_xml_id (MgXmlStorage *iface);
static xmlNodePtr  mg_qf_field_save_to_xml (MgXmlStorage *iface, GError **error);
static gboolean    mg_qf_field_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error);

/* Field interface */
static void              mg_qf_field_field_init      (MgFieldIface *iface);
static MgEntity         *mg_qf_field_get_entity      (MgField *iface);
static MgServerDataType *mg_qf_field_get_data_type   (MgField *iface);
static const gchar      *mg_qf_field_get_name        (MgField *iface);
static const gchar      *mg_qf_field_get_description (MgField *iface);

/* Renderer interface */
static void            mg_qf_field_renderer_init   (MgRendererIface *iface);
static GdaXqlItem     *mg_qf_field_render_as_xql   (MgRenderer *iface, MgContext *context, GError **error);
static gchar          *mg_qf_field_render_as_sql   (MgRenderer *iface, MgContext *context, GError **error);
static gchar          *mg_qf_field_render_as_str   (MgRenderer *iface, MgContext *context);

/* Referer interface */
static void        mg_qf_field_referer_init        (MgRefererIface *iface);
static gboolean    mg_qf_field_activate            (MgReferer *iface);
static void        mg_qf_field_deactivate          (MgReferer *iface);
static gboolean    mg_qf_field_is_active           (MgReferer *iface);
static GSList     *mg_qf_field_get_ref_objects     (MgReferer *iface);
static void        mg_qf_field_replace_refs        (MgReferer *iface, GHashTable *replacements);

/* virtual functions */
static GObject    *mg_qf_field_copy           (MgQfield *orig);
static gboolean    mg_qf_field_is_equal       (MgQfield *qfield1, MgQfield *qfield2);

#ifdef debug
static void        mg_qf_field_dump           (MgQfField *field, guint offset);
#endif


/* When the MgQuery or MgTarget is nullified */
static void nullified_object_cb (GObject *obj, MgQfField *field);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	TEMPL_SIGNAL,
	LAST_SIGNAL
};

static gint mg_qf_field_signals[LAST_SIGNAL] = { 0 };

/* properties */
enum
{
	PROP_0,
	PROP_QUERY,
	PROP_VALUE_PROVIDER_OBJECT,
	PROP_VALUE_PROVIDER_XML_ID,
	PROP_HANDLER_PLUGIN
};


/* private structure */
struct _MgQfFieldPrivate
{
	MgQuery    *query;
	MgRefBase  *target_ref; /* references a MgTarget */
	MgRefBase  *field_ref;  /* references a MgField in the entity behind the MgTarget */
	MgRefBase  *value_prov_ref;

	gchar       *plugin;       /* specific plugin to be used */
};


/* module error */
GQuark mg_qf_field_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_qf_field_error");
	return quark;
}


guint
mg_qf_field_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgQfFieldClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_qf_field_class_init,
			NULL,
			NULL,
			sizeof (MgQfField),
			0,
			(GInstanceInitFunc) mg_qf_field_init
		};

		static const GInterfaceInfo xml_storage_info = {
			(GInterfaceInitFunc) mg_qf_field_xml_storage_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo field_info = {
			(GInterfaceInitFunc) mg_qf_field_field_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo renderer_info = {
			(GInterfaceInitFunc) mg_qf_field_renderer_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo referer_info = {
			(GInterfaceInitFunc) mg_qf_field_referer_init,
			NULL,
			NULL
		};
		
		
		type = g_type_register_static (MG_QFIELD_TYPE, "MgQfField", &info, 0);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
		g_type_add_interface_static (type, MG_FIELD_TYPE, &field_info);
		g_type_add_interface_static (type, MG_RENDERER_TYPE, &renderer_info);
		g_type_add_interface_static (type, MG_REFERER_TYPE, &referer_info);
	}
	return type;
}

static void 
mg_qf_field_xml_storage_init (MgXmlStorageIface *iface)
{
	iface->get_xml_id = mg_qf_field_get_xml_id;
	iface->save_to_xml = mg_qf_field_save_to_xml;
	iface->load_from_xml = mg_qf_field_load_from_xml;
}

static void
mg_qf_field_field_init (MgFieldIface *iface)
{
	iface->get_entity = mg_qf_field_get_entity;
	iface->get_data_type = mg_qf_field_get_data_type;
	iface->get_name = mg_qf_field_get_name;
	iface->get_description = mg_qf_field_get_description;
}

static void
mg_qf_field_renderer_init (MgRendererIface *iface)
{
	iface->render_as_xql = mg_qf_field_render_as_xql;
	iface->render_as_sql = mg_qf_field_render_as_sql;
	iface->render_as_str = mg_qf_field_render_as_str;
}

static void
mg_qf_field_referer_init (MgRefererIface *iface)
{
        iface->activate = mg_qf_field_activate;
        iface->deactivate = mg_qf_field_deactivate;
        iface->is_active = mg_qf_field_is_active;
        iface->get_ref_objects = mg_qf_field_get_ref_objects;
        iface->replace_refs = mg_qf_field_replace_refs;
}

static void
mg_qf_field_class_init (MgQfFieldClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_qf_field_signals[TEMPL_SIGNAL] =
		g_signal_new ("templ_signal",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgQfFieldClass, templ_signal),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	class->templ_signal = NULL;

	object_class->dispose = mg_qf_field_dispose;
	object_class->finalize = mg_qf_field_finalize;

	/* Properties */
	object_class->set_property = mg_qf_field_set_property;
	object_class->get_property = mg_qf_field_get_property;
	g_object_class_install_property (object_class, PROP_QUERY,
					 g_param_spec_pointer ("query", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property (object_class, PROP_VALUE_PROVIDER_OBJECT,
					 g_param_spec_pointer ("value_provider", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property (object_class, PROP_VALUE_PROVIDER_XML_ID,
					 g_param_spec_string ("value_provider_xml_id", NULL, NULL, NULL,
							      (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property (object_class, PROP_HANDLER_PLUGIN,
                                         g_param_spec_string ("handler_plugin", NULL, NULL, NULL,
                                                              (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	
	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_qf_field_dump;
#endif
	MG_QFIELD_CLASS (class)->copy = mg_qf_field_copy;
	MG_QFIELD_CLASS (class)->is_equal = mg_qf_field_is_equal;
	MG_QFIELD_CLASS (class)->is_list = NULL;
	MG_QFIELD_CLASS (class)->get_params = NULL;
}

static void
mg_qf_field_init (MgQfField *mg_qf_field)
{
	mg_qf_field->priv = g_new0 (MgQfFieldPrivate, 1);
	mg_qf_field->priv->query = NULL;
	mg_qf_field->priv->target_ref = NULL;
	mg_qf_field->priv->field_ref = NULL;
	mg_qf_field->priv->value_prov_ref = NULL;
	mg_qf_field->priv->plugin = NULL;
}


/**
 * mg_qf_field_new_with_objects
 * @query: a #MgQuery in which the new object will be
 * @target: a #MgTarget object
 * @field: an object which implements the #MgField interface
 *
 * Creates a new MgQfField object which represents the @field of the entity represented
 * by @target. For example if @target represents my_table, and @field represents the "name" field,
 * then the created object would represent 'my_table.name' in SQL notation
 *
 * REM: @field MUST belong to the #MgEntity represented by @target (we need the @target since
 * an entity can appear several times within a query, for self joins for example).
 *
 * Returns: the new object
 */
GObject*
mg_qf_field_new_with_objects (MgQuery *query, MgTarget *target, MgField *field)
{
	GObject   *obj;
	MgQfField *mg_qf_field;
	MgConf *conf;
	MgEntity *ent_f, *ent_t;
	guint id;

	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (target && IS_MG_TARGET (target), NULL);
	g_return_val_if_fail (mg_target_get_query (target) == query, NULL);
	g_return_val_if_fail (mg_referer_activate (MG_REFERER (target)), NULL);
	g_return_val_if_fail (field && IS_MG_FIELD (field), NULL);
	ent_f = mg_field_get_entity (field);
	ent_t = mg_target_get_represented_entity (target);
	g_return_val_if_fail (ent_f == ent_t, NULL);

	conf = mg_base_get_conf (MG_BASE (query));

	obj = g_object_new (MG_QF_FIELD_TYPE, "conf", conf, NULL);
	mg_qf_field = MG_QF_FIELD (obj);
	g_object_get (G_OBJECT (query), "field_serial", &id, NULL);
	mg_base_set_id (MG_BASE (mg_qf_field), id);

	mg_qf_field->priv->query = query;
	g_signal_connect (G_OBJECT (query), "nullified",
			  G_CALLBACK (nullified_object_cb), mg_qf_field);

	mg_qf_field->priv->target_ref = MG_REF_BASE (mg_ref_base_new (conf));
	mg_ref_base_set_ref_object (mg_qf_field->priv->target_ref, MG_BASE (target));

	mg_qf_field->priv->field_ref = MG_REF_BASE (mg_ref_base_new (conf));
	mg_ref_base_set_ref_object (mg_qf_field->priv->field_ref, MG_BASE (field));
	
	return obj;
}

/**
 * mg_qf_field_new_with_xml_id
 * @query: a #MgQuery in which the new object will be
 * @target_xml_id: the XML Id of a #MgTarget object
 * @field_xml_id: the XML Id of an object which implements the #MgField interface
 *
 * Creates a new MgQfField object which represents a given field of the entity represented
 * by the target identified by @target_xml_id. 
 *
 * REM: the aimed field MUST belong the the entity represented by the aimed target. This test is
 * not done here but when this object will be activated (activation wll be refused if that test fails).
 *
 * Returns: the new object
 */
GObject*
mg_qf_field_new_with_xml_ids (MgQuery *query, const gchar *target_xml_id, const gchar *field_xml_id)
{
	GObject   *obj;
	MgQfField *mg_qf_field;
	MgConf *conf;
	gchar *str, *ptr, *tok, *qid;
	guint id;

	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (target_xml_id && *target_xml_id, NULL);
	g_return_val_if_fail (field_xml_id && *field_xml_id, NULL);

	qid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (query));
	str = g_strdup (target_xml_id);
	ptr = strtok_r (str, ":", &tok);
        g_return_val_if_fail (!strcmp (ptr, qid), NULL);
	g_free (qid);
	g_free (str);

	conf = mg_base_get_conf (MG_BASE (query));
	obj = g_object_new (MG_QF_FIELD_TYPE, "conf", conf, NULL);
	mg_qf_field = MG_QF_FIELD (obj);
	g_object_get (G_OBJECT (query), "field_serial", &id, NULL);
	mg_base_set_id (MG_BASE (mg_qf_field), id);

	mg_qf_field->priv->query = query;
	g_signal_connect (G_OBJECT (query), "nullified",
			  G_CALLBACK (nullified_object_cb), mg_qf_field);

	mg_qf_field->priv->target_ref = MG_REF_BASE (mg_ref_base_new (conf));
	mg_ref_base_set_ref_name (mg_qf_field->priv->target_ref, MG_TARGET_TYPE, REFERENCE_BY_XML_ID, target_xml_id);

	mg_qf_field->priv->field_ref = MG_REF_BASE (mg_ref_base_new (conf));
	mg_ref_base_set_ref_name (mg_qf_field->priv->field_ref, MG_FIELD_TYPE, REFERENCE_BY_XML_ID, field_xml_id);
	
	return obj;
}

static void 
nullified_object_cb (GObject *obj, MgQfField *field)
{
	mg_base_nullify (MG_BASE (field));
}

static void
mg_qf_field_dispose (GObject *object)
{
	MgQfField *mg_qf_field;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_QF_FIELD (object));

	mg_qf_field = MG_QF_FIELD (object);
	if (mg_qf_field->priv) {
		mg_base_nullify_check (MG_BASE (object));

		if (mg_qf_field->priv->value_prov_ref) {
			g_object_unref (G_OBJECT (mg_qf_field->priv->value_prov_ref));
			mg_qf_field->priv->value_prov_ref = NULL;
		}

		if (mg_qf_field->priv->query) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (mg_qf_field->priv->query),
							      G_CALLBACK (nullified_object_cb), mg_qf_field);
			mg_qf_field->priv->query = NULL;
		}
		if (mg_qf_field->priv->target_ref) {
			g_object_unref (G_OBJECT (mg_qf_field->priv->target_ref));
			mg_qf_field->priv->target_ref = NULL;
		}
		if (mg_qf_field->priv->field_ref) {
			g_object_unref (G_OBJECT (mg_qf_field->priv->field_ref));
			mg_qf_field->priv->field_ref = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_qf_field_finalize (GObject   * object)
{
	MgQfField *mg_qf_field;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_QF_FIELD (object));

	mg_qf_field = MG_QF_FIELD (object);
	if (mg_qf_field->priv) {
		if (mg_qf_field->priv->plugin)
			g_free (mg_qf_field->priv->plugin);

		g_free (mg_qf_field->priv);
		mg_qf_field->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_qf_field_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	MgQfField *mg_qf_field;
	gpointer ptr;
	const gchar *val;

	mg_qf_field = MG_QF_FIELD (object);
	if (mg_qf_field->priv) {
		switch (param_id) {
		case PROP_QUERY:
			ptr = g_value_get_pointer (value);
			g_return_if_fail (ptr && IS_MG_QUERY (ptr));

			if (mg_qf_field->priv->query) {
				if (mg_qf_field->priv->query == MG_QUERY (ptr))
					return;

				g_signal_handlers_disconnect_by_func (G_OBJECT (mg_qf_field->priv->query),
								      G_CALLBACK (nullified_object_cb), mg_qf_field);
			}

			mg_qf_field->priv->query = MG_QUERY (ptr);
			g_signal_connect (G_OBJECT (ptr), "nullified",
					  G_CALLBACK (nullified_object_cb), mg_qf_field);
			break;
		case PROP_VALUE_PROVIDER_OBJECT:
			ptr = g_value_get_pointer (value);
			if (ptr) {
				g_return_if_fail (IS_MG_QFIELD (ptr));
				g_return_if_fail (mg_field_get_entity (MG_FIELD (ptr)) == 
						  MG_ENTITY (mg_qf_field->priv->query));
				if (!mg_qf_field->priv->value_prov_ref)
					mg_qf_field->priv->value_prov_ref = MG_REF_BASE (mg_ref_base_new (mg_base_get_conf (MG_BASE (mg_qf_field))));
				mg_ref_base_set_ref_object_type (mg_qf_field->priv->value_prov_ref,
								 ptr, MG_FIELD_TYPE);
			}
			else {
				if (mg_qf_field->priv->value_prov_ref) {
					g_object_unref (G_OBJECT (mg_qf_field->priv->value_prov_ref));
					mg_qf_field->priv->value_prov_ref = NULL;
				}
			}
			break;
		case PROP_VALUE_PROVIDER_XML_ID:
			val = g_value_get_string (value);
			if (val && *val) {
				gchar *qid, *str, *start, *tok;

				str = g_strdup (val);
				start = strtok_r (str, ":", &tok);
				qid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (mg_qf_field->priv->query));
				g_return_if_fail (!strcmp (start, qid));
				g_free (str);
				g_free (qid);

				if (!mg_qf_field->priv->value_prov_ref)
					mg_qf_field->priv->value_prov_ref = MG_REF_BASE (mg_ref_base_new (mg_base_get_conf (MG_BASE (mg_qf_field))));
				mg_ref_base_set_ref_name (mg_qf_field->priv->value_prov_ref,
							  MG_FIELD_TYPE, REFERENCE_BY_XML_ID, val);
			}
			else {
				if (mg_qf_field->priv->value_prov_ref) {
					g_object_unref (G_OBJECT (mg_qf_field->priv->value_prov_ref));
					mg_qf_field->priv->value_prov_ref = NULL;
				}
			}
			break;
		case PROP_HANDLER_PLUGIN:
			val =  g_value_get_string (value);
			if (mg_qf_field->priv->plugin) {
				g_free (mg_qf_field->priv->plugin);
				mg_qf_field->priv->plugin = NULL;
			}
			if (val)
				mg_qf_field->priv->plugin = g_strdup (val);
			break;
		}
	}
}

static void
mg_qf_field_get_property (GObject              *object,
			guint                 param_id,
			GValue               *value,
			GParamSpec           *pspec)
{
	MgQfField *mg_qf_field;
	mg_qf_field = MG_QF_FIELD (object);
	
	if (mg_qf_field->priv) {
		switch (param_id) {
		case PROP_QUERY:
			g_value_set_pointer (value, mg_qf_field->priv->query);
			break;
		case PROP_VALUE_PROVIDER_OBJECT:
			if (mg_qf_field->priv->value_prov_ref)
				g_value_set_pointer (value, 
						     mg_ref_base_get_ref_object (mg_qf_field->priv->value_prov_ref));
			else
				g_value_set_pointer (value, NULL);
			break;
		case PROP_VALUE_PROVIDER_XML_ID:
			if (mg_qf_field->priv->value_prov_ref)
				g_value_set_string (value, 
						    mg_ref_base_get_ref_name (mg_qf_field->priv->value_prov_ref,
									      NULL, NULL));
			else
				g_value_set_string (value, NULL);
			break;
		case PROP_HANDLER_PLUGIN:
			g_value_set_string (value, mg_qf_field->priv->plugin);
			break;
		}	
	}
}

static GObject *
mg_qf_field_copy (MgQfield *orig)
{
	MgQfField *qf;
	GObject *obj;
	g_assert (IS_MG_QF_FIELD (orig));
	qf = MG_QF_FIELD (orig);

	obj = mg_qf_field_new_with_xml_ids (qf->priv->query, 
					    mg_ref_base_get_ref_name (qf->priv->target_ref, NULL, NULL),
					    mg_ref_base_get_ref_name (qf->priv->field_ref, NULL, NULL));
	if (mg_base_get_name (MG_BASE (orig)))
		mg_base_set_name (MG_BASE (obj), mg_base_get_name (MG_BASE (orig)));

	if (mg_base_get_description (MG_BASE (orig)))
		mg_base_set_description (MG_BASE (obj), mg_base_get_description (MG_BASE (orig)));

	if (qf->priv->value_prov_ref) {
		MgBase *ref = mg_ref_base_get_ref_object (qf->priv->value_prov_ref);
		if (ref)
			g_object_set (obj, "value_provider", ref, NULL);
		else
			g_object_set (obj, "value_provider_xml_id",
				      mg_ref_base_get_ref_name (qf->priv->value_prov_ref, NULL, NULL), NULL);
	}

	if (qf->priv->plugin)
		MG_QF_FIELD (obj)->priv->plugin = g_strdup (qf->priv->plugin);

	return obj;
}

static gboolean
mg_qf_field_is_equal (MgQfield *qfield1, MgQfield *qfield2)
{
	const gchar *ref1, *ref2;
	gboolean retval;
	g_assert (IS_MG_QF_FIELD (qfield1));
	g_assert (IS_MG_QF_FIELD (qfield2));
	
	/* it is here assumed that qfield1 and qfield2 are of the same type and refer to the same
	   query */
	ref1 = mg_ref_base_get_ref_name (MG_QF_FIELD (qfield1)->priv->target_ref, NULL, NULL);
	ref2 = mg_ref_base_get_ref_name (MG_QF_FIELD (qfield2)->priv->target_ref, NULL, NULL);

	retval = !strcmp (ref1, ref2) ? TRUE : FALSE;
	if (retval) {
		ref1 = mg_ref_base_get_ref_name (MG_QF_FIELD (qfield1)->priv->field_ref, NULL, NULL);
		ref2 = mg_ref_base_get_ref_name (MG_QF_FIELD (qfield2)->priv->field_ref, NULL, NULL);
		retval = !strcmp (ref1, ref2) ? TRUE : FALSE;
	}

	return retval;
}

/**
 * mg_qf_field_get_ref_field
 * @field: a #MgQfField object
 *
 * Get the real #MgField object (well, the object which implements that interface)
 * referenced by @field
 *
 * Returns: the #MgField object, or NULL if @field is not active
 */
MgField *
mg_qf_field_get_ref_field (MgQfField *field)
{
	MgBase *base;
	g_return_val_if_fail (field && IS_MG_QF_FIELD (field), NULL);
	g_return_val_if_fail (field->priv, NULL);

	base = mg_ref_base_get_ref_object (field->priv->field_ref);
	if (base)
		return MG_FIELD (base);
	else
		return NULL;
}

/**
 * mg_qf_field_get_target
 * @field: a #MgQfField object
 *
 * Get the #MgTarget object @field 'belongs' to
 *
 * Returns: the #MgTarget object
 */
MgTarget *
mg_qf_field_get_target (MgQfField *field)
{
	MgBase *base;
	g_return_val_if_fail (field && IS_MG_QF_FIELD (field), NULL);
	g_return_val_if_fail (field->priv, NULL);

	base = mg_ref_base_get_ref_object (field->priv->target_ref);
	if (base)
		return MG_TARGET (base);
	else
		return NULL;
}

#ifdef debug
static void
mg_qf_field_dump (MgQfField *field, guint offset)
{
	gchar *str;
	gint i;

	g_return_if_fail (field && IS_MG_QF_FIELD (field));
	
        /* string for the offset */
        str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;

        /* dump */
        if (field->priv) {
                g_print ("%s" D_COL_H1 "MgQfField" D_COL_NOR " \"%s\" (%p, id=%d) ",
                         str, mg_base_get_name (MG_BASE (field)), field, mg_base_get_id (MG_BASE (field)));
		if (mg_qf_field_is_active (MG_REFERER (field)))
			g_print ("Active, ");
		else
			g_print (D_COL_ERR "Inactive" D_COL_NOR ", ");
		if (mg_qfield_is_visible (MG_QFIELD (field)))
			g_print ("Visible, ");
		g_print ("references Target %p (%s) & Field %p (%s)", 
			 mg_ref_base_get_ref_object (field->priv->target_ref),
			 mg_ref_base_get_ref_name (field->priv->target_ref, NULL, NULL),
			 mg_ref_base_get_ref_object (field->priv->field_ref),
			 mg_ref_base_get_ref_name (field->priv->field_ref, NULL, NULL));

		if (field->priv->value_prov_ref) 
			g_print (" Value prov: %p (%s)\n",
				 mg_ref_base_get_ref_object (field->priv->value_prov_ref),
				 mg_ref_base_get_ref_name (field->priv->value_prov_ref, NULL, NULL));
		else
			g_print ("\n");
	}
        else
                g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, field);
}
#endif


/* 
 * MgField interface implementation
 */
static MgEntity *
mg_qf_field_get_entity (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_QF_FIELD (iface), NULL);
	g_return_val_if_fail (MG_QF_FIELD (iface)->priv, NULL);

	return MG_ENTITY (MG_QF_FIELD (iface)->priv->query);
}

static MgServerDataType *
mg_qf_field_get_data_type (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_QF_FIELD (iface), NULL);
	g_return_val_if_fail (MG_QF_FIELD (iface)->priv, NULL);
	
	if (mg_qf_field_activate (MG_REFERER (iface))) {
		MgField *field;
		field = MG_FIELD (mg_ref_base_get_ref_object (MG_QF_FIELD (iface)->priv->field_ref));
		return mg_field_get_data_type (field);
	}

	return NULL;
}

static const gchar *
mg_qf_field_get_name (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_QF_FIELD (iface), NULL);

	return mg_base_get_name (MG_BASE (iface));
}

static const gchar *
mg_qf_field_get_description (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_QF_FIELD (iface), NULL);
	
	return mg_base_get_description (MG_BASE (iface));	
}


/* 
 * MgXmlStorage interface implementation
 */
static gchar *
mg_qf_field_get_xml_id (MgXmlStorage *iface)
{
	gchar *q_xml_id, *xml_id;

	g_return_val_if_fail (iface && IS_MG_QF_FIELD (iface), NULL);
	g_return_val_if_fail (MG_QF_FIELD (iface)->priv, NULL);

	q_xml_id = mg_xml_storage_get_xml_id (MG_XML_STORAGE (MG_QF_FIELD (iface)->priv->query));
	xml_id = g_strdup_printf ("%s:QF%d", q_xml_id, mg_base_get_id (MG_BASE (iface)));
	g_free (q_xml_id);
	
	return xml_id;
}

static xmlNodePtr
mg_qf_field_save_to_xml (MgXmlStorage *iface, GError **error)
{
	xmlNodePtr node = NULL;
	MgQfField *field;
	gchar *str;

	g_return_val_if_fail (iface && IS_MG_QF_FIELD (iface), NULL);
	g_return_val_if_fail (MG_QF_FIELD (iface)->priv, NULL);

	field = MG_QF_FIELD (iface);

	node = xmlNewNode (NULL, "MG_QF");
	
	str = mg_qf_field_get_xml_id (iface);
	xmlSetProp (node, "id", str);
	g_free (str);

	xmlSetProp (node, "type", "FIELD");
	xmlSetProp (node, "name", mg_base_get_name (MG_BASE (field)));
	if (mg_base_get_description (MG_BASE (field)) && *mg_base_get_description (MG_BASE (field)))
		xmlSetProp (node, "descr", mg_base_get_description (MG_BASE (field)));
	xmlSetProp (node, "target", mg_ref_base_get_ref_name (field->priv->target_ref, NULL, NULL));
	xmlSetProp (node, "object", mg_ref_base_get_ref_name (field->priv->field_ref, NULL, NULL));
	if (! mg_qfield_is_visible (MG_QFIELD (field)))
		xmlSetProp (node, "is_visible",  "f");
	if (mg_qfield_is_internal (MG_QFIELD (field)))
		xmlSetProp (node, "is_internal", "t");

	if (field->priv->value_prov_ref)
		xmlSetProp (node, "value_prov", mg_ref_base_get_ref_name (field->priv->value_prov_ref, NULL, NULL));

	if (field->priv->plugin)
		xmlSetProp (node, "plugin", field->priv->plugin);

	return node;
}

static gboolean
mg_qf_field_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgQfField *field;
	gchar *prop;
	gboolean target = FALSE;

	g_return_val_if_fail (iface && IS_MG_QF_FIELD (iface), FALSE);
	g_return_val_if_fail (MG_QF_FIELD (iface)->priv, FALSE);
	g_return_val_if_fail (node, FALSE);

	field = MG_QF_FIELD (iface);
	if (strcmp (node->name, "MG_QF")) {
		g_set_error (error,
			     MG_QF_FIELD_ERROR,
			     MG_QF_FIELD_XML_LOAD_ERROR,
			     _("XML Tag is not <MG_QF>"));
		return FALSE;
	}

	prop = xmlGetProp (node, "type");
	if (prop) {
		if (strcmp (prop, "FIELD")) {
			g_set_error (error,
				     MG_QF_FIELD_ERROR,
				     MG_QF_FIELD_XML_LOAD_ERROR,
				     _("Wrong type of field in <MG_QF>"));
			return FALSE;
		}
		g_free (prop);
	}

	prop = xmlGetProp (node, "id");
	if (prop) {
		gchar *ptr, *tok;
		ptr = strtok_r (prop, ":", &tok);
		ptr = strtok_r (NULL, ":", &tok);
		if (strlen (ptr) < 3) {
			g_set_error (error,
				     MG_QF_FIELD_ERROR,
				     MG_QF_FIELD_XML_LOAD_ERROR,
				     _("Wrong 'id' attribute in <MG_QF>"));
			return FALSE;
		}
		mg_base_set_id (MG_BASE (field), atoi (ptr+2));
		g_free (prop);
	}

	prop = xmlGetProp (node, "name");
	if (prop) {
		mg_base_set_name (MG_BASE (field), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "descr");
	if (prop) {
		mg_base_set_description (MG_BASE (field), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "target");
	if (prop) {
		target = TRUE;
		mg_ref_base_set_ref_name (field->priv->target_ref, MG_TARGET_TYPE, REFERENCE_BY_XML_ID, prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "object");
	if (prop) {
		target = TRUE;
		mg_ref_base_set_ref_name (field->priv->field_ref, MG_FIELD_TYPE, REFERENCE_BY_XML_ID, prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "is_visible");
	if (prop) {
		mg_qfield_set_visible (MG_QFIELD (field), (*prop == 't') ? TRUE : FALSE);
		g_free (prop);
	}

	prop = xmlGetProp (node, "is_internal");
	if (prop) {
		mg_qfield_set_internal (MG_QFIELD (field), (*prop == 't') ? TRUE : FALSE);
		g_free (prop);
	}

	prop = xmlGetProp (node, "value_prov");
	if (prop) {
		g_object_set (G_OBJECT (iface), "value_provider_xml_id", prop, NULL);
		g_free (prop);
	}

	prop = xmlGetProp (node, "plugin");
	if (prop) 
		field->priv->plugin = prop;

	if (target)
		return TRUE;
	else {
		g_set_error (error,
			     MG_QF_FIELD_ERROR,
			     MG_QF_FIELD_XML_LOAD_ERROR,
			     _("Missing required attributes for <MG_QF>"));
		return FALSE;
	}
}


/*
 * MgRenderer interface implementation
 */
static GdaXqlItem *
mg_qf_field_render_as_xql (MgRenderer *iface, MgContext *context, GError **error)
{
	GdaXqlItem *node = NULL;

	g_return_val_if_fail (iface && IS_MG_QF_FIELD (iface), NULL);
	g_return_val_if_fail (MG_QF_FIELD (iface)->priv, NULL);
	
	TO_IMPLEMENT;
	return node;
}

static gchar *
mg_qf_field_render_as_sql (MgRenderer *iface, MgContext *context, GError **error)
{
	gchar *str = NULL;
	MgBase *base, *base2;
	MgQfField *field;

	g_return_val_if_fail (iface && IS_MG_QF_FIELD (iface), NULL);
	g_return_val_if_fail (MG_QF_FIELD (iface)->priv, NULL);
	field = MG_QF_FIELD (iface);

	base = mg_ref_base_get_ref_object (field->priv->target_ref);
	base2 = mg_ref_base_get_ref_object (field->priv->field_ref);

	if (base && base2) {
		MgQuery *query;

		/* if the query is a SELECT query, then we qualify the field with the target */
		query = mg_target_get_query (MG_TARGET (base));
		if (mg_query_is_modif_query (query))
			str = g_strdup_printf ("%s", mg_field_get_name (MG_FIELD (base2)));
		else
			str = g_strdup_printf ("%s.%s", mg_target_get_alias (MG_TARGET (base)),
					       mg_field_get_name (MG_FIELD (base2)));
	}
	else {
		if (base)
			g_set_error (error,
				     MG_QF_FIELD_ERROR,
				     MG_QF_FIELD_RENDER_ERROR,
				     _("Can't find target '%s'"), mg_ref_base_get_ref_name (field->priv->target_ref,
											    NULL, NULL));
		else
			g_set_error (error,
				     MG_QF_FIELD_ERROR,
				     MG_QF_FIELD_RENDER_ERROR,
				     _("Can't find field '%s'"), mg_ref_base_get_ref_name (field->priv->field_ref,
											   NULL, NULL));
	}

	
	return str;
}

static gchar *
mg_qf_field_render_as_str (MgRenderer *iface, MgContext *context)
{
	gchar *str = NULL;
	MgBase *base, *base2;
	MgQfField *field;

	g_return_val_if_fail (iface && IS_MG_QF_FIELD (iface), NULL);
	g_return_val_if_fail (MG_QF_FIELD (iface)->priv, NULL);
	field = MG_QF_FIELD (iface);

	base = mg_ref_base_get_ref_object (field->priv->target_ref);
	base2 = mg_ref_base_get_ref_object (field->priv->field_ref);

	if (base && base2) {
		MgEntity *ent = mg_target_get_represented_entity (MG_TARGET (base));
		str = g_strdup_printf ("%s(%s).%s", mg_base_get_name (MG_BASE (ent)), 
				       mg_target_get_alias (MG_TARGET (base)),
				       mg_field_get_name (MG_FIELD (base2)));
	}
	else 
		str = g_strdup (_("Non activated field"));
	
	return str;
}


/*
 * MgReferer interface implementation
 */
static gboolean
mg_qf_field_activate (MgReferer *iface)
{
	gboolean act1, act2;
	gboolean active = FALSE;
	MgQfField *field;

	g_return_val_if_fail (iface && IS_MG_QF_FIELD (iface), FALSE);
	g_return_val_if_fail (MG_QF_FIELD (iface)->priv, FALSE);
	field = MG_QF_FIELD (iface);

	act1 = mg_ref_base_activate (field->priv->target_ref);
	act2 = mg_ref_base_activate (field->priv->field_ref);
	if (act1 && act2) {
		/* coherence test */
		MgTarget *target;
		MgField *rfield;

		target = MG_TARGET (mg_ref_base_get_ref_object (field->priv->target_ref));
		rfield = MG_FIELD (mg_ref_base_get_ref_object (field->priv->field_ref));
		if (mg_target_get_represented_entity (target) != mg_field_get_entity (rfield))
			mg_ref_base_deactivate (field->priv->field_ref);
		else
			active = TRUE;
	}

	if (active && field->priv->value_prov_ref)
			active = mg_ref_base_activate (field->priv->value_prov_ref);

	return active;
}

static void
mg_qf_field_deactivate (MgReferer *iface)
{
	MgQfField *field;
	g_return_if_fail (iface && IS_MG_QF_FIELD (iface));
	g_return_if_fail (MG_QF_FIELD (iface)->priv);
	field = MG_QF_FIELD (iface);

	mg_ref_base_deactivate (field->priv->target_ref);
	mg_ref_base_deactivate (field->priv->field_ref);
	if (field->priv->value_prov_ref)
		mg_ref_base_deactivate (field->priv->value_prov_ref);
}

static gboolean
mg_qf_field_is_active (MgReferer *iface)
{
	gboolean active;
	MgQfField *field;

	g_return_val_if_fail (iface && IS_MG_QF_FIELD (iface), FALSE);
	g_return_val_if_fail (MG_QF_FIELD (iface)->priv, FALSE);
	field = MG_QF_FIELD (iface);

	active =  mg_ref_base_is_active (field->priv->target_ref) &&
		mg_ref_base_is_active (field->priv->field_ref);

	if (active && field->priv->value_prov_ref)
		active = mg_ref_base_is_active (field->priv->value_prov_ref);

	return active;
}

static GSList *
mg_qf_field_get_ref_objects (MgReferer *iface)
{
	GSList *list = NULL;
        MgBase *base;
	MgQfField *field;

	g_return_val_if_fail (iface && IS_MG_QF_FIELD (iface), NULL);
	g_return_val_if_fail (MG_QF_FIELD (iface)->priv, NULL);
	field = MG_QF_FIELD (iface);

        base = mg_ref_base_get_ref_object (field->priv->target_ref);
        if (base)
                list = g_slist_append (list, base);

        base = mg_ref_base_get_ref_object (field->priv->field_ref);
        if (base)
                list = g_slist_append (list, base);

	if (field->priv->value_prov_ref) {
		base = mg_ref_base_get_ref_object (field->priv->value_prov_ref);
		if (base)
			list = g_slist_append (list, base);
	}

        return list;
}

static void
mg_qf_field_replace_refs (MgReferer *iface, GHashTable *replacements)
{
	MgQfField *field;

        g_return_if_fail (iface && IS_MG_QF_FIELD (iface));
        g_return_if_fail (MG_QF_FIELD (iface)->priv);

        field = MG_QF_FIELD (iface);
        if (field->priv->query) {
                MgQuery *query = g_hash_table_lookup (replacements, field->priv->query);
                if (query) {
                        g_signal_handlers_disconnect_by_func (G_OBJECT (field->priv->query),
                                                              G_CALLBACK (nullified_object_cb), field);
                        field->priv->query = query;
                        g_signal_connect (G_OBJECT (query), "nullified",
                                          G_CALLBACK (nullified_object_cb), field);
                }
        }

        mg_ref_base_replace_ref_object (field->priv->target_ref, replacements);
        mg_ref_base_replace_ref_object (field->priv->field_ref, replacements);
	if (field->priv->value_prov_ref)
		mg_ref_base_replace_ref_object (field->priv->value_prov_ref, replacements);
}
