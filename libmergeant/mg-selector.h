/* mg-selector.h
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __MG_SELECTOR__
#define __MG_SELECTOR__

#include <gtk/gtk.h>
#include "mg-conf.h"

G_BEGIN_DECLS

#define MG_SELECTOR_TYPE          (mg_selector_get_type())
#define MG_SELECTOR(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_selector_get_type(), MgSelector)
#define MG_SELECTOR_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_selector_get_type (), MgSelectorClass)
#define IS_MG_SELECTOR(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_selector_get_type ())


typedef struct _MgSelector      MgSelector;
typedef struct _MgSelectorClass MgSelectorClass;
typedef struct _MgSelectorPriv  MgSelectorPriv;

typedef enum {
	MG_SELECTOR_DATA_TYPES  = 1 << 0,
	MG_SELECTOR_FUNCTIONS   = 1 << 1,
	MG_SELECTOR_AGGREGATES  = 1 << 2,
	/* database structure */
	MG_SELECTOR_TABLES      = 1 << 3,
	MG_SELECTOR_FIELDS      = 1 << 5,
	/* queries */
	MG_SELECTOR_QUERIES     = 1 << 4,
	MG_SELECTOR_TARGETS     = 1 << 6,
	MG_SELECTOR_JOINS       = 1 << 7,
	MG_SELECTOR_QVIS_FIELDS = 1 << 8,
	MG_SELECTOR_QALL_FIELDS = 1 << 9,
	MG_SELECTOR_SUB_QUERIES = 1 << 10,
} MgSelectorMode;

typedef enum {
	MG_SELECTOR_COLUMN_OWNER        = 1 << 1,
	MG_SELECTOR_COLUMN_COMMENTS     = 1 << 2,
	MG_SELECTOR_COLUMN_TYPE         = 1 << 3,
	MG_SELECTOR_COLUMN_FIELD_LENGTH = 1 << 4,
	MG_SELECTOR_COLUMN_FIELD_NNUL   = 1 << 5,
	MG_SELECTOR_COLUMN_FIELD_DEFAULT= 1 << 6,
	MG_SELECTOR_COLUMN_QFIELD_VALUE = 1 << 7,
	MG_SELECTOR_COLUMN_QFIELD_TYPE  = 1 << 8
} MgSelectorColumn;

/* struct for the object's data */
struct _MgSelector
{
	GtkVBox             object;

	MgSelectorPriv *priv;
};

/* struct for the object's class */
struct _MgSelectorClass
{
	GtkVBoxClass        parent_class;

	void              (*selection_changed) (MgSelector *mgsel, GObject *sel_object);
};

/* 
 * Generic widget's methods 
*/
guint      mg_selector_get_type            (void);
GtkWidget *mg_selector_new                 (MgConf *conf, gulong mode, gulong columns);

/*
 * Widget's manipulations
 */
gboolean   mg_selector_set_selected_object        (MgSelector *mgsel, GObject *selection);
void       mg_selector_set_headers_visible        (MgSelector *mgsel, gboolean visible);
void       mg_selector_set_column_label           (MgSelector *mgsel, guint column, 
						   const gchar *label);

/*
 * Getting information
 */
GObject   *mg_selector_get_selected_object        (MgSelector *mgsel);
GObject   *mg_selector_get_selected_object_parent (MgSelector *mgsel);

G_END_DECLS

#endif



