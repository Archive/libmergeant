/* mg-target.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_TARGET_H_
#define __MG_TARGET_H_

#include "mg-base.h"
#include "mg-defs.h"
#include <libgda/libgda.h>
#include "mg-query.h"

G_BEGIN_DECLS

#define MG_TARGET_TYPE          (mg_target_get_type())
#define MG_TARGET(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_target_get_type(), MgTarget)
#define MG_TARGET_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_target_get_type (), MgTargetClass)
#define IS_MG_TARGET(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_target_get_type ())


/* Properties:
 * name        type          read/write      description
 * --------------------------------------------------------------------------------------------
 * query       pointer       RW              The MgQuery object to which the object is attached
 */

/* Interfaces:
 * MgXmlStorage
 * MgRerefer
 * MgAlias
 */

/* error reporting */
extern GQuark mg_target_error_quark (void);
#define MG_TARGET_ERROR mg_target_error_quark ()

/* different possible types for a query */
typedef enum {
        MG_TARGET_TYPE_INNER,
	MG_TARGET_TYPE_LEFT_OUTER,
	MG_TARGET_TYPE_RIGHT_OUTER,
	MG_TARGET_TYPE_FULL_OUTER,
        MG_TARGET_TYPE_CROSS,
        MG_TARGET_TYPE_LAST
} MgTargetType;

enum
{
	MG_TARGET_XML_LOAD_ERROR,
	MG_TARGET_META_DATA_UPDATE,
	MG_TARGET_FIELDS_ERROR
};


/* struct for the object's data */
struct _MgTarget
{
	MgBase               object;
	MgTargetPrivate       *priv;
};

/* struct for the object's class */
struct _MgTargetClass
{
	MgBaseClass                    class;
};

guint           mg_target_get_type               (void);
GObject        *mg_target_new_with_entity        (MgQuery *query, MgEntity *entity);
GObject        *mg_target_new_with_xml_id        (MgQuery *query, const gchar *entity_xml_id);
GObject        *mg_target_new_copy               (MgTarget *orig);

MgQuery        *mg_target_get_query              (MgTarget *target);
MgEntity       *mg_target_get_represented_entity (MgTarget *target);

void            mg_target_set_alias              (MgTarget *target, const gchar *alias);
const gchar    *mg_target_get_alias              (MgTarget *target);

G_END_DECLS

#endif
