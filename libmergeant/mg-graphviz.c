/* mg-graphviz.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "mg-graphviz.h"
#include "mg-query.h"
#include "mg-target.h"
#include "mg-join.h"
#include "mg-entity.h"
#include "mg-qfield.h"
#include "mg-field.h"
#include "mg-qf-all.h"
#include "mg-qf-field.h"
#include "mg-qf-value.h"
#include "mg-qf-func.h"
#include "mg-xml-storage.h"
#include "mg-condition.h"
#include "mg-renderer.h"
#include "mg-referer.h"
#include "mg-context.h"
#include "mg-parameter.h"

/* 
 * Main static functions 
 */
static void mg_graphviz_class_init (MgGraphvizClass * class);
static void mg_graphviz_init (MgGraphviz * srv);
static void mg_graphviz_dispose (GObject   * object);
static void mg_graphviz_finalize (GObject   * object);

static void mg_graphviz_set_property (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec);
static void mg_graphviz_get_property (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec);

static void weak_obj_notify (MgGraphviz *graph, GObject *obj);
/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;


/* properties */
enum
{
	PROP_0,
	PROP
};


/* private structure */
struct _MgGraphvizPrivate
{
	GSList *graphed_objects;
};


/* module error */
GQuark mg_graphviz_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_graphviz_error");
	return quark;
}


guint
mg_graphviz_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgGraphvizClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_graphviz_class_init,
			NULL,
			NULL,
			sizeof (MgGraphviz),
			0,
			(GInstanceInitFunc) mg_graphviz_init
		};

		type = g_type_register_static (MG_BASE_TYPE, "MgGraphviz", &info, 0);
	}
	return type;
}

static void
mg_graphviz_class_init (MgGraphvizClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_graphviz_dispose;
	object_class->finalize = mg_graphviz_finalize;

	/* Properties */
	object_class->set_property = mg_graphviz_set_property;
	object_class->get_property = mg_graphviz_get_property;
	g_object_class_install_property (object_class, PROP,
					 g_param_spec_pointer ("prop", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static void
mg_graphviz_init (MgGraphviz *mg_graphviz)
{
	mg_graphviz->priv = g_new0 (MgGraphvizPrivate, 1);
	mg_graphviz->priv->graphed_objects = NULL;
}

/**
 * mg_graphviz_new
 * @conf: a #MgConf object
 *
 * Creates a new #MgGraphviz object
 *
 * Returns: the new object
 */
GObject*
mg_graphviz_new (MgConf *conf)
{
	GObject *obj;
	MgGraphviz *mg_graphviz;

	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);

	obj = g_object_new (MG_GRAPHVIZ_TYPE, "conf", conf, NULL);
	mg_graphviz = MG_GRAPHVIZ (obj);
	mg_base_set_id (MG_BASE (mg_graphviz), 0);

	return obj;
}

static void
mg_graphviz_dispose (GObject *object)
{
	MgGraphviz *mg_graphviz;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_GRAPHVIZ (object));

	mg_graphviz = MG_GRAPHVIZ (object);
	if (mg_graphviz->priv) {
		while (mg_graphviz->priv->graphed_objects) {
			g_object_weak_unref (mg_graphviz->priv->graphed_objects->data, 
					     (GWeakNotify) weak_obj_notify, mg_graphviz);
			weak_obj_notify (mg_graphviz, mg_graphviz->priv->graphed_objects->data);
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_graphviz_finalize (GObject   * object)
{
	MgGraphviz *mg_graphviz;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_GRAPHVIZ (object));

	mg_graphviz = MG_GRAPHVIZ (object);
	if (mg_graphviz->priv) {
		g_free (mg_graphviz->priv);
		mg_graphviz->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_graphviz_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	gpointer ptr;
	MgGraphviz *mg_graphviz;

	mg_graphviz = MG_GRAPHVIZ (object);
	if (mg_graphviz->priv) {
		switch (param_id) {
		case PROP:
			ptr = g_value_get_pointer (value);
			break;
		}
	}
}

static void
mg_graphviz_get_property (GObject              *object,
			  guint                 param_id,
			  GValue               *value,
			  GParamSpec           *pspec)
{
	MgGraphviz *mg_graphviz;
	mg_graphviz = MG_GRAPHVIZ (object);
	
	if (mg_graphviz->priv) {
		switch (param_id) {
		case PROP:
			break;
		}	
	}
}

/**
 * mg_graphviz_add_to_graph
 * @graph: a #MgGraphviz object
 * @obj: a #GObject object to be graphed
 *
 * Adds @obj to be graphed by @graph
 */
void
mg_graphviz_add_to_graph (MgGraphviz *graph, GObject *obj)
{
	g_return_if_fail (graph && IS_MG_GRAPHVIZ (graph));
	g_return_if_fail (graph->priv);

	if (!g_slist_find (graph->priv->graphed_objects, obj)) {
		graph->priv->graphed_objects = g_slist_append (graph->priv->graphed_objects, obj);
		g_object_weak_ref (obj, (GWeakNotify) weak_obj_notify, graph);
	}
}

static void
weak_obj_notify (MgGraphviz *graph, GObject *obj)
{
	graph->priv->graphed_objects = g_slist_remove (graph->priv->graphed_objects, obj);
}

/*
 * Graphing functions
 */
static GSList *prepare_queries (MgGraphviz *graph);
static void    do_graph_query (MgGraphviz *graph, GString *string, MgQuery *query, gint taboffset);
static void    do_graph_context (MgGraphviz *graph, GString *string, MgContext *context, gint numcontext, gint taboffset);

/**
 * mg_graphviz_save_file
 * @graph: a #MgGraphviz object
 * @filename:
 * @error:
 *
 * Saves a dot representation of the @graph object to @filename
 *
 * Returns: TRUE if no error occured
 */
gboolean
mg_graphviz_save_file (MgGraphviz *graph, const gchar *filename, GError **error)
{
	gboolean allok = TRUE;
	GString *dot;
	FILE *file;
	size_t size;
	GSList *list, *tmplist;
	gint ncontext = 0;

	g_return_val_if_fail (graph && IS_MG_GRAPHVIZ (graph), FALSE);
	g_return_val_if_fail (graph->priv, FALSE);

	/* File handling */
	file = fopen (filename, "w");
	if (!file) {
		TO_IMPLEMENT;
		return FALSE;
	}

	/* actual graph rendering */
	dot = g_string_new ("digraph G {\n"
			    "\tnode [shape=box];\n"
			    "\tnodesep = 0.5;\n");
	list = prepare_queries (graph);
	tmplist = list;
	while (list) {
		if (IS_MG_QUERY (list->data)) 
			do_graph_query (graph, dot, MG_QUERY (list->data), 1);
		list = g_slist_next (list);
	}
	g_slist_free (tmplist);

	list = graph->priv->graphed_objects;
	while (list) {
		if (IS_MG_CONTEXT (list->data))
			do_graph_context (graph, dot, MG_CONTEXT (list->data), ncontext++, 1);
		list = g_slist_next (list);
	}

	g_string_append (dot, "}\n");

	/* writing to filename */
	size = fwrite (dot->str, sizeof (gchar), strlen (dot->str), file);
	if (size != strlen (dot->str)) {
		TO_IMPLEMENT;
		allok = FALSE;
	}

	/* Finish */
	fclose (file);
	g_string_free (dot, TRUE);

	return allok;
}


/*
 * Queries
 */

static void    get_depend_query (MgQuery *query, GSList **deplist, GSList **alldisplayed);

static void
prepare_single_query (MgQuery *query, GSList **top_queries, GSList **all_queries)
{
	GSList *list;
	GSList *deplist, *alllist;

	get_depend_query (query, &deplist, &alllist);
	list = deplist;
	while (list) {
		if (!g_slist_find (*all_queries, list->data))
			prepare_single_query (MG_QUERY (list->data), top_queries, all_queries);
		list = g_slist_next (list);
	}
	g_slist_free (deplist);

	if (alllist)
		*all_queries = g_slist_concat (*all_queries, alllist);

	if (!g_slist_find (*top_queries, query))
		*top_queries = g_slist_append (*top_queries, query);
	if (!g_slist_find (*all_queries, query))
		*all_queries = g_slist_append (*all_queries, query);
}

static void
get_depend_query (MgQuery *query, GSList **deplist, GSList **alldisplayed)
{
	GSList *list, *tmplist;

	*deplist = NULL;
	*alldisplayed = NULL;

	list = mg_query_get_sub_queries (query);
	*alldisplayed = g_slist_concat (*alldisplayed, list);

	list = g_slist_copy (mg_query_get_param_sources (query));
	*alldisplayed = g_slist_concat (*alldisplayed, list);

	list = mg_entity_get_all_fields (MG_ENTITY (query));
	tmplist = list;
	while (list) {
		MgBase *base;
		if (g_object_class_find_property (G_OBJECT_GET_CLASS (list->data), "value_provider")) {
			g_object_get (G_OBJECT (list->data), "value_provider", &base, NULL);
			if (base) {
				MgQuery *dquery = MG_QUERY (mg_field_get_entity (MG_FIELD (base)));
				if (!g_slist_find (*alldisplayed, dquery) && (dquery != query))
					*deplist = g_slist_append (*deplist, dquery);
			}
		}
		list = g_slist_next (list);
	}
	g_slist_free (tmplist);
}

static GSList *
prepare_queries (MgGraphviz *graph)
{
	GSList *top_queries = NULL, *all_queries = NULL, *list;

	list = graph->priv->graphed_objects;
	while (list) {
		if (IS_MG_QUERY (list->data) && !g_slist_find (all_queries, list->data)) 
			prepare_single_query (MG_QUERY (list->data), &top_queries, &all_queries);
		list = g_slist_next (list);
	}	

	g_slist_free (all_queries);

	return top_queries;
}

static gchar *render_qf_all_label (MgGraphviz *graph, MgQfAll *field);
static gchar *render_qf_field_label (MgGraphviz *graph, MgQfField *field);
static gchar *render_qf_value_label (MgGraphviz *graph, MgQfValue *field);
static gchar *render_qf_func_label (MgGraphviz *graph, MgQfFunc *field);
static void
do_graph_query (MgGraphviz *graph, GString *string, MgQuery *query, gint taboffset)
{
	gchar *str, *qid, *qtype, *sql;
	gint i;
	GSList *list;
	const GSList *clist;
	MgCondition *cond;

	/* offset string */
	str = g_new0 (gchar, taboffset+1);
	for (i=0; i<taboffset; i++)
		str [i] = '\t';
	
	qid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (query));
	g_string_append_printf (string, "%ssubgraph cluster%s {\n", str, qid);

	/* param sources */
	clist = mg_query_get_param_sources (query);
	if (clist) {
		g_string_append_printf (string, "%s\tsubgraph cluster%sps {\n", str, qid);
		g_string_append_printf (string, "%s\t\tnode [style=filled, color=black, fillcolor=white, fontsize=10];\n", str);
		while (clist) {
			do_graph_query (graph, string, MG_QUERY (clist->data), taboffset+2);
			clist = g_slist_next (clist);
		}
		g_string_append_printf (string, "\n%s\t\tlabel = \"Parameter sources\";\n", str);
		g_string_append_printf (string, "%s\t\tstyle = filled;\n", str);
		g_string_append_printf (string, "%s\t\tcolor = white;\n", str);

		g_string_append_printf (string, "%s\t}\n", str);
	}
	
	/* sub queries */
	list = mg_query_get_sub_queries (query);
	if (list) {
		GSList *tmplist = list;
		g_string_append_printf (string, "%s\tsubgraph cluster%ssub {\n", str, qid);
		g_string_append_printf (string, "%s\t\tnode [style=filled, color=black, fillcolor=white, fontsize=10];\n", str);
		while (list) {
			do_graph_query (graph, string, MG_QUERY (list->data), taboffset+2);
			list = g_slist_next (list);
		}
		g_string_append_printf (string, "\n%s\t\tlabel = \"Sub queries\";\n", str);
		g_string_append_printf (string, "%s\t\tstyle = filled;\n", str);
		g_string_append_printf (string, "%s\t\tcolor = white;\n", str);

		g_string_append_printf (string, "%s\t}\n", str);
		g_slist_free (tmplist);
	}
	

	/* targets */
	list = mg_query_get_targets (query);
	if (list) {
		GSList *tmplist = list;
		MgEntity *ent;

		while (list) {
			gchar *tid;

			tid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
			g_string_append_printf (string, "%s\t\"%s\" ", str, tid);
			ent = mg_target_get_represented_entity (MG_TARGET (list->data));
			g_string_append_printf (string, "[label=\"%s (%s)\" style=filled fillcolor=orange];\n",
						mg_base_get_name (MG_BASE (ent)), mg_target_get_alias (MG_TARGET (list->data)));
			g_free (tid);
			list = g_slist_next (list);
		}
		g_slist_free (tmplist);

		list = mg_query_get_joins (query);
		tmplist = list;
		while (list) {
			MgTarget *target;
			gchar *tid;
			gchar *jtype = "none";

			target = mg_join_get_target_1 (MG_JOIN (list->data));
			tid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (target));
			g_string_append_printf (string, "%s\t\"%s\" -> ", str, tid);
			g_free (tid);
			target = mg_join_get_target_2 (MG_JOIN (list->data));
			tid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (target));
			g_string_append_printf (string, "\"%s\" ", tid);
			g_free (tid);

			switch (mg_join_get_join_type (MG_JOIN (list->data))) {
			case MG_JOIN_TYPE_LEFT_OUTER:
				jtype = "back";
				break;
			case MG_JOIN_TYPE_RIGHT_OUTER:
				jtype = "forward";
				break;
			case MG_JOIN_TYPE_FULL_OUTER:
				jtype = "both";
				break;
			default:
				jtype = NULL;
			}
			if (jtype)
				g_string_append_printf (string, "[dir=%s, style=filled arrowhead=odot];\n", jtype);
			else
				g_string_append (string, "[dir=none, style=filled];\n");
			list = g_slist_next (list);
		}
		g_slist_free (tmplist);
	}

	/* fields */
	g_object_get (G_OBJECT (query), "really_all_fields", &list, NULL);
	if (list) {
		GSList *tmplist = list;

		while (list) {
			gchar *fid;
			gchar *label = NULL;

			if (IS_MG_QF_ALL (list->data))
				label = render_qf_all_label (graph, MG_QF_ALL (list->data));
			if (IS_MG_QF_FIELD (list->data))
				label = render_qf_field_label (graph, MG_QF_FIELD (list->data));
			if (IS_MG_QF_VALUE (list->data))
				label = render_qf_value_label (graph, MG_QF_VALUE (list->data));
			if (IS_MG_QF_FUNC (list->data))
				label = render_qf_func_label (graph, MG_QF_FUNC (list->data));
			if (!label)
				label = g_strdup ("{??? | {?|?|?|?}}");

			fid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
			g_string_append_printf (string, "%s\t\"%s\" ", str, fid);
			if (mg_referer_is_active (MG_REFERER (list->data)))
				g_string_append_printf (string, "[shape=record, label=\"%s\", style=filled, fillcolor=light_blue];\n",
							label);
			else
				g_string_append_printf (string, "[shape=record, label=\"%s\", style=filled, fillcolor=indianred];\n",
							label);
			g_free (label);
			g_free (fid);
			list = g_slist_next (list);
		}

		list = tmplist;
		while (list) {
			MgTarget *target = NULL;
			
			/* target link */
			if (IS_MG_QF_ALL (list->data))
				target = mg_qf_all_get_target (MG_QF_ALL (list->data));
			if (IS_MG_QF_FIELD (list->data))
				target = mg_qf_field_get_target (MG_QF_FIELD (list->data));

			if (target) {
				gchar *fid, *tid = NULL;
				fid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
				tid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (target));
				g_string_append_printf (string, "%s\t\"%s\" -> \"%s\";\n", str, fid, tid);
				g_free (fid);
				g_free (tid);
			}

			/* value provider link */
			if (g_object_class_find_property (G_OBJECT_GET_CLASS (list->data), "value_provider")) {
				MgBase *base;

				g_object_get (G_OBJECT (list->data), "value_provider", &base, NULL);
				if (base) {
					gchar *fid, *bid;

					fid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
					bid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (base));
					g_string_append_printf (string, "%s\t\"%s\" -> \"%s\" [style=dotted];\n", str, fid, bid);
					g_free (fid);
					g_free (bid);
				}
			}
			
			/* arguments for functions */
			if (IS_MG_QF_FUNC (list->data)) {
				GSList *args;

				args = mg_qf_func_get_args (MG_QF_FUNC (list->data));
				if (args) {
					GSList *tmplist = args;
					gchar *fid;

					fid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
					while (args) {
						if (args->data) {
							gchar *did;

							did = mg_xml_storage_get_xml_id (MG_XML_STORAGE (args->data));
							g_string_append_printf (string, "%s\t\"%s\" -> \"%s\" [style=filled];\n", str, fid, did);
							g_free (did);
						}
						args = g_slist_next (args);
					}
					g_free (fid);
					g_slist_free (tmplist);
				}
			}

			list = g_slist_next (list);
		}
	}

	switch (mg_query_get_query_type (query)) {
	case MG_QUERY_TYPE_SELECT:
		qtype = "SELECT";
		break;
	case MG_QUERY_TYPE_INSERT:
		qtype = "INSERT";
		break;
	case MG_QUERY_TYPE_UPDATE:
		qtype = "UPDATE";
		break;
	case MG_QUERY_TYPE_DELETE:
		qtype = "DELETE";
		break;
	case MG_QUERY_TYPE_UNION:
		qtype = "UNION";
		break;
	case MG_QUERY_TYPE_INTERSECT:
		qtype = "INTERSECT";
		break;
	case MG_QUERY_TYPE_EXCEPT:
		qtype = "EXCEPT";
		break;
	case MG_QUERY_TYPE_SQL:
		qtype = "SQL";
		break;
	default:
		qtype = "???";
		break;
	}

	/* condition */
	cond = mg_query_get_condition (query);
	if (cond) {
		GSList *tmplist, *list;
		gchar *strcond;
		GError *error = NULL;

		strcond = mg_renderer_render_as_sql (MG_RENDERER (cond), NULL, &error);
		if (error) {
			g_string_append_printf (string, "%s\t\"%s:Cond\" [label=\"%s\" style=filled fillcolor=yellow];\n", str, qid,
						error->message);
			g_error_free (error);
		}
		else {
			g_string_append_printf (string, "%s\t\"%s:Cond\" [label=\"%s\" style=filled fillcolor=yellow];\n", str, qid, strcond);
			g_free (strcond);
		}

		list = mg_condition_get_ref_objects_all (cond);
		tmplist = list;
		while (list) {	
			gchar *fid;

			fid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
			g_string_append_printf (string, "%s\t\"%s:Cond\" -> \"%s\";\n", str, qid, fid);
		
			list = g_slist_next (list);
		}
	}


	/* query attributes */
	sql = mg_renderer_render_as_sql (MG_RENDERER (query), NULL, NULL);
	g_string_append_printf (string, "\n%s\tlabel = \"%s Query %d: %s\";\n", str, qtype,
				mg_base_get_id (MG_BASE (query)), mg_base_get_name (MG_BASE (query)));
	g_free (sql);
	g_string_append_printf (string, "%s\tstyle = filled;\n", str);
	g_string_append_printf (string, "%s\tcolor = lightgrey;\n", str);
	g_string_append_printf (string, "%s}\n", str);
	g_free (qid);
	g_free (str);
}

static gchar *
render_qf_all_label (MgGraphviz *graph, MgQfAll *field)
{
	GString *retval;
	gchar *str;
	const gchar *cstr;

	retval = g_string_new ("{");
	cstr = mg_base_get_name (MG_BASE (field));
	if (cstr)
		g_string_append (retval, cstr);

	g_string_append (retval, " | {*");

	if (mg_qfield_is_visible (MG_QFIELD (field)))
		g_string_append (retval, " |V");
	else
		g_string_append (retval, " |-");
	if (mg_qfield_is_internal (MG_QFIELD (field)))
		g_string_append (retval, " |I");
	else
		g_string_append (retval, " |-");
	g_string_append (retval, " |-}}");

	str = retval->str;
	g_string_free (retval, FALSE);
	return str;
}

static gchar *
render_qf_field_label (MgGraphviz *graph, MgQfField *field)
{
	GString *retval;
	gchar *str;
	const gchar *cstr;
	MgField *ref;

	retval = g_string_new ("{");
	cstr = mg_base_get_name (MG_BASE (field));
	if (cstr)
		g_string_append (retval, cstr);
	ref = mg_qf_field_get_ref_field (field);
	if (ref)
		g_string_append_printf (retval, " (%s)", mg_base_get_name (MG_BASE (ref)));

	g_string_append (retval, " | {Field");

	if (mg_qfield_is_visible (MG_QFIELD (field)))
		g_string_append (retval, " |V");
	else
		g_string_append (retval, " |-");
	if (mg_qfield_is_internal (MG_QFIELD (field)))
		g_string_append (retval, " |I");
	else
		g_string_append (retval, " |-");
	g_string_append (retval, " |-}}");

	str = retval->str;
	g_string_free (retval, FALSE);
	return str;
}

static gchar *
render_qf_value_label (MgGraphviz *graph, MgQfValue *field)
{
	GString *retval;
	gchar *str;
	const gchar *cstr;
	const GdaValue *value;

	retval = g_string_new ("{");
	cstr = mg_base_get_name (MG_BASE (field));
	if (cstr)
		g_string_append (retval, cstr);
	value = mg_qf_value_get_value (field);
	if (value) {
		str = gda_value_stringify (value);
		g_string_append_printf (retval, " (%s)", str);
		g_free (str);
	}

	g_string_append (retval, " | {Value");

	if (mg_qfield_is_visible (MG_QFIELD (field)))
		g_string_append (retval, " |V");
	else
		g_string_append (retval, " |-");
	if (mg_qfield_is_internal (MG_QFIELD (field)))
		g_string_append (retval, " |I");
	else
		g_string_append (retval, " |-");
	if (mg_qf_value_is_parameter (field))
		g_string_append (retval, " |P}}");
	else
		g_string_append (retval, " |-}}");

	str = retval->str;
	g_string_free (retval, FALSE);
	return str;
}

static gchar *
render_qf_func_label (MgGraphviz *graph, MgQfFunc *field)
{
	GString *retval;
	gchar *str;
	const gchar *cstr;
	MgServerFunction *func;

	retval = g_string_new ("{");
	cstr = mg_base_get_name (MG_BASE (field));
	if (cstr)
		g_string_append (retval, cstr);
	func = mg_qf_func_get_ref_func (field);
	if (func)
		g_string_append_printf (retval, " (%s)", mg_base_get_name (MG_BASE (func)));

	g_string_append (retval, " | {Func()");

	if (mg_qfield_is_visible (MG_QFIELD (field)))
		g_string_append (retval, " |V");
	else
		g_string_append (retval, " |-");
	if (mg_qfield_is_internal (MG_QFIELD (field)))
		g_string_append (retval, " |I");
	else
		g_string_append (retval, " |-");
	g_string_append (retval, " |-}}");

	str = retval->str;
	g_string_free (retval, FALSE);
	return str;
}



/*
 * Context rendering
 */
static void
do_graph_context (MgGraphviz *graph, GString *string, MgContext *context, gint numcontext, gint taboffset)
{
	gchar *str;
	gint i, j;
	GSList *list;

	/* offset string */
	str = g_new0 (gchar, taboffset+1);
	for (i=0; i<taboffset; i++)
		str [i] = '\t';

	/* parameters */
	list = context->parameters;
	while (list) {
		GSList *dest;
		gchar *fid;

		g_string_append_printf (string, "%sParameter%p [label=\"%s (%d)\", shape=ellipse, style=filled, fillcolor=linen];\n", 
					str, list->data, mg_base_get_name (MG_BASE (list->data)), numcontext);
		dest = mg_parameter_get_dest_fields (MG_PARAMETER (list->data));
		while (dest) {
			fid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (dest->data));
			g_string_append_printf (string, "%sParameter%p -> \"%s\";\n", str, list->data, fid);
			g_free (fid);
			dest = g_slist_next (dest);
		}
		list = g_slist_next (list);
	}

	/* context nodes */
	j = 0;
	g_string_append_printf (string, "%ssubgraph clustercontext%d {\n", str, numcontext);	
	list = context->nodes;
	while (list) {
		MgContextNode *node = MG_CONTEXT_NODE (list->data);
		g_string_append_printf (string, "%s\tNode%p [label=\"Node%d\", shape=octagon];\n", str, list->data, j);
		
		if (node->param)
			g_string_append_printf (string, "%s\tNode%p -> Parameter%p [constraint=false];\n", 
						str, list->data, node->param);
		else {
			GSList *params = node->params;
			while (params) {
				g_string_append_printf (string, "%s\tNode%p -> Parameter%p;\n", str, list->data, params->data);
				params = g_slist_next (params);
			}
		}
		list = g_slist_next (list);
		j++;
	}
	g_string_append_printf (string, "%s\tlabel = \"Context %d\";\n", str, numcontext);
	g_string_append_printf (string, "%s}\n", str);
	

	g_free (str);
}
