/* mg-database.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-database.h"
#include "mg-db-table.h"
#include "mg-db-field.h"
#include "mg-db-constraint.h"
#include "mg-referer.h"
#include "mg-entity.h"
#include "mg-xml-storage.h"
#include "mg-result-set.h"
#include "marshal.h"
#include <config.h>
#include <string.h>
#include "mg-server.h"

/* 
 * Main static functions 
 */
static void mg_database_class_init (MgDatabaseClass * class);
static void mg_database_init (MgDatabase * srv);
static void mg_database_dispose (GObject   * object);
static void mg_database_finalize (GObject   * object);

static void mg_database_set_property (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec);
static void mg_database_get_property (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec);

static void        mg_database_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_database_get_xml_id (MgXmlStorage *iface);
static xmlNodePtr  mg_database_save_to_xml (MgXmlStorage *iface, GError **error);
static gboolean    mg_database_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error);

static void mg_database_add_table (MgDatabase *mgdb, MgDbTable *table, gint pos);
static void mg_database_add_sequence (MgDatabase *mgdb, MgDbSequence *seq, gint pos);
static void mg_database_add_constraint (MgDatabase *mgdb, MgDbConstraint *cstr);

static void nullified_table_cb (MgDbTable *table, MgDatabase *mgdb);
static void nullified_sequence_cb (MgDbSequence *seq, MgDatabase *mgdb);
static void nullified_constraint_cb (MgDbConstraint *cons, MgDatabase *mgdb);

static void updated_table_cb (MgDbTable *table, MgDatabase *mgdb);
static void updated_sequence_cb (MgDbSequence *seq, MgDatabase *mgdb);
static void updated_constraint_cb (MgDbConstraint *cons, MgDatabase *mgdb);

#ifdef debug
static void mg_database_dump (MgDatabase *mgdb, gint offset);
#endif

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	DATA_UPDATE_STARTED,
	DATA_UPDATE_FINISHED,
	UPDATE_PROGRESS,
	TABLE_ADDED,
	TABLE_REMOVED,
	TABLE_UPDATED,
	FIELD_ADDED,
	FIELD_REMOVED,
	FIELD_UPDATED,
	SEQUENCE_ADDED,
	SEQUENCE_REMOVED,
	SEQUENCE_UPDATED,
	CONSTRAINT_ADDED,
	CONSTRAINT_REMOVED,
	CONSTRAINT_UPDATED,
	FS_LINK_ADDED,
	FS_LINK_REMOVED,
	LAST_SIGNAL
};

static gint mg_database_signals[LAST_SIGNAL] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

/* properties */
enum
{
	PROP_0,
	PROP
};


/* private structure */
struct _MgDatabasePrivate
{
	/* Db structure */
	GSList                 *tables;
	GSList                 *sequences;
	GSList                 *constraints;

	/* DBMS update related information */
	gboolean                update_in_progress;
	gboolean                stop_update; /* TRUE if a DBMS data update must be stopped */
};


/* module error */
GQuark mg_database_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_database_error");
	return quark;
}

/**
 * mg_database_get_type
 *
 * Returns: the type id
 */
guint
mg_database_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgDatabaseClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_database_class_init,
			NULL,
			NULL,
			sizeof (MgDatabase),
			0,
			(GInstanceInitFunc) mg_database_init
		};
		

		static const GInterfaceInfo xml_storage_info = {
			(GInterfaceInitFunc) mg_database_xml_storage_init,
			NULL,
			NULL
		};

		type = g_type_register_static (MG_BASE_TYPE, "MgDatabase", &info, 0);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
	}
	return type;
}

static void 
mg_database_xml_storage_init (MgXmlStorageIface *iface)
{
	iface->get_xml_id = mg_database_get_xml_id;
	iface->save_to_xml = mg_database_save_to_xml;
	iface->load_from_xml = mg_database_load_from_xml;
}

static void
mg_database_class_init (MgDatabaseClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

        mg_database_signals[TABLE_ADDED] =
                g_signal_new ("table_added",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, table_added),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
	mg_database_signals[TABLE_REMOVED] =
                g_signal_new ("table_removed",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, table_removed),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
	mg_database_signals[TABLE_UPDATED] =
                g_signal_new ("table_updated",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, table_updated),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_database_signals[FIELD_ADDED] =
                g_signal_new ("field_added",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, field_added),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_database_signals[FIELD_REMOVED] =
                g_signal_new ("field_removed",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, field_removed),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_database_signals[FIELD_UPDATED] =
                g_signal_new ("field_updated",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, field_updated),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_database_signals[SEQUENCE_ADDED] =
                g_signal_new ("sequence_added",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, sequence_added),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_database_signals[SEQUENCE_REMOVED] =
                g_signal_new ("sequence_removed",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, sequence_removed),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_database_signals[SEQUENCE_UPDATED] =
                g_signal_new ("sequence_updated",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, sequence_updated),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_database_signals[CONSTRAINT_ADDED] =
                g_signal_new ("constraint_added",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, constraint_added),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_database_signals[CONSTRAINT_REMOVED] =
                g_signal_new ("constraint_removed",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, constraint_removed),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
        mg_database_signals[CONSTRAINT_UPDATED] =
                g_signal_new ("constraint_updated",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, constraint_updated),
                              NULL, NULL,
                              marshal_VOID__POINTER,
                              G_TYPE_NONE, 1, G_TYPE_POINTER);
	mg_database_signals[FS_LINK_ADDED] =
                g_signal_new ("fs_link_added",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, fs_link_added),
                              NULL, NULL,
                              marshal_VOID__POINTER_POINTER,
                              G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER);
	mg_database_signals[FS_LINK_REMOVED] =
                g_signal_new ("fs_link_removed",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, fs_link_removed),
                              NULL, NULL,
                              marshal_VOID__POINTER_POINTER,
                              G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER);
	mg_database_signals[DATA_UPDATE_STARTED] =
                g_signal_new ("data_update_started",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, data_update_started),
                              NULL, NULL,
                              marshal_VOID__VOID,
                              G_TYPE_NONE, 0);
        mg_database_signals[UPDATE_PROGRESS] =
                g_signal_new ("update_progress",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, update_progress),
                              NULL, NULL,
                              marshal_VOID__POINTER_UINT_UINT,
                              G_TYPE_NONE, 3, G_TYPE_POINTER, G_TYPE_UINT, G_TYPE_UINT);
	mg_database_signals[DATA_UPDATE_FINISHED] =
                g_signal_new ("data_update_finished",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (MgDatabaseClass, data_update_finished),
                              NULL, NULL,
                              marshal_VOID__VOID,
                              G_TYPE_NONE, 0);

	class->table_added = NULL;
	class->table_removed = NULL;
	class->table_updated = NULL;
	class->field_added = NULL;
	class->field_removed = NULL;
	class->field_updated = NULL;
	class->sequence_added = NULL;
	class->sequence_removed = NULL;
	class->sequence_updated = NULL;
	class->constraint_added = NULL;
	class->constraint_removed = NULL;
	class->constraint_updated = NULL;
	class->fs_link_added = NULL;
	class->fs_link_removed = NULL;
        class->data_update_started = NULL;
        class->data_update_finished = NULL;
        class->update_progress = NULL;

	object_class->dispose = mg_database_dispose;
	object_class->finalize = mg_database_finalize;

	/* Properties */
	object_class->set_property = mg_database_set_property;
	object_class->get_property = mg_database_get_property;
	g_object_class_install_property (object_class, PROP,
					 g_param_spec_pointer ("prop", NULL, NULL, (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_database_dump;
#endif

}


static void
mg_database_init (MgDatabase * mg_database)
{
	mg_database->priv = g_new0 (MgDatabasePrivate, 1);
	mg_database->priv->tables = NULL;
        mg_database->priv->sequences = NULL;
        mg_database->priv->constraints = NULL;

	mg_database->priv->update_in_progress = FALSE;
	mg_database->priv->stop_update = FALSE;
}


/**
 * mg_database_new
 * @conf: a #MgConf object
 * 
 * Creates a new MgDatabase object
 *
 * Returns: the new object
 */
GObject   *
mg_database_new (MgConf *conf)
{
	GObject   *obj;
	MgDatabase *mg_database;

	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);

	obj = g_object_new (MG_DATABASE_TYPE, NULL);
	mg_database = MG_DATABASE (obj);

	mg_base_set_conf (MG_BASE (mg_database), conf);

	return obj;
}


static void
mg_database_dispose (GObject   *object)
{
	MgDatabase *mg_database;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_DATABASE (object));

	mg_database = MG_DATABASE (object);
	if (mg_database->priv) {
		mg_base_nullify_check (MG_BASE (object));

		/* getting rid of the constraints */
		while (mg_database->priv->constraints) 
			mg_base_nullify (MG_BASE (mg_database->priv->constraints->data));

		/* getting rid of the sequences */
		while (mg_database->priv->sequences) 
			mg_base_nullify (MG_BASE (mg_database->priv->sequences->data));
		
		/* getting rid of the tables */
		while (mg_database->priv->tables) 
			mg_base_nullify (MG_BASE (mg_database->priv->tables->data));
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_database_finalize (GObject *object)
{
	MgDatabase *mg_database;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_DATABASE (object));

	mg_database = MG_DATABASE (object);
	if (mg_database->priv) {
		g_free (mg_database->priv);
		mg_database->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_database_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	gpointer ptr;
	MgDatabase *mg_database;

	mg_database = MG_DATABASE (object);
	if (mg_database->priv) {
		switch (param_id) {
		case PROP:
			/* FIXME */
			ptr = g_value_get_pointer (value);
			break;
		}
	}
}
static void
mg_database_get_property (GObject              *object,
			guint                 param_id,
			GValue               *value,
			GParamSpec           *pspec)
{
	MgDatabase *mg_database;
	mg_database = MG_DATABASE (object);
	
	if (mg_database->priv) {
		switch (param_id) {
		case PROP:
			/* FIXME */
			g_value_set_pointer (value, NULL);
			break;
		}	
	}
}


/* MgXmlStorage interface implementation */
static gchar *
mg_database_get_xml_id (MgXmlStorage *iface)
{
	g_return_val_if_fail (iface && IS_MG_DATABASE (iface), NULL);
	g_return_val_if_fail (MG_DATABASE (iface)->priv, NULL);

	return NULL;
}

static xmlNodePtr
mg_database_save_to_xml (MgXmlStorage *iface, GError **error)
{
	xmlNodePtr toptree, tree;
	MgDatabase *mgdb;
	GSList *list;

	g_return_val_if_fail (iface && IS_MG_DATABASE (iface), NULL);
	g_return_val_if_fail (MG_DATABASE (iface)->priv, NULL);

	mgdb = MG_DATABASE (iface);

	/* main node */
        toptree = xmlNewNode (NULL, "MG_DATABASE");
	
	/* Tables */
	tree = xmlNewChild (toptree, NULL, "MG_TABLES", NULL);
	list = mgdb->priv->tables;
	while (list) {
		xmlNodePtr table;
		
		table = mg_xml_storage_save_to_xml (MG_XML_STORAGE (list->data), error);
				
		if (table)
			xmlAddChild (tree, table);
		else {
			xmlFreeNode (tree);
			return NULL;
		}

		list = g_slist_next (list);
	}

	/* Sequences */
	if (mgdb->priv->sequences) {
		tree = xmlNewChild (toptree, NULL, "MG_SEQUENCES", NULL);
		list = mgdb->priv->sequences;
		while (list) {
			xmlNodePtr table;
			
			table = mg_xml_storage_save_to_xml (MG_XML_STORAGE (list->data), error);
			
			if (table)
				xmlAddChild (tree, table);
			else {
				xmlFreeNode (tree);
				return NULL;
			}
			
			list = g_slist_next (list);
		}
	}

	/* Constraints */
	tree = xmlNewChild (toptree, NULL, "MG_CONSTRAINTS", NULL);
	list = mgdb->priv->constraints;
	while (list) {
		xmlNodePtr cstr;
		
		cstr = mg_xml_storage_save_to_xml (MG_XML_STORAGE (list->data), NULL);
				
		if (cstr)
			xmlAddChild (tree, cstr); /* otherwise, just ignore the error */
#ifdef debug
		else
			g_print (D_COL_ERR "ERROR\n" D_COL_NOR);
#endif
		list = g_slist_next (list);
	}

	return toptree;
}

static gboolean mg_database_load_from_xml_tables (MgXmlStorage *iface, xmlNodePtr node, GError **error);
static gboolean mg_database_load_from_xml_constraints (MgXmlStorage *iface, xmlNodePtr node, GError **error);

static gboolean
mg_database_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgDatabase *mgdb;
	xmlNodePtr subnode;

	g_return_val_if_fail (iface && IS_MG_DATABASE (iface), FALSE);
	g_return_val_if_fail (MG_DATABASE (iface)->priv, FALSE);
	g_return_val_if_fail (node, FALSE);

	mgdb = MG_DATABASE (iface);

	if (mgdb->priv->tables || mgdb->priv->sequences || mgdb->priv->constraints) {
		g_set_error (error,
			     MG_DATABASE_ERROR,
			     MG_DATABASE_XML_LOAD_ERROR,
			     _("Database already contains data"));
		return FALSE;
	}
	if (strcmp (node->name, "MG_DATABASE")) {
		g_set_error (error,
			     MG_DATABASE_ERROR,
			     MG_DATABASE_XML_LOAD_ERROR,
			     _("XML Tag is not <MG_DATABASE>"));
		return FALSE;
	}
	subnode = node->children;
	while (subnode) {
		gboolean done = FALSE;

		if (!strcmp (subnode->name, "MG_TABLES")) {
			if (!mg_database_load_from_xml_tables (iface, subnode, error))
				return FALSE;

			done = TRUE;
		}

		if (!done && !strcmp (subnode->name, "MG_SEQUENCES")) {
			TO_IMPLEMENT;
			done = TRUE;
		}

		if (!done && !strcmp (subnode->name, "MG_CONSTRAINTS")) {
			if (!mg_database_load_from_xml_constraints (iface, subnode, error))
				return FALSE;

			done = TRUE;
		}

		subnode = subnode->next;
	}
	
	return TRUE;
}

static gboolean
mg_database_load_from_xml_tables (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	gboolean allok = TRUE;
	xmlNodePtr subnode = node->children;

	while (subnode && allok) {
		if (! xmlNodeIsText (subnode)) {
			if (!strcmp (subnode->name, "MG_TABLE")) {
				MgDbTable *table;
				
				table = MG_DB_TABLE (mg_db_table_new (mg_base_get_conf (MG_BASE (iface))));
				allok = mg_xml_storage_load_from_xml (MG_XML_STORAGE (table), subnode, error);
				if (allok)
					mg_database_add_table (MG_DATABASE (iface), table, -1);
				g_object_unref (G_OBJECT (table));
			}
			else {
				allok = FALSE;
				g_set_error (error,
					     MG_DATABASE_ERROR,
					     MG_DATABASE_XML_LOAD_ERROR,
					     _("XML Tag below <MG_TABLES> is not <MG_TABLE>"));
			}
		}			

		subnode = subnode->next;
	}	

	return allok;
}

static gboolean
mg_database_load_from_xml_constraints (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	gboolean allok = TRUE;
	xmlNodePtr subnode = node->children;

	while (subnode && allok) {
		if (! xmlNodeIsText (subnode)) {
			if (!strcmp (subnode->name, "MG_CONSTRAINT")) {
				MgDbConstraint *cstr;
				
				cstr = MG_DB_CONSTRAINT (mg_db_constraint_new_with_db (MG_DATABASE (iface)));
				allok = mg_xml_storage_load_from_xml (MG_XML_STORAGE (cstr), subnode, error);
				mg_database_add_constraint (MG_DATABASE (iface), cstr);
				g_object_unref (G_OBJECT (cstr));
			}
			else {
				allok = FALSE;
				g_set_error (error,
					     MG_DATABASE_ERROR,
					     MG_DATABASE_XML_LOAD_ERROR,
					     _("XML Tag below <MG_CONSTRAINTS> is not <MG_CONSTRAINT>"));
			}
		}			

		subnode = subnode->next;
	}	

	return allok;
}

/*
 * pos = -1 to append the table to the list
 */
static void
mg_database_add_table (MgDatabase *mgdb, MgDbTable *table, gint pos)
{
	g_return_if_fail (table);
	g_return_if_fail (!g_slist_find (mgdb->priv->tables, table));

	g_object_set (G_OBJECT (table), "database", mgdb, NULL);
	mgdb->priv->tables = g_slist_insert (mgdb->priv->tables, table, pos);

	g_object_ref (G_OBJECT (table));
	g_signal_connect (G_OBJECT (table), "nullified",
			  G_CALLBACK (nullified_table_cb), mgdb);
	g_signal_connect (G_OBJECT (table), "changed",
			  G_CALLBACK (updated_table_cb), mgdb);


#ifdef debug_signal
	g_print (">> 'TABLE_ADDED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (mgdb), mg_database_signals[TABLE_ADDED], 0, table);
#ifdef debug_signal
	g_print ("<< 'TABLE_ADDED' from %s\n", __FUNCTION__);
#endif
}

/*
 * pos = -1 to append the table to the list
 */
static void
mg_database_add_sequence (MgDatabase *mgdb, MgDbSequence *seq, gint pos)
{
	g_return_if_fail (seq);
	g_return_if_fail (!g_slist_find (mgdb->priv->sequences, seq));

	g_object_set (G_OBJECT (seq), "database", mgdb, NULL);
	mgdb->priv->sequences = g_slist_insert (mgdb->priv->sequences, seq, pos);

	g_object_ref (G_OBJECT (seq));
	g_signal_connect (G_OBJECT (seq), "nullified",
			  G_CALLBACK (nullified_sequence_cb), mgdb);
	g_signal_connect (G_OBJECT (seq), "changed",
			  G_CALLBACK (updated_sequence_cb), mgdb);


#ifdef debug_signal
	g_print (">> 'SEQUENCE_ADDED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (mgdb), mg_database_signals[SEQUENCE_ADDED], 0, seq);
#ifdef debug_signal
	g_print ("<< 'SEQUENCE_ADDED' from %s\n", __FUNCTION__);
#endif
}

static void
mg_database_add_constraint (MgDatabase *mgdb, MgDbConstraint *cstr)
{
	GSList *list;
	MgDbConstraint *ptr = NULL;

	g_return_if_fail (cstr);

	/* Try to activate the constraints here */
	mg_referer_activate (MG_REFERER (cstr));

	/* try to find if a similar constraint is there */
	list = mgdb->priv->constraints;
	while (list && !ptr) {
		if (mg_db_constraint_equal (cstr, MG_DB_CONSTRAINT (list->data)))
			ptr = MG_DB_CONSTRAINT (list->data);
		list = g_slist_next (list);
	}

	if (ptr) {
		/* update the existing constraint with name and description */
		mg_base_set_name (MG_BASE (ptr), mg_base_get_name (MG_BASE (cstr)));
		mg_base_set_description (MG_BASE (ptr), mg_base_get_description (MG_BASE (cstr)));
		mg_base_set_owner (MG_BASE (ptr), mg_base_get_owner (MG_BASE (cstr)));
	}
	else {
		/* add @cstr to the list of constraints */
		mgdb->priv->constraints = g_slist_append (mgdb->priv->constraints, cstr);
		g_object_ref (G_OBJECT (cstr));
		g_signal_connect (G_OBJECT (cstr), "nullified",
				  G_CALLBACK (nullified_constraint_cb), mgdb);
		g_signal_connect (G_OBJECT (cstr), "changed",
				  G_CALLBACK (updated_constraint_cb), mgdb);

#ifdef debug_signal
		g_print (">> 'CONSTRAINT_ADDED' from %s\n", __FUNCTION__);
#endif
		g_signal_emit (G_OBJECT (mgdb), mg_database_signals[CONSTRAINT_ADDED], 0, cstr);
#ifdef debug_signal
		g_print ("<< 'CONSTRAINT_ADDED' from %s\n", __FUNCTION__);
#endif
	}
}

#ifdef debug
static void 
mg_database_dump (MgDatabase *mgdb, gint offset)
{
	gchar *str;
	guint i;
	GSList *list;

	g_return_if_fail (mgdb && IS_MG_DATABASE (mgdb));
	
	/* string for the offset */
	str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;
	
	/* dump */
	if (mgdb->priv) 
		g_print ("%s" D_COL_H1 "MgDatabase %p\n" D_COL_NOR, str, mgdb);
	else
		g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, mgdb);

	/* tables */
	list = mgdb->priv->tables;
	if (list) {
		g_print ("%sTables:\n", str);
		while (list) {
			mg_base_dump (MG_BASE (list->data), offset+5);
			list = g_slist_next (list);
		}
	}
	else
		g_print ("%sNo Table defined\n", str);

	/* sequences */
	list = mgdb->priv->sequences;
	if (list) {
		g_print ("%sSequences:\n", str);
		while (list) {
			mg_base_dump (MG_BASE (list->data), offset+5);
			list = g_slist_next (list);
		}
	}
	else
		g_print ("%sNo Sequence defined\n", str);


	/* constraints */
	list = mgdb->priv->constraints;
	if (list) {
		g_print ("%sConstraints:\n", str);
		while (list) {
			mg_base_dump (MG_BASE (list->data), offset+5);
			list = g_slist_next (list);
		}
	}
	else
		g_print ("%sNo Constraint defined\n", str);


	g_free (str);

}
#endif


/**
 * mg_database_get_conf
 * @mgdb: a #MgDatabase object
 *
 * Fetch the MgConf object to which the MgDatabase belongs.
 *
 * Returns: the MgConf object
 */
MgConf *
mg_database_get_conf (MgDatabase *mgdb)
{
	g_return_val_if_fail (mgdb && IS_MG_DATABASE (mgdb), NULL);
	g_return_val_if_fail (mgdb->priv, NULL);

	return mg_base_get_conf (MG_BASE (mgdb));
}



static gboolean database_tables_update_list (MgDatabase * mgdb, GError **error);
static gboolean database_sequences_update_list (MgDatabase * mgdb, GError **error);
static gboolean database_constraints_update_list (MgDatabase * mgdb, GError **error);
/**
 * mg_database_update_dbms_data
 * @mgdb: a #MgDatabase object
 * @error: location to store error, or %NULL
 *
 * Synchronises the Database representation with the database structure which is stored in
 * the DBMS. For this operation to succeed, the connection to the DBMS server MUST be opened
 * (using the corresponding #MgServer object).
 *
 * Returns: TRUE if no error
 */
gboolean
mg_database_update_dbms_data (MgDatabase *mgdb, GError **error)
{
	gboolean retval = TRUE;
	MgServer *srv;
	MgConf *conf;

	g_return_val_if_fail (mgdb && IS_MG_DATABASE (mgdb), FALSE);
	g_return_val_if_fail (mgdb->priv, FALSE);
	
	if (mgdb->priv->update_in_progress) {
		g_set_error (error, MG_DATABASE_ERROR, MG_DATABASE_META_DATA_UPDATE,
			     _("Update already started!"));
		return FALSE;
	}

	conf = mg_base_get_conf (MG_BASE (mgdb));
	srv = mg_conf_get_server (conf);
	if (!mg_server_conn_is_opened (srv)) {
		g_set_error (error, MG_DATABASE_ERROR, MG_DATABASE_META_DATA_UPDATE,
			     _("Connection is not opened!"));
		return FALSE;
	}

	mgdb->priv->update_in_progress = TRUE;
	mgdb->priv->stop_update = FALSE;

#ifdef debug_signal
	g_print (">> 'DATA_UPDATE_STARTED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (mgdb), mg_database_signals[DATA_UPDATE_STARTED], 0);
#ifdef debug_signal
	g_print ("<< 'DATA_UPDATE_STARTED' from %s\n", __FUNCTION__);
#endif

	retval = database_tables_update_list (mgdb, error);
	if (retval && !mgdb->priv->stop_update) 
		retval = database_sequences_update_list (mgdb, error);
	if (retval && !mgdb->priv->stop_update) 
		retval = database_constraints_update_list (mgdb, error);

#ifdef debug_signal
	g_print (">> 'DATA_UPDATE_FINISHED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (mgdb), mg_database_signals[DATA_UPDATE_FINISHED], 0);
#ifdef debug_signal
	g_print ("<< 'DATA_UPDATE_FINISHED' from %s\n", __FUNCTION__);
#endif

	mgdb->priv->update_in_progress = FALSE;
	if (mgdb->priv->stop_update) {
		g_set_error (error, MG_DATABASE_ERROR, MG_DATABASE_META_DATA_UPDATE_USER_STOPPED,
			     _("Update stopped!"));
		return FALSE;
	}

	return retval;
}


/**
 * mg_database_stop_update_dbms_data
 * @mgdb: a #MgDatabase object
 *
 * When the database updates its internal lists of DBMS objects, a call to this function will 
 * stop that update process. It has no effect when the database is not updating its DBMS data.
 */
void
mg_database_stop_update_dbms_data (MgDatabase *mgdb)
{
	g_return_if_fail (mgdb && IS_MG_DATABASE (mgdb));
	g_return_if_fail (mgdb->priv);

	mgdb->priv->stop_update = TRUE;
}

static gboolean
database_tables_update_list (MgDatabase *mgdb, GError **error)
{
	GSList *list;
	GdaDataModel *rs;
	gchar *str;
	guint now, total;
	GSList *updated_tables = NULL;
	MgServer *srv;
	MgDbTable *table;
	GSList *constraints;

	srv = mg_conf_get_server (mg_base_get_conf (MG_BASE (mgdb)));
	rs = mg_server_get_gda_schema (srv, GDA_CONNECTION_SCHEMA_TABLES, NULL);
	if (!rs) {
		g_set_error (error, MG_DATABASE_ERROR, MG_DATABASE_TABLES_ERROR,
			     _("Can't get list of tables"));
		return FALSE;
	}

	if (!mg_resultset_check_data_model (rs, 4, 
					    GDA_VALUE_TYPE_STRING, 
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING)) {
		g_set_error (error, MG_DATABASE_ERROR, MG_DATABASE_TABLES_ERROR,
			     _("Schema for list of tables is wrong"));
		g_object_unref (G_OBJECT (rs));
		return FALSE;
	}

	total = gda_data_model_get_n_rows (rs);
	now = 0;		
	while ((now < total) && !mgdb->priv->stop_update) {
		const GdaValue *value;
		gboolean newtable = FALSE;
		gint i = -1;

		value = gda_data_model_get_value_at (rs, 0, now);
		str = gda_value_stringify (value);
		table = mg_database_get_table_by_name (mgdb, str);
		if (!table) {
			gboolean found = FALSE;

			i = 0;
			/* table name */
			table = MG_DB_TABLE (mg_db_table_new (mg_base_get_conf (MG_BASE (mgdb))));
			mg_base_set_name (MG_BASE (table), str);
			newtable = TRUE;
			
			/* finding the right position for the table */
			list = mgdb->priv->tables;
			while (list && !found) {
				if (strcmp (str, mg_base_get_name (MG_BASE (list->data))) < 0)
					found = TRUE;
				else
					i++;
				list = g_slist_next (list);
			}
		}
		g_free (str);
		
		updated_tables = g_slist_append (updated_tables, table);

		/* description */
		value = gda_data_model_get_value_at (rs, 2, now);
		if (value && !gda_value_is_null (value) && (* gda_value_get_string(value))) {
			str = gda_value_stringify (value);
			mg_base_set_description (MG_BASE (table), str);
			g_free (str);
		}
		else 
			mg_base_set_description (MG_BASE (table), NULL);

		/* owner */
		value = gda_data_model_get_value_at (rs, 1, now);
		if (value && !gda_value_is_null (value) && (* gda_value_get_string(value))) {
			str = gda_value_stringify (value);
			mg_base_set_owner (MG_BASE (table), str);
			g_free (str);
		}
		else
			mg_base_set_owner (MG_BASE (table), NULL);
				
		/* fields */
		g_object_set (G_OBJECT (table), "database", mgdb, NULL);
		if (!mg_db_table_update_dbms_data (table, error)) {
			g_object_unref (G_OBJECT (rs));
			return FALSE;
		}

		/* signal if the DT is new */
		if (newtable) {
			mg_database_add_table (mgdb, table, i);
			g_object_unref (G_OBJECT (table));
		}

		/* Taking care of the constraints coming attached to the MgDbTable */
		constraints = g_object_get_data (G_OBJECT (table), "pending_constraints");
		if (constraints) {
			GSList *list = constraints;

			while (list) {
				mg_database_add_constraint (mgdb, MG_DB_CONSTRAINT (list->data));
				g_object_unref (G_OBJECT (list->data));
				list = g_slist_next (list);
			}
			g_slist_free (constraints);
			g_object_set_data (G_OBJECT (table), "pending_constraints", NULL);
		}

		g_signal_emit_by_name (G_OBJECT (mgdb), "update_progress", "TABLES",
				       now, total);
		now++;
	}
	
	g_object_unref (G_OBJECT (rs));
	
	/* remove the tables not existing anymore */
	list = mgdb->priv->tables;
	while (list) {
		if (!g_slist_find (updated_tables, list->data)) {
			mg_base_nullify (MG_BASE (list->data));
			list = mgdb->priv->tables;
		}
		else
			list = g_slist_next (list);
	}
	g_slist_free (updated_tables);
	
	g_signal_emit_by_name (G_OBJECT (mgdb), "update_progress", NULL, 0, 0);

	/* activate all the constraints here */
	list = mgdb->priv->constraints;
	while (list) {
		if (!mg_referer_activate (MG_REFERER (list->data))) {
			mg_base_nullify (MG_BASE (list->data));
			list = mgdb->priv->constraints;
		}
		else
			list = g_slist_next (list);
	}
	
	return TRUE;
}


static void
nullified_table_cb (MgDbTable *table, MgDatabase *mgdb)
{
	g_return_if_fail (g_slist_find (mgdb->priv->tables, table));

	mgdb->priv->tables = g_slist_remove (mgdb->priv->tables, table);

	g_signal_handlers_disconnect_by_func (G_OBJECT (table), 
					      G_CALLBACK (nullified_table_cb), mgdb);
	g_signal_handlers_disconnect_by_func (G_OBJECT (table), 
					      G_CALLBACK (updated_table_cb), mgdb);

#ifdef debug_signal
	g_print (">> 'TABLE_REMOVED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (mgdb), "table_removed", table);
#ifdef debug_signal
	g_print ("<< 'TABLE_REMOVED' from %s\n", __FUNCTION__);
#endif

	g_object_unref (G_OBJECT (table));
}

static void
updated_table_cb (MgDbTable *table, MgDatabase *mgdb)
{
#ifdef debug_signal
	g_print (">> 'TABLE_UPDATED' from %s\n", __FUCNTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (mgdb), "table_updated", table);
#ifdef debug_signal
	g_print ("<< 'TABLE_UPDATED' from %s\n", __FUCNTION__);
#endif	
}

static gboolean
database_sequences_update_list (MgDatabase *mgdb, GError **error)
{
	TO_IMPLEMENT;
	
	return TRUE;
}

static void
nullified_sequence_cb (MgDbSequence *seq, MgDatabase *mgdb)
{
	g_return_if_fail (g_slist_find (mgdb->priv->sequences, seq));
	mgdb->priv->sequences = g_slist_remove (mgdb->priv->sequences, seq);

	g_signal_handlers_disconnect_by_func (G_OBJECT (seq), 
					      G_CALLBACK (nullified_sequence_cb), mgdb);
	g_signal_handlers_disconnect_by_func (G_OBJECT (seq), 
					      G_CALLBACK (updated_sequence_cb), mgdb);

#ifdef debug_signal
	g_print (">> 'SEQUENCE_REMOVED' from %s\n", __SEQUENCE__);
#endif
	g_signal_emit_by_name (G_OBJECT (mgdb), "sequence_removed", seq);
#ifdef debug_signal
	g_print ("<< 'SEQUENCE_REMOVED' from %s\n", __SEQUENCE__);
#endif

	g_object_unref (G_OBJECT (seq));
}


static void
updated_sequence_cb (MgDbSequence *seq, MgDatabase *mgdb)
{
#ifdef debug_signal
	g_print (">> 'SEQUENCE_UPDATED' from %s\n", __FUCNTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (mgdb), "sequence_updated", seq);
#ifdef debug_signal
	g_print ("<< 'SEQUENCE_UPDATED' from %s\n", __FUCNTION__);
#endif	
}


static gboolean
database_constraints_update_list (MgDatabase *mgdb, GError **error)
{
	TO_IMPLEMENT;
	/* To be implemented when Libgda has a dedicated schema to fetch constraints */

	return TRUE;
}


static void
nullified_constraint_cb (MgDbConstraint *cons, MgDatabase *mgdb)
{
	g_return_if_fail (g_slist_find (mgdb->priv->constraints, cons));
	mgdb->priv->constraints = g_slist_remove (mgdb->priv->constraints, cons);

	g_signal_handlers_disconnect_by_func (G_OBJECT (cons), 
					      G_CALLBACK (nullified_constraint_cb), mgdb);
	g_signal_handlers_disconnect_by_func (G_OBJECT (cons), 
					      G_CALLBACK (updated_constraint_cb), mgdb);

#ifdef debug_signal
	g_print (">> 'CONSTRAINT_REMOVED' from %s\n", __FUCNTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (mgdb), "constraint_removed", cons);
#ifdef debug_signal
	g_print ("<< 'CONSTRAINT_REMOVED' from %s\n", __FUCNTION__);
#endif
	g_object_unref (G_OBJECT (cons));
}

static void
updated_constraint_cb (MgDbConstraint *cons, MgDatabase *mgdb)
{
#ifdef debug_signal
	g_print (">> 'CONSTRAINT_UPDATED' from %s\n", __FUCNTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (mgdb), "constraint_updated", cons);
#ifdef debug_signal
	g_print ("<< 'CONSTRAINT_UPDATED' from %s\n", __FUCNTION__);
#endif	
}


/**
 * mg_database_get_tables
 * @mgdb: a #MgDatabase object
 *
 * Get a list of all the tables within @mgdb
 *
 * Returns: a new list of all the #MgDbTable objects
 */
GSList *
mg_database_get_tables(MgDatabase *mgdb)
{
	g_return_val_if_fail (mgdb && IS_MG_DATABASE (mgdb), NULL);
	g_return_val_if_fail (mgdb->priv, NULL);

	return g_slist_copy (mgdb->priv->tables);
}

/**
 * mg_database_get_table_by_name
 * @mgdb: a #MgDatabase object
 * @name: the name of the requested table
 *
 * Get a reference to a MgDbTable using its name.
 *
 * Returns: The MgDbTable pointer or NULL if the requested table does not exist.
 */
MgDbTable *
mg_database_get_table_by_name (MgDatabase *mgdb, const gchar *name)
{
	MgDbTable *table = NULL;
	GSList *tables;

	g_return_val_if_fail (mgdb && IS_MG_DATABASE (mgdb), NULL);
	g_return_val_if_fail (mgdb->priv, NULL);
	g_return_val_if_fail (name && *name, NULL);

	tables = mgdb->priv->tables;
	while (!table && tables) {
		if (!strcmp (mg_base_get_name (MG_BASE (tables->data)), name))
		    table = MG_DB_TABLE (tables->data);

		tables = g_slist_next (tables);
	}
	
	return table;
}

/**
 * mg_database_get_table_by_xml_id
 * @mgdb: a #MgDatabase object
 * @xml_id: the XML id of the requested table
 *
 * Get a reference to a MgDbTable using its XML id.
 *
 * Returns: The MgDbTable pointer or NULL if the requested table does not exist.
 */
MgDbTable *
mg_database_get_table_by_xml_id (MgDatabase *mgdb, const gchar *xml_id)
{
	MgDbTable *table = NULL;
	GSList *list;
	gchar *str;

	g_return_val_if_fail (mgdb && IS_MG_DATABASE (mgdb), NULL);
	g_return_val_if_fail (xml_id && *xml_id, NULL);

	list = mgdb->priv->tables;
	while (list && !table) {
		str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
		if (!strcmp (str, xml_id))
			table = MG_DB_TABLE (list->data);
		g_free (str);
		list = g_slist_next (list);
	}

	return table;
}

/**
 * mg_database_get_field_by_name
 * @mgdb: a #MgDatabase object
 * @fullname: the name of the requested table field
 *
 * Get a reference to a MgDbField specifying the full name (table_name.field_name)
 * of the requested field.
 *
 * Returns: The MgDbField pointer or NULL if the requested field does not exist.
 */
MgDbField *
mg_database_get_field_by_name (MgDatabase *mgdb, const gchar *fullname)
{
	gchar *str, *tok, *ptr;
	MgDbTable *ref_table;
	MgDbField *ref_field = NULL;

	g_return_val_if_fail (mgdb && IS_MG_DATABASE (mgdb), NULL);
	g_return_val_if_fail (fullname && *fullname, NULL);

	str = g_strdup (fullname);
	ptr = strtok_r (str, ".", &tok);
	ref_table = mg_database_get_table_by_name (mgdb, ptr);
	if (ref_table) {
		gpointer data;
		ptr = strtok_r (NULL, ".", &tok);
		data = mg_entity_get_field_by_name (MG_ENTITY (ref_table), ptr);
		if (data)
			ref_field = MG_DB_FIELD (data);
	}
	g_free (str);

	return ref_field;
}


/**
 * mg_database_get_field_by_xml_id
 * @mgdb: a #MgDatabase object
 * @xml_id: the XML id of the requested table field
 *
 * Get a reference to a MgDbField specifying its XML id
 *
 * Returns: The MgDbField pointer or NULL if the requested field does not exist.
 */
MgDbField *
mg_database_get_field_by_xml_id (MgDatabase *mgdb, const gchar *xml_id)
{
	MgDbField *field = NULL;
	GSList *tables;

	g_return_val_if_fail (mgdb && IS_MG_DATABASE (mgdb), NULL);
	g_return_val_if_fail (xml_id && *xml_id, NULL);

	tables = mgdb->priv->tables;
	while (tables && !field) {
		gpointer data;
		data = mg_entity_get_field_by_xml_id (MG_ENTITY (tables->data), xml_id);
		if (data)
			field = MG_DB_FIELD (data);
		tables = g_slist_next (tables);
	}

	return field;
}

/**
 * mg_database_get_sequence_by_name
 * @mgdb: a #MgDatabase object
 * @name: the name of the requested sequence
 *
 * Get a reference to a MgDbSequence specifying its name
 *
 * Returns: The MgDbSequence pointer or NULL if the requested sequence does not exist.
 */
MgDbSequence *
mg_database_get_sequence_by_name (MgDatabase *mgdb, const gchar *name)
{
	MgDbSequence *seq = NULL;

	g_return_val_if_fail (mgdb && IS_MG_DATABASE (mgdb), NULL);
	g_return_val_if_fail (name && *name, NULL);

	TO_IMPLEMENT;
	return seq;
}

/**
 * mg_database_get_sequence_by_xml_id
 * @mgdb: a #MgDatabase object
 * @xml_id: the XML id of the requested sequence
 *
 * Get a reference to a MgDbSequence specifying its XML id.
 *
 * Returns: The MgDbSequence pointer or NULL if the requested sequence does not exist.
 */
MgDbSequence *
mg_database_get_sequence_by_xml_id (MgDatabase *mgdb, const gchar *xml_id)
{
	MgDbSequence *seq = NULL;

	g_return_val_if_fail (mgdb && IS_MG_DATABASE (mgdb), NULL);
	g_return_val_if_fail (xml_id && *xml_id, NULL);

	TO_IMPLEMENT;
	return seq;
}

/** 
 * mg_database_get_sequence_to_field
 * @mgdb: a #MgDatabase object
 * @field: a #MgDbField object
 *
 * Get a reference to a MgDbSequence which is "linked" to the MgDbField given as parameter.
 * This "link" means that each new value of the field will be given by the returned sequence
 *
 * Returns: The MgDbSequence pointer or NULL if there is no sequence for this job.
 */
MgDbSequence *
mg_database_get_sequence_to_field  (MgDatabase *mgdb, MgDbField *field)
{
	MgDbSequence *seq = NULL;

	g_return_val_if_fail (mgdb && IS_MG_DATABASE (mgdb), NULL);
	g_return_val_if_fail (field && IS_MG_DB_FIELD (field), NULL);

	TO_IMPLEMENT;
	return seq;
}


/**
 * mg_database_link_sequence
 * @mgdb: a #MgDatabase object
 * @seq:
 * @field:
 *
 * Tells the database that each new value of the field given as argument should be
 * obtained from the specified sequence (this is usefull when the field is a simple
 * primary key for example).
 */
void
mg_database_link_sequence (MgDatabase *mgdb, MgDbSequence *seq, MgDbField *field)
{
	g_return_if_fail (mgdb && IS_MG_DATABASE (mgdb));
	g_return_if_fail (seq && IS_MG_DB_SEQUENCE (seq));
	g_return_if_fail (field && IS_MG_DB_FIELD (field));

	TO_IMPLEMENT;	
}

/**
 * mg_database_unlink_sequence
 * @mgdb: a #MgDatabase object
 * @seq:
 * @field:
 *
 * Tells the database that each new value of the field given as argument should not be
 * obtained from the specified sequence (this is usefull when the field is a simple
 * primary key for example). This is the opposite of the mg_database_link_sequence() method.
 */
void
mg_database_unlink_sequence (MgDatabase *mgdb, MgDbSequence *seq, MgDbField *field)
{
	g_return_if_fail (mgdb && IS_MG_DATABASE (mgdb));
	g_return_if_fail (seq && IS_MG_DB_SEQUENCE (seq));
	g_return_if_fail (field && IS_MG_DB_FIELD (field));

	TO_IMPLEMENT;	
}

/**
 * mg_database_get_all_constraints
 * @mgdb: a #MgDatabase object
 *
 * Get a list of all the constraints applied to the database. Constraints are represented
 * as #MgDbConstraint objects and represent any type of constraint.
 *
 * Returns: a new list of the constraints
 */
GSList *
mg_database_get_all_constraints (MgDatabase *mgdb)
{
	g_return_val_if_fail (mgdb && IS_MG_DATABASE (mgdb), NULL);
	g_return_val_if_fail (mgdb->priv, NULL);

	return g_slist_copy (mgdb->priv->constraints);
}

/**
 * mg_database_get_fk_constraints
 * @mgdb: a #MgDatabase object
 *
 * Get a list of all the constraints applied to the database which represent a foreign constrains. 
 * Constraints are represented as #MgDbConstraint objects.
 *
 * Returns: a new list of the constraints
 */
GSList *
mg_database_get_fk_constraints (MgDatabase *mgdb)
{
	GSList *retval = NULL, *list;

	g_return_val_if_fail (mgdb && IS_MG_DATABASE (mgdb), NULL);
	g_return_val_if_fail (mgdb->priv, NULL);

	list = mgdb->priv->constraints;
	while (list) {
		if (mg_db_constraint_get_constraint_type (MG_DB_CONSTRAINT (list->data)) == 
		    CONSTRAINT_FOREIGN_KEY)
			retval = g_slist_append (retval, list->data);

		list = g_slist_next (list);
	}

	return retval;
}
