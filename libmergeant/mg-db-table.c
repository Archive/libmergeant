/* mg-db-table.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-db-table.h"
#include "mg-database.h"
#include "mg-db-field.h"
#include "mg-db-constraint.h"
#include "mg-xml-storage.h"
#include "mg-field.h"
#include "mg-entity.h"
#include "marshal.h"
#include "mg-data-handler.h"
#include "mg-server.h"
#include "mg-server-data-type.h"
#include "mg-result-set.h"
#include <string.h>
#include "mg-ref-base.h"

/* 
 * Main static functions 
 */
static void mg_db_table_class_init (MgDbTableClass * class);
static void mg_db_table_init (MgDbTable * srv);
static void mg_db_table_dispose (GObject   * object);
static void mg_db_table_finalize (GObject   * object);

static void mg_db_table_set_property (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec);
static void mg_db_table_get_property (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec);

/* XML storage interface */
static void        mg_db_table_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_db_table_get_xml_id (MgXmlStorage *iface);
static xmlNodePtr  mg_db_table_save_to_xml (MgXmlStorage *iface, GError **error);
static gboolean    mg_db_table_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error);

/* Entity interface */
static void        mg_db_table_entity_init         (MgEntityIface *iface);
static GSList     *mg_db_table_get_all_fields      (MgEntity *iface);
static GSList     *mg_db_table_get_visible_fields  (MgEntity *iface);
static MgField    *mg_db_table_get_field_by_name   (MgEntity *iface, const gchar *name);
static MgField    *mg_db_table_get_field_by_xml_id (MgEntity *iface, const gchar *xml_id);
static MgField    *mg_db_table_get_field_by_index  (MgEntity *iface, gint index);
static gint        mg_db_table_get_field_index     (MgEntity *iface, MgField *field);
static void        mg_db_table_add_field           (MgEntity *iface, MgField *field);
static void        mg_db_table_add_field_before    (MgEntity *iface, MgField *field, MgField *field_before);
static void        mg_db_table_swap_fields         (MgEntity *iface, MgField *field1, MgField *field2);
static void        mg_db_table_remove_field        (MgEntity *iface, MgField *field);
static gboolean    mg_db_table_is_writable         (MgEntity *iface);
static GSList     *mg_db_table_get_parameters      (MgEntity *iface);


static void        mg_db_table_set_database        (MgDbTable *table, MgDatabase *db);
static void        mg_db_table_add_field_at_pos    (MgDbTable *table, MgDbField *field, gint pos);

/* When the Database is nullified */
static void        nullified_object_cb (GObject *obj, MgDbTable *table);
static void        nullified_field_cb  (GObject *obj, MgDbTable *table);
static void        nullified_parent_cb (GObject *obj, MgDbTable *table);

static void        changed_field_cb    (GObject *obj, MgDbTable *table);


#ifdef debug
static void        mg_db_table_dump    (MgDbTable *table, guint offset);
#endif


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	TEMPL_SIGNAL,
	LAST_SIGNAL
};

static gint mg_db_table_signals[LAST_SIGNAL] = { 0 };

/* properties */
enum
{
	PROP_0,
	PROP_DB
};


/* private structure */
struct _MgDbTablePrivate
{
	MgDatabase *db;
	GSList     *fields;
	gboolean    is_view;
	GSList     *parents;     /* list of other MgDbTable objects which are parents */
};
#define TABLE_SRV(table) (mg_conf_get_server (mg_base_get_conf (MG_BASE (table))))


/* module error */
GQuark mg_db_table_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_db_table_error");
	return quark;
}


guint
mg_db_table_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgDbTableClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_db_table_class_init,
			NULL,
			NULL,
			sizeof (MgDbTable),
			0,
			(GInstanceInitFunc) mg_db_table_init
		};

		static const GInterfaceInfo xml_storage_info = {
			(GInterfaceInitFunc) mg_db_table_xml_storage_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo entity_info = {
			(GInterfaceInitFunc) mg_db_table_entity_init,
			NULL,
			NULL
		};
		
		type = g_type_register_static (MG_BASE_TYPE, "MgDbTable", &info, 0);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
		g_type_add_interface_static (type, MG_ENTITY_TYPE, &entity_info);
	}
	return type;
}

static void 
mg_db_table_xml_storage_init (MgXmlStorageIface *iface)
{
	iface->get_xml_id = mg_db_table_get_xml_id;
	iface->save_to_xml = mg_db_table_save_to_xml;
	iface->load_from_xml = mg_db_table_load_from_xml;
}

static void
mg_db_table_entity_init (MgEntityIface *iface)
{
	iface->get_all_fields = mg_db_table_get_all_fields;
	iface->get_visible_fields = mg_db_table_get_visible_fields;
	iface->get_field_by_name = mg_db_table_get_field_by_name;
	iface->get_field_by_xml_id = mg_db_table_get_field_by_xml_id;
	iface->get_field_by_index = mg_db_table_get_field_by_index;
	iface->get_field_index = mg_db_table_get_field_index;
	iface->add_field = mg_db_table_add_field;
	iface->add_field_before = mg_db_table_add_field_before;
	iface->swap_fields = mg_db_table_swap_fields;
	iface->remove_field = mg_db_table_remove_field;
	iface->is_writable = mg_db_table_is_writable;
	iface->get_parameters = mg_db_table_get_parameters;
}


static void
mg_db_table_class_init (MgDbTableClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_db_table_signals[TEMPL_SIGNAL] =
		g_signal_new ("templ_signal",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgDbTableClass, templ_signal),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	class->templ_signal = NULL;

	object_class->dispose = mg_db_table_dispose;
	object_class->finalize = mg_db_table_finalize;

	/* Properties */
	object_class->set_property = mg_db_table_set_property;
	object_class->get_property = mg_db_table_get_property;
	g_object_class_install_property (object_class, PROP_DB,
					 g_param_spec_pointer ("database", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_db_table_dump;
#endif

}

static void
mg_db_table_init (MgDbTable * mg_db_table)
{
	mg_db_table->priv = g_new0 (MgDbTablePrivate, 1);
	mg_db_table->priv->db = NULL;
	mg_db_table->priv->fields = NULL;
	mg_db_table->priv->is_view = FALSE;
	mg_db_table->priv->parents = NULL;
}

/**
 * mg_db_table_new
 * @conf: a #MgConf object
 *
 * Creates a new MgDbTable object
 *
 * Returns: the new object
 */
GObject*
mg_db_table_new (MgConf *conf)
{
	GObject   *obj;
	MgDbTable *mg_db_table;

	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);

	obj = g_object_new (MG_DB_TABLE_TYPE, "conf", conf, NULL);
	mg_db_table = MG_DB_TABLE (obj);
	mg_base_set_id (MG_BASE (mg_db_table), 0);

	return obj;
}

static void 
nullified_field_cb (GObject *obj, MgDbTable *table)
{
	g_assert (g_slist_find (table->priv->fields, obj));

	table->priv->fields = g_slist_remove (table->priv->fields, obj);
	g_signal_handlers_disconnect_by_func (G_OBJECT (obj), 
					      G_CALLBACK (nullified_field_cb), table);
	g_signal_handlers_disconnect_by_func (G_OBJECT (obj), 
					      G_CALLBACK (changed_field_cb), table);

#ifdef debug_signal
	g_print (">> 'FIELD_REMOVED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (table), "field_removed", obj);
#ifdef debug_signal
	g_print ("<< 'FIELD_REMOVED' from %s\n", __FUNCTION__);
#endif

	g_object_set (obj, "db_table", NULL, NULL);	
	g_object_unref (obj);
}

static void 
nullified_parent_cb (GObject *obj, MgDbTable *table)
{
	g_assert (g_slist_find (table->priv->parents, obj));
	g_signal_handlers_disconnect_by_func (G_OBJECT (obj), 
					      G_CALLBACK (nullified_parent_cb), table);
	table->priv->parents = g_slist_remove (table->priv->parents, obj);
}

static void
mg_db_table_dispose (GObject *object)
{
	MgDbTable *mg_db_table;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_DB_TABLE (object));

	mg_db_table = MG_DB_TABLE (object);
	if (mg_db_table->priv) {
		GSList *list;

		mg_base_nullify_check (MG_BASE (object));
		
		while (mg_db_table->priv->fields)
			mg_base_nullify (MG_BASE (mg_db_table->priv->fields->data));

		list = mg_db_table->priv->parents;
		while (list) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (list->data),
							      G_CALLBACK (nullified_parent_cb), mg_db_table);
			list = g_slist_next (list);
		}
		if (mg_db_table->priv->parents) {
			g_slist_free (mg_db_table->priv->parents);
			mg_db_table->priv->parents = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_db_table_finalize (GObject   * object)
{
	MgDbTable *mg_db_table;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_DB_TABLE (object));

	mg_db_table = MG_DB_TABLE (object);
	if (mg_db_table->priv) {

		g_free (mg_db_table->priv);
		mg_db_table->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_db_table_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	gpointer ptr;
	MgDbTable *mg_db_table;

	mg_db_table = MG_DB_TABLE (object);
	if (mg_db_table->priv) {
		switch (param_id) {
		case PROP_DB:
			ptr = g_value_get_pointer (value);
			mg_db_table_set_database (mg_db_table, MG_DATABASE (ptr));
			break;
		}
	}
}

static void
mg_db_table_get_property (GObject              *object,
			  guint                 param_id,
			  GValue               *value,
			  GParamSpec           *pspec)
{
	MgDbTable *mg_db_table;
	mg_db_table = MG_DB_TABLE (object);
	
	if (mg_db_table->priv) {
		switch (param_id) {
		case PROP_DB:
			g_value_set_pointer (value, mg_db_table->priv->db);
			break;
		}	
	}
}

static void
mg_db_table_set_database (MgDbTable *table, MgDatabase *db)
{
	if (table->priv->db) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (table->priv->db),
						      G_CALLBACK (nullified_object_cb), table);
		table->priv->db = NULL;
	}
	
	if (db && IS_MG_DATABASE (db)) {
		table->priv->db = MG_DATABASE (db);
		g_signal_connect (G_OBJECT (db), "nullified",
				  G_CALLBACK (nullified_object_cb), table);
	}
}

static void
nullified_object_cb (GObject *obj, MgDbTable *table)
{
	mg_base_nullify (MG_BASE (table));
}

/**
 * mg_db_table_get_database
 * @table: a #MgDbTable object
 *
 * Get the database to which the table belongs
 *
 * Returns: a #MgDatabase pointer
 */
MgDatabase
*mg_db_table_get_database (MgDbTable *table)
{
	g_return_val_if_fail (table && IS_MG_DB_TABLE (table), NULL);
	g_return_val_if_fail (table->priv, NULL);

	return table->priv->db;
}

/**
 * mg_db_table_is_view
 * @table: a #MgDbTable object
 *
 * Does the object represent a view rather than a table?
 *
 * Returns: TRUE if it is a view
 */
gboolean
mg_db_table_is_view (MgDbTable *table)
{
	g_return_val_if_fail (table && IS_MG_DB_TABLE (table), FALSE);
	g_return_val_if_fail (table->priv, FALSE);

	return table->priv->is_view;
}

/**
 * mg_db_table_get_parents
 * @table: a #MgDbTable object
 *
 * Get the parent tables of the table given as argument. This is significant only
 * for DBMS which support tables inheritance (like PostgreSQL for example).
 *
 * Returns: a constant list of #MgDbTable objects
 */
const GSList *
mg_db_table_get_parents (MgDbTable *table)
{
	g_return_val_if_fail (table && IS_MG_DB_TABLE (table), NULL);
	g_return_val_if_fail (table->priv, NULL);

	return table->priv->parents;
}

/**
 * mg_db_table_get_constraints
 * @table: a #MgDbTable object
 *
 * Get all the constraints which apply to the given table (each constraint
 * can represent a NOT NULL, a primary key or foreign key or a check constraint.
 *
 * Returns: a new list of #MgDbConstraint objects
 */
GSList *
mg_db_table_get_constraints (MgDbTable *table)
{
	GSList *retval = NULL;
	GSList *db_constraints, *list;

	g_return_val_if_fail (table && IS_MG_DB_TABLE (table), NULL);
	g_return_val_if_fail (table->priv, NULL);

	db_constraints = mg_database_get_all_constraints (table->priv->db);
	list = db_constraints;
	while (list) {
		if (mg_db_constraint_get_table (MG_DB_CONSTRAINT (list->data)) == table)
			retval = g_slist_append (retval, list->data);
		list = g_slist_next (list);
	}
	g_slist_free (db_constraints);

	return retval;
}


/**
 * mg_db_table_get_conf
 * @table: a #MgDbTable object
 *
 * Get the #MgConf to which the @table is associated
 *
 * Returns: the #MgConf object
 */
MgConf *
mg_db_table_get_conf (MgDbTable *table)
{
	g_return_val_if_fail (table && IS_MG_DB_TABLE (table), NULL);
	g_return_val_if_fail (MG_DB_TABLE (table)->priv, NULL);

	return mg_base_get_conf (MG_BASE (table));
}


/**
 * mg_database_update_dbms_data
 * @table: a #MgDbTable object
 * @error: location to store error, or %NULL
 *
 * Synchronises the Table representation with the table structure which is stored in
 * the DBMS. For this operation to succeed, the connection to the DBMS server MUST be opened
 * (using the corresponding #MgServer object).
 *
 * Returns: TRUE if no error
 */
gboolean
mg_db_table_update_dbms_data (MgDbTable *table, GError **error)
{
	MgConf *conf;
	GSList *fields;
	GdaDataModel *rs;
	gchar *str;
	guint now, total;
	GSList *updated_fields = NULL;
	MgServer *srv;
	MgDbField *field;
	GdaParameterList *paramlist;
        GdaParameter *param;
	gint current_position = 0;
	GSList *constraints;
	GHashTable *fk_hash;
	
	g_return_val_if_fail (table && IS_MG_DB_TABLE (table), FALSE);
	g_return_val_if_fail (MG_DB_TABLE (table)->priv, FALSE);

	/* g_print ("################ TABLE %s\n", mg_base_get_name (MG_BASE (table))); */
	conf = mg_base_get_conf (MG_BASE (table));
	srv = mg_conf_get_server (conf);
	if (!mg_server_conn_is_opened (srv)) {
		g_set_error (error, MG_DB_TABLE_ERROR, MG_DB_TABLE_META_DATA_UPDATE,
			     _("Connection is not opened!"));
		return FALSE;
	}

	/* In this procedure, we are creating some new MgDbConstraint objects to express
	 * the constraints the database has on this table. This list is "attached" to the MgDbTable
	 * object by the "pending_constraints" keyword, and is later normally fetched by the MgDatabase object.
	 * If we still have one at this point, we must emit a warning and free that list.
	 */
	if ((constraints = g_object_get_data (G_OBJECT (table), "pending_constraints"))) {
		GSList *list = constraints;

		g_warning ("MgDbTable object %p has a non empty list of pending constraints", table);
		while (list) {
			g_object_unref (G_OBJECT (list->data));
			list = g_slist_next (list);
		}
		g_slist_free (constraints);
		constraints = NULL;
		g_object_set_data (G_OBJECT (table), "pending_constraints", NULL);
	}
	fk_hash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);

	/* parameters list */
	paramlist = gda_parameter_list_new ();
        param = gda_parameter_new_string ("name", mg_base_get_name (MG_BASE (table)));
        gda_parameter_list_add_parameter (paramlist, param);
	rs = mg_server_get_gda_schema (srv, GDA_CONNECTION_SCHEMA_FIELDS, paramlist);
	gda_parameter_list_free (paramlist);

	/* Result set analysis */
	if (!mg_resultset_check_data_model (rs, 9, 
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_STRING,
					    GDA_VALUE_TYPE_INTEGER,
					    GDA_VALUE_TYPE_INTEGER,
					    GDA_VALUE_TYPE_BOOLEAN,
					    GDA_VALUE_TYPE_BOOLEAN,
					    GDA_VALUE_TYPE_BOOLEAN,
					    GDA_VALUE_TYPE_STRING, -1)) {
		g_set_error (error, MG_DB_TABLE_ERROR, MG_DB_TABLE_FIELDS_ERROR,
			     _("Schema for list of fields is wrong"));
		g_object_unref (G_OBJECT (rs));
		return FALSE;
	}

	/* Resultset parsing */
	total = gda_data_model_get_n_rows (rs);
	now = 0;		
	while (now < total) {
		const GdaValue *value;
		gboolean newfield = FALSE;
		MgField *mgf;

		value = gda_data_model_get_value_at (rs, 0, now);
		str = gda_value_stringify (value);
		mgf = mg_db_table_get_field_by_name (MG_ENTITY (table), str);
		if (!mgf) {
			/* field name */
			field = MG_DB_FIELD (mg_db_field_new (mg_base_get_conf (MG_BASE (table)), NULL));
			mg_base_set_name (MG_BASE (field), str);
			newfield = TRUE;
		}
		else {
			field = MG_DB_FIELD (mgf);
			current_position = g_slist_index (table->priv->fields, field) + 1;
		}
		g_free (str);
		
		updated_fields = g_slist_append (updated_fields, field);

		/* FIXME: No description for fields in the schema. */
		/* FIXME: No owner for fields in the schema. */
		
		/* Data type */
		value = gda_data_model_get_value_at (rs, 1, now);
		if (value && !gda_value_is_null (value) && (* gda_value_get_string(value))) {
			MgServerDataType *type;

			str = gda_value_stringify (value);
			type = mg_server_get_data_type_by_name (srv, str);
			if (type)
				mg_db_field_set_data_type (field, type);
			g_free (str);
		}
		if (!mg_field_get_data_type (MG_FIELD (field))) {
			g_set_error (error, MG_DB_TABLE_ERROR, MG_DB_TABLE_FIELDS_ERROR,
				     _("Can't find data type"));
			g_object_unref (G_OBJECT (rs));
			return FALSE;
		}

		/* Size */
		value = gda_data_model_get_value_at (rs, 2, now);
		if (value && !gda_value_is_null (value)) 
			mg_db_field_set_length (field, gda_value_get_integer (value));

		/* Scale */
		value = gda_data_model_get_value_at (rs, 3, now);
		if (value && !gda_value_is_null (value)) 
			mg_db_field_set_scale (field, gda_value_get_integer (value));

		/* Default value */
		value = gda_data_model_get_value_at (rs, 8, now);
		if (value && !gda_value_is_null (value)) {
			gchar *defv = gda_value_stringify (value);
			if (*defv)
				mg_db_field_set_default_value (field, value);
			g_free (defv);
		}
				
		/* signal if the field is new */
		if (newfield) {
			g_object_set (G_OBJECT (field), "db_table", table, NULL);
			mg_db_table_add_field_at_pos (table, field, current_position++);
			g_object_unref (G_OBJECT (field));
		}
		
		/* NOT NULL constraint */
		value = gda_data_model_get_value_at (rs, 4, now);
		if (value && !gda_value_is_null (value) && !gda_value_get_boolean (value)) {
			MgDbConstraint *cstr = MG_DB_CONSTRAINT (mg_db_constraint_new (table, CONSTRAINT_NOT_NULL));
			mg_db_constraint_not_null_set_field (cstr, field);
			constraints = g_slist_append (constraints, cstr);
		}

		/* Other constraints:
		 * For constraints other than the NOT NULL constraint, this is a temporary solution to
		 * get the constraints before we get a real constraints request schema in libgda.
		 *
		 * THE FOLLOWING ASSUMPTIONS ARE MADE:
		 * PRIMARY KEY: there is only ONE primary key per table even if it is a composed primary key
		 * UNIQUE: each field with the UNIQUE attribute is considered unique itself (=> no constraint
		 * for unique couple (or more) fields)
		 * FOREIGN KEY: we don't have the associated actions to ON UPDATE and ON DELETE actions
		 */

		/* PRIMARY KEY constraint */
		value = gda_data_model_get_value_at (rs, 5, now);
		if (value && !gda_value_is_null (value) && gda_value_get_boolean (value)) {
			MgDbConstraint *cstr = NULL;
			GSList *list = constraints, *nlist;
			
			/* find the primary key constraint if it already exists */
			while (list && !cstr) {
				if (mg_db_constraint_get_constraint_type (MG_DB_CONSTRAINT (list->data)) ==
				    CONSTRAINT_PRIMARY_KEY)
					cstr = MG_DB_CONSTRAINT (list->data);
				list = g_slist_next (list);
			}

			if (!cstr) {
				cstr = MG_DB_CONSTRAINT (mg_db_constraint_new (table, CONSTRAINT_PRIMARY_KEY));
				constraints = g_slist_append (constraints, cstr);
			}

			/* set the fields */
			nlist = mg_db_constraint_pkey_get_fields (cstr);
			nlist = g_slist_append (nlist, field);
			mg_db_constraint_pkey_set_fields (cstr, nlist);
			g_slist_free (nlist);
		}

		/* UNIQUE constraint */
		value = gda_data_model_get_value_at (rs, 6, now);
		if (value && !gda_value_is_null (value) && gda_value_get_boolean (value)) {
			MgDbConstraint *cstr;
			GSList *nlist;

			cstr = MG_DB_CONSTRAINT (mg_db_constraint_new (table, CONSTRAINT_UNIQUE));
			constraints = g_slist_append (constraints, cstr);

			nlist = g_slist_append (NULL, field);
			mg_db_constraint_unique_set_fields (cstr, nlist);
			g_slist_free (nlist);
		}

		/* FOREIGN KEY constraint */
		value = gda_data_model_get_value_at (rs, 7, now);
		if (value && !gda_value_is_null (value) && (* gda_value_get_string (value))) {
			gchar *ref_table, *str, *tok;
			MgRefBase *ref;
			MgDbConstraint *cstr = NULL;
			GSList *list, *nlist;
			MgDbConstraintFkeyPair *pair;

			/* ref table */
			str = g_strdup (gda_value_get_string (value));
			ref_table = g_strdup (strtok_r (str, ".", &tok));
			g_free (str);
			
			ref = MG_REF_BASE (mg_ref_base_new (mg_base_get_conf (MG_BASE (table))));
			mg_ref_base_set_ref_name (ref, MG_DB_FIELD_TYPE, 
						  REFERENCE_BY_NAME, gda_value_get_string (value));
			
			/* find the foreign key constraint if it already exists */
			cstr = g_hash_table_lookup (fk_hash, ref_table);
			if (!cstr) {
				cstr = MG_DB_CONSTRAINT (mg_db_constraint_new (table, CONSTRAINT_FOREIGN_KEY));
				constraints = g_slist_append (constraints, cstr);
				g_hash_table_insert (fk_hash, ref_table, cstr);
			}
			else
				g_free (ref_table);
			
			nlist = mg_db_constraint_fkey_get_fields (cstr);
			pair = g_new0 (MgDbConstraintFkeyPair, 1);
			pair->fkey = field;
			pair->ref_pkey = NULL;
			pair->ref_pkey_repl = ref;
			nlist = g_slist_append (nlist, pair);
			mg_db_constraint_fkey_set_fields (cstr, nlist);
			
			/* memory libreation */
			list = nlist;
			while (list) {
				g_free (list->data);
				list = g_slist_next (list);
			}
			g_object_unref (G_OBJECT (ref));
			g_slist_free (nlist);
		}
		
		now++;
	}
	    
	g_object_unref (G_OBJECT (rs));
	
	/* remove the fields not existing anymore */
	fields = table->priv->fields;
	while (fields) {
		if (!g_slist_find (updated_fields, fields->data)) {
			mg_base_nullify (MG_BASE (fields->data));
			fields = table->priv->fields;
		}
		else
			fields = g_slist_next (fields);
	}
	g_slist_free (updated_fields);

	/* stick the constraints list to the MgDbTable object */
	g_object_set_data (G_OBJECT (table), "pending_constraints", constraints);
	g_hash_table_destroy (fk_hash);
	
	return TRUE;
}


/*
 * pos = -1 to append
 */
static void
mg_db_table_add_field_at_pos (MgDbTable *table, MgDbField *field, gint pos)
{
	table->priv->fields = g_slist_insert (table->priv->fields, field, pos);

	g_object_ref (G_OBJECT (field));
	g_signal_connect (G_OBJECT (field), "nullified",
			  G_CALLBACK (nullified_field_cb), table);
	g_signal_connect (G_OBJECT (field), "changed",
			  G_CALLBACK (changed_field_cb), table);

#ifdef debug_signal
	g_print (">> 'FIELD_ADDED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (table), "field_added", field);
#ifdef debug_signal
	g_print ("<< 'FIELD_ADDED' from %s\n", __FUNCTION__);
#endif	
}


static void
changed_field_cb (GObject *obj, MgDbTable *table)
{
#ifdef debug_signal
	g_print (">> 'FIELD_UPDATED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (table), "field_updated", obj);
#ifdef debug_signal
	g_print ("<< 'FIELD_UPDATED' from %s\n", __FUNCTION__);
#endif	
}


#ifdef debug
static void
mg_db_table_dump (MgDbTable *table, guint offset)
{
	gchar *str;
        guint i;
        GSList *list;
	
	g_return_if_fail (table && IS_MG_DB_TABLE (table));

        /* string for the offset */
        str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;

        /* dump */
        if (table->priv)
                g_print ("%s" D_COL_H1 "MgDbTable" D_COL_NOR  " %s (%p)\n",
                         str, mg_base_get_name (MG_BASE (table)), table);
        else
                g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, table);

	/* fields */
        list = table->priv->fields;
        if (list) {
                g_print ("%sFields:\n", str);
                while (list) {
                        mg_base_dump (MG_BASE (list->data), offset+5);
                        list = g_slist_next (list);
                }
        }
        else
                g_print ("%sContains no field\n", str);

}
#endif





/* 
 * MgEntity interface implementation
 */
static GSList *
mg_db_table_get_all_fields (MgEntity *iface)
{
	g_return_val_if_fail (iface && IS_MG_DB_TABLE (iface), NULL);
	g_return_val_if_fail (MG_DB_TABLE (iface)->priv, NULL);

	return g_slist_copy (MG_DB_TABLE (iface)->priv->fields);
}

static GSList *
mg_db_table_get_visible_fields (MgEntity *iface)
{
	g_return_val_if_fail (iface && IS_MG_DB_TABLE (iface), NULL);
	g_return_val_if_fail (MG_DB_TABLE (iface)->priv, NULL);

	/* in a table, there is no hidden field */
	return g_slist_copy (MG_DB_TABLE (iface)->priv->fields);
}

static MgField *
mg_db_table_get_field_by_name (MgEntity *iface, const gchar *name)
{
	MgField *field = NULL;
	GSList *list;

	g_return_val_if_fail (iface && IS_MG_DB_TABLE (iface), NULL);
	g_return_val_if_fail (MG_DB_TABLE (iface)->priv, NULL);

	list = MG_DB_TABLE (iface)->priv->fields;
	while (list && !field) {
		if (!strcmp (mg_field_get_name (MG_FIELD (list->data)), name))
			field = MG_FIELD (list->data);
		list = g_slist_next (list);
	}

	return field;
}

static MgField *
mg_db_table_get_field_by_xml_id (MgEntity *iface, const gchar *xml_id)
{
	MgField *field = NULL;
	GSList *list;
	gchar *str;

	g_return_val_if_fail (iface && IS_MG_DB_TABLE (iface), NULL);
	g_return_val_if_fail (MG_DB_TABLE (iface)->priv, NULL);


	list = MG_DB_TABLE (iface)->priv->fields;
	while (list && !field) {
		str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
		if (!strcmp (str, xml_id))
			field = MG_FIELD (list->data);
		g_free (str);
		list = g_slist_next (list);
	}

	return field;
}

static MgField *
mg_db_table_get_field_by_index (MgEntity *iface, gint index)
{
	g_return_val_if_fail (iface && IS_MG_DB_TABLE (iface), NULL);
	g_return_val_if_fail (MG_DB_TABLE (iface)->priv, NULL);
	g_return_val_if_fail (index >= 0, NULL);
	g_return_val_if_fail (index < g_slist_length (MG_DB_TABLE (iface)->priv->fields), NULL);
	
	return MG_FIELD (g_slist_nth_data (MG_DB_TABLE (iface)->priv->fields, index));
}

static gint
mg_db_table_get_field_index (MgEntity *iface, MgField *field)
{
	g_return_val_if_fail (iface && IS_MG_DB_TABLE (iface), -1);
	g_return_val_if_fail (MG_DB_TABLE (iface)->priv, -1);

	return g_slist_index (MG_DB_TABLE (iface)->priv->fields, field);
}

static void
mg_db_table_add_field (MgEntity *iface, MgField *field)
{
	g_return_if_fail (iface && IS_MG_DB_TABLE (iface));
	g_return_if_fail (MG_DB_TABLE (iface)->priv);
	g_return_if_fail (field && IS_MG_DB_FIELD (field));
	g_return_if_fail (!g_slist_find (MG_DB_TABLE (iface)->priv->fields, field));
	g_return_if_fail (mg_field_get_entity (field) == iface);
	
	mg_db_table_add_field_at_pos (MG_DB_TABLE (iface), MG_DB_FIELD (field), -1);
}

static void
mg_db_table_add_field_before (MgEntity *iface, MgField *field, MgField *field_before)
{
	MgDbTable *table;
	gint pos = -1;

	g_return_if_fail (iface && IS_MG_DB_TABLE (iface));
	g_return_if_fail (MG_DB_TABLE (iface)->priv);
	table = MG_DB_TABLE (iface);

	g_return_if_fail (field && IS_MG_DB_FIELD (field));
	g_return_if_fail (!g_slist_find (MG_DB_TABLE (iface)->priv->fields, field));
	g_return_if_fail (mg_field_get_entity (field) == iface);
	if (field_before) {
		g_return_if_fail (field_before && IS_MG_DB_FIELD (field_before));
		g_return_if_fail (g_slist_find (MG_DB_TABLE (iface)->priv->fields, field_before));
		pos = g_slist_index (table->priv->fields, field_before);
	}

	mg_db_table_add_field_at_pos (table, MG_DB_FIELD (field), pos);
}

static void
mg_db_table_swap_fields (MgEntity *iface, MgField *field1, MgField *field2)
{
	GSList *ptr1, *ptr2;

	g_return_if_fail (iface && IS_MG_DB_TABLE (iface));
	g_return_if_fail (MG_DB_TABLE (iface)->priv);
	g_return_if_fail (field1 && IS_MG_DB_FIELD (field1));
	g_return_if_fail (field2 && IS_MG_DB_FIELD (field2));
	ptr1 = g_slist_find (MG_DB_TABLE (iface)->priv->fields, field1);
	ptr2 = g_slist_find (MG_DB_TABLE (iface)->priv->fields, field2);
	g_return_if_fail (ptr1);
	g_return_if_fail (ptr2);
	
	ptr1->data = field2;
	ptr2->data = field1;

#ifdef debug_signal
	g_print (">> 'FIELDS_ORDER_CHANGED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (iface), "fields_order_changed");
#ifdef debug_signal
	g_print ("<< 'FIELDS_ORDER_CHANGED' from %s\n", __FUNCTION__);
#endif

}

static void
mg_db_table_remove_field (MgEntity *iface, MgField *field)
{
	g_return_if_fail (iface && IS_MG_DB_TABLE (iface));
	g_return_if_fail (MG_DB_TABLE (iface)->priv);
	g_return_if_fail (field && IS_MG_DB_FIELD (field));
	g_return_if_fail (g_slist_find (MG_DB_TABLE (iface)->priv->fields, field));

	nullified_field_cb (G_OBJECT (field), MG_DB_TABLE (iface));
}

static gboolean
mg_db_table_is_writable (MgEntity *iface)
{
	g_return_val_if_fail (iface && IS_MG_DB_TABLE (iface), FALSE);
	g_return_val_if_fail (MG_DB_TABLE (iface)->priv, FALSE);
	
	return MG_DB_TABLE (iface)->priv->is_view ? FALSE : TRUE;
}

static GSList *
mg_db_table_get_parameters (MgEntity *iface)
{
	g_return_val_if_fail (iface && IS_MG_DB_TABLE (iface), NULL);
	g_return_val_if_fail (MG_DB_TABLE (iface)->priv, NULL);
	
	return NULL;
}


/* 
 * MgXmlStorage interface implementation
 */
static gchar *
mg_db_table_get_xml_id (MgXmlStorage *iface)
{
	g_return_val_if_fail (iface && IS_MG_DB_TABLE (iface), NULL);
	g_return_val_if_fail (MG_DB_TABLE (iface)->priv, NULL);

	return g_strdup_printf ("TV%s", mg_base_get_name (MG_BASE (iface)));
}

static xmlNodePtr
mg_db_table_save_to_xml (MgXmlStorage *iface, GError **error)
{
	xmlNodePtr node = NULL;
	MgDbTable *table;
	gchar *str;
	GSList *list;
	gint i;

	g_return_val_if_fail (iface && IS_MG_DB_TABLE (iface), NULL);
	g_return_val_if_fail (MG_DB_TABLE (iface)->priv, NULL);

	table = MG_DB_TABLE (iface);

	node = xmlNewNode (NULL, "MG_TABLE");
	
	str = mg_db_table_get_xml_id (iface);
	xmlSetProp (node, "id", str);
	g_free (str);
	xmlSetProp (node, "name", mg_base_get_name (MG_BASE (table)));
	xmlSetProp (node, "owner", mg_base_get_owner (MG_BASE (table)));
	xmlSetProp (node, "descr", mg_base_get_description (MG_BASE (table)));

	xmlSetProp (node, "is_view", table->priv->is_view ? "t" : "f");

	/* parent tables */
	i = 0;
	list = table->priv->parents;
	while (list) {
		xmlNodePtr parent;
		gchar *str;

		parent = xmlNewChild (node, NULL, "MG_PARENT_TABLE", NULL);
		str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
		xmlSetProp (parent, "table", str);
		g_free (str);

		str = g_strdup_printf ("%d", i);
		xmlSetProp (parent, "order", str);
		g_free (str);

		list = g_slist_next (list);
	}
	
	/* fields */
	list = table->priv->fields;
	while (list) {
		xmlNodePtr field;
		
		field = mg_xml_storage_save_to_xml (MG_XML_STORAGE (list->data), error);

		if (field)
			xmlAddChild (node, field);
		else {
			xmlFreeNode (node);
			return NULL;
		}
		list = g_slist_next (list);
	}

	return node;
}

static gboolean
mg_db_table_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgDbTable *table;
	gchar *prop;
	gboolean name = FALSE;
	xmlNodePtr children;

	g_return_val_if_fail (iface && IS_MG_DB_TABLE (iface), FALSE);
	g_return_val_if_fail (MG_DB_TABLE (iface)->priv, FALSE);
	g_return_val_if_fail (node, FALSE);

	table = MG_DB_TABLE (iface);
	if (strcmp (node->name, "MG_TABLE")) {
		g_set_error (error,
			     MG_DB_TABLE_ERROR,
			     MG_DB_TABLE_XML_LOAD_ERROR,
			     _("XML Tag is not <MG_TABLE>"));
		return FALSE;
	}

	prop = xmlGetProp (node, "name");
	if (prop) {
		name = TRUE;
		mg_base_set_name (MG_BASE (table), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "descr");
	if (prop) {
		mg_base_set_description (MG_BASE (table), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "owner");
	if (prop) {
		mg_base_set_owner (MG_BASE (table), prop);
		g_free (prop);
	}

	table->priv->is_view = FALSE;
	prop = xmlGetProp (node, "is_view");
	if (prop) {
		table->priv->is_view = (*prop == 't') ? TRUE : FALSE;
		g_free (prop);
	}

	children = node->children;
	while (children) {
		gboolean done = FALSE;

		/* parent table */
		if (!strcmp (children->name, "MG_PARENT_TABLE")) {
			TO_IMPLEMENT;
			done = TRUE;
		}
		/* fields */
		if (!done && !strcmp (children->name, "MG_FIELD")) {
			MgDbField *field;
			field = MG_DB_FIELD (mg_db_field_new (mg_base_get_conf (MG_BASE (iface)), NULL));
			if (mg_xml_storage_load_from_xml (MG_XML_STORAGE (field), children, error)) {
				g_object_set (G_OBJECT (field), "db_table", table, NULL);
				mg_db_table_add_field (MG_ENTITY (table), MG_FIELD (field));
				g_object_unref (G_OBJECT (field));
			}
			else
				return FALSE;
		}
		
		children = children->next;
	}

	if (name)
		return TRUE;
	else {
		g_set_error (error,
			     MG_DB_TABLE_ERROR,
			     MG_DB_TABLE_XML_LOAD_ERROR,
			     _("Missing required attributes for <MG_TABLE>"));
		return FALSE;
	}
}

