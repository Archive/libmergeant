/* mg-renderer.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_RENDERER_H_
#define __MG_RENDERER_H_

#include <glib-object.h>
#include <libxml/tree.h>
#include <libgda/gda-xql-item.h>
#include "mg-defs.h"

G_BEGIN_DECLS

#define MG_RENDERER_TYPE          (mg_renderer_get_type())
#define MG_RENDERER(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_renderer_get_type(), MgRenderer)
#define IS_MG_RENDERER(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_renderer_get_type ())
#define MG_RENDERER_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MG_RENDERER_TYPE, MgRendererIface))


/* struct for the interface */
struct _MgRendererIface
{
	GTypeInterface           g_iface;

	/* virtual table */
	GdaXqlItem *(* render_as_xql)   (MgRenderer *iface, MgContext *context, GError **error);
	gchar      *(* render_as_sql)   (MgRenderer *iface, MgContext *context, GError **error); 
	gchar      *(* render_as_str)   (MgRenderer *iface, MgContext *context);
};

GType           mg_renderer_get_type        (void) G_GNUC_CONST;

GdaXqlItem     *mg_renderer_render_as_xql   (MgRenderer *iface, MgContext *context, GError **error);
gchar          *mg_renderer_render_as_sql   (MgRenderer *iface, MgContext *context, GError **error);
gchar          *mg_renderer_render_as_str   (MgRenderer *iface, MgContext *context);

G_END_DECLS

#endif
