/* mg-graphviz.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_GRAPHVIZ_H_
#define __MG_GRAPHVIZ_H_

#include "mg-base.h"
#include "mg-defs.h"

G_BEGIN_DECLS

typedef struct _MgGraphviz MgGraphviz;
typedef struct _MgGraphvizClass MgGraphvizClass;
typedef struct _MgGraphvizPrivate MgGraphvizPrivate;

#define MG_GRAPHVIZ_TYPE          (mg_graphviz_get_type())
#define MG_GRAPHVIZ(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_graphviz_get_type(), MgGraphviz)
#define MG_GRAPHVIZ_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_graphviz_get_type (), MgGraphvizClass)
#define IS_MG_GRAPHVIZ(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_graphviz_get_type ())

/* error reporting */
extern GQuark mg_graphviz_error_quark (void);
#define MG_GRAPHVIZ_ERROR mg_graphviz_error_quark ()

enum
{
        MG_GRAPHVIZ_SAVE_ERROR
};


/* struct for the object's data */
struct _MgGraphviz
{
	MgBase                   object;
	MgGraphvizPrivate       *priv;
};

/* struct for the object's class */
struct _MgGraphvizClass
{
	MgBaseClass              class;
};

guint           mg_graphviz_get_type         (void);
GObject        *mg_graphviz_new              (MgConf *conf);

void            mg_graphviz_add_to_graph     (MgGraphviz *graph, GObject *obj);
gboolean        mg_graphviz_save_file        (MgGraphviz *graph, const gchar *filename, GError **error);
G_END_DECLS

#endif
