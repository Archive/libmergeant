/* mg-server.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_SERVER_H_
#define __MG_SERVER_H_

#include <glib-object.h>
#include <libgda/libgda.h>
#include "mg-defs.h"
#include "mg-conf.h"

G_BEGIN_DECLS

#define MG_SERVER_TYPE          (mg_server_get_type())
#define MG_SERVER(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_server_get_type(), MgServer)
#define MG_SERVER_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_server_get_type (), MgServerClass)
#define IS_MG_SERVER(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_server_get_type ())


/* error reporting */
extern GQuark mg_server_error_quark (void);
#define MG_SERVER_ERROR mg_server_error_quark ()

enum
{
	MG_SERVER_CONN_OPEN_ERROR,
	MG_SERVER_DO_QUERY_ERROR,
	MG_SERVER_XML_SAVE_ERROR,
	MG_SERVER_XML_LOAD_ERROR,
	MG_SERVER_META_DATA_UPDATE,
	MG_SERVER_META_DATA_UPDATE_USER_STOPPED,
	MG_SERVER_DATATYPE_ERROR,
	MG_SERVER_FUNCTIONS_ERROR,
	MG_SERVER_AGGREGATES_ERROR
};


typedef enum {
	MG_SERVER_QUERY_SQL,
	MG_SERVER_QUERY_XML
} MgServerQueryType;

typedef enum {
	MG_SERVER_SELECT_OP,
	MG_SERVER_INSERT_OP,
	MG_SERVER_UPDATE_OP,
	MG_SERVER_DELETE_OP,
	MG_SERVER_UNKNOWN_OP
} MgServerOpMode;


/* struct for the object's data */
struct _MgServer
{
	GdaClient              object;
	MgServerPrivate       *priv;
};

/* struct for the object's class */
struct _MgServerClass
{
	GdaClientClass class;

	/* signals */
	void   (*conn_opened)               (MgServer *obj);
	void   (*conn_to_close)             (MgServer *obj);
	void   (*conn_closed)               (MgServer *obj);

	void   (*data_type_added)           (MgServer *obj, MgServerDataType *type);
	void   (*data_type_removed)         (MgServer *obj, MgServerDataType *type);
	void   (*data_type_updated)         (MgServer *obj, MgServerDataType *type);

	void   (*data_function_added)       (MgServer *obj, MgServerFunction *function);
	void   (*data_function_removed)     (MgServer *obj, MgServerFunction *function);
	void   (*data_function_updated)     (MgServer *obj, MgServerFunction *function);

	void   (*data_aggregate_added)      (MgServer *obj, MgServerAggregate *aggregate);
	void   (*data_aggregate_removed)    (MgServer *obj, MgServerAggregate *aggregate);
	void   (*data_aggregate_updated)    (MgServer *obj, MgServerAggregate *aggregate);

	void   (*data_update_started)       (MgServer *obj);
	void   (*update_progress)           (MgServer *obj, gchar * msg, guint now, guint total);
	void   (*data_update_finished)      (MgServer *obj);

	void   (*object_handler_updated)    (MgServer *obj);
};

guint              mg_server_get_type                  (void);
GObject           *mg_server_new                       (MgConf *conf);

gboolean           mg_server_set_datasource            (MgServer *srv, const gchar *datasource);
gchar             *mg_server_get_datasource            (MgServer *srv);
gboolean           mg_server_set_user_name             (MgServer *srv, const gchar *username);
gchar             *mg_server_get_user_name             (MgServer *srv);
gboolean           mg_server_set_user_password         (MgServer *srv, const gchar *password);
void               mg_server_reset                     (MgServer *srv);

gboolean           mg_server_open_connect              (MgServer *srv, GError **error);
gboolean           mg_server_conn_is_opened            (MgServer *srv);
void               mg_server_close_connect             (MgServer *srv);
void               mg_server_close_connect_no_warn     (MgServer *srv);

MgConf            *mg_server_get_conf                  (MgServer *srv);
MgServerOpMode     mg_server_get_sql_op_mode           (MgServer *srv, const gchar *query);
MgResultSet       *mg_server_do_query                  (MgServer *srv, const gchar *query,
							MgServerQueryType type, GError **error);
gboolean           mg_server_update_dbms_data          (MgServer *srv, GError **error);
void               mg_server_stop_update_dbms_data     (MgServer *srv);

MgDataHandler     *mg_server_get_handler_by_name       (MgServer *srv, const gchar *name);
MgDataHandler     *mg_server_get_handler_by_type       (MgServer *srv, MgServerDataType *type);
MgDataHandler     *mg_server_get_handler_by_gda        (MgServer *srv, GdaValueType gda_type);
GSList            *mg_server_get_plugin_handlers       (MgServer *srv);

MgUser            *mg_server_get_user_by_name          (MgServer *srv, const gchar *username);
GSList            *mg_server_get_data_types            (MgServer *srv);
MgServerDataType  *mg_server_get_data_type_by_name     (MgServer *srv, const gchar *typename);
MgServerDataType  *mg_server_get_data_type_by_xml_id   (MgServer *srv, const gchar *xml_id);

GSList            *mg_server_get_functions             (MgServer *srv);
GSList            *mg_server_get_functions_by_name     (MgServer *srv, const gchar *funcname);
MgServerFunction  *mg_server_get_function_by_name_arg  (MgServer *srv, const gchar *funcname, 
							const GSList *argtypes);
MgServerFunction  *mg_server_get_function_by_xml_id    (MgServer *srv, const gchar *xml_id);
MgServerFunction  *mg_server_get_function_by_dbms_id   (MgServer *srv, const gchar *dbms_id);

GSList            *mg_server_get_aggregates            (MgServer *srv);
GSList            *mg_server_get_aggregates_by_name    (MgServer *srv, const gchar *aggname);
MgServerAggregate *mg_server_get_aggregate_by_name_arg (MgServer *srv, const gchar *aggname, 
							MgServerDataType *argtype);
MgServerAggregate *mg_server_get_aggregate_by_xml_id   (MgServer *srv, const gchar *xml_id);
MgServerAggregate *mg_server_get_aggregate_by_dbms_id  (MgServer *srv, const gchar *dbms_id);
 
MgDataHandler     *mg_server_get_object_handler        (MgServer *srv, GObject *object);
void               mg_server_set_object_handler        (MgServer *srv, GObject *object, 
							MgDataHandler *handler);
void               mg_server_unset_object_handler      (MgServer *srv, GObject *object);
gboolean           mg_server_object_has_handler        (MgServer *srv, GObject *object);
void               mg_server_set_object_func_handler   (MgServer *srv, 
							MgDataHandler *(*func)(MgServer *, GObject *));

/* Convenience function */
gchar             *mg_server_stringify_value           (MgServer *srv, const GdaValue * value);
gchar             *mg_server_escape_chars              (MgServer *srv, const gchar *string);
GdaDataModel      *mg_server_get_gda_schema            (MgServer *srv, GdaConnectionSchema schema,
							GdaParameterList *params);

#ifdef debug
void               mg_server_dump                      (MgServer *srv, gint offset);
#endif

G_END_DECLS

#endif
