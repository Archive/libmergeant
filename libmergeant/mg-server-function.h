/* mg-server-function.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_SERVER_FUNCTION_H_
#define __MG_SERVER_FUNCTION_H_

#include "mg-base.h"
#include "mg-defs.h"

G_BEGIN_DECLS

#define MG_SERVER_FUNCTION_TYPE          (mg_server_function_get_type())
#define MG_SERVER_FUNCTION(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_server_function_get_type(), MgServerFunction)
#define MG_SERVER_FUNCTION_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_server_function_get_type (), MgServerFunctionClass)
#define IS_MG_SERVER_FUNCTION(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_server_function_get_type ())


/* error reporting */
extern GQuark mg_server_function_error_quark (void);
#define MG_SERVER_FUNCTION_ERROR mg_server_function_error_quark ()

enum
{
	MG_SERVER_FUNCTION_XML_LOAD_ERROR
};


/* struct for the object's data */
struct _MgServerFunction
{
	MgBase                         object;
	MgServerFunctionPrivate       *priv;
};

/* struct for the object's class */
struct _MgServerFunctionClass
{
	MgBaseClass                    class;

	/* signals */
	void   (*templ_signal)        (MgServerFunction *obj);
};

guint             mg_server_function_get_type      (void);
GObject          *mg_server_function_new           (MgServer *srv);
void              mg_server_function_set_dbms_id   (MgServerFunction *func, const gchar *id);
const gchar      *mg_server_function_get_dbms_id   (MgServerFunction *func);
void              mg_server_function_set_sqlname   (MgServerFunction *func, const gchar *sqlname);
const gchar      *mg_server_function_get_sqlname   (MgServerFunction *func);
void              mg_server_function_set_arg_types (MgServerFunction *func, const GSList *arg_types);
const GSList     *mg_server_function_get_arg_types (MgServerFunction *func);
void              mg_server_function_set_ret_type  (MgServerFunction *func, MgServerDataType *dt);
MgServerDataType *mg_server_function_get_ret_type  (MgServerFunction *func);

G_END_DECLS

#endif
