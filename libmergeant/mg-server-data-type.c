/* mg-server-data-type.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-server.h"
#include "mg-server-data-type.h"
#include "mg-xml-storage.h"
#include "mg-data-handler.h"
#include "marshal.h"
#include <libgda/libgda.h>
#include <string.h>

/* 
 * Main static functions 
 */
static void mg_server_data_type_class_init (MgServerDataTypeClass * class);
static void mg_server_data_type_init (MgServerDataType * srv);
static void mg_server_data_type_dispose (GObject   * object);
static void mg_server_data_type_finalize (GObject   * object);

static void mg_server_data_type_set_property (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec);
static void mg_server_data_type_get_property (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec);

static void        mg_data_type_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_data_type_get_xml_id (MgXmlStorage *iface);
static xmlNodePtr  mg_data_type_save_to_xml (MgXmlStorage *iface, GError **error);
static gboolean    mg_data_type_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error);


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	TEMPL_SIGNAL,
	LAST_SIGNAL
};

static gint mg_server_data_type_signals[LAST_SIGNAL] = { 0 };

/* properties */
enum
{
	PROP_0,
	PROP
};


/* private structure */
struct _MgServerDataTypePrivate
{
	MgServer          *srv;
	guint              numparams;
	GdaValueType       gda_type;
};


/* module error */
GQuark mg_server_data_type_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_server_data_type_error");
	return quark;
}


guint
mg_server_data_type_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgServerDataTypeClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_server_data_type_class_init,
			NULL,
			NULL,
			sizeof (MgServerDataType),
			0,
			(GInstanceInitFunc) mg_server_data_type_init
		};

		static const GInterfaceInfo xml_storage_info = {
			(GInterfaceInitFunc) mg_data_type_xml_storage_init,
			NULL,
			NULL
		};
		
		type = g_type_register_static (MG_BASE_TYPE, "MgServerDataType", &info, 0);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
	}
	return type;
}

static void 
mg_data_type_xml_storage_init (MgXmlStorageIface *iface)
{
	iface->get_xml_id = mg_data_type_get_xml_id;
	iface->save_to_xml = mg_data_type_save_to_xml;
	iface->load_from_xml = mg_data_type_load_from_xml;
}


static void
mg_server_data_type_class_init (MgServerDataTypeClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_server_data_type_signals[TEMPL_SIGNAL] =
		g_signal_new ("templ_signal",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgServerDataTypeClass, templ_signal),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	class->templ_signal = NULL;

	object_class->dispose = mg_server_data_type_dispose;
	object_class->finalize = mg_server_data_type_finalize;

	/* Properties */
	object_class->set_property = mg_server_data_type_set_property;
	object_class->get_property = mg_server_data_type_get_property;
	g_object_class_install_property (object_class, PROP,
					 g_param_spec_pointer ("prop", NULL, NULL, (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static void
mg_server_data_type_init (MgServerDataType * mg_server_data_type)
{
	mg_server_data_type->priv = g_new0 (MgServerDataTypePrivate, 1);
	mg_server_data_type->priv->srv = NULL;
	mg_server_data_type->priv->numparams = -1;
	mg_server_data_type->priv->gda_type = 0;
}

/**
 * mg_server_data_type_new
 * @srv: a #MgServer object
 *
 * Creates a new MgServerDataType object
 *
 * Returns: the new object
 */
GObject*
mg_server_data_type_new (MgServer *srv)
{
	GObject   *obj;
	MgServerDataType *mg_server_data_type;

	g_return_val_if_fail (srv && MG_SERVER (srv), NULL);

	obj = g_object_new (MG_SERVER_DATA_TYPE_TYPE, "conf",
			    mg_server_get_conf (srv), NULL);
	mg_server_data_type = MG_SERVER_DATA_TYPE (obj);
	mg_base_set_id (MG_BASE (mg_server_data_type), 0);
	g_object_add_weak_pointer (G_OBJECT (srv), (gpointer) &(mg_server_data_type->priv->srv));
	mg_server_data_type->priv->srv = srv;
	
	return obj;
}


static void
mg_server_data_type_dispose (GObject *object)
{
	MgServerDataType *mg_server_data_type;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_SERVER_DATA_TYPE (object));

	mg_server_data_type = MG_SERVER_DATA_TYPE (object);
	if (mg_server_data_type->priv) {
		mg_base_nullify_check (MG_BASE (object));

		g_object_remove_weak_pointer (G_OBJECT (mg_server_data_type->priv->srv), 
					      (gpointer) &(mg_server_data_type->priv->srv));
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_server_data_type_finalize (GObject   * object)
{
	MgServerDataType *mg_server_data_type;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_SERVER_DATA_TYPE (object));

	mg_server_data_type = MG_SERVER_DATA_TYPE (object);
	if (mg_server_data_type->priv) {

		g_free (mg_server_data_type->priv);
		mg_server_data_type->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_server_data_type_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	gpointer ptr;
	MgServerDataType *mg_server_data_type;

	mg_server_data_type = MG_SERVER_DATA_TYPE (object);
	if (mg_server_data_type->priv) {
		switch (param_id) {
		case PROP:
			/* FIXME */
			ptr = g_value_get_pointer (value);
			break;
		}
	}
}

static void
mg_server_data_type_get_property (GObject              *object,
			guint                 param_id,
			GValue               *value,
			GParamSpec           *pspec)
{
	MgServerDataType *mg_server_data_type;
	mg_server_data_type = MG_SERVER_DATA_TYPE (object);
	
	if (mg_server_data_type->priv) {
		switch (param_id) {
		case PROP:
			/* FIXME */
			g_value_set_pointer (value, NULL);
			break;
		}	
	}
}


/* MgXmlStorage interface implementation */
static gchar *
mg_data_type_get_xml_id (MgXmlStorage *iface)
{
	g_return_val_if_fail (iface && IS_MG_SERVER_DATA_TYPE (iface), NULL);
	g_return_val_if_fail (MG_SERVER_DATA_TYPE (iface)->priv, NULL);

	return g_strdup_printf ("DT%s", mg_base_get_name (MG_BASE (iface)));
}

static xmlNodePtr
mg_data_type_save_to_xml (MgXmlStorage *iface, GError **error)
{
	xmlNodePtr node = NULL;
	MgServerDataType *dt;
	gchar *str;

	g_return_val_if_fail (iface && IS_MG_SERVER_DATA_TYPE (iface), NULL);
	g_return_val_if_fail (MG_SERVER_DATA_TYPE (iface)->priv, NULL);
	dt = MG_SERVER_DATA_TYPE (iface);

	node = xmlNewNode (NULL, "MG_DATATYPE");
	
	str = mg_data_type_get_xml_id (iface);
	xmlSetProp (node, "id", str);
	g_free (str);
	xmlSetProp (node, "name", mg_base_get_name (MG_BASE (dt)));
	xmlSetProp (node, "owner", mg_base_get_owner (MG_BASE (dt)));
	xmlSetProp (node, "descr", mg_base_get_description (MG_BASE (dt)));
	str = g_strdup_printf ("%d", dt->priv->numparams);
	xmlSetProp (node, "nparam", str);
	g_free (str);
	xmlSetProp (node, "gdatype", gda_type_to_string (dt->priv->gda_type));

	return node;
}

static gboolean
mg_data_type_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgServerDataType *dt;
	gchar *prop;
	gboolean pname = FALSE, pnparam = FALSE, pgdatype = FALSE;

	g_return_val_if_fail (iface && IS_MG_SERVER_DATA_TYPE (iface), FALSE);
	g_return_val_if_fail (MG_SERVER_DATA_TYPE (iface)->priv, FALSE);
	g_return_val_if_fail (node, FALSE);

	dt = MG_SERVER_DATA_TYPE (iface);
	if (strcmp (node->name, "MG_DATATYPE")) {
		g_set_error (error,
			     MG_SERVER_DATA_TYPE_ERROR,
			     MG_SERVER_DATA_TYPE_XML_LOAD_ERROR,
			     _("XML Tag is not <MG_DATATYPE>"));
		return FALSE;
	}

	prop = xmlGetProp (node, "name");
	if (prop) {
		pname = TRUE;
		mg_base_set_name (MG_BASE (dt), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "descr");
	if (prop) {
		mg_base_set_description (MG_BASE (dt), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "owner");
	if (prop) {
		mg_base_set_owner (MG_BASE (dt), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "nparam");
	if (prop) {
		pnparam = TRUE;
		dt->priv->numparams = atoi (prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "gdatype");
	if (prop) {
		pgdatype = TRUE;
		dt->priv->gda_type = gda_type_from_string (prop);
		g_free (prop);
	}

	if (pname && pnparam && pgdatype)
		return TRUE;
	else {
		g_set_error (error,
			     MG_SERVER_DATA_TYPE_ERROR,
			     MG_SERVER_DATA_TYPE_XML_LOAD_ERROR,
			     _("Missing required attributes for <MG_DATATYPE>"));
		return FALSE;
	}
}







/**
 * mg_server_data_type_set_sqlname
 * @dt: a #MgServerDataType object
 * @sqlname: 
 *
 * Set the SQL name of the data type.
 */
void
mg_server_data_type_set_sqlname (MgServerDataType *dt, const gchar *sqlname)
{
	g_return_if_fail (dt && IS_MG_SERVER_DATA_TYPE (dt));
	g_return_if_fail (dt->priv);

	mg_base_set_name (MG_BASE (dt), sqlname);
}


/**
 * mg_server_data_type_get_sqlname
 * @dt: a #MgServerDataType object
 *
 * Get the DBMS's name of a data type.
 *
 * Returns: the name of the data type
 */
const gchar *
mg_server_data_type_get_sqlname (MgServerDataType *dt)
{
	g_return_val_if_fail (dt && IS_MG_SERVER_DATA_TYPE (dt), NULL);
	g_return_val_if_fail (dt->priv, NULL);

	return mg_base_get_name (MG_BASE (dt));
}

/**
 * mg_server_data_type_set_gda_type
 * @dt: a #MgServerDataType object
 * @gda_type: 
 *
 * Set the gda type for a data type
 */
void
mg_server_data_type_set_gda_type (MgServerDataType *dt, GdaValueType gda_type)
{
	g_return_if_fail (dt && IS_MG_SERVER_DATA_TYPE (dt));
	g_return_if_fail (dt->priv);

	dt->priv->gda_type = gda_type;
}

/**
 * mg_server_data_type_get_gda_type
 * @dt: a #MgServerDataType object
 *
 * Get the gda type of a data type
 *
 * Returns: the gda type
 */
GdaValueType
mg_server_data_type_get_gda_type (MgServerDataType *dt)
{
	g_return_val_if_fail (dt && IS_MG_SERVER_DATA_TYPE (dt), GDA_VALUE_TYPE_UNKNOWN);
	g_return_val_if_fail (dt->priv, GDA_VALUE_TYPE_UNKNOWN);

	return dt->priv->gda_type;
}

/**
 * mg_server_data_type_set_handler
 * @dt: a #MgServerDataType object
 * @dh: an object which implements the #MgDataHandler interface
 *
 * Forces the MgDataHandler associated with the data type.
 */
void
mg_server_data_type_set_handler (MgServerDataType *dt, MgDataHandler *dh)
{
	g_return_if_fail (dt && IS_MG_SERVER_DATA_TYPE (dt));
	g_return_if_fail (dh && IS_MG_DATA_HANDLER (dh));
	g_return_if_fail (dt->priv);

	mg_server_set_object_handler (dt->priv->srv, G_OBJECT (dt), dh);
}


/**
 * mg_server_data_type_get_handler
 * @dt: a #MgServerDataType object
 *
 * Get the MgDataHandler associated with the data type.
 *
 * Returns: the MgDataHandler
 */
MgDataHandler *
mg_server_data_type_get_handler (MgServerDataType *dt)
{
	g_return_val_if_fail (dt && IS_MG_SERVER_DATA_TYPE (dt), NULL);
	g_return_val_if_fail (dt->priv, NULL);

	return mg_server_get_object_handler (dt->priv->srv, G_OBJECT (dt));
}
