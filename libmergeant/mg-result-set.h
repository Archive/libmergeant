/* mg-result-set.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_RESULTSET_H_
#define __MG_RESULTSET_H_

#include <glib-object.h>
#include "mg-defs.h"
#include "mg-base.h"
#include <libgda/libgda.h>

G_BEGIN_DECLS

#define MG_RESULTSET_TYPE          (mg_resultset_get_type())
#define MG_RESULTSET(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_resultset_get_type(), MgResultSet)
#define MG_RESULTSET_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_resultset_get_type (), MgResultSetClass)
#define IS_MG_RESULTSET(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_resultset_get_type ())



/* error reporting */
extern GQuark mg_resultset_error_quark (void);
#define MG_RESULTSET_ERROR mg_resultset_error_quark ()

enum
{
	ERROR1
};


/* struct for the object's data */
struct _MgResultSet
{
	MgBase              object;
	MgResultSetPrivate *priv;
};

/* struct for the object's class */
struct _MgResultSetClass
{
	MgBaseClass             class;

	/* signals */
	void   (*dummy)        (MgResultSet *obj);
};

guint           mg_resultset_get_type         (void);
GObject        *mg_resultset_new              (MgServer *srv, GdaCommand *cmd, GdaDataModel * model);

gint            mg_resultset_get_nbtuples     (MgResultSet * rs);
gint            mg_resultset_get_nbcols       (MgResultSet * rs);
gchar          *mg_resultset_get_item         (MgResultSet * rs, gint row, gint col);
const GdaValue *mg_resultset_get_gdavalue     (MgResultSet * rs, gint row, gint col);
const gchar    *mg_resultset_get_col_name     (MgResultSet * rs, gint col);
GdaDataModel   *mg_resultset_get_data_model   (MgResultSet * rs);

gboolean        mg_resultset_check_model      (MgResultSet * rs, gint nbcols, ...);
gboolean        mg_resultset_check_data_model (GdaDataModel *model, gint nbcols, ...);


G_END_DECLS

#endif
