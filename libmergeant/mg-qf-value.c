/* mg-qf-value.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-qf-value.h"
#include "mg-xml-storage.h"
#include "mg-field.h"
#include "mg-renderer.h"
#include "mg-referer.h"
#include "marshal.h"
#include "mg-entity.h"
#include "mg-query.h"
#include <string.h>
#include "mg-server.h"
#include "mg-data-handler.h"
#include "mg-parameter.h"
#include "mg-ref-base.h"
#include "mg-context.h"

/* 
 * Main static functions 
 */
static void mg_qf_value_class_init (MgQfValueClass * class);
static void mg_qf_value_init (MgQfValue *qf);
static void mg_qf_value_dispose (GObject *object);
static void mg_qf_value_finalize (GObject *object);

static void mg_qf_value_set_property (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec);
static void mg_qf_value_get_property (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec);

/* XML storage interface */
static void        mg_qf_value_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_qf_value_get_xml_id (MgXmlStorage *iface);
static xmlNodePtr  mg_qf_value_save_to_xml (MgXmlStorage *iface, GError **error);
static gboolean    mg_qf_value_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error);

/* Field interface */
static void              mg_qf_value_field_init      (MgFieldIface *iface);
static MgEntity         *mg_qf_value_get_entity      (MgField *iface);
static MgServerDataType *mg_qf_value_get_data_type   (MgField *iface);
static const gchar      *mg_qf_value_get_name        (MgField *iface);
static const gchar      *mg_qf_value_get_description (MgField *iface);

/* Renderer interface */
static void            mg_qf_value_renderer_init      (MgRendererIface *iface);
static GdaXqlItem     *mg_qf_value_render_as_xql   (MgRenderer *iface, MgContext *context, GError **error);
static gchar          *mg_qf_value_render_as_sql   (MgRenderer *iface, MgContext *context, GError **error);
static gchar          *mg_qf_value_render_as_str   (MgRenderer *iface, MgContext *context);

/* Referer interface */
static void        mg_qf_value_referer_init        (MgRefererIface *iface);
static gboolean    mg_qf_value_activate            (MgReferer *iface);
static void        mg_qf_value_deactivate          (MgReferer *iface);
static gboolean    mg_qf_value_is_active           (MgReferer *iface);
static GSList     *mg_qf_value_get_ref_objects     (MgReferer *iface);
static void        mg_qf_value_replace_refs        (MgReferer *iface, GHashTable *replacements);

/* virtual functions */
static GObject    *mg_qf_value_copy           (MgQfield *orig);
static gboolean    mg_qf_value_is_equal       (MgQfield *qfield1, MgQfield *qfield2);


/* When the MgQuery or MgTarget is nullified */
static void nullified_object_cb (GObject *obj, MgQfValue *field);

static GSList   *mg_qf_value_get_params (MgQfield *qfield);
static gboolean  mg_qf_value_set_force_param (MgQfValue *field, MgQfield *provider, GError **error);
static gboolean  mg_qf_value_set_force_param_xml (MgQfValue *field, const gchar *prov_xml_id, GError **error);
static MgQfield *mg_qf_value_get_force_param (MgQfValue *field);


static gboolean mg_qf_value_render_find_value (MgQfValue *field, MgContext *context,
					       const GdaValue **value_found, MgParameter **param_source);

#ifdef debug
static void mg_qf_value_dump (MgQfValue *field, guint offset);
#endif

/* get a pointer to the parents to be able to cvalue their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	TEMPL_SIGNAL,
	LAST_SIGNAL
};

static gint mg_qf_value_signals[LAST_SIGNAL] = { 0 };

/* properties */
enum
{
	PROP_0,
	PROP_QUERY,
	PROP_VALUE_PROVIDER_OBJECT,
        PROP_VALUE_PROVIDER_XML_ID,
	PROP_HANDLER_PLUGIN
};


/* private structure */
struct _MgQfValuePrivate
{
	MgQuery          *query;
	GdaValueType      gda_type;
	MgServerDataType *srv_type;
	GdaValue         *value;        /* MUST either be NULL, or of type GDA_VALUE_NULL or 'type' */
	GdaValue         *default_value;/* CAN either be NULL, or of any type */
	gboolean          is_parameter;
	gboolean          is_null_allowed;
	MgRefBase        *value_prov_ref; /* to a MgQfield of another query */

	gchar            *plugin;       /* specific plugin to be used */
};


/* module error */
GQuark mg_qf_value_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_qf_value_error");
	return quark;
}


guint
mg_qf_value_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgQfValueClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_qf_value_class_init,
			NULL,
			NULL,
			sizeof (MgQfValue),
			0,
			(GInstanceInitFunc) mg_qf_value_init
		};

		static const GInterfaceInfo xml_storage_info = {
			(GInterfaceInitFunc) mg_qf_value_xml_storage_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo field_info = {
			(GInterfaceInitFunc) mg_qf_value_field_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo renderer_info = {
			(GInterfaceInitFunc) mg_qf_value_renderer_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo referer_info = {
			(GInterfaceInitFunc) mg_qf_value_referer_init,
			NULL,
			NULL
		};
		
		
		type = g_type_register_static (MG_QFIELD_TYPE, "MgQfValue", &info, 0);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
		g_type_add_interface_static (type, MG_FIELD_TYPE, &field_info);
		g_type_add_interface_static (type, MG_RENDERER_TYPE, &renderer_info);
		g_type_add_interface_static (type, MG_REFERER_TYPE, &referer_info);
	}
	return type;
}

static void 
mg_qf_value_xml_storage_init (MgXmlStorageIface *iface)
{
	iface->get_xml_id = mg_qf_value_get_xml_id;
	iface->save_to_xml = mg_qf_value_save_to_xml;
	iface->load_from_xml = mg_qf_value_load_from_xml;
}

static void
mg_qf_value_field_init (MgFieldIface *iface)
{
	iface->get_entity = mg_qf_value_get_entity;
	iface->get_data_type = mg_qf_value_get_data_type;
	iface->get_name = mg_qf_value_get_name;
	iface->get_description = mg_qf_value_get_description;
}

static void
mg_qf_value_renderer_init (MgRendererIface *iface)
{
	iface->render_as_xql = mg_qf_value_render_as_xql;
	iface->render_as_sql = mg_qf_value_render_as_sql;
	iface->render_as_str = mg_qf_value_render_as_str;
}

static void
mg_qf_value_referer_init (MgRefererIface *iface)
{
        iface->activate = mg_qf_value_activate;
        iface->deactivate = mg_qf_value_deactivate;
        iface->is_active = mg_qf_value_is_active;
        iface->get_ref_objects = mg_qf_value_get_ref_objects;
        iface->replace_refs = mg_qf_value_replace_refs;
}

static void
mg_qf_value_class_init (MgQfValueClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_qf_value_signals[TEMPL_SIGNAL] =
		g_signal_new ("templ_signal",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgQfValueClass, templ_signal),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	class->templ_signal = NULL;

	object_class->dispose = mg_qf_value_dispose;
	object_class->finalize = mg_qf_value_finalize;

	/* Properties */
	object_class->set_property = mg_qf_value_set_property;
	object_class->get_property = mg_qf_value_get_property;
	g_object_class_install_property (object_class, PROP_QUERY,
					 g_param_spec_pointer ("query", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property (object_class, PROP_VALUE_PROVIDER_OBJECT,
                                         g_param_spec_pointer ("value_provider", NULL, NULL,
                                                               (G_PARAM_READABLE | G_PARAM_WRITABLE)));
        g_object_class_install_property (object_class, PROP_VALUE_PROVIDER_XML_ID,
                                         g_param_spec_string ("value_provider_xml_id", NULL, NULL, NULL,
                                                              (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property (object_class, PROP_HANDLER_PLUGIN,
                                         g_param_spec_string ("handler_plugin", NULL, NULL, NULL,
                                                              (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_qf_value_dump;
#endif
	MG_QFIELD_CLASS (class)->copy = mg_qf_value_copy;
	MG_QFIELD_CLASS (class)->is_equal = mg_qf_value_is_equal;
	MG_QFIELD_CLASS (class)->is_list = NULL;
	MG_QFIELD_CLASS (class)->get_params = mg_qf_value_get_params;
}

static void
mg_qf_value_init (MgQfValue *mg_qf_value)
{
	mg_qf_value->priv = g_new0 (MgQfValuePrivate, 1);
	mg_qf_value->priv->query = NULL;
	mg_qf_value->priv->gda_type = GDA_VALUE_TYPE_UNKNOWN;
	mg_qf_value->priv->srv_type = NULL;
	mg_qf_value->priv->value = NULL;
	mg_qf_value->priv->default_value = NULL;
	mg_qf_value->priv->is_parameter = FALSE;
	mg_qf_value->priv->is_null_allowed = FALSE;
	mg_qf_value->priv->value_prov_ref = NULL;
	mg_qf_value->priv->plugin = NULL;
}


/**
 * mg_qf_value_new
 * @query: a #MgQuery in which the new object will be
 * @type: the requested type for the value
 *
 * Creates a new MgQfValue object which represents a value or a parameter.
 *
 * Returns: the new object
 */
GObject*
mg_qf_value_new (MgQuery *query, MgServerDataType *type)
{
	GObject   *obj;
	MgQfValue *mg_qf_value;
	MgConf *conf;
	guint id;

	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (type && IS_MG_SERVER_DATA_TYPE (type), NULL);
	conf = mg_base_get_conf (MG_BASE (query));

	obj = g_object_new (MG_QF_VALUE_TYPE, "conf", conf, NULL);
	mg_qf_value = MG_QF_VALUE (obj);
	g_object_get (G_OBJECT (query), "field_serial", &id, NULL);
	mg_base_set_id (MG_BASE (mg_qf_value), id);

	mg_qf_value->priv->query = query;
	g_signal_connect (G_OBJECT (query), "nullified",
			  G_CALLBACK (nullified_object_cb), mg_qf_value);

	mg_qf_value->priv->srv_type = type;
	g_signal_connect (G_OBJECT (type), "nullified",
			  G_CALLBACK (nullified_object_cb), mg_qf_value);

	mg_qf_value->priv->gda_type = mg_server_data_type_get_gda_type (type);
	
	return obj;
}


static void 
nullified_object_cb (GObject *obj, MgQfValue *field)
{
	mg_base_nullify (MG_BASE (field));
}

static void
mg_qf_value_dispose (GObject *object)
{
	MgQfValue *mg_qf_value;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_QF_VALUE (object));

	mg_qf_value = MG_QF_VALUE (object);
	if (mg_qf_value->priv) {
		mg_base_nullify_check (MG_BASE (object));
		if (mg_qf_value->priv->query) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (mg_qf_value->priv->query),
							      G_CALLBACK (nullified_object_cb), mg_qf_value);
			mg_qf_value->priv->query = NULL;
		}
		if (mg_qf_value->priv->srv_type) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (mg_qf_value->priv->srv_type),
							      G_CALLBACK (nullified_object_cb), mg_qf_value);
			mg_qf_value->priv->srv_type = NULL;
		}
		if (mg_qf_value->priv->value) {
			gda_value_free (mg_qf_value->priv->value);
			mg_qf_value->priv->value = NULL;
		}
		if (mg_qf_value->priv->default_value) {
			gda_value_free (mg_qf_value->priv->default_value);
			mg_qf_value->priv->default_value = NULL;
		}
		if (mg_qf_value->priv->value_prov_ref)
			mg_qf_value_set_force_param (mg_qf_value, NULL, NULL);
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_qf_value_finalize (GObject   * object)
{
	MgQfValue *mg_qf_value;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_QF_VALUE (object));

	mg_qf_value = MG_QF_VALUE (object);
	if (mg_qf_value->priv) {
		if (mg_qf_value->priv->plugin)
			g_free (mg_qf_value->priv->plugin);

		g_free (mg_qf_value->priv);
		mg_qf_value->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_qf_value_set_property (GObject              *object,
			  guint                 param_id,
			  const GValue         *value,
			  GParamSpec           *pspec)
{
	MgQfValue *mg_qf_value;
	gpointer ptr;
	const gchar *str;

	mg_qf_value = MG_QF_VALUE (object);
	if (mg_qf_value->priv) {
		switch (param_id) {
		case PROP_QUERY:
			ptr = g_value_get_pointer (value);
			g_return_if_fail (ptr && IS_MG_QUERY (ptr));

			if (mg_qf_value->priv->query) {
				if (mg_qf_value->priv->query == MG_QUERY (ptr))
					return;

				g_signal_handlers_disconnect_by_func (G_OBJECT (mg_qf_value->priv->query),
								      G_CALLBACK (nullified_object_cb), mg_qf_value);
			}

			mg_qf_value->priv->query = MG_QUERY (ptr);
			g_signal_connect (G_OBJECT (ptr), "nullified",
					  G_CALLBACK (nullified_object_cb), mg_qf_value);
			break;
		case PROP_VALUE_PROVIDER_OBJECT:
			ptr = g_value_get_pointer (value);
			g_return_if_fail (mg_qf_value_set_force_param (mg_qf_value, (MgQfield *) ptr, NULL));
			break;
		case PROP_VALUE_PROVIDER_XML_ID:
			str =  g_value_get_string (value);
			g_return_if_fail (mg_qf_value_set_force_param_xml (mg_qf_value, str, NULL));
			break;
		case PROP_HANDLER_PLUGIN:
			str =  g_value_get_string (value);
			if (mg_qf_value->priv->plugin) {
				g_free (mg_qf_value->priv->plugin);
				mg_qf_value->priv->plugin = NULL;
			}
			if (str)
				mg_qf_value->priv->plugin = g_strdup (str);
			break;
		}
	}
}

static void
mg_qf_value_get_property (GObject              *object,
			guint                 param_id,
			GValue               *value,
			GParamSpec           *pspec)
{
	MgQfValue *mg_qf_value = MG_QF_VALUE (object);
	
	if (mg_qf_value->priv) {
		switch (param_id) {
		case PROP_QUERY:
			g_value_set_pointer (value, mg_qf_value->priv->query);
			break;
		case PROP_VALUE_PROVIDER_OBJECT:
			if (mg_qf_value->priv->value_prov_ref)
				g_value_set_pointer (value, mg_ref_base_get_ref_object (mg_qf_value->priv->value_prov_ref));
			else
				g_value_set_pointer (value, NULL);
			break;
		case PROP_VALUE_PROVIDER_XML_ID:
			if (mg_qf_value->priv->value_prov_ref)
                                g_value_set_string (value,
                                                    mg_ref_base_get_ref_name (mg_qf_value->priv->value_prov_ref,
                                                                              NULL, NULL));
                        else
                                g_value_set_string (value, NULL);
			break;
		case PROP_HANDLER_PLUGIN:
			g_value_set_string (value, mg_qf_value->priv->plugin);
			break;
		}	
	}
}

static GObject *
mg_qf_value_copy (MgQfield *orig)
{
	MgQfValue *qf, *nqf;
	GObject *obj;
	g_assert (IS_MG_QF_VALUE (orig));
	qf = MG_QF_VALUE (orig);

	obj = mg_qf_value_new (qf->priv->query, qf->priv->srv_type);
	nqf = MG_QF_VALUE (obj);
	if (qf->priv->value)
		nqf->priv->value = gda_value_copy (qf->priv->value);
	if (qf->priv->default_value)
		nqf->priv->default_value = gda_value_copy (qf->priv->default_value);
	nqf->priv->is_parameter = qf->priv->is_parameter;
	nqf->priv->is_null_allowed = qf->priv->is_null_allowed;

	if (qf->priv->value_prov_ref) {
		MgBase *ref = mg_ref_base_get_ref_object (qf->priv->value_prov_ref);
		if (ref) {
			g_assert (IS_MG_QFIELD (ref));
			mg_qf_value_set_force_param (nqf, MG_QFIELD (ref), NULL);
		}
		else
			mg_qf_value_set_force_param_xml (nqf, 
							 mg_ref_base_get_ref_name (qf->priv->value_prov_ref, NULL, NULL),
							 NULL);
	}

	if (mg_base_get_name (MG_BASE (orig)))
		mg_base_set_name (MG_BASE (obj), mg_base_get_name (MG_BASE (orig)));

	if (mg_base_get_description (MG_BASE (orig)))
		mg_base_set_description (MG_BASE (obj), mg_base_get_description (MG_BASE (orig)));

	if (qf->priv->plugin)
		nqf->priv->plugin = g_strdup (qf->priv->plugin);


	return obj;
}

static gboolean
mg_qf_value_is_equal (MgQfield *qfield1, MgQfield *qfield2)
{
	gboolean retval;
	MgQfValue *qf1, *qf2;
	GdaValue *val1, *val2;
	GdaValueType t1 = GDA_VALUE_TYPE_NULL, t2 = GDA_VALUE_TYPE_NULL;

	/* it is here assumed that qfield1 and qfield2 are of the same type and refer to the same
	   query */
	g_assert (IS_MG_QF_VALUE (qfield1));
	g_assert (IS_MG_QF_VALUE (qfield2));
	qf1 = MG_QF_VALUE (qfield1);
	qf2 = MG_QF_VALUE (qfield2);

	/* comparing values */
	val1 = qf1->priv->value;
	val2 = qf2->priv->value;
	if (val1)
		t1 = gda_value_get_type (val1);
	if (val2)
		t2 = gda_value_get_type (val2);

	retval = qf1->priv->srv_type == qf2->priv->srv_type ? TRUE : FALSE;

	if (retval) 
		retval = (t1 == t2) ? TRUE : FALSE;

	if (retval && (t1 != GDA_VALUE_TYPE_NULL)) 
		retval = gda_value_compare (val1, val2) ? FALSE : TRUE;

	return retval;
}

/**
 * mg_qf_value_get_value
 * @field: a #MgQfValue object
 *
 * Get the value stored by @field. If there is no value, but a default value exists, then the
 * default value is returned.n it's up to the caller to test if there is a default value for @field.
 * The default value can be of a different type than the one expected by @field.
 *
 * Returns: the value or NULL
 */
const GdaValue *
mg_qf_value_get_value (MgQfValue *field)
{
	g_return_val_if_fail (field && IS_MG_QF_VALUE (field), NULL);
	g_return_val_if_fail (field->priv, NULL);

	return field->priv->value;

	return NULL;
}

/**
 * mg_qf_value_set_default_value
 * @field: a #MgQfValue object
 * @default_val: the default value to be set, or %NULL
 *
 * Sets the default value of @field, or removes it (if @default_val is %NULL)
 */
void
mg_qf_value_set_default_value (MgQfValue *field, const GdaValue *default_val)
{
	g_return_if_fail (field && IS_MG_QF_VALUE (field));
	g_return_if_fail (field->priv);
	
	if (field->priv->default_value) {
		gda_value_free (field->priv->default_value);
		field->priv->default_value = NULL;
	}

	if (default_val) 
		field->priv->default_value = gda_value_copy (default_val);
}

/**
 * mg_qf_value_get_default_value
 * @field: a #MgQfValue object
 *
 * Get the default value stored by @field.
 *
 * Returns: the value or NULL
 */
const GdaValue *
mg_qf_value_get_default_value (MgQfValue *field)
{
	g_return_val_if_fail (field && IS_MG_QF_VALUE (field), NULL);
	g_return_val_if_fail (field->priv, NULL);

	return field->priv->default_value;
}

/**
 * mg_qf_value_get_value_type
 * @field: a #MgQfValue object
 *
 * Get the GDA type of value stored within @field
 *
 * Returns: the type
 */
GdaValueType
mg_qf_value_get_value_type (MgQfValue *field)
{
	g_return_val_if_fail (field && IS_MG_QF_VALUE (field), GDA_VALUE_TYPE_UNKNOWN);
	g_return_val_if_fail (field->priv, GDA_VALUE_TYPE_UNKNOWN);

	return field->priv->gda_type;
}

/**
 * mg_qf_value_set_is_parameter
 * @field: a #MgQfValue object
 * @is_param:
 *
 * Sets wether @field can be considered as a parameter
 */
void
mg_qf_value_set_is_parameter (MgQfValue *field, gboolean is_param)
{
	g_return_if_fail (field && IS_MG_QF_VALUE (field));
	g_return_if_fail (field->priv);

	field->priv->is_parameter = is_param;
}


/**
 * mg_qf_value_is_parameter
 * @field: a #MgQfValue object
 *
 * Tells if @field can be considered as a parameter
 *
 * Returns: TRUE if @field can be considered as a parameter
 */
gboolean
mg_qf_value_is_parameter (MgQfValue *field)
{
	g_return_val_if_fail (field && IS_MG_QF_VALUE (field), FALSE);
	g_return_val_if_fail (field->priv, FALSE);

	return field->priv->is_parameter;
}

static GSList *
mg_qf_value_get_params (MgQfield *qfield)
{
	GSList *list = NULL;
	MgQfValue *field = MG_QF_VALUE (qfield);
	
	if (field->priv->is_parameter) {
		MgParameter *param;
		param = MG_PARAMETER (mg_parameter_new_with_field (qfield, field->priv->srv_type));
		
		/* parameter's attributes */
		mg_base_set_name (MG_BASE (param), mg_base_get_name (MG_BASE (field)));
		mg_base_set_description (MG_BASE (param), mg_base_get_description (MG_BASE (field)));

		mg_parameter_set_value (param, field->priv->value);

		if (field->priv->default_value)
			mg_parameter_set_default_value (param, field->priv->default_value);
		mg_parameter_set_not_null (param, !field->priv->is_null_allowed);
		g_object_set (G_OBJECT (param), "dest_qf_internal", mg_qfield_is_internal (qfield), NULL);
		
		mg_parameter_set_user_input_required (param, field->priv->is_parameter);
			

		/* specified plugin */
		if (field->priv->plugin)
			g_object_set (G_OBJECT (param), "handler_plugin", field->priv->plugin, NULL);

		/* possible values in a sub query */
		if (field->priv->value_prov_ref) {
			MgBase *ref;

			ref = mg_ref_base_get_ref_object (field->priv->value_prov_ref);
			if (ref) {
				MgField *provf;
				MgEntity *provq;
				GSList *tmplist;

				provf = MG_FIELD (ref);
				provq = mg_field_get_entity (provf);
				g_return_val_if_fail (IS_MG_QUERY (provq), NULL);

				tmplist = mg_entity_get_parameters (provq);
				if (tmplist) {
					GSList *dlist = tmplist;
					while (dlist) {
						mg_parameter_add_dependency (param, MG_PARAMETER (dlist->data));
						dlist = g_slist_next (dlist);
					}
					list = g_slist_concat (list, tmplist);
				}
				mg_parameter_set_in_field (param, MG_QFIELD (ref), NULL);
			}
		}

		list = g_slist_append (list, param);
	}
	
	return list;
}

/**
 * mg_qf_value_set_not_null
 * @field: a #MgQfValue object
 * @not_null:
 *
 * Sets if a NULL value is acceptable for @field. If @not_null is TRUE, then @field
 * can't have a NULL value.
 */
void
mg_qf_value_set_not_null (MgQfValue *field, gboolean not_null)
{
	g_return_if_fail (field && IS_MG_QF_VALUE (field));
	g_return_if_fail (field->priv);

	field->priv->is_null_allowed = !not_null;
}

/**
 * mg_qf_value_get_not_null
 * @field: a #MgQfValue object
 *
 * Tells if @field can receive a NULL value.
 *
 * Returns: TRUE if @field can't have a NULL value
 */
gboolean
mg_qf_value_get_not_null (MgQfValue *field)
{
	g_return_val_if_fail (field && IS_MG_QF_VALUE (field), FALSE);
	g_return_val_if_fail (field->priv, FALSE);

	return !field->priv->is_null_allowed;
}


/**
 * mg_qf_value_is_value_null
 * @field: a #MgQfValue object
 *
 * Tells if @field represents a NULL value.
 *
 * Returns:
 */
gboolean
mg_qf_value_is_value_null (MgQfValue *field, MgContext *context)
{
	gboolean value_found;
	const GdaValue *value;

	g_return_val_if_fail (field && IS_MG_QF_VALUE (field), FALSE);
	g_return_val_if_fail (field->priv, FALSE);

	value_found = mg_qf_value_render_find_value (field, context, &value, NULL);
	if (!value_found) 
		value = field->priv->value;

	if (!value || gda_value_is_null (value))
		return TRUE;
	else
		return FALSE;
}

/*
 * mg_qf_value_set_force_param
 * @field: a #MgQfValue object
 * @provider: a #MgQfield object, or NULL
 * @error: location to store error, or %NULL
 *
 * Forces the @field parameter's value to be in the possible values represented by @provider.
 * @provider must be a field in another entity (a #MgQuery listed as a parameter source for @field's
 * own query). This other entity must be a selection query.
 *
 * Returns: TRUE if no error occured
 */
static gboolean
mg_qf_value_set_force_param (MgQfValue *field, MgQfield *provider, GError **error)
{
	g_return_val_if_fail (field && IS_MG_QF_VALUE (field), FALSE);
	g_return_val_if_fail (field->priv, FALSE);

	if (field->priv->value_prov_ref) {
		g_object_unref (G_OBJECT (field->priv->value_prov_ref));
		field->priv->value_prov_ref = NULL;
	}

	if (provider) {
		MgEntity *entity;
		MgQuery *query;
		MgServerDataType *dt;

		/* Specific tests */
		g_return_val_if_fail (IS_MG_QFIELD (provider), FALSE);
		
		if (!mg_qfield_is_visible (provider)) {
			g_set_error (error,
				     MG_QF_VALUE_ERROR,
				     MG_QF_VALUE_PARAM_ERROR,
				     _("A field providing a parameter's value must be visible"));
			return FALSE;
		}

		entity = mg_field_get_entity (MG_FIELD (provider));
		g_return_val_if_fail (IS_MG_QUERY (entity), FALSE);
		query = MG_QUERY (entity);

		if (!mg_query_is_select_query (query)) {
			g_set_error (error,
				     MG_QF_VALUE_ERROR,
				     MG_QF_VALUE_PARAM_ERROR,
				     _("A query providing a parameter must be a selection query"));
			return FALSE;
		}
		
		dt = mg_field_get_data_type (MG_FIELD (provider));
		if (dt && (dt != field->priv->srv_type)) {
				g_set_error (error,
				     MG_QF_VALUE_ERROR,
				     MG_QF_VALUE_PARAM_ERROR,
				     _("Incompatible field type for a parameter's provider (%s / %s)"), 
				     mg_server_data_type_get_sqlname (dt), mg_server_data_type_get_sqlname (field->priv->srv_type));
				return FALSE;
			}
		
		field->priv->value_prov_ref = MG_REF_BASE (mg_ref_base_new (mg_base_get_conf (MG_BASE (field))));
		mg_ref_base_set_ref_object_type (field->priv->value_prov_ref, MG_BASE (provider), MG_FIELD_TYPE);
	}
	
	return TRUE;
}

/*
 * mg_qf_value_set_force_param_xml
 * @field: a #MgQfValue object
 * @prov_xml_id: the XML Id of a #MgDfield object, or NULL
 * @error: location to store error, or %NULL
 *
 * Same as mg_qf_value_set_force_param() but using an XML Id instead of a direct pointer
 *
 * Returns: TRUE if no error occured
 */
static gboolean 
mg_qf_value_set_force_param_xml (MgQfValue *field, const gchar *prov_xml_id, GError **error)
{
	g_return_val_if_fail (field && IS_MG_QF_VALUE (field), FALSE);
	g_return_val_if_fail (field->priv, FALSE);

	if (field->priv->value_prov_ref) {
		g_object_unref (G_OBJECT (field->priv->value_prov_ref));
		field->priv->value_prov_ref = NULL;
	}

	if (prov_xml_id) {
		/* No test possible, really */
		field->priv->value_prov_ref = MG_REF_BASE (mg_ref_base_new (mg_base_get_conf (MG_BASE (field))));
		mg_ref_base_set_ref_name (field->priv->value_prov_ref, MG_FIELD_TYPE, REFERENCE_BY_XML_ID,
					  prov_xml_id);
	}

	return TRUE;	
}


/*
 * mg_qf_value_get_force_param
 * @field: a #MgQfValue object
 *
 * Get the #MgQuery which restricts the possible values for the parameter
 *
 * Returns: the #MgQuery, or NULL
 */
static MgQfield *
mg_qf_value_get_force_param (MgQfValue *field)
{
	MgQfield *retval = NULL;

	g_return_val_if_fail (field && IS_MG_QF_VALUE (field), NULL);
	g_return_val_if_fail (field->priv, NULL);
	
	if (field->priv->value_prov_ref) {
		MgBase *base = mg_ref_base_get_ref_object (field->priv->value_prov_ref);
		if (base)
			retval = MG_QFIELD (base);
	}

	return retval;
}

/**
 * mg_qf_value_get_server_data_type
 * @field: a #MgQfValue object
 *
 * Get the #MgserverDataType type of @field
 *
 * Returns: the #MgserverDataType type
 */
MgServerDataType *
mg_qf_value_get_server_data_type (MgQfValue *field)
{
	g_return_val_if_fail (field && IS_MG_QF_VALUE (field), NULL);
	g_return_val_if_fail (field->priv, NULL);

	return field->priv->srv_type;
}

#ifdef debug
static void
mg_qf_value_dump (MgQfValue *field, guint offset)
{
	gchar *str;
	gint i;
	MgDataHandler *dh;

	g_return_if_fail (field && IS_MG_QF_VALUE (field));
	
        /* string for the offset */
        str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;

        /* dump */
        if (field->priv) {
		gchar *val;
		dh = mg_server_data_type_get_handler (field->priv->srv_type);
                g_print ("%s" D_COL_H1 "MgQfValue" D_COL_NOR " \"%s\" (%p, id=%d) ",
                         str, mg_base_get_name (MG_BASE (field)), field, mg_base_get_id (MG_BASE (field)));
		if (field->priv->is_parameter) 
			g_print ("Parameter ");

		if (mg_qf_value_is_active (MG_REFERER (field)))
			g_print ("Active, ");
		else
			g_print (D_COL_ERR "Inactive" D_COL_NOR ", ");

		if (mg_qfield_is_visible (MG_QFIELD (field)))
			g_print ("Visible, ");
		if (field->priv->value) {
			val = mg_data_handler_get_sql_from_value (dh, field->priv->value);
			g_print ("Value: %s ", val);
			g_free (val);
		}

		if (field->priv->default_value) {
			MgServer *srv = mg_conf_get_server (MG_CONF (mg_base_get_conf (MG_BASE (field->priv->srv_type))));
			MgDataHandler *dhd = mg_server_get_handler_by_gda (srv, gda_value_get_type (field->priv->default_value));
			val = mg_data_handler_get_sql_from_value (dhd, field->priv->default_value);
			g_print ("Default: %s ", val);
			g_free (val);
		}
		g_print ("\n");

		if (field->priv->value_prov_ref) {
			g_print ("%sPossible values:\n", str);
			mg_base_dump (MG_BASE (field->priv->value_prov_ref), offset+5);
		}
	}
        else
                g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, field);
}
#endif


/* 
 * MgField interface implementation
 */
static MgEntity *
mg_qf_value_get_entity (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_QF_VALUE (iface), NULL);
	g_return_val_if_fail (MG_QF_VALUE (iface)->priv, NULL);

	return MG_ENTITY (MG_QF_VALUE (iface)->priv->query);
}

static MgServerDataType *
mg_qf_value_get_data_type (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_QF_VALUE (iface), NULL);
	g_return_val_if_fail (MG_QF_VALUE (iface)->priv, NULL);

	return MG_QF_VALUE (iface)->priv->srv_type;
}

static const gchar *
mg_qf_value_get_name (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_QF_VALUE (iface), NULL);

	return mg_base_get_name (MG_BASE (iface));
}

static const gchar *
mg_qf_value_get_description (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_QF_VALUE (iface), NULL);
	
	return mg_base_get_description (MG_BASE (iface));	
}


/* 
 * MgXmlStorage interface implementation
 */
static gchar *
mg_qf_value_get_xml_id (MgXmlStorage *iface)
{
	gchar *q_xml_id, *xml_id;

	g_return_val_if_fail (iface && IS_MG_QF_VALUE (iface), NULL);
	g_return_val_if_fail (MG_QF_VALUE (iface)->priv, NULL);

	q_xml_id = mg_xml_storage_get_xml_id (MG_XML_STORAGE (MG_QF_VALUE (iface)->priv->query));
	xml_id = g_strdup_printf ("%s:QF%d", q_xml_id, mg_base_get_id (MG_BASE (iface)));
	g_free (q_xml_id);
	
	return xml_id;
}

static xmlNodePtr
mg_qf_value_save_to_xml (MgXmlStorage *iface, GError **error)
{
	xmlNodePtr node = NULL;
	MgQfValue *field;
	gchar *str;
	MgDataHandler *dh;

	g_return_val_if_fail (iface && IS_MG_QF_VALUE (iface), NULL);
	g_return_val_if_fail (MG_QF_VALUE (iface)->priv, NULL);

	field = MG_QF_VALUE (iface);

	node = xmlNewNode (NULL, "MG_QF");
	
	str = mg_qf_value_get_xml_id (iface);
	xmlSetProp (node, "id", str);
	g_free (str);

	xmlSetProp (node, "type", "VAL");
	xmlSetProp (node, "name", mg_base_get_name (MG_BASE (field)));
	if (mg_base_get_description (MG_BASE (field)) && *mg_base_get_description (MG_BASE (field)))
		xmlSetProp (node, "descr", mg_base_get_description (MG_BASE (field)));
	if (! mg_qfield_is_visible (MG_QFIELD (field)))
		xmlSetProp (node, "is_visible",  "f");
	if (mg_qfield_is_internal (MG_QFIELD (field)))
		xmlSetProp (node, "is_internal", "t");

	xmlSetProp (node, "is_param", field->priv->is_parameter ? "t" : "f");
	str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (field->priv->srv_type));
	xmlSetProp (node, "srv_type",  str);
	g_free (str);

	dh = mg_server_data_type_get_handler (field->priv->srv_type);
	if (field->priv->value) {
		str = mg_data_handler_get_str_from_value (dh, field->priv->value);
		xmlSetProp (node, "value", str);
		g_free (str);
	}
	if (field->priv->default_value) {
		MgServer *srv = mg_conf_get_server (MG_CONF (mg_base_get_conf (MG_BASE (field->priv->srv_type))));
		MgDataHandler *dhd = mg_server_get_handler_by_gda (srv, gda_value_get_type (field->priv->default_value));
		GdaValueType vtype;
		
		str = mg_data_handler_get_str_from_value (dhd, field->priv->default_value);
		xmlSetProp (node, "default", str);
		g_free (str);
		vtype = gda_value_get_type (field->priv->default_value);
		xmlSetProp (node, "default_gda_type", gda_type_to_string (vtype));
	}

	xmlSetProp (node, "null_ok", field->priv->is_null_allowed ? "t" : "f");
	if (field->priv->value_prov_ref)
		xmlSetProp (node, "value_prov", 
			    mg_ref_base_get_ref_name (field->priv->value_prov_ref, NULL, NULL));

	if (field->priv->plugin)
		xmlSetProp (node, "plugin", field->priv->plugin);

	return node;
}

static gboolean
mg_qf_value_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgQfValue *field;
	gchar *prop;
	MgDataHandler *dh = NULL;
	gboolean err = FALSE;

	g_return_val_if_fail (iface && IS_MG_QF_VALUE (iface), FALSE);
	g_return_val_if_fail (MG_QF_VALUE (iface)->priv, FALSE);
	g_return_val_if_fail (node, FALSE);

	field = MG_QF_VALUE (iface);
	if (strcmp (node->name, "MG_QF")) {
		g_set_error (error,
			     MG_QF_VALUE_ERROR,
			     MG_QF_VALUE_XML_LOAD_ERROR,
			     _("XML Tag is not <MG_QF>"));
		return FALSE;
	}

	prop = xmlGetProp (node, "type");
	if (prop) {
		if (strcmp (prop, "VAL")) {
			g_set_error (error,
				     MG_QF_VALUE_ERROR,
				     MG_QF_VALUE_XML_LOAD_ERROR,
				     _("Wrong type of field in <MG_QF>"));
			return FALSE;
		}
		g_free (prop);
	}

	prop = xmlGetProp (node, "id");
	if (prop) {
		gchar *ptr, *tok;
		ptr = strtok_r (prop, ":", &tok);
		ptr = strtok_r (NULL, ":", &tok);
		if (strlen (ptr) < 3) {
			g_set_error (error,
				     MG_QF_VALUE_ERROR,
				     MG_QF_VALUE_XML_LOAD_ERROR,
				     _("Wrong 'id' attribute in <MG_QF>"));
			return FALSE;
		}
		mg_base_set_id (MG_BASE (field), atoi (ptr+2));
		g_free (prop);
	}

	prop = xmlGetProp (node, "name");
	if (prop) {
		mg_base_set_name (MG_BASE (field), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "descr");
	if (prop) {
		mg_base_set_description (MG_BASE (field), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "is_visible");
	if (prop) {
		mg_qfield_set_visible (MG_QFIELD (field), (*prop == 't') ? TRUE : FALSE);
		g_free (prop);
	}

	prop = xmlGetProp (node, "is_internal");
	if (prop) {
		mg_qfield_set_internal (MG_QFIELD (field), (*prop == 't') ? TRUE : FALSE);
		g_free (prop);
	}

	prop = xmlGetProp (node, "srv_type");
	if (prop) {
		field->priv->srv_type = mg_server_get_data_type_by_xml_id (mg_conf_get_server (mg_base_get_conf (MG_BASE (field))), prop);
		if (field->priv->srv_type) {
			dh = mg_server_data_type_get_handler (field->priv->srv_type);
			field->priv->gda_type = mg_server_data_type_get_gda_type (field->priv->srv_type);
		}
		g_free (prop);
	}

	prop = xmlGetProp (node, "value");
	if (prop) {
		if (dh)
			field->priv->value = mg_data_handler_get_value_from_str (dh, prop, field->priv->gda_type);

		g_free (prop);
	}

	prop = xmlGetProp (node, "default");
	if (prop) {
		gchar *str2;

		str2 = xmlGetProp (node, "default_gda_type");
		if (str2) {
			if (field->priv->srv_type) {
				MgServer *srv = mg_conf_get_server (MG_CONF (mg_base_get_conf (MG_BASE (field->priv->srv_type))));
				MgDataHandler *dh2;
				GdaValueType vtype;
				GdaValue *value;
				
				vtype = gda_type_from_string (str2);			
				dh2 = mg_server_get_handler_by_gda (srv, vtype);
				value = mg_data_handler_get_value_from_str (dh2, prop, vtype);
				field->priv->default_value = value;
			}

			g_free (str2);
		}
		g_free (prop);
	}

	prop = xmlGetProp (node, "is_param");
	if (prop) {
		field->priv->is_parameter = (*prop == 't') ? TRUE : FALSE;
		g_free (prop);
	}

	prop = xmlGetProp (node, "null_ok");
	if (prop) {
		field->priv->is_null_allowed = (*prop == 't') ? TRUE : FALSE;
		g_free (prop);
	}

	prop = xmlGetProp (node, "plugin");
	if (prop) 
		field->priv->plugin = prop;

	prop = xmlGetProp (node, "value_prov");
	if (prop) {
		if (!mg_qf_value_set_force_param_xml (field, prop, error))
			err = TRUE;
		g_free (prop);
	}

	if (!dh) {
		err = TRUE;
		g_set_error (error,
			     MG_QF_VALUE_ERROR,
			     MG_QF_VALUE_XML_LOAD_ERROR,
			     _("Missing required gda_type for <MG_QF>"));
	}

	if (!err) {
		if (!field->priv->is_parameter) {
			if (!field->priv->value) {
				err = TRUE;
				g_set_error (error,
					     MG_QF_VALUE_ERROR,
					     MG_QF_VALUE_XML_LOAD_ERROR,
					     _("Value field '%s' does not have a value!"),
					     mg_base_get_name (MG_BASE (field)));
			}
		}
	}

	return !err;
}


/*
 * MgRenderer interface implementation
 */
static GdaXqlItem *
mg_qf_value_render_as_xql (MgRenderer *iface, MgContext *context, GError **error)
{
	GdaXqlItem *node = NULL;

	g_return_val_if_fail (iface && IS_MG_QF_VALUE (iface), NULL);
	g_return_val_if_fail (MG_QF_VALUE (iface)->priv, NULL);
	
	TO_IMPLEMENT;
	return node;
}

static gboolean
mg_qf_value_render_find_value (MgQfValue *field, MgContext *context, const GdaValue **value_found, MgParameter **param_source)
{
	const GdaValue *cvalue = NULL;
	gboolean found = FALSE;

	if (param_source)
		*param_source = NULL;
	if (value_found)
		*value_found = NULL;

	/* looking for a value into the context first */
	if (context) {
		GSList *list = context->parameters;
		GSList *for_fields;
		
		while (list && !found) {
			for_fields = mg_parameter_get_dest_fields (MG_PARAMETER (list->data));
			if (g_slist_find (for_fields, field)) {
				if (param_source)
					*param_source = MG_PARAMETER (list->data);
				found = TRUE;
				cvalue = mg_parameter_get_value (MG_PARAMETER (list->data));
			}
			list = g_slist_next (list);
		}
	}
	
	/* using the field's value, if available */
	if (!cvalue && field->priv->value) {
		found = TRUE;
		cvalue = field->priv->value;
	}

	if (value_found)
		*value_found = cvalue;

	return found;
}

static gchar *
mg_qf_value_render_as_sql (MgRenderer *iface, MgContext *context, GError **error)
{
	gchar *str = NULL;
	MgQfValue *field;
	const GdaValue *value = NULL;
	gboolean value_found = FALSE;
	MgParameter *param_source = NULL;

	g_return_val_if_fail (iface && IS_MG_QF_VALUE (iface), NULL);
	g_return_val_if_fail (MG_QF_VALUE (iface)->priv, NULL);
	field = MG_QF_VALUE (iface);

	value_found = mg_qf_value_render_find_value (field, context, &value, &param_source);
	
	/* actual rendering */
	if (value_found) {
		if (param_source)
			if (! mg_parameter_is_valid (param_source)) {
				g_set_error (error,
					     MG_QF_VALUE_ERROR,
					     MG_QF_VALUE_RENDER_ERROR,
					     _("Using invalid parameter data"));
				return NULL;
			}
		if (value && (gda_value_get_type (value) != GDA_VALUE_TYPE_NULL)) {
			MgDataHandler *dh;
			
			dh = mg_server_data_type_get_handler (field->priv->srv_type);
			str = mg_data_handler_get_sql_from_value (dh, value);
		}
		else {
			if (param_source) {
				gboolean use_default;
				g_object_get (G_OBJECT (param_source), "use_default_value", &use_default, NULL);
				if (use_default) {
					/* REM: we don't use a MgDataHandler here because we don't need it. */
					value = mg_parameter_get_default_value (param_source);

					str = gda_value_stringify (value);
				}
				else
					str = g_strdup ("NULL");
			}
			else
				str = g_strdup ("NULL");
		}
	}
	else {
		if (field->priv->is_null_allowed)
			str = g_strdup (_("<VALUE>"));
		else {
			if (context) {
				g_set_error (error,
					     MG_QF_VALUE_ERROR,
					     MG_QF_VALUE_RENDER_ERROR,
					     _("No specified value"));
			}
			else
				str = g_strdup (_("<VALUE>"));
		}
	}
	
	return str;
}

static gchar *
mg_qf_value_render_as_str (MgRenderer *iface, MgContext *context)
{
	gchar *str = NULL;

	g_return_val_if_fail (iface && IS_MG_QF_VALUE (iface), NULL);
	g_return_val_if_fail (MG_QF_VALUE (iface)->priv, NULL);
	
	str = mg_qf_value_render_as_sql (iface, context, NULL);
	if (!str)
		str = g_strdup ("???");
	return str;
}


/*
 * MgReferer interface implementation
 */
static gboolean
mg_qf_value_activate (MgReferer *iface)
{
	g_return_val_if_fail (iface && IS_MG_QF_VALUE (iface), FALSE);
	g_return_val_if_fail (MG_QF_VALUE (iface)->priv, FALSE);

	if (MG_QF_VALUE (iface)->priv->value_prov_ref)
		return mg_ref_base_activate (MG_QF_VALUE (iface)->priv->value_prov_ref);
	else
		return TRUE;
}

static void
mg_qf_value_deactivate (MgReferer *iface)
{
	g_return_if_fail (iface && IS_MG_QF_VALUE (iface));
	g_return_if_fail (MG_QF_VALUE (iface)->priv);

	if (MG_QF_VALUE (iface)->priv->value_prov_ref)
		mg_ref_base_deactivate (MG_QF_VALUE (iface)->priv->value_prov_ref);
}

static gboolean
mg_qf_value_is_active (MgReferer *iface)
{
	g_return_val_if_fail (iface && IS_MG_QF_VALUE (iface), FALSE);
	g_return_val_if_fail (MG_QF_VALUE (iface)->priv, FALSE);

	if (MG_QF_VALUE (iface)->priv->value_prov_ref)
		return mg_ref_base_is_active (MG_QF_VALUE (iface)->priv->value_prov_ref);
	else
		return TRUE;
}

static GSList *
mg_qf_value_get_ref_objects (MgReferer *iface)
{
	GSList *list = NULL;

	g_return_val_if_fail (iface && IS_MG_QF_VALUE (iface), NULL);
	g_return_val_if_fail (MG_QF_VALUE (iface)->priv, NULL);

	if (MG_QF_VALUE (iface)->priv->value_prov_ref) {
		MgBase *base = mg_ref_base_get_ref_object (MG_QF_VALUE (iface)->priv->value_prov_ref);
		if (base)
			list = g_slist_append (list, base);
	}

        return list;
}

static void
mg_qf_value_replace_refs (MgReferer *iface, GHashTable *replacements)
{
	MgQfValue *field;

        g_return_if_fail (iface && IS_MG_QF_VALUE (iface));
        g_return_if_fail (MG_QF_VALUE (iface)->priv);

        field = MG_QF_VALUE (iface);	
        if (field->priv->query) {
                MgQuery *query = g_hash_table_lookup (replacements, field->priv->query);
                if (query) {
                        g_signal_handlers_disconnect_by_func (G_OBJECT (field->priv->query),
                                                              G_CALLBACK (nullified_object_cb), field);
                        field->priv->query = query;
                        g_signal_connect (G_OBJECT (query), "nullified",
                                          G_CALLBACK (nullified_object_cb), field);
                }
        }

	if (field->priv->value_prov_ref)
		mg_ref_base_replace_ref_object (field->priv->value_prov_ref, replacements);
}
