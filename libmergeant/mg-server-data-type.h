/* mg-server-data-type.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_SERVER_DATA_TYPE_H_
#define __MG_SERVER_DATA_TYPE_H_

#include "mg-base.h"
#include "mg-defs.h"
#include <libgda/libgda.h>

G_BEGIN_DECLS

#define MG_SERVER_DATA_TYPE_TYPE          (mg_server_data_type_get_type())
#define MG_SERVER_DATA_TYPE(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_server_data_type_get_type(), MgServerDataType)
#define MG_SERVER_DATA_TYPE_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_server_data_type_get_type (), MgServerDataTypeClass)
#define IS_MG_SERVER_DATA_TYPE(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_server_data_type_get_type ())


/* error reporting */
extern GQuark mg_server_data_type_error_quark (void);
#define MG_SERVER_DATA_TYPE_ERROR mg_server_data_type_error_quark ()

enum
{
	MG_SERVER_DATA_TYPE_XML_LOAD_ERROR
};


/* struct for the object's data */
struct _MgServerDataType
{
	MgBase                         object;
	MgServerDataTypePrivate       *priv;
};

/* struct for the object's class */
struct _MgServerDataTypeClass
{
	MgBaseClass                    class;

	/* signals */
	void   (*templ_signal)        (MgServerDataType *obj);
};

guint          mg_server_data_type_get_type      (void);
GObject       *mg_server_data_type_new           (MgServer *srv);
void           mg_server_data_type_set_sqlname   (MgServerDataType *dt, const gchar *sqlname);
const gchar   *mg_server_data_type_get_sqlname   (MgServerDataType *dt);
void           mg_server_data_type_set_gda_type  (MgServerDataType *dt, GdaValueType gda_type);
GdaValueType   mg_server_data_type_get_gda_type  (MgServerDataType *dt);

void           mg_server_data_type_set_handler   (MgServerDataType *dt, MgDataHandler *dh);
MgDataHandler *mg_server_data_type_get_handler   (MgServerDataType *dt);

G_END_DECLS

#endif
