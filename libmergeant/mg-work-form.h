/* mg-work-form.h
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __MG_WORK_FORM__
#define __MG_WORK_FORM__

#include <gtk/gtk.h>
#include "mg-conf.h"

G_BEGIN_DECLS

#define MG_WORK_FORM_TYPE          (mg_work_form_get_type())
#define MG_WORK_FORM(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_work_form_get_type(), MgWorkForm)
#define MG_WORK_FORM_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_work_form_get_type (), MgWorkFormClass)
#define IS_MG_WORK_FORM(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_work_form_get_type ())


typedef struct _MgWorkForm      MgWorkForm;
typedef struct _MgWorkFormClass MgWorkFormClass;
typedef struct _MgWorkFormPriv  MgWorkFormPriv;
typedef enum   _MgWorkFormMode  MgWorkFormMode;

/* enum for the different modes of action */
enum _MgWorkFormMode
{
	MG_WORK_FORM_NAVIGATION_ARROWS  = 1 << 0,
	MG_WORK_FORM_NAVIGATION_SCROLL  = 1 << 1,
	MG_WORK_FORM_MODIF_BUTTONS      = 1 << 2,
	MG_WORK_FORM_MODIF_AUTO_COMMIT  = 1 << 3,
	MG_WORK_FORM_ASK_CONFIRM_UPDATE = 1 << 4,
	MG_WORK_FORM_ASK_CONFIRM_DELETE = 1 << 5,
	MG_WORK_FORM_ASK_CONFIRM_INSERT = 1 << 6
};

/* struct for the object's data */
struct _MgWorkForm
{
	GtkVBox             object;

	MgWorkFormPriv     *priv;
};

/* struct for the object's class */
struct _MgWorkFormClass
{
	GtkVBoxClass parent_class;
};

/* 
 * Generic widget's methods 
 */
guint        mg_work_form_get_type            (void);
GtkWidget   *mg_work_form_new                 (MgQuery *query, MgTarget *modified);
void         mg_work_form_run                 (MgWorkForm *form, guint mode);

void         mg_work_form_set_action_mode     (MgWorkForm *form, guint mode);

MgParameter *mg_work_form_get_param_for_field (MgWorkForm *form, MgQfield *field);

G_END_DECLS

#endif



