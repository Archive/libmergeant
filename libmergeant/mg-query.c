/* mg-query.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-query.h"
#include "mg-field.h"
#include "mg-qfield.h"
#include "mg-qf-field.h"
#include "mg-qf-value.h"
#include "mg-qf-all.h"
#include "mg-ref-base.h"
#include "mg-target.h"
#include "mg-entity.h"
#include "mg-db-table.h"
#include "mg-database.h"
#include "mg-db-constraint.h"
#include "mg-xml-storage.h"
#include "mg-referer.h"
#include "mg-renderer.h"
#include "mg-parameter.h"
#include "marshal.h"
#include "mg-join.h"
#include <string.h>
#include "mg-context.h"
#include "mg-condition.h"

/* 
 * Main static functions 
 */
static void mg_query_class_init (MgQueryClass * class);
static void mg_query_init (MgQuery * srv);
static void mg_query_dispose (GObject   * object);
static void mg_query_finalize (GObject   * object);

static void mg_query_set_property (GObject              *object,
				   guint                 param_id,
				   const GValue         *value,
				   GParamSpec           *pspec);
static void mg_query_get_property (GObject              *object,
				   guint                 param_id,
				   GValue               *value,
				   GParamSpec           *pspec);
/* Entity interface */
static void        mg_query_entity_init         (MgEntityIface *iface);
static GSList     *mg_query_get_all_fields      (MgEntity *iface);
static GSList     *mg_query_get_visible_fields  (MgEntity *iface);
static MgField    *mg_query_get_field_by_name   (MgEntity *iface, const gchar *name);
static MgField    *mg_query_get_field_by_xml_id (MgEntity *iface, const gchar *xml_id);
static MgField    *mg_query_get_field_by_index  (MgEntity *iface, gint index);
static gint        mg_query_get_field_index     (MgEntity *iface, MgField *field);
static void        mg_query_add_field           (MgEntity *iface, MgField *field);
static void        mg_query_add_field_before    (MgEntity *iface, MgField *field, MgField *field_before);
static void        mg_query_swap_fields         (MgEntity *iface, MgField *field1, MgField *field2);
static void        mg_query_remove_field        (MgEntity *iface, MgField *field);
static gboolean    mg_query_is_writable         (MgEntity *iface);
static GSList     *mg_query_get_parameters      (MgEntity *iface);

/* XML storage interface */
static void        mg_query_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_query_get_xml_id       (MgXmlStorage *iface);
static xmlNodePtr  mg_query_save_to_xml      (MgXmlStorage *iface, GError **error);
static gboolean    mg_query_load_from_xml    (MgXmlStorage *iface, xmlNodePtr node, GError **error);

/* Referer interface */
static void        mg_query_referer_init        (MgRefererIface *iface);
static gboolean    mg_query_activate            (MgReferer *iface);
static void        mg_query_deactivate          (MgReferer *iface);
static gboolean    mg_query_is_active           (MgReferer *iface);
static GSList     *mg_query_get_ref_objects     (MgReferer *iface);
static void        mg_query_replace_refs        (MgReferer *iface, GHashTable *replacements);

/* Renderer interface */
static void        mg_query_renderer_init   (MgRendererIface *iface);
static GdaXqlItem *mg_query_render_as_xql   (MgRenderer *iface, MgContext *context, GError **error);
static gchar      *mg_query_render_as_sql   (MgRenderer *iface, MgContext *context, GError **error);
static gchar      *mg_query_render_as_str   (MgRenderer *iface, MgContext *context);

/* callbaks from sub objects */
static void nullified_target_cb (MgTarget *target, MgQuery *query);
static void nullified_field_cb (MgField *field, MgQuery *query);
static void nullified_join_cb (MgJoin *join, MgQuery *query);
static void nullified_cond_cb (MgCondition *cond, MgQuery *query);
static void nullified_sub_query_cb (MgQuery *sub_query, MgQuery *query);
static void nullified_parent_query (MgQuery *parent_query, MgQuery *query);

static void nullified_param_source_cb (MgQuery *param_source, MgQuery *query);

static void changed_target_cb (MgTarget *target, MgQuery *query);
static void changed_field_cb (MgField *field, MgQuery *query);
static void changed_join_cb (MgJoin *join, MgQuery *query);
static void changed_sub_query_cb (MgQuery *sub_query, MgQuery *query);

static void change_parent_query (MgQuery *query, MgQuery *parent_query);

static void id_target_changed_cb (MgTarget *target, MgQuery *query);
static void id_field_changed_cb (MgQfield *field, MgQuery *query);
static void id_cond_changed_cb (MgCondition *cond, MgQuery *query);

#ifdef debug
static void mg_query_dump (MgQuery *table, guint offset);
#endif

typedef struct
{
	GSList *targets;
	GSList *joins;
} JoinsPack;
#define JOINS_PACK(x) ((JoinsPack *)x)


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	TYPE_CHANGED,
	CONDITION_CHANGED,
	TARGET_ADDED,
	TARGET_REMOVED,
	TARGET_UPDATED,
	JOIN_ADDED,
	JOIN_REMOVED,
	JOIN_UPDATED,
	SUB_QUERY_ADDED,
	SUB_QUERY_REMOVED,
	SUB_QUERY_UPDATED,
	LAST_SIGNAL
};

static gint mg_query_signals[LAST_SIGNAL] = { 0, 0 };

/* properties */
enum
{
	PROP_0,
	PROP_SERIAL_TARGET,
	PROP_SERIAL_FIELD,
	PROP_SERIAL_COND,
	PROP_REALLY_ALL_FIELDS
};


/* private structure */
struct _MgQueryPrivate
{
	MgQueryType  query_type;
	GSList      *targets;
	GSList      *joins_flat;
	GSList      *joins_pack;
	GSList      *fields;
	GSList      *sub_queries;
	GSList      *param_sources;
	MgCondition *cond;
	MgQuery     *parent_query;
	gchar       *sql; /* For MG_QUERY_TYPE_SQL only */

	GSList      *fields_order_by;

	guint        serial_target;
	guint        serial_field;
	guint        serial_cond;
};

/* module error */
GQuark mg_query_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_query_error");
	return quark;
}


guint
mg_query_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgQueryClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_query_class_init,
			NULL,
			NULL,
			sizeof (MgQuery),
			0,
			(GInstanceInitFunc) mg_query_init
		};

		static const GInterfaceInfo entity_info = {
			(GInterfaceInitFunc) mg_query_entity_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo xml_storage_info = {
			(GInterfaceInitFunc) mg_query_xml_storage_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo referer_info = {
			(GInterfaceInitFunc) mg_query_referer_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo renderer_info = {
			(GInterfaceInitFunc) mg_query_renderer_init,
			NULL,
			NULL
		};
		
		type = g_type_register_static (MG_BASE_TYPE, "MgQuery", &info, 0);
		g_type_add_interface_static (type, MG_ENTITY_TYPE, &entity_info);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
		g_type_add_interface_static (type, MG_REFERER_TYPE, &referer_info);
		g_type_add_interface_static (type, MG_RENDERER_TYPE, &renderer_info);
	}
	return type;
}

static void
mg_query_entity_init (MgEntityIface *iface)
{
	iface->get_all_fields = mg_query_get_all_fields;
	iface->get_visible_fields = mg_query_get_visible_fields;
	iface->get_field_by_name = mg_query_get_field_by_name;
	iface->get_field_by_xml_id = mg_query_get_field_by_xml_id;
	iface->get_field_by_index = mg_query_get_field_by_index;
	iface->get_field_index = mg_query_get_field_index;
	iface->add_field = mg_query_add_field;
	iface->add_field_before = mg_query_add_field_before;
	iface->swap_fields = mg_query_swap_fields;
	iface->remove_field = mg_query_remove_field;
	iface->is_writable = mg_query_is_writable;
	iface->get_parameters = mg_query_get_parameters;
}

static void 
mg_query_xml_storage_init (MgXmlStorageIface *iface)
{
	iface->get_xml_id = mg_query_get_xml_id;
	iface->save_to_xml = mg_query_save_to_xml;
	iface->load_from_xml = mg_query_load_from_xml;
}

static void
mg_query_referer_init (MgRefererIface *iface)
{
	iface->activate = mg_query_activate;
	iface->deactivate = mg_query_deactivate;
	iface->is_active = mg_query_is_active;
	iface->get_ref_objects = mg_query_get_ref_objects;
	iface->replace_refs = mg_query_replace_refs;
}

static void
mg_query_renderer_init (MgRendererIface *iface)
{
	iface->render_as_xql = mg_query_render_as_xql;
	iface->render_as_sql = mg_query_render_as_sql;
	iface->render_as_str = mg_query_render_as_str;
}

static void
mg_query_class_init (MgQueryClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_query_signals[TYPE_CHANGED] =
		g_signal_new ("type_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgQueryClass, type_changed),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	mg_query_signals[CONDITION_CHANGED] =
		g_signal_new ("condition_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgQueryClass, condition_changed),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	mg_query_signals[TARGET_ADDED] =
		g_signal_new ("target_added",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgQueryClass, target_added),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE,
			      1, G_TYPE_POINTER);
	mg_query_signals[TARGET_REMOVED] =
		g_signal_new ("target_removed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgQueryClass, target_removed),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE,
			      1, G_TYPE_POINTER);
	mg_query_signals[TARGET_UPDATED] =
		g_signal_new ("target_updated",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgQueryClass, target_updated),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE,
			      1, G_TYPE_POINTER);
	mg_query_signals[JOIN_ADDED] =
		g_signal_new ("join_added",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgQueryClass, join_added),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE,
			      1, G_TYPE_POINTER);
	mg_query_signals[JOIN_REMOVED] =
		g_signal_new ("join_removed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgQueryClass, join_removed),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE,
			      1, G_TYPE_POINTER);
	mg_query_signals[JOIN_UPDATED] =
		g_signal_new ("join_updated",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgQueryClass, join_updated),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE,
			      1, G_TYPE_POINTER);
	mg_query_signals[SUB_QUERY_ADDED] =
		g_signal_new ("sub_query_added",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgQueryClass, sub_query_added),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE,
			      1, G_TYPE_POINTER);
	mg_query_signals[SUB_QUERY_REMOVED] =
		g_signal_new ("sub_query_removed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgQueryClass, sub_query_removed),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE,
			      1, G_TYPE_POINTER);
	mg_query_signals[SUB_QUERY_UPDATED] =
		g_signal_new ("sub_query_updated",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgQueryClass, sub_query_updated),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE,
			      1, G_TYPE_POINTER);

	class->type_changed = NULL;
	class->condition_changed = NULL;
	class->target_added = NULL;
	class->target_removed = NULL;
	class->target_updated = NULL;
	class->join_added = NULL;
	class->join_removed = NULL;
	class->join_updated = NULL;
	class->sub_query_added = NULL;
	class->sub_query_removed = NULL;
	class->sub_query_updated = NULL;

	object_class->dispose = mg_query_dispose;
	object_class->finalize = mg_query_finalize;

	/* Properties */
	object_class->set_property = mg_query_set_property;
	object_class->get_property = mg_query_get_property;
	g_object_class_install_property (object_class, PROP_SERIAL_TARGET,
					 g_param_spec_uint ("target_serial", NULL, NULL, 
							    1, G_MAXUINT, 1, G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_SERIAL_FIELD,
					 g_param_spec_uint ("field_serial", NULL, NULL, 
							    1, G_MAXUINT, 1, G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_SERIAL_COND,
					 g_param_spec_uint ("cond_serial", NULL, NULL, 
							    1, G_MAXUINT, 1, G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_REALLY_ALL_FIELDS,
					 g_param_spec_pointer ("really_all_fields", NULL, NULL, G_PARAM_READABLE));

	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_query_dump;
#endif

}

static void
mg_query_init (MgQuery * mg_query)
{
	mg_query->priv = g_new0 (MgQueryPrivate, 1);
	mg_query->priv->query_type = MG_QUERY_TYPE_SELECT;
	mg_query->priv->targets = NULL;
	mg_query->priv->joins_flat = NULL;
	mg_query->priv->joins_pack = NULL;
	mg_query->priv->fields = NULL;
	mg_query->priv->sub_queries = NULL;
	mg_query->priv->param_sources = NULL;
	mg_query->priv->cond = NULL;
	mg_query->priv->parent_query = NULL;
	mg_query->priv->sql = NULL;
	mg_query->priv->fields_order_by = NULL;

	mg_query->priv->serial_target = 1;
	mg_query->priv->serial_field = 1;
	mg_query->priv->serial_cond = 1;
}

/**
 * mg_query_new
 * @conf: a #MgConf object
 *
 * Creates a new #MgQuery object
 *
 * Returns: the new object
 */
GObject*
mg_query_new (MgConf *conf)
{
	GObject   *obj;
	MgQuery *mg_query;
	guint id;

	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);
	obj = g_object_new (MG_QUERY_TYPE, "conf", conf, NULL);
	mg_query = MG_QUERY (obj);

	g_object_get (G_OBJECT (conf), "query_serial", &id, NULL);
	mg_base_set_id (MG_BASE (obj), id);

	mg_conf_declare_query (conf, mg_query);	

	return obj;
}


/**
 * mg_query_new_copy
 * @orig: a #MgQuery to make a copy of
 * @replacements: a hash table to store replacements, or %NULL
 * 
 * Copy constructor
 *
 * Returns: a the new copy of @orig
 */
GObject *
mg_query_new_copy (MgQuery *orig, GHashTable *replacements)
{
	GObject *obj;
	MgQuery *mg_query;
	MgConf *conf;
	GHashTable *repl;
	GSList *list;
	guint id;
	gint order_pos;

	g_return_val_if_fail (orig && IS_MG_QUERY (orig), NULL);

	conf = mg_base_get_conf (MG_BASE (orig));
	obj = g_object_new (MG_QUERY_TYPE, "conf", conf, NULL);
	mg_query = MG_QUERY (obj);

	g_object_get (G_OBJECT (conf), "query_serial", &id, NULL);
	mg_base_set_id (MG_BASE (obj), id);
	mg_conf_declare_query (conf, mg_query);	

	/* hash table for replacements */
	if (!replacements)
		repl = g_hash_table_new (NULL, NULL);
	else
		repl = replacements;

	g_hash_table_insert (repl, orig, mg_query);

	/* REM: parent query link is handled by the caller of this method */
	
	/* copy starts */
	mg_base_set_name (MG_BASE (mg_query), mg_base_get_name (MG_BASE (orig)));
	mg_base_set_description (MG_BASE (mg_query), mg_base_get_description (MG_BASE (orig)));
	mg_query->priv->query_type = orig->priv->query_type;
	mg_referer_replace_refs (MG_REFERER (mg_query), repl);

	/* copy sub queries */
	list = orig->priv->sub_queries;
	while (list) {
		MgQuery *copy = MG_QUERY (mg_query_new_copy (MG_QUERY (list->data), repl));
		mg_referer_replace_refs (MG_REFERER (copy), repl);
		mg_query_add_sub_query (mg_query, copy);
		g_object_unref (G_OBJECT (copy));
		
		list = g_slist_next (list);
	}

	/* copy param sources */
	list = orig->priv->param_sources;
	while (list) {
		MgQuery *copy = MG_QUERY (mg_query_new_copy (MG_QUERY (list->data), repl));
		mg_referer_replace_refs (MG_REFERER (copy), repl);
		mg_query_add_param_source (mg_query, copy);
		g_object_unref (G_OBJECT (copy));
		
		list = g_slist_next (list);
	}

	/* copy targets */
	list = orig->priv->targets;
	while (list) {
		guint id;
		MgTarget *target = MG_TARGET (mg_target_new_copy (MG_TARGET (list->data)));

		mg_referer_replace_refs (MG_REFERER (target), repl);
		mg_query_add_target (mg_query, target, NULL);
		g_object_get (G_OBJECT (mg_query), "target_serial", &id, NULL);
		mg_base_set_id (MG_BASE (target), id);

		g_object_unref (G_OBJECT (target));
		g_hash_table_insert (repl, list->data, target);
		list = g_slist_next (list);
	}

	/* copy fields */
	list = orig->priv->fields;
	while (list) {
		guint id;
		MgQfield *qf = MG_QFIELD (mg_qfield_new_copy (MG_QFIELD (list->data)));
		mg_referer_replace_refs (MG_REFERER (qf), repl);
		mg_query_add_field (MG_ENTITY (mg_query), MG_FIELD (qf));
		g_object_get (G_OBJECT (mg_query), "field_serial", &id, NULL);
		mg_base_set_id (MG_BASE (qf), id);

		g_object_unref (G_OBJECT (qf));
		g_hash_table_insert (repl, list->data, qf);
		list = g_slist_next (list);
	}

	/* copy joins */
	list = orig->priv->joins_flat;
	while (list) {
		MgJoin *join = MG_JOIN (mg_join_new_copy (MG_JOIN (list->data)));
		mg_referer_replace_refs (MG_REFERER (join), repl);
		mg_query_add_join (mg_query, join);
		g_object_unref (G_OBJECT (join));
		g_hash_table_insert (repl, list->data, join);
		list = g_slist_next (list);
	}

	/* copy condition */
	if (orig->priv->cond) {
		MgCondition *cond;
		guint id;

		cond = MG_CONDITION (mg_condition_new_copy (orig->priv->cond, repl));
		g_object_get (G_OBJECT (mg_query), "cond_serial", &id, NULL);
		mg_base_set_id (MG_BASE (cond), id);
		mg_query_set_condition (mg_query, cond);

		g_object_unref (G_OBJECT (cond));
		g_hash_table_insert (repl, orig->priv->cond, cond);
	}

	/* copy ORDER BY fields list */
	order_pos = 0;
	list = orig->priv->fields_order_by;
	while (list) {
		MgQfield *field;
		gboolean asc;

		asc = g_object_get_data (G_OBJECT (list->data), "order_by_asc") ? TRUE : FALSE;
		field = g_hash_table_lookup (repl, list->data);
		mg_query_set_order_by_field (mg_query, field, order_pos, asc);

		order_pos++;
		list = g_slist_next (list);
	}
		
	/* Another loop to make sure all the references have been replaced */
	mg_query_replace_refs (MG_REFERER (mg_query), repl);

	if (!replacements)
		g_hash_table_destroy (repl);
	return obj;	
}


static void
mg_query_dispose (GObject *object)
{
	MgQuery *mg_query;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_QUERY (object));

	mg_query = MG_QUERY (object);
	if (mg_query->priv) {
		mg_base_nullify_check (MG_BASE (object));
		
		/* order by list */
		if (mg_query->priv->fields_order_by) {
			g_slist_free (mg_query->priv->fields_order_by);
			mg_query->priv->fields_order_by = NULL;
		}

		/* parent query */
		if (mg_query->priv->parent_query)
			change_parent_query (mg_query, NULL);
		
		/* condition */
		if (mg_query->priv->cond)
			mg_base_nullify (MG_BASE (mg_query->priv->cond));

		/* parameter sources */
		while (mg_query->priv->param_sources) 
			mg_base_nullify (MG_BASE (mg_query->priv->param_sources->data));

		/* sub queries */
		while (mg_query->priv->sub_queries) 
			mg_base_nullify (MG_BASE (mg_query->priv->sub_queries->data));

		/* joins */
		while (mg_query->priv->joins_flat) 
			mg_base_nullify (MG_BASE (mg_query->priv->joins_flat->data));

		/* fields */
		while (mg_query->priv->fields) 
			mg_base_nullify (MG_BASE (mg_query->priv->fields->data));
		
		/* targets */
		while (mg_query->priv->targets) 
			mg_base_nullify (MG_BASE (mg_query->priv->targets->data));
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_query_finalize (GObject *object)
{
	MgQuery *mg_query;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_QUERY (object));

	mg_query = MG_QUERY (object);
	if (mg_query->priv) {
		if (mg_query->priv->sql)
			g_free (mg_query->priv->sql);
		g_free (mg_query->priv);
		mg_query->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_query_set_property (GObject              *object,
		       guint                 param_id,
		       const GValue         *value,
		       GParamSpec           *pspec)
{
	MgQuery *mg_query;

	mg_query = MG_QUERY (object);
	if (mg_query->priv) {
		switch (param_id) {
		case PROP_SERIAL_TARGET:
			break;
		}
	}
}

static void
mg_query_get_property (GObject              *object,
		       guint                 param_id,
		       GValue               *value,
		       GParamSpec           *pspec)
{
	MgQuery *mg_query;
	mg_query = MG_QUERY (object);
	
	if (mg_query->priv) {
		switch (param_id) {
		case PROP_SERIAL_TARGET:
			g_value_set_uint (value, mg_query->priv->serial_target++);
			break;
		case PROP_SERIAL_FIELD:
			g_value_set_uint (value, mg_query->priv->serial_field++);
			break;
		case PROP_SERIAL_COND:
			g_value_set_uint (value, mg_query->priv->serial_cond++);
			break;
		case PROP_REALLY_ALL_FIELDS:
			g_value_set_pointer (value, mg_query->priv->fields);
			break;
		}	
	}
}


static void
id_target_changed_cb (MgTarget *target, MgQuery *query)
{
	if (query->priv->serial_target <= mg_base_get_id (MG_BASE (target)))
		query->priv->serial_target = mg_base_get_id (MG_BASE (target)) + 1;
}

static void
id_field_changed_cb (MgQfield *field, MgQuery *query)
{
	if (query->priv->serial_field <= mg_base_get_id (MG_BASE (field)))
		query->priv->serial_field = mg_base_get_id (MG_BASE (field)) + 1;
}

static void
id_cond_changed_cb (MgCondition *cond, MgQuery *query)
{
	if (query->priv->serial_cond <= mg_base_get_id (MG_BASE (cond)))
		query->priv->serial_cond = mg_base_get_id (MG_BASE (cond)) + 1;
}

/**
 * mg_query_set_query_type
 * @query: a #MgQuery object
 * @type: the new type of query
 *
 * Sets the type of @query
 */
void
mg_query_set_query_type (MgQuery *query, MgQueryType type)
{
	g_return_if_fail (query && IS_MG_QUERY (query));
	g_return_if_fail (query->priv);
	
	if (query->priv->query_type != type) {
		if ((type == MG_QUERY_TYPE_SQL) ||
		    (query->priv->query_type == MG_QUERY_TYPE_SQL)) {
			/* FIXME: remove all the targets, joins, etc to make a clean room */
			TO_IMPLEMENT;
		}
		    
		query->priv->query_type = type;
		mg_base_changed (MG_BASE (query));
	}
}

/**
 * mg_query_get_query_type
 * @query: a #MgQuery object
 *
 * Get the type of a query
 *
 * Returns: the type of @query
 */
MgQueryType
mg_query_get_query_type (MgQuery *query)
{
	g_return_val_if_fail (query && IS_MG_QUERY (query), MG_QUERY_TYPE_SELECT);
	g_return_val_if_fail (query->priv, MG_QUERY_TYPE_SELECT);
	
	return query->priv->query_type;
}

/**
 * mg_query_is_select_query
 * @query: a # MgQuery object
 *
 * Tells if @query is a SELECTION query (a simple SELECT, UNION, INTERSECT or EXCEPT); pure SQL
 * queries are not handled and will always return FALSE.
 *
 * Returns: TRUE if @query is a SELECT query
 */
gboolean
mg_query_is_select_query (MgQuery *query)
{
	g_return_val_if_fail (query && IS_MG_QUERY (query), FALSE);
	g_return_val_if_fail (query->priv, FALSE);

	if ((query->priv->query_type == MG_QUERY_TYPE_SELECT) ||
	    (query->priv->query_type == MG_QUERY_TYPE_UNION) ||
	    (query->priv->query_type == MG_QUERY_TYPE_INTERSECT) ||
	    (query->priv->query_type == MG_QUERY_TYPE_EXCEPT))
		return TRUE;
	else
		return FALSE;
}

/**
 * mg_query_is_modif_query
 * @query: a # MgQuery object
 *
 * Tells if @query is a modification query (a simple UPDATE, DELETE, INSERT).; pure SQL
 * queries are not handled and will always return FALSE.
 *
 * Returns: TRUE if @query is a modification query
 */
gboolean
mg_query_is_modif_query (MgQuery *query)
{
	g_return_val_if_fail (query && IS_MG_QUERY (query), FALSE);
	g_return_val_if_fail (query->priv, FALSE);

	if ((query->priv->query_type == MG_QUERY_TYPE_INSERT) ||
	    (query->priv->query_type == MG_QUERY_TYPE_DELETE) ||
	    (query->priv->query_type == MG_QUERY_TYPE_UPDATE))
		return TRUE;
	else
		return FALSE;
}


/**
* mg_query_sql_set_text
* @query: a # MgQuery object
* @sql: the SQL query
*
* Sets @query to be a SQL query and sets it to @sql
*/
void
mg_query_sql_set_text (MgQuery *query, const gchar *sql)
{
	g_return_if_fail (query && IS_MG_QUERY (query));
	g_return_if_fail (query->priv);

	mg_query_set_query_type (query, MG_QUERY_TYPE_SQL);
	if (query->priv->sql && sql && *sql) {
		if (strcmp (query->priv->sql, sql)) {
			g_free (query->priv->sql);
			query->priv->sql = g_strdup (sql);
			mg_base_changed (MG_BASE (query));
		}
	}
	else {
		if (query->priv->sql || sql) {
			if (query->priv->sql) {
				g_free (query->priv->sql);
				query->priv->sql = NULL;
			}
			
			if (sql)
				query->priv->sql = g_strdup (sql);
			mg_base_changed (MG_BASE (query));
		}
	}
}

const gchar *
mg_query_sql_get_text (MgQuery *query)
{
	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (query->priv, NULL);

	return query->priv->sql;
}





/**
 * mg_query_get_parent_query
 * @query: a #MgQuery object
 *
 * Get the parent query of @query
 *
 * Returns: the parent query, or NULL if @query does not have any parent
 */
MgQuery *
mg_query_get_parent_query (MgQuery *query)
{
	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (query->priv, NULL);

	return query->priv->parent_query;
}


/**
 * mg_query_get_field_by_ref_field
 * @query: a #MgQuery object
 * @ref_field: a #MgField object
 *
 * Finds the first #MgQfield object in @query which represents @ref_field.
 * The returned object will be a #MgQfField object which represents @ref_field.
 *
 * Returns: a #MgQfField object or %NULL
 */
MgQfield *
mg_query_get_field_by_ref_field (MgQuery *query, MgField *ref_field)
{
	MgQfield *field = NULL;
	GSList *list;

	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (query->priv, NULL);

	list = query->priv->fields;
	while (list && !field) {
		if (IS_MG_QF_FIELD (list->data) &&
		    (mg_qf_field_get_ref_field (MG_QF_FIELD (list->data)) == ref_field))
			field = MG_QFIELD (list->data);
		list = g_slist_next (list);
	}

	return field;
}

/**
 * mg_query_get_sub_queries
 * @query: a #MgQuery object
 *
 * Get a list of all the sub-queries managed by @query
 *
 * Returns: a new list of the sub-queries
 */
GSList *
mg_query_get_sub_queries (MgQuery *query)
{
	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (query->priv, NULL);

	if (query->priv->sub_queries)
		return g_slist_copy (query->priv->sub_queries);
	else
		return NULL;
}

/**
 * mg_query_add_param_source
 * @query: a #MgQuery object
 * @param_source: a #MgQuery object
 *
 * Tells @query that @param_source is a query which potentially will constraint the possible values
 * of one or more of @query's parameters. This implies that @query keeps a reference on @param_source.
 */
void 
mg_query_add_param_source (MgQuery *query, MgQuery *param_source)
{
	g_return_if_fail (query && IS_MG_QUERY (query));
        g_return_if_fail (query->priv);
	g_return_if_fail (param_source && IS_MG_QUERY (param_source));
	g_return_if_fail (param_source->priv);
        g_return_if_fail (!g_slist_find (query->priv->param_sources, param_source));

	query->priv->param_sources = g_slist_append (query->priv->param_sources, param_source);
	change_parent_query (param_source, NULL);
	g_object_ref (G_OBJECT (param_source));

	g_signal_connect (G_OBJECT (param_source), "nullified",
                          G_CALLBACK (nullified_param_source_cb), query);
}

static void
nullified_param_source_cb (MgQuery *param_source, MgQuery *query)
{
	g_assert (g_slist_find (query->priv->param_sources, param_source));

        query->priv->param_sources = g_slist_remove (query->priv->param_sources, param_source);
        g_signal_handlers_disconnect_by_func (G_OBJECT (param_source),
                                              G_CALLBACK (nullified_param_source_cb), query);

        g_object_unref (param_source);
}

/**
 * mg_query_del_param_source
 * @query: a #MgQuery object
 * @param_source: a #MgQuery object
 *
 * Tells @query that it should no longer take care of @param_source.
 * The parameters which depend on @param_source will still depend on it, though.
 */
void
mg_query_del_param_source (MgQuery *query, MgQuery *param_source)
{
	g_return_if_fail (query && IS_MG_QUERY (query));
        g_return_if_fail (query->priv);
	g_return_if_fail (param_source && IS_MG_QUERY (param_source));
	g_return_if_fail (g_slist_find (query->priv->param_sources, param_source));

	nullified_param_source_cb (param_source, query);
}

/**
 * mg_query_get_param_sources
 * @query: a #MgQuery object
 *
 * Get a list of the parameter source queries that are references as such by @query.
 *
 * Return: the list of #MgQuery objects
 */
const GSList *
mg_query_get_param_sources (MgQuery *query)
{
	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
        g_return_val_if_fail (query->priv, NULL);

	return query->priv->param_sources;
}


/**
 * mg_query_add_sub_query
 * @query: a #MgQuery object
 * @sub_query: a #MgQuery object
 *
 * Add @sub_query to @query. Sub queries are managed by their parent query, and as such they
 * are destroyed when their parent query is destroyed.
 */
void
mg_query_add_sub_query (MgQuery *query, MgQuery *sub_query)
{
	g_return_if_fail (query && IS_MG_QUERY (query));
        g_return_if_fail (query->priv);
	g_return_if_fail (sub_query && IS_MG_QUERY (sub_query));
	g_return_if_fail (sub_query->priv);
        g_return_if_fail (!g_slist_find (query->priv->sub_queries, sub_query));

	query->priv->sub_queries = g_slist_append (query->priv->sub_queries, sub_query);
	change_parent_query (sub_query, query);
	g_object_ref (G_OBJECT (sub_query));

	g_signal_connect (G_OBJECT (sub_query), "nullified",
                          G_CALLBACK (nullified_sub_query_cb), query);
        g_signal_connect (G_OBJECT (sub_query), "changed",
                          G_CALLBACK (changed_sub_query_cb), query);

#ifdef debug_signal
        g_print (">> 'SUB_QUERY_ADDED' from %s\n", __FUNCTION__);
#endif
        g_signal_emit_by_name (G_OBJECT (query), "sub_query_added", sub_query);
#ifdef debug_signal
        g_print ("<< 'SUB_QUERY_ADDED' from %s\n", __FUNCTION__);
#endif	
}

static void
nullified_sub_query_cb (MgQuery *sub_query, MgQuery *query)
{
	g_assert (g_slist_find (query->priv->sub_queries, sub_query));

        query->priv->sub_queries = g_slist_remove (query->priv->sub_queries, sub_query);
        g_signal_handlers_disconnect_by_func (G_OBJECT (sub_query),
                                              G_CALLBACK (nullified_sub_query_cb), query);
        g_signal_handlers_disconnect_by_func (G_OBJECT (sub_query),
                                              G_CALLBACK (changed_sub_query_cb), query);

#ifdef debug_signal
        g_print (">> 'SUB_QUERY_REMOVED' from %s\n", __FUNCTION__);
#endif
        g_signal_emit_by_name (G_OBJECT (query), "sub_query_removed", sub_query);
#ifdef debug_signal
        g_print ("<< 'SUB_QUERY_REMOVED' from %s\n", __FUNCTION__);
#endif

        g_object_unref (sub_query);
}

static void
changed_sub_query_cb (MgQuery *sub_query, MgQuery *query)
{
#ifdef debug_signal
        g_print (">> 'SUB_QUERY_UPDATED' from %s\n", __FUNCTION__);
#endif
        g_signal_emit_by_name (G_OBJECT (query), "sub_query_updated", sub_query);
#ifdef debug_signal
        g_print ("<< 'SUB_QUERY_UPDATED' from %s\n", __FUNCTION__);
#endif
}

/**
 * mg_query_del_sub_query
 * @query: a #MgQuery object
 * @sub_query: a #MgQuery object
 *
 * Removes @sub_query from @query. @sub_query MUST be present within @query.
 */
void
mg_query_del_sub_query (MgQuery *query, MgQuery *sub_query)
{
	g_return_if_fail (query && IS_MG_QUERY (query));
        g_return_if_fail (query->priv);
	g_return_if_fail (sub_query && IS_MG_QUERY (sub_query));
	g_return_if_fail (g_slist_find (query->priv->sub_queries, sub_query));

	nullified_sub_query_cb (sub_query, query);
}

static void
change_parent_query (MgQuery *query, MgQuery *parent_query)
{
	MgConf *conf;
	g_return_if_fail (query && IS_MG_QUERY (query));
	g_return_if_fail (query->priv);

	conf = mg_base_get_conf (MG_BASE (query));

	/* if there was a parent query, then we break that link and give the query to the MgConf */
	if (query->priv->parent_query) {
		/* REM: we don't remove 'query' from 'parent_query' since that already has been done */
		g_signal_handlers_disconnect_by_func (G_OBJECT (query->priv->parent_query),
						      G_CALLBACK (nullified_parent_query), query);
		query->priv->parent_query = NULL;
	}
	
	if (parent_query) {
		g_return_if_fail (IS_MG_QUERY (parent_query));
		query->priv->parent_query = parent_query;
		g_signal_connect (G_OBJECT (parent_query), "nullified",
				  G_CALLBACK (nullified_parent_query), query);
	}
}

static void
nullified_parent_query (MgQuery *parent_query, MgQuery *query)
{
	mg_base_nullify (MG_BASE (query));
}







/**
 * mg_query_get_targets
 * @query: a #MgQuery object
 *
 * Get a list of all the targets used in @query
 *
 * Returns: a new list of the targets
 */
GSList *
mg_query_get_targets (MgQuery *query)
{
	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (query->priv, NULL);

	if (query->priv->targets)
		return g_slist_copy (query->priv->targets);
	else
		return NULL;
}

static void mg_query_assign_targets_aliases (MgQuery *query);

/**
 * mg_query_add_target
 * @query: a #MgQuery object
 * @target: a #MgTarget to add to @query
 * @error: location to store error, or %NULL
 *
 * Adds a target to @query. A target represents a entity (it can actually be a table,
 * a view, or another query) which @query will use. 
 *
 * For a SELECT query, the targets appear
 * after the FROM clause. The targets can be joined two by two using #MgJoin objects
 *
 * For UPDATE, DELETE or INSERT queries, there can be only ONE #MgTarget object which is
 * the one where the data modifications are performed.
 *
 * For UNION and INTERSECT queries, there is no possible #MgTarget object.
 *
 * Returns: TRUE if no error occured
 */
gboolean
mg_query_add_target (MgQuery *query, MgTarget *target, GError **error)
{
	MgEntity *ent;
	g_return_val_if_fail (query && IS_MG_QUERY (query), FALSE);
        g_return_val_if_fail (query->priv, FALSE);
        g_return_val_if_fail (target && IS_MG_TARGET (target), FALSE);
        g_return_val_if_fail (!g_slist_find (query->priv->targets, target), FALSE);
	g_return_val_if_fail (mg_target_get_query (target) == query, FALSE);

	/* if target represents another MgQuery, then make sure that other query is a sub query of @query */
	ent = mg_target_get_represented_entity (target);
	if (ent && IS_MG_QUERY (ent)) {
		if ((mg_query_get_parent_query (MG_QUERY (ent)) != query) ||
		    !g_slist_find (query->priv->sub_queries, ent)) {
			g_set_error (error,
				     MG_QUERY_ERROR,
				     MG_QUERY_TARGETS_ERROR,
				     _("The query represented by a target must be a sub query of the current query"));
			return FALSE;
		}
	}

	/* specific tests */
	switch (query->priv->query_type) {
	case MG_QUERY_TYPE_INSERT:
	case MG_QUERY_TYPE_UPDATE:
	case MG_QUERY_TYPE_DELETE:
		/* if there is already one target, then refuse to add another one */
		if (query->priv->targets) {
			g_set_error (error,
				     MG_QUERY_ERROR,
				     MG_QUERY_TARGETS_ERROR,
				     _("Queries which update data can only have one target"));
			return FALSE;
		}
		break;
	case MG_QUERY_TYPE_UNION:
	case MG_QUERY_TYPE_INTERSECT:
	case MG_QUERY_TYPE_EXCEPT:
		g_set_error (error,
			     MG_QUERY_ERROR,
			     MG_QUERY_TARGETS_ERROR,
			     _("Aggregation queries can't have any target, only sub queries"));
		return FALSE;
		break;
	default:
	case MG_QUERY_TYPE_SELECT:
	case MG_QUERY_TYPE_SQL:
		/* no specific test to be done */
		break;
	}

	query->priv->targets = g_slist_append (query->priv->targets, target);
	g_object_ref (G_OBJECT (target));
	g_signal_connect (G_OBJECT (target), "nullified",
                          G_CALLBACK (nullified_target_cb), query);
        g_signal_connect (G_OBJECT (target), "changed",
                          G_CALLBACK (changed_target_cb), query);
        g_signal_connect (G_OBJECT (target), "id_changed",
                          G_CALLBACK (id_target_changed_cb), query);

	mg_query_assign_targets_aliases (query);

#ifdef debug_signal
        g_print (">> 'TARGET_ADDED' from %s\n", __FUNCTION__);
#endif
        g_signal_emit_by_name (G_OBJECT (query), "target_added", target);
#ifdef debug_signal
        g_print ("<< 'TARGET_ADDED' from %s\n", __FUNCTION__);
#endif
	return TRUE;
}

static void
nullified_target_cb (MgTarget *target, MgQuery *query)
{
	/* REM: when a MgTarget object is nullified, the MgJoin object using it are 
	   also nullified, so we don't need to take care of them here */
	g_assert (g_slist_find (query->priv->targets, target));

        query->priv->targets = g_slist_remove (query->priv->targets, target);
        g_signal_handlers_disconnect_by_func (G_OBJECT (target),
                                              G_CALLBACK (nullified_target_cb), query);
        g_signal_handlers_disconnect_by_func (G_OBJECT (target),
                                              G_CALLBACK (changed_target_cb), query);
        g_signal_handlers_disconnect_by_func (G_OBJECT (target),
                                              G_CALLBACK (id_target_changed_cb), query);

#ifdef debug_signal
        g_print (">> 'TARGET_REMOVED' from %s\n", __FUNCTION__);
#endif
        g_signal_emit_by_name (G_OBJECT (query), "target_removed", target);
#ifdef debug_signal
        g_print ("<< 'TARGET_REMOVED' from %s\n", __FUNCTION__);
#endif

        g_object_unref (target);
	mg_query_assign_targets_aliases (query);
}

static void
changed_target_cb (MgTarget *target, MgQuery *query)
{
#ifdef debug_signal
        g_print (">> 'TARGET_UPDATED' from %s\n", __FUNCTION__);
#endif
        g_signal_emit_by_name (G_OBJECT (query), "target_updated", target);
#ifdef debug_signal
        g_print ("<< 'TARGET_UPDATED' from %s\n", __FUNCTION__);
#endif
}

static void
mg_query_assign_targets_aliases (MgQuery *query)
{
	/* FIXME: add a targets assigning policy here; otherwise targets assign themselves the aliases as T<id>.
	 * The problem is with targets referencing other queries where there is a potential alias clash if the policy
	 * does not take care of the other queries
	 */
}

/**
 * mg_query_del_target
 * @query: a #MgQuery object
 * @target: a #MgTarget object
 *
 * Removes @target from @query. @target MUST be present within @query. Warning:
 * All the joins and fields which depended on @target are also removed.
 */
void
mg_query_del_target (MgQuery *query, MgTarget *target)
{
	g_return_if_fail (query && IS_MG_QUERY (query));
        g_return_if_fail (query->priv);
	g_return_if_fail (target && IS_MG_TARGET (target));
	g_return_if_fail (g_slist_find (query->priv->targets, target));

	nullified_target_cb (target, query);
}


/**
 * mg_query_get_target_by_xml_id
 * @query: a #MgQuery object
 * @xml_id: the XML Id of the requested #MgTarget object
 *
 * Get a pointer to a #MgTarget (which must be within @query) using
 * its XML Id
 *
 * Returns: the #MgTarget object, or NULL if not found
 */
MgTarget *
mg_query_get_target_by_xml_id (MgQuery *query, const gchar *xml_id)
{
	MgTarget *target = NULL;
	GSList *list;
	gchar *str;

	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (query->priv, NULL);

	list = query->priv->targets;
	while (list && !target) {
		str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
		if (!strcmp (str, xml_id))
			target = MG_TARGET (list->data);
		g_free (str);
		list = g_slist_next (list);
	}

	return target;
}

/**
 * mg_query_get_joins
 * @query: a #MgQuery object
 *
 * Get a list of all the joins used in @query
 *
 * Returns: a new list of the joins
 */
GSList *
mg_query_get_joins (MgQuery *query)
{
	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (query->priv, NULL);

	if (query->priv->joins_flat)
		return g_slist_copy (query->priv->joins_flat);
	else
		return NULL;
}

static gboolean mg_query_are_joins_active (MgQuery *query);
#ifdef debug
static void joins_pack_dump (MgQuery *query);
#endif
static gboolean joins_pack_add_join (MgQuery *query, MgJoin *join);
static void joins_pack_del_join (MgQuery *query, MgJoin *join);

/**
 * mg_query_add_join
 * @query: a #MgQuery object
 * @join : a #MgJoin object
 *
 * Add a join to @query. A join is defined by the two #MgTarget objects it joins and by
 * a join condition which MUST ONLY make use of fields of the two entities represented by the
 * targets.
 *
 * For any given couple of #MgTarget objects, there can exist ONLY ONE #MgJoin which joins the
 * two.
 *
 * Returns: TRUE on success, and FALSE otherwise
 */
gboolean
mg_query_add_join (MgQuery *query, MgJoin *join)
{
	GSList *joins;
	MgTarget *t1, *t2, *lt1, *lt2;
	gboolean already_exists = FALSE;

	g_return_val_if_fail (query && IS_MG_QUERY (query), FALSE);
        g_return_val_if_fail (query->priv, FALSE);
        g_return_val_if_fail (join && IS_MG_JOIN (join), FALSE);
        g_return_val_if_fail (!g_slist_find (query->priv->joins_flat, join), FALSE);
	g_return_val_if_fail (mg_join_get_query (join) == query, FALSE);
	g_return_val_if_fail (mg_referer_is_active (MG_REFERER (join)), FALSE);
	g_return_val_if_fail (mg_query_are_joins_active (query), FALSE);

	/* make sure there is not yet another join for the couple of #MgTarget objects
	   used by 'join' */
	t1 = mg_join_get_target_1 (join);
	t2 = mg_join_get_target_2 (join);

	joins = query->priv->joins_flat;
	while (joins && !already_exists) {
		lt1 = mg_join_get_target_1 (MG_JOIN (joins->data));
		lt2 = mg_join_get_target_2 (MG_JOIN (joins->data));
		if (((lt1 == t1) && (lt2 == t2)) ||
		    ((lt1 == t2) && (lt2 == t1)))
			already_exists = TRUE;

		joins = g_slist_next (joins);
	}
	g_return_val_if_fail (!already_exists, FALSE);

	g_return_val_if_fail (joins_pack_add_join (query, join), FALSE);
	query->priv->joins_flat = g_slist_append (query->priv->joins_flat, join);
	g_object_ref (G_OBJECT (join));
	g_signal_connect (G_OBJECT (join), "nullified",
                          G_CALLBACK (nullified_join_cb), query);
        g_signal_connect (G_OBJECT (join), "changed",
                          G_CALLBACK (changed_join_cb), query);

#ifdef debug_signal
        g_print (">> 'JOIN_ADDED' from %s\n", __FUNCTION__);
#endif
        g_signal_emit_by_name (G_OBJECT (query), "join_added", join);
#ifdef debug_signal
        g_print ("<< 'JOIN_ADDED' from %s\n", __FUNCTION__);
#endif	
	return TRUE;
}

static void
nullified_join_cb (MgJoin *join, MgQuery *query)
{
	g_assert (g_slist_find (query->priv->joins_flat, join));

        query->priv->joins_flat = g_slist_remove (query->priv->joins_flat, join);
	joins_pack_del_join  (query, join);

        g_signal_handlers_disconnect_by_func (G_OBJECT (join),
                                              G_CALLBACK (nullified_join_cb), query);
        g_signal_handlers_disconnect_by_func (G_OBJECT (join),
                                              G_CALLBACK (changed_join_cb), query);

#ifdef debug_signal
        g_print (">> 'JOIN_REMOVED' from %s\n", __FUNCTION__);
#endif
        g_signal_emit_by_name (G_OBJECT (query), "join_removed", join);
#ifdef debug_signal
        g_print ("<< 'JOIN_REMOVED' from %s\n", __FUNCTION__);
#endif

        g_object_unref (join);
}

static void
changed_join_cb (MgJoin *join, MgQuery *query)
{
#ifdef debug_signal
        g_print (">> 'JOIN_UPDATED' from %s\n", __FUNCTION__);
#endif
        g_signal_emit_by_name (G_OBJECT (query), "join_updated", join);
#ifdef debug_signal
        g_print ("<< 'JOIN_UPDATED' from %s\n", __FUNCTION__);
#endif
}

/**
 * mg_query_del_join
 * @query: a #MgQuery object
 * @join: a #MgJoin object
 *
 * Removes @join from @query. @join MUST be present within @query.
 */
void
mg_query_del_join (MgQuery *query, MgJoin *join)
{
	g_return_if_fail (query && IS_MG_QUERY (query));
        g_return_if_fail (query->priv);
	g_return_if_fail (join && IS_MG_JOIN (join));
	g_return_if_fail (g_slist_find (query->priv->joins_flat, join));

	nullified_join_cb (join, query);
}

#ifdef debug
static void
joins_pack_dump (MgQuery *query)
{
	GSList *packs, *list;
	gchar *xml;

	packs = query->priv->joins_pack;
	while (packs) {
		g_print ("=== PACK ===\n");
		list = JOINS_PACK (packs->data)->targets;
		g_print ("TARGETS: ");
		while (list) {
			xml = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
			g_print ("%s ", xml);
			g_free (xml);
			list = g_slist_next (list);
		}
		g_print ("\nJOINS: ");
		list = JOINS_PACK (packs->data)->joins;
		while (list) {
			g_print ("%p ", list->data);
			list = g_slist_next (list);
		}
		g_print ("\n");
		packs = g_slist_next (packs);
	}
}
#endif

/*
 * Adds a join in the joins pack structure, which is another way of storing joins
 * within the query.
 * It is assumed that the join itself is not yet present in the list of joins
 *
 * Returns: TRUE if no error
 */
static gboolean
joins_pack_add_join (MgQuery *query, MgJoin *join)
{
	GSList *pack_list;
	JoinsPack *pack;
	JoinsPack *pack1 = NULL, *pack2 = NULL;
	MgTarget *t1, *t2;

	/* we want an active join only */
	g_return_val_if_fail (mg_referer_activate (MG_REFERER (join)), FALSE);
	t1 = mg_join_get_target_1 (join);
	t2 = mg_join_get_target_2 (join);

	/* try fo identify existing joins packings in which the new join can go */
	pack_list = query->priv->joins_pack;
	while (pack_list && !pack1 && !pack2) {
		pack = JOINS_PACK (pack_list->data);
		if (!pack1) {
			if (g_slist_find (pack->targets, t2)) {
				MgTarget *tmp;
				mg_join_swap_targets (join);
				tmp = t1;
				t1 = t2;
				t2 = tmp;
			}

			if (g_slist_find (pack->targets, t1)) 
				pack1 = pack;
		}
		else 
			if (g_slist_find (pack->targets, t2)) 
				pack2 = pack;
		
		pack_list = g_slist_next (pack_list);
	}

	/* updating the packings */
	if (!pack1) {
		/* a new JoinsPack is necessary */
		pack = g_new0 (JoinsPack, 1);
		pack->targets = g_slist_append (NULL, t1);
		pack->targets = g_slist_append (pack->targets, t2);
		pack->joins = g_slist_append (NULL, join);

		query->priv->joins_pack = g_slist_append (query->priv->joins_pack, pack);
	}
	else {
		/* append join to the identified pack1 */
		pack1->joins = g_slist_append (pack1->joins, join);
		pack1->targets = g_slist_append (pack1->targets, t2);

		if (pack2 && (pack2 != pack1)) {
			/* merge pack2 into pack1 */
			GSList *joins;
			GSList *targets;
			MgJoin *cjoin;
			
			/* reodering the joins to start with t2 */
			targets = g_slist_append (NULL, t2);
			while (pack2->joins) {
				joins = pack2->joins;
				cjoin = NULL;

				while (joins && !cjoin) {
					t1 = mg_join_get_target_1 (MG_JOIN (joins->data));
					t2 = mg_join_get_target_2 (MG_JOIN (joins->data));

					if (g_slist_find (targets, t1)) {
						cjoin = MG_JOIN (joins->data);
						targets = g_slist_append (targets, t2);
						pack1->targets = g_slist_append (pack1->targets, t2);
					}
					else {
						if (g_slist_find (targets, t2)) {
							cjoin = MG_JOIN (joins->data);
							mg_join_swap_targets (cjoin);
							targets = g_slist_append (targets, t1);
							pack1->targets = g_slist_append (pack1->targets, t1);
						}
					}
				}

				g_assert (cjoin);
				pack2->joins = g_slist_remove (pack2->joins, cjoin);
				pack1->joins = g_slist_append (pack1->joins, cjoin);
			}
			
			g_slist_free (targets);
			/* getting rid of pack2 */
			query->priv->joins_pack = g_slist_remove (query->priv->joins_pack, pack2);
			g_slist_free (pack2->targets);
			g_free (pack2);
		}
	}

	return TRUE;
}

/* 
 * Removes a join from the joins packing structures.
 * It is assumed that the join itself IS present in the list of joins
 */
static void
joins_pack_del_join (MgQuery *query, MgJoin *join)
{
	JoinsPack *joinpack = NULL, *pack;
	GSList *pack_list, *list;

	/* identifying the pack in which join is */
	pack_list = query->priv->joins_pack;
	while (pack_list && !joinpack) {
		pack = JOINS_PACK (pack_list->data);
		if (g_slist_find (pack->joins, join)) 
			joinpack = pack;
		pack_list = g_slist_next (pack_list);
	}
	g_assert (joinpack);

	/* removing the pack and adding again all the joins within that pack */
	query->priv->joins_pack = g_slist_remove (query->priv->joins_pack, joinpack);

	list = joinpack->joins;
	while (list) {
		if (MG_JOIN (list->data) != join)
			joins_pack_add_join (query, MG_JOIN (list->data));

		list = g_slist_next (list);
	}
	g_slist_free (joinpack->targets);
	g_slist_free (joinpack->joins);
	g_free (joinpack);
}






/**
 * mg_query_get_condition
 * @query: a #MgQuery object
 *
 * Get the query's associated condition
 *
 * Returns: the #MgCondition object
 */
MgCondition *
mg_query_get_condition (MgQuery *query)
{
	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (query->priv, NULL);
	
	return query->priv->cond;
}

/**
 * mg_query_set_condition
 * @query: a #MgQuery object
 * @cond: a #MgCondition object
 *
 * Sets the query's associated condition
 */
void
mg_query_set_condition (MgQuery *query, MgCondition *cond)
{
	g_return_if_fail (query && IS_MG_QUERY (query));
	g_return_if_fail (query->priv);
	g_return_if_fail (cond && IS_MG_CONDITION (cond));
	
	if (query->priv->cond) 
		nullified_cond_cb (query->priv->cond, query);

	query->priv->cond = cond;
	g_signal_connect (G_OBJECT (cond), "nullified",
			  G_CALLBACK (nullified_cond_cb), query);
	g_signal_connect (G_OBJECT (cond), "id_changed",
			  G_CALLBACK (id_cond_changed_cb), query);

	g_object_ref (G_OBJECT (cond));
}

static void
nullified_cond_cb (MgCondition *cond, MgQuery *query)
{
	g_assert (query->priv->cond == cond);
	g_signal_handlers_disconnect_by_func (G_OBJECT (cond),
					      G_CALLBACK (nullified_cond_cb), query);
	g_signal_handlers_disconnect_by_func (G_OBJECT (cond),
					      G_CALLBACK (id_cond_changed_cb), query);
	query->priv->cond = NULL;
	g_object_unref (G_OBJECT (cond));
}

#ifdef debug
static void
mg_query_dump (MgQuery *query, guint offset)
{
	gchar *str;
        guint i;
	GSList *list;
	GError *error = NULL;
	
	g_return_if_fail (query && IS_MG_QUERY (query));

        /* string for the offset */
        str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;

        /* dump */
        if (query->priv) {
		gchar *sql;

                g_print ("%s" D_COL_H1 "MgQuery" D_COL_NOR " %p (type=%d, QU%d) %s",
                         str, query, query->priv->query_type, mg_base_get_id (MG_BASE (query)),
			 mg_base_get_name (MG_BASE (query)));
		if (mg_query_is_active (MG_REFERER (query)))
			g_print (": Active\n");
		else
			g_print (D_COL_ERR ": Non active\n" D_COL_NOR);
		
		/* targets */
		if (query->priv->targets)
			g_print ("%sTargets:\n", str);
		else
			g_print ("%sNo target defined.\n", str);
		list = query->priv->targets;
		while (list) {
			mg_base_dump (MG_BASE (list->data), offset+5);
			list = g_slist_next (list);
		}

		/* fields */
		if (query->priv->fields)
			g_print ("%sFields:\n", str);
		else
			g_print ("%sNo field defined.\n", str);
		list = query->priv->fields;
		while (list) {
			mg_base_dump (MG_BASE (list->data), offset+5);
			list = g_slist_next (list);
		}

		/* joins */
		if (query->priv->joins_flat)
			g_print ("%sJoins:\n", str);
		else
			g_print ("%sNo join defined.\n", str);
		list = query->priv->joins_flat;
		while (list) {
			mg_base_dump (MG_BASE (list->data), offset+5);
			list = g_slist_next (list);
		}
		joins_pack_dump (query); /* RAW joins pack output */

		/* condition */
		if (query->priv->cond) {
			g_print ("%sCondition:\n", str);
			mg_base_dump (MG_BASE (query->priv->cond), offset+5);
		}
		else
			g_print ("%sNo Condition defined.\n", str);

		/* sub queries */
		if (query->priv->sub_queries)
			g_print ("%sSub-queries:\n", str);
		else
			g_print ("%sNo sub-query defined.\n", str);
		list = query->priv->sub_queries;
		while (list) {
			mg_base_dump (MG_BASE (list->data), offset+5);
			list = g_slist_next (list);
		}

		/* parameters sources */
		if (query->priv->param_sources)
			g_print ("%sParameters sources:\n", str);
		list = query->priv->param_sources;
		while (list) {
			mg_base_dump (MG_BASE (list->data), offset+5);
			list = g_slist_next (list);
		}

		/* Rendered version of the query */
		sql = mg_renderer_render_as_sql (MG_RENDERER (query), NULL, &error);
		if (sql) {
			g_print ("%sSQL=%s\n", str, sql);
			g_free (sql);
		}
		else {
			g_print ("%sError occured:\n%s%s\n", str, str, error->message);
			g_error_free (error);
		}
	}
        else
                g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, query);
	g_free (str);
}
#endif


/**
 * mg_query_set_order_by_field
 * @query: a #MgQuery
 * @field: a #MgQfield which is in @query
 * @order: the order in the list of ORDER BY fields (starts at 0), or -1
 * @ascendant: TRUE to sort ascending
 *
 * Sets @field to be used in the ORDER BY clause (using the @order and @ascendant attributes) if
 * @order >= 0. If @order < 0, then @field will not be used in the ORDER BY clause.
 */
void
mg_query_set_order_by_field (MgQuery *query, MgQfield *field, gint order, gboolean ascendant)
{
	g_return_if_fail (query && IS_MG_QUERY (query));
	g_return_if_fail (query->priv);
	g_return_if_fail (field && IS_MG_QFIELD (field));
	g_return_if_fail (g_slist_find (query->priv->fields, field));
	
	if ((query->priv->query_type == MG_QUERY_TYPE_INSERT) ||
	    (query->priv->query_type == MG_QUERY_TYPE_DELETE) ||
	    (query->priv->query_type == MG_QUERY_TYPE_UPDATE))
		return;
	
	if (g_slist_find (query->priv->fields_order_by, field))
		query->priv->fields_order_by = g_slist_remove (query->priv->fields_order_by, field);
	
	if (order < 0)
		g_object_set_data (G_OBJECT (field), "order_by_asc", NULL);
	else { /* add to the ORDER BY */
		g_object_set_data (G_OBJECT (field), "order_by_asc", GINT_TO_POINTER (ascendant));
		query->priv->fields_order_by = g_slist_insert (query->priv->fields_order_by, field, order);
	}
}

/**
 * mg_query_get_order_by_field
 * @query: a #MgQuery
 * @field: a #MgQfield which is in @query
 * @ascendant: if not %NULL, will be set TRUE if ascendant sorting and FALSE otherwise
 *
 * Tells if @field (which MUST be in @query) is part of the ORDER BY clause.
 *
 * Returns: -1 if no, and the order where it appears in the ORDER BY list otherwise
 */
gint
mg_query_get_order_by_field (MgQuery *query, MgQfield *field, gboolean *ascendant)
{
	g_return_val_if_fail (query && IS_MG_QUERY (query), -1);
	g_return_val_if_fail (query->priv, -1);
	g_return_val_if_fail (field && IS_MG_QFIELD (field), -1);
	g_return_val_if_fail (g_slist_find (query->priv->fields, field), -1);

	if (ascendant)
		*ascendant = g_object_get_data (G_OBJECT (field), "order_by_asc") ? TRUE : FALSE;
	return g_slist_index (query->priv->fields_order_by, field);
}



/*
 * MgEntity interface implementation
 */

/* NOTE: A query's field has several status:
   -> internal: such a field is added by libmergeant for its own needs, should NEVER
      get out of the query
   -> visible: such fields make the external representation of the equivalent entity
   -> non visible: fields that take part in making the query (condition, values, function params, etc
*/

static GSList *
mg_query_get_all_fields (MgEntity *iface)
{
	MgQuery *query;
	GSList *list, *fields = NULL;

	g_return_val_if_fail (iface && IS_MG_QUERY (iface), NULL);
	g_return_val_if_fail (MG_QUERY (iface)->priv, NULL);
	query = MG_QUERY (iface);
	
	list = query->priv->fields;
	while (list) {
		if (mg_qfield_is_visible (MG_QFIELD (list->data)) ||
		    !mg_qfield_is_internal (MG_QFIELD (list->data)))
			fields = g_slist_append (fields, list->data);
		list = g_slist_next (list);
	}

	return fields;
}

static GSList *
mg_query_get_visible_fields (MgEntity *iface)
{
	MgQuery *query;
	GSList *list, *fields = NULL;

	g_return_val_if_fail (iface && IS_MG_QUERY (iface), NULL);
	g_return_val_if_fail (MG_QUERY (iface)->priv, NULL);
	query = MG_QUERY (iface);

	list = query->priv->fields;
	while (list) {
		if (mg_qfield_is_visible (MG_QFIELD (list->data)))
			fields = g_slist_append (fields, list->data);
		list = g_slist_next (list);
	}

	return fields;
}

static MgField *
mg_query_get_field_by_name (MgEntity *iface, const gchar *name)
{
	MgQuery *query;

	g_return_val_if_fail (iface && IS_MG_QUERY (iface), NULL);
	g_return_val_if_fail (MG_QUERY (iface)->priv, NULL);
	query = MG_QUERY (iface);
	
	TO_IMPLEMENT;
	return NULL;
}

static MgField *
mg_query_get_field_by_xml_id (MgEntity *iface, const gchar *xml_id)
{
	MgQuery *query;
	GSList *list;
	MgField *field = NULL;
	gchar *str;

	g_return_val_if_fail (iface && IS_MG_QUERY (iface), NULL);
	g_return_val_if_fail (MG_QUERY (iface)->priv, NULL);
	query = MG_QUERY (iface);
	
	list = query->priv->fields;
	while (list && !field) {
		str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
		if (!strcmp (str, xml_id))
			field = MG_FIELD (list->data);
		list = g_slist_next (list);
	}

	return field;
}

static MgField *
mg_query_get_field_by_index (MgEntity *iface, gint index)
{
	MgQuery *query;
	GSList *list;
	MgField *field = NULL;
	gint i = -1;

	g_return_val_if_fail (iface && IS_MG_QUERY (iface), NULL);
	g_return_val_if_fail (MG_QUERY (iface)->priv, NULL);
	query = MG_QUERY (iface);
	
	list = query->priv->fields;
	while (list && !field) {
		if (mg_qfield_is_visible (MG_QFIELD (list->data))) {
			i++;
			if (i == index)
				field = MG_FIELD (list->data);
		}

		list = g_slist_next (list);
	}

	return field;
}

static gint
mg_query_get_field_index (MgEntity *iface, MgField *field)
{
	MgQuery *query;
	GSList *list;
	gint current, pos = -1;

	g_return_val_if_fail (iface && IS_MG_QUERY (iface), -1);
	g_return_val_if_fail (MG_QUERY (iface)->priv, -1);
	g_return_val_if_fail (field && IS_MG_QFIELD (field), -1);
	query = MG_QUERY (iface);
	g_return_val_if_fail (g_slist_find (query->priv->fields, field), -1);

	if (!mg_qfield_is_visible (MG_QFIELD (field)))
		return -1;

	current = 0;
	list = query->priv->fields;
	while (list && (pos==-1)) {
		if (list->data == (gpointer) field)
			pos = current;
		if (mg_qfield_is_visible (MG_QFIELD (list->data)))
			current++;
		list = g_slist_next (list);
	}

	return pos;
}

static void
mg_query_add_field (MgEntity *iface, MgField *field)
{
	mg_query_add_field_before (iface, field, NULL);
}

static void
mg_query_add_field_before (MgEntity *iface, MgField *field, MgField *field_before)
{
	MgQuery *query;
	gint pos = -1;

	g_return_if_fail (iface && IS_MG_QUERY (iface));
	g_return_if_fail (MG_QUERY (iface)->priv);
	query = MG_QUERY (iface);

	g_return_if_fail (field && IS_MG_QFIELD (field));
        g_return_if_fail (!g_slist_find (query->priv->fields, field));
	g_return_if_fail (mg_field_get_entity (field) == MG_ENTITY (query));

	if (field_before) {
		g_return_if_fail (field_before && IS_MG_QFIELD (field_before));
		g_return_if_fail (g_slist_find (query->priv->fields, field_before));
		g_return_if_fail (mg_field_get_entity (field_before) == MG_ENTITY (query));
		pos = g_slist_index (query->priv->fields, field_before);
	}

	query->priv->fields = g_slist_insert (query->priv->fields, field, pos);
	g_object_ref (G_OBJECT (field));
	g_signal_connect (G_OBJECT (field), "nullified",
                          G_CALLBACK (nullified_field_cb), query);
        g_signal_connect (G_OBJECT (field), "changed",
                          G_CALLBACK (changed_field_cb), query);
        g_signal_connect (G_OBJECT (field), "id_changed",
                          G_CALLBACK (id_field_changed_cb), query);

#ifdef debug_signal
        g_print (">> 'FIELD_ADDED' from %s\n", __FUNCTION__);
#endif
        g_signal_emit_by_name (G_OBJECT (query), "field_added", field);
#ifdef debug_signal
        g_print ("<< 'FIELD_ADDED' from %s\n", __FUNCTION__);
#endif	
}

static void
nullified_field_cb (MgField *field, MgQuery *query)
{
	g_assert (g_slist_find (query->priv->fields, field));

	mg_query_set_order_by_field (query, MG_QFIELD (field), -1, FALSE);
        query->priv->fields = g_slist_remove (query->priv->fields, field);
        g_signal_handlers_disconnect_by_func (G_OBJECT (field),
                                              G_CALLBACK (nullified_field_cb), query);
        g_signal_handlers_disconnect_by_func (G_OBJECT (field),
                                              G_CALLBACK (changed_field_cb), query);
        g_signal_handlers_disconnect_by_func (G_OBJECT (field),
                                              G_CALLBACK (id_field_changed_cb), query);

#ifdef debug_signal
        g_print (">> 'FIELD_REMOVED' from %s\n", __FUNCTION__);
#endif
        g_signal_emit_by_name (G_OBJECT (query), "field_removed", field);
#ifdef debug_signal
        g_print ("<< 'FIELD_REMOVED' from %s\n", __FUNCTION__);
#endif

        g_object_unref (field);
}

static void
changed_field_cb (MgField *field, MgQuery *query)
{
#ifdef debug_signal
        g_print (">> 'FIELD_UPDATED' from %s\n", __FUNCTION__);
#endif
        g_signal_emit_by_name (G_OBJECT (query), "field_updated", field);
#ifdef debug_signal
        g_print ("<< 'FIELD_UPDATED' from %s\n", __FUNCTION__);
#endif
}


static void
mg_query_swap_fields (MgEntity *iface, MgField *field1, MgField *field2)
{
	MgQuery *query;

	g_return_if_fail (iface && IS_MG_QUERY (iface));
	g_return_if_fail (MG_QUERY (iface)->priv);
	query = MG_QUERY (iface);

	TO_IMPLEMENT;
}

static void
mg_query_remove_field (MgEntity *iface, MgField *field)
{
	MgQuery *query;

	g_return_if_fail (iface && IS_MG_QUERY (iface));
	g_return_if_fail (MG_QUERY (iface)->priv);
	query = MG_QUERY (iface);
	g_return_if_fail (field && IS_MG_QFIELD (field));
	g_return_if_fail (g_slist_find (query->priv->fields, field));

	nullified_field_cb (field, query);
}

static gboolean
mg_query_is_writable (MgEntity *iface)
{
	g_return_val_if_fail (iface && IS_MG_QUERY (iface), FALSE);
	g_return_val_if_fail (MG_QUERY (iface)->priv, FALSE);
	
	return FALSE;
}

static GSList *
mg_query_get_parameters (MgEntity *iface)
{
	MgQuery *query;
	GSList *list, *tmplist, *retval = NULL;
	GHashTable *hash;

	g_return_val_if_fail (iface && IS_MG_QUERY (iface), NULL);
	g_return_val_if_fail (MG_QUERY (iface)->priv, NULL);
	query = MG_QUERY (iface);
	
	/* make the list of parameters */
	list = query->priv->fields;
	while (list) {
		tmplist = mg_qfield_get_parameters (MG_QFIELD (list->data));
		if (tmplist)
			retval = g_slist_concat (retval, tmplist);
		list = g_slist_next (list);
	}

	list = query->priv->sub_queries;
	while (list) {
		tmplist = mg_query_get_parameters (MG_ENTITY (list->data));
		if (tmplist)
			retval = g_slist_concat (retval, tmplist);
		list = g_slist_next (list);
	}

	/* reorder the parameters to have only one MgParameter object for query field parameters which have the same
	   name */
	hash = g_hash_table_new (g_str_hash, g_str_equal);
	/* list = retval; */
	list = NULL;
	tmplist = NULL;
	while (list) {
		GSList *for_fields = mg_parameter_get_dest_fields (MG_PARAMETER (list->data));

		if (for_fields) {
			MgParameter *param;
			if (!mg_field_get_name (MG_FIELD (for_fields->data)) ||
			    !(param=g_hash_table_lookup (hash, mg_field_get_name (MG_FIELD (for_fields->data))))) {
				/* keep that param */
				tmplist = g_slist_append (tmplist, list->data); 
				if (mg_field_get_name (MG_FIELD (for_fields->data)))
					g_hash_table_insert (hash, mg_field_get_name (MG_FIELD (for_fields->data)), list->data);
			}
			else {
				/* transfer the destination fields to 'param', get rid of the 'list->data' param */
				while (for_fields) {
					mg_parameter_add_dest_field (param, for_fields->data);
					for_fields = g_slist_next (for_fields);
				}
				g_object_unref (G_OBJECT (list->data));
			}
		}
		list = g_slist_next (list);
	}
	g_hash_table_destroy (hash);
	/* retval = tmplist; */

	/* IMPROVE: reorder the parameters by number of dependencies ascending (first the params withiut any dependency,
	 * then the ones with one, etc; or even better group the parameters which depend on another one after but close to that
	 * other parameter */

	return retval;
}




/* 
 * MgReferer interface implementation
 */

static gboolean
mg_query_activate (MgReferer *iface)
{
	gboolean retval = TRUE;
	MgQuery *query;
	GSList *list;

	g_return_val_if_fail (iface && IS_MG_QUERY (iface), FALSE);
	g_return_val_if_fail (MG_QUERY (iface)->priv, FALSE);
	query = MG_QUERY (iface);


	list = query->priv->param_sources;
	while (list && retval) {
		retval = mg_referer_activate (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	list = query->priv->sub_queries;
	while (list && retval) {
		retval = mg_referer_activate (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	list = query->priv->targets;
	while (list && retval) {
		retval = mg_referer_activate (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	list = query->priv->fields;
	while (list && retval) {
		retval = mg_referer_activate (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	list = query->priv->joins_flat;
	while (list && retval) {
		retval = mg_referer_activate (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	if (retval && query->priv->cond)
		retval = mg_referer_activate (MG_REFERER (query->priv->cond));

	return retval;
}

static void
mg_query_deactivate (MgReferer *iface)
{
	MgQuery *query;
	GSList *list;

	g_return_if_fail (iface && IS_MG_QUERY (iface));
	g_return_if_fail (MG_QUERY (iface)->priv);
	query = MG_QUERY (iface);

	list = query->priv->param_sources;
	while (list) {
		mg_referer_deactivate (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	list = query->priv->sub_queries;
	while (list) {
		mg_referer_deactivate (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	list = query->priv->targets;
	while (list) {
		mg_referer_deactivate (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	list = query->priv->fields;
	while (list) {
		mg_referer_deactivate (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	list = query->priv->joins_flat;
	while (list) {
		mg_referer_deactivate (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	if (query->priv->cond)
		mg_referer_deactivate (MG_REFERER (query->priv->cond));
}

static gboolean
mg_query_are_joins_active (MgQuery *query)
{
	gboolean retval = TRUE;
	GSList *list;
	list = query->priv->joins_flat;
	while (list && retval) {
		retval = mg_referer_is_active (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	return retval;
}

static gboolean
mg_query_is_active (MgReferer *iface)
{
	gboolean retval = TRUE;
	MgQuery *query;
	GSList *list;

	g_return_val_if_fail (iface && IS_MG_QUERY (iface), FALSE);
	g_return_val_if_fail (MG_QUERY (iface)->priv, FALSE);
	query = MG_QUERY (iface);

	list = query->priv->param_sources;
	while (list && retval) {
		retval = mg_referer_is_active (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	list = query->priv->sub_queries;
	while (list && retval) {
		retval = mg_referer_is_active (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	list = query->priv->targets;
	while (list && retval) {
		retval = mg_referer_is_active (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	if (retval)
		retval = mg_query_are_joins_active (query);

	list = query->priv->fields;
	while (list && retval) {
		retval = mg_referer_is_active (MG_REFERER (list->data));
		list = g_slist_next (list);
	}

	if (retval && query->priv->cond)
		retval = mg_referer_is_active (MG_REFERER (query->priv->cond));

	return retval;
}

static GSList *
mg_query_get_ref_objects (MgReferer *iface)
{
	GSList *list = NULL, *sub, *retval = NULL;
	MgQuery *query;

	/* FIXME: do not take care of the objects which belong to the query itself */

	g_return_val_if_fail (iface && IS_MG_QUERY (iface), NULL);
	g_return_val_if_fail (MG_QUERY (iface)->priv, NULL);
	query = MG_QUERY (iface);

	list = query->priv->param_sources;
	while (list && retval) {
		sub = mg_referer_get_ref_objects (MG_REFERER (list->data));
		retval = g_slist_concat (retval, sub);
		list = g_slist_next (list);
	}

	list = query->priv->sub_queries;
	while (list && retval) {
		sub = mg_referer_get_ref_objects (MG_REFERER (list->data));
		retval = g_slist_concat (retval, sub);
		list = g_slist_next (list);
	}

	list = query->priv->targets;
	while (list && retval) {
		sub = mg_referer_get_ref_objects (MG_REFERER (list->data));
		retval = g_slist_concat (retval, sub);
		list = g_slist_next (list);
	}

	list = query->priv->fields;
	while (list && retval) {
		sub = mg_referer_get_ref_objects (MG_REFERER (list->data));
		retval = g_slist_concat (retval, sub);
		list = g_slist_next (list);
	}

	list = query->priv->joins_flat;
	while (list && retval) {
		sub = mg_referer_get_ref_objects (MG_REFERER (list->data));
		retval = g_slist_concat (retval, sub);
		list = g_slist_next (list);
	}

	if (query->priv->cond) {
		sub = mg_referer_get_ref_objects (MG_REFERER (query->priv->cond));
		retval = g_slist_concat (retval, sub);
	}

	return retval;
}

static void
mg_query_replace_refs (MgReferer *iface, GHashTable *replacements)
{
	GSList *list;
	MgQuery *query;

	g_return_if_fail (iface && IS_MG_QUERY (iface));
	g_return_if_fail (MG_QUERY (iface)->priv);
	query = MG_QUERY (iface);


	list = query->priv->param_sources;
	while (list) {
		mg_referer_replace_refs (MG_REFERER (list->data), replacements);
		list = g_slist_next (list);
	}

	list = query->priv->sub_queries;
	while (list) {
		mg_referer_replace_refs (MG_REFERER (list->data), replacements);
		list = g_slist_next (list);
	}

	list = query->priv->targets;
	while (list) {
		mg_referer_replace_refs (MG_REFERER (list->data), replacements);
		list = g_slist_next (list);
	}

	list = query->priv->fields;
	while (list) {
		mg_referer_replace_refs (MG_REFERER (list->data), replacements);
		list = g_slist_next (list);
	}

	list = query->priv->joins_flat;
	while (list) {
		mg_referer_replace_refs (MG_REFERER (list->data), replacements);
		list = g_slist_next (list);
	}

	if (query->priv->cond)
		mg_referer_replace_refs (MG_REFERER (query->priv->cond), replacements);
}

/* 
 * MgXmlStorage interface implementation
 */

static const gchar *convert_query_type_to_str (MgQueryType type);
static MgQueryType  convert_str_to_query_type (const gchar *str);

static gchar *
mg_query_get_xml_id (MgXmlStorage *iface)
{
	g_return_val_if_fail (iface && IS_MG_QUERY (iface), NULL);
	g_return_val_if_fail (MG_QUERY (iface)->priv, NULL);

	return g_strdup_printf ("QU%d", mg_base_get_id (MG_BASE (iface)));
}

static xmlNodePtr
mg_query_save_to_xml (MgXmlStorage *iface, GError **error)
{
	xmlNodePtr node = NULL, psources = NULL;
	MgQuery *query;
	gchar *str;
	const gchar *type;
	GSList *list;

	g_return_val_if_fail (iface && IS_MG_QUERY (iface), NULL);
	g_return_val_if_fail (MG_QUERY (iface)->priv, NULL);
	query = MG_QUERY (iface);


	/* query itself */
	node = xmlNewNode (NULL, "MG_QUERY");
	str = mg_query_get_xml_id (MG_XML_STORAGE (query));
	xmlSetProp (node, "id", str);
	g_free (str);
	xmlSetProp (node, "name", mg_base_get_name (MG_BASE (query)));
        xmlSetProp (node, "descr", mg_base_get_description (MG_BASE (query)));
	type = convert_query_type_to_str (query->priv->query_type);
	xmlSetProp (node, "query_type", type);

	/* param sources */
	list = query->priv->param_sources;
	if (list) 
		psources = xmlNewChild (node, NULL, "MG_PARAM_SOURCES", NULL);

	while (list) {
		xmlNodePtr sub = mg_xml_storage_save_to_xml (MG_XML_STORAGE (list->data), error);
		if (sub)
                        xmlAddChild (psources, sub);
                else {
                        xmlFreeNode (node);
                        return NULL;
                }
		list = g_slist_next (list);
	}
	
	/* targets */
	list = query->priv->targets;
	while (list) {
		xmlNodePtr sub = mg_xml_storage_save_to_xml (MG_XML_STORAGE (list->data), error);
		if (sub)
                        xmlAddChild (node, sub);
                else {
                        xmlFreeNode (node);
                        return NULL;
                }
		list = g_slist_next (list);
	}

	/* fields */
	list = query->priv->fields;
	while (list) {
		xmlNodePtr sub = mg_xml_storage_save_to_xml (MG_XML_STORAGE (list->data), error);
		if (sub)
                        xmlAddChild (node, sub);
                else {
                        xmlFreeNode (node);
                        return NULL;
                }
		list = g_slist_next (list);
	}

	/* joins */
	list = query->priv->joins_flat;
	while (list) {
		xmlNodePtr sub = mg_xml_storage_save_to_xml (MG_XML_STORAGE (list->data), error);
		if (sub)
                        xmlAddChild (node, sub);
                else {
                        xmlFreeNode (node);
                        return NULL;
                }
		list = g_slist_next (list);
	}

	/* condition */
	if (query->priv->cond) {
		xmlNodePtr sub = mg_xml_storage_save_to_xml (MG_XML_STORAGE (query->priv->cond), error);
		if (sub)
                        xmlAddChild (node, sub);
                else {
                        xmlFreeNode (node);
                        return NULL;
                }
	}

	/* fields ORDER BY */
	if (query->priv->fields_order_by) {
		xmlNodePtr sub = xmlNewChild (node, NULL, "MG_FIELDS_ORDER", NULL);

		list = query->priv->fields_order_by;
		while (list) {
			xmlNodePtr order = xmlNewChild (sub, NULL, "MG_QF_REF", NULL);

			str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
			xmlSetProp (order, "object", str);
			g_free (str);
				    
			xmlSetProp (order, "order", 
				    g_object_get_data (G_OBJECT (list->data), "order_by_asc") ? "ASC" : "DES");
			list = g_slist_next (list);
		}
	}

	/* Text if SQL query */
	if (query->priv->query_type == MG_QUERY_TYPE_SQL)
		xmlNewChild (node, NULL, "MG_QUERY_TEXT", query->priv->sql);


	/* sub queries */
	list = query->priv->sub_queries;
	while (list) {
		xmlNodePtr sub = mg_xml_storage_save_to_xml (MG_XML_STORAGE (list->data), error);
		if (sub)
                        xmlAddChild (node, sub);
                else {
                        xmlFreeNode (node);
                        return NULL;
                }
		list = g_slist_next (list);
	}

	return node;
}


static gboolean
mg_query_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgQuery *query;
	gchar *prop;
	gboolean id = FALSE;
	xmlNodePtr children;

	g_return_val_if_fail (iface && IS_MG_QUERY (iface), FALSE);
	g_return_val_if_fail (MG_QUERY (iface)->priv, FALSE);
	g_return_val_if_fail (node, FALSE);

	query = MG_QUERY (iface);
	if (strcmp (node->name, "MG_QUERY")) {
		g_set_error (error,
			     MG_QUERY_ERROR,
			     MG_QUERY_XML_LOAD_ERROR,
			     _("XML Tag is not <MG_QUERY>"));
		return FALSE;
	}

	/* query itself */
	prop = xmlGetProp (node, "id");
	if (prop) {
		mg_base_set_id (MG_BASE (query), atoi (prop+2));
		g_free (prop);
		id = TRUE;
	}	

	prop = xmlGetProp (node, "name");
        if (prop) {
                mg_base_set_name (MG_BASE (query), prop);
                g_free (prop);
        }

        prop = xmlGetProp (node, "descr");
        if (prop) {
                mg_base_set_description (MG_BASE (query), prop);
                g_free (prop);
        }

	prop = xmlGetProp (node, "query_type");
	if (prop) {
		query->priv->query_type = convert_str_to_query_type (prop);
		g_free (prop);
	}

	/* children nodes */
	children = node->children;
	while (children) {
		gboolean done = FALSE;


		/* parameters sources */
		if (!done && !strcmp (children->name, "MG_PARAM_SOURCES")) {
			MgConf *conf = mg_base_get_conf (MG_BASE (query));
			xmlNodePtr sparams = children->children;
			while (sparams) {
				if (!strcmp (sparams->name, "MG_QUERY")) {
					MgQuery *squery;
					
					squery = MG_QUERY (mg_query_new (conf));
					if (mg_xml_storage_load_from_xml (MG_XML_STORAGE (squery), sparams, error)) {
						mg_query_add_param_source (query, squery);
						g_object_unref (G_OBJECT (squery));
					}
					else
						return FALSE;
				}
				sparams = sparams->next;
			}
			done = TRUE;
                }

		/* targets */
		if (!done && !strcmp (children->name, "MG_TARGET")) {
                        MgTarget *target;
			gchar *ent_id;

			ent_id = xmlGetProp (children, "entity_ref");
			if (ent_id) {
				target = MG_TARGET (mg_target_new_with_xml_id (query, ent_id));
				g_free (ent_id);
				if (mg_xml_storage_load_from_xml (MG_XML_STORAGE (target), children, error)) {
					if (!mg_query_add_target (query, target, error)) {
						g_object_unref (G_OBJECT (target));
						return FALSE;
					}
					g_object_unref (G_OBJECT (target));
				}
				else
					return FALSE;
			}
			else
				return FALSE;
			done = TRUE;
                }

		/* fields */
		if (!done && !strcmp (children->name, "MG_QF")) {
			GObject *obj;

			obj = mg_qfield_new_from_xml (query, children, error);
			if (obj) {
				mg_query_add_field (MG_ENTITY (query), MG_FIELD (obj));
				g_object_unref (G_OBJECT (obj));
			}
			else
				return FALSE;
			done = TRUE;
                }

		/* joins */
		if (!done && !strcmp (children->name, "MG_JOIN")) {
                        MgJoin *join;
			gchar *t1, *t2;

			t1 = xmlGetProp (children, "target1");
			t2 = xmlGetProp (children, "target2");

			if (t1 && t2) {
				join = MG_JOIN (mg_join_new_with_xml_ids (query, t1, t2));
				g_free (t1);
				g_free (t2);
				if (mg_xml_storage_load_from_xml (MG_XML_STORAGE (join), children, error)) {
					mg_query_add_join (query, join);
					g_object_unref (G_OBJECT (join));
				}
				else
					return FALSE;
			}
			else
				return FALSE;
			done = TRUE;
                }

		/* condition */
		if (!done && !strcmp (children->name, "MG_COND")) {
			MgCondition *cond;

			cond = MG_CONDITION (mg_condition_new (query, MG_CONDITION_NODE_AND));
			if (mg_xml_storage_load_from_xml (MG_XML_STORAGE (cond), children, error)) {
				mg_query_set_condition (query, cond);
				g_object_unref (G_OBJECT (cond));
			}
			else
				return FALSE;
			done = TRUE;
                }

		/* fields ORDER BY */
		if (!done && !strcmp (children->name, "MG_FIELDS_ORDER")) {
			xmlNodePtr order = children->children;
			gint pos = 0;

			while (order) {
				if (!strcmp (order->name, "MG_QF_REF")) {
					MgField *field = NULL;
					gboolean asc = TRUE;

					prop = xmlGetProp (order, "object");
					if (prop) {
						field = mg_entity_get_field_by_xml_id (MG_ENTITY (query), prop);
						if (!field)
							g_set_error (error,
								     MG_QUERY_ERROR,
								     MG_QUERY_XML_LOAD_ERROR,
								     _("Can't find field '%s'"), prop);
						g_free (prop);
						pos ++;
					}
					
					prop = xmlGetProp (order, "order");
					if (prop) {
						asc = (*prop == 'A');
						g_free (prop);
					}
					if (field) 
						mg_query_set_order_by_field (query, MG_QFIELD (field), pos, asc);
					else 
						return FALSE;
				}
				order = order->next;
			}

			done = TRUE;
                }

		/* textual query */
		if (!done && !strcmp (children->name, "MG_QUERY_TEXT")) {
			gchar *contents;

			contents = xmlNodeGetContent (children);
			mg_query_sql_set_text (query, contents);
			g_free (contents);
			done = TRUE;
                }

		/* sub queries */
		if (!done && !strcmp (children->name, "MG_QUERY")) {
			MgQuery *squery;
			MgConf *conf = mg_base_get_conf (MG_BASE (query));

			squery = MG_QUERY (mg_query_new (conf));
			if (mg_xml_storage_load_from_xml (MG_XML_STORAGE (squery), children, error)) {
				mg_query_add_sub_query (query, squery);
				g_object_unref (G_OBJECT (squery));
			}
			else
				return FALSE;
			done = TRUE;
                }
		
		children = children->next;
	}

	if (id)
		return TRUE;
	else {
		g_set_error (error,
			     MG_QUERY_ERROR,
			     MG_QUERY_XML_LOAD_ERROR,
			     _("Problem loading <MG_QUERY>"));
		return FALSE;
	}
}

static const gchar *
convert_query_type_to_str (MgQueryType type)
{
	switch (type) {
	default:
	case MG_QUERY_TYPE_SELECT:
		return "SEL";
	case MG_QUERY_TYPE_INSERT:
		return "INS";
	case MG_QUERY_TYPE_UPDATE:
		return "UPD";
	case MG_QUERY_TYPE_DELETE:
		return "DEL";
	case MG_QUERY_TYPE_UNION:
		return "NION";
	case MG_QUERY_TYPE_INTERSECT:
		return "ECT";
	case MG_QUERY_TYPE_EXCEPT:
		return "XPT";
	case MG_QUERY_TYPE_SQL:
		return "TXT";
	}
}

static MgQueryType
convert_str_to_query_type (const gchar *str)
{
	switch (*str) {
	case 'S':
	default:
		return MG_QUERY_TYPE_SELECT;
	case 'I':
		return MG_QUERY_TYPE_INSERT;
	case 'U':
		return MG_QUERY_TYPE_UPDATE;
	case 'D':
		return MG_QUERY_TYPE_DELETE;
	case 'N':
		return MG_QUERY_TYPE_UNION;
	case 'E':
		return MG_QUERY_TYPE_INTERSECT;
	case 'T':
		return MG_QUERY_TYPE_SQL;
	case 'X':
		return MG_QUERY_TYPE_EXCEPT;
	}
}





/*
 * MgRenderer interface implementation
 */
static GdaXqlItem *
mg_query_render_as_xql (MgRenderer *iface, MgContext *context, GError **error)
{
	MgQuery *query;
	g_return_val_if_fail (iface && IS_MG_QUERY (iface), NULL);
	g_return_val_if_fail (MG_QUERY (iface)->priv, NULL);
	query = MG_QUERY (iface);

	TO_IMPLEMENT;
	return NULL;
}

static gchar *render_sql_select (MgQuery *query, MgContext *context, GError **error);
static gchar *render_sql_insert (MgQuery *query, MgContext *context, GError **error);
static gchar *render_sql_update (MgQuery *query, MgContext *context, GError **error);
static gchar *render_sql_delete (MgQuery *query, MgContext *context, GError **error);
static gchar *render_sql_union (MgQuery *query, MgContext *context, GError **error);
static gchar *render_sql_intersect (MgQuery *query, MgContext *context, GError **error);
static gchar *render_sql_except (MgQuery *query, MgContext *context, GError **error);

static gboolean assert_coherence_all_params_present (MgQuery *query, MgContext *context, GError **error);
static gboolean assert_coherence_entities_same_fields (MgEntity *ent1, MgEntity *ent2);
static gboolean assert_coherence_sub_query_select (MgQuery *query, MgContext *context, GError **error);
static gboolean assert_coherence_data_select_query (MgQuery *query, MgContext *context, GError **error);
static gboolean assert_coherence_data_modify_query (MgQuery *query, MgContext *context, GError **error);
static gboolean assert_coherence_aggregate_query (MgQuery *query, MgContext *context, GError **error);

static gchar *
mg_query_render_as_sql (MgRenderer *iface, MgContext *context, GError **error)
{
	MgQuery *query;
	gchar *sql = NULL;
	
	g_return_val_if_fail (iface && IS_MG_QUERY (iface), NULL);
	g_return_val_if_fail (MG_QUERY (iface)->priv, NULL);
	if (!mg_referer_activate (MG_REFERER (iface))) {
		g_set_error (error,
			     MG_QUERY_ERROR,
			     MG_QUERY_RENDER_ERROR,
			     _("Can't resolve some references in the query"));
		return NULL;
	}
	query = MG_QUERY (iface);

	/* make sure all the required parameters are in @context */
	if (!assert_coherence_all_params_present (query, context, error))
		return NULL;


	switch (query->priv->query_type) {
	case MG_QUERY_TYPE_SELECT:
		if (assert_coherence_data_select_query (query, context, error))
			sql = render_sql_select (query, context, error);
		break;
	case MG_QUERY_TYPE_INSERT:
		if (assert_coherence_data_modify_query (query, context, error))
			sql = render_sql_insert (query, context, error);
		break;
	case MG_QUERY_TYPE_UPDATE:
		if (assert_coherence_data_modify_query (query, context, error))
			sql = render_sql_update (query, context, error);
		break;
	case MG_QUERY_TYPE_DELETE:
		if (assert_coherence_data_modify_query (query, context, error))
			sql = render_sql_delete (query, context, error);
		break;
	case MG_QUERY_TYPE_UNION:
		if (assert_coherence_aggregate_query (query, context, error))
			sql = render_sql_union (query, context, error);
		break;
	case MG_QUERY_TYPE_INTERSECT:
		if (assert_coherence_aggregate_query (query, context, error))
			sql = render_sql_intersect (query, context, error);
		break;
	case MG_QUERY_TYPE_EXCEPT:
		if (assert_coherence_aggregate_query (query, context, error)) {
			if (g_slist_length (query->priv->sub_queries) != 2)
				g_set_error (error,
					     MG_QUERY_ERROR,
					     MG_QUERY_RENDER_ERROR,
					     _("More than two sub queries for an EXCEPT query"));
			else
				sql = render_sql_except (query, context, error);
		}
		break;
	case MG_QUERY_TYPE_SQL:
		if (query->priv->sql && *(query->priv->sql))
			sql = g_strdup (query->priv->sql);
		else
			g_set_error (error,
				     MG_QUERY_ERROR,
				     MG_QUERY_RENDER_ERROR,
				     _("Query without any SQL code"));
		break;
	default:
		g_assert_not_reached ();
	}

	return sql;
}

/* make sure the context provides all the required values for the parameters */
static gboolean
assert_coherence_all_params_present (MgQuery *query, MgContext *context, GError **error)
{
	gboolean retval = TRUE;
	GSList *params, *plist;

	params = mg_entity_get_parameters (MG_ENTITY (query));
	plist = params;
	while (plist && retval) {
		GSList *for_fields = mg_parameter_get_dest_fields (MG_PARAMETER (plist->data));
		while (retval && for_fields) {
			if (mg_field_get_entity (MG_FIELD (for_fields->data)) == MG_ENTITY (query)) {
				gboolean found = FALSE;
				GSList *clist = NULL;
				MgQfield *for_field = MG_QFIELD (for_fields->data);
				
				if (context)
					clist = context->parameters;
				
				/* if the parameter has a value, then OK */
				if (IS_MG_QF_VALUE (for_field) && mg_qf_value_get_value (MG_QF_VALUE (for_field)))
					found = TRUE;
				
				/* try to find a value within the context */
				while (clist && !found) {
					if (g_slist_find (mg_parameter_get_dest_fields (MG_PARAMETER (clist->data)), for_field)) {
						/* if (mg_parameter_get_value (MG_PARAMETER (clist->data))) */
						found = TRUE;
					}
					clist = g_slist_next (clist);
				}
				
				if (!found) {
					if (context) {
						retval = FALSE;
						g_set_error (error,
							     MG_QUERY_ERROR,
							     MG_QUERY_RENDER_ERROR,
							     _("Missing parameters"));
						g_print ("QUERY MISSING PARAM: QU%d %s\n", 
							 mg_base_get_id (MG_BASE (query)),
							 mg_base_get_name (MG_BASE (query)));
					}
				}
			}
			for_fields = g_slist_next (for_fields);
		}
		
		g_object_unref (G_OBJECT (plist->data));
		plist = g_slist_next (plist);
	}
	g_slist_free (params);

	return retval;
}

/* makes sure the number of fields of the two entities is the same.
 * IMPROVE: test for the field types compatibilities, but we know nothing about
 * possible (implicit or not) conversions between data types
 */
static gboolean
assert_coherence_entities_same_fields (MgEntity *ent1, MgEntity *ent2)
{
	gboolean retval;
	GSList *list1, *list2;

	list1 = mg_entity_get_visible_fields (ent1);
	list2 = mg_entity_get_visible_fields (ent2);

	retval = g_slist_length (list1) == g_slist_length (list2) ? TRUE : FALSE;

	g_slist_free (list1);
	g_slist_free (list2);

	return retval;
}

/* makes sure that all the sub queries of @query are SELECT queries, and are valid.
 * It is assumed that @query is active.
 *
 * Fills error if provided
 */
static gboolean 
assert_coherence_sub_query_select (MgQuery *query, MgContext *context, GError **error)
{
	GSList *list;
	gboolean retval = TRUE;
	MgQuery *sub;

	list = query->priv->sub_queries;
	while (list && retval) {
		sub = MG_QUERY (list->data);
		if ((sub->priv->query_type != MG_QUERY_TYPE_SELECT) &&
		    (sub->priv->query_type != MG_QUERY_TYPE_UNION) && 
		    (sub->priv->query_type != MG_QUERY_TYPE_INTERSECT) &&
		    (sub->priv->query_type != MG_QUERY_TYPE_EXCEPT)){
			gchar *str = mg_query_render_as_str (MG_RENDERER (sub), context);
			
			retval = FALSE;
			g_set_error (error,
				     MG_QUERY_ERROR,
				     MG_QUERY_RENDER_ERROR,
				     _("Query %s is not a selection query"), str);
			g_free (str);
		}
		else 
			retval = assert_coherence_sub_query_select (sub, context, error);
		list = g_slist_next (list);
	}
	
	return retval;
}


static gboolean
assert_coherence_data_select_query (MgQuery *query, MgContext *context, GError **error)
{
	gboolean retval;

	/* make sure all the sub queries are of type SELECT (recursively) and are also valid */
	retval = assert_coherence_sub_query_select (query, context, error);
	return retval;
}

static gboolean
assert_coherence_data_modify_query (MgQuery *query, MgContext *context, GError **error)
{
	gboolean retval = TRUE;

	/* make sure there is only 1 target and the represented entity can be modified */
	if (retval && (g_slist_length (query->priv->targets) == 0)) {
		g_set_error (error,
			     MG_QUERY_ERROR,
			     MG_QUERY_RENDER_ERROR,
			     _("No target defined to apply modifications"));
		retval = FALSE;
	}

	if (retval && (g_slist_length (query->priv->targets) > 1)) {
		g_set_error (error,
			     MG_QUERY_ERROR,
			     MG_QUERY_RENDER_ERROR,
			     _("More than one target defined to apply modifications"));
		retval = FALSE;
	}
	
	/* make sure entity is writable */
	if (retval) {
		MgEntity *entity = mg_target_get_represented_entity (MG_TARGET (query->priv->targets->data));
		if (!mg_entity_is_writable (entity)) {
			g_set_error (error,
				     MG_QUERY_ERROR,
				     MG_QUERY_RENDER_ERROR,
				     _("Entity %s is not writable"), mg_base_get_name (MG_BASE (entity)));
			retval = FALSE;	
		}
	}

	/* make sure all the sub queries are of type SELECT (recursively) and are also valid */
	if (retval)
		retval = assert_coherence_sub_query_select (query, context, error);

	/* make sure all visible fields are of type MG_QF_FIELD */
	if (retval) {
		GSList *list;
		list = query->priv->fields;
		while (list && retval) {
			if (mg_qfield_is_visible (MG_QFIELD (list->data))) {
				if (G_OBJECT_TYPE (list->data) != MG_QF_FIELD_TYPE) {
					g_set_error (error,
						     MG_QUERY_ERROR,
						     MG_QUERY_RENDER_ERROR,
						     _("Modification query field has incompatible type"));
					retval = FALSE;
				}
			}
			list = g_slist_next (list);
		}
	}

	/* INSERT specific tests */
	if (retval && (query->priv->query_type == MG_QUERY_TYPE_INSERT)) {
		/* I there is a sub query, make sure that:
		   - there is only ONE sub query of type SELECT
		   - that sub query has the same number and the same types of visible fields
		*/
		if (query->priv->sub_queries) {
			if (g_slist_length (query->priv->sub_queries) > 1) {
				g_set_error (error,
					     MG_QUERY_ERROR,
					     MG_QUERY_RENDER_ERROR,
					     _("An insertion query can only have one sub-query"));
				retval = FALSE;
			}
			if (retval && !assert_coherence_entities_same_fields (MG_ENTITY (query),
									      MG_ENTITY (query->priv->sub_queries->data))) {
				g_set_error (error,
					     MG_QUERY_ERROR,
					     MG_QUERY_RENDER_ERROR,
					     _("Insertion query fields incompatible with sub query's fields"));
				retval = FALSE;
			}
		}
		else {
			/* If there is no sub query, then make sure all the fields are of values */
			GSList *list;
			list = query->priv->fields;
			while (list && retval) {
				if (mg_qfield_is_visible (MG_QFIELD (list->data))) {
					MgBase *value_prov;
					g_object_get (G_OBJECT (list->data), "value_provider", &value_prov, NULL);
					if (value_prov && (G_OBJECT_TYPE (value_prov) != MG_QF_VALUE_TYPE)) {
						g_set_error (error,
							     MG_QUERY_ERROR,
							     MG_QUERY_RENDER_ERROR,
							     _("Insertion query field has incompatible value assignment"));
						retval = FALSE;
					}
				}
				list = g_slist_next (list);
			}
		}
		
		/* make sure there is no condition */
		if (retval && query->priv->cond) {
			g_set_error (error,
				     MG_QUERY_ERROR,
				     MG_QUERY_RENDER_ERROR,
				     _("Insertion query can't have any condition"));
			retval = FALSE;
		}
	}


	/* DELETE specific tests */
	if (retval && (query->priv->query_type == MG_QUERY_TYPE_DELETE)) {
		GSList *list;
		list = query->priv->fields;
		while (list && retval) {
			if (mg_qfield_is_visible (MG_QFIELD (list->data))) {
				g_set_error (error,
					     MG_QUERY_ERROR,
					     MG_QUERY_RENDER_ERROR,
					     _("Deletion query can't have any visible field"));
				retval = FALSE;
			}
			list = g_slist_next (list);
		}
	}

	/* UPDATE specific tests */
	if (retval && (query->priv->query_type == MG_QUERY_TYPE_UPDATE)) {
		GSList *list;
		list = query->priv->fields;
		while (list && retval) {
			if (mg_qfield_is_visible (MG_QFIELD (list->data))) {
				MgBase *value_prov;
				g_object_get (G_OBJECT (list->data), "value_provider", &value_prov, NULL);
				if (value_prov && IS_MG_QF_ALL (value_prov)) {
					g_set_error (error,
						     MG_QUERY_ERROR,
						     MG_QUERY_RENDER_ERROR,
						     _("Update query field has incompatible value assignment"));
					retval = FALSE;
				}
			}
			list = g_slist_next (list);
		}
	}

	return retval;
}

static gboolean
assert_coherence_aggregate_query (MgQuery *query, MgContext *context, GError **error)
{
	gboolean retval;

	/* FIXME: 
	   - make sure all the fields in each sub query are of the same type (and same number of them)
	*/

	/* make sure all the sub queries are of type SELECT (recursively) and are also valid */
	retval = assert_coherence_sub_query_select (query, context, error);

	if (retval && (g_slist_length (query->priv->targets) != 0)) {
		g_set_error (error,
			     MG_QUERY_ERROR,
			     MG_QUERY_RENDER_ERROR,
			     _("An aggregate type (UNION, etc) of query can't have any target"));
		retval = FALSE;
	}

	if (retval && query->priv->cond) {
		g_set_error (error,
			     MG_QUERY_ERROR,
			     MG_QUERY_RENDER_ERROR,
			     _("An aggregate type (UNION, etc) of query can't have any condition"));
		retval = FALSE;
	}

	return retval;
}


/* Structure used while rendering a JoinsPack: each time a join is analysed, such a structure is added to a list.
 * --> 'target' is always not NULL and represents the MgTarget the node brings.
 * --> 'first_join', if present, is the join which will be used to get the join type (INNER, etc)
 * --> 'other_joins', if present is the list of other joins participating in 'cond'
 * --> 'cond' is the string representing the join condition.
 */
typedef struct {
	MgTarget *target;
	MgJoin   *first_join;
	GSList   *other_joins;
	GString  *cond;
} JoinRenderNode;
#define JOIN_RENDER_NODE(x) ((JoinRenderNode *) x)

static gchar *render_join_condition (MgJoin *join, MgContext *context, GError **error, GSList *fk_constraints);
static gchar *
render_sql_select (MgQuery *query, MgContext *context, GError **error)
{
	GString *sql;
	gchar *retval, *str;
	GSList *list;
	gboolean first;
	gboolean err = FALSE;
	MgConf *conf = mg_base_get_conf (MG_BASE (query));
	GSList *fk_constraints = NULL;

	fk_constraints = mg_database_get_all_constraints (mg_conf_get_database (conf));

	/* query is supposed to be active and of a good constitution here */
	sql = g_string_new ("SELECT ");

	/* fields */
	first = TRUE;
	list = query->priv->fields;
	while (list && !err) {
		if (mg_qfield_is_visible (MG_QFIELD (list->data))) {
			if (first)
				first = FALSE;
			else
				g_string_append (sql, ", ");

			str = mg_renderer_render_as_sql (MG_RENDERER (list->data), context, error);
			if (str) {
				g_string_append (sql, str);
				g_free (str);
			}
			else {
				if (error && *error)
					err = TRUE;
				else
					g_string_append (sql, "NULL");
			}
		}
		list = g_slist_next (list);
	}

	
	/* targets and joins */
	if (query->priv->targets && !err) {
		GSList *all_joined = NULL; /* all the joined targets */
		GSList *packs, *list;
		JoinsPack *pack;
		MgJoin *join;

		first = TRUE;
		g_string_append (sql, " FROM ");
		packs = query->priv->joins_pack;
		while (packs && !err) {
			/* preparing the list of JoinRenderNodes */
			GSList *join_nodes = NULL;

			if (first) 
				first = FALSE;
			else
				g_string_append (sql, ", ");

			pack = JOINS_PACK (packs->data);
			list = pack->joins;
			while (list && !err) {
				GSList *jnode;
				gint targets_found = 0;
				MgTarget *l_target, *r_target;
				JoinRenderNode *node = NULL;
				gchar *str;

				join = MG_JOIN (list->data);
				l_target = mg_join_get_target_1 (join);
				r_target = mg_join_get_target_2 (join);

				/* Find a JoinRenderNode for the current join (in 'node')*/
				jnode = join_nodes;
				while (jnode && (targets_found < 2)) {
					if (JOIN_RENDER_NODE (jnode->data)->target == l_target)
						targets_found ++;
					if (JOIN_RENDER_NODE (jnode->data)->target == r_target)
						targets_found ++;
					if (targets_found == 2)
						node = JOIN_RENDER_NODE (jnode->data);
					jnode = g_slist_next (jnode);
				}
				g_assert (targets_found <= 2);
				switch (targets_found) {
				case 0:
					node = g_new0 (JoinRenderNode, 1);
					node->target = l_target;
					join_nodes = g_slist_append (join_nodes, node);
				case 1:
					node = g_new0 (JoinRenderNode, 1);
					node->target = r_target;
					node->first_join = join;
					join_nodes = g_slist_append (join_nodes, node);
					break;
				default:
					node->other_joins = g_slist_append (node->other_joins, join);
					break;
				}

				/* render the join condition in 'node->cond' */
				str = render_join_condition (join, context, error, fk_constraints);
				if (str) {
					if (!node->cond)
						node->cond = g_string_new ("");
					if (node->other_joins)
						g_string_append (node->cond, " AND ");
					g_string_append (node->cond, str);
				}
				else 
					err = TRUE;

				list = g_slist_next (list);
			}
			
			/* actual rendering of the joins in the JoinsPack */
			list = join_nodes;
			while (list) {
				if (list != join_nodes)
					g_string_append (sql, " ");
				
				/* join type if applicable */
				if (JOIN_RENDER_NODE (list->data)->first_join) {
					g_string_append (sql, mg_join_render_type (JOIN_RENDER_NODE (list->data)->first_join));
					g_string_append (sql, " ");
				}

				/* target */
				str = mg_renderer_render_as_sql (MG_RENDERER (JOIN_RENDER_NODE (list->data)->target), 
								 context, error);
				
				if (str) {
					g_string_append (sql, str);
					g_free (str);
				}
				else
					err = TRUE;
				g_string_append (sql, " ");

				/* condition */
				if (JOIN_RENDER_NODE (list->data)->cond) 
					g_string_append_printf (sql, " ON (%s)", 
								JOIN_RENDER_NODE (list->data)->cond->str);
				list = g_slist_next (list);
			}

			/* free the list of JoinRenderNodes */
			list = join_nodes;
			while (list) {
				if (JOIN_RENDER_NODE (list->data)->other_joins)
					g_slist_free (JOIN_RENDER_NODE (list->data)->other_joins);
				if (JOIN_RENDER_NODE (list->data)->cond)
					g_string_free (JOIN_RENDER_NODE (list->data)->cond, TRUE);
				g_free (list->data);
				list = g_slist_next (list);
			}
			if (join_nodes)
				g_slist_free (join_nodes);

			/* update the all_joined list */
			all_joined = g_slist_concat (all_joined, g_slist_copy (pack->targets));

			packs = g_slist_next (packs);
		}

		/* Adding targets in no join at all */
		list = query->priv->targets;
		while (list && !err) {
			if (!g_slist_find (all_joined, list->data)) {
				if (first) 
					first = FALSE;
				else
					g_string_append (sql, ", ");
				str = mg_renderer_render_as_sql (MG_RENDERER (list->data), context, error);
				if (str) {
					g_string_append (sql, str);
					g_free (str);
				}
				else
					err = TRUE;
			}
			list = g_slist_next (list);
		}
		g_string_append (sql, " ");
		g_slist_free (all_joined);
	}

	/* GROUP BY */
	/* FIXME */	

	/* condition */
	if (query->priv->cond) {
		g_string_append (sql, "WHERE ");
		str = mg_renderer_render_as_sql (MG_RENDERER (query->priv->cond), context, error);
		if (str) {
			g_string_append (sql, str);
			g_free (str);
		}
		else
			err = TRUE;
	}
	
	/* ORDER BY */
	list = query->priv->fields_order_by;
	while (list) {
		gint pos = mg_query_get_field_index (MG_ENTITY (query), MG_FIELD (list->data));
		if (pos > -1) {
			if (list == query->priv->fields_order_by) 
				g_string_append (sql, " ORDER BY ");
			else
				g_string_append (sql, ", ");

			g_string_append_printf (sql, "%d", pos + 1);
			g_string_append (sql, g_object_get_data (G_OBJECT (list->data), "order_by_asc") ? " ASC" : " DESC");
		}
			
		list = g_slist_next (list);
	}

	if (!err) 
		retval = sql->str;
	else 
		retval = NULL;
	g_string_free (sql, err);

	if (fk_constraints)
		g_slist_free (fk_constraints);

	return retval;
}

static gchar *
render_join_condition (MgJoin *join, MgContext *context, GError **error, GSList *fk_constraints)
{
	GString *string;
	gchar *retval = NULL;
	gboolean joincond = FALSE;
	MgCondition *cond;
	gboolean err = FALSE;

	string = g_string_new ("");

	if ((cond = mg_join_get_condition (join))) {
		joincond = TRUE;
		TO_IMPLEMENT;
		g_string_append (string, "TODO");
	}
	else {
		MgEntity *ent1, *ent2;
		ent1 = mg_target_get_represented_entity (mg_join_get_target_1 (join));
		ent2 = mg_target_get_represented_entity (mg_join_get_target_2 (join));

		if (IS_MG_DB_TABLE (ent1) && IS_MG_DB_TABLE (ent2)) {
			/* find a FK in the database constraints for the two targets */
			GSList *fklist;
			MgDbConstraint *fkcons = NULL, *fkptr;
			gboolean same_order = TRUE;

			fklist = fk_constraints;
			while (fklist && !fkcons) {
				fkptr = MG_DB_CONSTRAINT (fklist->data);
				if ((mg_db_constraint_get_constraint_type (fkptr) == CONSTRAINT_FOREIGN_KEY) &&
				    (((mg_db_constraint_get_table (fkptr) == MG_DB_TABLE (ent1)) &&
				      (mg_db_constraint_fkey_get_ref_table (fkptr) == MG_DB_TABLE (ent2))) ||
				     ((mg_db_constraint_get_table (fkptr) == MG_DB_TABLE (ent2)) &&
				      (mg_db_constraint_fkey_get_ref_table (fkptr) == MG_DB_TABLE (ent1))))) {
					fkcons = fkptr;

					if ((mg_db_constraint_get_table (fkptr) == MG_DB_TABLE (ent2)) &&
					    (mg_db_constraint_fkey_get_ref_table (fkptr) == MG_DB_TABLE (ent1)))
						same_order = FALSE;
				}
				fklist = g_slist_next (fklist);
			}

			if (fkcons) {
				GSList *fkpairs;
				MgDbConstraintFkeyPair *pair;
				gboolean fkfirst = TRUE;

				fkpairs = mg_db_constraint_fkey_get_fields (fkcons);
				fklist = fkpairs;
				while (fklist) {
					pair = MG_DB_CONSTRAINT_FK_PAIR (fklist->data);
					if (fkfirst)
						fkfirst = FALSE;
					else
						g_string_append (string, " AND ");
					if (same_order)
						g_string_append (string, mg_target_get_alias (mg_join_get_target_1 (join)));
					else
						g_string_append (string, mg_target_get_alias (mg_join_get_target_2 (join)));
					g_string_append (string, ".");
					g_string_append (string, mg_field_get_name (MG_FIELD (pair->fkey)));
					g_string_append (string, "=");
					if (same_order)
						g_string_append (string, mg_target_get_alias (mg_join_get_target_2 (join)));
					else
						g_string_append (string, mg_target_get_alias (mg_join_get_target_1 (join)));
					g_string_append (string, ".");
					g_string_append (string, mg_field_get_name (MG_FIELD (pair->ref_pkey)));
					g_free (fklist->data);
					fklist = g_slist_next (fklist);
				}
				g_slist_free (fkpairs);
				joincond = TRUE;
			}
		}
	}
				
	if (!joincond) {
		err = TRUE;
		g_set_error (error,
			     MG_QUERY_ERROR,
			     MG_QUERY_RENDER_ERROR,
			     _("Join has no joining condition"));
	}

	if (err) {
		retval = NULL;
		g_string_free (string, TRUE);
	}
	else {
		retval = string->str;
		g_string_free (string, FALSE);
	}

	return retval;
}


static gchar *
render_sql_insert (MgQuery *query, MgContext *context, GError **error)
{
	GString *sql;
	gchar *retval;
	MgEntity *ent;
	GSList *list;
	gboolean first, err = FALSE;

	/* query is supposed to be active and of a good constitution here */
	sql = g_string_new ("INSERT INTO ");
	ent = mg_target_get_represented_entity (MG_TARGET (query->priv->targets->data));
	g_string_append (sql, mg_base_get_name (MG_BASE (ent)));
	g_string_append (sql, " (");
	list = query->priv->fields;
	first = TRUE;
	while (list) {
		if (mg_qfield_is_visible (MG_QFIELD (list->data))) {
			MgField *field;
			if (first) 
				first = FALSE;
			else
				g_string_append (sql, ", ");
			field = mg_qf_field_get_ref_field (MG_QF_FIELD (list->data));
			g_string_append (sql, mg_field_get_name (field));
		}
		list = g_slist_next (list);
	}
	g_string_append (sql, ") ");

	if (query->priv->sub_queries) {
		gchar *str = mg_query_render_as_sql (MG_RENDERER (query->priv->sub_queries->data), context, error);
		if (str) {
			g_string_append (sql, str);
			g_free (str);
		}
		else
			err = FALSE;
	}
	else {
		GSList *vfields;

		g_string_append (sql, "VALUES (");
		vfields = mg_entity_get_visible_fields (MG_ENTITY (query));
		first = TRUE;
		list = vfields;
		while (list && !err) {
			MgField *value_prov;

			if (first) 
				first = FALSE;
			else
				g_string_append (sql, ", ");

			g_object_get (G_OBJECT (list->data), "value_provider", &value_prov, NULL);
			if (value_prov) {
				gchar *str;
				str = mg_renderer_render_as_sql (MG_RENDERER (value_prov), context, error);
				if (str) {
					g_string_append (sql, str);
					g_free (str);
				}
				else {
					if (error && *error)
						err = TRUE;
					else
						g_string_append (sql, "NULL");
				}
			}
			else {
				/* fetch the value in the context */
				TO_IMPLEMENT;
			}

			list = g_slist_next (list);
		}
		g_slist_free (vfields);
		g_string_append (sql, ")");
	}

	if (!err)
		retval = sql->str;
	else
		retval = NULL;

	g_string_free (sql, err);
	return retval;
}

static gchar *
render_sql_update (MgQuery *query, MgContext *context, GError **error)
{
	GString *sql;
	gchar *retval;
	MgEntity *ent;
	GSList *list;
	gboolean first, err = FALSE;

	/* query is supposed to be active and of a good constitution here */
	sql = g_string_new ("UPDATE ");
	ent = mg_target_get_represented_entity (MG_TARGET (query->priv->targets->data));
	g_string_append (sql, mg_base_get_name (MG_BASE (ent)));
	g_string_append (sql, " SET ");
	list = query->priv->fields;
	first = TRUE;
	while (list && !err) {
		if (mg_qfield_is_visible (MG_QFIELD (list->data))) {
			MgField *field, *value_prov;
			if (first) 
				first = FALSE;
			else
				g_string_append (sql, ", ");
			field = mg_qf_field_get_ref_field (MG_QF_FIELD (list->data));
			g_string_append (sql, mg_field_get_name (field));
			g_string_append (sql, "=");

			g_object_get (G_OBJECT (list->data), "value_provider", &value_prov, NULL);
			if (value_prov) {
				gchar *str;
				str = mg_renderer_render_as_sql (MG_RENDERER (value_prov), context, error);
				if (str) {
					g_string_append (sql, str);
					g_free (str);
				}
				else {
					if (error && *error)
						err = TRUE;
					else
						g_string_append (sql, "NULL");
				}
			}
			else {
				/* signal an error */
				g_set_error (error,
					     MG_QUERY_ERROR,
					     MG_QUERY_RENDER_ERROR,
					     _("Missing values"));
				err = TRUE;
			}
		}
		list = g_slist_next (list);
	}
	g_string_append (sql, " ");


	if (!err && query->priv->cond) {
		gchar *str;
		g_string_append (sql, "WHERE ");
		str = mg_renderer_render_as_sql (MG_RENDERER (query->priv->cond), context, error);
		if (str) {
			g_string_append (sql, str);
			g_free (str);
		}
		else
			err = TRUE;
	}

	if (!err)
		retval = sql->str;
	else
		retval = NULL;

	g_string_free (sql, err);
	return retval;
}

static gchar *
render_sql_delete (MgQuery *query, MgContext *context, GError **error)
{
	GString *sql;
	gchar *retval;
	MgEntity *ent;
	gboolean err = FALSE;

	/* query is supposed to be active and of a good constitution here */
	sql = g_string_new ("DELETE FROM ");
	ent = mg_target_get_represented_entity (MG_TARGET (query->priv->targets->data));
	g_string_append (sql, mg_base_get_name (MG_BASE (ent)));

	if (query->priv->cond) {
		gchar *str;
		g_string_append (sql, " WHERE ");
		str = mg_renderer_render_as_sql (MG_RENDERER (query->priv->cond), context, error);
		if (str) {
			g_string_append (sql, str);
			g_free (str);
		}
		else
			err = TRUE;
	}

	if (!err)
		retval = sql->str;
	else
		retval = NULL;

	g_string_free (sql, FALSE);
	return retval;
}

static gchar *
render_sql_union (MgQuery *query, MgContext *context, GError **error)
{
	GString *sql;
	gchar *retval, *str;
	GSList *list;
	gboolean first = TRUE;
	gboolean err = FALSE;

	/* query is supposed to be active here, and of good constitution */
	sql = g_string_new ("");
	list = query->priv->sub_queries;
	while (list && !err) {
		if (first)
			first = FALSE;
		else
			g_string_append (sql, " UNION ");
		str = mg_query_render_as_sql (MG_RENDERER (list->data), context, error);
		if (str) {
			g_string_append_printf (sql, "(%s)", str);
			g_free (str);
		}
		else err = TRUE;

		list = g_slist_next (list);
	}

	if (!err)
		retval = sql->str;
	else
		retval = NULL;

	g_string_free (sql, err);
	return retval;
}

static gchar *
render_sql_intersect (MgQuery *query, MgContext *context, GError **error)
{
	GString *sql;
	gchar *retval, *str;
	GSList *list;
	gboolean first = TRUE;
	gboolean err = FALSE;

	/* query is supposed to be active here, and of good constitution */
	sql = g_string_new ("");
	list = query->priv->sub_queries;
	while (list && !err) {
		if (first)
			first = FALSE;
		else
			g_string_append (sql, " INTERSECT ");
		str = mg_query_render_as_sql (MG_RENDERER (list->data), context, error);
		if (str) {
			g_string_append_printf (sql, "(%s)", str);
			g_free (str);
		}
		else err = TRUE;

		list = g_slist_next (list);
	}

	if (!err)
		retval = sql->str;
	else
		retval = NULL;

	g_string_free (sql, err);
	return retval;
}

static gchar *
render_sql_except (MgQuery *query, MgContext *context, GError **error)
{
	GString *sql;
	gchar *retval, *str;
	GSList *list;
	gboolean first = TRUE;
	gboolean err = FALSE;

	/* query is supposed to be active here, and of good constitution */
	sql = g_string_new ("");
	list = query->priv->sub_queries;
	while (list && !err) {
		if (first)
			first = FALSE;
		else
			g_string_append (sql, " EXCEPT ");
		str = mg_query_render_as_sql (MG_RENDERER (list->data), context, error);
		if (str) {
			g_string_append_printf (sql, "(%s)", str);
			g_free (str);
		}
		else err = TRUE;

		list = g_slist_next (list);
	}

	if (!err)
		retval = sql->str;
	else
		retval = NULL;

	g_string_free (sql, err);
	return retval;
}


static gchar *
mg_query_render_as_str (MgRenderer *iface, MgContext *context)
{
	MgQuery *query;
	gchar *str;
	const gchar *cstr;

	g_return_val_if_fail (iface && IS_MG_QUERY (iface), NULL);
	g_return_val_if_fail (MG_QUERY (iface)->priv, NULL);
	query = MG_QUERY (iface);

	cstr = mg_base_get_name (MG_BASE (query));
	if (cstr && *cstr)
		str = g_strdup_printf (_("Query '%s'"), cstr);
	else
		str = g_strdup (_("Unnamed Query"));
	       
	return str;
}
