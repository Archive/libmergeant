/* mg-conf.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <string.h>
#include "mg-conf.h"
#include "marshal.h"
#include <libxml/parser.h>
#include <libxml/tree.h>
#include "mg-base.h"
#include "mg-server.h"
#include "mg-database.h"
#include "mg-xml-storage.h"
#include "mg-query.h"
#include "mg-referer.h"

/* 
 * Main static functions 
 */
static void mg_conf_class_init (MgConfClass * class);
static void mg_conf_init (MgConf * srv);
static void mg_conf_dispose (GObject   * object);
static void mg_conf_finalize (GObject   * object);

static void mg_conf_set_property (GObject              *object,
				  guint                 param_id,
				  const GValue         *value,
				  GParamSpec           *pspec);
static void mg_conf_get_property (GObject              *object,
				  guint                 param_id,
				  GValue               *value,
				  GParamSpec           *pspec);

static gboolean mg_conf_load_queries (MgConf *conf, xmlNodePtr queries, GError **error);


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	QUERY_ADDED,
	QUERY_REMOVED,
	LAST_SIGNAL
};

static gint mg_conf_signals[LAST_SIGNAL] = { 0, 0};

/* properties */
enum
{
	PROP_0,
	PROP_SERIAL_QUERY
};


struct _MgConfPrivate
{
	guint              serial_query; /* counter */
	GSList            *assumed_queries;   /* list of MgQuery objects */
	GSList            *all_queries;
	MgDatabase        *database;
	MgServer          *srv;
};

/* module error */
GQuark mg_conf_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_conf_error");
	return quark;
}


guint
mg_conf_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgConfClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_conf_class_init,
			NULL,
			NULL,
			sizeof (MgConf),
			0,
			(GInstanceInitFunc) mg_conf_init
		};
		
		type = g_type_register_static (G_TYPE_OBJECT, "MgConf", &info, 0);
	}
	return type;
}

static void
mg_conf_class_init (MgConfClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_conf_signals[QUERY_ADDED] =
		g_signal_new ("query_added",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgConfClass, query_added),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE,
			      1, G_TYPE_POINTER);
	mg_conf_signals[QUERY_REMOVED] =
		g_signal_new ("query_removed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgConfClass, query_removed),
			      NULL, NULL,
			      marshal_VOID__POINTER, G_TYPE_NONE,
			      1, G_TYPE_POINTER);
	class->query_added = NULL;
	class->query_removed = NULL;

	/* Properties */
	object_class->set_property = mg_conf_set_property;
	object_class->get_property = mg_conf_get_property;
	g_object_class_install_property (object_class, PROP_SERIAL_QUERY,
					 g_param_spec_uint ("query_serial", NULL, NULL, 
							    1, G_MAXUINT, 1, G_PARAM_READABLE));

	object_class->dispose = mg_conf_dispose;
	object_class->finalize = mg_conf_finalize;
}

static void
mg_conf_init (MgConf * conf)
{
	conf->priv = g_new0 (MgConfPrivate, 1);
	conf->priv->serial_query = 1;
	conf->priv->assumed_queries = NULL;
	conf->priv->all_queries = NULL;
	conf->priv->database = NULL;
	conf->priv->srv = NULL;
}

/**
 * mg_conf_new
 *
 * Create a new #MgConf object.
 *
 * Returns: the newly created object.
 */
GObject   *
mg_conf_new ()
{
	GObject   *obj;
	MgConf *conf;

	obj = g_object_new (MG_CONF_TYPE, NULL);
	conf = MG_CONF (obj);

	/* Server and database objects creation */
	conf->priv->srv = MG_SERVER (mg_server_new (conf));
	conf->priv->database = MG_DATABASE (mg_database_new (conf));

	return obj;
}


static void
mg_conf_dispose (GObject   * object)
{
	MgConf *conf;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_CONF (object));

	conf = MG_CONF (object);
	if (conf->priv) {
		while (conf->priv->assumed_queries)
			mg_base_nullify (MG_BASE (conf->priv->assumed_queries->data));
		
		if (conf->priv->database) {
			g_object_unref (G_OBJECT (conf->priv->database));
			conf->priv->database = NULL;
		}
		
		if (conf->priv->srv) {
			g_object_unref (G_OBJECT (conf->priv->srv));
			conf->priv->srv = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_conf_finalize (GObject   * object)
{
	MgConf *conf;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_CONF (object));

	conf = MG_CONF (object);
	if (conf->priv) {
		g_free (conf->priv);
		conf->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

static void 
mg_conf_set_property (GObject              *object,
		       guint                 param_id,
		       const GValue         *value,
		       GParamSpec           *pspec)
{
	MgConf *mg_conf;

	mg_conf = MG_CONF (object);
	if (mg_conf->priv) {
		switch (param_id) {
		case PROP_SERIAL_QUERY:
			mg_conf->priv->serial_query = g_value_get_uint (value);
			break;
		}
	}
}

static void
mg_conf_get_property (GObject              *object,
		       guint                 param_id,
		       GValue               *value,
		       GParamSpec           *pspec)
{
	MgConf *mg_conf;
	mg_conf = MG_CONF (object);
	
	if (mg_conf->priv) {
		switch (param_id) {
		case PROP_SERIAL_QUERY:
			g_value_set_uint (value, mg_conf->priv->serial_query++);
			break;
		}	
	}
}


static void xml_validity_error_func (void *ctx, const char *msg, ...);

/**
 * mg_conf_load_xml_file
 * @conf: a #MgConf object
 * @xmlfile: the name of the file to which the XML will be written to
 * @error: location to store error, or %NULL
 *
 * Loads an XML file which respects the Mergeant DTD, and creates all the necessary
 * objects that are defined within the XML file. During the creation of the other
 * objects, all the normal signals are emitted.
 *
 * If the MgConf object already has some contents, then it is first of all
 * nullified (to return its state as when it was first created).
 *
 * If an error occurs during loading then the MgConf object is left as empty
 * as when it is first created.
 *
 * Returns: TRUE if loading was successfull and FALSE otherwise.
 */
gboolean
mg_conf_load_xml_file (MgConf *conf, const gchar *xmlfile, GError **error)
{
	xmlDocPtr doc;
	xmlNodePtr node, subnode;

	g_return_val_if_fail (conf && IS_MG_CONF (conf), FALSE);
	g_return_val_if_fail (conf->priv, FALSE);
	g_return_val_if_fail (xmlfile && *xmlfile, FALSE);

	if (! g_file_test (xmlfile, G_FILE_TEST_EXISTS)) {
		g_set_error (error,
			     MG_CONF_ERROR,
			     MG_CONF_FILE_LOAD_ERROR,
			     "File '%s' does not exist", xmlfile);
		return FALSE;
	}

	doc = xmlParseFile (xmlfile);

	if (doc) {
		/* doc validation */
		xmlValidCtxtPtr validc;

		validc = g_new0 (xmlValidCtxt, 1);
		validc->userData = conf;
		validc->error = xml_validity_error_func;
		validc->warning = NULL; 
		xmlDoValidityCheckingDefaultValue = 1;
		if (! xmlValidateDocument (validc, doc)) {
			gchar *str;

			xmlFreeDoc (doc);
			g_free (validc);
			str = g_object_get_data (G_OBJECT (conf), "xmlerror");
			if (str) {
				g_set_error (error,
					     MG_CONF_ERROR,
					     MG_CONF_FILE_LOAD_ERROR,
					     "File '%s' does not conform to DTD:\n%s", xmlfile, str);
				g_free (str);
				g_object_set_data (G_OBJECT (conf), "xmlerror", NULL);
			}
			else 
				g_set_error (error,
					     MG_CONF_ERROR,
					     MG_CONF_FILE_LOAD_ERROR,
					     "File '%s' does not conform to DTD", xmlfile);

			return FALSE;
		}
		g_free (validc);
	}
	else {
		g_set_error (error,
			     MG_CONF_ERROR,
			     MG_CONF_FILE_LOAD_ERROR,
			     "Can't load file '%s'", xmlfile);
		return FALSE;
	}

	/* doc is now OK */
	node = xmlDocGetRootElement (doc);
	if (strcmp (node->name, "MG_CONF")) {
		g_set_error (error,
			     MG_CONF_ERROR,
				     MG_CONF_FILE_LOAD_ERROR,
			     "XML file '%s' does not have any <MG_CONF> node", xmlfile);
		return FALSE;
	}
	subnode = node->children;
	
	if (!subnode) {
		g_set_error (error,
			     MG_CONF_ERROR,
			     MG_CONF_FILE_LOAD_ERROR,
			     "XML file '%s': <MG_CONF> does not have any children",
			     xmlfile);
		return FALSE;
	}

	/* MgServer object */
	if (xmlNodeIsText (subnode)) 
		subnode = subnode->next;

	if (strcmp (subnode->name, "MG_SERVER")) {
		g_set_error (error,
			     MG_CONF_ERROR,
			     MG_CONF_FILE_LOAD_ERROR,
			     "XML file '%s': <MG_SERVER> not first child of <MG_CONF>",
			     xmlfile);
		return FALSE;
	}
	if (!mg_xml_storage_load_from_xml (MG_XML_STORAGE (conf->priv->srv), subnode, error))
		return FALSE;
	
	/* MgDatabase object */
	subnode = subnode->next;
	if (xmlNodeIsText (subnode)) 
		subnode = subnode->next;
	if (!subnode || strcmp (subnode->name, "MG_DATABASE")) {
		g_set_error (error,
			     MG_CONF_ERROR,
			     MG_CONF_FILE_LOAD_ERROR,
			     "XML file '%s': <MG_DATABASE> not second child of <MG_CONF>",
			     xmlfile);
		return FALSE;
	}
	if (!mg_xml_storage_load_from_xml (MG_XML_STORAGE (conf->priv->database), subnode, error))
		return FALSE;

	/* MgQuery objects */
	subnode = subnode->next;
	if (xmlNodeIsText (subnode)) 
		subnode = subnode->next;
	if (!subnode || strcmp (subnode->name, "MG_QUERIES")) {
		g_set_error (error,
			     MG_CONF_ERROR,
			     MG_CONF_FILE_LOAD_ERROR,
			     "XML file '%s': <MG_QUERIES> not 3rd child of <MG_CONF>",
			     xmlfile);
		return FALSE;
	}
	if (!mg_conf_load_queries (conf, subnode, error))
		return FALSE;
	

	/* Other objects... FIXME */

	xmlFreeDoc (doc);

	return TRUE;
}

static gboolean
mg_conf_load_queries (MgConf *conf, xmlNodePtr queries, GError **error)
{
	xmlNodePtr qnode = queries->children;
	gboolean allok = TRUE;
	
	while (qnode && allok) {
		if (!strcmp (qnode->name, "MG_QUERY")) {
			MgQuery *query;

			query = MG_QUERY (mg_query_new (conf));
			allok = mg_xml_storage_load_from_xml (MG_XML_STORAGE (query), qnode, error);
			mg_conf_assume_query (conf, query);
			g_object_unref (G_OBJECT (query));
		}
		qnode = qnode->next;
	}

	if (allok) {
		GSList *list = conf->priv->assumed_queries;
		while (list) {
			mg_referer_activate (MG_REFERER (list->data));
			list= g_slist_next (list);
		}
	}

	return allok;
}


/*
 * function called when an error occured during the document validation
 */
static void
xml_validity_error_func (void *ctx, const char *msg, ...)
{
        xmlValidCtxtPtr pctxt;
        va_list args;
        gchar *str, *str2, *newerr;
	MgConf *conf;

	pctxt = (xmlValidCtxtPtr) ctx;
	g_print ("ERROR CONF: %p\n", pctxt->userData);
	g_print ("ERROR CTXT: %p\n", ctx);
	/* FIXME: it looks like libxml does set ctx to userData... */
	/*conf = MG_CONF (pctxt->userData);*/
	conf = MG_CONF (pctxt);
	str2 = g_object_get_data (G_OBJECT (conf), "xmlerror");

        va_start (args, msg);
        str = g_strdup_vprintf (msg, args);
        va_end (args);
	
	if (str2) {
		newerr = g_strdup_printf ("%s\n%s", str2, str);
		g_free (str2);
	}
	else
		newerr = g_strdup (str);
        g_free (str);
	g_object_set_data (G_OBJECT (conf), "xmlerror", newerr);
}


/**
 * mg_conf_save_xml_file
 * @conf: a #MgConf object
 * @xmlfile: the name of the file to which the XML will be written to
 * @error: location to store error, or %NULL
 *
 * Saves the contents of a MgConf object to a file which is given as argument.
 *
 * Returns: TRUE if saving was successfull and FALSE otherwise.
 */
gboolean
mg_conf_save_xml_file (MgConf *conf, const gchar *xmlfile, GError **error)
{
	gboolean retval = TRUE;
	xmlDocPtr doc;
#define XML_LIBMG_DTD_FILE DTDINSTALLDIR"/libmergeant.dtd"

	g_return_val_if_fail (conf && IS_MG_CONF (conf), FALSE);
	g_return_val_if_fail (conf->priv, FALSE);
	
	doc = xmlNewDoc ("1.0");
	if (doc) {
		xmlNodePtr topnode, node;

		/* DTD insertion */
                xmlCreateIntSubset(doc, "MG_CONF", NULL, XML_LIBMG_DTD_FILE);

		/* Top node */
		topnode = xmlNewDocNode (doc, NULL, "MG_CONF", NULL);
		xmlDocSetRootElement (doc, topnode);

		/* MgServer */
		node = mg_xml_storage_save_to_xml (MG_XML_STORAGE (conf->priv->srv), error);
		if (node)
			xmlAddChild (topnode, node);
		else 
			/* error handling */
			retval = FALSE;
		
		/* MgDatabase */
		if (retval) {
			node = mg_xml_storage_save_to_xml (MG_XML_STORAGE (conf->priv->database), error);
			if (node)
				xmlAddChild (topnode, node);
			else 
				/* error handling */
				retval = FALSE;
		}
		
		/* MgQueries */
		if (retval) {
			GSList *list;
			node = xmlNewChild (topnode, NULL, "MG_QUERIES", NULL);
			list = conf->priv->assumed_queries;
			while (list) {
				if (!mg_query_get_parent_query (MG_QUERY (list->data))) {
					/* We only add nodes for queries which do not have any parent query */
					xmlNodePtr qnode;
					
					qnode = mg_xml_storage_save_to_xml (MG_XML_STORAGE (list->data), error);
					if (qnode)
						xmlAddChild (node, qnode);
					else 
						/* error handling */
						retval = FALSE;
				}
				list = g_slist_next(list);
			}
		}

		if (retval) {
			gint i;
			i = xmlSaveFormatFile (xmlfile, doc, TRUE);
			if (i == -1) {
				/* error handling */
				g_set_error (error, MG_CONF_ERROR, MG_CONF_FILE_SAVE_ERROR,
					     _("Error writing XML file %s"), xmlfile);
				retval = FALSE;
			}
		}

		/* getting rid of the doc */
		xmlFreeDoc (doc);
	}
	else {
		/* error handling */
		g_set_error (error, MG_CONF_ERROR, MG_CONF_FILE_SAVE_ERROR,
			     _("Can't allocate memory for XML structure."));
		retval = FALSE;
	}

	return retval;
}

static void query_id_changed_cb (MgQuery *query, MgConf *conf);
static void query_nullified_cb (MgQuery *query, MgConf *conf);
static void query_weak_ref_notify (MgConf *conf, MgQuery *query);

/**
 * mg_conf_declare_query
 * @conf: a #MgConf object
 * @query: a #MgQuery object
 *
 * Declares the existance of a new query to @conf. All the #MgQuery objects MUST
 * be declared to the corresponding #MgConf object for the library to work correctly.
 * Once @query has been declared, @conf does not hold any reference to @query. If @conf
 * must hold such a reference, then use mg_conf_assume_query().
 *
 * This functions is called automatically from each mg_query_new* function, and it should not be necessary
 * to call it except for classes extending the #MgQuery class.
 */
void
mg_conf_declare_query (MgConf *conf, MgQuery *query)
{
	g_return_if_fail (conf && IS_MG_CONF (conf));
	g_return_if_fail (conf->priv);
	g_return_if_fail (query && IS_MG_QUERY (query));
	
	/* we don't take any reference on query */
	if (g_slist_find (conf->priv->all_queries, query))
		return;	
	else {
		conf->priv->all_queries = g_slist_append (conf->priv->all_queries, query);
		g_object_weak_ref (G_OBJECT (query), (GWeakNotify) query_weak_ref_notify, conf);

		/* make sure the conf->priv->serial_query value is always 1 above this query's id */
		query_id_changed_cb (query, conf);
		g_signal_connect (G_OBJECT (query), "id_changed",
				  G_CALLBACK (query_id_changed_cb), conf);
	}
}

static void
query_id_changed_cb (MgQuery *query, MgConf *conf)
{
	if (conf->priv->serial_query <= mg_base_get_id (MG_BASE (query)))
		conf->priv->serial_query = mg_base_get_id (MG_BASE (query)) + 1;
}


static void
query_weak_ref_notify (MgConf *conf, MgQuery *query)
{
	conf->priv->all_queries = g_slist_remove (conf->priv->all_queries, query);
	g_signal_handlers_disconnect_by_func (G_OBJECT (query),
					      G_CALLBACK (query_id_changed_cb), conf);
}

/**
 * mg_conf_assume_query
 * @conf: a #MgConf object
 * @query: a #MgQuery object
 *
 * Force @conf to manage @query: it will get a reference to it.
 */
void
mg_conf_assume_query (MgConf *conf, MgQuery *query)
{
	g_return_if_fail (conf && IS_MG_CONF (conf));
	g_return_if_fail (conf->priv);
	g_return_if_fail (query && IS_MG_QUERY (query));
	
	if (g_slist_find (conf->priv->assumed_queries, query))
		return;

	mg_conf_declare_query (conf, query);

	conf->priv->assumed_queries = g_slist_append (conf->priv->assumed_queries, query);
	g_object_ref (G_OBJECT (query));
	g_signal_connect (G_OBJECT (query), "nullified",
			  G_CALLBACK (query_nullified_cb), conf);
#ifdef debug_signal
	g_print (">> 'QUERY_ADDED' from mg_conf_assume_query\n");
#endif
	g_signal_emit (G_OBJECT (conf), mg_conf_signals[QUERY_ADDED], 0, query);
#ifdef debug_signal
	g_print ("<< 'QUERY_ADDED' from mg_conf_assume_query\n");
#endif
}


/* called when a MgQuery is "nullified" */
static void 
query_nullified_cb (MgQuery *query, MgConf *conf)
{
	mg_conf_unassume_query (conf, query);
}


/**
 * mg_conf_unassume_query
 * @conf: a #MgConf object
 * @query: a #MgQuery object
 *
 * Forces @conf to lose a reference it has on @query
 */
void
mg_conf_unassume_query (MgConf *conf, MgQuery *query)
{
	g_return_if_fail (conf && IS_MG_CONF (conf));
	g_return_if_fail (conf->priv);

	if (g_slist_find (conf->priv->assumed_queries, query)) {
		conf->priv->assumed_queries = g_slist_remove (conf->priv->assumed_queries, query);
		g_signal_handlers_disconnect_by_func (G_OBJECT (query),
						      G_CALLBACK (query_nullified_cb), conf);
#ifdef debug_signal
		g_print (">> 'QUERY_REMOVED' from mg_conf_unassume_query\n");
#endif
		g_signal_emit (G_OBJECT (conf), mg_conf_signals[QUERY_REMOVED], 0, query);
#ifdef debug_signal
		g_print ("<< 'QUERY_REMOVED' from mg_conf_unassume_query\n");
#endif
		g_object_unref (G_OBJECT (query));
	}
}


/**
 * mg_conf_get_queries
 * @conf: a #MgConf object
 *
 * Get a list of all the non interdependant queries managed by @conf
 * (only queries with no parent query are listed)
 *
 * Returns: a new list of #MgQuery objects
 */
GSList *
mg_conf_get_queries (MgConf *conf)
{
	GSList *list, *retval = NULL;

	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);
	g_return_val_if_fail (conf->priv, NULL);

	list = conf->priv->assumed_queries;
	while (list) {
		if (! mg_query_get_parent_query (MG_QUERY (list->data)))
			retval = g_slist_append (retval, list->data);
		list = g_slist_next (list);
	}

	return retval;
}

/**
 * mg_conf_get_query_by_xml_id
 * @conf: a #MgConf object
 * @xml_id: the XML Id of the query being searched
 *
 * Find a #MgQuery object from its XML Id
 *
 * Returns: the #MgQuery object, or NULL if not found
 */
MgQuery *
mg_conf_get_query_by_xml_id (MgConf *conf, const gchar *xml_id)
{
	MgQuery *query = NULL;

	GSList *list;
        gchar *str;

        g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);
        g_return_val_if_fail (conf->priv, NULL);

        list = conf->priv->all_queries;
        while (list && !query) {
                str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
                if (!strcmp (str, xml_id))
                        query = MG_QUERY (list->data);
                g_free (str);
                list = g_slist_next (list);
        }

        return query;
}

/**
 * mg_conf_get_server
 * @conf: a #MgConf object
 *
 * Fetch a pointer to the MgServer used by the MgConf object.
 *
 * Returns: a pointer to the MgServer
 */
MgServer *
mg_conf_get_server (MgConf *conf)
{
	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);
	g_return_val_if_fail (conf->priv, NULL);

	return conf->priv->srv;
}

/**
 * mg_conf_get_database
 * @conf: a #MgConf object
 *
 * Fetch a pointer to the MgDatabase used by the MgConf object.
 *
 * Returns: a pointer to the MgDatabase
 */
MgDatabase *
mg_conf_get_database (MgConf *conf)
{
	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);
	g_return_val_if_fail (conf->priv, NULL);

	return conf->priv->database;
}

#ifdef debug
/**
 * mg_conf_dump
 * @conf: a #MgConf object
 *
 * Dumps the whole configuration managed by the MgConf object
 */
void
mg_conf_dump (MgConf *conf)
{
	GSList *list;

	g_return_if_fail (conf && IS_MG_CONF (conf));
	g_return_if_fail (conf->priv);

	g_print ("\n----------------- DUMPING START -----------------\n");
	g_print (D_COL_H1 "MgConf %p\n" D_COL_NOR, conf);
	if (conf->priv->srv) 
		mg_server_dump (conf->priv->srv, 0);

	if (conf->priv->database)
		mg_base_dump (MG_BASE (conf->priv->database), 0);

	list = conf->priv->assumed_queries;
	if (list)
		g_print ("Queries:\n");
	else
		g_print ("No Query defined\n");
	while (list) {
		if (!mg_query_get_parent_query (MG_QUERY (list->data)))
			mg_base_dump (MG_BASE (list->data), 0);
		list = g_slist_next (list);
	}

	g_print ("----------------- DUMPING END -----------------\n\n");
}
#endif
