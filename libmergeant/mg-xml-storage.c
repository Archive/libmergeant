/* mg-xml-storage.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-xml-storage.h"
#include "marshal.h"


/* signals */
enum
{
	LAST_SIGNAL
};

static gint mg_xml_storage_signals[LAST_SIGNAL] = { };

static void mg_xml_storage_iface_init (gpointer g_class);

GType
mg_xml_storage_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgXmlStorageIface),
			(GBaseInitFunc) mg_xml_storage_iface_init,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) NULL,
			NULL,
			NULL,
			0,
			0,
			(GInstanceInitFunc) NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, "MgXmlStorage", &info, 0);
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
	}
	return type;
}


static void
mg_xml_storage_iface_init (gpointer g_class)
{
	static gboolean initialized = FALSE;

	if (! initialized) {
		initialized = TRUE;
	}
}


/**
 * mg_xml_storage_get_xml_id
 * @iface: an object which implements the #MgXmlStorage interface
 * 
 * Fetch the xml id string of the object, it's up to the caller to
 * free the string.
 * 
 * Returns: the xml id.
 */
gchar *
mg_xml_storage_get_xml_id (MgXmlStorage *iface)
{
	g_return_val_if_fail (iface && IS_MG_XML_STORAGE (iface), NULL);

	if (MG_XML_STORAGE_GET_IFACE (iface)->get_xml_id)
		return (MG_XML_STORAGE_GET_IFACE (iface)->get_xml_id) (iface);
	
	return NULL;
}

/**
 * mg_xml_storage_save_to_xml
 * @iface: an object which implements the #MgXmlStorage interface
 * @error: location to store error, or %NULL
 * 
 * Creates a new xmlNodePtr structure and fills it with data representing the
 * object given as argument.
 * 
 * Returns: the new XML node, or NULL if an error occured.
 */
xmlNodePtr 
mg_xml_storage_save_to_xml (MgXmlStorage *iface, GError **error)
{
	g_return_val_if_fail (iface && IS_MG_XML_STORAGE (iface), NULL);

	if (MG_XML_STORAGE_GET_IFACE (iface)->save_to_xml)
		return (MG_XML_STORAGE_GET_IFACE (iface)->save_to_xml) (iface, error);
	
	return NULL;
}


/**
 * mg_xml_storage_load_from_xml
 * @iface: an object which implements the #MgXmlStorage interface
 * @node: an XML node from an XML structure
 * @error: location to store error, or %NULL
 * 
 * Updates the object with data stored in the XML node. The object MUST already
 * exist and be of the correct type before calling this function. 
 * This is a virtual function.
 * 
 * Returns: TRUE if no error occured.
 */
gboolean 
mg_xml_storage_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	g_return_val_if_fail (iface && IS_MG_XML_STORAGE (iface), FALSE);

	if (MG_XML_STORAGE_GET_IFACE (iface)->load_from_xml)
		return (MG_XML_STORAGE_GET_IFACE (iface)->load_from_xml) (iface, node, error);
	
	return TRUE;
}
