/* mg-qf-func.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-qf-func.h"
#include "mg-xml-storage.h"
#include "mg-field.h"
#include "mg-entity.h"
#include "mg-renderer.h"
#include "mg-referer.h"
#include "mg-ref-base.h"
#include "marshal.h"
#include "mg-query.h"
#include <string.h>

/* 
 * Main static functions 
 */
static void mg_qf_func_class_init (MgQfFuncClass * class);
static void mg_qf_func_init (MgQfFunc *qf);
static void mg_qf_func_dispose (GObject *object);
static void mg_qf_func_finalize (GObject *object);

static void mg_qf_func_set_property (GObject              *object,
				      guint                 param_id,
				      const GValue         *value,
				      GParamSpec           *pspec);
static void mg_qf_func_get_property (GObject              *object,
				      guint                 param_id,
				      GValue               *value,
				      GParamSpec           *pspec);

/* XML storage interface */
static void        mg_qf_func_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_qf_func_get_xml_id (MgXmlStorage *iface);
static xmlNodePtr  mg_qf_func_save_to_xml (MgXmlStorage *iface, GError **error);
static gboolean    mg_qf_func_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error);

/* Field interface */
static void              mg_qf_func_field_init      (MgFieldIface *iface);
static MgEntity         *mg_qf_func_get_entity      (MgField *iface);
static MgServerDataType *mg_qf_func_get_data_type   (MgField *iface);
static const gchar      *mg_qf_func_get_name        (MgField *iface);
static const gchar      *mg_qf_func_get_description (MgField *iface);

/* Renderer interface */
static void            mg_qf_func_renderer_init   (MgRendererIface *iface);
static GdaXqlItem     *mg_qf_func_render_as_xql   (MgRenderer *iface, MgContext *context, GError **error);
static gchar          *mg_qf_func_render_as_sql   (MgRenderer *iface, MgContext *context, GError **error);
static gchar          *mg_qf_func_render_as_str   (MgRenderer *iface, MgContext *context);

/* Referer interface */
static void        mg_qf_func_referer_init        (MgRefererIface *iface);
static gboolean    mg_qf_func_activate            (MgReferer *iface);
static void        mg_qf_func_deactivate          (MgReferer *iface);
static gboolean    mg_qf_func_is_active           (MgReferer *iface);
static GSList     *mg_qf_func_get_ref_objects     (MgReferer *iface);
static void        mg_qf_func_replace_refs        (MgReferer *iface, GHashTable *replacements);

/* virtual functions */
static GObject    *mg_qf_func_copy           (MgQfield *orig);
static gboolean    mg_qf_func_is_equal       (MgQfield *qfield1, MgQfield *qfield2);


#ifdef debug
static void        mg_qf_func_dump           (MgQfFunc *func, guint offset);
#endif

/* When the MgQuery or MgTarget is nullified */
static void nullified_object_cb (GObject *obj, MgQfFunc *func);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	TEMPL_SIGNAL,
	LAST_SIGNAL
};

static gint mg_qf_func_signals[LAST_SIGNAL] = { 0 };

/* properties */
enum
{
	PROP_0,
	PROP_QUERY,
};


/* private structure */
struct _MgQfFuncPrivate
{
	MgQuery    *query;
	MgRefBase  *func_ref;  /* references a MgServerFunction */
	GSList     *args;      /* list of MgRefBase objects */
};


/* module error */
GQuark mg_qf_func_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_qf_func_error");
	return quark;
}


guint
mg_qf_func_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgQfFuncClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_qf_func_class_init,
			NULL,
			NULL,
			sizeof (MgQfFunc),
			0,
			(GInstanceInitFunc) mg_qf_func_init
		};

		static const GInterfaceInfo xml_storage_info = {
			(GInterfaceInitFunc) mg_qf_func_xml_storage_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo field_info = {
			(GInterfaceInitFunc) mg_qf_func_field_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo renderer_info = {
			(GInterfaceInitFunc) mg_qf_func_renderer_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo referer_info = {
			(GInterfaceInitFunc) mg_qf_func_referer_init,
			NULL,
			NULL
		};
		
		
		type = g_type_register_static (MG_QFIELD_TYPE, "MgQfFunc", &info, 0);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
		g_type_add_interface_static (type, MG_FIELD_TYPE, &field_info);
		g_type_add_interface_static (type, MG_RENDERER_TYPE, &renderer_info);
		g_type_add_interface_static (type, MG_REFERER_TYPE, &referer_info);
	}
	return type;
}

static void 
mg_qf_func_xml_storage_init (MgXmlStorageIface *iface)
{
	iface->get_xml_id = mg_qf_func_get_xml_id;
	iface->save_to_xml = mg_qf_func_save_to_xml;
	iface->load_from_xml = mg_qf_func_load_from_xml;
}

static void
mg_qf_func_field_init (MgFieldIface *iface)
{
	iface->get_entity = mg_qf_func_get_entity;
	iface->get_data_type = mg_qf_func_get_data_type;
	iface->get_name = mg_qf_func_get_name;
	iface->get_description = mg_qf_func_get_description;
}

static void
mg_qf_func_renderer_init (MgRendererIface *iface)
{
	iface->render_as_xql = mg_qf_func_render_as_xql;
	iface->render_as_sql = mg_qf_func_render_as_sql;
	iface->render_as_str = mg_qf_func_render_as_str;
}

static void
mg_qf_func_referer_init (MgRefererIface *iface)
{
        iface->activate = mg_qf_func_activate;
        iface->deactivate = mg_qf_func_deactivate;
        iface->is_active = mg_qf_func_is_active;
        iface->get_ref_objects = mg_qf_func_get_ref_objects;
        iface->replace_refs = mg_qf_func_replace_refs;
}

static void
mg_qf_func_class_init (MgQfFuncClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_qf_func_signals[TEMPL_SIGNAL] =
		g_signal_new ("templ_signal",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgQfFuncClass, templ_signal),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	class->templ_signal = NULL;

	object_class->dispose = mg_qf_func_dispose;
	object_class->finalize = mg_qf_func_finalize;

	/* Properties */
	object_class->set_property = mg_qf_func_set_property;
	object_class->get_property = mg_qf_func_get_property;
	g_object_class_install_property (object_class, PROP_QUERY,
					 g_param_spec_pointer ("query", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_qf_func_dump;
#endif
	MG_QFIELD_CLASS (class)->copy = mg_qf_func_copy;
	MG_QFIELD_CLASS (class)->is_equal = mg_qf_func_is_equal;
	MG_QFIELD_CLASS (class)->is_list = NULL;
	MG_QFIELD_CLASS (class)->get_params = NULL;
}

static void
mg_qf_func_init (MgQfFunc *mg_qf_func)
{
	mg_qf_func->priv = g_new0 (MgQfFuncPrivate, 1);
	mg_qf_func->priv->query = NULL;
	mg_qf_func->priv->func_ref = NULL;
	mg_qf_func->priv->args = NULL;
}


/**
 * mg_qf_func_new_with_func
 * @query: a #MgQuery in which the new object will be
 * @func: a #MgServerFunction object
 *
 * Creates a new MgQfFunc object which represents the @func function
 *
 * Returns: the new object
 */
GObject*
mg_qf_func_new_with_func (MgQuery *query, MgServerFunction *func)
{
	GObject   *obj;
	MgQfFunc *mg_qf_func;
	MgConf *conf;
	guint id;

	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (func && IS_MG_SERVER_FUNCTION (func), NULL);

	conf = mg_base_get_conf (MG_BASE (query));

	obj = g_object_new (MG_QF_FUNC_TYPE, "conf", conf, NULL);
	mg_qf_func = MG_QF_FUNC (obj);
	g_object_get (G_OBJECT (query), "field_serial", &id, NULL);
	mg_base_set_id (MG_BASE (mg_qf_func), id);

	mg_qf_func->priv->query = query;
	g_signal_connect (G_OBJECT (query), "nullified",
			  G_CALLBACK (nullified_object_cb), mg_qf_func);

	mg_qf_func->priv->func_ref = MG_REF_BASE (mg_ref_base_new (conf));
	mg_ref_base_set_ref_object (mg_qf_func->priv->func_ref, MG_BASE (func));
	
	return obj;
}

/**
 * mg_qf_func_new_with_xml_id
 * @query: a #MgQuery in which the new object will be
 * @func_xml_id: the XML Id of a #MgServerFunction object
 *
 * Creates a new MgQfFunc object which represents a given function
 *
 * Returns: the new object
 */
GObject*
mg_qf_func_new_with_xml_id (MgQuery *query, const gchar *func_xml_id)
{
	GObject   *obj;
	MgQfFunc *mg_qf_func;
	MgConf *conf;
	guint id;

	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (func_xml_id && *func_xml_id, NULL);

	conf = mg_base_get_conf (MG_BASE (query));
	obj = g_object_new (MG_QF_FUNC_TYPE, "conf", conf, NULL);
	mg_qf_func = MG_QF_FUNC (obj);
	g_object_get (G_OBJECT (query), "field_serial", &id, NULL);
	mg_base_set_id (MG_BASE (mg_qf_func), id);

	mg_qf_func->priv->query = query;
	g_signal_connect (G_OBJECT (query), "nullified",
			  G_CALLBACK (nullified_object_cb), mg_qf_func);

	mg_qf_func->priv->func_ref = MG_REF_BASE (mg_ref_base_new (conf));
	mg_ref_base_set_ref_name (mg_qf_func->priv->func_ref, MG_SERVER_FUNCTION_TYPE, 
				  REFERENCE_BY_XML_ID, func_xml_id);
	
	return obj;
}

static void 
nullified_object_cb (GObject *obj, MgQfFunc *func)
{
	mg_base_nullify (MG_BASE (func));
}

static void
mg_qf_func_dispose (GObject *object)
{
	MgQfFunc *mg_qf_func;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_QF_FUNC (object));

	mg_qf_func = MG_QF_FUNC (object);
	if (mg_qf_func->priv) {
		mg_base_nullify_check (MG_BASE (object));

		if (mg_qf_func->priv->args) {
			GSList *list = mg_qf_func->priv->args;
			while (list) {
				g_object_unref (G_OBJECT (list->data));
				list = g_slist_next (list);
			}
			g_slist_free (mg_qf_func->priv->args);
			mg_qf_func->priv->args = NULL;
		}

		if (mg_qf_func->priv->query) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (mg_qf_func->priv->query),
							      G_CALLBACK (nullified_object_cb), mg_qf_func);
			mg_qf_func->priv->query = NULL;
		}

		if (mg_qf_func->priv->func_ref) {
			g_object_unref (G_OBJECT (mg_qf_func->priv->func_ref));
			mg_qf_func->priv->func_ref = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_qf_func_finalize (GObject   * object)
{
	MgQfFunc *mg_qf_func;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_QF_FUNC (object));

	mg_qf_func = MG_QF_FUNC (object);
	if (mg_qf_func->priv) {
		g_free (mg_qf_func->priv);
		mg_qf_func->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_qf_func_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	MgQfFunc *mg_qf_func;
	gpointer ptr;

	mg_qf_func = MG_QF_FUNC (object);
	if (mg_qf_func->priv) {
		switch (param_id) {
		case PROP_QUERY:
			ptr = g_value_get_pointer (value);
			g_return_if_fail (ptr && IS_MG_QUERY (ptr));

			if (mg_qf_func->priv->query) {
				if (mg_qf_func->priv->query == MG_QUERY (ptr))
					return;

				g_signal_handlers_disconnect_by_func (G_OBJECT (mg_qf_func->priv->query),
								      G_CALLBACK (nullified_object_cb), mg_qf_func);
			}

			mg_qf_func->priv->query = MG_QUERY (ptr);
			g_signal_connect (G_OBJECT (ptr), "nullified",
					  G_CALLBACK (nullified_object_cb), mg_qf_func);
			break;
		}
	}
}

static void
mg_qf_func_get_property (GObject              *object,
			guint                 param_id,
			GValue               *value,
			GParamSpec           *pspec)
{
	MgQfFunc *mg_qf_func;
	mg_qf_func = MG_QF_FUNC (object);
	
	if (mg_qf_func->priv) {
		switch (param_id) {
		case PROP_QUERY:
			g_value_set_pointer (value, mg_qf_func->priv->query);
			break;
		}	
	}
}

static GObject *
mg_qf_func_copy (MgQfield *orig)
{
	MgQfFunc *qf;
	GObject *obj;
	GSList *list;
	MgConf *conf;

	g_assert (IS_MG_QF_FUNC (orig));
	qf = MG_QF_FUNC (orig);

	obj = mg_qf_func_new_with_xml_id (qf->priv->query, 
					  mg_ref_base_get_ref_name (qf->priv->func_ref, NULL, NULL));
	if (mg_base_get_name (MG_BASE (orig)))
		mg_base_set_name (MG_BASE (obj), mg_base_get_name (MG_BASE (orig)));

	if (mg_base_get_description (MG_BASE (orig)))
		mg_base_set_description (MG_BASE (obj), mg_base_get_description (MG_BASE (orig)));


	/* arguments */
	conf = mg_base_get_conf (MG_BASE (orig));
	list = qf->priv->args;
	while (list) {
		MgRefBase *ref;
		const gchar *refname;
		GType type;

		refname = mg_ref_base_get_ref_name (MG_REF_BASE (list->data), &type, NULL);
		ref = MG_REF_BASE (mg_ref_base_new (conf));
		mg_ref_base_set_ref_name (ref, type, REFERENCE_BY_XML_ID, refname);
		MG_QF_FUNC (obj)->priv->args = g_slist_append (MG_QF_FUNC (obj)->priv->args, ref);
		list = g_slist_next (list);
	}

	return obj;
}

static gboolean
mg_qf_func_is_equal (MgQfield *qfield1, MgQfield *qfield2)
{
	const gchar *ref1, *ref2;
	gboolean retval;
	g_assert (IS_MG_QF_FUNC (qfield1));
	g_assert (IS_MG_QF_FUNC (qfield2));
	
	/* it is here assumed that qfield1 and qfield2 are of the same type and refer to the same
	   query */
	ref1 = mg_ref_base_get_ref_name (MG_QF_FUNC (qfield1)->priv->func_ref, NULL, NULL);
	ref2 = mg_ref_base_get_ref_name (MG_QF_FUNC (qfield2)->priv->func_ref, NULL, NULL);

	retval = !strcmp (ref1, ref2) ? TRUE : FALSE;
	if (retval) {
		TO_IMPLEMENT; /* arguments */
	}

	return retval;
}

/**
 * mg_qf_func_get_ref_func
 * @func: a #MgQfFunc object
 *
 * Get the real #MgServerFunction object used by @func
 *
 * Returns: the #MgServerFunction object, or NULL if @func is not active
 */
MgServerFunction *
mg_qf_func_get_ref_func (MgQfFunc *func)
{
	MgBase *base;
	g_return_val_if_fail (func && IS_MG_QF_FUNC (func), NULL);
	g_return_val_if_fail (func->priv, NULL);

	base = mg_ref_base_get_ref_object (func->priv->func_ref);
	if (base)
		return MG_SERVER_FUNCTION (base);
	else
		return NULL;
}

/**
 * mg_qf_func_get_args
 * @func: a #MgQfFunc object
 *
 * Get a list of the other #MgQfield objects which are arguments of @func. If some
 * of them are missing, then a %NULL is inserted where it should have been.
 *
 * Returns: a new list of arguments
 */
GSList *
mg_qf_func_get_args (MgQfFunc *func)
{
	GSList *retval = NULL, *list;

	g_return_val_if_fail (func && IS_MG_QF_FUNC (func), NULL);
	g_return_val_if_fail (func->priv, NULL);
	
	list = func->priv->args;
	while (list) {
		MgBase *base = NULL;

		if (list->data)
			base = mg_ref_base_get_ref_object (MG_REF_BASE (list->data));

		retval = g_slist_append (retval, base);
		list = g_slist_next (list);
	}

	return retval;
}

#ifdef debug
static void
mg_qf_func_dump (MgQfFunc *func, guint offset)
{
	gchar *str;
	gint i;

	g_return_if_fail (func && IS_MG_QF_FUNC (func));
	
        /* string for the offset */
        str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;

        /* dump */
        if (func->priv) {
                g_print ("%s" D_COL_H1 "MgQfFunc" D_COL_NOR " \"%s\" (%p, id=%d) ",
                         str, mg_base_get_name (MG_BASE (func)), func, mg_base_get_id (MG_BASE (func)));
		if (mg_qf_func_is_active (MG_REFERER (func)))
			g_print ("Active, ");
		else
			g_print (D_COL_ERR "Inactive" D_COL_NOR ", ");
		if (mg_qfield_is_visible (MG_QFIELD (func)))
			g_print ("Visible\n");
	}
        else
                g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, func);
}
#endif


/* 
 * MgFunc interface implementation
 */
static MgEntity *
mg_qf_func_get_entity (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_QF_FUNC (iface), NULL);
	g_return_val_if_fail (MG_QF_FUNC (iface)->priv, NULL);

	return MG_ENTITY (MG_QF_FUNC (iface)->priv->query);
}

static MgServerDataType *
mg_qf_func_get_data_type (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_QF_FUNC (iface), NULL);
	g_return_val_if_fail (MG_QF_FUNC (iface)->priv, NULL);
	
	if (mg_qf_func_activate (MG_REFERER (iface))) {
		MgServerFunction *func;
		func = MG_SERVER_FUNCTION (mg_ref_base_get_ref_object (MG_QF_FUNC (iface)->priv->func_ref));
		return mg_server_function_get_ret_type (func);
	}

	return NULL;
}

static const gchar *
mg_qf_func_get_name (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_QF_FUNC (iface), NULL);

	return mg_base_get_name (MG_BASE (iface));
}

static const gchar *
mg_qf_func_get_description (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_QF_FUNC (iface), NULL);
	
	return mg_base_get_description (MG_BASE (iface));	
}


/* 
 * MgXmlStorage interface implementation
 */
static gchar *
mg_qf_func_get_xml_id (MgXmlStorage *iface)
{
	gchar *q_xml_id, *xml_id;

	g_return_val_if_fail (iface && IS_MG_QF_FUNC (iface), NULL);
	g_return_val_if_fail (MG_QF_FUNC (iface)->priv, NULL);

	q_xml_id = mg_xml_storage_get_xml_id (MG_XML_STORAGE (MG_QF_FUNC (iface)->priv->query));
	xml_id = g_strdup_printf ("%s:QF%d", q_xml_id, mg_base_get_id (MG_BASE (iface)));
	g_free (q_xml_id);
	
	return xml_id;
}

static xmlNodePtr
mg_qf_func_save_to_xml (MgXmlStorage *iface, GError **error)
{
	xmlNodePtr node = NULL;
	MgQfFunc *func;
	gchar *str;
	GSList *list;

	g_return_val_if_fail (iface && IS_MG_QF_FUNC (iface), NULL);
	g_return_val_if_fail (MG_QF_FUNC (iface)->priv, NULL);

	func = MG_QF_FUNC (iface);

	node = xmlNewNode (NULL, "MG_QF");
	
	str = mg_qf_func_get_xml_id (iface);
	xmlSetProp (node, "id", str);
	g_free (str);

	xmlSetProp (node, "type", "FUNC");
	xmlSetProp (node, "name", mg_base_get_name (MG_BASE (func)));
	if (mg_base_get_description (MG_BASE (func)) && *mg_base_get_description (MG_BASE (func)))
		xmlSetProp (node, "descr", mg_base_get_description (MG_BASE (func)));
	xmlSetProp (node, "object", mg_ref_base_get_ref_name (func->priv->func_ref, NULL, NULL));
	if (! mg_qfield_is_visible (MG_QFIELD (func)))
		xmlSetProp (node, "is_visible",  "f");
	if (mg_qfield_is_internal (MG_QFIELD (func)))
		xmlSetProp (node, "is_internal", "t");

	/* function's arguments */
	list = func->priv->args;
	while (list) {
		xmlNodePtr argnode;

		argnode = xmlNewChild (node, NULL, "MG_QF_REF", NULL);
		xmlSetProp (argnode, "object", mg_ref_base_get_ref_name (MG_REF_BASE (list->data), NULL, NULL));
		list = g_slist_next (list);
	}

	return node;
}

static gboolean
mg_qf_func_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgQfFunc *func;
	gchar *prop;
	gboolean funcref = FALSE;

	g_return_val_if_fail (iface && IS_MG_QF_FUNC (iface), FALSE);
	g_return_val_if_fail (MG_QF_FUNC (iface)->priv, FALSE);
	g_return_val_if_fail (node, FALSE);

	func = MG_QF_FUNC (iface);
	if (strcmp (node->name, "MG_QF")) {
		g_set_error (error,
			     MG_QF_FUNC_ERROR,
			     MG_QF_FUNC_XML_LOAD_ERROR,
			     _("XML Tag is not <MG_QF>"));
		return FALSE;
	}

	prop = xmlGetProp (node, "type");
	if (prop) {
		if (strcmp (prop, "FUNC")) {
			g_set_error (error,
				     MG_QF_FUNC_ERROR,
				     MG_QF_FUNC_XML_LOAD_ERROR,
				     _("Wrong type of func in <MG_QF>"));
			return FALSE;
		}
		g_free (prop);
	}

	prop = xmlGetProp (node, "id");
	if (prop) {
		gchar *ptr, *tok;
		ptr = strtok_r (prop, ":", &tok);
		ptr = strtok_r (NULL, ":", &tok);
		if (strlen (ptr) < 3) {
			g_set_error (error,
				     MG_QF_FUNC_ERROR,
				     MG_QF_FUNC_XML_LOAD_ERROR,
				     _("Wrong 'id' attribute in <MG_QF>"));
			return FALSE;
		}
		mg_base_set_id (MG_BASE (func), atoi (ptr+2));
		g_free (prop);
	}

	prop = xmlGetProp (node, "name");
	if (prop) {
		mg_base_set_name (MG_BASE (func), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "descr");
	if (prop) {
		mg_base_set_description (MG_BASE (func), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "object");
	if (prop) {
		funcref = TRUE;
		mg_ref_base_set_ref_name (func->priv->func_ref, MG_SERVER_FUNCTION_TYPE, REFERENCE_BY_XML_ID, prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "is_visible");
	if (prop) {
		mg_qfield_set_visible (MG_QFIELD (func), (*prop == 't') ? TRUE : FALSE);
		g_free (prop);
	}

	prop = xmlGetProp (node, "is_internal");
	if (prop) {
		mg_qfield_set_internal (MG_QFIELD (func), (*prop == 't') ? TRUE : FALSE);
		g_free (prop);
	}
	
	/* function's arguments */
	if (node->children) {
		MgConf *conf;
		xmlNodePtr argnode = node->children;
		conf = mg_base_get_conf (MG_BASE (func));
		while (argnode) {
			if (!strcmp (argnode->name, "MG_QF_REF")) {
				prop = xmlGetProp (argnode, "object");
				if (prop) {
					MgRefBase *ref;

					ref = MG_REF_BASE (mg_ref_base_new (conf));
					mg_ref_base_set_ref_name (ref, MG_FIELD_TYPE, REFERENCE_BY_XML_ID, prop);
					g_free (prop);
					func->priv->args = g_slist_append (func->priv->args, ref);
				}
			}
			argnode = argnode->next;
		}
	}
	
	if (funcref) {
		/* test the right number of arguments */
		MgBase *ref;
		
		ref = mg_ref_base_get_ref_object (func->priv->func_ref);
		if (ref && 
		    (g_slist_length (func->priv->args) != 
		     g_slist_length (mg_server_function_get_arg_types (MG_SERVER_FUNCTION (ref))))) {
			g_set_error (error,
				     MG_QF_FUNC_ERROR,
				     MG_QF_FUNC_XML_LOAD_ERROR,
				     _("Wrong number of arguments for function %s"), mg_base_get_name (ref));
			return FALSE;
		}
		return TRUE;
	}
	else {
		g_set_error (error,
			     MG_QF_FUNC_ERROR,
			     MG_QF_FUNC_XML_LOAD_ERROR,
			     _("Missing required attributes for <MG_QF>"));
		return FALSE;
	}
}


/*
 * MgRenderer interface implementation
 */
static GdaXqlItem *
mg_qf_func_render_as_xql (MgRenderer *iface, MgContext *context, GError **error)
{
	GdaXqlItem *node = NULL;

	g_return_val_if_fail (iface && IS_MG_QF_FUNC (iface), NULL);
	g_return_val_if_fail (MG_QF_FUNC (iface)->priv, NULL);
	
	TO_IMPLEMENT;
	return node;
}

static gchar *
mg_qf_func_render_as_sql (MgRenderer *iface, MgContext *context, GError **error)
{
	gchar *str = NULL;
	MgBase *base;
	MgQfFunc *func;
	gboolean err = FALSE;

	g_return_val_if_fail (iface && IS_MG_QF_FUNC (iface), NULL);
	g_return_val_if_fail (MG_QF_FUNC (iface)->priv, NULL);
	func = MG_QF_FUNC (iface);

	base = mg_ref_base_get_ref_object (func->priv->func_ref);

	if (base) {
		GString *string;
		GSList *list;

		string = g_string_new (mg_base_get_name (base));
		g_string_append (string, " (");
		list = func->priv->args;
		while (list && !err) {
			gchar *argstr;
			MgBase *argbase;

			if (list != func->priv->args)
				g_string_append (string, ", ");

			argbase = mg_ref_base_get_ref_object (MG_REF_BASE (list->data));
			if (argbase) {
				argstr = mg_renderer_render_as_sql (MG_RENDERER (argbase), context, error);
				if (argstr) {
					g_string_append (string, argstr);
					g_free (argstr);
				}
				else
					err = TRUE;
			}
			else {
				const gchar *tmpstr;
				tmpstr = mg_ref_base_get_ref_name (MG_REF_BASE (list->data), NULL, NULL);
				g_set_error (error,
					     MG_QF_FUNC_ERROR,
					     MG_QF_FUNC_RENDER_ERROR,
					     _("Can't find referenced field '%s'"), tmpstr);
				err = TRUE;
			}

			list = g_slist_next (list);
		}
		g_string_append (string, ")");
		str = string->str;
		g_string_free (string, FALSE);
	}
	else {
		g_set_error (error,
			     MG_QF_FUNC_ERROR,
			     MG_QF_FUNC_RENDER_ERROR,
			     _("Can't find function '%s'"), mg_ref_base_get_ref_name (func->priv->func_ref,
										      NULL, NULL));
		err = TRUE;
	}

	if (err) {
		if (str)
			g_free (str);
		return NULL;
	}

	return str;
}

static gchar *
mg_qf_func_render_as_str (MgRenderer *iface, MgContext *context)
{
	gchar *str = NULL;
	MgBase *base;
	MgQfFunc *func;

	g_return_val_if_fail (iface && IS_MG_QF_FUNC (iface), NULL);
	g_return_val_if_fail (MG_QF_FUNC (iface)->priv, NULL);
	func = MG_QF_FUNC (iface);

	base = mg_ref_base_get_ref_object (func->priv->func_ref);

	if (base) {
		GString *string;
		GSList *list;

		string = g_string_new (mg_base_get_name (base));
		g_string_append (string, " (");
		list = func->priv->args;
		while (list) {
			gchar *argstr;
			MgBase *argbase;

			if (list != func->priv->args)
				g_string_append (string, ", ");

			argbase = mg_ref_base_get_ref_object (MG_REF_BASE (list->data));
			if (argbase) {
				argstr = mg_renderer_render_as_str (MG_RENDERER (argbase), context);
				g_assert (argstr);
				g_string_append (string, argstr);
				g_free (argstr);
			}
			else {
				const gchar *tmpstr;
				tmpstr = mg_ref_base_get_ref_name (MG_REF_BASE (list->data), NULL, NULL);
				g_string_append (string, tmpstr);
			}

			list = g_slist_next (list);
		}
		str = string->str;
		g_string_free (string, FALSE);

	}
	else 
		str = g_strdup (_("Non activated function"));
	
	return str;
}


/*
 * MgReferer interface implementation
 */
static gboolean
mg_qf_func_activate (MgReferer *iface)
{
	gboolean active = FALSE;
	MgQfFunc *func;
	GSList *list;

	g_return_val_if_fail (iface && IS_MG_QF_FUNC (iface), FALSE);
	g_return_val_if_fail (MG_QF_FUNC (iface)->priv, FALSE);
	func = MG_QF_FUNC (iface);

	active = mg_ref_base_activate (func->priv->func_ref);
	list = func->priv->args;
	while (list) {
		active = mg_ref_base_activate (MG_REF_BASE (list->data)) && active;
		list = g_slist_next (list);
	}

	return active;
}

static void
mg_qf_func_deactivate (MgReferer *iface)
{
	MgQfFunc *func;
	GSList *list;

	g_return_if_fail (iface && IS_MG_QF_FUNC (iface));
	g_return_if_fail (MG_QF_FUNC (iface)->priv);
	func = MG_QF_FUNC (iface);

	mg_ref_base_deactivate (func->priv->func_ref);
	list = func->priv->args;
	while (list) {
		mg_ref_base_deactivate (MG_REF_BASE (list->data));
		list = g_slist_next (list);
	}
}

static gboolean
mg_qf_func_is_active (MgReferer *iface)
{
	gboolean active;
	MgQfFunc *func;
	GSList *list;

	g_return_val_if_fail (iface && IS_MG_QF_FUNC (iface), FALSE);
	g_return_val_if_fail (MG_QF_FUNC (iface)->priv, FALSE);
	func = MG_QF_FUNC (iface);

	active = mg_ref_base_is_active (func->priv->func_ref);
	list = func->priv->args;
	while (list && active) {
		active = mg_ref_base_is_active (MG_REF_BASE (list->data)) && active;
		list = g_slist_next (list);
	}

	return active;
}

static GSList *
mg_qf_func_get_ref_objects (MgReferer *iface)
{
	GSList *list = NULL;
        MgBase *base;
	MgQfFunc *func;
	GSList *args;

	g_return_val_if_fail (iface && IS_MG_QF_FUNC (iface), NULL);
	g_return_val_if_fail (MG_QF_FUNC (iface)->priv, NULL);
	func = MG_QF_FUNC (iface);

        base = mg_ref_base_get_ref_object (func->priv->func_ref);
        if (base)
                list = g_slist_append (list, base);

	args = func->priv->args;
	while (args) {
		base = mg_ref_base_get_ref_object (MG_REF_BASE (list->data));
		if (base)
			list = g_slist_append (list, base);
		list = g_slist_next (list);
	}

        return list;
}

static void
mg_qf_func_replace_refs (MgReferer *iface, GHashTable *replacements)
{
	MgQfFunc *func;
	GSList *list;

        g_return_if_fail (iface && IS_MG_QF_FUNC (iface));
        g_return_if_fail (MG_QF_FUNC (iface)->priv);

        func = MG_QF_FUNC (iface);
        if (func->priv->query) {
                MgQuery *query = g_hash_table_lookup (replacements, func->priv->query);
                if (query) {
                        g_signal_handlers_disconnect_by_func (G_OBJECT (func->priv->query),
                                                              G_CALLBACK (nullified_object_cb), func);
                        func->priv->query = query;
                        g_signal_connect (G_OBJECT (query), "nullified",
                                          G_CALLBACK (nullified_object_cb), func);
                }
        }

        mg_ref_base_replace_ref_object (func->priv->func_ref, replacements);
	list = func->priv->args;
	while (list) {
		mg_ref_base_replace_ref_object (MG_REF_BASE (list->data), replacements);
		list = g_slist_next (list);
	}
}
