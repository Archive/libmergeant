/* libmergeant.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */



#ifndef __LIBMERGEANT_H_
#define __LIBMERGEANT_H_

/* objects */
#include <libmergeant/mg-conf.h>
#include <libmergeant/mg-base.h>
#include <libmergeant/mg-ref-base.h>

#include <libmergeant/mg-server.h>
#include <libmergeant/mg-server-data-type.h>
#include <libmergeant/mg-server-function.h>
#include <libmergeant/mg-server-aggregate.h>
#include <libmergeant/mg-result-set.h>

#include <libmergeant/mg-database.h>
#include <libmergeant/mg-db-table.h>
#include <libmergeant/mg-db-field.h>
#include <libmergeant/mg-db-constraint.h>

/* queries */
#include <libmergeant/mg-context.h>
#include <libmergeant/mg-parameter.h>
#include <libmergeant/mg-query.h>
#include <libmergeant/mg-target.h>
#include <libmergeant/mg-qfield.h>
#include <libmergeant/mg-join.h>
#include <libmergeant/mg-condition.h>
#include <libmergeant/mg-qf-field.h>
#include <libmergeant/mg-qf-all.h>
#include <libmergeant/mg-qf-value.h>
#include <libmergeant/mg-qf-func.h>

/* interfaces */
#include <libmergeant/mg-xml-storage.h>
#include <libmergeant/mg-data-handler.h>
#include <libmergeant/mg-data-entry.h>
#include <libmergeant/mg-renderer.h>
#include <libmergeant/mg-entity.h>
#include <libmergeant/mg-field.h>

/* data handlers widgets */
#include <libmergeant/handlers/mg-entry-shell.h>
#include <libmergeant/handlers/mg-entry-wrapper.h>
#include <libmergeant/handlers/mg-entry-string.h>
#include <libmergeant/handlers/mg-entry-boolean.h>
#include <libmergeant/handlers/mg-entry-none.h>
#include <libmergeant/handlers/mg-entry-time.h>
#include <libmergeant/handlers/mg-entry-combo.h>

/* widgets */
#include <libmergeant/mg-selector.h>
#include <libmergeant/mg-dbms-update-viewer.h>
#include <libmergeant/mg-form.h>
#include <libmergeant/mg-work-form.h>

/* misc */
#include <libmergeant/mg-graphviz.h>

#endif
