/* mg-parameter.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <string.h>
#include "mg-parameter.h"
#include "marshal.h"
#include "mg-conf.h"
#include "mg-query.h"
#include "mg-entity.h"
#include "mg-field.h"
#include "mg-qfield.h"
#include "mg-server-data-type.h"

/* 
 * Main static functions 
 */
static void mg_parameter_class_init (MgParameterClass * class);
static void mg_parameter_init (MgParameter * srv);
static void mg_parameter_dispose (GObject   * object);
static void mg_parameter_finalize (GObject   * object);

static void mg_parameter_set_property    (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec);
static void mg_parameter_get_property    (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec);

static void mg_parameter_add_for_field (MgParameter *param, MgQfield *for_field);
static void mg_parameter_del_for_field (MgParameter *param, MgQfield *for_field);
static void mg_parameter_set_data_type (MgParameter *param, MgServerDataType *type);

static void nullified_object_cb (MgBase *obj, MgParameter *param);
static void nullified_for_field_cb (MgBase *obj, MgParameter *param);
static void nullified_depend_on_cb (MgBase *obj, MgParameter *param);
static void nullified_in_field_cb (MgBase *obj, MgParameter *param);
static void nullified_alias_of_cb (MgBase *obj, MgParameter *param);

static void alias_of_changed_cb (MgParameter *alias_of, MgParameter *param);

#ifdef debug
static void mg_parameter_dump (MgParameter *parameter, guint offset);
#endif

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	LAST_SIGNAL
};

static gint mg_parameter_signals[LAST_SIGNAL] = {  };

/* properties */
enum
{
	PROP_0,
	PROP_PLUGIN,
	PROP_QF_INTERNAL,
	PROP_USE_DEFAULT_VALUE
};


struct _MgParameterPrivate
{
	GSList           *for_fields;
	MgServerDataType *type;
	MgParameter      *alias_of;
	
	gboolean          valid;
	gboolean          default_forced;
	GdaValue         *value;
	GdaValue         *default_value; /* CAN be either NULL or of any type */
	gboolean          not_null;      /* TRUE if 'value' must not be NULL when passed to destination fields */

	MgQfield         *in_field;      /* if not NULL, limits the possible values for the parameter */
	GSList           *dependencies;  /* other MgParameter objects on which the parameter depends on */

	gchar            *plugin;        /* plugin to be used for user interaction */

	gboolean          dest_qf_internal;
	gboolean          user_input_required;
};

/* module error */
GQuark mg_parameter_error_quark (void)
{
        static GQuark quark;
        if (!quark)
                quark = g_quark_from_static_string ("mg_parameter_error");
        return quark;
}

guint
mg_parameter_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgParameterClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_parameter_class_init,
			NULL,
			NULL,
			sizeof (MgParameter),
			0,
			(GInstanceInitFunc) mg_parameter_init
		};
		
		type = g_type_register_static (MG_BASE_TYPE, "MgParameter", &info, 0);
	}
	return type;
}

static void
mg_parameter_class_init (MgParameterClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);


	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_parameter_dump;
#endif

	object_class->dispose = mg_parameter_dispose;
	object_class->finalize = mg_parameter_finalize;

	/* Properties */
	object_class->set_property = mg_parameter_set_property;
	object_class->get_property = mg_parameter_get_property;
	g_object_class_install_property (object_class, PROP_PLUGIN,
					 g_param_spec_string ("handler_plugin", NULL, NULL, NULL, (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property (object_class, PROP_QF_INTERNAL,
					 g_param_spec_boolean ("dest_qf_internal", NULL, NULL, FALSE,
                                                               (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property (object_class, PROP_USE_DEFAULT_VALUE,
					 g_param_spec_boolean ("use_default_value", NULL, NULL, FALSE,
                                                               (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static void
mg_parameter_init (MgParameter *parameter)
{
	parameter->priv = g_new0 (MgParameterPrivate, 1);
	parameter->priv->for_fields = NULL;
	parameter->priv->type = NULL;
	parameter->priv->alias_of = NULL;

	parameter->priv->valid = FALSE;
	parameter->priv->default_forced = FALSE;
	parameter->priv->value = NULL;
	parameter->priv->default_value = NULL;

	parameter->priv->not_null = FALSE;
	parameter->priv->in_field = NULL;
	parameter->priv->dependencies = NULL;
	parameter->priv->plugin = NULL;
	parameter->priv->dest_qf_internal = FALSE;
	parameter->priv->user_input_required = FALSE;
}

/**
 * mg_parameter_new
 * @conf: a #MgConf object
 * @type: the #MgServerDataType requested
 *
 * Creates a new parameter of type @type
 *
 * Returns: a new #MgParameter object
 */
GObject *
mg_parameter_new (MgConf *conf, MgServerDataType *type)
{
	GObject   *obj;
	MgParameter *param;

        g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);
	g_return_val_if_fail (type && IS_MG_SERVER_DATA_TYPE (type), NULL);

        obj = g_object_new (MG_PARAMETER_TYPE, "conf", conf, NULL);
	param = MG_PARAMETER (obj);

	mg_parameter_set_data_type (param, type);

        return obj;
}

/**
 * mg_parameter_new_with_field
 * @field: the #MgQfield object the parameter is for
 * @type: the #MgServerDataType requested
 *
 * Creates a new parameter to be passed to @field for the operations where a parameter is required;
 * other #MgQfield can also be added by using the mg_parameter_add_dest_field() method.
 *
 * Returns: a new #MgParameter object
 */
GObject *
mg_parameter_new_with_field (MgQfield *field, MgServerDataType *type)
{
	GObject   *obj;
	MgParameter *param;
	MgConf *conf;

        g_return_val_if_fail (field && IS_MG_QFIELD (field), NULL);
	g_return_val_if_fail (type && IS_MG_SERVER_DATA_TYPE (type), NULL);

	conf = mg_base_get_conf (MG_BASE (field));
	g_return_val_if_fail (conf, NULL);

        obj = g_object_new (MG_PARAMETER_TYPE, "conf", conf, NULL);
	param = MG_PARAMETER (obj);

	mg_parameter_add_for_field (param, field);
	mg_parameter_set_data_type (param, type);

        return obj;
}

static void
mg_parameter_add_for_field (MgParameter *param, MgQfield *for_field)
{
	if (!g_slist_find (param->priv->for_fields, for_field)) {
		param->priv->for_fields = g_slist_append (param->priv->for_fields, for_field);
		g_signal_connect (G_OBJECT (for_field), "nullified",
				  G_CALLBACK (nullified_for_field_cb), param);
		g_object_ref (G_OBJECT (for_field));
	}
}

static void
mg_parameter_del_for_field (MgParameter *param, MgQfield *for_field)
{
	if (g_slist_find (param->priv->for_fields, for_field)) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (for_field),
						      G_CALLBACK (nullified_for_field_cb), param);
		g_object_unref (G_OBJECT (for_field));
		param->priv->for_fields = g_slist_remove (param->priv->for_fields, for_field);
	}
}

static void
mg_parameter_set_data_type (MgParameter *param, MgServerDataType *type)
{
	if (param->priv->type) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (param->priv->type),
						      G_CALLBACK (nullified_object_cb), param);
		g_object_unref (G_OBJECT (param->priv->type));
		param->priv->type = NULL;
	}
	
	if (type) {
		param->priv->type = type;
		g_signal_connect (G_OBJECT (type), "nullified",
				  G_CALLBACK (nullified_object_cb), param);
		g_object_ref (G_OBJECT (param->priv->type));
	}
}

static void
nullified_for_field_cb (MgBase *obj, MgParameter *param)
{
	mg_parameter_del_for_field (param, MG_QFIELD (obj));
}

static void
nullified_object_cb (MgBase *obj, MgParameter *param)
{
	mg_base_nullify (MG_BASE (param));
}

static void
mg_parameter_dispose (GObject *object)
{
	MgParameter *parameter;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_PARAMETER (object));

	parameter = MG_PARAMETER (object);
	if (parameter->priv) {
		mg_base_nullify_check (MG_BASE (object));

		mg_parameter_set_alias_of (parameter, NULL);

		if (parameter->priv->in_field)
			mg_parameter_set_in_field (parameter, NULL, NULL);

		while (parameter->priv->for_fields)
			mg_parameter_del_for_field (parameter, MG_QFIELD (parameter->priv->for_fields->data));

		while (parameter->priv->dependencies)
			mg_parameter_del_dependency (parameter, MG_PARAMETER (parameter->priv->dependencies->data));

		mg_parameter_set_data_type (parameter, NULL);

		if (parameter->priv->value) {
			gda_value_free (parameter->priv->value);
			parameter->priv->value = NULL;
		}

		if (parameter->priv->default_value) {
			gda_value_free (parameter->priv->default_value);
			parameter->priv->default_value = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_parameter_finalize (GObject   * object)
{
	MgParameter *parameter;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_PARAMETER (object));

	parameter = MG_PARAMETER (object);
	if (parameter->priv) {
		if (parameter->priv->plugin)
			g_free (parameter->priv->plugin);

		g_free (parameter->priv);
		parameter->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
	/*g_print ("P fin %p\n", object);*/
}


static void 
mg_parameter_set_property    (GObject              *object,
			      guint                 param_id,
			      const GValue         *value,
			      GParamSpec           *pspec)
{
	const gchar *ptr;
	MgParameter *parameter;

	parameter = MG_PARAMETER (object);
	if (parameter->priv) {
		switch (param_id) {
		case PROP_PLUGIN:
			ptr = g_value_get_string (value);
			if (parameter->priv->plugin) {
				g_free (parameter->priv->plugin);
				parameter->priv->plugin = NULL;
			}
			if (ptr)
				parameter->priv->plugin = g_strdup (ptr);
			break;
		case PROP_QF_INTERNAL:
			parameter->priv->dest_qf_internal = g_value_get_boolean (value);
			break;
		case PROP_USE_DEFAULT_VALUE:
			parameter->priv->default_forced = g_value_get_boolean (value);
			break;
		}
	}
}

static void
mg_parameter_get_property    (GObject              *object,
			      guint                 param_id,
			      GValue               *value,
			      GParamSpec           *pspec)
{
	MgParameter *parameter;

	parameter = MG_PARAMETER (object);
	if (parameter->priv) {
		switch (param_id) {
		case PROP_PLUGIN:
			g_value_set_string (value, parameter->priv->plugin);
			break;
		case PROP_QF_INTERNAL:
			g_value_set_boolean (value, parameter->priv->dest_qf_internal);
			break;
		case PROP_USE_DEFAULT_VALUE:
			g_value_set_boolean (value, parameter->priv->default_forced);
			break;
		}
	}
}


/**
 * mg_parameter_add_dest_field
 * @param: a #MgParameter object
 * @field: the #MgQfield object the parameter is for
 *
 * Adds a #MgQfield object for which the parameter is for
 */
void
mg_parameter_add_dest_field (MgParameter *param, MgQfield *field)
{
	g_return_if_fail (param && IS_MG_PARAMETER (param));
	g_return_if_fail (param->priv);
	g_return_if_fail (field && IS_MG_QFIELD (field));

	mg_parameter_add_for_field (param, field);	
}

/**
 * mg_parameter_get_dest_fields
 * @param: a #MgParameter object
 * 
 * Get the #MgQfield objects which created @param (and which will use its value)
 *
 * Returns: the list of #MgQfield object
 */
GSList *
mg_parameter_get_dest_fields (MgParameter *param)
{
	g_return_val_if_fail (param && IS_MG_PARAMETER (param), NULL);
	g_return_val_if_fail (param->priv, NULL);

	return param->priv->for_fields;
}

/**
 * mg_parameter_get_data_type
 * @param: a #MgParameter object
 * 
 * Get the requested data type for @param.
 *
 * Returns: the data type
 */
MgServerDataType *
mg_parameter_get_data_type (MgParameter *param)
{
	g_return_val_if_fail (param && IS_MG_PARAMETER (param), NULL);
	g_return_val_if_fail (param->priv, NULL);

	return param->priv->type;
}


/**
 * mg_parameter_get_value
 * @param: a #MgParameter object
 *
 * Get the value held into the parameter
 *
 * Returns: the value
 */
const GdaValue *
mg_parameter_get_value (MgParameter *param)
{
	g_return_val_if_fail (param && IS_MG_PARAMETER (param), NULL);
	g_return_val_if_fail (param->priv, NULL);

	if (!param->priv->alias_of)
		return param->priv->value;
	else
		return mg_parameter_get_value (param->priv->alias_of);
}


/*
 * mg_parameter_set_value
 * @param: a #MgParameter object
 * @value: a value to set the parameter to
 *
 * Sets the value within the parameter. If @param is an alias for another
 * parameter, then the value is also set for that other parameter.
 */
void
mg_parameter_set_value (MgParameter *param, const GdaValue *value)
{
	gboolean changed = TRUE;
	g_return_if_fail (param && IS_MG_PARAMETER (param));
	g_return_if_fail (param->priv);

	if (param->priv->value) {
		if (value && (gda_value_get_type (value) == gda_value_get_type (param->priv->value)))
			changed = gda_value_compare (value, param->priv->value);

		if (changed) {
			gda_value_free (param->priv->value);
			param->priv->value = NULL;
		}
	}
	
	if (value && changed)
		param->priv->value = gda_value_copy (value);
	
	param->priv->valid = TRUE;
	if (!value || gda_value_is_null (value))
		if (param->priv->not_null)
			param->priv->valid = FALSE;

	if (value &&
	    (gda_value_get_type (value) != GDA_VALUE_TYPE_NULL) &&
	    (gda_value_get_type (value) != mg_server_data_type_get_gda_type (param->priv->type)))
		param->priv->valid = FALSE;
	
	/* g_print ("Param %p changed to %s (valid=%d)\n", param,  */
/* 		 param->priv->value ? gda_value_stringify (param->priv->value) : "Null", param->priv->valid); */

	/* if we are an alias, then we forward the value setting to the master */
	if (param->priv->alias_of) 
		mg_parameter_set_value (param->priv->alias_of, value);
	else {
		if (changed)
			mg_base_changed (MG_BASE (param));
	}
}

/**
 * mg_parameter_is_valid
 * @param: a #MgParameter object
 *
 * Get the validity of @param (that is, of the value held by @param)
 *
 * Returns: TRUE if @param's value can safely be used
 */
gboolean
mg_parameter_is_valid (MgParameter *param)
{
	g_return_val_if_fail (param && IS_MG_PARAMETER (param), FALSE);
	g_return_val_if_fail (param->priv, FALSE);

	if (param->priv->alias_of)
		return mg_parameter_is_valid (param->priv->alias_of);
	else {
		if (param->priv->default_forced) 
			return param->priv->default_value ? TRUE : FALSE;
		else
			return param->priv->valid;
	}
}


/**
 * mg_parameter_get_default_value
 * @param: a #MgParameter object
 *
 * Get the default value held into the parameter. WARNING: the default value does not need to be of 
 * the same type as the one required by @param.
 *
 * Returns: the default value
 */
const GdaValue *
mg_parameter_get_default_value (MgParameter *param)
{
	g_return_val_if_fail (param && IS_MG_PARAMETER (param), NULL);
	g_return_val_if_fail (param->priv, NULL);

	return param->priv->default_value;	
}


/*
 * mg_parameter_set_default_value
 * @param: a #MgParameter object
 * @value: a value to set the parameter's default value to
 *
 * Sets the default value within the parameter. WARNING: the default value does not need to be of 
 * the same type as the one required by @param.
 */
void
mg_parameter_set_default_value (MgParameter *param, const GdaValue *value)
{
	g_return_if_fail (param && IS_MG_PARAMETER (param));
	g_return_if_fail (param->priv);

	if (param->priv->default_value) {
		gda_value_free (param->priv->default_value);
		param->priv->default_value = NULL;
	}

	if (value) 
		param->priv->default_value = gda_value_copy (value);

	mg_base_changed (MG_BASE (param));
}

/**
 * mg_parameter_set_not_null
 * @param: a #MgParameter object
 * @not_null:
 *
 * Sets if the parameter can have a NULL value. If @not_null is TRUE, then that won't be allowed
 */
void
mg_parameter_set_not_null (MgParameter *param, gboolean not_null)
{
	g_return_if_fail (param && IS_MG_PARAMETER (param));
	g_return_if_fail (param->priv);

	if (not_null != param->priv->not_null) {
		param->priv->not_null = not_null;

		/* updating the parameter's validity regarding the NULL value */
		if (!not_null && 
		    (!param->priv->value || gda_value_is_null (param->priv->value)))
			param->priv->valid = TRUE;

		if (not_null && 
		    (!param->priv->value || gda_value_is_null (param->priv->value)))
			param->priv->valid = FALSE;

		mg_base_changed (MG_BASE (param));
	}
}

/**
 * mg_parameter_get_not_null
 * @param: a #MgParameter object
 *
 * Get wether the parameter can be NULL or not
 *
 * Returns: TRUE if the parameter cannot be NULL
 */
gboolean
mg_parameter_get_not_null (MgParameter *param)
{
	g_return_val_if_fail (param && IS_MG_PARAMETER (param), FALSE);
	g_return_val_if_fail (param->priv, FALSE);

	return param->priv->not_null;
}

/**
 * mg_parameter_set_in_field
 * @param: a #MgParameter object
 * @field: a #MgQfield object or NULL
 * @error: location to store error, or %NULL
 *
 * Sets a limit on the possible values for the @param parameter. @field must be of the same type
 * as the requested type for @param.
 *
 * @field is not copied, just referenced (maybe it'd better to make a copy of it...)
 *
 * Returns: TRUE if no error occured
 */
gboolean
mg_parameter_set_in_field (MgParameter *param, MgQfield *field, GError **error)
{
	g_return_val_if_fail (param && IS_MG_PARAMETER (param), FALSE);
	g_return_val_if_fail (param->priv, FALSE);
	
	if (param->priv->in_field) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (param->priv->in_field),
						      G_CALLBACK (nullified_in_field_cb), param);
		g_object_unref (G_OBJECT (param->priv->in_field));
		param->priv->in_field = NULL;
	}
	
	if (field) {
		MgQuery *query;
		MgEntity *ent;

		g_return_val_if_fail (IS_MG_QFIELD (field), FALSE);
		ent = mg_field_get_entity (MG_FIELD (field));
		if (! IS_MG_QUERY (ent)) {
			g_set_error (error,
				     MG_PARAMETER_ERROR,
				     MG_PARAMETER_QUERY_LIMIT_ERROR,
				     _("A parameter can only get its value within a query"));
			return FALSE;
		}
		query = MG_QUERY (ent);

		/* test to see if @query is well formed for the job */
		if (!mg_query_is_select_query (query)) {
			g_set_error (error,
				     MG_PARAMETER_ERROR,
				     MG_PARAMETER_QUERY_LIMIT_ERROR,
				     _("Parameter: query to limit range is not a selection query"));
			return FALSE;
		}
		
		param->priv->in_field = field;
		g_signal_connect (G_OBJECT (field), "nullified",
				  G_CALLBACK (nullified_in_field_cb), param);
		g_object_ref (G_OBJECT (param->priv->in_field));
	}
	return TRUE;
}

static void
nullified_in_field_cb (MgBase *obj, MgParameter *param)
{
	mg_parameter_set_in_field (param, NULL, NULL);
}

/**
 * mg_parameter_get_limit_query
 * @param: a #MgParameter object
 *
 * Get the query field which limits the possible value for the parameter.
 *
 * Returns: the #MgQfield, or %NULL
 */
MgQfield *
mg_parameter_get_in_field (MgParameter *param)
{
 	g_return_val_if_fail (param && IS_MG_PARAMETER (param), NULL);
	g_return_val_if_fail (param->priv, NULL);
	
	return param->priv->in_field;
}

/**
 * mg_parameter_add_dependency
 * @param: a #MgParameter object
 * @depend_on: a #MgParameter object
 *
 * Tell @param that its value will depend on the value of @depend_on
 */
void
mg_parameter_add_dependency (MgParameter *param, MgParameter *depend_on)
{
 	g_return_if_fail (param && IS_MG_PARAMETER (param));
	g_return_if_fail (param->priv);
 	g_return_if_fail (depend_on && IS_MG_PARAMETER (depend_on));
	g_return_if_fail (depend_on->priv);
	g_return_if_fail (!g_slist_find (depend_on->priv->dependencies, param));

	if (!g_slist_find (param->priv->dependencies, depend_on)) {
		param->priv->dependencies = g_slist_append (param->priv->dependencies, depend_on);
		g_signal_connect (G_OBJECT (depend_on), "nullified",
				  G_CALLBACK (nullified_depend_on_cb), param);
		g_object_ref (G_OBJECT (depend_on));
	}	
}

/**
 * mg_parameter_del_dependency
 * @param: a #MgParameter object
 * @depend_on: a #MgParameter object
 *
 * Remove the @depend_on dependency from @param
 */
void
mg_parameter_del_dependency (MgParameter *param, MgParameter *depend_on)
{
 	g_return_if_fail (param && IS_MG_PARAMETER (param));
	g_return_if_fail (param->priv);
 	g_return_if_fail (depend_on && IS_MG_PARAMETER (depend_on));
	g_return_if_fail (depend_on->priv);

	if (g_slist_find (param->priv->dependencies, depend_on)) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (depend_on),
						      G_CALLBACK (nullified_depend_on_cb), param);
		g_object_unref (G_OBJECT (depend_on));
		param->priv->dependencies = g_slist_remove (param->priv->dependencies, depend_on);
	}
}

static void
nullified_depend_on_cb (MgBase *obj, MgParameter *param)
{
	mg_parameter_del_dependency (param, MG_PARAMETER (obj));
}

/**
 * mg_parameter_get_dependencies
 * @param: a #MgParameter object
 *
 * Get the list of #MgParameter @param depends on
 *
 * Returns: the list of parameters
 */
GSList *
mg_parameter_get_dependencies (MgParameter *param)
{
 	g_return_val_if_fail (param && IS_MG_PARAMETER (param), NULL);
	g_return_val_if_fail (param->priv, NULL);
	
	return param->priv->dependencies;
}

/**
 * mg_parameter_replace_ref
 * @param: a #MgParameter object
 * @replacements: the (objects to be replaced, replacing object) pairs
 *
 * Replace references to some objects by references to some other objects,
 * as listed in @replacements.
 */
void
mg_parameter_replace_ref (MgParameter *param, GHashTable *replacements)
{
	GSList *list;
	gpointer ref;

	g_return_if_fail (param && IS_MG_PARAMETER (param));
	g_return_if_fail (param->priv);

	/* Destination fields */
	list = param->priv->for_fields;
	while (list) {
		ref = g_hash_table_lookup (replacements, list->data);
		if (ref) /* we don't actually replace the ref, but simply add one destination field */
			mg_parameter_add_dest_field (param, ref);

		list = g_slist_next (list);
	}

	/* dependencies */ 
	list = param->priv->dependencies;
	while (list) {
		ref = g_hash_table_lookup (replacements, list->data);
		if (ref) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (list->data),
							      G_CALLBACK (nullified_depend_on_cb), param);
			g_object_unref (G_OBJECT (list->data));
			list->data = ref;
			g_signal_connect (G_OBJECT (ref), "nullified",
					  G_CALLBACK (nullified_depend_on_cb), param);
			g_object_ref (G_OBJECT (ref));
		}
		list = g_slist_next (list);
	}
	
	/* in_field */
	if (param->priv->in_field) {
		ref = g_hash_table_lookup (replacements, param->priv->in_field);
		if (ref) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (param->priv->in_field),
							      G_CALLBACK (nullified_in_field_cb), param);
			g_object_unref (G_OBJECT (param->priv->in_field));
			param->priv->in_field = MG_QFIELD (ref);
			g_signal_connect (G_OBJECT (param->priv->in_field), "nullified",
					  G_CALLBACK (nullified_in_field_cb), param);
			g_object_ref (G_OBJECT (param->priv->in_field));
		}
	}
}

/**
 * mg_parameter_set_alias_of
 * @param: a #MgParameter
 * @alias_of: a #MgParameter or %NULL
 *
 * Sets @param to ba alias of @alias_of: its attributes and value will be the same as 
 * the ones of @alias_of, and it will emit a "changed" signal when @alias_of does so.
 *
 * Except for the validity, the other attributes of @param will still be handled in a 
 * separate way.
 */
void
mg_parameter_set_alias_of (MgParameter *param, MgParameter *alias_of)
{
	g_return_if_fail (param && IS_MG_PARAMETER (param));
	g_return_if_fail (param->priv);

	if (alias_of) {
		g_return_if_fail (alias_of && IS_MG_PARAMETER (alias_of));
		g_return_if_fail (alias_of->priv);
		g_return_if_fail (param->priv->type == alias_of->priv->type);
	}
	
	if (param->priv->alias_of) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (param->priv->alias_of),
						      G_CALLBACK (nullified_alias_of_cb), param);
		g_signal_handlers_disconnect_by_func (G_OBJECT (param->priv->alias_of),
						      G_CALLBACK (alias_of_changed_cb), param);
		param->priv->alias_of = NULL;
	}

	if (alias_of) {
		param->priv->alias_of = alias_of;
		g_signal_connect (G_OBJECT (param->priv->alias_of), "nullified",
				  G_CALLBACK (nullified_alias_of_cb), param);
		g_signal_connect (G_OBJECT (param->priv->alias_of), "changed",
				  G_CALLBACK (alias_of_changed_cb), param);
	}
}

static void
nullified_alias_of_cb (MgBase *obj, MgParameter *param)
{
	mg_parameter_set_alias_of (param, NULL);
}

static void
alias_of_changed_cb (MgParameter *alias_of, MgParameter *param)
{
	mg_base_changed (MG_BASE (param));
}

/**
 * mg_parameter_get_alias_of
 * @param: a #MgParameter
 *
 * Get the parameter for which @param is an alias.
 *
 * Returns: the #MgParameter for which @param is an alias
 */
MgParameter *
mg_parameter_get_alias_of (MgParameter *param)
{
	g_return_val_if_fail (param && IS_MG_PARAMETER (param), NULL);
	g_return_val_if_fail (param->priv, NULL);

	return param->priv->alias_of;
}


/**
 * mg_parameter_requires_user_input
 * @param: a #MgParameter
 *
 * Tells if the parameter is configured in a way that even if there is a value, it requires
 * that the user at least validates that value, or change it.
 *
 * Returns: TRUE if user input is required
 */
gboolean
mg_parameter_requires_user_input (MgParameter *param)
{
	g_return_val_if_fail (param && IS_MG_PARAMETER (param), FALSE);
	g_return_val_if_fail (param->priv, FALSE);

	return param->priv->user_input_required;
}

/**
 * mg_parameter_set_user_input_required
 * @param: a #MgParameter
 * @input_required:
 * 
 * Sets if the user input is required for @param (even though it may already have
 * a value) and be valid.
 */
void
mg_parameter_set_user_input_required (MgParameter *param, gboolean input_required)
{
	g_return_if_fail (param && IS_MG_PARAMETER (param));
	g_return_if_fail (param->priv);

	param->priv->user_input_required = input_required;
}


#ifdef debug
static void
mg_parameter_dump (MgParameter *parameter, guint offset)
{
	gchar *str;
	guint i;

	g_return_if_fail (parameter);
	g_return_if_fail (IS_MG_PARAMETER (parameter));

	/* string for the offset */
	str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;
	
	/* dump */
	if (parameter->priv) {
		GSList *list;
		g_print ("%s" D_COL_H1 "MgParameter %p (%s)\n" D_COL_NOR, str, parameter, mg_base_get_name (MG_BASE (parameter)));
		
		list = parameter->priv->for_fields;
		while (list) {
			if (list == parameter->priv->for_fields)
				g_print ("%sFor fields: ", str);
			else
				g_print (", ");
				g_print ("%p", list->data);
			list = g_slist_next (list);
		}
		if (parameter->priv->for_fields)
			g_print ("\n");

		list = parameter->priv->dependencies;
		while (list) {
			if (list == parameter->priv->dependencies)
				g_print ("%sParam dependencies:", str);
			else
				g_print (", ");
			g_print ("%p", list->data);
			list = g_slist_next (list);
		}
		if (parameter->priv->dependencies)
			g_print ("\n");

		if (parameter->priv->in_field)
			g_print ("%sDepends on query field %p\n", str, parameter->priv->in_field);
	}
	else
		g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, parameter);

	g_free (str);
}
#endif
