/* mg-parameter.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_PARAMETER_H_
#define __MG_PARAMETER_H_

#include "mg-defs.h"
#include "mg-base.h"
#include <libgda/libgda.h>


G_BEGIN_DECLS

#define MG_PARAMETER_TYPE          (mg_parameter_get_type())
#define MG_PARAMETER(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_parameter_get_type(), MgParameter)
#define MG_PARAMETER_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_parameter_get_type (), MgParameterClass)
#define IS_MG_PARAMETER(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_parameter_get_type ())

/* error reporting */
extern GQuark mg_parameter_error_quark (void);
#define MG_PARAMETER_ERROR mg_parameter_error_quark ()

enum {
	MG_PARAMETER_QUERY_LIMIT_ERROR
};

/* struct for the object's data */
struct _MgParameter
{
	MgBase                    object;
	MgParameterPrivate       *priv;
};


/* struct for the object's class */
struct _MgParameterClass
{
	MgBaseClass                    class;
};

guint             mg_parameter_get_type           (void);
GObject          *mg_parameter_new                (MgConf *conf, MgServerDataType *type);
GObject          *mg_parameter_new_with_field     (MgQfield *field, MgServerDataType *type);

void              mg_parameter_add_dest_field     (MgParameter *param, MgQfield *field);
GSList           *mg_parameter_get_dest_fields    (MgParameter *param);
MgServerDataType *mg_parameter_get_data_type      (MgParameter *param);

const GdaValue   *mg_parameter_get_value          (MgParameter *param);
void              mg_parameter_set_value          (MgParameter *param, const GdaValue *value);
gboolean          mg_parameter_is_valid           (MgParameter *param);

const GdaValue   *mg_parameter_get_default_value  (MgParameter *param);
void              mg_parameter_set_default_value  (MgParameter *param, const GdaValue *value);

void              mg_parameter_set_not_null       (MgParameter *param, gboolean not_null);
gboolean          mg_parameter_get_not_null       (MgParameter *param);

gboolean          mg_parameter_set_in_field       (MgParameter *param, MgQfield *field, GError **error);
MgQfield         *mg_parameter_get_in_field       (MgParameter *param);

void              mg_parameter_add_dependency     (MgParameter *param, MgParameter *depend_on);
void              mg_parameter_del_dependency     (MgParameter *param, MgParameter *depend_on);
GSList           *mg_parameter_get_dependencies   (MgParameter *param);

void              mg_parameter_replace_ref        (MgParameter *param, GHashTable *replacements);

void              mg_parameter_set_alias_of       (MgParameter *param, MgParameter *alias_of);
MgParameter      *mg_parameter_get_alias_of       (MgParameter *param);

gboolean          mg_parameter_requires_user_input (MgParameter *param);
void              mg_parameter_set_user_input_required (MgParameter *param, gboolean input_required);


G_END_DECLS

#endif
