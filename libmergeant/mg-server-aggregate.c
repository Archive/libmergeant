/* mg-server-aggregate.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-server.h"
#include "mg-server-aggregate.h"
#include "mg-server-data-type.h"
#include "mg-xml-storage.h"
#include "mg-data-handler.h"
#include "marshal.h"
#include <libgda/libgda.h>
#include <string.h>

/* 
 * Main static functions 
 */
static void mg_server_aggregate_class_init (MgServerAggregateClass * class);
static void mg_server_aggregate_init (MgServerAggregate * srv);
static void mg_server_aggregate_dispose (GObject   * object);
static void mg_server_aggregate_finalize (GObject   * object);

static void mg_server_aggregate_set_property (GObject              *object,
					     guint                 param_id,
					     const GValue         *value,
					     GParamSpec           *pspec);
static void mg_server_aggregate_get_property (GObject              *object,
					     guint                 param_id,
					     GValue               *value,
					     GParamSpec           *pspec);

#ifdef debug
static void mg_server_aggregate_dump (MgServerAggregate *agg, guint offset);
#endif

static void        mg_aggregate_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_aggregate_get_xml_id (MgXmlStorage *iface);
static xmlNodePtr  mg_aggregate_save_to_xml (MgXmlStorage *iface, GError **error);
static gboolean    mg_aggregate_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	TEMPL_SIGNAL,
	LAST_SIGNAL
};

static gint mg_server_aggregate_signals[LAST_SIGNAL] = { 0 };

/* properties */
enum
{
	PROP_0,
	PROP
};


/* private structure */
struct _MgServerAggregatePrivate
{
	MgServer         *srv;
	gchar            *objectid;       /* unique id for the aggregate */
	MgServerDataType *result_type;
	MgServerDataType *arg_type;
};


/* module error */
GQuark mg_server_aggregate_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_server_aggregate_error");
	return quark;
}


guint
mg_server_aggregate_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgServerAggregateClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_server_aggregate_class_init,
			NULL,
			NULL,
			sizeof (MgServerAggregate),
			0,
			(GInstanceInitFunc) mg_server_aggregate_init
		};

		static const GInterfaceInfo xml_storage_info = {
			(GInterfaceInitFunc) mg_aggregate_xml_storage_init,
			NULL,
			NULL
		};
		
		type = g_type_register_static (MG_BASE_TYPE, "MgServerAggregate", &info, 0);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
	}
	return type;
}

static void 
mg_aggregate_xml_storage_init (MgXmlStorageIface *iface)
{
	iface->get_xml_id = mg_aggregate_get_xml_id;
	iface->save_to_xml = mg_aggregate_save_to_xml;
	iface->load_from_xml = mg_aggregate_load_from_xml;
}


static void
mg_server_aggregate_class_init (MgServerAggregateClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_server_aggregate_signals[TEMPL_SIGNAL] =
		g_signal_new ("templ_signal",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgServerAggregateClass, templ_signal),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	class->templ_signal = NULL;

	object_class->dispose = mg_server_aggregate_dispose;
	object_class->finalize = mg_server_aggregate_finalize;

	/* Properties */
	object_class->set_property = mg_server_aggregate_set_property;
	object_class->get_property = mg_server_aggregate_get_property;
	g_object_class_install_property (object_class, PROP,
					 g_param_spec_pointer ("prop", NULL, NULL, (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_server_aggregate_dump;
#endif

}

static void
mg_server_aggregate_init (MgServerAggregate * mg_server_aggregate)
{
	mg_server_aggregate->priv = g_new0 (MgServerAggregatePrivate, 1);
	mg_server_aggregate->priv->srv = NULL;
	mg_server_aggregate->priv->objectid = NULL;
	mg_server_aggregate->priv->result_type = NULL;
	mg_server_aggregate->priv->arg_type = NULL;
}


/**
 * mg_server_aggregate_new
 * @srv: a #MgServer object
 *
 * Creates a new MgServerAggregate object
 *
 * Returns: the new object
 */
GObject*
mg_server_aggregate_new (MgServer *srv)
{
	GObject   *obj;
	MgServerAggregate *mg_server_aggregate;

	g_return_val_if_fail (srv && MG_SERVER (srv), NULL);

	obj = g_object_new (MG_SERVER_AGGREGATE_TYPE, "conf",
			    mg_server_get_conf (srv), NULL);
	mg_server_aggregate = MG_SERVER_AGGREGATE (obj);
	mg_base_set_id (MG_BASE (mg_server_aggregate), 0);
	g_object_add_weak_pointer (G_OBJECT (srv), (gpointer) &(mg_server_aggregate->priv->srv));
	mg_server_aggregate->priv->srv = srv;

	return obj;
}


static void
mg_server_aggregate_dispose (GObject *object)
{
	MgServerAggregate *mg_server_aggregate;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_SERVER_AGGREGATE (object));

	mg_server_aggregate = MG_SERVER_AGGREGATE (object);
	if (mg_server_aggregate->priv) {
		mg_base_nullify_check (MG_BASE (object));

		g_object_remove_weak_pointer (G_OBJECT (mg_server_aggregate->priv->srv), 
					      (gpointer) &(mg_server_aggregate->priv->srv));

		mg_server_aggregate_set_ret_type (mg_server_aggregate, NULL);
		mg_server_aggregate_set_arg_type (mg_server_aggregate, NULL);
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_server_aggregate_finalize (GObject   * object)
{
	MgServerAggregate *mg_server_aggregate;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_SERVER_AGGREGATE (object));

	mg_server_aggregate = MG_SERVER_AGGREGATE (object);
	if (mg_server_aggregate->priv) {

		g_free (mg_server_aggregate->priv);
		mg_server_aggregate->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_server_aggregate_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	gpointer ptr;
	MgServerAggregate *mg_server_aggregate;

	mg_server_aggregate = MG_SERVER_AGGREGATE (object);
	if (mg_server_aggregate->priv) {
		switch (param_id) {
		case PROP:
			/* FIXME */
			ptr = g_value_get_pointer (value);
			break;
		}
	}
}

static void
mg_server_aggregate_get_property (GObject              *object,
			guint                 param_id,
			GValue               *value,
			GParamSpec           *pspec)
{
	MgServerAggregate *mg_server_aggregate;
	mg_server_aggregate = MG_SERVER_AGGREGATE (object);
	
	if (mg_server_aggregate->priv) {
		switch (param_id) {
		case PROP:
			/* FIXME */
			g_value_set_pointer (value, NULL);
			break;
		}	
	}
}

#ifdef debug
static void
mg_server_aggregate_dump (MgServerAggregate *agg, guint offset)
{
	gchar *str;
	gint i;

	g_return_if_fail (agg && IS_MG_SERVER_AGGREGATE (agg));
	g_return_if_fail (agg->priv);
	
	/* string for the offset */
	str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;

	/* return type */
	g_print ("%s" D_COL_H1 "MgServerAggregate" D_COL_NOR " %s (%p)\n",
		 str, mg_base_get_name (MG_BASE (agg)), agg);
	if (agg->priv->result_type)
		g_print ("%sReturn data type: %s\n", str, mg_server_data_type_get_sqlname (agg->priv->result_type));
	else
		g_print ("%s" D_COL_ERR "No return type defined" D_COL_NOR "\n", str);

	if (agg->priv->arg_type)
		g_print ("%sParameter data type: %s\n", str, mg_server_data_type_get_sqlname (agg->priv->arg_type));
	else
		g_print ("%sAny argument type allowed\n", str);

	g_free (str);
}
#endif



/* MgXmlStorage interface implementation */
static gchar *
mg_aggregate_get_xml_id (MgXmlStorage *iface)
{
	g_return_val_if_fail (iface && IS_MG_SERVER_AGGREGATE (iface), NULL);
	g_return_val_if_fail (MG_SERVER_AGGREGATE (iface)->priv, NULL);

	return g_strdup_printf ("AG%s", MG_SERVER_AGGREGATE (iface)->priv->objectid);
}

static xmlNodePtr
mg_aggregate_save_to_xml (MgXmlStorage *iface, GError **error)
{
	xmlNodePtr node = NULL, subnode;
	MgServerAggregate *agg;
	gchar *str;

	g_return_val_if_fail (iface && IS_MG_SERVER_AGGREGATE (iface), NULL);
	g_return_val_if_fail (MG_SERVER_AGGREGATE (iface)->priv, NULL);

	agg = MG_SERVER_AGGREGATE (iface);

	node = xmlNewNode (NULL, "MG_AGGREGATE");

	str = mg_aggregate_get_xml_id (iface);
	xmlSetProp (node, "id", str);
	g_free (str);
	xmlSetProp (node, "name", mg_base_get_name (MG_BASE (agg)));
	xmlSetProp (node, "descr", mg_base_get_description (MG_BASE (agg)));
	xmlSetProp (node, "owner", mg_base_get_owner (MG_BASE (agg)));

	/* return type */
	if (agg->priv->result_type) {
		subnode = xmlNewChild (node, NULL, "MG_FUNC_PARAM", NULL);
		
		str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (agg->priv->result_type));
		xmlSetProp (subnode, "type", str);
		g_free (str);
		xmlSetProp (subnode, "way", "out");
	}

	/* argument type */
	if (agg->priv->arg_type) {
		subnode = xmlNewChild (node, NULL, "MG_FUNC_PARAM", NULL);
		
		str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (agg->priv->arg_type));
		xmlSetProp (subnode, "type", str);
		g_free (str);
		xmlSetProp (subnode, "way", "in");
	}

	return node;
}

static gboolean
mg_aggregate_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgServerAggregate *agg;
	gchar *prop;
	gboolean pname = FALSE, pid = FALSE;
	xmlNodePtr subnode;

	g_return_val_if_fail (iface && IS_MG_SERVER_AGGREGATE (iface), FALSE);
	g_return_val_if_fail (MG_SERVER_AGGREGATE (iface)->priv, FALSE);
	g_return_val_if_fail (node, FALSE);

	agg = MG_SERVER_AGGREGATE (iface);
	if (strcmp (node->name, "MG_AGGREGATE")) {
		g_set_error (error,
			     MG_SERVER_AGGREGATE_ERROR,
			     MG_SERVER_AGGREGATE_XML_LOAD_ERROR,
			     _("XML Tag is not <MG_AGGREGATE>"));
		return FALSE;
	}

	/* aggregate's attributes */
	prop = xmlGetProp (node, "id");
	if (prop) {
		if ((*prop == 'A') && (*(prop+1)=='G')) {
			pid = TRUE;
			if (agg->priv->objectid)
				g_free (agg->priv->objectid);
			agg->priv->objectid = g_strdup (prop+2);
		}
		g_free (prop);
	}

	prop = xmlGetProp (node, "name");
	if (prop) {
		pname = TRUE;
		mg_base_set_name (MG_BASE (agg), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "descr");
	if (prop) {
		mg_base_set_description (MG_BASE (agg), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "owner");
	if (prop) {
		mg_base_set_owner (MG_BASE (agg), prop);
		g_free (prop);
	}
	
	/* arguments and return type */
	subnode = node->children;
	while (subnode) {
		if (!strcmp (subnode->name, "MG_FUNC_PARAM")) {
			MgServerDataType *dt = NULL;
			prop = xmlGetProp (subnode, "type");
			if (prop) {
				dt = mg_server_get_data_type_by_xml_id (agg->priv->srv, prop);
				g_free (prop);
			}

			if (!dt) {
				g_set_error (error,
					     MG_SERVER_AGGREGATE_ERROR,
					     MG_SERVER_AGGREGATE_XML_LOAD_ERROR,
					     _("Can't find data type for aggregate '%s'"), 
					     mg_base_get_name (MG_BASE (agg)));
				return FALSE;
			}
			
			prop = xmlGetProp (subnode, "way");
			if (prop) {
				if (*prop == 'o') {
					if (agg->priv->result_type) {
						g_set_error (error,
							     MG_SERVER_AGGREGATE_ERROR,
							     MG_SERVER_AGGREGATE_XML_LOAD_ERROR,
							     _("More than one return type for aggregate '%s'"), 
							     mg_base_get_name (MG_BASE (agg)));
						return FALSE;
					}
					mg_server_aggregate_set_ret_type (agg, dt);
				}
				else {
					if (agg->priv->arg_type) {
						g_set_error (error,
							     MG_SERVER_AGGREGATE_ERROR,
							     MG_SERVER_AGGREGATE_XML_LOAD_ERROR,
							     _("More than one argument type for aggregate '%s'"), 
							     mg_base_get_name (MG_BASE (agg)));
						return FALSE;
					}
					mg_server_aggregate_set_arg_type (agg, dt);
				}
				g_free (prop);
			}
		}
		subnode = subnode->next;
	}

	if (pname && pid)
		return TRUE;
	else {
		g_set_error (error,
			     MG_SERVER_AGGREGATE_ERROR,
			     MG_SERVER_AGGREGATE_XML_LOAD_ERROR,
			     _("Missing required attributes for <MG_AGGREGATE>"));
		return FALSE;
	}
}




/**
 * mg_server_aggregate_set_dbms_id
 * @agg: a #MgServerAggregate object
 * @id: the DBMS identifier
 *
 * Set the DBMS identifier of the aggregate
 */
void
mg_server_aggregate_set_dbms_id (MgServerAggregate *agg, const gchar *id)
{
	g_return_if_fail (agg && IS_MG_SERVER_AGGREGATE (agg));
	g_return_if_fail (agg->priv);
	g_return_if_fail (id && *id);

	if (agg->priv->objectid)
		g_free (agg->priv->objectid);
	agg->priv->objectid = g_strdup (id);
}


/**
 * mg_server_aggregate_get_dbms_id
 * @agg: a #MgServerAggregate object
 *
 * Get the DBMS identifier of the aggregate
 *
 * Returns: the aggregate's id
 */
const gchar *
mg_server_aggregate_get_dbms_id (MgServerAggregate *agg)
{
	g_return_val_if_fail (agg && IS_MG_SERVER_AGGREGATE (agg), NULL);
	g_return_val_if_fail (agg->priv, NULL);

	return agg->priv->objectid;
}


/**
 * mg_server_aggregate_set_sqlname
 * @agg: a #MgServerAggregate object
 * @sqlname: 
 *
 * Set the SQL name of the data type.
 */
void
mg_server_aggregate_set_sqlname (MgServerAggregate *agg, const gchar *sqlname)
{
	g_return_if_fail (agg && IS_MG_SERVER_AGGREGATE (agg));
	g_return_if_fail (agg->priv);

	mg_base_set_name (MG_BASE (agg), sqlname);
}


/**
 * mg_server_aggregate_get_sqlname
 * @agg: a #MgServerAggregate object
 *
 * Get the DBMS's name of a data type.
 *
 * Returns: the name of the data type
 */
const gchar *
mg_server_aggregate_get_sqlname (MgServerAggregate *agg)
{
	g_return_val_if_fail (agg && IS_MG_SERVER_AGGREGATE (agg), NULL);
	g_return_val_if_fail (agg->priv, NULL);

	return mg_base_get_name (MG_BASE (agg));
}

static void nullified_data_type_cb (MgServerDataType *dt, MgServerAggregate *agg);

/**
 * mg_server_aggregate_set_arg_type
 * @agg: a #MgServerAggregate object
 * @dt: a #MgServerDataType objects or #NULL value to represent the data type
 * of the aggregate's unique argument .
 *
 * Set the argument type of a aggregate
 */
void 
mg_server_aggregate_set_arg_type (MgServerAggregate *agg, MgServerDataType *dt)
{
	g_return_if_fail (agg && IS_MG_SERVER_AGGREGATE (agg));
	g_return_if_fail (agg->priv);
	if (dt)
		g_return_if_fail (dt && IS_MG_SERVER_DATA_TYPE (dt));
	
	if (agg->priv->arg_type) { 
		g_signal_handlers_disconnect_by_func (G_OBJECT (agg->priv->arg_type), 
						      G_CALLBACK (nullified_data_type_cb), agg);
		g_object_unref (G_OBJECT (agg->priv->arg_type));
	}

	agg->priv->arg_type = dt;
	if (dt) {
		g_signal_connect (G_OBJECT (dt), "nullified",
				  G_CALLBACK (nullified_data_type_cb), agg);
		g_object_ref (G_OBJECT (dt));
	}
}

/**
 * mg_server_aggregate_get_arg_type
 * @agg: a #MgServerAggregate object
 * 
 * To consult the list of arguments types (and number) of a aggregate.
 *
 * Returns: a list of #MgServerDataType objects, the list MUST NOT be modified.
 */
MgServerDataType *
mg_server_aggregate_get_arg_type (MgServerAggregate *agg)
{
	g_return_val_if_fail (agg && IS_MG_SERVER_AGGREGATE (agg), NULL);
	g_return_val_if_fail (agg->priv, NULL);

	return agg->priv->arg_type;
}

/**
 * mg_server_aggregate_set_ret_type
 * @agg: a #MgServerAggregate object
 * @dt: a #MgServerDataType object or #NULL
 *
 * Set the return type of a aggregate
 */
void 
mg_server_aggregate_set_ret_type  (MgServerAggregate *agg, MgServerDataType *dt)
{
	g_return_if_fail (agg && IS_MG_SERVER_AGGREGATE (agg));
	g_return_if_fail (agg->priv);
	if (dt)
		g_return_if_fail (dt && IS_MG_SERVER_DATA_TYPE (dt));
	
	if (agg->priv->result_type) { 
		g_signal_handlers_disconnect_by_func (G_OBJECT (agg->priv->result_type), 
						      G_CALLBACK (nullified_data_type_cb), agg);
		g_object_unref (G_OBJECT (agg->priv->result_type));
	}

	agg->priv->result_type = dt;
	if (dt) {
		g_signal_connect (G_OBJECT (dt), "nullified",
				  G_CALLBACK (nullified_data_type_cb), agg);
		g_object_ref (G_OBJECT (dt));
	}
}

static void
nullified_data_type_cb (MgServerDataType *dt, MgServerAggregate *agg)
{
	mg_base_nullify (MG_BASE (agg));
}

/**
 * mg_server_aggregate_get_ret_type
 * @agg: a #MgServerAggregate object
 * 
 * To consult the return type of a aggregate.
 *
 * Returns: a #MgServerDataType object.
 */
MgServerDataType *
mg_server_aggregate_get_ret_type  (MgServerAggregate *agg)
{
	g_return_val_if_fail (agg && IS_MG_SERVER_AGGREGATE (agg), NULL);
	g_return_val_if_fail (agg->priv, NULL);

	return agg->priv->result_type;
}
