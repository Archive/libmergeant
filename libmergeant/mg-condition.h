/* mg-condition.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_CONDITION_H_
#define __MG_CONDITION_H_

#include "mg-base.h"
#include "mg-defs.h"
#include <libgda/libgda.h>

/* Implements the MgXmlStorage, MgReferer and MgRenderer interfaces */

G_BEGIN_DECLS

#define MG_CONDITION_TYPE          (mg_condition_get_type())
#define MG_CONDITION(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_condition_get_type(), MgCondition)
#define MG_CONDITION_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_condition_get_type (), MgConditionClass)
#define IS_MG_CONDITION(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_condition_get_type ())

/* error reporting */
extern GQuark mg_condition_error_quark (void);
#define MG_CONDITION_ERROR mg_condition_error_quark ()

/* different possible types for a query */
typedef enum {
        MG_CONDITION_NODE_AND,
        MG_CONDITION_NODE_OR,
        MG_CONDITION_NODE_NOT,
	MG_CONDITION_LEAF_EQUAL,
        MG_CONDITION_LEAF_DIFF,
        MG_CONDITION_LEAF_SUP,
        MG_CONDITION_LEAF_SUPEQUAL,
        MG_CONDITION_LEAF_INF,
        MG_CONDITION_LEAF_INFEQUAL,
        MG_CONDITION_LEAF_LIKE,
        MG_CONDITION_LEAF_REGEX,
        MG_CONDITION_LEAF_IN,
        MG_CONDITION_LEAF_BETWEEN,
        MG_CONDITION_TYPE_UNKNOWN
} MgConditionType;

typedef enum {
	MG_CONDITION_OP_LEFT   = 0,
	MG_CONDITION_OP_RIGHT  = 1,
	MG_CONDITION_OP_RIGHT2 = 2
} MgConditionOperator;

enum
{
	MG_CONDITION_XML_LOAD_ERROR,
	MG_CONDITION_RENDERER_ERROR,
	MG_CONDITION_PARENT_ERROR
};


/* struct for the object's data */
struct _MgCondition
{
	MgBase               object;
	MgConditionPrivate  *priv;
};

/* struct for the object's class */
struct _MgConditionClass
{
	MgBaseClass                    class;
};

guint            mg_condition_get_type            (void);
GObject         *mg_condition_new                 (MgQuery *query, MgConditionType type);
GObject         *mg_condition_new_copy            (MgCondition *orig, GHashTable *replacements);

void             mg_condition_set_cond_type       (MgCondition *condition, MgConditionType type);
MgConditionType  mg_condition_get_cond_type       (MgCondition *condition);

GSList          *mg_condition_get_children        (MgCondition *condition);
MgCondition     *mg_condition_get_parent          (MgCondition *condition);
MgCondition     *mg_condition_get_child_by_xml_id (MgCondition *condition, const gchar *xml_id);
gboolean         mg_condition_is_ancestor         (MgCondition *condition, MgCondition *ancestor);
gboolean         mg_condition_is_leaf             (MgCondition *condition);

/* Node conditions */
gboolean         mg_condition_node_add_child      (MgCondition *condition, MgCondition *child, GError **error);
void             mg_condition_node_del_child      (MgCondition *condition, MgCondition *child);


/* Leaf conditions */
void             mg_condition_leaf_set_operator  (MgCondition *condition, 
						  MgConditionOperator op, MgQfield *field);
MgQfield        *mg_condition_leaf_get_operator  (MgCondition *condition, MgConditionOperator op);

GSList          *mg_condition_get_ref_objects_all(MgCondition *condition);

G_END_DECLS

#endif
