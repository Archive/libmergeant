/* mg-db-table.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_DB_TABLE_H_
#define __MG_DB_TABLE_H_

#include "mg-base.h"
#include "mg-defs.h"
#include <libgda/libgda.h>

G_BEGIN_DECLS

#define MG_DB_TABLE_TYPE          (mg_db_table_get_type())
#define MG_DB_TABLE(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_db_table_get_type(), MgDbTable)
#define MG_DB_TABLE_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_db_table_get_type (), MgDbTableClass)
#define IS_MG_DB_TABLE(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_db_table_get_type ())


/* error reporting */
extern GQuark mg_db_table_error_quark (void);
#define MG_DB_TABLE_ERROR mg_db_table_error_quark ()

enum
{
	MG_DB_TABLE_XML_LOAD_ERROR,
	MG_DB_TABLE_META_DATA_UPDATE,
	MG_DB_TABLE_FIELDS_ERROR
};


/* struct for the object's data */
struct _MgDbTable
{
	MgBase                  object;
	MgDbTablePrivate       *priv;
};

/* struct for the object's class */
struct _MgDbTableClass
{
	MgBaseClass                    class;

	/* signals */
	void   (*templ_signal)        (MgDbTable *obj);
};

guint           mg_db_table_get_type          (void);
GObject        *mg_db_table_new               (MgConf *conf);

MgDatabase     *mg_db_table_get_database      (MgDbTable *table);
gboolean        mg_db_table_is_view           (MgDbTable *table);
const GSList   *mg_db_table_get_parents       (MgDbTable *table);
GSList         *mg_db_table_get_constraints   (MgDbTable *table);
MgConf         *mg_db_table_get_conf          (MgDbTable *table);

gboolean        mg_db_table_update_dbms_data  (MgDbTable *table, GError **error);

G_END_DECLS

#endif
