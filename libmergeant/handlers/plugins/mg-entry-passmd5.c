/* mg-entry-passmd5.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-entry-passmd5.h"
#include <libmergeant/mg-data-handler.h>
#include <openssl/evp.h>
#include <string.h>

/* 
 * Main static functions 
 */
static void mg_entry_passmd5_class_init (MgEntryPassmd5Class * class);
static void mg_entry_passmd5_init (MgEntryPassmd5 * srv);
static void mg_entry_passmd5_dispose (GObject   * object);
static void mg_entry_passmd5_finalize (GObject   * object);

/* virtual functions */
static GtkWidget *create_entry (MgEntryWrapper *mgwrap);
static void       real_set_value (MgEntryWrapper *mgwrap, const GdaValue *value);
static GdaValue  *real_get_value (MgEntryWrapper *mgwrap);
static void       connect_signals(MgEntryWrapper *mgwrap, GCallback callback);
static gboolean   expand_in_layout (MgEntryWrapper *mgwrap);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* private structure */
struct _MgEntryPassmd5Private
{
	GtkWidget  *entry;
};


guint
mg_entry_passmd5_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgEntryPassmd5Class),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_entry_passmd5_class_init,
			NULL,
			NULL,
			sizeof (MgEntryPassmd5),
			0,
			(GInstanceInitFunc) mg_entry_passmd5_init
		};
		
		type = g_type_register_static (MG_ENTRY_WRAPPER_TYPE, "MgEntryPassmd5", &info, 0);
	}
	return type;
}

static void
mg_entry_passmd5_class_init (MgEntryPassmd5Class * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_entry_passmd5_dispose;
	object_class->finalize = mg_entry_passmd5_finalize;

	MG_ENTRY_WRAPPER_CLASS (class)->create_entry = create_entry;
	MG_ENTRY_WRAPPER_CLASS (class)->real_set_value = real_set_value;
	MG_ENTRY_WRAPPER_CLASS (class)->real_get_value = real_get_value;
	MG_ENTRY_WRAPPER_CLASS (class)->connect_signals = connect_signals;
	MG_ENTRY_WRAPPER_CLASS (class)->expand_in_layout = expand_in_layout;
}

static void
mg_entry_passmd5_init (MgEntryPassmd5 * mg_entry_passmd5)
{
	mg_entry_passmd5->priv = g_new0 (MgEntryPassmd5Private, 1);
	mg_entry_passmd5->priv->entry = NULL;
}

/**
 * mg_entry_passmd5_new
 * @dh: the data handler to be used by the new widget
 * @type: the requested data type (compatible with @dh)
 *
 * Creates a new widget which is mainly a GtkEntry
 *
 * Returns: the new widget
 */
GtkWidget *
mg_entry_passmd5_new (MgDataHandler *dh, GdaValueType type)
{
	GObject *obj;
	MgEntryPassmd5 *mgtxt;

	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (type != GDA_VALUE_TYPE_UNKNOWN, NULL);
	g_return_val_if_fail (mg_data_handler_accepts_gda_type (dh, type), NULL);

	obj = g_object_new (MG_ENTRY_PASSMD5_TYPE, "handler", dh, NULL);
	mgtxt = MG_ENTRY_PASSMD5 (obj);
	mg_data_entry_set_value_type (MG_DATA_ENTRY (mgtxt), type);

	return GTK_WIDGET (obj);
}


static void
mg_entry_passmd5_dispose (GObject   * object)
{
	MgEntryPassmd5 *mg_entry_passmd5;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_PASSMD5 (object));

	mg_entry_passmd5 = MG_ENTRY_PASSMD5 (object);
	if (mg_entry_passmd5->priv) {

	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_entry_passmd5_finalize (GObject   * object)
{
	MgEntryPassmd5 *mg_entry_passmd5;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_PASSMD5 (object));

	mg_entry_passmd5 = MG_ENTRY_PASSMD5 (object);
	if (mg_entry_passmd5->priv) {

		g_free (mg_entry_passmd5->priv);
		mg_entry_passmd5->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

static GtkWidget *
create_entry (MgEntryWrapper *mgwrap)
{
	GtkWidget *entry;
        MgEntryPassmd5 *mgstr;

        g_return_val_if_fail (mgwrap && IS_MG_ENTRY_PASSMD5 (mgwrap), NULL);
        mgstr = MG_ENTRY_PASSMD5 (mgwrap);
        g_return_val_if_fail (mgstr->priv, NULL);

        entry = gtk_entry_new ();
        mgstr->priv->entry = entry;
	gtk_entry_set_visibility (GTK_ENTRY (entry), FALSE);

        return entry;
}

static void
real_set_value (MgEntryWrapper *mgwrap, const GdaValue *value)
{
	MgEntryPassmd5 *mgstr;

        g_return_if_fail (mgwrap && IS_MG_ENTRY_PASSMD5 (mgwrap));
        mgstr = MG_ENTRY_PASSMD5 (mgwrap);
        g_return_if_fail (mgstr->priv);

        if (value) {
                if (gda_value_is_null (value))
                        gtk_entry_set_text (GTK_ENTRY (mgstr->priv->entry), "");
                else {
                        MgDataHandler *dh;
                        gchar *str;

                        dh = mg_data_entry_get_handler (MG_DATA_ENTRY (mgwrap));
                        str = mg_data_handler_get_str_from_value (dh, value);
                        gtk_entry_set_text (GTK_ENTRY (mgstr->priv->entry), str);
                        g_free (str);
                }
        }
        else
                gtk_entry_set_text (GTK_ENTRY (mgstr->priv->entry), "");
}

static GdaValue *
real_get_value (MgEntryWrapper *mgwrap)
{
	GdaValue *value;
        MgEntryPassmd5 *mgstr;
        MgDataHandler *dh;
        const gchar *str;

	EVP_MD_CTX mdctx;
        const EVP_MD *md;
        unsigned char md5str [EVP_MAX_MD_SIZE+1];
        int md_len, i;
        GString *md5pass;

        g_return_val_if_fail (mgwrap && IS_MG_ENTRY_PASSMD5 (mgwrap), NULL);
        mgstr = MG_ENTRY_PASSMD5 (mgwrap);
        g_return_val_if_fail (mgstr->priv, NULL);

        dh = mg_data_entry_get_handler (MG_DATA_ENTRY (mgwrap));
        str = gtk_entry_get_text (GTK_ENTRY (mgstr->priv->entry));

        /* calcul MD5 */
        OpenSSL_add_all_digests ();
        md = EVP_md5 ();
        EVP_DigestInit(&mdctx, md);
        EVP_DigestUpdate(&mdctx, str, strlen(str));
        EVP_DigestFinal(&mdctx, md5str, &md_len);
        md5str [md_len] = 0;
        md5pass = g_string_new ("");
        for(i = 0; i < md_len; i++)
                g_string_append_printf (md5pass, "%02x", md5str[i]);

        value = mg_data_handler_get_value_from_sql (dh, md5pass->str, mg_data_entry_get_value_type (MG_DATA_ENTRY (mgwrap)));
        g_string_free (md5pass, TRUE);

        if (!value) {
                /* in case the mg_data_handler_get_value_from_sql() returned an error because
                   the contents of the GtkEntry cannot be interpreted as a GdaValue */
                value = gda_value_new_null ();
        }

        return value;
}

static void
connect_signals(MgEntryWrapper *mgwrap, GCallback callback)
{
	MgEntryPassmd5 *mgstr;

        g_return_if_fail (mgwrap && IS_MG_ENTRY_PASSMD5 (mgwrap));
        mgstr = MG_ENTRY_PASSMD5 (mgwrap);
        g_return_if_fail (mgstr->priv);

        g_signal_connect (G_OBJECT (mgstr->priv->entry), "changed",
                          callback, mgwrap);
}

static gboolean
expand_in_layout (MgEntryWrapper *mgwrap)
{
	return FALSE;
}
