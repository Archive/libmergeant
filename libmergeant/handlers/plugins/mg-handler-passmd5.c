/* mg-handler-passmd5.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-handler-passmd5.h"
#include "mg-entry-passmd5.h"
#include <libmergeant/mg-server.h>
#include <gmodule.h>

static void mg_handler_passmd5_class_init (MgHandlerPassmd5Class * class);
static void mg_handler_passmd5_init (MgHandlerPassmd5 * wid);
static void mg_handler_passmd5_dispose (GObject   * object);

/* plugin initialisation */
MgDataHandler* plugin_init (MgServer *srv, GModule *module);

/* MgDataHandler interface */
static void         mg_handler_passmd5_data_handler_init      (MgDataHandlerIface *iface);
static MgDataEntry *mg_handler_passmd5_get_entry_from_value   (MgDataHandler *dh, const GdaValue *value, 
							      GdaValueType type);
static gchar       *mg_handler_passmd5_get_sql_from_value     (MgDataHandler *dh, const GdaValue *value);
static gchar       *mg_handler_passmd5_get_str_from_value     (MgDataHandler *dh, const GdaValue *value);
static GdaValue    *mg_handler_passmd5_get_value_from_sql     (MgDataHandler *dh, const gchar *sql, 
							      GdaValueType type);
static GdaValue    *mg_handler_passmd5_get_sane_init_value    (MgDataHandler * dh, GdaValueType type);

static guint        mg_handler_passmd5_get_nb_gda_types       (MgDataHandler *dh);
static GdaValueType mg_handler_passmd5_get_gda_type_index     (MgDataHandler *dh, guint index);
static gboolean     mg_handler_passmd5_accepts_gda_type       (MgDataHandler * dh, GdaValueType type);

static const gchar *mg_handler_passmd5_get_descr              (MgDataHandler *dh);
static const gchar *mg_handler_passmd5_get_descr_detail       (MgDataHandler *dh);
static const gchar *mg_handler_passmd5_get_version            (MgDataHandler *dh);
static gboolean     mg_handler_passmd5_is_plugin              (MgDataHandler *dh);
static const gchar *mg_handler_passmd5_get_plugin_name        (MgDataHandler *dh);
static const gchar *mg_handler_passmd5_get_plugin_file        (MgDataHandler *dh);
static gchar       *mg_handler_passmd5_get_key                (MgDataHandler *dh);


/* signals */
enum
{
	LAST_SIGNAL
};

static gint mg_handler_passmd5_signals[LAST_SIGNAL] = { };

struct  _MgHandlerPassmd5Priv {
	gchar          *detailled_descr;
	guint           nb_gda_types;
	GdaValueType   *valid_gda_types;
	MgServer       *srv;
	MgDataHandler  *string_handler;

	GModule        *module;
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

guint
mg_handler_passmd5_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgHandlerPassmd5Class),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_handler_passmd5_class_init,
			NULL,
			NULL,
			sizeof (MgHandlerPassmd5),
			0,
			(GInstanceInitFunc) mg_handler_passmd5_init
		};		

		static const GInterfaceInfo data_entry_info = {
			(GInterfaceInitFunc) mg_handler_passmd5_data_handler_init,
			NULL,
			NULL
		};

		type = g_type_register_static (MG_BASE_TYPE, "MgHandlerPassmd5", &info, 0);
		g_type_add_interface_static (type, MG_DATA_HANDLER_TYPE, &data_entry_info);
	}
	return type;
}

static void
mg_handler_passmd5_data_handler_init (MgDataHandlerIface *iface)
{
	iface->get_entry_from_value = mg_handler_passmd5_get_entry_from_value;
	iface->get_sql_from_value = mg_handler_passmd5_get_sql_from_value;
	iface->get_str_from_value = mg_handler_passmd5_get_str_from_value;
	iface->get_value_from_sql = mg_handler_passmd5_get_value_from_sql;
	iface->get_value_from_str = NULL;
	iface->get_sane_init_value = mg_handler_passmd5_get_sane_init_value;
	iface->get_nb_gda_types = mg_handler_passmd5_get_nb_gda_types;
	iface->accepts_gda_type = mg_handler_passmd5_accepts_gda_type;
	iface->get_gda_type_index = mg_handler_passmd5_get_gda_type_index;
	iface->get_descr = mg_handler_passmd5_get_descr;
	iface->get_descr_detail = mg_handler_passmd5_get_descr_detail;
	iface->get_version = mg_handler_passmd5_get_version;
	iface->is_plugin = mg_handler_passmd5_is_plugin;
	iface->get_plugin_name = mg_handler_passmd5_get_plugin_name;
	iface->get_plugin_file = mg_handler_passmd5_get_plugin_file;
	iface->get_key = mg_handler_passmd5_get_key;
}


static void
mg_handler_passmd5_class_init (MgHandlerPassmd5Class * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_handler_passmd5_dispose;
}

static void
mg_handler_passmd5_init (MgHandlerPassmd5 * hdl)
{
	/* Private structure */
	hdl->priv = g_new0 (MgHandlerPassmd5Priv, 1);
	hdl->priv->detailled_descr = _("Data handler for passwords stored as MD5 strings");
	hdl->priv->nb_gda_types = 1;
	hdl->priv->valid_gda_types = g_new0 (GdaValueType, 1);
	hdl->priv->valid_gda_types[0] = GDA_VALUE_TYPE_STRING;
	hdl->priv->srv = NULL;
	hdl->priv->string_handler = NULL;
	hdl->priv->module = NULL;

	mg_base_set_name (MG_BASE (hdl), mg_handler_passmd5_get_plugin_name (hdl));
	mg_base_set_description (MG_BASE (hdl), _("MD5 password handling"));
}

static void
mg_handler_passmd5_dispose (GObject *object)
{
	MgHandlerPassmd5 *hdl;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_HANDLER_PASSMD5 (object));

	hdl = MG_HANDLER_PASSMD5 (object);

	if (hdl->priv) {
		mg_base_nullify_check (MG_BASE (object));
		
		g_free (hdl->priv->valid_gda_types);
		hdl->priv->valid_gda_types = NULL;
		
		if (hdl->priv->srv)
			g_object_remove_weak_pointer (G_OBJECT (hdl->priv->srv),
						      (gpointer *) & (hdl->priv->srv));

		g_free (hdl->priv);
		hdl->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

/*
 * Plugin initialization
 */
MgDataHandler*
plugin_init (MgServer *srv, GModule *module)
{
	MgHandlerPassmd5 *passmd5;

	passmd5 = MG_HANDLER_PASSMD5 (mg_handler_passmd5_new (srv));
	passmd5->priv->module = module;
	
	return MG_DATA_HANDLER (passmd5);
}


/**
 * mg_handler_passmd5_new
 * @srv: a #MgServer object
 *
 * Creates a data handler for passmd5s
 *
 * Returns: the new object
 */
GObject *
mg_handler_passmd5_new (MgServer *srv)
{
	GObject *obj;
	MgHandlerPassmd5 *hdl;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	obj = g_object_new (MG_HANDLER_PASSMD5_TYPE, NULL);
	hdl = MG_HANDLER_PASSMD5 (obj);

	g_object_add_weak_pointer (G_OBJECT (srv), (gpointer *) &(hdl->priv->srv));
	hdl->priv->srv = srv;

	/* get a pointer to the String Handler which we will use */
	hdl->priv->string_handler = mg_server_get_handler_by_gda (srv, GDA_VALUE_TYPE_STRING);
	g_assert (hdl->priv->string_handler);

	return obj;
}


/* Interface implementation */
static MgDataEntry *
mg_handler_passmd5_get_entry_from_value (MgDataHandler *iface, const GdaValue *value, GdaValueType type)
{
	MgHandlerPassmd5 *hdl;
	MgDataEntry *de;
	GdaValueType real_type;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), NULL);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, NULL);

	if (value && (gda_value_get_type (value) != GDA_VALUE_TYPE_NULL)) {
                real_type = gda_value_get_type (value);
                g_return_val_if_fail (mg_data_handler_accepts_gda_type (iface, type), NULL);
        }
        else
                real_type = type;

        de = MG_DATA_ENTRY (mg_entry_passmd5_new (iface, real_type));
        if (value && (gda_value_get_type (value) != GDA_VALUE_TYPE_NULL))
                mg_data_entry_set_value (de, value);
        else 
                mg_data_entry_set_value (de, NULL);

        return de;
}

static gchar *
mg_handler_passmd5_get_sql_from_value (MgDataHandler *iface, const GdaValue *value)
{
	MgHandlerPassmd5 *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), NULL);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, NULL);

	return mg_data_handler_get_sql_from_value (hdl->priv->string_handler, value);
}

static gchar *
mg_handler_passmd5_get_str_from_value (MgDataHandler *iface, const GdaValue *value)
{
	MgHandlerPassmd5 *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), NULL);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, NULL);

	return mg_data_handler_get_str_from_value (hdl->priv->string_handler, value);
}

static GdaValue *
mg_handler_passmd5_get_value_from_sql (MgDataHandler *iface, const gchar *sql, GdaValueType type)
{
	MgHandlerPassmd5 *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), NULL);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, NULL);

	return mg_data_handler_get_value_from_sql (hdl->priv->string_handler, sql, type);
}

static GdaValue *
mg_handler_passmd5_get_sane_init_value (MgDataHandler *iface, GdaValueType type)
{
	MgHandlerPassmd5 *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), NULL);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, NULL);

	return mg_data_handler_get_sane_init_value (hdl->priv->string_handler, type);
}

static guint
mg_handler_passmd5_get_nb_gda_types (MgDataHandler *iface)
{
	MgHandlerPassmd5 *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), 0);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, 0);

	return hdl->priv->nb_gda_types;
}


static gboolean
mg_handler_passmd5_accepts_gda_type (MgDataHandler *iface, GdaValueType type)
{
	MgHandlerPassmd5 *hdl;
	guint i = 0;
	gboolean found = FALSE;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), FALSE);
	g_return_val_if_fail (type != GDA_VALUE_TYPE_UNKNOWN, FALSE);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, 0);

	while (!found && (i < hdl->priv->nb_gda_types)) {
		if (hdl->priv->valid_gda_types [i] == type)
			found = TRUE;
		i++;
	}

	return found;
}

static GdaValueType
mg_handler_passmd5_get_gda_type_index (MgDataHandler *iface, guint index)
{
	MgHandlerPassmd5 *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), GDA_VALUE_TYPE_UNKNOWN);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, GDA_VALUE_TYPE_UNKNOWN);
	g_return_val_if_fail (index < hdl->priv->nb_gda_types, GDA_VALUE_TYPE_UNKNOWN);

	return hdl->priv->valid_gda_types[index];
}

static const gchar *
mg_handler_passmd5_get_descr (MgDataHandler *iface)
{
	MgHandlerPassmd5 *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), NULL);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, NULL);

	return mg_base_get_description (MG_BASE (hdl));
}

static const gchar *
mg_handler_passmd5_get_descr_detail (MgDataHandler *iface)
{
	MgHandlerPassmd5 *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), NULL);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, NULL);

	return hdl->priv->detailled_descr;
}

static const gchar *
mg_handler_passmd5_get_version (MgDataHandler *iface)
{
	MgHandlerPassmd5 *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), NULL);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, NULL);
	
	return g_strdup ("V1R0");
}

static gboolean
mg_handler_passmd5_is_plugin (MgDataHandler *iface)
{
	MgHandlerPassmd5 *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), FALSE);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, FALSE);

	return TRUE;
}

static const gchar *
mg_handler_passmd5_get_plugin_name (MgDataHandler *iface)
{
	MgHandlerPassmd5 *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), NULL);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, NULL);
	
	return "Passmd5 Plugin";
}

static const gchar *
mg_handler_passmd5_get_plugin_file (MgDataHandler *iface)
{
	MgHandlerPassmd5 *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), NULL);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, NULL);
	
	if (hdl->priv->module)
		return g_module_name (hdl->priv->module);
	else
		return NULL;
}

static gchar *
mg_handler_passmd5_get_key (MgDataHandler *iface)
{
	MgHandlerPassmd5 *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_PASSMD5 (iface), NULL);
	hdl = MG_HANDLER_PASSMD5 (iface);
	g_return_val_if_fail (hdl->priv, NULL);
	
	return g_strdup (mg_base_get_name (MG_BASE (hdl)));
}

