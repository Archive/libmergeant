/* mg-entry-text.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-entry-text.h"
#include <libmergeant/mg-data-handler.h>

/* 
 * Main static functions 
 */
static void mg_entry_text_class_init (MgEntryTextClass * class);
static void mg_entry_text_init (MgEntryText * srv);
static void mg_entry_text_dispose (GObject   * object);
static void mg_entry_text_finalize (GObject   * object);

/* virtual functions */
static GtkWidget *create_entry (MgEntryWrapper *mgwrap);
static void       real_set_value (MgEntryWrapper *mgwrap, const GdaValue *value);
static GdaValue  *real_get_value (MgEntryWrapper *mgwrap);
static void       connect_signals(MgEntryWrapper *mgwrap, GCallback callback);
static gboolean   expand_in_layout (MgEntryWrapper *mgwrap);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* private structure */
struct _MgEntryTextPrivate
{
	GtkTextBuffer *buffer;
	GtkWidget     *view;
};


guint
mg_entry_text_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgEntryTextClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_entry_text_class_init,
			NULL,
			NULL,
			sizeof (MgEntryText),
			0,
			(GInstanceInitFunc) mg_entry_text_init
		};
		
		type = g_type_register_static (MG_ENTRY_WRAPPER_TYPE, "MgEntryText", &info, 0);
	}
	return type;
}

static void
mg_entry_text_class_init (MgEntryTextClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_entry_text_dispose;
	object_class->finalize = mg_entry_text_finalize;

	MG_ENTRY_WRAPPER_CLASS (class)->create_entry = create_entry;
	MG_ENTRY_WRAPPER_CLASS (class)->real_set_value = real_set_value;
	MG_ENTRY_WRAPPER_CLASS (class)->real_get_value = real_get_value;
	MG_ENTRY_WRAPPER_CLASS (class)->connect_signals = connect_signals;
	MG_ENTRY_WRAPPER_CLASS (class)->expand_in_layout = expand_in_layout;
}

static void
mg_entry_text_init (MgEntryText * mg_entry_text)
{
	mg_entry_text->priv = g_new0 (MgEntryTextPrivate, 1);
	mg_entry_text->priv->buffer = NULL;
	mg_entry_text->priv->view = NULL;
}

/**
 * mg_entry_text_new
 * @dh: the data handler to be used by the new widget
 * @type: the requested data type (compatible with @dh)
 *
 * Creates a new widget which is mainly a GtkEntry
 *
 * Returns: the new widget
 */
GtkWidget *
mg_entry_text_new (MgDataHandler *dh, GdaValueType type)
{
	GObject *obj;
	MgEntryText *mgtxt;

	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (type != GDA_VALUE_TYPE_UNKNOWN, NULL);
	g_return_val_if_fail (mg_data_handler_accepts_gda_type (dh, type), NULL);

	obj = g_object_new (MG_ENTRY_TEXT_TYPE, "handler", dh, NULL);
	mgtxt = MG_ENTRY_TEXT (obj);
	mg_data_entry_set_value_type (MG_DATA_ENTRY (mgtxt), type);

	return GTK_WIDGET (obj);
}


static void
mg_entry_text_dispose (GObject   * object)
{
	MgEntryText *mg_entry_text;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_TEXT (object));

	mg_entry_text = MG_ENTRY_TEXT (object);
	if (mg_entry_text->priv) {

	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_entry_text_finalize (GObject   * object)
{
	MgEntryText *mg_entry_text;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_TEXT (object));

	mg_entry_text = MG_ENTRY_TEXT (object);
	if (mg_entry_text->priv) {

		g_free (mg_entry_text->priv);
		mg_entry_text->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

static GtkWidget *
create_entry (MgEntryWrapper *mgwrap)
{
	MgEntryText *mgtxt;
	GtkWidget *sw;

	g_return_val_if_fail (mgwrap && IS_MG_ENTRY_TEXT (mgwrap), NULL);
	mgtxt = MG_ENTRY_TEXT (mgwrap);
	g_return_val_if_fail (mgtxt->priv, NULL);

	mgtxt->priv->view = gtk_text_view_new ();
	mgtxt->priv->buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (mgtxt->priv->view));
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (sw), mgtxt->priv->view);
	gtk_widget_show (mgtxt->priv->view);
	
	return sw;
}

static void
real_set_value (MgEntryWrapper *mgwrap, const GdaValue *value)
{
	MgEntryText *mgtxt;

	g_return_if_fail (mgwrap && IS_MG_ENTRY_TEXT (mgwrap));
	mgtxt = MG_ENTRY_TEXT (mgwrap);
	g_return_if_fail (mgtxt->priv);

	if (value) {
		if (gda_value_is_null (value))
                        gtk_text_buffer_set_text (mgtxt->priv->buffer, "", -1);
		else {
			MgDataHandler *dh;		
			gchar *str;

			dh = mg_data_entry_get_handler (MG_DATA_ENTRY (mgwrap));
			str = mg_data_handler_get_str_from_value (dh, value);
			if (str) {
				gtk_text_buffer_set_text (mgtxt->priv->buffer, str, -1);
				g_free (str);
			}
		}
	}
	else 
		gtk_text_buffer_set_text (mgtxt->priv->buffer, "", -1);
}

static GdaValue *
real_get_value (MgEntryWrapper *mgwrap)
{
	GdaValue *value;
	MgEntryText *mgtxt;
	MgDataHandler *dh;
	gchar *str;
	GtkTextIter start, end;

	g_return_val_if_fail (mgwrap && IS_MG_ENTRY_TEXT (mgwrap), NULL);
	mgtxt = MG_ENTRY_TEXT (mgwrap);
	g_return_val_if_fail (mgtxt->priv, NULL);

	dh = mg_data_entry_get_handler (MG_DATA_ENTRY (mgwrap));
	gtk_text_buffer_get_start_iter (mgtxt->priv->buffer, &start);
	gtk_text_buffer_get_end_iter (mgtxt->priv->buffer, &end);
	str = gtk_text_buffer_get_text (mgtxt->priv->buffer, &start, &end, FALSE);
	value = mg_data_handler_get_value_from_sql (dh, str, 
						    mg_data_entry_get_value_type (MG_DATA_ENTRY (mgwrap)));
	g_free (str);
	if (!value) {
		/* in case the mg_data_handler_get_value_from_sql() returned an error because
		   the contents of the GtkEntry cannot be interpreted as a GdaValue */
		value = gda_value_new_null ();
	}

	return value;
}

static void
connect_signals(MgEntryWrapper *mgwrap, GCallback callback)
{
	MgEntryText *mgtxt;

	g_return_if_fail (mgwrap && IS_MG_ENTRY_TEXT (mgwrap));
	mgtxt = MG_ENTRY_TEXT (mgwrap);
	g_return_if_fail (mgtxt->priv);

	g_signal_connect (G_OBJECT (mgtxt->priv->buffer), "changed",
			  callback, mgwrap);
}

static gboolean
expand_in_layout (MgEntryWrapper *mgwrap)
{
	return TRUE;
}
