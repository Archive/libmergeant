/* mg-entry-cidr.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-entry-cidr.h"
#include <libmergeant/mg-data-handler.h>
#include <string.h>

/* 
 * Main static functions 
 */
static void mg_entry_cidr_class_init (MgEntryCidrClass * class);
static void mg_entry_cidr_init (MgEntryCidr * srv);
static void mg_entry_cidr_dispose (GObject   * object);
static void mg_entry_cidr_finalize (GObject   * object);

/* virtual functions */
static GtkWidget *create_entry (MgEntryWrapper *mgwrap);
static void       real_set_value (MgEntryWrapper *mgwrap, const GdaValue *value);
static GdaValue  *real_get_value (MgEntryWrapper *mgwrap);
static void       connect_signals(MgEntryWrapper *mgwrap, GCallback callback);
static gboolean   expand_in_layout (MgEntryWrapper *mgwrap);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* usefull static functions */
static gint     get_ip_nb_bits (MgEntryCidr *mgcidr);
static gint     get_mask_nb_bits (MgEntryCidr *mgcidr);
static gboolean get_complete_value (MgEntryCidr *mgcidr, gboolean target_mask, gulong *result);
static void     truncate_entries_to_mask_length (MgEntryCidr *mgcidr, gboolean target_mask,
						 guint mask_nb_bits);

/* private structure */
struct _MgEntryCidrPrivate
{
	GtkWidget *ip[4];
	GtkWidget *mask[4];
};


guint
mg_entry_cidr_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgEntryCidrClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_entry_cidr_class_init,
			NULL,
			NULL,
			sizeof (MgEntryCidr),
			0,
			(GInstanceInitFunc) mg_entry_cidr_init
		};
		
		type = g_type_register_static (MG_ENTRY_WRAPPER_TYPE, "MgEntryCidr", &info, 0);
	}
	return type;
}

static void
mg_entry_cidr_class_init (MgEntryCidrClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_entry_cidr_dispose;
	object_class->finalize = mg_entry_cidr_finalize;

	MG_ENTRY_WRAPPER_CLASS (class)->create_entry = create_entry;
	MG_ENTRY_WRAPPER_CLASS (class)->real_set_value = real_set_value;
	MG_ENTRY_WRAPPER_CLASS (class)->real_get_value = real_get_value;
	MG_ENTRY_WRAPPER_CLASS (class)->connect_signals = connect_signals;
	MG_ENTRY_WRAPPER_CLASS (class)->expand_in_layout = expand_in_layout;
}

static void
mg_entry_cidr_init (MgEntryCidr * mg_entry_cidr)
{
	gint i;
	mg_entry_cidr->priv = g_new0 (MgEntryCidrPrivate, 1);
	for (i=0; i<4; i++) {
		mg_entry_cidr->priv->ip[i] = NULL;
		mg_entry_cidr->priv->mask[i] = NULL;
	}
}

/**
 * mg_entry_cidr_new
 * @dh: the data handler to be used by the new widget
 * @type: the requested data type (compatible with @dh)
 *
 * Creates a new widget which is mainly a GtkEntry
 *
 * Returns: the new widget
 */
GtkWidget *
mg_entry_cidr_new (MgDataHandler *dh, GdaValueType type)
{
	GObject *obj;
	MgEntryCidr *mgcidr;

	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (type != GDA_VALUE_TYPE_UNKNOWN, NULL);
	g_return_val_if_fail (mg_data_handler_accepts_gda_type (dh, type), NULL);

	obj = g_object_new (MG_ENTRY_CIDR_TYPE, "handler", dh, NULL);
	mgcidr = MG_ENTRY_CIDR (obj);
	mg_data_entry_set_value_type (MG_DATA_ENTRY (mgcidr), type);

	return GTK_WIDGET (obj);
}


static void
mg_entry_cidr_dispose (GObject   * object)
{
	MgEntryCidr *mg_entry_cidr;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_CIDR (object));

	mg_entry_cidr = MG_ENTRY_CIDR (object);
	if (mg_entry_cidr->priv) {

	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_entry_cidr_finalize (GObject   * object)
{
	MgEntryCidr *mg_entry_cidr;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_CIDR (object));

	mg_entry_cidr = MG_ENTRY_CIDR (object);
	if (mg_entry_cidr->priv) {

		g_free (mg_entry_cidr->priv);
		mg_entry_cidr->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static gboolean ip_focus_out_event_cb (GtkEntry *entry, GdkEventFocus *event, MgEntryCidr *mgcidr);
static gboolean mask_focus_out_event_cb (GtkEntry *entry, GdkEventFocus *event, MgEntryCidr *mgcidr);
static void mask_popup (GtkEntry *entry, GtkMenu *arg1, MgEntryCidr *mgcidr);

static GtkWidget *
create_entry (MgEntryWrapper *mgwrap)
{
	GtkWidget *entry, *table, *hbox, *label;
	MgEntryCidr *mgcidr;
	gint i;

	g_return_val_if_fail (mgwrap && IS_MG_ENTRY_CIDR (mgwrap), NULL);
	mgcidr = MG_ENTRY_CIDR (mgwrap);
	g_return_val_if_fail (mgcidr->priv, NULL);

	hbox = gtk_hbox_new (FALSE, 0);
	table = gtk_table_new (2, 7, FALSE);
	gtk_box_pack_start (GTK_BOX (hbox), table, FALSE, FALSE, 0);
	gtk_widget_show (table);

	for (i=0; i<4; i++) {
		if (i) {
			label = gtk_label_new (".");
			gtk_table_attach (GTK_TABLE (table), label, 2*i-1, 2*i, 0, 1, 0, 0, 0, 0);
			gtk_widget_show (label);
		}
		entry = gtk_entry_new ();
		gtk_entry_set_max_length (GTK_ENTRY (entry), 3);
		gtk_entry_set_width_chars (GTK_ENTRY (entry), 3);
		gtk_table_attach_defaults (GTK_TABLE (table), entry, 2*i, 2*i+1, 0, 1);
		gtk_widget_show (entry);
		mgcidr->priv->ip[i] = entry;
		g_signal_connect (G_OBJECT (entry), "focus-out-event",
				  G_CALLBACK (ip_focus_out_event_cb), mgcidr);		

		if (i) {
			label = gtk_label_new (".");
			gtk_table_attach (GTK_TABLE (table), label, 2*i-1, 2*i, 1, 2, 0, 0, 0, 0);
			gtk_widget_show (label);
		}
		entry = gtk_entry_new ();
		gtk_entry_set_max_length (GTK_ENTRY (entry), 3);
		gtk_entry_set_width_chars (GTK_ENTRY (entry), 3);
		gtk_table_attach_defaults (GTK_TABLE (table), entry, 2*i, 2*i+1, 1, 2);
		gtk_widget_show (entry);
		mgcidr->priv->mask[i] = entry;
		g_signal_connect (G_OBJECT (entry), "focus-out-event",
				  G_CALLBACK (mask_focus_out_event_cb), mgcidr);
		g_signal_connect (G_OBJECT (entry), "populate-popup",
				  G_CALLBACK (mask_popup), mgcidr);
	}

	return hbox;
}

/* makes sure the mask part of the widget is compatible with the ip part */
static gboolean
ip_focus_out_event_cb (GtkEntry *entry, GdkEventFocus *event, MgEntryCidr *mgcidr)
{
	gint ip;

	ip = get_ip_nb_bits (mgcidr);
	if (ip >= 0) {
		gint mask;

		mask = get_mask_nb_bits (mgcidr);
		if (ip>mask) {
			int i;
			
			for (i=0; i<4; i++)
				gtk_entry_set_text (GTK_ENTRY (mgcidr->priv->mask[i]), "255");
			truncate_entries_to_mask_length (mgcidr, TRUE, ip);
		}
	}

	return FALSE;
}

/* makes sure the ip part of the widget is truncated to the right number of bits corresponding to
 * the mask part */
static gboolean
mask_focus_out_event_cb (GtkEntry *entry, GdkEventFocus *event, MgEntryCidr *mgcidr)
{
	gint mask;

	mask = get_mask_nb_bits (mgcidr);
	if (mask >= 0) 
		truncate_entries_to_mask_length (mgcidr, FALSE, mask);

	return FALSE;
}

static void popup_menu_item_activate_cb (GtkMenuItem *item, MgEntryCidr *mgcidr);
static void
mask_popup (GtkEntry *entry, GtkMenu *arg1, MgEntryCidr *mgcidr)
{
	GtkWidget *submenu, *item;
	gint net;

	submenu = gtk_menu_item_new_with_label (_("Mask settings"));

	item = gtk_separator_menu_item_new ();
	gtk_menu_shell_prepend (GTK_MENU_SHELL (arg1), item);
	gtk_widget_show (item);
	
	item = gtk_menu_item_new_with_label (_("Set to host mask"));
	gtk_menu_shell_prepend (GTK_MENU_SHELL (arg1), item);
	g_signal_connect (G_OBJECT (item), "activate",
			  G_CALLBACK (popup_menu_item_activate_cb), mgcidr);
	g_object_set_data (G_OBJECT (item), "mask", GINT_TO_POINTER ('D'));
	gtk_widget_show (item);

	for (net='C'; net >= 'A'; net--) {
		gchar *str;
		str = g_strdup_printf (_("Set to class %c network"), net);
		item = gtk_menu_item_new_with_label (str);
		g_free (str);
		gtk_menu_shell_prepend (GTK_MENU_SHELL (arg1), item);
		g_signal_connect (G_OBJECT (item), "activate",
				  G_CALLBACK (popup_menu_item_activate_cb), mgcidr);
		g_object_set_data (G_OBJECT (item), "mask", GINT_TO_POINTER (net));
		gtk_widget_show (item);
	}
}

static void
popup_menu_item_activate_cb (GtkMenuItem *item, MgEntryCidr *mgcidr)
{
	gint i, mask, limit;

	mask = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (item), "mask"));
	switch (mask) {
	default:
	case 'D':
		limit = 4;
		break;
	case 'C':
		limit = 3;
		break;
	case 'B':
		limit = 2;
		break;
	case 'A':
		limit = 1;
		break;
	}

	for (i=0; i<limit; i++)
		gtk_entry_set_text (GTK_ENTRY (mgcidr->priv->mask[i]), "255");
	for (i=limit; i<4; i++)
		gtk_entry_set_text (GTK_ENTRY (mgcidr->priv->mask[i]), "0");

	
	/* force the ip part to be updated */
	mask_focus_out_event_cb (NULL, NULL, mgcidr);
}

static void
real_set_value (MgEntryWrapper *mgwrap, const GdaValue *value)
{
	MgEntryCidr *mgcidr;
	gint i;

	g_return_if_fail (mgwrap && IS_MG_ENTRY_CIDR (mgwrap));
	mgcidr = MG_ENTRY_CIDR (mgwrap);
	g_return_if_fail (mgcidr->priv);

	if (value) {
		if (gda_value_is_null (value))
			for (i=0; i<4; i++) {
				gtk_entry_set_text (GTK_ENTRY (mgcidr->priv->ip[i]), "");
				gtk_entry_set_text (GTK_ENTRY (mgcidr->priv->mask[i]), "");
			}
		else {
			gchar *str, *ptr, *tok;
			gint i=1;
			str = g_strdup (gda_value_get_string (value));

			
			ptr = strtok_r (str, ".", &tok);
			gtk_entry_set_text (GTK_ENTRY (mgcidr->priv->ip[0]), ptr);
			while ((i<4) && ptr) {
				if (i<3)
					ptr = strtok_r (NULL, ".", &tok);
				else
					ptr = strtok_r (NULL, "/", &tok);
				gtk_entry_set_text (GTK_ENTRY (mgcidr->priv->ip[i]), ptr);
				i++;
			}
			
			if (ptr) {
				for (i=0; i<4; i++) 
					gtk_entry_set_text (GTK_ENTRY (mgcidr->priv->mask[i]), "255");
				
				ptr = strtok_r (NULL, "./", &tok);
				if (ptr) {
					gint net;
					
					net = atoi (ptr);
					if (net >= 0)
						truncate_entries_to_mask_length (mgcidr, TRUE, net);
				}
			}

			g_free (str);
		}
	}
	else {
		for (i=0; i<4; i++) {
			gtk_entry_set_text (GTK_ENTRY (mgcidr->priv->ip[i]), "");
			gtk_entry_set_text (GTK_ENTRY (mgcidr->priv->mask[i]), "");
		}
	}
}

static void truncate_entries_to_mask_length (MgEntryCidr *mgcidr, gboolean target_mask, guint mask_nb_bits)
{
	gint i, j;
	gchar *val;
	guint mask, maskiter;
	gint oldval, newval;
	
	for (j=0; j<4; j++) {
		mask = 0;
		maskiter = 1 << 7;
		i = 0;
		while ((i < 8) && (8*j + i < mask_nb_bits)) {
			mask += maskiter;
			maskiter >>= 1;
			i++;
		}

		if (target_mask)
			oldval = atoi (gtk_entry_get_text (GTK_ENTRY (mgcidr->priv->mask[j])));
		else
			oldval = atoi (gtk_entry_get_text (GTK_ENTRY (mgcidr->priv->ip[j])));
		
		newval = oldval & mask;
		val = g_strdup_printf ("%d", newval);
		if (target_mask)
			gtk_entry_set_text (GTK_ENTRY (mgcidr->priv->mask[j]), val);
		else
			gtk_entry_set_text (GTK_ENTRY (mgcidr->priv->ip[j]), val);
		g_free (val);
	}
}

static gboolean
get_complete_value (MgEntryCidr *mgcidr, gboolean target_mask, gulong *result)
{
	gboolean error = FALSE;
	gulong retval = 0;
	gint i;

	for (i=0; i<4; i++) {
		const gchar *str;
		gint part;
		guint part2;

		if (target_mask)
			str = gtk_entry_get_text (GTK_ENTRY (mgcidr->priv->mask[i]));
		else
			str = gtk_entry_get_text (GTK_ENTRY (mgcidr->priv->ip[i]));
		part = atoi (str);
		if ((part < 0) || (part > 255))
			error = TRUE;
		else {
			part2 = part;
			retval += part2 << 8*(3-i);
		}
	}

	*result = retval;

	return !error;
}

static gint
get_ip_nb_bits (MgEntryCidr *mgcidr)
{
	gulong ipval;
	if (get_complete_value (mgcidr, FALSE, &ipval)) {
		/* bits counting */
		gboolean ipend = FALSE;
		gint i, ip;
		gulong ipiter;

		ip = 32;
		i = 0;
		ipiter = 1;
		while (!ipend && (i<=31)) {
			if (ipval & ipiter)
				ipend = TRUE;
			else
				ip--;
			ipiter <<= 1;
			i++;
		}

		return ip;
	}
	else
		return -1;
}

/* returns -1 if error */
static gint
get_mask_nb_bits (MgEntryCidr *mgcidr)
{
	gulong maskval;
	if (get_complete_value (mgcidr, TRUE, &maskval)) {
		/* bits counting */
		gboolean maskend = FALSE;
		gint i, mask;
		gulong maskiter;
		gboolean error = FALSE;

		mask = 0;
		i = 31;
		maskiter = 1 << i;
		while (!error && (i>=0)) {
			if (maskval & maskiter) {
				mask ++;
				if (maskend)
					error = TRUE;
			}
			else
				maskend = TRUE;
			maskiter >>= 1;
			i--;
		}

		if (error) 
			return -1;
		else
			return mask;
	}
	else 
		return -1;
}

static GdaValue *
real_get_value (MgEntryWrapper *mgwrap)
{
	GdaValue *value = NULL;
	MgEntryCidr *mgcidr;
	gint i;
	gboolean error = FALSE;
	gint iplen, masklen;

	g_return_val_if_fail (mgwrap && IS_MG_ENTRY_CIDR (mgwrap), NULL);
	mgcidr = MG_ENTRY_CIDR (mgwrap);
	g_return_val_if_fail (mgcidr->priv, NULL);

	iplen = get_ip_nb_bits (mgcidr);
	masklen = get_mask_nb_bits (mgcidr);
	if (iplen > masklen)
		error = TRUE;
	else {
		GString *string;
		string = g_string_new ("");
		/* ip part */
		for (i=0; i<4; i++) {
			const gchar *str;
			gint ippart;
			
			if (i)
				g_string_append_c (string, '.');
			str = gtk_entry_get_text (GTK_ENTRY (mgcidr->priv->ip[i]));
			if (! *str)
				str = "0";
			ippart = atoi (str);
			if ((ippart < 0) || (ippart > 255))
				error = TRUE;
			g_string_append (string, str);
		}
		
		/* mask part */
		if (masklen < 0)
			error = TRUE;

		if (!error) {
			g_string_append_printf (string, "/%d", masklen);
			value = gda_value_new_string (string->str);
		}
		g_string_free (string, TRUE);
	}

	if (!value) {
		/* in case the mg_data_handler_get_value_from_sql() returned an error because
		   the contents of the GtkEntry cannot be interpreted as a GdaValue */
		value = gda_value_new_null ();
	}

	return value;
}

static void
connect_signals(MgEntryWrapper *mgwrap, GCallback callback)
{
	MgEntryCidr *mgcidr;
	gint i;

	g_return_if_fail (mgwrap && IS_MG_ENTRY_CIDR (mgwrap));
	mgcidr = MG_ENTRY_CIDR (mgwrap);
	g_return_if_fail (mgcidr->priv);

	for (i=0; i<4; i++) {
		g_signal_connect (G_OBJECT (mgcidr->priv->ip[i]), "changed", callback, mgwrap);
		g_signal_connect (G_OBJECT (mgcidr->priv->mask[i]), "changed", callback, mgwrap);
	}
}

static gboolean
expand_in_layout (MgEntryWrapper *mgwrap)
{
	return FALSE;
}
