/* mg-handler-text.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __MG_HANDLER_TEXT__
#define __MG_HANDLER_TEXT__

#include <libmergeant/mg-base.h>
#include <libmergeant/mg-server.h>
#include <libmergeant/mg-data-handler.h>

G_BEGIN_DECLS

#define MG_HANDLER_TEXT_TYPE          (mg_handler_text_get_type())
#define MG_HANDLER_TEXT(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_handler_text_get_type(), MgHandlerText)
#define MG_HANDLER_TEXT_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_handler_text_get_type (), MgHandlerTextClass)
#define IS_MG_HANDLER_TEXT(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_handler_text_get_type ())


typedef struct _MgHandlerText      MgHandlerText;
typedef struct _MgHandlerTextClass MgHandlerTextClass;
typedef struct _MgHandlerTextPriv  MgHandlerTextPriv;


/* struct for the object's data */
struct _MgHandlerText
{
	MgBase                object;

	MgHandlerTextPriv  *priv;
};

/* struct for the object's class */
struct _MgHandlerTextClass
{
	MgBaseClass            parent_class;
};


guint           mg_handler_text_get_type      (void);
GObject        *mg_handler_text_new           (MgServer *srv);

G_END_DECLS

#endif
