/* mg-handler-none.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-handler-none.h"
#include "mg-entry-none.h"
#include <libmergeant/mg-server.h>

static void mg_handler_none_class_init (MgHandlerNoneClass * class);
static void mg_handler_none_init (MgHandlerNone * wid);
static void mg_handler_none_dispose (GObject   * object);


/* MgDataHandler interface */
static void         mg_handler_none_data_handler_init      (MgDataHandlerIface *iface);
static MgDataEntry *mg_handler_none_get_entry_from_value   (MgDataHandler *dh, const GdaValue *value, 
							    GdaValueType type);
static gchar       *mg_handler_none_get_sql_from_value     (MgDataHandler *dh, const GdaValue *value);
static gchar       *mg_handler_none_get_str_from_value     (MgDataHandler *dh, const GdaValue *value);
static GdaValue    *mg_handler_none_get_value_from_sql     (MgDataHandler *dh, const gchar *sql, 
							    GdaValueType type);
static GdaValue    *mg_handler_none_get_sane_init_value    (MgDataHandler * dh, GdaValueType type);

static guint        mg_handler_none_get_nb_gda_types       (MgDataHandler *dh);
static GdaValueType mg_handler_none_get_gda_type_index     (MgDataHandler *dh, guint index);
static gboolean     mg_handler_none_accepts_gda_type       (MgDataHandler * dh, GdaValueType type);

static const gchar *mg_handler_none_get_descr              (MgDataHandler *dh);
static const gchar *mg_handler_none_get_descr_detail       (MgDataHandler *dh);
static const gchar *mg_handler_none_get_version            (MgDataHandler *dh);
static gboolean     mg_handler_none_is_plugin              (MgDataHandler *dh);
static const gchar *mg_handler_none_get_plugin_name        (MgDataHandler *dh);
static const gchar *mg_handler_none_get_plugin_file        (MgDataHandler *dh);
static gchar       *mg_handler_none_get_key                (MgDataHandler *dh);


/* signals */
enum
{
	LAST_SIGNAL
};

static gint mg_handler_none_signals[LAST_SIGNAL] = { };

struct  _MgHandlerNonePriv {
	gchar          *detailled_descr;
	MgServer       *srv;
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

guint
mg_handler_none_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgHandlerNoneClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_handler_none_class_init,
			NULL,
			NULL,
			sizeof (MgHandlerNone),
			0,
			(GInstanceInitFunc) mg_handler_none_init
		};		

		static const GInterfaceInfo data_entry_info = {
			(GInterfaceInitFunc) mg_handler_none_data_handler_init,
			NULL,
			NULL
		};

		type = g_type_register_static (MG_BASE_TYPE, "MgHandlerNone", &info, 0);
		g_type_add_interface_static (type, MG_DATA_HANDLER_TYPE, &data_entry_info);
	}
	return type;
}

static void
mg_handler_none_data_handler_init (MgDataHandlerIface *iface)
{
	iface->get_entry_from_value = mg_handler_none_get_entry_from_value;
	iface->get_sql_from_value = mg_handler_none_get_sql_from_value;
	iface->get_str_from_value = mg_handler_none_get_str_from_value;
	iface->get_value_from_sql = mg_handler_none_get_value_from_sql;
	iface->get_value_from_str = NULL;
	iface->get_sane_init_value = mg_handler_none_get_sane_init_value;
	iface->get_nb_gda_types = mg_handler_none_get_nb_gda_types;
	iface->accepts_gda_type = mg_handler_none_accepts_gda_type;
	iface->get_gda_type_index = mg_handler_none_get_gda_type_index;
	iface->get_descr = mg_handler_none_get_descr;
	iface->get_descr_detail = mg_handler_none_get_descr_detail;
	iface->get_version = mg_handler_none_get_version;
	iface->is_plugin = mg_handler_none_is_plugin;
	iface->get_plugin_name = mg_handler_none_get_plugin_name;
	iface->get_plugin_file = mg_handler_none_get_plugin_file;
	iface->get_key = mg_handler_none_get_key;
}


static void
mg_handler_none_class_init (MgHandlerNoneClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_handler_none_dispose;
}

static void
mg_handler_none_init (MgHandlerNone * hdl)
{
	/* Private structure */
	hdl->priv = g_new0 (MgHandlerNonePriv, 1);
	hdl->priv->detailled_descr = _("Default no-action handler");
	hdl->priv->srv = NULL;

	mg_base_set_name (MG_BASE (hdl), _("InternalNone"));
	mg_base_set_description (MG_BASE (hdl), _("Default representation for unhandled GDA data types"));
}

static void
mg_handler_none_dispose (GObject *object)
{
	MgHandlerNone *hdl;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_HANDLER_NONE (object));

	hdl = MG_HANDLER_NONE (object);

	if (hdl->priv) {
		mg_base_nullify_check (MG_BASE (object));

		if (hdl->priv->srv)
			g_object_remove_weak_pointer (G_OBJECT (hdl->priv->srv),
						      (gpointer *) & (hdl->priv->srv));

		g_free (hdl->priv);
		hdl->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

/**
 * mg_handler_none_new
 * @srv: a #MgServer object
 *
 * Creates a data handler for handling unhandled (bu other) gda data types.
 *
 * Returns: the new object
 */
GObject *
mg_handler_none_new (MgServer *srv)
{
	GObject *obj;
	MgHandlerNone *hdl;

	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	obj = g_object_new (MG_HANDLER_NONE_TYPE, NULL);
	hdl = MG_HANDLER_NONE (obj);

	g_object_add_weak_pointer (G_OBJECT (srv), (gpointer *) &(hdl->priv->srv));
	hdl->priv->srv = srv;

	return obj;
}


/* Interface implementation */
static MgDataEntry *
mg_handler_none_get_entry_from_value (MgDataHandler *iface, const GdaValue *value, GdaValueType type)
{
	MgHandlerNone *hdl;
	MgDataEntry *de;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), NULL);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, NULL);

	/* no need to set a value since it only displays a textual message */
	de = MG_DATA_ENTRY (mg_entry_none_new (iface, type));

	return de;
}

static gchar *
mg_handler_none_get_sql_from_value (MgDataHandler *iface, const GdaValue *value)
{
	MgHandlerNone *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), NULL);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, NULL);

	return NULL;
}

static gchar *
mg_handler_none_get_str_from_value (MgDataHandler *iface, const GdaValue *value)
{
	MgHandlerNone *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), NULL);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, NULL);

	return NULL;
}

static GdaValue *
mg_handler_none_get_value_from_sql (MgDataHandler *iface, const gchar *sql, GdaValueType type)
{
	MgHandlerNone *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), NULL);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, NULL);

	return gda_value_new_null ();
}

static GdaValue *
mg_handler_none_get_sane_init_value (MgDataHandler *iface, GdaValueType type)
{
	MgHandlerNone *hdl;
	GdaValue *value;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), NULL);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, NULL);

	value = gda_value_new_null ();
	return value;
}

static guint
mg_handler_none_get_nb_gda_types (MgDataHandler *iface)
{
	MgHandlerNone *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), 0);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, 0);

	return GDA_VALUE_TYPE_UNKNOWN;
}


static gboolean
mg_handler_none_accepts_gda_type (MgDataHandler *iface, GdaValueType type)
{
	MgHandlerNone *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), FALSE);
	g_return_val_if_fail (type != GDA_VALUE_TYPE_UNKNOWN, FALSE);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, 0);

	return TRUE;
}

static GdaValueType
mg_handler_none_get_gda_type_index (MgDataHandler *iface, guint index)
{
	MgHandlerNone *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), GDA_VALUE_TYPE_UNKNOWN);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, GDA_VALUE_TYPE_UNKNOWN);

	return index;
}

static const gchar *
mg_handler_none_get_descr (MgDataHandler *iface)
{
	MgHandlerNone *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), NULL);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, NULL);

	return mg_base_get_description (MG_BASE (hdl));
}

static const gchar *
mg_handler_none_get_descr_detail (MgDataHandler *iface)
{
	MgHandlerNone *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), NULL);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, NULL);

	return hdl->priv->detailled_descr;
}

static const gchar *
mg_handler_none_get_version (MgDataHandler *iface)
{
	MgHandlerNone *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), NULL);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, NULL);
	
	return g_strdup ("Internal");
}

static gboolean
mg_handler_none_is_plugin (MgDataHandler *iface)
{
	MgHandlerNone *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), FALSE);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, FALSE);

	return FALSE;
}

static const gchar *
mg_handler_none_get_plugin_name (MgDataHandler *iface)
{
	MgHandlerNone *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), NULL);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, NULL);
	
	return NULL;
}

static const gchar *
mg_handler_none_get_plugin_file (MgDataHandler *iface)
{
	MgHandlerNone *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), NULL);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, NULL);
	
	return NULL;
}

static gchar *
mg_handler_none_get_key (MgDataHandler *iface)
{
	MgHandlerNone *hdl;

	g_return_val_if_fail (iface && IS_MG_HANDLER_NONE (iface), NULL);
	hdl = MG_HANDLER_NONE (iface);
	g_return_val_if_fail (hdl->priv, NULL);
	
	return g_strdup (mg_base_get_name (MG_BASE (hdl)));
}

