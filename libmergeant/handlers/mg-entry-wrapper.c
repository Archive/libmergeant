/* mg-entry-wrapper.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-entry-wrapper.h"
#include <libmergeant/mg-data-handler.h>

static void mg_entry_wrapper_class_init (MgEntryWrapperClass *class);
static void mg_entry_wrapper_init (MgEntryWrapper *wid);
static void mg_entry_wrapper_dispose (GObject *object);

static void mg_entry_wrapper_set_property (GObject              *object,
					   guint                 param_id,
					   const GValue         *value,
					   GParamSpec           *pspec);
static void mg_entry_wrapper_get_property (GObject              *object,
					   guint                 param_id,
					   GValue               *value,
					   GParamSpec           *pspec);

static void contents_changed_cb (GtkWidget *entry, MgEntryWrapper *mgwrap);
static void check_correct_init (MgEntryWrapper *wid);
static void block_signals (MgEntryWrapper *wid);
static void unblock_signals (MgEntryWrapper *wid);

/* MgDataEntry interface */
static void            mg_entry_wrapper_data_entry_init   (MgDataEntryIface *iface);
static void            mg_entry_wrapper_set_value_type    (MgDataEntry *de, GdaValueType type);
static GdaValueType    mg_entry_wrapper_get_value_type    (MgDataEntry *de);
static void            mg_entry_wrapper_set_value         (MgDataEntry *de, const GdaValue *value);
static GdaValue       *mg_entry_wrapper_get_value         (MgDataEntry *de);
static void            mg_entry_wrapper_set_value_orig    (MgDataEntry *de, const GdaValue *value);
static const GdaValue *mg_entry_wrapper_get_value_orig    (MgDataEntry *de);
static void            mg_entry_wrapper_set_value_default (MgDataEntry *de, const GdaValue *value);
static void            mg_entry_wrapper_set_attributes    (MgDataEntry *de, guint attrs, guint mask);
static guint           mg_entry_wrapper_get_attributes    (MgDataEntry *de);
static MgDataHandler  *mg_entry_wrapper_get_handler       (MgDataEntry *de);
static gboolean        mg_entry_wrapper_expand_in_layout  (MgDataEntry *de);


/* properties */
enum
{
        PROP_0,
        PROP_SET_DEFAULT_IF_INVALID
};

/* signals */
enum
{
	LAST_SIGNAL
};

static gint mg_entry_wrapper_signals[LAST_SIGNAL] = { };


struct  _MgEntryWrapperPriv {
	gboolean            impl_is_correct;
        GtkWidget          *entry;
	MgEntryWrapperClass *real_class;
	guint               signals_blocked;

	GdaValueType        type;
	GdaValue           *value_orig;
	GdaValue           *value_default; /* Can be of any type, not just 'type' */

	gboolean            null_forced;
	gboolean            default_forced;

	gboolean            null_possible;
	gboolean            default_possible;
	gboolean            show_actions;

	/* property */
	gboolean            set_default_if_invalid;
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

guint
mg_entry_wrapper_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgEntryWrapperClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_entry_wrapper_class_init,
			NULL,
			NULL,
			sizeof (MgEntryWrapper),
			0,
			(GInstanceInitFunc) mg_entry_wrapper_init
		};		

		static const GInterfaceInfo data_entry_info = {
			(GInterfaceInitFunc) mg_entry_wrapper_data_entry_init,
			NULL,
			NULL
		};

		type = g_type_register_static (MG_ENTRY_SHELL_TYPE, "MgEntryWrapper", &info, 0);
		g_type_add_interface_static (type, MG_DATA_ENTRY_TYPE, &data_entry_info);
	}
	return type;
}

static void
mg_entry_wrapper_data_entry_init (MgDataEntryIface *iface)
{
	iface->set_value_type = mg_entry_wrapper_set_value_type;
	iface->get_value_type = mg_entry_wrapper_get_value_type;
	iface->set_value = mg_entry_wrapper_set_value;
	iface->get_value = mg_entry_wrapper_get_value;
	iface->set_value_orig = mg_entry_wrapper_set_value_orig;
	iface->get_value_orig = mg_entry_wrapper_get_value_orig;
	iface->set_value_default = mg_entry_wrapper_set_value_default;
	iface->set_attributes = mg_entry_wrapper_set_attributes;
	iface->get_attributes = mg_entry_wrapper_get_attributes;
	iface->get_handler = mg_entry_wrapper_get_handler;
	iface->expand_in_layout = mg_entry_wrapper_expand_in_layout;
}


static void
mg_entry_wrapper_class_init (MgEntryWrapperClass *class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	/* virtual functions */
	class->create_entry = NULL;
	class->real_set_value = NULL;
	class->real_get_value = NULL;

	/* Properties */
        object_class->set_property = mg_entry_wrapper_set_property;
        object_class->get_property = mg_entry_wrapper_get_property;
        g_object_class_install_property (object_class, PROP_SET_DEFAULT_IF_INVALID,
					 g_param_spec_boolean ("set_default_if_invalid", NULL, NULL, FALSE,
                                                               (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	object_class->dispose = mg_entry_wrapper_dispose;
}

static void
check_correct_init (MgEntryWrapper *mgwrap)
{
	if (!mgwrap->priv->impl_is_correct) {
		GtkWidget *entry = NULL;
		MgEntryWrapperClass *class;
		gboolean class_impl_error = FALSE;;
		
		class = MG_ENTRY_WRAPPER_CLASS (G_OBJECT_GET_CLASS (mgwrap));
		if (! class->create_entry) {
			g_warning ("create_entry () virtual function not implemented for object class %s\n",
				   G_OBJECT_TYPE_NAME (mgwrap));
			class_impl_error = TRUE;
		}
		if (! class->real_set_value) {
			g_warning ("real_set_value () virtual function not implemented for object class %s\n",
				   G_OBJECT_TYPE_NAME (mgwrap));
			class_impl_error = TRUE;
		}
		if (! class->real_get_value) {
			g_warning ("real_get_value () virtual function not implemented for object class %s\n",
				   G_OBJECT_TYPE_NAME (mgwrap));
			class_impl_error = TRUE;
		}
		if (! class->connect_signals) {
			g_warning ("connect_signals () virtual function not implemented for object class %s\n",
				   G_OBJECT_TYPE_NAME (mgwrap));
			class_impl_error = TRUE;
		}
		if (! class->expand_in_layout) {
			g_warning ("expand_in_layout () virtual function not implemented for object class %s\n",
				   G_OBJECT_TYPE_NAME (mgwrap));
			class_impl_error = TRUE;
		}

		if (!class_impl_error) {
			mgwrap->priv->real_class = class;
			mgwrap->priv->impl_is_correct = TRUE;
			entry = (*mgwrap->priv->real_class->create_entry) (mgwrap);
			
			mg_entry_shell_pack_entry (MG_ENTRY_SHELL (mgwrap), entry);
			gtk_widget_show (entry);
			mgwrap->priv->entry = entry;
			
			(*mgwrap->priv->real_class->connect_signals) (mgwrap, G_CALLBACK (contents_changed_cb));
		}
		else {
			/* we need to exit because the program WILL BE unstable and WILL crash */
			g_assert_not_reached ();
		}	
	}
}

static void
block_signals (MgEntryWrapper *mgwrap)
{
	mgwrap->priv->signals_blocked ++;
}

static void
unblock_signals (MgEntryWrapper *mgwrap)
{
	mgwrap->priv->signals_blocked --;
}


static void
mg_entry_wrapper_init (MgEntryWrapper *mgwrap)
{
	/* Private structure */
	mgwrap->priv = g_new0 (MgEntryWrapperPriv, 1);
	mgwrap->priv->impl_is_correct = FALSE;
	mgwrap->priv->entry = NULL;
	mgwrap->priv->real_class = NULL;
	mgwrap->priv->signals_blocked = 0;

	mgwrap->priv->type = GDA_VALUE_TYPE_UNKNOWN;
	mgwrap->priv->value_orig = NULL;
	mgwrap->priv->value_default = NULL;

	mgwrap->priv->null_forced = FALSE;
	mgwrap->priv->default_forced = FALSE;

	mgwrap->priv->null_possible = TRUE;
	mgwrap->priv->default_possible = FALSE;
	mgwrap->priv->show_actions = TRUE;

	mgwrap->priv->set_default_if_invalid = FALSE;
}

static void
mg_entry_wrapper_dispose (GObject   *object)
{
	MgEntryWrapper *mgwrap;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_WRAPPER (object));

	mgwrap = MG_ENTRY_WRAPPER (object);

	if (mgwrap->priv) {
		if (mgwrap->priv->value_orig)
			gda_value_free (mgwrap->priv->value_orig);
		if (mgwrap->priv->value_default)
			gda_value_free (mgwrap->priv->value_default);

		g_free (mgwrap->priv);
		mgwrap->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}


static void 
mg_entry_wrapper_set_property (GObject              *object,
			    guint                 param_id,
			    const GValue         *value,
			    GParamSpec           *pspec)
{
	MgEntryWrapper *mgwrap = MG_ENTRY_WRAPPER (object);
	if (mgwrap->priv) {
		switch (param_id) {
		case PROP_SET_DEFAULT_IF_INVALID:
			if (mgwrap->priv->set_default_if_invalid != g_value_get_boolean (value)) {
				guint attrs;

				mgwrap->priv->set_default_if_invalid = g_value_get_boolean (value);
				attrs = mg_data_entry_get_attributes (MG_DATA_ENTRY (mgwrap));

				if (mgwrap->priv->set_default_if_invalid && (attrs & MG_DATA_ENTRY_DATA_NON_VALID)) {
					GdaValue *sane_value;
					MgDataHandler *dh;
					GdaValueType type;

					check_correct_init (mgwrap);
					dh = mg_data_entry_get_handler (MG_DATA_ENTRY (mgwrap));
					type = mg_data_entry_get_value_type (MG_DATA_ENTRY (mgwrap));
					sane_value = mg_data_handler_get_sane_init_value (dh, type);
					(*mgwrap->priv->real_class->real_set_value) (mgwrap, sane_value);
					gda_value_free (sane_value);
				}
			}
			break;
		}
	}
}

static void
mg_entry_wrapper_get_property (GObject              *object,
			     guint                 param_id,
			     GValue               *value,
			     GParamSpec           *pspec)
{
	MgEntryWrapper *mgwrap = MG_ENTRY_WRAPPER (object);
	if (mgwrap->priv) {
		switch (param_id) {
		case PROP_SET_DEFAULT_IF_INVALID:
			g_value_set_boolean (value, mgwrap->priv->set_default_if_invalid);
			break;
		}
	}
}


static void mg_entry_wrapper_emit_signal (MgEntryWrapper *mgwrap);
static void
contents_changed_cb (GtkWidget *entry, MgEntryWrapper *mgwrap)
{
	if (! mgwrap->priv->signals_blocked) {
		mgwrap->priv->null_forced = FALSE;
		mgwrap->priv->default_forced = FALSE;
		mg_entry_wrapper_emit_signal (mgwrap);
	}
}

static void
mg_entry_wrapper_emit_signal (MgEntryWrapper *mgwrap)
{
	if (! mgwrap->priv->signals_blocked) {
#ifdef debug_signal
		g_print (">> 'CONTENTS_MODIFIED' from %s\n", __FUNCTION__);
#endif
		g_signal_emit_by_name (G_OBJECT (mgwrap), "contents_modified");
#ifdef debug_signal
		g_print ("<< 'CONTENTS_MODIFIED' from %s\n", __FUNCTION__);
#endif
	}
}



/* Interface implementation */
static void
mg_entry_wrapper_set_value_type (MgDataEntry *iface, GdaValueType type)
{
	MgEntryWrapper *mgwrap;

	g_return_if_fail (iface && IS_MG_ENTRY_WRAPPER (iface));
	mgwrap = MG_ENTRY_WRAPPER (iface);
	g_return_if_fail (mgwrap->priv);
	
	if (mgwrap->priv->type != type) {
		GdaValue *value;
		MgDataHandler *dh;

		if (mgwrap->priv->value_orig) {
			gda_value_free (mgwrap->priv->value_orig);
			mgwrap->priv->value_orig = NULL;
		}
		if (mgwrap->priv->value_default) {
			gda_value_free (mgwrap->priv->value_default);
			mgwrap->priv->value_default = NULL;
		}

		mgwrap->priv->type = type;
		mgwrap->priv->value_default = gda_value_new_null ();

		/* Set original value */
		dh = mg_data_entry_get_handler (MG_DATA_ENTRY (mgwrap));
		value = gda_value_new_null ();
		mg_entry_wrapper_set_value_orig (MG_DATA_ENTRY (mgwrap), value);
 		gda_value_free (value);
	}
}

static GdaValueType
mg_entry_wrapper_get_value_type (MgDataEntry *iface)
{
	MgEntryWrapper *mgwrap;

	g_return_val_if_fail (iface && IS_MG_ENTRY_WRAPPER (iface), GDA_VALUE_TYPE_UNKNOWN);
	mgwrap = MG_ENTRY_WRAPPER (iface);
	g_return_val_if_fail (mgwrap->priv, GDA_VALUE_TYPE_UNKNOWN);

	return mgwrap->priv->type;
}


static void
mg_entry_wrapper_set_value (MgDataEntry *iface, const GdaValue *value)
{
	MgEntryWrapper *mgwrap;

	g_return_if_fail (iface && IS_MG_ENTRY_WRAPPER (iface));
	mgwrap = MG_ENTRY_WRAPPER (iface);
	g_return_if_fail (mgwrap->priv);
	check_correct_init (mgwrap);
	
	block_signals (mgwrap);
	if (value) {
		g_return_if_fail ((gda_value_get_type (value) == mgwrap->priv->type) || 
				  (gda_value_get_type (value) == GDA_VALUE_TYPE_NULL));
		(*mgwrap->priv->real_class->real_set_value) (mgwrap, value);
		if (gda_value_is_null (value))
			mgwrap->priv->null_forced = TRUE;
		else 
			mgwrap->priv->null_forced = FALSE;
	}
	else {
		(*mgwrap->priv->real_class->real_set_value) (mgwrap, NULL);
		mgwrap->priv->null_forced = TRUE;
	}
	unblock_signals (mgwrap);
	mgwrap->priv->default_forced = FALSE;

	mg_entry_wrapper_emit_signal (mgwrap);
}

static GdaValue *
mg_entry_wrapper_get_value (MgDataEntry *iface)
{
	GdaValue *value = NULL;
	MgEntryWrapper *mgwrap;
	MgDataHandler *dh;

	g_return_val_if_fail (iface && IS_MG_ENTRY_WRAPPER (iface), NULL);
	mgwrap = MG_ENTRY_WRAPPER (iface);
	g_return_val_if_fail (mgwrap->priv, NULL);

	dh = mg_entry_wrapper_get_handler (MG_DATA_ENTRY (mgwrap));

	if (mgwrap->priv->null_forced)
		value = gda_value_new_null ();
	else {
		if (mgwrap->priv->default_forced) {
			if (gda_value_get_type (mgwrap->priv->value_default) == mgwrap->priv->type)
				value = gda_value_copy (mgwrap->priv->value_default);
			else
				value = gda_value_new_null ();
		}
		else {
			check_correct_init (mgwrap);
			value = (*mgwrap->priv->real_class->real_get_value) (mgwrap);
		}
	}

	return value;
}

static void
mg_entry_wrapper_set_value_orig (MgDataEntry *iface, const GdaValue *value)
{
	MgEntryWrapper *mgwrap;

	g_return_if_fail (iface && IS_MG_ENTRY_WRAPPER (iface));
	mgwrap = MG_ENTRY_WRAPPER (iface);
	g_return_if_fail (mgwrap->priv);
	check_correct_init (mgwrap);

	block_signals (mgwrap);
	mg_entry_wrapper_set_value (iface, value);
	unblock_signals (mgwrap);

	if (mgwrap->priv->value_orig) { 
		gda_value_free (mgwrap->priv->value_orig);
		mgwrap->priv->value_orig = NULL;
	}

	if (value) {
		g_return_if_fail ((gda_value_get_type (value) == mgwrap->priv->type) || 
				  (gda_value_get_type (value) == GDA_VALUE_TYPE_NULL));
		mgwrap->priv->value_orig = gda_value_copy (value);
	}
	else
		mgwrap->priv->value_orig = gda_value_new_null ();
	mg_entry_wrapper_emit_signal (mgwrap);
}

static const GdaValue *
mg_entry_wrapper_get_value_orig (MgDataEntry *iface)
{
	g_return_val_if_fail (iface && IS_MG_ENTRY_WRAPPER (iface), NULL);
	g_return_val_if_fail (MG_ENTRY_WRAPPER (iface)->priv, NULL);

	return MG_ENTRY_WRAPPER (iface)->priv->value_orig;
}

static void
mg_entry_wrapper_set_value_default (MgDataEntry *iface, const GdaValue *value)
{
	MgEntryWrapper *mgwrap;

	g_return_if_fail (iface && IS_MG_ENTRY_WRAPPER (iface));
	mgwrap = MG_ENTRY_WRAPPER (iface);
	g_return_if_fail (mgwrap->priv);

	if (mgwrap->priv->value_default)
		gda_value_free (mgwrap->priv->value_default);

	if (value) 
		mgwrap->priv->value_default = gda_value_copy (value);
	else 
		mgwrap->priv->value_default = gda_value_new_null ();

	if (mgwrap->priv->default_forced) {
		if (gda_value_get_type (mgwrap->priv->value_default) == mgwrap->priv->type) {
			check_correct_init (mgwrap);
			block_signals (mgwrap);
			mg_entry_wrapper_set_value (iface, mgwrap->priv->value_default);
			unblock_signals (mgwrap);
			mgwrap->priv->default_forced = TRUE;
			mg_entry_wrapper_emit_signal (mgwrap);
		}
		else {
			check_correct_init (mgwrap);
			(*mgwrap->priv->real_class->real_set_value) (mgwrap, NULL);
			mg_entry_wrapper_emit_signal (mgwrap);
		}
	}
}

static void
mg_entry_wrapper_set_attributes (MgDataEntry *iface, guint attrs, guint mask)
{
	MgEntryWrapper *mgwrap;

	g_return_if_fail (iface && IS_MG_ENTRY_WRAPPER (iface));
	mgwrap = MG_ENTRY_WRAPPER (iface);
	g_return_if_fail (mgwrap->priv);
	check_correct_init (mgwrap);

	/* Setting to NULL */
	if (mask & MG_DATA_ENTRY_IS_NULL) {
		if ((mask & MG_DATA_ENTRY_CAN_BE_NULL) &&
		    !(attrs & MG_DATA_ENTRY_CAN_BE_NULL))
			g_return_if_reached ();
		if (attrs & MG_DATA_ENTRY_IS_NULL) {
			block_signals (mgwrap);
			mg_entry_wrapper_set_value (iface, NULL);
			unblock_signals (mgwrap);
			mgwrap->priv->null_forced = TRUE;
			
			/* if default is set, see if we can keep it that way */
			if (mgwrap->priv->default_forced) {
				if (gda_value_get_type (mgwrap->priv->value_default) != 
				    GDA_VALUE_TYPE_NULL)
					mgwrap->priv->default_forced = FALSE;
			}

			mg_entry_wrapper_emit_signal (mgwrap);
			return;
		}
		else {
			mgwrap->priv->null_forced = FALSE;
			mg_entry_wrapper_emit_signal (mgwrap);
		}
	}

	/* Can be NULL ? */
	if (mask & MG_DATA_ENTRY_CAN_BE_NULL)
		mgwrap->priv->null_possible = (attrs & MG_DATA_ENTRY_CAN_BE_NULL) ? TRUE : FALSE;

	/* Setting to DEFAULT */
	if (mask & MG_DATA_ENTRY_IS_DEFAULT) {
		if ((mask & MG_DATA_ENTRY_CAN_BE_DEFAULT) &&
		    !(attrs & MG_DATA_ENTRY_CAN_BE_DEFAULT))
			g_return_if_reached ();
		if (attrs & MG_DATA_ENTRY_IS_DEFAULT) {
			block_signals (mgwrap);
			if (mgwrap->priv->value_default) {
				if (gda_value_get_type (mgwrap->priv->value_default) == mgwrap->priv->type)
					mg_entry_wrapper_set_value (iface, mgwrap->priv->value_default);
				else 
					(*mgwrap->priv->real_class->real_set_value) (mgwrap, NULL);
			}
			else
				mg_entry_wrapper_set_value (iface, NULL);
			unblock_signals (mgwrap);

			/* if NULL is set, see if we can keep it that way */
			if (mgwrap->priv->null_forced) {
				if (gda_value_get_type (mgwrap->priv->value_default) != 
				    GDA_VALUE_TYPE_NULL)
					mgwrap->priv->null_forced = FALSE;
			}

			mgwrap->priv->default_forced = TRUE;
			mg_entry_wrapper_emit_signal (mgwrap);
			return;
		}
		else {
			mgwrap->priv->default_forced = FALSE;
			mg_entry_wrapper_emit_signal (mgwrap);
		}
	}

	/* Can be DEFAULT ? */
	if (mask & MG_DATA_ENTRY_CAN_BE_DEFAULT)
		mgwrap->priv->default_possible = (attrs & MG_DATA_ENTRY_CAN_BE_DEFAULT) ? TRUE : FALSE;
	
	/* Modified ? */
	if (mask & MG_DATA_ENTRY_IS_UNCHANGED) {
		if (attrs & MG_DATA_ENTRY_IS_UNCHANGED) {
			mgwrap->priv->default_forced = FALSE;
			block_signals (mgwrap);
			mg_entry_wrapper_set_value (iface, mgwrap->priv->value_orig);
			unblock_signals (mgwrap);
			mg_entry_wrapper_emit_signal (mgwrap);
		}
	}

	/* Actions buttons ? */
	if (mask & MG_DATA_ENTRY_ACTIONS_SHOWN) {
		GValue *gval;
		mgwrap->priv->show_actions = (attrs & MG_DATA_ENTRY_ACTIONS_SHOWN) ? TRUE : FALSE;
		
		gval = g_new0 (GValue, 1);
		g_value_init (gval, G_TYPE_BOOLEAN);
		g_value_set_boolean (gval, mgwrap->priv->show_actions);
		g_object_set_property (G_OBJECT (mgwrap), "actions", gval);
		g_free (gval);
	}

	/* Can't force data to be valid */
	if (mask & MG_DATA_ENTRY_DATA_NON_VALID) 
		g_assert_not_reached ();

	g_signal_emit_by_name (G_OBJECT (mgwrap), "status_changed");
}

static guint
mg_entry_wrapper_get_attributes (MgDataEntry *iface)
{
	guint retval = 0;
	MgEntryWrapper *mgwrap;
	GdaValue *value;

	g_return_val_if_fail (iface && IS_MG_ENTRY_WRAPPER (iface), 0);
	mgwrap = MG_ENTRY_WRAPPER (iface);
	g_return_val_if_fail (mgwrap->priv, 0);

	value = mg_entry_wrapper_get_value (iface);

	/* NULL? */
	if (gda_value_get_type (value) == GDA_VALUE_TYPE_NULL) {
		if (mgwrap->priv->default_forced) {
			if (mgwrap->priv->null_forced)
				retval = retval | MG_DATA_ENTRY_IS_NULL;
		}
		else
			retval = retval | MG_DATA_ENTRY_IS_NULL;
	}

	/* can be NULL? */
	if (mgwrap->priv->null_possible) 
		retval = retval | MG_DATA_ENTRY_CAN_BE_NULL;
	
	/* is default */
	if (mgwrap->priv->default_forced)
		retval = retval | MG_DATA_ENTRY_IS_DEFAULT;
	
	/* can be default? */
	if (mgwrap->priv->default_possible)
		retval = retval | MG_DATA_ENTRY_CAN_BE_DEFAULT;
	
	/* is unchanged */
	if (mgwrap->priv->value_orig && 
	    (gda_value_get_type (value) == gda_value_get_type (mgwrap->priv->value_orig))) {
		if (gda_value_is_null (value)) 
			retval = retval | MG_DATA_ENTRY_IS_UNCHANGED;
		else {
			if (! gda_value_compare (value, mgwrap->priv->value_orig))
				retval = retval | MG_DATA_ENTRY_IS_UNCHANGED;
		}
	}

	/* actions shown */
	if (mgwrap->priv->show_actions)
		retval = retval | MG_DATA_ENTRY_ACTIONS_SHOWN;

	/* data valid? */
	if (! (mgwrap->priv->default_forced && mgwrap->priv->default_possible)) {
		if ((gda_value_is_null (value) && !mgwrap->priv->null_forced) ||
		    (gda_value_is_null (value) && !mgwrap->priv->null_possible))
			retval = retval | MG_DATA_ENTRY_DATA_NON_VALID;
	}
	
	gda_value_free (value);

	return retval;
}


static MgDataHandler *
mg_entry_wrapper_get_handler (MgDataEntry *iface)
{
	GValue val = { 0, };
	MgDataHandler *dh;

	g_return_val_if_fail (iface && IS_MG_ENTRY_WRAPPER (iface), NULL);
	g_return_val_if_fail (MG_ENTRY_WRAPPER (iface)->priv, NULL);

	g_value_init (&val, G_TYPE_POINTER);
	g_object_get_property (G_OBJECT (iface), "handler", &val);
	dh = g_value_get_pointer (&val);
	g_value_unset (&val);

	return dh;
}

static gboolean
mg_entry_wrapper_expand_in_layout (MgDataEntry *iface)
{
	MgEntryWrapper *mgwrap;

	g_return_val_if_fail (iface && IS_MG_ENTRY_WRAPPER (iface), FALSE);
	mgwrap = MG_ENTRY_WRAPPER (iface);
	g_return_val_if_fail (mgwrap->priv, FALSE);
	check_correct_init (mgwrap);

	return (*mgwrap->priv->real_class->expand_in_layout) (mgwrap);
}
