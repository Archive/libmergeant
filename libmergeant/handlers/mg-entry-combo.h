/* mg-entry-combo.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __MG_ENTRY_COMBO__
#define __MG_ENTRY_COMBO__

#include <gtk/gtk.h>
#include "mg-entry-shell.h"
#include <libmergeant/mg-data-entry.h>
#include <libmergeant/mg-context.h>

G_BEGIN_DECLS

#define MG_ENTRY_COMBO_TYPE          (mg_entry_combo_get_type())
#define MG_ENTRY_COMBO(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_entry_combo_get_type(), MgEntryCombo)
#define MG_ENTRY_COMBO_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_entry_combo_get_type (), MgEntryComboClass)
#define IS_MG_ENTRY_COMBO(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_entry_combo_get_type ())


typedef struct _MgEntryCombo      MgEntryCombo;
typedef struct _MgEntryComboClass MgEntryComboClass;
typedef struct _MgEntryComboPriv  MgEntryComboPriv;


/* struct for the object's data */
struct _MgEntryCombo
{
	MgEntryShell        object;
	MgEntryComboPriv   *priv;
};

/* struct for the object's class */
struct _MgEntryComboClass
{
	MgEntryShellClass   parent_class;
};


guint           mg_entry_combo_get_type          (void);
GtkWidget      *mg_entry_combo_new               (MgConf *conf, MgContext*context, MgContextNode *node);

gboolean        mg_entry_combo_set_values        (MgEntryCombo *combo, GSList *values);
GSList         *mg_entry_combo_get_values        (MgEntryCombo *combo);
gboolean        mg_entry_combo_set_values_orig   (MgEntryCombo *combo, GSList *values);
GSList         *mg_entry_combo_get_values_orig   (MgEntryCombo *combo);
gboolean        mg_entry_combo_set_values_default(MgEntryCombo *combo, GSList *values);

G_END_DECLS

#endif
