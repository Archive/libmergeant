/* mg-entry-combo.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-entry-combo.h"
#include <libmergeant/mg-server.h>
#include <libmergeant/mg-data-handler.h>
#include <libmergeant/mg-query.h>
#include <libmergeant/mg-entity.h>
#include <libmergeant/mg-field.h>
#include <libmergeant/mg-qfield.h>
#include <libmergeant/mg-parameter.h>
#include <libmergeant/mg-renderer.h>
#include <libmergeant/mg-result-set.h>
#include <libgnomedb/libgnomedb.h>

#define USE_OLD_GTK_COMBO_WIDGET

static void mg_entry_combo_class_init (MgEntryComboClass *class);
static void mg_entry_combo_init (MgEntryCombo *wid);
static void mg_entry_combo_dispose (GObject   *object);

static void mg_entry_combo_set_property (GObject              *object,
					 guint                 param_id,
					 const GValue         *value,
					 GParamSpec           *pspec);
static void mg_entry_combo_get_property (GObject              *object,
					 guint                 param_id,
					 GValue               *value,
					 GParamSpec           *pspec);

static void          compute_combo_model       (MgEntryCombo *combo);
static void          display_combo_model       (MgEntryCombo *combo);
static void          choose_auto_default_value (MgEntryCombo *combo);
#ifdef USE_OLD_GTK_COMBO_WIDGET
static void          set_selection_combo_model (MgEntryCombo *combo, gint pos);
#endif

static void          mg_entry_combo_emit_signal (MgEntryCombo *combo);
static void          real_combo_block_signals (MgEntryCombo *wid);
static void          real_combo_unblock_signals (MgEntryCombo *wid);

/* MgDataEntry interface */
static void            mg_entry_combo_data_entry_init   (MgDataEntryIface *iface);
static void            mg_entry_combo_set_attributes    (MgDataEntry *de, guint attrs, guint mask);
static guint           mg_entry_combo_get_attributes    (MgDataEntry *de);
static gboolean        mg_entry_combo_expand_in_layout  (MgDataEntry *de);

static void            mg_conf_weak_notify (MgEntryCombo *combo, MgConf *conf);
static void            nullified_param_cb (MgParameter *param, MgEntryCombo *combo);
static void            nullified_query_cb (MgQuery *query, MgEntryCombo *combo);
static void            dependency_param_changed_cb (MgParameter *param, MgEntryCombo *combo);

/* signals */
enum
{
	LAST_SIGNAL
};

static gint mg_entry_combo_signals[LAST_SIGNAL] = { };

/* properties */
enum
{
        PROP_0,
        PROP_SET_DEFAULT_IF_INVALID
};

typedef struct {
	MgParameter  *param;
	GdaValue     *value;    /* we don't own the value, since it belongs to a GdaDataModel => don't free it */
	gint          position; /* pos. in the priv->resultset */
	GdaValue     *value_orig;
	GdaValue     *value_default;
} ComboNode;
#define COMBO_NODE(x) ((ComboNode *)x)

struct  _MgEntryComboPriv {
	MgConf             *conf;

	MgContext          *context;
	GSList             *nodes; /* ComboNode structures */
	MgQuery            *query;

	MgResultSet        *resultset;
	GdaDataModel       *data_model;
	gboolean            data_model_valid;
	
        GtkWidget          *combo_entry;

	gboolean            data_valid;
	gboolean            null_forced;
	gboolean            default_forced;
	gboolean            null_possible;
	gboolean            default_possible;

	gboolean            show_actions;
	gboolean            set_default_if_invalid; /* use first entry when provided value is not found ? */
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

guint
mg_entry_combo_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgEntryComboClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_entry_combo_class_init,
			NULL,
			NULL,
			sizeof (MgEntryCombo),
			0,
			(GInstanceInitFunc) mg_entry_combo_init
		};		

		static const GInterfaceInfo data_entry_info = {
			(GInterfaceInitFunc) mg_entry_combo_data_entry_init,
			NULL,
			NULL
		};

		type = g_type_register_static (MG_ENTRY_SHELL_TYPE, "MgEntryCombo", &info, 0);
		g_type_add_interface_static (type, MG_DATA_ENTRY_TYPE, &data_entry_info);
	}
	return type;
}

static void
mg_entry_combo_data_entry_init (MgDataEntryIface *iface)
{
	iface->set_value_type = NULL;
	iface->get_value_type = NULL;
	iface->set_value = NULL;
	iface->get_value = NULL;
	iface->set_value_orig = NULL;
	iface->get_value_orig = NULL;
	iface->set_value_default = NULL;
	iface->set_attributes = mg_entry_combo_set_attributes;
	iface->get_attributes = mg_entry_combo_get_attributes;
	iface->get_handler = NULL;
	iface->expand_in_layout = mg_entry_combo_expand_in_layout;
}


static void
mg_entry_combo_class_init (MgEntryComboClass *class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_entry_combo_dispose;

	/* Properties */
        object_class->set_property = mg_entry_combo_set_property;
        object_class->get_property = mg_entry_combo_get_property;
        g_object_class_install_property (object_class, PROP_SET_DEFAULT_IF_INVALID,
					 g_param_spec_boolean ("set_default_if_invalid", NULL, NULL, FALSE,
                                                               (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

#ifdef USE_OLD_GTK_COMBO_WIDGET
static void combo_contents_changed_cb (GtkWidget *entry, MgEntryCombo *combo);
#endif

static void
real_combo_block_signals (MgEntryCombo *wid)
{
#ifdef USE_OLD_GTK_COMBO_WIDGET
	g_signal_handlers_block_by_func (G_OBJECT (GTK_COMBO (wid->priv->combo_entry)->entry),
					 G_CALLBACK (combo_contents_changed_cb), wid);
#else
	TO_IMPLEMENT;
#endif
}

static void
real_combo_unblock_signals (MgEntryCombo *wid)
{
#ifdef USE_OLD_GTK_COMBO_WIDGET
	g_signal_handlers_unblock_by_func (G_OBJECT (GTK_COMBO (wid->priv->combo_entry)->entry),
					   G_CALLBACK (combo_contents_changed_cb), wid);
#else
	TO_IMPLEMENT;
#endif
}


static void
mg_entry_combo_emit_signal (MgEntryCombo *combo)
{
#ifdef debug_signal
	g_print (">> 'CONTENTS_MODIFIED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit_by_name (G_OBJECT (combo), "contents_modified");
#ifdef debug_signal
	g_print ("<< 'CONTENTS_MODIFIED' from %s\n", __FUNCTION__);
#endif
}

static void
mg_entry_combo_init (MgEntryCombo *combo)
{
	/* Private structure */
	combo->priv = g_new0 (MgEntryComboPriv, 1);
	combo->priv->conf = NULL;
	combo->priv->context = NULL;
	combo->priv->nodes = NULL;
	combo->priv->query = NULL;
	combo->priv->resultset = NULL;
	combo->priv->data_model = NULL;
	combo->priv->set_default_if_invalid = FALSE;
	combo->priv->data_model_valid = FALSE;
	combo->priv->combo_entry = NULL;
	combo->priv->data_valid = FALSE;
	combo->priv->null_forced = FALSE;
	combo->priv->default_forced = FALSE;
	combo->priv->null_possible = TRUE;
	combo->priv->default_possible = FALSE;
	combo->priv->show_actions = TRUE;
}


/**
 * mg_entry_combo_new
 * @conf: a #MgConf object
 * @context: a #MgContext object
 * @node: a #MgContextNode structure, part of @context
 *
 * Creates a new #MgEntryCombo widget. The widget is a combo box which displays a
 * selectable list of items (the items come from the result of the execution of the 'node->query'
 * SELECT query). Thus the widget allows the simultaneuos selection of one or more values
 * (one for each 'node->params') while proposing potentially "more readable" choices.
 * 
 * @node is not used afterwards.
 *
 * Returns: the new widget
 */
GtkWidget *
mg_entry_combo_new (MgConf *conf, MgContext *context, MgContextNode *node)
{
	GObject *obj;
	MgEntryCombo *combo;
	GSList *list, *values;
	MgQueryType qtype;
	GtkWidget *entry;
	GSList *depend;
	gboolean null_possible;

	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);
	g_return_val_if_fail (context && IS_MG_CONTEXT (context), NULL);
	g_return_val_if_fail (node, NULL);
	g_return_val_if_fail (g_slist_find (context->nodes, node), NULL);
	g_return_val_if_fail (node->query, NULL);
	qtype = mg_query_get_query_type (node->query);
	g_return_val_if_fail ((qtype == MG_QUERY_TYPE_SELECT) ||
			      (qtype == MG_QUERY_TYPE_UNION) ||
			      (qtype == MG_QUERY_TYPE_INTERSECT), NULL);

	obj = g_object_new (MG_ENTRY_COMBO_TYPE, NULL);
	combo = MG_ENTRY_COMBO (obj);

	/* conf */
	combo->priv->conf = conf;
	g_object_weak_ref (G_OBJECT (combo->priv->conf),
			   (GWeakNotify) mg_conf_weak_notify, combo);

	/* context */
	combo->priv->context = context;
	g_object_add_weak_pointer (G_OBJECT (combo->priv->context), (gpointer) &(combo->priv->context));

	/* query */
	combo->priv->query = node->query;
	g_object_ref (G_OBJECT (combo->priv->query));
	g_signal_connect (G_OBJECT (combo->priv->query), "nullified",
			  G_CALLBACK (nullified_query_cb), combo);

	/* parameters */
	list = node->params;
	while (list) {
		ComboNode *cnode = g_new0 (ComboNode, 1);
		cnode->param = MG_PARAMETER (list->data);
		cnode->position = mg_entity_get_field_index (MG_ENTITY (combo->priv->query), 
							     MG_FIELD (mg_parameter_get_in_field 
								       (MG_PARAMETER (list->data))));
		cnode->value_orig = NULL;
		cnode->value_default = NULL;
		combo->priv->nodes = g_slist_append (combo->priv->nodes, cnode);
		g_object_ref (G_OBJECT (list->data));
		g_signal_connect (G_OBJECT (list->data), "nullified",
				  G_CALLBACK (nullified_param_cb), combo);

		list = g_slist_next (list);
	}

	/* create the combo itself */
#ifdef USE_OLD_GTK_COMBO_WIDGET
	entry = gtk_combo_new ();
	mg_entry_shell_pack_entry (MG_ENTRY_SHELL (combo), entry);
	gtk_widget_show (entry);
	combo->priv->combo_entry = entry;
	g_signal_connect (G_OBJECT (GTK_COMBO (entry)->entry), "changed",
			  G_CALLBACK (combo_contents_changed_cb), combo);
#else
	TO_IMPLEMENT;
	entry = NULL;
#endif

	/* create the data model */
	compute_combo_model (combo);

	/* connect the parameters on which combo depends */
	list = combo->priv->nodes;
	while (list) {
		depend = mg_parameter_get_dependencies (COMBO_NODE (list->data)->param);
		while (depend) {
			g_signal_connect (G_OBJECT (depend->data), "changed",
					  G_CALLBACK (dependency_param_changed_cb), combo);
			depend = g_slist_next (depend);
		}
		list = g_slist_next (list);
	}

	/* find an intelligent original value: the ones provided by the parameters themselves */
	null_possible = TRUE;
	values = NULL;
	list = combo->priv->nodes;
	while (list) {
		const GdaValue *init = mg_parameter_get_value (COMBO_NODE (list->data)->param);
		values = g_slist_append (values, init);

		if (mg_parameter_get_not_null (COMBO_NODE (list->data)->param))
			null_possible = FALSE;
		
		list = g_slist_next (list);
	}
	combo->priv->null_possible = null_possible;
	display_combo_model (combo);
	mg_entry_combo_set_values (combo, values);
	g_slist_free (values);

	return GTK_WIDGET (obj);
}

static GdaDataModel *make_single_line_model (const gchar *line);

/*
 * Computes a new GdaDataModel model (eventualy re-runs 'combo->priv->query') if necessary.
 * Some special 1 line models are created in case of errors.
 */
static void
compute_combo_model (MgEntryCombo *combo)
{
	MgServer *srv;
	gboolean valid_data;
	GdaDataModel *data_model = NULL;

	srv = mg_conf_get_server (combo->priv->conf);
	if (combo->priv->data_model) {
		g_object_unref (combo->priv->data_model);
		combo->priv->data_model = NULL;
	}

	if (mg_server_conn_is_opened (srv)) {
		MgResultSet *rs = NULL;
		gchar *sql;
		GError *error = NULL;
		
		sql = mg_renderer_render_as_sql (MG_RENDERER (combo->priv->query), combo->priv->context, &error);
		if (!sql) {
			valid_data = FALSE;
			data_model = make_single_line_model (_("No value available"));
			if (error)
				g_error_free (error);
		}
		else {
			rs = mg_server_do_query (srv, sql, MG_SERVER_QUERY_SQL, &error);
			if (!rs) {
				valid_data = FALSE;
				data_model = make_single_line_model (error->message);
				g_error_free (error);
			}
			else {
				if (mg_resultset_get_nbtuples (rs) == 0) {
					valid_data = FALSE;
					data_model = make_single_line_model (_("No value available"));
				}
				else {
					valid_data = TRUE;
					combo->priv->resultset = rs;
					data_model = mg_resultset_get_data_model (rs);

					/* keep our own reference on the data model */
					g_object_ref (G_OBJECT (data_model));
				}
			}
		}
	}
	else {
		valid_data = FALSE;
		data_model = make_single_line_model (_("Connexion not opened"));
	}

	/* setting the new model */
	combo->priv->data_model = data_model;
	combo->priv->data_model_valid = valid_data;
}


/*
 * Creates a MgDataModel object with a single line and column
 * containing the provided message
 */
static GdaDataModel *
make_single_line_model (const gchar *line)
{
	GdaValue *value;
	GdaDataModel *data_model;
	GList *rowvalues;
	
	data_model = gda_data_model_array_new (1);
	value = gda_value_new_string (line);
	rowvalues = g_list_append (NULL, value);
	gda_data_model_append_row (GDA_DATA_MODEL (data_model), rowvalues);
	g_list_free (rowvalues);
	gda_value_free (value);

	return data_model;
}


/*
 * Displays the combo->priv->data_model model in the real combo widget, without emitting
 * a single signal.
 */
static void
display_combo_model (MgEntryCombo *combo)
{
#ifdef USE_OLD_GTK_COMBO_WIDGET
	GList *strings = NULL, *list;
	const GdaValue *value;
	gint ncols, nrows, i, j;
	GString *str;
	gchar *tmpstr;
	GtkWidget *combo_entry = combo->priv->combo_entry;
	gint *mask, masksize;
	GdaDataModel *model = combo->priv->data_model;
	
	/* 1st row empty if NULL is possible */
	if (combo->priv->null_possible) 
		strings = g_list_append (strings, g_strdup (""));

	/* mask making: we only want columns which are not parameter values and if not internal MgQField */
	ncols = gda_data_model_get_n_columns (model);
	if (combo->priv->data_model_valid && 
	    (ncols - g_slist_length (combo->priv->nodes) > 0)) {
		gint i, current = 0;
		
		masksize = ncols - g_slist_length (combo->priv->nodes);
		mask = g_new0 (gint, masksize);
		for (i=0; i<ncols ; i++) {
			GSList *list = combo->priv->nodes;
			gboolean found = FALSE;
			while (list && !found) {
				if (COMBO_NODE (list->data)->position == i)
					found = TRUE;
				else
					list = g_slist_next (list);
			}
			if (!found) {
				MgField *field = mg_entity_get_field_by_index (MG_ENTITY (combo->priv->query), i);

				if (!mg_qfield_is_internal (MG_QFIELD (field))) {
					mask[current] = i;
					current ++;
				}
			}
		}
		masksize = current;
	}
	else {
		masksize = 0;
		mask = NULL;
	}

	/* actual string rendering */
	nrows = gda_data_model_get_n_rows (model);
	for (i=0; i<nrows; i++) {
		str = g_string_new ("");
		if (mask) {
			for (j=0; j<masksize; j++) {
				value = gda_data_model_get_value_at (model, mask[j], i);
				tmpstr = gda_value_stringify (value);
				if (j>0)
					g_string_append (str, " / " );
				g_string_append (str, tmpstr);
				g_free (tmpstr);
			}
		}
		else {
			for (j=0; j<ncols; j++) {
				value = gda_data_model_get_value_at (model, j, i);
				tmpstr = gda_value_stringify (value);
				if (j>0)
					g_string_append (str, " / " );
				g_string_append (str, tmpstr);
				g_free (tmpstr);
			}
		}
		strings = g_list_append (strings, str->str);
		g_string_free (str, FALSE);
	}
	
	if (mask)
		g_free (mask);

	real_combo_block_signals (combo);
	gtk_combo_set_popdown_strings (GTK_COMBO (combo_entry), strings);	      
	gtk_combo_set_value_in_list (GTK_COMBO (combo_entry), TRUE, FALSE);
	real_combo_unblock_signals (combo);
	list = strings;
	while (list) {
		g_free (list->data);
		list = g_list_next (list);
	}
	g_list_free (strings);
#else
	/* TODO: use a widget which operates with something similar to GtkTreeIter where
	 * it is possible to add / removed the 1st empty line without making all over again the
	 * complete list of possible selectable items */
	TO_IMPLEMENT;
#endif
}


static void
choose_auto_default_value (MgEntryCombo *combo)
{
#ifdef USE_OLD_GTK_COMBO_WIDGET
	gint pos = 0;
	if (combo->priv->null_possible)
		pos ++;
	set_selection_combo_model (combo, pos);
	combo_contents_changed_cb (NULL, combo);
#else
	TO_IMPLEMENT;
#endif
}

static void
mg_conf_weak_notify (MgEntryCombo *combo, MgConf *conf)
{
	/* render non sensitive */
	gtk_widget_set_sensitive (GTK_WIDGET (combo), FALSE);

	/* Tell that we don't need to weak unref the MgConf */
	combo->priv->conf = NULL;
}

static void
nullified_param_cb (MgParameter *param, MgEntryCombo *combo)
{
	/* remove the associated ComboNode */
	GSList *list;
	ComboNode *node = NULL;

	list = combo->priv->nodes;
	while (list) {
		if (COMBO_NODE (list->data)->param == param)
			node = COMBO_NODE (list->data);
		list = g_slist_next (list);
	}

	g_assert (node);
	combo->priv->nodes = g_slist_remove (combo->priv->nodes, node);
	g_signal_handlers_disconnect_by_func (G_OBJECT (param),
					      G_CALLBACK (nullified_param_cb), combo);
	g_object_unref (G_OBJECT (param));
	if (node->value)
		node->value = NULL; /* don't free that value since we have not copied it */
	if (node->value_orig)
		gda_value_free (node->value_orig);
	if (node->value_default)
		gda_value_free (node->value_default);
	g_free (node);
}

static void
nullified_query_cb (MgQuery *query, MgEntryCombo *combo)
{
	g_assert (combo->priv->query == query);
	g_signal_handlers_disconnect_by_func (G_OBJECT (query),
					      G_CALLBACK (nullified_query_cb), combo);
	g_object_unref (G_OBJECT (query));
	combo->priv->query = NULL;
}

static void
dependency_param_changed_cb (MgParameter *param, MgEntryCombo *combo)
{
	GSList *values = NULL;

	if (combo->priv->data_valid) 
		values = mg_entry_combo_get_values (combo);

	compute_combo_model (combo);
	display_combo_model (combo);

	if (values) {
		GSList *list;

		mg_entry_combo_set_values (combo, values);
		list = values;
		while (list) {
			gda_value_free ((GdaValue *)list->data);
			list = g_slist_next (list);
		}
		g_slist_free (values);
	}

	if (!combo->priv->data_valid) 
		choose_auto_default_value (combo);
}

static void
mg_entry_combo_dispose (GObject *object)
{
	MgEntryCombo *combo;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_COMBO (object));

	combo = MG_ENTRY_COMBO (object);

	if (combo->priv) {
		while (combo->priv->nodes) {
			ComboNode *node = COMBO_NODE (combo->priv->nodes->data);
			g_assert (node->param);
			nullified_param_cb (node->param, combo);
		}

		if (combo->priv->query)
			nullified_query_cb (combo->priv->query, combo);

		if (combo->priv->resultset) {
			g_object_unref (G_OBJECT (combo->priv->resultset));
			combo->priv->resultset = NULL;
		}
		
		if (combo->priv->data_model) {
			g_object_unref (G_OBJECT (combo->priv->data_model));
			combo->priv->data_model = NULL;
		}
		

		/* Weak unref the MgConf is necessary */
		if (combo->priv->conf)
			g_object_weak_unref (G_OBJECT (combo->priv->conf),
					     (GWeakNotify) mg_conf_weak_notify, combo);

		g_free (combo->priv);
		combo->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

static void 
mg_entry_combo_set_property (GObject              *object,
			     guint                 param_id,
			     const GValue         *value,
			     GParamSpec           *pspec)
{
	MgEntryCombo *combo = MG_ENTRY_COMBO (object);
	if (combo->priv) {
		switch (param_id) {
		case PROP_SET_DEFAULT_IF_INVALID:
			if (combo->priv->set_default_if_invalid != g_value_get_boolean (value)) {
				guint attrs;

				combo->priv->set_default_if_invalid = g_value_get_boolean (value);
				attrs = mg_data_entry_get_attributes (MG_DATA_ENTRY (combo));

				if (combo->priv->set_default_if_invalid && (attrs & MG_DATA_ENTRY_DATA_NON_VALID)) {
					if (combo->priv->data_model_valid)
						choose_auto_default_value (combo);
				}
			}
			break;
		}
	}
}

static void
mg_entry_combo_get_property (GObject              *object,
			     guint                 param_id,
			     GValue               *value,
			     GParamSpec           *pspec)
{
	MgEntryCombo *combo = MG_ENTRY_COMBO (object);
	if (combo->priv) {
		switch (param_id) {
		case PROP_SET_DEFAULT_IF_INVALID:
			g_value_set_boolean (value, combo->priv->set_default_if_invalid);
			break;
		}
	}
}

#ifdef USE_OLD_GTK_COMBO_WIDGET
static void
combo_contents_changed_cb (GtkWidget *entry, MgEntryCombo *combo)
{
	gint selection;
	
	if (!GTK_LIST (GTK_COMBO (combo->priv->combo_entry)->list)->selection)
		return;

	selection = g_list_index (GTK_LIST (GTK_COMBO (combo->priv->combo_entry)->list)->children,
				  GTK_LIST (GTK_COMBO (combo->priv->combo_entry)->list)->selection->data);
	
	if ((selection == 0) && combo->priv->null_possible) /* Set to NULL? */
		mg_entry_combo_set_values (combo, NULL);
	else {
		if (combo->priv->null_possible)
			selection --; /* remove the "" entry count */

		if (combo->priv->data_model_valid) {
			combo->priv->null_forced = FALSE;
			combo->priv->default_forced = FALSE;
			combo->priv->data_valid = TRUE;
			
			/* updating the node's values */
			if (combo->priv->data_model && combo->priv->data_model_valid &&
			    GTK_LIST (GTK_COMBO (combo->priv->combo_entry)->list)->selection) {
				GSList *list;
				list = combo->priv->nodes;
				while (list) {
					ComboNode *node = COMBO_NODE (list->data);
					node->value = gda_data_model_get_value_at (combo->priv->data_model, 
										   node->position, selection);
					/* g_print ("%s(): Set Node value to %s\n", __FUNCTION__, */
/* 						 node->value ? gda_value_stringify (node->value) : "Null"); */
					list = g_slist_next (list);
				}
			}
			
			g_signal_emit_by_name (G_OBJECT (combo), "status_changed");
			mg_entry_combo_emit_signal (combo);
		}
	}
}


/* Forces the selection of an item in the items list, without emitting a single signal */
static void
set_selection_combo_model (MgEntryCombo *combo, gint pos)
{
	real_combo_block_signals (combo);
	gtk_list_select_item (GTK_LIST (GTK_COMBO (combo->priv->combo_entry)->list), pos);
	real_combo_unblock_signals (combo);
}

#endif

/**
 * mg_entry_combo_set_values
 * @combo: a #MgEntryCombo widet
 * @values: a list of #GdaValue values, or %NULL
 *
 * Sets the values of @combo to the specified ones. None of the
 * values provided in the list is modified.
 *
 * An error can occur when there is no corresponding value(s) to be displayed
 * for the provided values.
 *
 * If @values is %NULL, then the entry itself is set to NULL, and no error is returned if the entry
 * can be NULL.
 *
 * Returns: TRUE if no error occured
 */
gboolean
mg_entry_combo_set_values (MgEntryCombo *combo, GSList *values)
{
	gboolean err = FALSE;
	gint current_index = -1;
	gboolean allnull = TRUE;
	GSList *list;

	g_return_val_if_fail (combo && IS_MG_ENTRY_COMBO (combo), FALSE);
	g_return_val_if_fail (combo->priv, FALSE);

	/* try to determine if all the values are NULL or of type GDA_VALUE_TYPE_NULL */
	list = values;
	while (list && allnull) {
		if (values->data && (gda_value_get_type ((GdaValue *)values->data) != GDA_VALUE_TYPE_NULL))
			allnull = FALSE;
		list = g_slist_next (list);
	}

	/* actual values settings */
	if (!allnull) {
		g_return_val_if_fail (g_slist_length (values) == g_slist_length (combo->priv->nodes), FALSE);
		
		if (combo->priv->data_model_valid) {
			gint row = 0, nbrows;
			
			nbrows = gda_data_model_get_n_rows (combo->priv->data_model);
			while ((row < nbrows) && (current_index == -1)) {
				GSList *nodes = combo->priv->nodes;
				GSList *argptr = values;
				gboolean equal = TRUE;
				ComboNode *node;
				const GdaValue *model_value, *arg_value;
				
				while (argptr && equal) {
					GdaValueType type1=GDA_VALUE_TYPE_NULL, type2=GDA_VALUE_TYPE_NULL;
					
					node = COMBO_NODE (nodes->data);
					model_value = gda_data_model_get_value_at (combo->priv->data_model, 
										   node->position, row);
					arg_value = (GdaValue*) (argptr->data);
					
					if (arg_value)
						type1 = gda_value_get_type (arg_value);
					if (model_value)
						type2 = gda_value_get_type (model_value);
					if (type1 != type2)
						equal = FALSE;
					else
						if (type1 != GDA_VALUE_TYPE_NULL)
							equal = !gda_value_compare (model_value, arg_value);
					
					nodes = g_slist_next (nodes);
					argptr = g_slist_next (argptr);
				}
				
				if (equal) {
					current_index = row;
					nodes = combo->priv->nodes;
					argptr = values;

					while (nodes) {
						node = COMBO_NODE (nodes->data);
						node->value = gda_data_model_get_value_at (combo->priv->data_model, 
											   node->position, row);
						
						nodes = g_slist_next (nodes);
						argptr = g_slist_next (argptr);
					}
				}
				row++;
			}
		}
		
		if (current_index == -1) 
			err = TRUE;
		else {
			if (combo->priv->null_possible)
				current_index++;

			combo->priv->null_forced = FALSE;
			combo->priv->default_forced = FALSE;
#ifdef USE_OLD_GTK_COMBO_WIDGET
			set_selection_combo_model (combo, current_index);
#else
			TO_IMPLEMENT;
#endif
		}
	}
	else  { /* set to NULL */
		if (combo->priv->null_possible) {
			GSList *list;
		
			/* the 1st entry in the real combo is already a "" entry, so select it. */
#ifdef USE_OLD_GTK_COMBO_WIDGET
			set_selection_combo_model (combo, 0);
#else
			TO_IMPLEMENT;
#endif

			/* adjusting the values */
			list = combo->priv->nodes;
			while (list) {
				COMBO_NODE (list->data)->value = NULL;
				
				list = g_slist_next (list);
			}
			combo->priv->null_forced = TRUE;
		}
		else 
			err = TRUE;
	}

	combo->priv->data_valid = !err;
	g_signal_emit_by_name (G_OBJECT (combo), "status_changed");

	if (!err) 
		/* notify the status and contents changed */
		mg_entry_combo_emit_signal (combo);

	return !err;
}

/**
 * mg_entry_combo_get_values
 * @combo: a #MgEntryCombo widet
 *
 * Get the values stored within @combo. The returned values are a copy of the ones
 * within @combo, so they must be freed afterwards, the same for the list.
 *
 * Returns: a new list of values
 */
GSList *
mg_entry_combo_get_values (MgEntryCombo *combo)
{
	GSList *list, *retval = NULL;
	g_return_val_if_fail (combo && IS_MG_ENTRY_COMBO (combo), NULL);
	g_return_val_if_fail (combo->priv, NULL);

	list = combo->priv->nodes;
	while (list) {
		ComboNode *node = COMBO_NODE (list->data);
		
		if (node->value) 
			retval = g_slist_append (retval, gda_value_copy (node->value));
		else 
			retval = g_slist_append (retval, gda_value_new_null ());

		list = g_slist_next (list);
	}

	return retval;
}

/**
 * mg_entry_combo_set_values_orig
 * @combo: a #MgEntryCombo widet
 * @values: a list of #GdaValue values
 *
 * Sets the original values of @combo to the specified ones. None of the
 * values provided in the list is modified.
 *
 * Returns: TRUE if no error occured
 */
gboolean
mg_entry_combo_set_values_orig (MgEntryCombo *combo, GSList *values)
{
	GSList *list;
	gboolean retval;

	g_return_val_if_fail (combo && IS_MG_ENTRY_COMBO (combo), FALSE);
	g_return_val_if_fail (combo->priv, FALSE);

	retval = mg_entry_combo_set_values (combo, values);

	/* clean all the orig values */
	list = combo->priv->nodes;
	while (list) {
		ComboNode *node = COMBO_NODE (list->data);
		if (node->value_orig) {
			gda_value_free (node->value_orig);
			node->value_orig = NULL;
		}
		
		list = g_slist_next (list);
	}

	if (values) {
		GSList *nodes;
		GSList *argptr;
		const GdaValue *arg_value;
		gboolean equal = TRUE;
		
		g_return_val_if_fail (g_slist_length (values) == g_slist_length (combo->priv->nodes), FALSE);
		
		/* 
		 * first make sure the value types are the same as for the data model 
		 */
		nodes = combo->priv->nodes;
		argptr = values;
		while (argptr && nodes && equal) {
			GdaFieldAttributes *attrs;
			GdaValueType type=GDA_VALUE_TYPE_NULL;
			
			attrs = gda_data_model_describe_column (combo->priv->data_model, 
								COMBO_NODE (nodes->data)->position);
			arg_value = (GdaValue*) (argptr->data);
			
			if (arg_value)
				type = gda_value_get_type (arg_value);
			equal = (type == attrs->gda_type);
			
			nodes = g_slist_next (nodes);
			argptr = g_slist_next (argptr);
		}
		
		/* 
		 * then, actual copy of the values
		 */
		if (equal) {
			nodes = combo->priv->nodes;
			argptr = values;
			while (argptr && nodes && equal) {
				if (argptr->data)
					COMBO_NODE (nodes->data)->value_orig = gda_value_copy ((GdaValue*) (argptr->data));
				nodes = g_slist_next (nodes);
				argptr = g_slist_next (argptr);
			}
		} 
	}

	return retval;;
}

/**
 * mg_entry_combo_get_values_orig
 * @combo: a #MgEntryCombo widet
 *
 * Get the original values stored within @combo. The returned values are the ones
 * within @combo, so they must not be freed afterwards; the list has to be freed afterwards.
 *
 * Returns: a new list of values
 */
GSList *
mg_entry_combo_get_values_orig (MgEntryCombo *combo)
{
	GSList *list, *retval = NULL;
	gboolean allnull = TRUE;

	g_return_val_if_fail (combo && IS_MG_ENTRY_COMBO (combo), NULL);
	g_return_val_if_fail (combo->priv, NULL);

	list = combo->priv->nodes;
	while (list) {
		ComboNode *node = COMBO_NODE (list->data);

		if (node->value_orig && 
		    (gda_value_get_type (node->value_orig) != GDA_VALUE_TYPE_NULL))
			allnull = FALSE;
		
		retval = g_slist_append (retval, node->value_orig);
		
		list = g_slist_next (list);
	}

	if (allnull) {
		g_slist_free (retval);
		retval = NULL;
	}

	return retval;
}

/**
 * mg_entry_combo_set_values_default
 * @combo: a #MgEntryCombo widet
 * @values: a list of #GdaValue values
 *
 * Sets the default values of @combo to the specified ones. None of the
 * values provided in the list is modified.
 *
 * Returns: TRUE if no error occured
 */
gboolean
mg_entry_combo_set_values_default (MgEntryCombo *combo, GSList *values)
{
	g_return_val_if_fail (combo && IS_MG_ENTRY_COMBO (combo), FALSE);
	g_return_val_if_fail (combo->priv, FALSE);
	TO_IMPLEMENT;
	return FALSE;
}


/* 
 * MgDataEntry Interface implementation 
 */

static void
mg_entry_combo_set_attributes (MgDataEntry *iface, guint attrs, guint mask)
{
	MgEntryCombo *combo;

	g_return_if_fail (iface && IS_MG_ENTRY_COMBO (iface));
	combo = MG_ENTRY_COMBO (iface);
	g_return_if_fail (combo->priv);

	/* Setting to NULL */
	if (mask & MG_DATA_ENTRY_IS_NULL) {
		if ((mask & MG_DATA_ENTRY_CAN_BE_NULL) &&
		    !(attrs & MG_DATA_ENTRY_CAN_BE_NULL))
			g_return_if_reached ();
		if (attrs & MG_DATA_ENTRY_IS_NULL) {
			mg_entry_combo_set_values (combo, NULL);
			
			/* if default is set, see if we can keep it that way */
			if (combo->priv->default_forced) {
				GSList *list;
				gboolean allnull = TRUE;
				list = combo->priv->nodes;
				while (list && allnull) {
					if (COMBO_NODE (list->data)->value_default && 
					    (gda_value_get_type (COMBO_NODE (list->data)->value_default) != 
					     GDA_VALUE_TYPE_NULL))
						allnull = FALSE;
					list = g_slist_next (list);
				}

				if (!allnull)
					combo->priv->default_forced = FALSE;
			}

			mg_entry_combo_emit_signal (combo);
			return;
		}
		else {
			combo->priv->null_forced = FALSE;
			mg_entry_combo_emit_signal (combo);
		}
	}

	/* Can be NULL ? */
	if (mask & MG_DATA_ENTRY_CAN_BE_NULL)
		if (combo->priv->null_possible != (attrs & MG_DATA_ENTRY_CAN_BE_NULL) ? TRUE : FALSE) {
			combo->priv->null_possible = (attrs & MG_DATA_ENTRY_CAN_BE_NULL) ? TRUE : FALSE;
			display_combo_model (combo);
		}


	/* Setting to DEFAULT */
	if (mask & MG_DATA_ENTRY_IS_DEFAULT) {
		if ((mask & MG_DATA_ENTRY_CAN_BE_DEFAULT) &&
		    !(attrs & MG_DATA_ENTRY_CAN_BE_DEFAULT))
			g_return_if_reached ();
		if (attrs & MG_DATA_ENTRY_IS_DEFAULT) {
			GSList *tmplist = NULL, *list;
			
			list = combo->priv->nodes;
			while (list) {
				g_slist_append (tmplist, COMBO_NODE (list->data)->value_default);
				list = g_slist_next (list);
			}
			mg_entry_combo_set_values (combo, tmplist);
			g_slist_free (tmplist);

			/* if NULL is set, see if we can keep it that way */
			if (combo->priv->null_forced) {
				GSList *list;
				gboolean allnull = TRUE;
				list = combo->priv->nodes;
				while (list && allnull) {
					if (COMBO_NODE (list->data)->value_default && 
					    (gda_value_get_type (COMBO_NODE (list->data)->value_default) != 
					     GDA_VALUE_TYPE_NULL))
						allnull = FALSE;
					list = g_slist_next (list);
				}
				
				if (!allnull)
					combo->priv->null_forced = FALSE;
			}

			combo->priv->default_forced = TRUE;
			mg_entry_combo_emit_signal (combo);
			return;
		}
		else {
			combo->priv->default_forced = FALSE;
			mg_entry_combo_emit_signal (combo);
		}
	}

	/* Can be DEFAULT ? */
	if (mask & MG_DATA_ENTRY_CAN_BE_DEFAULT)
		combo->priv->default_possible = (attrs & MG_DATA_ENTRY_CAN_BE_DEFAULT) ? TRUE : FALSE;
	
	/* Modified ? */
	if (mask & MG_DATA_ENTRY_IS_UNCHANGED) {
		if (attrs & MG_DATA_ENTRY_IS_UNCHANGED) {
			GSList *tmplist = NULL, *list;

			list = combo->priv->nodes;
			while (list) {
				tmplist = g_slist_append (tmplist, COMBO_NODE (list->data)->value_orig);
				list = g_slist_next (list);
			}
				
			mg_entry_combo_set_values (combo, tmplist);
			g_slist_free (tmplist);
			combo->priv->default_forced = FALSE;
			mg_entry_combo_emit_signal (combo);
		}
	}

	/* Actions buttons ? */
	if (mask & MG_DATA_ENTRY_ACTIONS_SHOWN) {
		GValue *gval;
		combo->priv->show_actions = (attrs & MG_DATA_ENTRY_ACTIONS_SHOWN) ? TRUE : FALSE;
		
		gval = g_new0 (GValue, 1);
		g_value_init (gval, G_TYPE_BOOLEAN);
		g_value_set_boolean (gval, combo->priv->show_actions);
		g_object_set_property (G_OBJECT (combo), "actions", gval);
		g_free (gval);
	}

	/* Can't force data to be valid */
	if (mask & MG_DATA_ENTRY_DATA_NON_VALID) 
		g_assert_not_reached ();

	g_signal_emit_by_name (G_OBJECT (combo), "status_changed");
}

static guint
mg_entry_combo_get_attributes (MgDataEntry *iface)
{
	guint retval = 0;
	MgEntryCombo *combo;
	GSList *list;
	gboolean isnull = TRUE;
	gboolean isunchanged = TRUE;

	g_return_val_if_fail (iface && IS_MG_ENTRY_COMBO (iface), 0);
	combo = MG_ENTRY_COMBO (iface);
	g_return_val_if_fail (combo->priv, 0);

	list = combo->priv->nodes;
	while (list) {
		gboolean changed = FALSE;

		/* NULL? */
		if (COMBO_NODE (list->data)->value &&
		    (gda_value_get_type (COMBO_NODE (list->data)->value) != GDA_VALUE_TYPE_NULL))
			isnull = FALSE;
		
		/* is unchanged */
		if (COMBO_NODE (list->data)->value_orig) {
			if (COMBO_NODE (list->data)->value && 
			    (gda_value_get_type (COMBO_NODE (list->data)->value) == 
			     gda_value_get_type (COMBO_NODE (list->data)->value_orig))) {
				if (gda_value_get_type (COMBO_NODE (list->data)->value) == GDA_VALUE_TYPE_NULL)
					changed = FALSE;
				else {
					if (gda_value_compare (COMBO_NODE (list->data)->value, 
							       COMBO_NODE (list->data)->value_orig))
						changed = TRUE;
				}
			}
			else
				changed = TRUE;
		}

		if (changed)
			isunchanged = FALSE;

		list = g_slist_next (list);
	}

	if (isunchanged)
		retval = retval | MG_DATA_ENTRY_IS_UNCHANGED;

	if (isnull || combo->priv->null_forced)
		retval = retval | MG_DATA_ENTRY_IS_NULL;

	/* can be NULL? */
	if (combo->priv->null_possible) 
		retval = retval | MG_DATA_ENTRY_CAN_BE_NULL;
	
	/* is default */
	if (combo->priv->default_forced)
		retval = retval | MG_DATA_ENTRY_IS_DEFAULT;
	
	/* can be default? */
	if (combo->priv->default_possible)
		retval = retval | MG_DATA_ENTRY_CAN_BE_DEFAULT;
	

	/* actions shown */
	if (combo->priv->show_actions)
		retval = retval | MG_DATA_ENTRY_ACTIONS_SHOWN;

	/* data valid? */
	if (! combo->priv->data_valid)
		retval = retval | MG_DATA_ENTRY_DATA_NON_VALID;
	else {
		GSList *nodes;
		gboolean allnull = TRUE;
		
		nodes = combo->priv->nodes;
 		while (nodes) {
			ComboNode *node = COMBO_NODE (nodes->data);

			/* all the nodes are NULL ? */
			if (node->value && (gda_value_get_type (node->value) != GDA_VALUE_TYPE_NULL))
				allnull = FALSE;

			nodes = g_slist_next (nodes);
		}

		if ((allnull && !combo->priv->null_possible) ||
		    (combo->priv->null_forced && !combo->priv->null_possible))
			retval = retval | MG_DATA_ENTRY_DATA_NON_VALID;
	}

	return retval;
}


static gboolean
mg_entry_combo_expand_in_layout (MgDataEntry *iface)
{
	MgEntryCombo *combo;

	g_return_val_if_fail (iface && IS_MG_ENTRY_COMBO (iface), FALSE);
	combo = MG_ENTRY_COMBO (iface);
	g_return_val_if_fail (combo->priv, FALSE);

	return FALSE;
}
