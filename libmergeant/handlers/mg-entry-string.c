/* mg-entry-string.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-entry-string.h"
#include <libmergeant/mg-data-handler.h>

/* 
 * Main static functions 
 */
static void mg_entry_string_class_init (MgEntryStringClass * class);
static void mg_entry_string_init (MgEntryString * srv);
static void mg_entry_string_dispose (GObject   * object);
static void mg_entry_string_finalize (GObject   * object);

/* virtual functions */
static GtkWidget *create_entry (MgEntryWrapper *mgwrap);
static void       real_set_value (MgEntryWrapper *mgwrap, const GdaValue *value);
static GdaValue  *real_get_value (MgEntryWrapper *mgwrap);
static void       connect_signals(MgEntryWrapper *mgwrap, GCallback callback);
static gboolean   expand_in_layout (MgEntryWrapper *mgwrap);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* private structure */
struct _MgEntryStringPrivate
{
	GtkWidget *entry;
};


guint
mg_entry_string_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgEntryStringClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_entry_string_class_init,
			NULL,
			NULL,
			sizeof (MgEntryString),
			0,
			(GInstanceInitFunc) mg_entry_string_init
		};
		
		type = g_type_register_static (MG_ENTRY_WRAPPER_TYPE, "MgEntryString", &info, 0);
	}
	return type;
}

static void
mg_entry_string_class_init (MgEntryStringClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_entry_string_dispose;
	object_class->finalize = mg_entry_string_finalize;

	MG_ENTRY_WRAPPER_CLASS (class)->create_entry = create_entry;
	MG_ENTRY_WRAPPER_CLASS (class)->real_set_value = real_set_value;
	MG_ENTRY_WRAPPER_CLASS (class)->real_get_value = real_get_value;
	MG_ENTRY_WRAPPER_CLASS (class)->connect_signals = connect_signals;
	MG_ENTRY_WRAPPER_CLASS (class)->expand_in_layout = expand_in_layout;
}

static void
mg_entry_string_init (MgEntryString * mg_entry_string)
{
	mg_entry_string->priv = g_new0 (MgEntryStringPrivate, 1);
	mg_entry_string->priv->entry = NULL;
}

/**
 * mg_entry_string_new
 * @dh: the data handler to be used by the new widget
 * @type: the requested data type (compatible with @dh)
 *
 * Creates a new widget which is mainly a GtkEntry
 *
 * Returns: the new widget
 */
GtkWidget *
mg_entry_string_new (MgDataHandler *dh, GdaValueType type)
{
	GObject *obj;
	MgEntryString *mgstr;

	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (type != GDA_VALUE_TYPE_UNKNOWN, NULL);
	g_return_val_if_fail (mg_data_handler_accepts_gda_type (dh, type), NULL);

	obj = g_object_new (MG_ENTRY_STRING_TYPE, "handler", dh, NULL);
	mgstr = MG_ENTRY_STRING (obj);
	mg_data_entry_set_value_type (MG_DATA_ENTRY (mgstr), type);

	return GTK_WIDGET (obj);
}


static void
mg_entry_string_dispose (GObject   * object)
{
	MgEntryString *mg_entry_string;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_STRING (object));

	mg_entry_string = MG_ENTRY_STRING (object);
	if (mg_entry_string->priv) {

	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_entry_string_finalize (GObject   * object)
{
	MgEntryString *mg_entry_string;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_STRING (object));

	mg_entry_string = MG_ENTRY_STRING (object);
	if (mg_entry_string->priv) {

		g_free (mg_entry_string->priv);
		mg_entry_string->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

static GtkWidget *
create_entry (MgEntryWrapper *mgwrap)
{
	GtkWidget *entry;
	MgEntryString *mgstr;

	g_return_val_if_fail (mgwrap && IS_MG_ENTRY_STRING (mgwrap), NULL);
	mgstr = MG_ENTRY_STRING (mgwrap);
	g_return_val_if_fail (mgstr->priv, NULL);

	entry = gtk_entry_new ();
	mgstr->priv->entry = entry;

	return entry;
}

static void
real_set_value (MgEntryWrapper *mgwrap, const GdaValue *value)
{
	MgEntryString *mgstr;

	g_return_if_fail (mgwrap && IS_MG_ENTRY_STRING (mgwrap));
	mgstr = MG_ENTRY_STRING (mgwrap);
	g_return_if_fail (mgstr->priv);

	if (value) {
		if (gda_value_is_null (value))
			gtk_entry_set_text (GTK_ENTRY (mgstr->priv->entry), "");
		else {
			MgDataHandler *dh;		
			gchar *str;

			dh = mg_data_entry_get_handler (MG_DATA_ENTRY (mgwrap));
			str = mg_data_handler_get_str_from_value (dh, value);
			if (str) {
				gtk_entry_set_text (GTK_ENTRY (mgstr->priv->entry), str);
				g_free (str);
			}
			else
				gtk_entry_set_text (GTK_ENTRY (mgstr->priv->entry), "");
		}
	}
	else 
		gtk_entry_set_text (GTK_ENTRY (mgstr->priv->entry), "");
}

static GdaValue *
real_get_value (MgEntryWrapper *mgwrap)
{
	GdaValue *value;
	MgEntryString *mgstr;
	MgDataHandler *dh;
	const gchar *str;

	g_return_val_if_fail (mgwrap && IS_MG_ENTRY_STRING (mgwrap), NULL);
	mgstr = MG_ENTRY_STRING (mgwrap);
	g_return_val_if_fail (mgstr->priv, NULL);

	dh = mg_data_entry_get_handler (MG_DATA_ENTRY (mgwrap));
	str = gtk_entry_get_text (GTK_ENTRY (mgstr->priv->entry));
	value = mg_data_handler_get_value_from_sql (dh, str, mg_data_entry_get_value_type (MG_DATA_ENTRY (mgwrap)));
	if (!value) {
		/* in case the mg_data_handler_get_value_from_sql() returned an error because
		   the contents of the GtkEntry cannot be interpreted as a GdaValue */
		value = gda_value_new_null ();
	}

	return value;
}

static void
connect_signals(MgEntryWrapper *mgwrap, GCallback callback)
{
	MgEntryString *mgstr;

	g_return_if_fail (mgwrap && IS_MG_ENTRY_STRING (mgwrap));
	mgstr = MG_ENTRY_STRING (mgwrap);
	g_return_if_fail (mgstr->priv);

	g_signal_connect (G_OBJECT (mgstr->priv->entry), "changed",
			  callback, mgwrap);
}

static gboolean
expand_in_layout (MgEntryWrapper *mgwrap)
{
	return FALSE;
}
