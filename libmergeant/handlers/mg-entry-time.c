/* mg-entry-time.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-entry-time.h"
#include <libmergeant/mg-data-handler.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>

/* 
 * Main static functions 
 */
static void mg_entry_time_class_init (MgEntryTimeClass * class);
static void mg_entry_time_init (MgEntryTime * srv);
static void mg_entry_time_dispose (GObject *object);
static void mg_entry_time_finalize (GObject *object);

/* virtual functions */
static GtkWidget *create_entry (MgEntryWrapper *mgwrap);
static void       real_set_value (MgEntryWrapper *mgwrap, const GdaValue *value);
static GdaValue  *real_get_value (MgEntryWrapper *mgwrap);
static void       connect_signals(MgEntryWrapper *mgwrap, GCallback callback);
static gboolean   expand_in_layout (MgEntryWrapper *mgwrap);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;


/* private structure */
struct _MgEntryTimePrivate
{
	/* for date */
	GtkWidget *entry_date;
	GtkWidget *date;
        GtkWidget *window;
        GtkWidget *date_button;

	/* for time */
	GtkWidget *entry_time;
};


guint
mg_entry_time_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgEntryTimeClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_entry_time_class_init,
			NULL,
			NULL,
			sizeof (MgEntryTime),
			0,
			(GInstanceInitFunc) mg_entry_time_init
		};
		
		type = g_type_register_static (MG_ENTRY_WRAPPER_TYPE, "MgEntryTime", &info, 0);
	}
	return type;
}

static void
mg_entry_time_class_init (MgEntryTimeClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_entry_time_dispose;
	object_class->finalize = mg_entry_time_finalize;

	MG_ENTRY_WRAPPER_CLASS (class)->create_entry = create_entry;
	MG_ENTRY_WRAPPER_CLASS (class)->real_set_value = real_set_value;
	MG_ENTRY_WRAPPER_CLASS (class)->real_get_value = real_get_value;
	MG_ENTRY_WRAPPER_CLASS (class)->connect_signals = connect_signals;
	MG_ENTRY_WRAPPER_CLASS (class)->expand_in_layout = expand_in_layout;
}

static void
mg_entry_time_init (MgEntryTime * mg_entry_time)
{
	mg_entry_time->priv = g_new0 (MgEntryTimePrivate, 1);
	mg_entry_time->priv->entry_date = NULL;
	mg_entry_time->priv->entry_time = NULL;
	mg_entry_time->priv->date = NULL;
	mg_entry_time->priv->window = NULL;
	mg_entry_time->priv->date_button = NULL;
}

/**
 * mg_entry_time_new
 * @dh: the data handler to be used by the new widget
 * @type: the requested data type (compatible with @dh)
 *
 * Creates a new widget which is mainly a GtkEntry
 *
 * Returns: the new widget
 */
GtkWidget *
mg_entry_time_new (MgDataHandler *dh, GdaValueType type)
{
	GObject *obj;
	MgEntryTime *mgtim;

	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (type != GDA_VALUE_TYPE_UNKNOWN, NULL);
	g_return_val_if_fail (mg_data_handler_accepts_gda_type (dh, type), NULL);

	obj = g_object_new (MG_ENTRY_TIME_TYPE, "handler", dh, NULL);
	mgtim = MG_ENTRY_TIME (obj);
	mg_data_entry_set_value_type (MG_DATA_ENTRY (mgtim), type);

	return GTK_WIDGET (obj);
}


static void
mg_entry_time_dispose (GObject   * object)
{
	MgEntryTime *mg_entry_time;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_TIME (object));

	mg_entry_time = MG_ENTRY_TIME (object);
	if (mg_entry_time->priv) {

	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_entry_time_finalize (GObject   * object)
{
	MgEntryTime *mg_entry_time;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_TIME (object));

	mg_entry_time = MG_ENTRY_TIME (object);
	if (mg_entry_time->priv) {

		g_free (mg_entry_time->priv);
		mg_entry_time->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

static GtkWidget *create_entry_date (MgEntryTime *mgtim);
static GtkWidget *create_entry_time (MgEntryTime *mgtim);
static GtkWidget *create_entry_ts (MgEntryTime *mgtim);
static GtkWidget *
create_entry (MgEntryWrapper *mgwrap)
{
	MgEntryTime *mgtim;
	GtkWidget *entry = NULL;

	g_return_val_if_fail (mgwrap && IS_MG_ENTRY_TIME (mgwrap), NULL);
	mgtim = MG_ENTRY_TIME (mgwrap);
	g_return_val_if_fail (mgtim->priv, NULL);

	switch (mg_data_entry_get_value_type (MG_DATA_ENTRY (mgtim))) {
	case GDA_VALUE_TYPE_DATE:
		entry = create_entry_date (mgtim);
		break;
	case GDA_VALUE_TYPE_TIME:
		entry = create_entry_time (mgtim);
		break;
	case GDA_VALUE_TYPE_TIMESTAMP:
		entry = create_entry_ts (mgtim);
		break;
	default:
		g_assert_not_reached ();
		break;
	}
		
	return entry;
}

static void
real_set_value (MgEntryWrapper *mgwrap, const GdaValue *value)
{
	MgEntryTime *mgtim;
	GdaValueType type;
	MgDataHandler *dh;

	g_return_if_fail (mgwrap && IS_MG_ENTRY_TIME (mgwrap));
	mgtim = MG_ENTRY_TIME (mgwrap);
	g_return_if_fail (mgtim->priv);

	dh = mg_data_entry_get_handler (MG_DATA_ENTRY (mgwrap));
	type = mg_data_entry_get_value_type (MG_DATA_ENTRY (mgtim));
	switch (type) {
	case GDA_VALUE_TYPE_DATE:
		if (value) {
			if (gda_value_is_null (value))
				gtk_entry_set_text (GTK_ENTRY (mgtim->priv->entry_date), "");
			else {
				gchar *str;
				
				str = mg_data_handler_get_str_from_value (dh, value);
				gtk_entry_set_text (GTK_ENTRY (mgtim->priv->entry_date), str);
				g_free (str);
			}
		}
		else 
			gtk_entry_set_text (GTK_ENTRY (mgtim->priv->entry_date), "");
		break;
	case GDA_VALUE_TYPE_TIME:
		if (value) {
			if (gda_value_is_null (value))
				gtk_entry_set_text (GTK_ENTRY (mgtim->priv->entry_time), "");
			else {
				gchar *str;
				
				str = mg_data_handler_get_str_from_value (dh, value);
				gtk_entry_set_text (GTK_ENTRY (mgtim->priv->entry_time), str);
				g_free (str);
			}
		}
		else 
			gtk_entry_set_text (GTK_ENTRY (mgtim->priv->entry_time), "");
		break;
	case GDA_VALUE_TYPE_TIMESTAMP:
		if (value) {
			if (gda_value_is_null (value))
				gtk_entry_set_text (GTK_ENTRY (mgtim->priv->entry_time), "");
			else {
				gchar *str, *ptr;
				
				str = mg_data_handler_get_str_from_value (dh, value);
				ptr = strtok (str, " ");
				gtk_entry_set_text (GTK_ENTRY (mgtim->priv->entry_date), ptr);
				ptr = strtok (NULL, " ");
				gtk_entry_set_text (GTK_ENTRY (mgtim->priv->entry_time), ptr);
				g_free (str);
			}
		}
		else {
			gtk_entry_set_text (GTK_ENTRY (mgtim->priv->entry_date), "");
			gtk_entry_set_text (GTK_ENTRY (mgtim->priv->entry_time), "");
		}
		break;
	default:
		g_assert_not_reached ();
		break;	
	}
}

static GdaValue *
real_get_value (MgEntryWrapper *mgwrap)
{
	GdaValue *value = NULL;
	MgEntryTime *mgtim;
	MgDataHandler *dh;
	const gchar *str;
	gchar *str2;
	GdaValueType type;

	g_return_val_if_fail (mgwrap && IS_MG_ENTRY_TIME (mgwrap), NULL);
	mgtim = MG_ENTRY_TIME (mgwrap);
	g_return_val_if_fail (mgtim->priv, NULL);

	type = mg_data_entry_get_value_type (MG_DATA_ENTRY (mgtim));
	dh = mg_data_entry_get_handler (MG_DATA_ENTRY (mgwrap));
	switch (type) {
	case GDA_VALUE_TYPE_DATE:
		str = gtk_entry_get_text (GTK_ENTRY (mgtim->priv->entry_date));
		value = mg_data_handler_get_value_from_str (dh, str, type); /* FIXME: not SQL but STR */
		break;
	case GDA_VALUE_TYPE_TIME:
		str = gtk_entry_get_text (GTK_ENTRY (mgtim->priv->entry_time));
		value = mg_data_handler_get_value_from_str (dh, str, type);
		break;
	case GDA_VALUE_TYPE_TIMESTAMP:
		str2 = g_strdup_printf ("%s %s",
					gtk_entry_get_text (GTK_ENTRY (mgtim->priv->entry_date)),
					gtk_entry_get_text (GTK_ENTRY (mgtim->priv->entry_time)));
		value = mg_data_handler_get_value_from_str (dh, str2, type);
		g_free (str2);
		break;
	default:
		g_assert_not_reached ();
		break;	
	}

	if (!value) {
		/* in case the mg_data_handler_get_value_from_str() returned an error because
		   the contents of the GtkEntry cannot be interpreted as a GdaValue */
		value = gda_value_new_null ();
	}

	return value;
}

static void
connect_signals(MgEntryWrapper *mgwrap, GCallback callback)
{
	MgEntryTime *mgtim;
	GdaValueType type;

	g_return_if_fail (mgwrap && IS_MG_ENTRY_TIME (mgwrap));
	mgtim = MG_ENTRY_TIME (mgwrap);
	g_return_if_fail (mgtim->priv);

	type = mg_data_entry_get_value_type (MG_DATA_ENTRY (mgtim));
	switch (type) {
	case GDA_VALUE_TYPE_DATE:
		g_signal_connect (G_OBJECT (mgtim->priv->entry_date), "changed",
				  callback, mgwrap);
		break;
	case GDA_VALUE_TYPE_TIME:
		g_signal_connect (G_OBJECT (mgtim->priv->entry_time), "changed",
				  callback, mgwrap);
		break;
	case GDA_VALUE_TYPE_TIMESTAMP:
		g_signal_connect (G_OBJECT (mgtim->priv->entry_date), "changed",
				  callback, mgwrap);
		g_signal_connect (G_OBJECT (mgtim->priv->entry_time), "changed",
				  callback, mgwrap);
		break;
	default:
		g_assert_not_reached ();
		break;
	}
}

static gboolean
expand_in_layout (MgEntryWrapper *mgwrap)
{
	return FALSE;
}






/*
 * callbacks for the date 
 */

static gint date_delete_popup (GtkWidget *widget, MgEntryTime *mgtim);
static gint date_key_press_popup (GtkWidget *widget, GdkEventKey *event, MgEntryTime *mgtim);
static gint date_button_press_popup (GtkWidget *widget, GdkEventButton *event, MgEntryTime *mgtim);
static void date_day_selected (GtkCalendar *calendar, MgEntryTime *mgtim);
static void date_day_selected_double_click (GtkCalendar *calendar, MgEntryTime *mgtim);
static void date_calendar_choose_cb (GtkWidget *button, MgEntryTime *mgtim);

static GtkWidget *
create_entry_date (MgEntryTime *mgtim)
{
	GtkWidget *wid, *hb, *window;

	/* top widget */
	hb = gtk_hbox_new (FALSE, 5);
	/* text entry */
	wid = gtk_entry_new ();
	gtk_entry_set_max_length (GTK_ENTRY (wid), 10); /* for a numerical date format */
	gtk_entry_set_width_chars (GTK_ENTRY (wid), 10);
	gtk_box_pack_start (GTK_BOX (hb), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);
	mgtim->priv->entry_date = wid;
	
	/* window to hold the calendar popup */
	window = gtk_window_new (GTK_WINDOW_POPUP);
	gtk_widget_set_events (window, gtk_widget_get_events (window) | GDK_KEY_PRESS_MASK);
	gtk_window_set_resizable (GTK_WINDOW (window), FALSE);
	g_signal_connect (G_OBJECT (window), "delete_event",
			  G_CALLBACK (date_delete_popup), mgtim);
	g_signal_connect (G_OBJECT (window), "key_press_event",
			  G_CALLBACK (date_key_press_popup), mgtim);
	g_signal_connect (G_OBJECT (window), "button_press_event",
			  G_CALLBACK (date_button_press_popup), mgtim);
	mgtim->priv->window = window;
	
	/* calendar */
	wid = gtk_calendar_new ();
	mgtim->priv->date = wid;
	gtk_container_add (GTK_CONTAINER (window), wid);
	gtk_widget_show (wid);
	g_signal_connect (G_OBJECT (wid), "day_selected",
			  G_CALLBACK (date_day_selected), mgtim);
	g_signal_connect (G_OBJECT (wid), "day_selected_double_click",
			  G_CALLBACK (date_day_selected_double_click), mgtim);
	
	/* button to pop up the calendar */
	wid = gtk_button_new_with_label (_("Choose"));
	gtk_box_pack_start (GTK_BOX (hb), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);
	g_signal_connect (G_OBJECT (wid), "clicked",
			  G_CALLBACK (date_calendar_choose_cb), mgtim);
	mgtim->priv->date_button = wid;

	/* padding */
	wid = gtk_label_new ("");
	gtk_box_pack_start (GTK_BOX (hb), wid, TRUE, TRUE, 0);
	gtk_widget_show (wid);

	return hb;
}

static void hide_popup (MgEntryTime *mgtim)
{
        gtk_widget_hide (mgtim->priv->window);
        gtk_grab_remove (mgtim->priv->window);
}
static gint
date_delete_popup (GtkWidget *widget, MgEntryTime *mgtim)
{
	hide_popup (mgtim);
	return TRUE;
}

static gint
date_key_press_popup (GtkWidget *widget, GdkEventKey *event, MgEntryTime *mgtim)
{
	if (event->keyval != GDK_Escape)
                return FALSE;

        g_signal_stop_emission_by_name (widget, "key_press_event");
        hide_popup (mgtim);

        return TRUE;
}

static gint
date_button_press_popup (GtkWidget *widget, GdkEventButton *event, MgEntryTime *mgtim)
{
	GtkWidget *child;

        child = gtk_get_event_widget ((GdkEvent *) event);

        /* We don't ask for button press events on the grab widget, so
         *  if an event is reported directly to the grab widget, it must
         *  be on a window outside the application (and thus we remove
         *  the popup window). Otherwise, we check if the widget is a child
         *  of the grab widget, and only remove the popup window if it
         *  is not.
         */
        if (child != widget) {
                while (child) {
                        if (child == widget)
                                return FALSE;
                        child = child->parent;
                }
        }

        hide_popup (mgtim);

        return TRUE;
}

static void
date_day_selected (GtkCalendar *calendar, MgEntryTime *mgtim)
{
	char buffer [256];
        guint year, month, day;
        struct tm mtm = {0};
        char *str_utf8;

        gtk_calendar_get_date (calendar, &year, &month, &day);

        mtm.tm_mday = day;
        mtm.tm_mon = month;
        if (year > 1900)
                mtm.tm_year = year - 1900;
        else
                mtm.tm_year = year;

        if (strftime (buffer, sizeof (buffer), "%x", &mtm) == 0)
                strcpy (buffer, "???");
        buffer[sizeof(buffer)-1] = '\0';

        str_utf8 = g_locale_to_utf8 (buffer, -1, NULL, NULL, NULL);
        gtk_entry_set_text (GTK_ENTRY (mgtim->priv->entry_date),
                            str_utf8 ? str_utf8 : "");
        g_free (str_utf8);
}

static void
date_day_selected_double_click (GtkCalendar *calendar, MgEntryTime *mgtim)
{
	hide_popup (mgtim);
}


static gboolean popup_grab_on_window (GdkWindow *window, guint32 activate_time);
static void position_popup (MgEntryTime *mgtim);
static void
date_calendar_choose_cb (GtkWidget *button, MgEntryTime *mgtim)
{
	GdaValue *value;
        guint year=0, month=0, day=0;
	MgDataHandler *dh;

        /* setting the calendar to the latest displayed date */
	dh = mg_data_entry_get_handler (MG_DATA_ENTRY (mgtim));
	value = mg_data_entry_get_value (MG_DATA_ENTRY (mgtim));
	
        if (value && !gda_value_is_null (value)) {
		const GdaDate *date;
		const GdaTimestamp *ts;

		switch (mg_data_entry_get_value_type (MG_DATA_ENTRY (mgtim))) {
		case GDA_VALUE_TYPE_DATE:
			date = gda_value_get_date (value);
			year = date->year;
			month = date->month - 1;
			day = date->day;
			break;
		case GDA_VALUE_TYPE_TIMESTAMP:
			ts = gda_value_get_timestamp (value);
			year = ts->year;
			month = ts->month - 1;
			day = ts->day;
			break;
		default:
			g_assert_not_reached ();
			break;
		}
        }
        else {
                time_t now;
                struct tm *stm;

                now = time (NULL);
                stm = localtime (&now);
                year = stm->tm_year + 1900;
                month = stm->tm_mon;
                day = stm->tm_mday;
        }

        gtk_calendar_select_month (GTK_CALENDAR (mgtim->priv->date), month, year);
        gtk_calendar_select_day (GTK_CALENDAR (mgtim->priv->date), day);

        /* popup window */
        /* Temporarily grab pointer and keyboard, copied from GnomeDateEdit */
        if (!popup_grab_on_window (button->window, gtk_get_current_event_time ()))
                return;
        position_popup (mgtim);
        gtk_grab_add (mgtim->priv->window);
        gtk_widget_show (mgtim->priv->window);
        gtk_widget_grab_focus (mgtim->priv->date);
        popup_grab_on_window (mgtim->priv->window->window,
                              gtk_get_current_event_time ());
}

static gboolean
popup_grab_on_window (GdkWindow *window, guint32 activate_time)
{
        if ((gdk_pointer_grab (window, TRUE,
                               GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK |
                               GDK_POINTER_MOTION_MASK,
                               NULL, NULL, activate_time) == 0)) {
                if (gdk_keyboard_grab (window, TRUE,
                                       activate_time) == 0)
                        return TRUE;
                else {
                        gdk_pointer_ungrab (activate_time);
                        return FALSE;
                }
        }
        return FALSE;
}

static void
position_popup (MgEntryTime *mgtim)
{
        gint x, y;
        gint bwidth, bheight;
        GtkRequisition req;

        gtk_widget_size_request (mgtim->priv->window, &req);

        gdk_window_get_origin (mgtim->priv->date_button->window, &x, &y);

        x += mgtim->priv->date_button->allocation.x;
        y += mgtim->priv->date_button->allocation.y;
        bwidth = mgtim->priv->date_button->allocation.width;
        bheight = mgtim->priv->date_button->allocation.height;

        x += bwidth - req.width;
        y += bheight;

        if (x < 0)
                x = 0;

        if (y < 0)
                y = 0;

        gtk_window_move (GTK_WINDOW (mgtim->priv->window), x, y);
}




/*
 * callbacks for the time
 */
static GtkWidget *
create_entry_time (MgEntryTime *mgtim)
{
	GtkWidget *wid, *hb;

	hb = gtk_hbox_new (FALSE, 5);

	/* text entry */
        wid = gtk_entry_new ();
        gtk_box_pack_start (GTK_BOX (hb), wid, FALSE, FALSE, 0);
        gtk_entry_set_max_length (GTK_ENTRY (wid), 8); /* max for HH:MM:SS format */
	gtk_entry_set_width_chars (GTK_ENTRY (wid), 8);
        gtk_widget_show (wid);
        mgtim->priv->entry_time = wid;

        /* small label */
        wid = gtk_label_new (_("hh:mm:ss"));
        gtk_box_pack_start (GTK_BOX (hb), wid, FALSE, FALSE, 0);
        gtk_widget_show (wid);

        return hb;
}


/*
 * callbacks for the timestamp
 */
static GtkWidget *
create_entry_ts (MgEntryTime *mgtim)
{
	GtkWidget *hb, *wid;

	hb = gtk_hbox_new (FALSE, 0);
	
	/* date part */
	wid = create_entry_date (mgtim);
	gtk_box_pack_start (GTK_BOX (hb), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);

	/* time part */
	wid = create_entry_time (mgtim);
	gtk_box_pack_start (GTK_BOX (hb), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);

	return hb;
}
