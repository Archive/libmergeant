/* mg-entry-none.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_ENTRY_NONE_H_
#define __MG_ENTRY_NONE_H_

#include "mg-entry-wrapper.h"

G_BEGIN_DECLS

#define MG_ENTRY_NONE_TYPE          (mg_entry_none_get_type())
#define MG_ENTRY_NONE(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_entry_none_get_type(), MgEntryNone)
#define MG_ENTRY_NONE_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_entry_none_get_type (), MgEntryNoneClass)
#define IS_MG_ENTRY_NONE(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_entry_none_get_type ())


typedef struct _MgEntryNone MgEntryNone;
typedef struct _MgEntryNoneClass MgEntryNoneClass;
typedef struct _MgEntryNonePrivate MgEntryNonePrivate;


/* struct for the object's data */
struct _MgEntryNone
{
	MgEntryWrapper              object;
	MgEntryNonePrivate       *priv;
};

/* struct for the object's class */
struct _MgEntryNoneClass
{
	MgEntryWrapperClass         class;
};

guint        mg_entry_none_get_type        (void);
GtkWidget   *mg_entry_none_new             (MgDataHandler *dh, GdaValueType type);


G_END_DECLS

#endif
