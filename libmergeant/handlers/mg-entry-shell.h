/* mg-entry-shell.h
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __MG_ENTRY_SHELL__
#define __MG_ENTRY_SHELL__

#include <gtk/gtk.h>
#include <libmergeant/mg-data-entry.h>

G_BEGIN_DECLS

#define MG_ENTRY_SHELL_TYPE          (mg_entry_shell_get_type())
#define MG_ENTRY_SHELL(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_entry_shell_get_type(), MgEntryShell)
#define MG_ENTRY_SHELL_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_entry_shell_get_type (), MgEntryShellClass)
#define IS_MG_ENTRY_SHELL(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_entry_shell_get_type ())

/*
 * Very simple object wrapper for the widgets that will be used to display
 * the data.
 */

typedef struct _MgEntryShell      MgEntryShell;
typedef struct _MgEntryShellClass MgEntryShellClass;
typedef struct _MgEntryShellPriv  MgEntryShellPriv;

/* Properties:
 * name        type          read/write      description
 * ----------------------------------------------------------------
 * handler     pointer       RW              The associated MgDataHandler object
 * actions     boolean       RW              Show or hide the actions buttons
 */



/* struct for the object's data */
struct _MgEntryShell
{
	GtkVBox         object;

	MgEntryShellPriv  *priv;
};

/* struct for the object's class */
struct _MgEntryShellClass
{
	GtkVBoxClass    parent_class;
};


guint           mg_entry_shell_get_type      (void);
void            mg_entry_shell_pack_entry    (MgEntryShell *shell, GtkWidget *main_widget);
void            mg_entry_shell_refresh       (MgEntryShell *shell);

G_END_DECLS

#endif
