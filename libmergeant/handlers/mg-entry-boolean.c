/* mg-entry-boolean.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-entry-boolean.h"
#include <libmergeant/mg-data-handler.h>

/* 
 * Main static functions 
 */
static void mg_entry_boolean_class_init (MgEntryBooleanClass * class);
static void mg_entry_boolean_init (MgEntryBoolean * srv);
static void mg_entry_boolean_dispose (GObject   * object);
static void mg_entry_boolean_finalize (GObject   * object);

/* virtual functions */
static GtkWidget *create_entry (MgEntryWrapper *mgwrap);
static void       real_set_value (MgEntryWrapper *mgwrap, const GdaValue *value);
static GdaValue  *real_get_value (MgEntryWrapper *mgwrap);
static void       connect_signals(MgEntryWrapper *mgwrap, GCallback callback);
static gboolean   expand_in_layout (MgEntryWrapper *mgwrap);


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* private structure */
struct _MgEntryBooleanPrivate
{
	GtkWidget *hbox;
	GtkWidget *check;
	GtkWidget *label_true;
	GtkWidget *label_false;
	GtkWidget *label_unset;
};


guint
mg_entry_boolean_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgEntryBooleanClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_entry_boolean_class_init,
			NULL,
			NULL,
			sizeof (MgEntryBoolean),
			0,
			(GInstanceInitFunc) mg_entry_boolean_init
		};
		
		type = g_type_register_static (MG_ENTRY_WRAPPER_TYPE, "MgEntryBoolean", &info, 0);
	}
	return type;
}

static void
mg_entry_boolean_class_init (MgEntryBooleanClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_entry_boolean_dispose;
	object_class->finalize = mg_entry_boolean_finalize;

	MG_ENTRY_WRAPPER_CLASS (class)->create_entry = create_entry;
	MG_ENTRY_WRAPPER_CLASS (class)->real_set_value = real_set_value;
	MG_ENTRY_WRAPPER_CLASS (class)->real_get_value = real_get_value;
	MG_ENTRY_WRAPPER_CLASS (class)->connect_signals = connect_signals;
	MG_ENTRY_WRAPPER_CLASS (class)->expand_in_layout = expand_in_layout;
}

static void
mg_entry_boolean_init (MgEntryBoolean * mg_entry_boolean)
{
	mg_entry_boolean->priv = g_new0 (MgEntryBooleanPrivate, 1);
	mg_entry_boolean->priv->hbox = NULL;
	mg_entry_boolean->priv->check = NULL;
	mg_entry_boolean->priv->label_true = NULL;
	mg_entry_boolean->priv->label_false = NULL;
	mg_entry_boolean->priv->label_unset = NULL;
}

/**
 * mg_entry_boolean_new
 * @dh: the data handler to be used by the new widget
 * @type: the requested data type (compatible with @dh)
 *
 * Creates a new widget which is mainly a GtkEntry
 *
 * Returns: the new widget
 */
GtkWidget *
mg_entry_boolean_new (MgDataHandler *dh, GdaValueType type)
{
	GObject *obj;
	MgEntryBoolean *mgbool;

	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (type != GDA_VALUE_TYPE_UNKNOWN, NULL);
	g_return_val_if_fail (mg_data_handler_accepts_gda_type (dh, type), NULL);

	obj = g_object_new (MG_ENTRY_BOOLEAN_TYPE, "handler", dh, NULL);
	mgbool = MG_ENTRY_BOOLEAN (obj);
	mg_data_entry_set_value_type (MG_DATA_ENTRY (mgbool), type);

	return GTK_WIDGET (obj);
}


static void
mg_entry_boolean_dispose (GObject   * object)
{
	MgEntryBoolean *mg_entry_boolean;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_BOOLEAN (object));

	mg_entry_boolean = MG_ENTRY_BOOLEAN (object);
	if (mg_entry_boolean->priv) {

	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_entry_boolean_finalize (GObject   * object)
{
	MgEntryBoolean *mg_entry_boolean;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_BOOLEAN (object));

	mg_entry_boolean = MG_ENTRY_BOOLEAN (object);
	if (mg_entry_boolean->priv) {

		g_free (mg_entry_boolean->priv);
		mg_entry_boolean->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

static void widget_shown_cb (GtkWidget *wid, MgEntryBoolean *mgbool);
static void display_label (MgEntryBoolean *mgbool, GtkWidget *label_to_show);
static GtkWidget *
create_entry (MgEntryWrapper *mgwrap)
{
	GtkWidget *hbox, *cb, *label;
	MgEntryBoolean *mgbool;

	g_return_val_if_fail (mgwrap && IS_MG_ENTRY_BOOLEAN (mgwrap), NULL);
	mgbool = MG_ENTRY_BOOLEAN (mgwrap);
	g_return_val_if_fail (mgbool->priv, NULL);

	hbox = gtk_hbox_new (FALSE, 5);
	mgbool->priv->hbox = hbox;

	cb = gtk_check_button_new ();
	mgbool->priv->check = cb;
	gtk_box_pack_start (GTK_BOX (hbox), cb, FALSE, FALSE, 0);
	gtk_widget_show (cb);

	label = gtk_label_new (_("Yes"));
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	mgbool->priv->label_true = label;
	g_signal_connect_after (G_OBJECT (label), "show", 
				G_CALLBACK (widget_shown_cb), mgwrap);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);

	label = gtk_label_new (_("No"));
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	mgbool->priv->label_false = label;
	g_signal_connect_after (G_OBJECT (label), "show", 
				G_CALLBACK (widget_shown_cb), mgwrap);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);

	label = gtk_label_new (_("Unset"));
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	mgbool->priv->label_unset = label;
	g_signal_connect_after (G_OBJECT (label), "show", 
				G_CALLBACK (widget_shown_cb), mgwrap);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);

	display_label (mgbool, mgbool->priv->label_false);

	return hbox;
}

static void
widget_shown_cb (GtkWidget *wid, MgEntryBoolean *mgbool)
{
	GdaValue *value;
	gboolean isnull, istrue = FALSE;

	value = mg_data_entry_get_value (MG_DATA_ENTRY (mgbool));
	g_assert ((gda_value_get_type (value) == GDA_VALUE_TYPE_BOOLEAN) || 
		  (gda_value_get_type (value) == GDA_VALUE_TYPE_NULL));

	isnull = gda_value_get_type (value) == GDA_VALUE_TYPE_NULL;
	if (!isnull)
		istrue = gda_value_get_boolean (value);

	if ((wid == mgbool->priv->label_true) && (isnull || !istrue))
		gtk_widget_hide (wid);
	if ((wid == mgbool->priv->label_false) && (isnull || istrue))
		gtk_widget_hide (wid);
	if ((wid == mgbool->priv->label_unset) && !isnull)
		gtk_widget_hide (wid);
}

static void
real_set_value (MgEntryWrapper *mgwrap, const GdaValue *value)
{
	MgEntryBoolean *mgbool;

	g_return_if_fail (mgwrap && IS_MG_ENTRY_BOOLEAN (mgwrap));
	mgbool = MG_ENTRY_BOOLEAN (mgwrap);
	g_return_if_fail (mgbool->priv);

	if (value) {
		if (gda_value_is_null (value)) {
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (mgbool->priv->check), FALSE);
			display_label (mgbool, mgbool->priv->label_unset);
		}
		else {
			if (gda_value_get_boolean (value)) {
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (mgbool->priv->check), TRUE);
				display_label (mgbool, mgbool->priv->label_true);
			}
			else {
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (mgbool->priv->check), FALSE);
				display_label (mgbool, mgbool->priv->label_false);
			}
		}
	}
	else {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (mgbool->priv->check), FALSE);
		display_label (mgbool, mgbool->priv->label_unset);
	}
}

static void
display_label (MgEntryBoolean *mgbool, GtkWidget *label_to_show)
{
       if (mgbool->priv->label_true == label_to_show)
	       gtk_widget_show (mgbool->priv->label_true);
       else
	       gtk_widget_hide (mgbool->priv->label_true);

       if (mgbool->priv->label_false == label_to_show)
	       gtk_widget_show (mgbool->priv->label_false);
       else
	       gtk_widget_hide (mgbool->priv->label_false);

       if (mgbool->priv->label_unset == label_to_show)
	       gtk_widget_show (mgbool->priv->label_unset);
       else
	       gtk_widget_hide (mgbool->priv->label_unset);
}

static GdaValue *
real_get_value (MgEntryWrapper *mgwrap)
{
	GdaValue *value;
	MgEntryBoolean *mgbool;
	MgDataHandler *dh;
	const gchar *str;

	g_return_val_if_fail (mgwrap && IS_MG_ENTRY_BOOLEAN (mgwrap), NULL);
	mgbool = MG_ENTRY_BOOLEAN (mgwrap);
	g_return_val_if_fail (mgbool->priv, NULL);

	dh = mg_data_entry_get_handler (MG_DATA_ENTRY (mgwrap));
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (mgbool->priv->check)))
		str = "TRUE";
	else
		str = "FALSE";
	value = mg_data_handler_get_value_from_sql (dh, str, mg_data_entry_get_value_type (MG_DATA_ENTRY (mgwrap)));

	return value;
}

static void check_toggled_cb (GtkToggleButton *toggle, MgEntryBoolean *mgbool);
static void
connect_signals(MgEntryWrapper *mgwrap, GCallback callback)
{
	MgEntryBoolean *mgbool;

	g_return_if_fail (mgwrap && IS_MG_ENTRY_BOOLEAN (mgwrap));
	mgbool = MG_ENTRY_BOOLEAN (mgwrap);
	g_return_if_fail (mgbool->priv);

	g_signal_connect (G_OBJECT (mgbool->priv->check), "toggled",
			  callback, mgwrap);
	g_signal_connect (G_OBJECT (mgbool->priv->check), "toggled",
			  G_CALLBACK (check_toggled_cb), mgwrap);
}

static void
check_toggled_cb (GtkToggleButton *toggle, MgEntryBoolean *mgbool)
{
	if (gtk_toggle_button_get_active (toggle))
		display_label (mgbool, mgbool->priv->label_true);
	else
		display_label (mgbool, mgbool->priv->label_false);
}

static gboolean
expand_in_layout (MgEntryWrapper *mgwrap)
{
	return FALSE;
}
