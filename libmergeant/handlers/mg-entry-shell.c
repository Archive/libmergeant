/* mg-entry-shell.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gdk/gdkkeysyms.h>
#include <config.h>
#include "mg-entry-shell.h"
#include <libmergeant/mg-data-handler.h>
#include "mg-entry-combo.h"

static void mg_entry_shell_class_init (MgEntryShellClass * class);
static void mg_entry_shell_init (MgEntryShell * wid);
static void mg_entry_shell_dispose (GObject   * object);

static void mg_entry_shell_set_property    (GObject              *object,
					    guint                 param_id,
					    const GValue         *value,
					    GParamSpec           *pspec);
static void mg_entry_shell_get_property    (GObject              *object,
					    guint                 param_id,
					    GValue               *value,
					    GParamSpec           *pspec);


static gint event_cb (GtkWidget *widget, GdkEvent *event, MgEntryShell *shell);
static void contents_modified_cb (MgEntryShell *shell, gpointer unused);
static void mg_entry_shell_refresh_status_display (MgEntryShell *shell);


/* signals */
enum
{
	LAST_SIGNAL
};

static gint mg_entry_shell_signals[LAST_SIGNAL] = { };

/* properties */
enum
{
	PROP_0,
	PROP_HANDLER,
	PROP_ACTIONS,
};

struct  _MgEntryShellPriv {
        GtkWidget      *top_box;
        GtkWidget      *button;
        GtkStyle       *orig_style;
        MgDataHandler  *data_handler;
	gboolean        show_actions;

	gboolean        value_is_null;
	gboolean        value_is_modified;
	gboolean        value_is_default;
	gboolean        value_is_non_valid;
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

/**
 * mg_entry_shell_get_type
 * 
 * Register the MgEntryShell class on the GLib type system.
 * 
 * Returns: the GType identifying the class.
 */
guint
mg_entry_shell_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgEntryShellClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_entry_shell_class_init,
			NULL,
			NULL,
			sizeof (MgEntryShell),
			0,
			(GInstanceInitFunc) mg_entry_shell_init
		};		

		type = g_type_register_static (GTK_TYPE_VBOX, "MgEntryShell", &info, 0);
	}
	return type;
}


static void
mg_entry_shell_class_init (MgEntryShellClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_entry_shell_dispose;
	
	/* Properties */
	object_class->set_property = mg_entry_shell_set_property;
	object_class->get_property = mg_entry_shell_get_property;
	g_object_class_install_property (object_class, PROP_HANDLER,
					 g_param_spec_pointer ("handler", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property (object_class, PROP_ACTIONS,
					 g_param_spec_boolean ("actions", NULL, NULL, TRUE,
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static void
mg_entry_shell_init (MgEntryShell * shell)
{
	GtkWidget *button, *hbox, *vbox, *arrow;
	GValue *gval;

	/* Private structure */
	shell->priv = g_new0 (MgEntryShellPriv, 1);
	shell->priv->top_box = NULL;
	shell->priv->button = NULL;
	shell->priv->show_actions = TRUE;
	shell->priv->data_handler = NULL;

	shell->priv->value_is_null = FALSE;
	shell->priv->value_is_modified = FALSE;
	shell->priv->value_is_default = FALSE;
	shell->priv->value_is_non_valid = FALSE;

	/* Setting the initial layout */
	shell->priv->orig_style = gtk_style_copy (gtk_widget_get_style (GTK_WIDGET (shell)));

	/* hbox */
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (shell), hbox, TRUE, TRUE, 0);  
	gtk_widget_show (hbox);

	/* vbox to insert the real widget to edit data */
	vbox = gtk_vbox_new (FALSE, 0); 
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 3); 
	shell->priv->top_box = vbox;
	gtk_widget_show (vbox);

	/* button to change the entry's state and to display that state */
	arrow = gtk_arrow_new (GTK_ARROW_RIGHT, GTK_SHADOW_NONE);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), arrow);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, TRUE, 0);  
	shell->priv->button = button;
	gtk_widget_show_all (button);
	gtk_widget_set_size_request (button, 15, 15);

	g_signal_connect (G_OBJECT (button), "event",
			  G_CALLBACK (event_cb), shell);

	/* focus */
	gval = g_new0 (GValue, 1);
	g_value_init (gval, G_TYPE_BOOLEAN);
	g_value_set_boolean (gval, TRUE);
	g_object_set_property (G_OBJECT (button), "can-focus", gval);
	g_free (gval);
}

static void handler_nullified_cb (MgDataHandler *dh, MgEntryShell *shell);
static void
mg_entry_shell_dispose (GObject   * object)
{
	MgEntryShell *shell;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_SHELL (object));

	shell = MG_ENTRY_SHELL (object);

	if (shell->priv) {
		if (shell->priv->data_handler)
			handler_nullified_cb (shell->priv->data_handler, shell);
		
		g_free (shell->priv);
		shell->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

static void
mg_entry_shell_set_property (GObject              *object,
			     guint                 param_id,
			     const GValue         *value,
			     GParamSpec           *pspec)
{
	gpointer ptr;
	MgEntryShell *shell;

	shell = MG_ENTRY_SHELL (object);
	if (shell->priv) {
		switch (param_id) {
		case PROP_HANDLER:
			ptr = g_value_get_pointer (value);
			if (shell->priv->data_handler)
				handler_nullified_cb (shell->priv->data_handler, shell);
			if (ptr) {
				g_assert (IS_MG_DATA_HANDLER (ptr));
				shell->priv->data_handler = MG_DATA_HANDLER (ptr);
				g_object_ref (G_OBJECT (shell->priv->data_handler));
				g_signal_connect (G_OBJECT (shell->priv->data_handler), "nullified",
						  G_CALLBACK (handler_nullified_cb), shell);
			}
			break;
		case PROP_ACTIONS:
			shell->priv->show_actions = g_value_get_boolean (value);
			if (shell->priv->show_actions) 
				gtk_widget_show (shell->priv->button);
			else
				gtk_widget_hide (shell->priv->button);
			gtk_widget_set_sensitive (shell->priv->top_box, shell->priv->show_actions);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
		}
	}
}
static void
handler_nullified_cb (MgDataHandler *dh, MgEntryShell *shell)
{
	g_signal_handlers_disconnect_by_func (G_OBJECT (shell->priv->data_handler),
					      G_CALLBACK (handler_nullified_cb),
					      shell);
	g_object_unref (G_OBJECT (shell->priv->data_handler));
	shell->priv->data_handler = NULL;
}


static void
mg_entry_shell_get_property (GObject              *object,
			     guint                 param_id,
			     GValue               *value,
			     GParamSpec           *pspec)
{
	MgEntryShell *shell;

	shell = MG_ENTRY_SHELL (object);
	if (shell->priv) {
		switch (param_id) {
		case PROP_HANDLER:
			g_value_set_pointer (value, shell->priv->data_handler);
			break;
		case PROP_ACTIONS:
			g_value_set_boolean (value, shell->priv->show_actions);
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
		}
	}
}


/**
 * mg_entry_shell_pack_entry
 * @shell: a #MgEntryShell object
 * @main_widget: a #GtkWidget to pack into @shell
 *
 * Packs a MgDataEntry widget into the MgEntryShell.
 */
void
mg_entry_shell_pack_entry (MgEntryShell *shell, GtkWidget *main_widget)
{
	g_return_if_fail (shell && IS_MG_ENTRY_SHELL (shell));
	g_return_if_fail (main_widget && GTK_IS_WIDGET (main_widget));

	gtk_box_pack_start (GTK_BOX (shell->priv->top_box), main_widget,
			    mg_data_entry_expand_in_layout (MG_DATA_ENTRY (shell)), 
			    TRUE, 0);

	/* signals */
	g_signal_connect (G_OBJECT (shell), "contents_modified",
			  G_CALLBACK (contents_modified_cb), NULL);

	g_signal_connect (G_OBJECT (shell), "status_changed",
			  G_CALLBACK (contents_modified_cb), NULL);
}

static void
contents_modified_cb (MgEntryShell *shell, gpointer unused)
{
	mg_entry_shell_refresh (shell);
}

static GtkWidget *build_actions_menu (MgEntryShell *shell);
static gint
event_cb (GtkWidget *widget, GdkEvent *event, MgEntryShell *shell)
{
	gboolean done = FALSE;

	if (!shell->priv->show_actions)
		return done;

	if (event->type == GDK_BUTTON_PRESS) {
		GdkEventButton *bevent = (GdkEventButton *) event; 
		if ((bevent->button == 1) || (bevent->button == 3)) {
			GtkWidget *menu;
			
			menu = build_actions_menu (shell);
			gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL,
					bevent->button, bevent->time);
			done = TRUE;
		}
	}

	if (event->type == GDK_KEY_PRESS) {
		GtkWidget *menu;
		GdkEventKey *kevent = (GdkEventKey *) event;

		if (kevent->keyval == GDK_space) {
			menu = build_actions_menu (shell);
			gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL,
					0, kevent->time);
			done = TRUE;
		}
		else {
			if (kevent->keyval == GDK_Tab)
				done = FALSE;
			else
				done = TRUE;
		}
	}
	
	return done;
}

static void mitem_reset_to_orig_activated_cb (GtkWidget *mitem, MgEntryShell *shell);
static void mitem_to_null_activated_cb (GtkWidget *mitem, MgEntryShell *shell);
static void mitem_to_defval_activated_cb (GtkWidget *mitem, MgEntryShell *shell);
static GtkWidget *
build_actions_menu (MgEntryShell *shell)
{
	GtkWidget *menu, *mitem;
	gchar *str;
	gboolean nullact = FALSE;
	gboolean defact = FALSE;
	gboolean reset = FALSE;
	guint attrs;

	menu = gtk_menu_new ();

	/* which menu items to make sensitive ? */
	attrs = mg_data_entry_get_attributes (MG_DATA_ENTRY (shell));
	shell->priv->value_is_null = attrs & MG_DATA_ENTRY_IS_NULL;
	shell->priv->value_is_modified = ! (attrs & MG_DATA_ENTRY_IS_UNCHANGED);
	shell->priv->value_is_default = attrs & MG_DATA_ENTRY_IS_DEFAULT;
	shell->priv->value_is_non_valid = attrs & MG_DATA_ENTRY_DATA_NON_VALID;

	if ((attrs & MG_DATA_ENTRY_CAN_BE_NULL) && 
	    !(attrs & MG_DATA_ENTRY_IS_NULL))
		nullact = TRUE;
	if ((attrs & MG_DATA_ENTRY_CAN_BE_DEFAULT) && 
	    !(attrs & MG_DATA_ENTRY_IS_DEFAULT))
		defact = TRUE;
	if (!(attrs & MG_DATA_ENTRY_IS_UNCHANGED)) {
		/* Dirty hack to make this widget work when it is derived as a MgEntryCombo widget */
		if (IS_MG_ENTRY_COMBO (shell)) {
			GSList *list = mg_entry_combo_get_values_orig (MG_ENTRY_COMBO (shell));
			if (list) {
				reset = TRUE;
				g_slist_free (list);
			}
		}
		else
			if (mg_data_entry_get_value_orig (MG_DATA_ENTRY (shell)))
				reset = TRUE;
	}

	/* set to NULL item */
	str = g_strdup (_("Set to NULL"));
	mitem = gtk_check_menu_item_new_with_label (str);
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (mitem),
					shell->priv->value_is_null);
	gtk_widget_show (mitem);
	g_signal_connect (G_OBJECT (mitem), "activate",
			  G_CALLBACK (mitem_to_null_activated_cb), shell);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), mitem);
	g_free (str);
	gtk_widget_set_sensitive (mitem, nullact);

	/* default value item */
	str = g_strdup (_("Set to default value"));
	mitem = gtk_check_menu_item_new_with_label (str);
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (mitem), 
					shell->priv->value_is_default);
	gtk_widget_show (mitem);
	g_signal_connect (G_OBJECT (mitem), "activate",
			  G_CALLBACK (mitem_to_defval_activated_cb), shell);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), mitem);
	g_free (str);
	gtk_widget_set_sensitive (mitem, defact);
		
	/* reset to original value item */
	str = g_strdup (_("Reset to original value"));
	mitem = gtk_check_menu_item_new_with_label (str);
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (mitem), 
					!shell->priv->value_is_modified);
	gtk_widget_show (mitem);
	g_signal_connect (G_OBJECT (mitem), "activate",
			  G_CALLBACK (mitem_reset_to_orig_activated_cb), shell);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), mitem);
	g_free (str);
	gtk_widget_set_sensitive (mitem, reset);

	return menu;
}

static void 
mitem_reset_to_orig_activated_cb (GtkWidget *mitem, MgEntryShell *shell)
{
	mg_data_entry_set_attributes (MG_DATA_ENTRY (shell), MG_DATA_ENTRY_IS_UNCHANGED,
				      MG_DATA_ENTRY_IS_UNCHANGED);
}

static void 
mitem_to_null_activated_cb (GtkWidget *mitem, MgEntryShell *shell)
{
	mg_data_entry_set_attributes (MG_DATA_ENTRY (shell), MG_DATA_ENTRY_IS_NULL,
				      MG_DATA_ENTRY_IS_NULL);
}

static void
mitem_to_defval_activated_cb (GtkWidget *mitem, MgEntryShell *shell)
{
	mg_data_entry_set_attributes (MG_DATA_ENTRY (shell), MG_DATA_ENTRY_IS_DEFAULT,
				      MG_DATA_ENTRY_IS_DEFAULT);
}

static void 
mg_entry_shell_refresh_status_display (MgEntryShell *shell)
{
	static GdkColor **colors = NULL;
	GdkColor *normal = NULL, *prelight = NULL;
	GdkColor *orig_normal, *orig_prelight;

	g_return_if_fail (shell && IS_MG_ENTRY_SHELL (shell));

	orig_normal = & (shell->priv->orig_style->bg[GTK_STATE_NORMAL]);
	orig_prelight = & (shell->priv->orig_style->bg[GTK_STATE_PRELIGHT]);

	if (!colors) {
		GdkColor *color;

		colors = g_new0 (GdkColor *, 6);

		/* Green color */
		color = g_new0 (GdkColor, 1);
		gdk_color_parse ("#00cd66", color);
		if (!gdk_colormap_alloc_color (gtk_widget_get_default_colormap (), color, FALSE, TRUE)) {
			g_free (color);
			color = NULL;
		}
		colors[0] = color;

		color = g_new0 (GdkColor, 1);
		gdk_color_parse ("#00ef77", color);
		if (!gdk_colormap_alloc_color (gtk_widget_get_default_colormap (), color, FALSE, TRUE)) {
			g_free (color);
			color = NULL;
		}
		colors[1] = color;


		/* Blue color */
		color = g_new0 (GdkColor, 1);
		gdk_color_parse ("#6495ed", color);
		if (!gdk_colormap_alloc_color (gtk_widget_get_default_colormap (), color, FALSE, TRUE)) {
			g_free (color);
			color = NULL;
		}
		colors[2] = color;

		color = g_new0 (GdkColor, 1);
		gdk_color_parse ("#75a6fe", color);
		if (!gdk_colormap_alloc_color (gtk_widget_get_default_colormap (), color, FALSE, TRUE)) {
			g_free (color);
			color = NULL;
		}
		colors[3] = color;


		/* Red color */
		color = g_new0 (GdkColor, 1);
		gdk_color_parse ("#ff6a6a", color);
		if (!gdk_colormap_alloc_color (gtk_widget_get_default_colormap (), color, FALSE, TRUE)) {
			g_free (color);
			color = NULL;
		}
		colors[4] = color;

		color = g_new0 (GdkColor, 1);
		gdk_color_parse ("#ff7b7b", color);
		if (!gdk_colormap_alloc_color (gtk_widget_get_default_colormap (), color, FALSE, TRUE)) {
			g_free (color);
			color = NULL;
		}
		colors[5] = color;		
	}

	if (shell->priv->value_is_null) {
		normal = colors[0];
		prelight = colors[1];
	}

	if (shell->priv->value_is_default) {
		normal = colors[2];
		prelight = colors[3];
	}

	if (shell->priv->value_is_non_valid) {
		normal = colors[4];
		prelight = colors[5];
	}

	if (!normal)
		normal = orig_normal;
	if (!prelight)
		prelight = orig_prelight;

	gtk_widget_modify_bg (shell->priv->button, GTK_STATE_NORMAL, normal);
	gtk_widget_modify_bg (shell->priv->button, GTK_STATE_ACTIVE, normal);
	gtk_widget_modify_bg (shell->priv->button, GTK_STATE_PRELIGHT, prelight);
}

/**
 * mg_entry_shell_refresh
 * @shell: the MgEntryShell widget to refresh
 *
 * Forces the shell to refresh its display (mainly the color of the
 * button).
 */
void
mg_entry_shell_refresh (MgEntryShell *shell)
{
	guint attrs;
	g_return_if_fail (shell && IS_MG_ENTRY_SHELL (shell));

	attrs = mg_data_entry_get_attributes (MG_DATA_ENTRY (shell));
	shell->priv->value_is_null = attrs & MG_DATA_ENTRY_IS_NULL;
	shell->priv->value_is_modified = ! (attrs & MG_DATA_ENTRY_IS_UNCHANGED);
	shell->priv->value_is_default = attrs & MG_DATA_ENTRY_IS_DEFAULT;
	shell->priv->value_is_non_valid = attrs & MG_DATA_ENTRY_DATA_NON_VALID;

	mg_entry_shell_refresh_status_display (shell);
}
