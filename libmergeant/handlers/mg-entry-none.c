/* mg-entry-none.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-entry-none.h"
#include <libmergeant/mg-data-handler.h>

/* 
 * Main static functions 
 */
static void mg_entry_none_class_init (MgEntryNoneClass * class);
static void mg_entry_none_init (MgEntryNone * srv);
static void mg_entry_none_dispose (GObject   * object);
static void mg_entry_none_finalize (GObject   * object);

/* virtual functions */
static GtkWidget *create_entry (MgEntryWrapper *mgwrap);
static void       real_set_value (MgEntryWrapper *mgwrap, const GdaValue *value);
static GdaValue  *real_get_value (MgEntryWrapper *mgwrap);
static void       connect_signals(MgEntryWrapper *mgwrap, GCallback callback);
static gboolean   expand_in_layout (MgEntryWrapper *mgwrap);


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* private structure */
struct _MgEntryNonePrivate
{
	gboolean dummy;
};


guint
mg_entry_none_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgEntryNoneClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_entry_none_class_init,
			NULL,
			NULL,
			sizeof (MgEntryNone),
			0,
			(GInstanceInitFunc) mg_entry_none_init
		};
		
		type = g_type_register_static (MG_ENTRY_WRAPPER_TYPE, "MgEntryNone", &info, 0);
	}
	return type;
}

static void
mg_entry_none_class_init (MgEntryNoneClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_entry_none_dispose;
	object_class->finalize = mg_entry_none_finalize;

	MG_ENTRY_WRAPPER_CLASS (class)->create_entry = create_entry;
	MG_ENTRY_WRAPPER_CLASS (class)->real_set_value = real_set_value;
	MG_ENTRY_WRAPPER_CLASS (class)->real_get_value = real_get_value;
	MG_ENTRY_WRAPPER_CLASS (class)->connect_signals = connect_signals;
	MG_ENTRY_WRAPPER_CLASS (class)->expand_in_layout = expand_in_layout;
}

static void
mg_entry_none_init (MgEntryNone * mg_entry_none)
{
	mg_entry_none->priv = g_new0 (MgEntryNonePrivate, 1);
}

/**
 * mg_entry_none_new
 * @dh: the data handler to be used by the new widget
 * @type: the requested data type (compatible with @dh)
 *
 * Creates a new widget which is mainly a GtkEntry
 *
 * Returns: the new widget
 */
GtkWidget *
mg_entry_none_new (MgDataHandler *dh, GdaValueType type)
{
	GObject *obj;
	MgEntryNone *mgnone;

	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (type != GDA_VALUE_TYPE_UNKNOWN, NULL);
	g_return_val_if_fail (mg_data_handler_accepts_gda_type (dh, type), NULL);

	obj = g_object_new (MG_ENTRY_NONE_TYPE, "handler", dh, NULL);
	mgnone = MG_ENTRY_NONE (obj);
	mg_data_entry_set_value_type (MG_DATA_ENTRY (mgnone), type);

	return GTK_WIDGET (obj);
}


static void
mg_entry_none_dispose (GObject   * object)
{
	MgEntryNone *mg_entry_none;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_NONE (object));

	mg_entry_none = MG_ENTRY_NONE (object);
	if (mg_entry_none->priv) {

	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_entry_none_finalize (GObject   * object)
{
	MgEntryNone *mg_entry_none;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_ENTRY_NONE (object));

	mg_entry_none = MG_ENTRY_NONE (object);
	if (mg_entry_none->priv) {

		g_free (mg_entry_none->priv);
		mg_entry_none->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

static GtkWidget *
create_entry (MgEntryWrapper *mgwrap)
{
	GtkWidget *frame, *label;
	MgEntryNone *mgnone;

	g_return_val_if_fail (mgwrap && IS_MG_ENTRY_NONE (mgwrap), NULL);
	mgnone = MG_ENTRY_NONE (mgwrap);
	g_return_val_if_fail (mgnone->priv, NULL);

	frame = gtk_frame_new (NULL);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 5);
	
	label = gtk_label_new (_("This data cannot be displayed or edited\n"
				 "because of its data type, a plugin needs to be\n"
				 "affected to that data type (or develop if none exists!)"));
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_CENTER);

	gtk_container_add (GTK_CONTAINER (frame), label);
	gtk_widget_show (label);

	return frame;
}

static void
real_set_value (MgEntryWrapper *mgwrap, const GdaValue *value)
{
	MgEntryNone *mgnone;

	g_return_if_fail (mgwrap && IS_MG_ENTRY_NONE (mgwrap));
	mgnone = MG_ENTRY_NONE (mgwrap);
	g_return_if_fail (mgnone->priv);
}

static GdaValue *
real_get_value (MgEntryWrapper *mgwrap)
{
	MgEntryNone *mgnone;

	g_return_val_if_fail (mgwrap && IS_MG_ENTRY_NONE (mgwrap), NULL);
	mgnone = MG_ENTRY_NONE (mgwrap);
	g_return_val_if_fail (mgnone->priv, NULL);

	return gda_value_new_null ();
}

static void
connect_signals(MgEntryWrapper *mgwrap, GCallback callback)
{
	MgEntryNone *mgnone;

	g_return_if_fail (mgwrap && IS_MG_ENTRY_NONE (mgwrap));
	mgnone = MG_ENTRY_NONE (mgwrap);
	g_return_if_fail (mgnone->priv);
}

static gboolean
expand_in_layout (MgEntryWrapper *mgwrap)
{
	return FALSE;
}
