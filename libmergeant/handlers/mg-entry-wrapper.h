/* mg-entry-wrapper.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __MG_ENTRY_WRAPPER__
#define __MG_ENTRY_WRAPPER__

#include <gtk/gtk.h>
#include "mg-entry-shell.h"
#include <libmergeant/mg-data-entry.h>

G_BEGIN_DECLS

#define MG_ENTRY_WRAPPER_TYPE          (mg_entry_wrapper_get_type())
#define MG_ENTRY_WRAPPER(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_entry_wrapper_get_type(), MgEntryWrapper)
#define MG_ENTRY_WRAPPER_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_entry_wrapper_get_type (), MgEntryWrapperClass)
#define IS_MG_ENTRY_WRAPPER(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_entry_wrapper_get_type ())


typedef struct _MgEntryWrapper      MgEntryWrapper;
typedef struct _MgEntryWrapperClass MgEntryWrapperClass;
typedef struct _MgEntryWrapperPriv  MgEntryWrapperPriv;


/* struct for the object's data */
struct _MgEntryWrapper
{
	MgEntryShell        object;

	MgEntryWrapperPriv  *priv;
};

/* struct for the object's class */
struct _MgEntryWrapperClass
{
	MgEntryShellClass   parent_class;

	/* pure virtual functions */
	GtkWidget        *(*create_entry)     (MgEntryWrapper *mgwrp);
	void              (*real_set_value)   (MgEntryWrapper *mgwrp, const GdaValue *value);
	GdaValue         *(*real_get_value)   (MgEntryWrapper *mgwrp);
	void              (*connect_signals)  (MgEntryWrapper *mgwrp, GCallback callback);
	gboolean          (*expand_in_layout) (MgEntryWrapper *mgwrp);
};


guint           mg_entry_wrapper_get_type      (void);

G_END_DECLS

#endif
