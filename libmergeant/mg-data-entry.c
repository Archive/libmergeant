/* mg-data-entry.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-data-entry.h"
#include "marshal.h"


/* Description:
 *
 * This interface is used to access any widget which implements basic data editing (usually an editing
 * area and a button to have some more control on the value being edited).
 *
 * The interface allows to control how the widget works and to query the value and the attributes
 * of the data held by the widget.
 *
 * The widget can store the original value (to be able to tell if the value has been changed
 * by the user) and a default value (which will be returned if the user explicitely forces the widget
 * to be set to the default value).
 *
 * Control methods allow to set the type of value to be edited (the requested type must be 
 * compatible with what the widget can handle), set the value (which replaces the currently edited
 * value), set the value and the original value (the value passed as argument is set and is also
 * considered to be the original value).
 */

/* signals */
enum
{
	CONTENTS_MODIFIED,
	STATUS_CHANGED,
	LAST_SIGNAL
};

static gint mg_data_entry_signals[LAST_SIGNAL] = { 0 };
static void mg_data_entry_iface_init (gpointer g_class);

guint
mg_data_entry_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgDataEntryIface),
			(GBaseInitFunc) mg_data_entry_iface_init,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) NULL,
			NULL,
			NULL,
			0,
			0,
			(GInstanceInitFunc) NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, "MgDataEntry", &info, 0);
	}
	return type;
}


static void
mg_data_entry_iface_init (gpointer g_class)
{
	static gboolean initialized = FALSE;

	if (! initialized) {
		mg_data_entry_signals[CONTENTS_MODIFIED] =
			g_signal_new ("contents_modified",
				      MG_DATA_ENTRY_TYPE,
				      G_SIGNAL_RUN_FIRST,
				      G_STRUCT_OFFSET (MgDataEntryIface, contents_modified),
				      NULL, NULL,
				      marshal_VOID__VOID,
				      G_TYPE_NONE, 0);
		mg_data_entry_signals[STATUS_CHANGED] =
			g_signal_new ("status_changed",
				      MG_DATA_ENTRY_TYPE,
				      G_SIGNAL_RUN_FIRST,
				      G_STRUCT_OFFSET (MgDataEntryIface, status_changed),
				      NULL, NULL,
				      marshal_VOID__VOID,
				      G_TYPE_NONE, 0);

		initialized = TRUE;
	}
}

/**
 * mg_data_entry_set_value_type
 * @de: a #GtkWidget object which implements the #MgDataEntry interface
 * @type: 
 *
 * Sets the type of value the MgDataEntry will handle. The type must be compatible with what
 * the widget can handle.
 */
void
mg_data_entry_set_value_type (MgDataEntry *de, GdaValueType type)
{
	g_return_if_fail (de && IS_MG_DATA_ENTRY (de));
	g_return_if_fail (type != GDA_VALUE_TYPE_UNKNOWN);

	if (MG_DATA_ENTRY_GET_IFACE (de)->set_value_type)
		(MG_DATA_ENTRY_GET_IFACE (de)->set_value_type) (de, type);
}


/**
 * mg_data_entry_get_value_type
 * @de: a #GtkWidget object which implements the #MgDataEntry interface
 *
 * Fetch the type of data the MgDataEntry handles
 *
 * Returns: the GdaValueType type
 */
GdaValueType
mg_data_entry_get_value_type (MgDataEntry *de)
{
	g_return_val_if_fail (de && IS_MG_DATA_ENTRY (de), GDA_VALUE_TYPE_UNKNOWN);

	if (MG_DATA_ENTRY_GET_IFACE (de)->get_value_type)
		return (MG_DATA_ENTRY_GET_IFACE (de)->get_value_type) (de);
	return GDA_VALUE_TYPE_UNKNOWN;
}


/**
 * mg_data_entry_set_value
 * @de: a #GtkWidget object which implements the #MgDataEntry interface
 * @value: 
 *
 * Push a value into the MgDataEntry. The value parameter must either be:
 * - NULL or of type GDA_VALUE_TYPE_NULL, or
 * - of type specified using mg_data_entry_set_value_type.
 */
void
mg_data_entry_set_value (MgDataEntry *de, const GdaValue * value)
{
	g_return_if_fail (de && IS_MG_DATA_ENTRY (de));

	if (MG_DATA_ENTRY_GET_IFACE (de)->set_value)
		(MG_DATA_ENTRY_GET_IFACE (de)->set_value) (de, value);
}

/**
 * mg_data_entry_get_value
 * @de: a #GtkWidget object which implements the #MgDataEntry interface
 *
 * Fetch the value held in the MgDataEntry widget. If the value is set to NULL,
 * the returned value is of type GDA_VALUE_TYPE_NULL. If the value is set to default,
 * then the returned value is of type GDA_VALUE_TYPE_NULL or is the default value if it
 * has been provided to the widget (and is of the same type as the one provided by @de).
 *
 * Returns: a new GdaValue
 */
GdaValue *
mg_data_entry_get_value (MgDataEntry *de)
{
	g_return_val_if_fail (de && IS_MG_DATA_ENTRY (de), NULL);

	if (MG_DATA_ENTRY_GET_IFACE (de)->get_value)
		return (MG_DATA_ENTRY_GET_IFACE (de)->get_value) (de);

	return NULL;
}


/**
 * mg_data_entry_set_value_orig
 * @de: a #GtkWidget object which implements the #MgDataEntry interface
 * @value: 
 *
 * Push a value into the MgDataEntry in the same way as mg_data_entry_set_value but
 * also sets this value as the original value.
 */
void
mg_data_entry_set_value_orig (MgDataEntry *de, const GdaValue *value)
{
	g_return_if_fail (de && IS_MG_DATA_ENTRY (de));

	if (MG_DATA_ENTRY_GET_IFACE (de)->set_value_orig)
		(MG_DATA_ENTRY_GET_IFACE (de)->set_value_orig) (de, value);
}

/**
 * mg_data_entry_get_value_orig
 * @de: a #GtkWidget object which implements the #MgDataEntry interface
 *
 * Fetch the original value held in the MgDataEntry widget
 *
 * Returns: the GdaValue
 */
const GdaValue *
mg_data_entry_get_value_orig (MgDataEntry *de)
{
	g_return_val_if_fail (de && IS_MG_DATA_ENTRY (de), NULL);

	if (MG_DATA_ENTRY_GET_IFACE (de)->get_value_orig)
		return (MG_DATA_ENTRY_GET_IFACE (de)->get_value_orig) (de);

	return NULL;
}


/**
 * mg_data_entry_set_value_default
 * @de: a #GtkWidget object which implements the #MgDataEntry interface
 * @value: 
 *
 * Sets the default value for the MgDataEntry which gets displayed when the
 * user forces the default value. If it is not set then it is set to type GDA_VALUE_TYPE_NULL.
 * The value parameter must either be:
 * - NULL or of type GDA_VALUE_TYPE_NULL, or
 * - of type specified using mg_data_entry_set_value_type.
 */
void
mg_data_entry_set_value_default (MgDataEntry *de, const GdaValue *value)
{
	g_return_if_fail (de && IS_MG_DATA_ENTRY (de));
	g_return_if_fail (value);

	if (MG_DATA_ENTRY_GET_IFACE (de)->set_value_default)
		(MG_DATA_ENTRY_GET_IFACE (de)->set_value_default) (de, value);
}

/**
 * mg_data_entry_set_attributes
 * @de: a #GtkWidget object which implements the #MgDataEntry interface
 * @attrs: the attributes to set (OR'ed between them)
 * @mask: the mask corresponding to the considered attributes
 *
 * Sets the parameters of the MgDataEntry. Only the attributes corresponding to the
 * mask are set, the other ones are ignored.
 */
void
mg_data_entry_set_attributes (MgDataEntry *de, guint attrs, guint mask)
{
	g_return_if_fail (de && IS_MG_DATA_ENTRY (de));

	if (MG_DATA_ENTRY_GET_IFACE (de)->set_attributes)
		(MG_DATA_ENTRY_GET_IFACE (de)->set_attributes) (de, attrs, mask);
}

/**
 * mg_data_entry_get_attributes
 * @de: a #GtkWidget object which implements the #MgDataEntry interface
 *
 * Retreives the parameters of the MgDataEntry widget.
 *
 * Returns: the OR'ed bits corresponding to the attributes.
 */
guint
mg_data_entry_get_attributes (MgDataEntry *de)
{
	g_return_val_if_fail (de && IS_MG_DATA_ENTRY (de), 0);

	if (MG_DATA_ENTRY_GET_IFACE (de)->get_attributes)
		return (MG_DATA_ENTRY_GET_IFACE (de)->get_attributes) (de);

	return 0;
}


/**
 * mg_data_entry_get_handler
 * @de: a #GtkWidget object which implements the #MgDataEntry interface
 * 
 * Fetch the MgDataHandler the MgDataEntry is using
 *
 * Returns: the MgDataHandler object
 */
MgDataHandler  *
mg_data_entry_get_handler (MgDataEntry *de)
{
	g_return_val_if_fail (de && IS_MG_DATA_ENTRY (de), NULL);

	if (MG_DATA_ENTRY_GET_IFACE (de)->get_handler)
		return (MG_DATA_ENTRY_GET_IFACE (de)->get_handler) (de);

	return NULL;
}

/**
 * mg_data_entry_expand_in_layout
 * @de: a #GtkWidget object which implements the #MgDataEntry interface
 *
 * Used for the layout of the widget in containers.
 *
 * Returns: TRUE if the widget should expand
 */
gboolean
mg_data_entry_expand_in_layout (MgDataEntry *de)
{
	g_return_val_if_fail (de && IS_MG_DATA_ENTRY (de), FALSE);

	if (MG_DATA_ENTRY_GET_IFACE (de)->expand_in_layout)
		(MG_DATA_ENTRY_GET_IFACE (de)->expand_in_layout) (de);

	return FALSE;
}

