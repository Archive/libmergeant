/* mg-context.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_CONTEXT_H_
#define __MG_CONTEXT_H_

#include "mg-base.h"
#include "mg-defs.h"
#include <libgda/libgda.h>

G_BEGIN_DECLS

#define MG_CONTEXT_TYPE          (mg_context_get_type())
#define MG_CONTEXT(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_context_get_type(), MgContext)
#define MG_CONTEXT_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_context_get_type (), MgContextClass)
#define IS_MG_CONTEXT(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_context_get_type ())


/* Interfaces:
 * MgRerefer
 */

/* error reporting */
extern GQuark mg_context_error_quark (void);
#define MG_CONTEXT_ERROR mg_context_error_quark ()


/* use either 'param' or 'query' and 'params' */
struct _MgContextNode {
	/* this MgContextNode is only for one parameter, free fill */
        MgParameter *param; 

	/* this MgContextNode is for one or mode parameters, listed in 'params' and the params values
	   depend on the result of the execution of 'query' */
        MgQuery     *query; 
        GSList      *params;
};

#define MG_CONTEXT_NODE(x) ((MgContextNode *)x)

/* struct for the object's data */
struct _MgContext
{
	MgBase               object;
	GSList              *parameters;
        GSList              *nodes;
	MgContextPrivate    *priv;
};

/* struct for the object's class */
struct _MgContextClass
{
	MgBaseClass                    class;
};

guint           mg_context_get_type                 (void);
GObject        *mg_context_new                      (MgConf *conf, GSList *params);
GObject        *mg_context_new_copy                 (MgContext *orig);

gboolean        mg_context_is_coherent              (MgContext *context, GError **error);
gboolean        mg_context_is_valid                 (MgContext *context);
gboolean        mg_context_needs_user_input         (MgContext *context);

void            mg_context_free_node                (MgContext *context, MgContextNode *node);

MgParameter    *mg_context_find_parameter_for_field (MgContext *context, MgQfield *for_field);

G_END_DECLS

#endif
