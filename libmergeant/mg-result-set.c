/* mg-result-set.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-result-set.h"
#include "marshal.h"
#include "mg-server.h"

/* 
 * Main static functions 
 */
static void mg_resultset_class_init (MgResultSetClass * class);
static void mg_resultset_init (MgResultSet * srv);
static void mg_resultset_dispose (GObject   * object);
static void mg_resultset_finalize (GObject   * object);

static void mg_resultset_set_property (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec);
static void mg_resultset_get_property (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec);
#ifdef debug
static void mg_resultset_dump (MgResultSet *rs, guint offset);
#endif

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	DUMMY,
	LAST_SIGNAL
};

static gint mg_resultset_signals[LAST_SIGNAL] = { 0 };

/* properties */
enum
{
	PROP_0,
	PROP
};


/* private structure */
struct _MgResultSetPrivate
{
	MgServer     *srv;
	GdaCommand   *cmd;
	GdaDataModel *model;
};


/* module error */
GQuark mg_resultset_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_resultset_error");
	return quark;
}


guint
mg_resultset_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgResultSetClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_resultset_class_init,
			NULL,
			NULL,
			sizeof (MgResultSet),
			0,
			(GInstanceInitFunc) mg_resultset_init
		};
		
		type = g_type_register_static (MG_BASE_TYPE, "MgResultSet", &info, 0);
	}
	return type;
}

static void
mg_resultset_class_init (MgResultSetClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_resultset_signals[DUMMY] =
		g_signal_new ("dummy",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgResultSetClass, dummy),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	class->dummy = NULL;

	object_class->dispose = mg_resultset_dispose;
	object_class->finalize = mg_resultset_finalize;

	/* Properties */
	object_class->set_property = mg_resultset_set_property;
	object_class->get_property = mg_resultset_get_property;
	g_object_class_install_property (object_class, PROP,
					 g_param_spec_pointer ("prop", NULL, NULL, (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_resultset_dump;
#endif
}

static void
mg_resultset_init (MgResultSet * rs)
{
	rs->priv = g_new0 (MgResultSetPrivate, 1);
	rs->priv->srv = NULL;
	rs->priv->cmd = NULL;
	rs->priv->model = NULL;
}

static void conn_closed_cb (MgServer *srv, MgResultSet *rs);

/**
 * mg_resultset_new
 * @srv: a #MgServer object
 * @cmd: the #GdaCommand which returned a result (stored in @model)
 * @model: the #GdaDataModel result
 *
 * Creates a new #MgResultSet object
 *
 * Returns: the new object
 */
GObject   *
mg_resultset_new (MgServer *srv, GdaCommand *cmd, GdaDataModel *model)
{
	GObject   *obj;
	MgResultSet *rs;
	
	g_return_val_if_fail (srv && IS_MG_SERVER (srv), NULL);
	g_return_val_if_fail (cmd, NULL);
	g_return_val_if_fail (model && GDA_IS_DATA_MODEL (model), NULL);

	obj = g_object_new (MG_RESULTSET_TYPE, "conf", mg_server_get_conf (srv), NULL);
	rs = MG_RESULTSET (obj);

	rs->priv->srv = srv;
	rs->priv->cmd = cmd;
	rs->priv->model = model;

	g_object_ref (G_OBJECT (model));

	g_signal_connect (G_OBJECT (srv), "conn_closed",
			  G_CALLBACK (conn_closed_cb), rs);

	return obj;
}

static void
conn_closed_cb (MgServer *srv, MgResultSet *rs)
{
	mg_base_nullify (MG_BASE (rs));
}


static void
mg_resultset_dispose (GObject *object)
{
	MgResultSet *rs;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_RESULTSET (object));

	rs = MG_RESULTSET (object);
	if (rs->priv) {
		mg_base_nullify_check (MG_BASE (object));

		g_signal_handlers_disconnect_by_func (G_OBJECT (rs->priv->srv),
						      G_CALLBACK (conn_closed_cb), rs);
		g_object_unref (G_OBJECT (rs->priv->model));
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_resultset_finalize (GObject   * object)
{
	MgResultSet *rs;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_RESULTSET (object));

	rs = MG_RESULTSET (object);
	if (rs->priv) {
		gda_command_free (rs->priv->cmd);

		g_free (rs->priv);
		rs->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_resultset_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	gpointer ptr;
	MgResultSet *rs;

	rs = MG_RESULTSET (object);
	if (rs->priv) {
		switch (param_id) {
		case PROP:
			/* FIXME */
			ptr = g_value_get_pointer (value);
			break;
		}
	}
}

static void
mg_resultset_get_property (GObject              *object,
			guint                 param_id,
			GValue               *value,
			GParamSpec           *pspec)
{
	MgResultSet *rs;
	rs = MG_RESULTSET (object);
	
	if (rs->priv) {
		switch (param_id) {
		case PROP:
			/* FIXME */
			g_value_set_pointer (value, NULL);
			break;
		}	
	}
}

#ifdef debug
static void
mg_resultset_dump (MgResultSet *rs, guint offset)
{
	gint cols, rows;
        gint c, r;
	GdaDataModel *model;

	g_return_if_fail (rs && IS_MG_RESULTSET (rs));
	g_return_if_fail (rs->priv);
	g_return_if_fail (rs->priv->model);

	model = rs->priv->model;
        cols = gda_data_model_get_n_columns (model);
        rows = gda_data_model_get_n_rows (model);

        g_print ("Displaying recordset %p, with %d columns and %d rows:\n",
                 model, cols, rows);
	
        for (r = 0; r < rows; r++) {
                g_print ("\tRow %02d -------\n", r);
                for (c = 0; c < cols; c++) {
                        const GdaValue *value;
                        gchar *strvalue;
                        const gchar *colname;

                        colname = gda_data_model_get_column_title (model, c);
                        value = gda_data_model_get_value_at (model, c, r);
                        strvalue = gda_value_stringify (value);
                        g_print ("\t\tColumn %02d name: %s\n", c, colname);
                        g_print ("\t\t         value: ('%s')\n", strvalue);
                        g_free (strvalue);
                }
        }
}
#endif


/**
 * mg_resultset_get_nbtuples
 * @rs: a #MgResultSet object
 *
 * Fetch the number of tuples (rows) in the resultset
 *
 * Returns: the number of tuples
 */
gint
mg_resultset_get_nbtuples (MgResultSet * rs)
{
	g_return_val_if_fail (rs && IS_MG_RESULTSET (rs), -1);
	g_return_val_if_fail (rs->priv, -1);
	g_return_val_if_fail (rs->priv->model, -1);

	return gda_data_model_get_n_rows (rs->priv->model);
}

/**
 * mg_resultset_get_nbcols
 * @rs: a #MgResultSet object
 *
 * Fetch the number of columns of a resultset
 *
 * Returns: the number of columns
 */
gint
mg_resultset_get_nbcols (MgResultSet * rs)
{
	g_return_val_if_fail (rs && IS_MG_RESULTSET (rs), -1);
	g_return_val_if_fail (rs->priv, -1);
	g_return_val_if_fail (rs->priv->model, -1);

	return gda_data_model_get_n_columns (rs->priv->model);
}


/**
 * mg_resultset_get_item
 * @rs: a #MgResultSet object
 * @row: 
 * @col: 
 *
 * Get a textual version of an item in the resultset, situated at (row, col).
 *
 * Returns: a new string.
 */
gchar *
mg_resultset_get_item (MgResultSet * rs, gint row, gint col)
{
	const GdaValue *value;

	g_return_val_if_fail (rs && IS_MG_RESULTSET (rs), NULL);
	g_return_val_if_fail (rs->priv, NULL);
	g_return_val_if_fail (rs->priv->model, NULL);

	g_return_val_if_fail ((row >= 0) && (row < gda_data_model_get_n_rows (rs->priv->model)),
			      NULL);
	g_return_val_if_fail ((col >= 0) && (col < gda_data_model_get_n_columns (rs->priv->model)),
			      NULL);

	value = gda_data_model_get_value_at (rs->priv->model, col, row);
	return gda_value_stringify (value);
}

/**
 * mg_resultset_get_gdavalue
 * @rs: a #MgResultSet object
 * @row: 
 * @col: 
 *
 * Get an item in the resultset, situated at (row, col) as a GdaValue.
 *
 * Returns: the item as a GdaValue
 */
const GdaValue *
mg_resultset_get_gdavalue (MgResultSet * rs, gint row, gint col)
{
	const GdaValue *value;

	g_return_val_if_fail (rs && IS_MG_RESULTSET (rs), NULL);
	g_return_val_if_fail (rs->priv, NULL);
	g_return_val_if_fail (rs->priv->model, NULL);

	g_return_val_if_fail ((row >= 0) && (row < gda_data_model_get_n_rows (rs->priv->model)),
			      NULL);
	g_return_val_if_fail ((col >= 0) && (col < gda_data_model_get_n_columns (rs->priv->model)),
			      NULL);

	value = gda_data_model_get_value_at (rs->priv->model, col, row);
	return value;
}

/**
 * mg_resultset_get_col_name
 * @rs: a #MgResultSet object
 * @col: 
 *
 * Get a column name from a resultset.
 *
 * Returns: the column's name
 */
const gchar *
mg_resultset_get_col_name (MgResultSet *rs, gint col)
{
	const gchar *str;
	g_return_val_if_fail (rs && IS_MG_RESULTSET (rs), NULL);
	g_return_val_if_fail (rs->priv, NULL);
	g_return_val_if_fail (rs->priv->model, NULL);

	g_return_val_if_fail ((col >= 0) && (col < gda_data_model_get_n_columns (rs->priv->model)),
			      NULL);

	str = gda_data_model_get_column_title (rs->priv->model, col);
	return str;
}

/**
 * mg_resultset_get_data_model
 * @rs: a #MgResultSet object
 *
 * Get @rs's associated #GdaDataModel
 *
 * Returns: the #GdaDataModel
 */
GdaDataModel *
mg_resultset_get_data_model (MgResultSet * rs)
{
	g_return_val_if_fail (rs && IS_MG_RESULTSET (rs), NULL);
	g_return_val_if_fail (rs->priv, NULL);

	return rs->priv->model;
}


/**
 * mg_resultset_check_data_model
 * @model: a #GdaDataModel object
 * @nbcols: the requested number of columns
 * @Varargs: @nbcols arguments of type GdaValueType or -1 (if any data type is accepted)
 *
 * Check the column types of a GdaDataModel.
 *
 * Returns: TRUE if the data model's columns match the provided data types and number
 */
gboolean
mg_resultset_check_data_model (GdaDataModel *model, gint nbcols, ...)
{
	gboolean retval = TRUE;
	gint i;

	g_return_val_if_fail (model && GDA_IS_DATA_MODEL (model), FALSE);
	
	/* number of columns */
	if (gda_data_model_get_n_columns (model) != nbcols)
		return FALSE;

	/* type of each column */
	if (nbcols > 0) {
		GdaFieldAttributes *att;
		GdaValueType mtype, rtype;
		gint argtype;
		va_list ap;

		va_start  (ap, nbcols);
		i = 0;
		while ((i<nbcols) && retval) {
			att = gda_data_model_describe_column (model, i);
			mtype = gda_field_attributes_get_gdatype (att);
			gda_field_attributes_free (att);
			
			argtype = va_arg (ap, GdaValueType);
			if (argtype >= 0) {
				rtype = (GdaValueType) argtype;
				if (mtype != rtype) {
					retval = FALSE;
#ifdef debug
					g_print ("Position %d: Expected %d, got %d\n",
						 i, rtype, mtype);
#endif
				}
			}
			
			i++;
		}
		va_end (ap);
	}

	return retval;

}

/** 
 * mg_resultset_check_model
 * @rs: a #MgResultSet object
 * @nbcols: the requested number of columns
 * @Varargs: @nbcols arguments of type GdaValueType or -1 (if any data type is accepted)
 * 
 * Check the column types of a resultset. 
 *
 * Returns: TRUE if the resultset's columns match the provided data types and number
 */
gboolean
mg_resultset_check_model (MgResultSet * rs, gint nbcols, ...)
{
	va_list args;
	gboolean retval;

	g_return_val_if_fail (rs && IS_MG_RESULTSET (rs), FALSE);
	g_return_val_if_fail (rs->priv, FALSE);
	g_return_val_if_fail (rs->priv->model, FALSE);

	va_start (args, nbcols);
	retval = mg_resultset_check_data_model (rs->priv->model, nbcols, args);	
	va_end (args);

	return retval;
}
