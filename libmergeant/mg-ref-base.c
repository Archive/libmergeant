/* mg-ref-base.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-ref-base.h"
#include "marshal.h"
#include <string.h>
#include "mg-xml-storage.h"
#include "mg-database.h"
#include "mg-db-table.h"
#include "mg-db-field.h"
#include "mg-query.h"
#include "mg-target.h"
#include "mg-entity.h"
#include "mg-field.h"
#include "mg-qfield.h"
#include "mg-server.h"
#include "mg-server-function.h"
#include "mg-server-aggregate.h"


/* 
 * Main static functions 
 */
static void mg_ref_base_class_init (MgRefBaseClass * class);
static void mg_ref_base_init (MgRefBase * srv);
static void mg_ref_base_dispose (GObject   * object);
static void mg_ref_base_finalize (GObject   * object);

static void mg_ref_base_set_property (GObject              *object,
				      guint                 param_id,
				      const GValue         *value,
				      GParamSpec           *pspec);
static void mg_ref_base_get_property (GObject              *object,
				      guint                 param_id,
				      GValue               *value,
				      GParamSpec           *pspec);


/* When the DbTable or MgServerDataType is nullified */
static void nullified_object_cb (GObject *obj, MgRefBase *ref);

#ifdef debug
static void mg_ref_base_dump (MgRefBase *field, guint offset);
#endif

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	REF_FOUND,
	REF_LOST,
	LAST_SIGNAL
};

static gint mg_ref_base_signals[LAST_SIGNAL] = { 0, 0 };

/* properties */
enum
{
	PROP_0,
	PROP
};


/* private structure */
struct _MgRefBasePrivate
{
	MgBase           *ref_object;
	GType             requested_type;
	MgRefBaseType     ref_type;
	gchar            *name;
	gboolean          block_signals;
};


/* module error */
GQuark mg_ref_base_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_ref_base_error");
	return quark;
}


guint
mg_ref_base_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgRefBaseClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_ref_base_class_init,
			NULL,
			NULL,
			sizeof (MgRefBase),
			0,
			(GInstanceInitFunc) mg_ref_base_init
		};		
		
		type = g_type_register_static (MG_BASE_TYPE, "MgRefBase", &info, 0);
	}
	return type;
}

static void
mg_ref_base_class_init (MgRefBaseClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_ref_base_signals[REF_FOUND] =
		g_signal_new ("ref_found",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgRefBaseClass, ref_found),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	mg_ref_base_signals[REF_LOST] =
		g_signal_new ("ref_lost",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgRefBaseClass, ref_lost),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	class->ref_found = NULL;
	class->ref_lost = NULL;

	object_class->dispose = mg_ref_base_dispose;
	object_class->finalize = mg_ref_base_finalize;

	/* Properties */
	object_class->set_property = mg_ref_base_set_property;
	object_class->get_property = mg_ref_base_get_property;
	g_object_class_install_property (object_class, PROP,
					 g_param_spec_pointer ("prop", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_ref_base_dump;
#endif
}

static void
mg_ref_base_init (MgRefBase * mg_ref_base)
{
	mg_ref_base->priv = g_new0 (MgRefBasePrivate, 1);
	mg_ref_base->priv->ref_object = NULL;
	mg_ref_base->priv->requested_type = 0;
	mg_ref_base->priv->ref_type = REFERENCE_BY_XML_ID;
	mg_ref_base->priv->name = NULL;
	mg_ref_base->priv->block_signals = FALSE;
}


/**
 * mg_ref_base_new
 * @conf: a #MgConf object
 *
 * Creates a new MgRefBase object
 *
 * Returns: the new object
 */
GObject*
mg_ref_base_new (MgConf *conf)
{
	GObject   *obj;
	MgRefBase *mg_ref_base;

	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);

	obj = g_object_new (MG_REF_BASE_TYPE, "conf", conf, NULL);
	mg_ref_base = MG_REF_BASE (obj);
	mg_base_set_id (MG_BASE (mg_ref_base), 0);

	return obj;
}


/**
 * mg_ref_base_new_copy
 * @orig: a #MgRefBase object
 *
 * Creates a new MgRefBase object which is a copy of @orig. This is a copy constructor.
 *
 * Returns: the new object
 */
GObject
*mg_ref_base_new_copy (MgRefBase *orig)
{
	GObject   *obj;
	MgRefBase *mg_ref_base;

	g_return_val_if_fail (orig && IS_MG_REF_BASE (orig), NULL);

	obj = g_object_new (MG_REF_BASE_TYPE, "conf", mg_base_get_conf (MG_BASE (orig)), NULL);
	mg_ref_base = MG_REF_BASE (obj);
	mg_base_set_id (MG_BASE (mg_ref_base), 0);

	if (orig->priv->ref_object) {
		GObject *obj = G_OBJECT (orig->priv->ref_object);

		g_object_ref (obj);
		g_signal_connect (G_OBJECT (obj), "nullified",
				  G_CALLBACK (nullified_object_cb), mg_ref_base);
		mg_ref_base->priv->ref_object = MG_BASE (obj);
		if (! mg_ref_base->priv->block_signals) {
#ifdef debug_signal
			g_print (">> 'REF_FOUND' from %s\n", __FUNCTION__);
#endif
			g_signal_emit (G_OBJECT (mg_ref_base), mg_ref_base_signals[REF_FOUND], 0);
#ifdef debug_signal
			g_print ("<< 'REF_FOUND' from %s\n", __FUNCTION__);
#endif
		}
	}
	
	mg_ref_base->priv->requested_type = orig->priv->requested_type;
	mg_ref_base->priv->ref_type = orig->priv->ref_type;
	if (orig->priv->name) 
		mg_ref_base->priv->name = g_strdup (orig->priv->name);

	return obj;	
}

static void 
nullified_object_cb (GObject *obj, MgRefBase *ref)
{
	g_return_if_fail (ref->priv->ref_object && (G_OBJECT (ref->priv->ref_object) == obj));

	g_signal_handlers_disconnect_by_func (G_OBJECT (ref->priv->ref_object),
					      G_CALLBACK (nullified_object_cb), ref);
	g_object_unref (ref->priv->ref_object);
	ref->priv->ref_object = NULL;
#ifdef debug_signal
	g_print (">> 'REF_LOST' from %s\n", __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (ref), mg_ref_base_signals[REF_LOST], 0);
#ifdef debug_signal
	g_print ("<< 'REF_LOST' from %s\n", __FUNCTION__);
#endif
}

static void
mg_ref_base_dispose (GObject *object)
{
	MgRefBase *mg_ref_base;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_REF_BASE (object));

	mg_ref_base = MG_REF_BASE (object);
	if (mg_ref_base->priv) {
		mg_base_nullify_check (MG_BASE (object));

		if (mg_ref_base->priv->ref_object)
			nullified_object_cb (G_OBJECT (mg_ref_base->priv->ref_object), mg_ref_base);
		if (mg_ref_base->priv->name) {
			g_free (mg_ref_base->priv->name);
			mg_ref_base->priv->name = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_ref_base_finalize (GObject   * object)
{
	MgRefBase *mg_ref_base;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_REF_BASE (object));

	mg_ref_base = MG_REF_BASE (object);
	if (mg_ref_base->priv) {

		g_free (mg_ref_base->priv);
		mg_ref_base->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_ref_base_set_property (GObject              *object,
			  guint                 param_id,
			  const GValue         *value,
			  GParamSpec           *pspec)
{
	gpointer ptr;
	MgRefBase *mg_ref_base;

	mg_ref_base = MG_REF_BASE (object);
	if (mg_ref_base->priv) {
		switch (param_id) {
		case PROP:
			ptr = g_value_get_pointer (value);
			break;
		}
	}
}

static void
mg_ref_base_get_property (GObject              *object,
			  guint                 param_id,
			  GValue               *value,
			  GParamSpec           *pspec)
{
	MgRefBase *mg_ref_base;
	mg_ref_base = MG_REF_BASE (object);
	
	if (mg_ref_base->priv) {
		switch (param_id) {
		case PROP:
			g_value_set_pointer (value, mg_ref_base->priv->ref_object);
			break;
		}	
	}
}

/**
 * mg_ref_base_set_ref_name
 * @ref: a #MgRefBase object
 * @ref_type: the requested referenced object's data type
 * @type: how to interpret the @name argument
 * @name: the name of the requested object
 *
 * Sets the type and XML Id of the object we want to reference. If any other object was already
 * referenced @ref is first reinitialized
 *
 * Rem: the name format is dependant on the type of object which is requested
 */
void
mg_ref_base_set_ref_name (MgRefBase *ref, GType ref_type, MgRefBaseType type, const gchar *name)
{
	g_return_if_fail (ref && IS_MG_REF_BASE (ref));
	g_return_if_fail (ref->priv);

	/* make sure we know how to retreive the requested object */
	g_return_if_fail ((ref_type == MG_DB_TABLE_TYPE) ||
			  (ref_type == MG_DB_FIELD_TYPE) ||
			  (ref_type == MG_QUERY_TYPE) ||
			  (ref_type == MG_TARGET_TYPE) ||
			  (ref_type == MG_QFIELD_TYPE) ||
			  (ref_type == MG_FIELD_TYPE) ||
			  (ref_type == MG_SERVER_FUNCTION_TYPE) ||
			  (ref_type == MG_SERVER_AGGREGATE_TYPE));

	/* Is there anything to change ? */
	if (ref->priv->name && name && !strcmp (ref->priv->name, name) &&
	    (ref_type == ref->priv->requested_type) && (ref->priv->ref_type == type)) {
		mg_ref_base_activate (ref);
		return;
	}
	
	mg_ref_base_deactivate (ref);
	
	ref->priv->ref_type = type;
	if (ref->priv->name) {
		g_free (ref->priv->name);
		ref->priv->name = NULL;
	}
	if (name)
		ref->priv->name = g_strdup (name);
	ref->priv->requested_type = ref_type;

	mg_ref_base_activate (ref);
}

/**
 * mg_ref_base_set_ref_object_type
 * @ref: a #MgRefBase object
 * @object: the object to keep a reference to
 * @type: the type of object requested: it must be a type in the class hierarchy of @object
 *
 * Rather than to set the XML Id of the object @ref has to reference, this function allows
 * to directly give the object, and specify the requested type, in case the object is known.
 */
void
mg_ref_base_set_ref_object_type (MgRefBase *ref, MgBase *object, GType type)
{
	g_return_if_fail (ref && IS_MG_REF_BASE (ref));
	g_return_if_fail (ref->priv);
	g_return_if_fail (object && IS_MG_BASE (object));

	/* make sure we know how to retreive the requested object */
	g_return_if_fail ((type == MG_DB_TABLE_TYPE) ||
			  (type == MG_DB_FIELD_TYPE) ||
			  (type == MG_QUERY_TYPE) ||
			  (type == MG_TARGET_TYPE) ||
			  (type == MG_QFIELD_TYPE) ||
			  (type == MG_FIELD_TYPE) ||
			  (type == MG_SERVER_FUNCTION_TYPE) ||
			  (type == MG_SERVER_AGGREGATE_TYPE));

	mg_ref_base_deactivate (ref);

	ref->priv->ref_type =  REFERENCE_BY_XML_ID;
	if (ref->priv->name) {
		g_free (ref->priv->name);
		ref->priv->name = NULL;
	}
	
	ref->priv->name = mg_xml_storage_get_xml_id (MG_XML_STORAGE (object));
	ref->priv->requested_type = type;

	/* Object treatment */
	g_object_ref (object);
	g_signal_connect (G_OBJECT (object), "nullified",
			  G_CALLBACK (nullified_object_cb), ref);
	ref->priv->ref_object = object;
	if (! ref->priv->block_signals) {
#ifdef debug_signal
		g_print (">> 'REF_FOUND' from %s\n", __FUNCTION__);
#endif
		g_signal_emit (G_OBJECT (ref), mg_ref_base_signals[REF_FOUND], 0);
#ifdef debug_signal
		g_print ("<< 'REF_FOUND' from %s\n", __FUNCTION__);
#endif
	}
}

/**
 * mg_ref_base_set_ref_object
 * @ref: a #MgRefBase object
 * @object: the object to keep a reference to
 *
 * Rather than to set the XML Id of the object @ref has to reference, this function allows
 * to directly give the object, in case the object is known.
 */
void
mg_ref_base_set_ref_object (MgRefBase *ref, MgBase *object)
{
	GType ref_type;

	g_return_if_fail (object && IS_MG_BASE (object));
	ref_type = G_OBJECT_TYPE (object);
	
	mg_ref_base_set_ref_object_type (ref, object, ref_type);
}

/**
 * mg_ref_base_replace_ref_object
 * @ref: a #MgRefBase object
 * @replacements: a #GHashTable
 *
 * Changes the referenced object with a new one: it looks into @replacements and if the
 * currently referenced object appears there as a key, then the reference is replaced with
 * the corresponding value.
 *
 * Nothing happens if @ref is not active, or if the referenced object is not found in @replacaments.
 */
void
mg_ref_base_replace_ref_object (MgRefBase *ref, GHashTable *replacements)
{
	g_return_if_fail (ref && IS_MG_REF_BASE (ref));
	g_return_if_fail (ref->priv);

	if (!replacements)
		return;
	
	if (ref->priv->ref_object) {
		MgBase *repl;
		repl = g_hash_table_lookup (replacements, ref->priv->ref_object);
		if (repl) {
			/* we don't want to send a "ref_dropped" signal here since we are just
			   changing the referenced object without really losing the reference */
			ref->priv->block_signals = TRUE;
			mg_ref_base_set_ref_object_type (ref, repl, ref->priv->requested_type);		
			ref->priv->block_signals = FALSE;
		}
	}
}

/**
 * mg_ref_base_get_ref_name
 * @ref: a #MgRefBase object
 * @ref_type: where to store the requested referenced object's data type, or NULL
 * @type: where to store how to interpret the @name argument, or NULL
 *
 * Get the caracteristics of the requested object
 *
 * Returns: the name of the object (to be interpreted with @type)
 */
const gchar *
mg_ref_base_get_ref_name (MgRefBase *ref, GType *ref_type, MgRefBaseType *type)
{
	g_return_val_if_fail (ref && IS_MG_REF_BASE (ref), 0);
	g_return_val_if_fail (ref->priv, 0);

	if (ref_type)
		*ref_type = ref->priv->requested_type;
	if (type)
		*type = ref->priv->ref_type;

	return ref->priv->name;
}

/**
 * mg_ref_base_get_ref_type
 * @ref: a #MgRefBase object
 *
 * Get the type of the referenced object by @ref (or the requested type if @ref is not active)
 *
 * Returns: the type
 */
GType
mg_ref_base_get_ref_type (MgRefBase *ref)
{
	g_return_val_if_fail (ref && IS_MG_REF_BASE (ref), 0);
	g_return_val_if_fail (ref->priv, 0);

	return ref->priv->requested_type;
}


/**
 * mg_ref_base_get_ref_object
 * @ref: a #MgRefBase object
 *
 * Get the referenced object by @ref
 *
 * Returns: a pointer to the object, or NULL if the reference is not active
 */
MgBase *
mg_ref_base_get_ref_object (MgRefBase *ref)
{
	g_return_val_if_fail (ref && IS_MG_REF_BASE (ref), NULL);
	g_return_val_if_fail (ref->priv, NULL);

	if (!ref->priv->ref_object)
		mg_ref_base_activate (ref);

	return ref->priv->ref_object;
}

/**
 * mg_ref_base_activate
 * @ref: a #MgRefBase object
 *
 * Tries to "activate" a reference (to find the referenced object). Nothing happens if
 * the object is already activated
 *
 * Returns: TRUE on success
 */
gboolean
mg_ref_base_activate (MgRefBase *ref)
{
	MgBase *obj = NULL;
	gboolean done = FALSE;

	g_return_val_if_fail (ref && IS_MG_REF_BASE (ref), FALSE);
	g_return_val_if_fail (ref->priv, FALSE);

	if (!ref->priv->name)
		/* no object reference set, so we consider ourselve active */
		return TRUE;

	if (ref->priv->ref_object)
		return TRUE;

	/* Find the object */
	/* TABLE */
	if (!done && (ref->priv->requested_type == MG_DB_TABLE_TYPE)) {
		MgDatabase *db;
		MgDbTable *table;

		done = TRUE;
		db = mg_conf_get_database (mg_base_get_conf (MG_BASE (ref)));
		if (ref->priv->ref_type == REFERENCE_BY_XML_ID)
			table = mg_database_get_table_by_xml_id (db, ref->priv->name);
		else
			table = mg_database_get_table_by_name (db, ref->priv->name);

		if (table)
			obj = MG_BASE (table);
	}

	/* TABLE's FIELD */
	if (!done && (ref->priv->requested_type == MG_DB_FIELD_TYPE)) {
		MgDatabase *db;
		MgDbField *field;

		done = TRUE;
		db = mg_conf_get_database (mg_base_get_conf (MG_BASE (ref)));
		if (ref->priv->ref_type == REFERENCE_BY_XML_ID)
			field = mg_database_get_field_by_xml_id (db, ref->priv->name);
		else
			field = mg_database_get_field_by_name (db, ref->priv->name);

		if (field)
			obj = MG_BASE (field);
	}

	/* QUERY */
	if (!done && (ref->priv->requested_type == MG_QUERY_TYPE)) {
		MgQuery *query = NULL;

		done = TRUE;
		if (ref->priv->ref_type == REFERENCE_BY_XML_ID)
			query = mg_conf_get_query_by_xml_id (mg_base_get_conf (MG_BASE (ref)), ref->priv->name);
		else
			TO_IMPLEMENT; /* not really needed, anyway */
		if (query)
			obj = MG_BASE (query);
	}

	/* QUERY's FIELD */
	if (!done && (ref->priv->requested_type == MG_QFIELD_TYPE)) {
		MgQuery *query;
		gchar *str, *ptr, *tok;

		str = g_strdup (ref->priv->name);
		ptr = strtok_r (str, ":", &tok);
		
		done = TRUE;
		query = mg_conf_get_query_by_xml_id (mg_base_get_conf (MG_BASE (ref)), ptr);
		
		if (query) {
			MgField *field;
			
			field = mg_entity_get_field_by_xml_id (MG_ENTITY (query), ref->priv->name);
			if (field)
				obj = MG_BASE (field);
		}
	}


	/* TARGET */
	if (!done && (ref->priv->requested_type == MG_TARGET_TYPE)) {
		done = TRUE;

		if (ref->priv->ref_type == REFERENCE_BY_XML_ID) {
			gchar *str, *ptr, *tok;
			MgQuery *query;
			
			str = g_strdup (ref->priv->name);
			ptr = strtok_r (str, ":", &tok);
			query = mg_conf_get_query_by_xml_id (mg_base_get_conf (MG_BASE (ref)), ptr);
			g_free (str);
			if (query) {
				MgTarget *target;
				
				target = mg_query_get_target_by_xml_id (query, ref->priv->name);
				if (target)
					obj = MG_BASE (target);
			}
		}
		else 
			TO_IMPLEMENT;
	}
	
	/* Generic FIELD (MgField interface)*/
	if (!done && (ref->priv->requested_type == MG_FIELD_TYPE)) {
		if (ref->priv->ref_type == REFERENCE_BY_XML_ID) {
			gchar *str, *ptr, *tok;
			str = g_strdup (ref->priv->name);
			ptr = strtok_r (str, ":", &tok);
			if ((*ptr == 'T') && (*(ptr+1) == 'V')) {
				/* we are really looking for a table's field */
				MgDatabase *db;
				MgDbField *field;
				
				done = TRUE;
				
				db = mg_conf_get_database (mg_base_get_conf (MG_BASE (ref)));
				field = mg_database_get_field_by_xml_id (db, ref->priv->name);
				
				if (field)
					obj = MG_BASE (field);
			}
			
			if (!done && (*ptr == 'Q') && (*(ptr+1) == 'U')) {
				/* we are really looking for a query's field */
				MgQuery *query;
				
				done = TRUE;
				query = mg_conf_get_query_by_xml_id (mg_base_get_conf (MG_BASE (ref)), ptr);
				
				if (query) {
					MgField *field;
					
					field = mg_entity_get_field_by_xml_id (MG_ENTITY (query), ref->priv->name);
					if (field)
						obj = MG_BASE (field);
				}
			}
			g_free (str);		
		}
		else {
			done = TRUE;
			TO_IMPLEMENT;
		}
	}

	/* Server function */
	if (!done && (ref->priv->requested_type == MG_SERVER_FUNCTION_TYPE)) {
		MgServerFunction *func = NULL;
		MgServer *srv;

		srv = mg_conf_get_server (mg_base_get_conf (MG_BASE (ref)));
		done = TRUE;
		if (ref->priv->ref_type == REFERENCE_BY_XML_ID)
			func = mg_server_get_function_by_xml_id (srv, ref->priv->name);
		else
			TO_IMPLEMENT; /* not really needed, anyway */
		if (func)
			obj = MG_BASE (func);
	}

	/* Server aggregate */
	if (!done && (ref->priv->requested_type == MG_SERVER_AGGREGATE_TYPE)) {
		MgServerAggregate *agg = NULL;
		MgServer *srv;

		srv = mg_conf_get_server (mg_base_get_conf (MG_BASE (ref)));
		done = TRUE;
		if (ref->priv->ref_type == REFERENCE_BY_XML_ID)
			agg = mg_server_get_aggregate_by_xml_id (srv, ref->priv->name);
		else
			TO_IMPLEMENT; /* not really needed, anyway */
		if (agg)
			obj = MG_BASE (agg);
	}


	/* Object treatment */
	if (obj) {
		g_object_ref (obj);

		g_signal_connect (G_OBJECT (obj), "nullified",
				  G_CALLBACK (nullified_object_cb), ref);
		ref->priv->ref_object = obj;
#ifdef debug_signal
		g_print (">> 'REF_FOUND' from %s\n", __FUNCTION__);
#endif
		g_signal_emit (G_OBJECT (ref), mg_ref_base_signals[REF_FOUND], 0);
#ifdef debug_signal
		g_print ("<< 'REF_FOUND' from %s\n", __FUNCTION__);
#endif
	}

	return ref->priv->ref_object ? TRUE : FALSE;
}

/**
 * mg_ref_base_deactivate
 * @ref: a #MgRefBase object
 *
 * Desctivates the object (loses the reference to the object)
 */
void
mg_ref_base_deactivate (MgRefBase *ref)
{
	g_return_if_fail (ref && IS_MG_REF_BASE (ref));
	g_return_if_fail (ref->priv);

	if (!ref->priv->name)
		return;
	
	if (! ref->priv->ref_object)
		return;

	g_signal_handlers_disconnect_by_func (G_OBJECT (ref->priv->ref_object),
					      G_CALLBACK (nullified_object_cb), ref);
	g_object_unref (ref->priv->ref_object);
	ref->priv->ref_object = NULL;

	if (! ref->priv->block_signals) {
#ifdef debug_signal
		g_print (">> 'REF_LOST' from %s\n", __FUNCTION__);
#endif
		g_signal_emit (G_OBJECT (ref), mg_ref_base_signals[REF_LOST], 0);
#ifdef debug_signal
		g_print ("<< 'REF_LOST' from %s\n", __FUNCTION__);
#endif
	}
}

/**
 * mg_ref_base_is_active
 * @ref: a #MgRefBase object
 *
 * Find wether @ref is active
 *
 * Returns: TRUE if @ref is active
 */
gboolean
mg_ref_base_is_active (MgRefBase *ref)
{
	g_return_val_if_fail (ref && IS_MG_REF_BASE (ref), FALSE);
	g_return_val_if_fail (ref->priv, FALSE);
	
	if (!ref->priv->name)
		/* no object reference set, so we consider ourselve active */
		return TRUE;

	return ref->priv->ref_object ? TRUE : FALSE;
}


#ifdef debug
static void
mg_ref_base_dump (MgRefBase *ref, guint offset)
{
	g_return_if_fail (ref && IS_MG_REF_BASE (ref));
	g_return_if_fail (ref->priv);

	if (ref->priv->ref_object) 
		mg_base_dump (MG_BASE (ref->priv->ref_object), offset);
	else {
		gchar *str;
		gint i;

		/* string for the offset */
		str = g_new0 (gchar, offset+1);
		for (i=0; i<offset; i++)
			str[i] = ' ';
		str[offset] = 0;

		g_print ("%s" D_COL_ERR "BaseRef to id '%s' not active\n" D_COL_NOR, str, ref->priv->name);
	}
}
#endif

