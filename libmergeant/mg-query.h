/* mg-query.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_QUERY_H_
#define __MG_QUERY_H_

#include "mg-base.h"
#include "mg-defs.h"
#include <libgda/libgda.h>

G_BEGIN_DECLS

#define MG_QUERY_TYPE          (mg_query_get_type())
#define MG_QUERY(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_query_get_type(), MgQuery)
#define MG_QUERY_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_query_get_type (), MgQueryClass)
#define IS_MG_QUERY(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_query_get_type ())

/* interfaces:
 * MgEntity
 * MgXmlStorage
 * MgReferer
 * MgRenderer
 */

/* error reporting */
extern GQuark mg_query_error_quark (void);
#define MG_QUERY_ERROR mg_query_error_quark ()

/* different possible types for a query */
typedef enum {
        MG_QUERY_TYPE_SELECT,
	MG_QUERY_TYPE_INSERT,
	MG_QUERY_TYPE_UPDATE,
	MG_QUERY_TYPE_DELETE,
        MG_QUERY_TYPE_UNION,
        MG_QUERY_TYPE_INTERSECT,
	MG_QUERY_TYPE_EXCEPT,
        MG_QUERY_TYPE_SQL,
        MG_QUERY_TYPE_LAST
} MgQueryType;

enum
{
	MG_QUERY_XML_LOAD_ERROR,
	MG_QUERY_META_DATA_UPDATE,
	MG_QUERY_FIELDS_ERROR,
	MG_QUERY_TARGETS_ERROR,
	MG_QUERY_RENDER_ERROR
};


/* struct for the object's data */
struct _MgQuery
{
	MgBase                object;
	MgQueryPrivate       *priv;
};

/* struct for the object's class */
struct _MgQueryClass
{
	MgBaseClass                    class;

	/* signals */
	void   (*type_changed)         (MgQuery *query);
	void   (*condition_changed)    (MgQuery *query);

	void   (*target_added)         (MgQuery *query, MgTarget *target);
	void   (*target_removed)       (MgQuery *query, MgTarget *target);
	void   (*target_updated)       (MgQuery *query, MgTarget *target);

	void   (*join_added)           (MgQuery *query, MgJoin *join);
	void   (*join_removed)         (MgQuery *query, MgJoin *join);
	void   (*join_updated)         (MgQuery *query, MgJoin *join);
	
	void   (*sub_query_added)      (MgQuery *query, MgQuery *sub_query);
	void   (*sub_query_removed)    (MgQuery *query, MgQuery *sub_query);
	void   (*sub_query_updated)    (MgQuery *query, MgQuery *sub_query);
};

guint           mg_query_get_type               (void);
GObject        *mg_query_new                    (MgConf *conf);
GObject        *mg_query_new_copy               (MgQuery *orig, GHashTable *replacements);

void            mg_query_set_query_type         (MgQuery *query, MgQueryType type);
MgQueryType     mg_query_get_query_type         (MgQuery *query);
gboolean        mg_query_is_select_query       (MgQuery *query);
gboolean        mg_query_is_modif_query         (MgQuery *query);
MgQuery        *mg_query_get_parent_query       (MgQuery *query);

MgQfield       *mg_query_get_field_by_ref_field (MgQuery *query, MgField *ref_field);

/* if SQL queries */
void            mg_query_sql_set_text           (MgQuery *query, const gchar *sql);
const gchar    *mg_query_sql_get_text           (MgQuery *query);

/* for other types of queries */
GSList         *mg_query_get_sub_queries        (MgQuery *query);
void            mg_query_add_sub_query          (MgQuery *query, MgQuery *sub_query);
void            mg_query_del_sub_query          (MgQuery *query, MgQuery *sub_query);

void            mg_query_add_param_source       (MgQuery *query, MgQuery *param_source);
void            mg_query_del_param_source       (MgQuery *query, MgQuery *param_source);
const GSList   *mg_query_get_param_sources      (MgQuery *query);

GSList         *mg_query_get_targets            (MgQuery *query);
gboolean        mg_query_add_target             (MgQuery *query, MgTarget *target, GError **error);
void            mg_query_del_target             (MgQuery *query, MgTarget *target);
MgTarget       *mg_query_get_target_by_xml_id   (MgQuery *query, const gchar *xml_id);

GSList         *mg_query_get_joins              (MgQuery *query);
gboolean        mg_query_add_join               (MgQuery *query, MgJoin *join);
void            mg_query_del_join               (MgQuery *query, MgJoin *join);

void            mg_query_set_condition          (MgQuery *query, MgCondition *cond);
MgCondition    *mg_query_get_condition          (MgQuery *query);

void            mg_query_set_order_by_field     (MgQuery *query, MgQfield *field, gint order, gboolean ascendant);
gint            mg_query_get_order_by_field     (MgQuery *query, MgQfield *field, gboolean *ascendant);

G_END_DECLS

#endif
