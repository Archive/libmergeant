/* mg-conf.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


/*
 * This object manages the objects used my mergeant (for tables, queries, etc),
 * but not what is specific to Mergeant's own GUI.
 */

#ifndef __MG_CONF_H_
#define __MG_CONF_H_

#include <glib-object.h>
#include "mg-defs.h"

G_BEGIN_DECLS

#define MG_CONF_TYPE          (mg_conf_get_type())
#define MG_CONF(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_conf_get_type(), MgConf)
#define MG_CONF_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_conf_get_type (), MgConfClass)
#define IS_MG_CONF(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_conf_get_type ())


/* error reporting */
extern GQuark mg_conf_error_quark (void);
#define MG_CONF_ERROR mg_conf_error_quark ()

enum {
	MG_CONF_FILE_LOAD_ERROR,
	MG_CONF_FILE_SAVE_ERROR
};


/* struct for the object's data */
struct _MgConf
{
	GObject              object;
	MgConfPrivate       *priv;
};

/* struct for the object's class */
struct _MgConfClass
{
	GObjectClass            class;

        /* signal the addition or removal of a query in the queries list */
        void (*query_added)      (MgConf * conf, MgQuery *new_query);
        void (*query_removed)    (MgConf * conf, MgQuery *old_query);
};

guint        mg_conf_get_type            (void);
GObject     *mg_conf_new                 (void);

guint        mg_conf_get_id_serial       (MgConf *conf);
void         mg_conf_set_id_serial       (MgConf *conf, guint value);

gboolean     mg_conf_load_xml_file       (MgConf *conf, const gchar *xmlfile, GError **error);
gboolean     mg_conf_save_xml_file       (MgConf *conf, const gchar *xmlfile, GError **error);

void         mg_conf_declare_query       (MgConf *conf, MgQuery *query);
void         mg_conf_assume_query        (MgConf *conf, MgQuery *query);
void         mg_conf_unassume_query      (MgConf *conf, MgQuery *query);
GSList      *mg_conf_get_queries         (MgConf *conf);
MgQuery     *mg_conf_get_query_by_xml_id (MgConf *conf, const gchar *xml_id);

MgServer    *mg_conf_get_server          (MgConf *conf);
MgDatabase  *mg_conf_get_database        (MgConf *conf);

#ifdef debug
void         mg_conf_dump                (MgConf *conf);
#endif
G_END_DECLS

#endif
