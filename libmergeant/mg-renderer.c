/* mg-renderer.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-renderer.h"
#include "marshal.h"

static void mg_renderer_iface_init (gpointer g_class);

GType
mg_renderer_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgRendererIface),
			(GBaseInitFunc) mg_renderer_iface_init,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) NULL,
			NULL,
			NULL,
			0,
			0,
			(GInstanceInitFunc) NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, "MgRenderer", &info, 0);
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
	}
	return type;
}


static void
mg_renderer_iface_init (gpointer g_class)
{
	static gboolean initialized = FALSE;

	if (! initialized) {
		initialized = TRUE;
	}
}

/**
 * mg_renderer_render_as_xql
 * @iface: an object which implements the #MgRenderer interface
 * @context: rendering context
 * @error: location to store error, or %NULL
 *
 * Build a new XML node representing the object, in the specified context.
 *
 * Returns: the new XML node, or %NULL in case of error
 */
GdaXqlItem *
mg_renderer_render_as_xql (MgRenderer *iface, MgContext *context, GError **error)
{
	g_return_val_if_fail (iface && IS_MG_RENDERER (iface), NULL);

	if (MG_RENDERER_GET_IFACE (iface)->render_as_xql)
		return (MG_RENDERER_GET_IFACE (iface)->render_as_xql) (iface, context, error);
	
	return NULL;
}

/**
 * mg_renderer_render_as_sql
 * @iface: an object which implements the #MgRenderer interface
 * @context: rendering context
 * @error: location to store error, or %NULL
 *
 * Build a SQL statement representing the object, in the specified context.
 *
 * If @context is %NULL, then no error related to missing parameters (which should be in the
 * context) is returned, and missing values are replaced by 'human readable' SQL.
 *
 * Returns: the new SQL statement (new string), or %NULL in case of error
 */
gchar *
mg_renderer_render_as_sql (MgRenderer *iface, MgContext *context, GError **error)
{
	g_return_val_if_fail (iface && IS_MG_RENDERER (iface), NULL);

	if (MG_RENDERER_GET_IFACE (iface)->render_as_sql)
		return (MG_RENDERER_GET_IFACE (iface)->render_as_sql) (iface, context, error);
	
	return NULL;
}


/**
 * mg_renderer_render_as_str
 * @iface: an object which implements the #MgRenderer interface
 * @context: rendering context
 *
 * Build a human readable string representing the object, in the specified context.
 *
 * Returns: the new string
 */
gchar *
mg_renderer_render_as_str (MgRenderer *iface, MgContext *context)
{
	g_return_val_if_fail (iface && IS_MG_RENDERER (iface), NULL);

	if (MG_RENDERER_GET_IFACE (iface)->render_as_str)
		return (MG_RENDERER_GET_IFACE (iface)->render_as_str) (iface, context);
	
	return NULL;
}
