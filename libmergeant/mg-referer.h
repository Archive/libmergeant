/* mg-referer.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_REFERER_H_
#define __MG_REFERER_H_

#include <glib-object.h>
#include "mg-defs.h"

G_BEGIN_DECLS

#define MG_REFERER_TYPE          (mg_referer_get_type())
#define MG_REFERER(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_referer_get_type(), MgReferer)
#define IS_MG_REFERER(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_referer_get_type ())
#define MG_REFERER_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MG_REFERER_TYPE, MgRefererIface))

struct _MgRefererContext
{
	gpointer fill_later;
};

/* struct for the interface */
struct _MgRefererIface
{
	GTypeInterface           g_iface;

	/* virtual table */
	gboolean    (* activate)        (MgReferer *iface);
	void        (* deactivate)      (MgReferer *iface);
	gboolean    (* is_active)       (MgReferer *iface);
	GSList     *(* get_ref_objects) (MgReferer *iface);
	void        (* replace_refs)    (MgReferer *iface, GHashTable *replacements);

	/* signals */
	void        (* activated)       (MgReferer *iface);
	void        (* deactivated)     (MgReferer *iface);
};

GType       mg_referer_get_type            (void) G_GNUC_CONST;

gboolean    mg_referer_activate            (MgReferer *iface);
void        mg_referer_deactivate          (MgReferer *iface);
gboolean    mg_referer_is_active           (MgReferer *iface);
GSList     *mg_referer_get_ref_objects     (MgReferer *iface);
void        mg_referer_replace_refs        (MgReferer *iface, GHashTable *replacements);

G_END_DECLS

#endif
