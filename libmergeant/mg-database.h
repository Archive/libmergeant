/* mg-database.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_DATABASE_H_
#define __MG_DATABASE_H_

#include <glib-object.h>
#include "mg-defs.h"
#include "mg-base.h"
#include "mg-conf.h"

G_BEGIN_DECLS

#define MG_DATABASE_TYPE          (mg_database_get_type())
#define MG_DATABASE(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_database_get_type(), MgDatabase)
#define MG_DATABASE_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_database_get_type (), MgDatabaseClass)
#define IS_MG_DATABASE(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_database_get_type ())


/* error reporting */
extern GQuark mg_database_error_quark (void);
#define MG_DATABASE_ERROR mg_database_error_quark ()

enum
{
	MG_DATABASE_XML_SAVE_ERROR,
	MG_DATABASE_XML_LOAD_ERROR,
	MG_DATABASE_META_DATA_UPDATE,
	MG_DATABASE_META_DATA_UPDATE_USER_STOPPED,
	MG_DATABASE_TABLES_ERROR,
	MG_DATABASE_SEQUENCES_ERROR
};



/* struct for the object's data */
struct _MgDatabase
{
	MgBase                   object;
	MgDatabasePrivate       *priv;
};

/* struct for the object's class */
struct _MgDatabaseClass
{
	MgBaseClass class;

	/* signals */
	void   (*table_added)               (MgDatabase *obj, MgDbTable *table);
	void   (*table_removed)             (MgDatabase *obj, MgDbTable *table);
	void   (*table_updated)             (MgDatabase *obj, MgDbTable *table);

	void   (*field_added)               (MgDatabase *obj, MgDbField *field);
	void   (*field_removed)             (MgDatabase *obj, MgDbField *field);
	void   (*field_updated)             (MgDatabase *obj, MgDbField *field);

	void   (*sequence_added)            (MgDatabase *obj, MgDbSequence *seq);
	void   (*sequence_removed)          (MgDatabase *obj, MgDbSequence *seq);
	void   (*sequence_updated)          (MgDatabase *obj, MgDbSequence *seq);

	void   (*constraint_added)          (MgDatabase *obj, MgDbConstraint *cstr);
	void   (*constraint_removed)        (MgDatabase *obj, MgDbConstraint *cstr);
	void   (*constraint_updated)        (MgDatabase *obj, MgDbConstraint *cstr);
	
	void   (*fs_link_added)              (MgDatabase *obj, MgDbSequence *seq, MgDbField *field);
	void   (*fs_link_removed)            (MgDatabase *obj, MgDbSequence *seq, MgDbField *field);

	void   (*data_update_started)       (MgDatabase *obj);
	void   (*update_progress)           (MgDatabase *obj, gchar * msg, guint now, guint total);
	void   (*data_update_finished)      (MgDatabase *obj);
};

guint              mg_database_get_type               (void);
GObject           *mg_database_new                    (MgConf *conf);

MgConf            *mg_database_get_conf               (MgDatabase *mgdb);

gboolean           mg_database_update_dbms_data       (MgDatabase *mgdb, GError **error);
void               mg_database_stop_update_dbms_data  (MgDatabase *mgdb);

GSList            *mg_database_get_tables             (MgDatabase *mgdb);
MgDbTable         *mg_database_get_table_by_name      (MgDatabase *mgdb, const gchar *name);
MgDbTable         *mg_database_get_table_by_xml_id    (MgDatabase *mgdb, const gchar *xml_id);
MgDbField         *mg_database_get_field_by_name      (MgDatabase *mgdb, const gchar *fullname);
MgDbField         *mg_database_get_field_by_xml_id    (MgDatabase *mgdb, const gchar *xml_id);

MgDbSequence      *mg_database_get_sequence_by_name   (MgDatabase *mgdb, const gchar *name);
MgDbSequence      *mg_database_get_sequence_by_xml_id (MgDatabase *mgdb, const gchar *xml_id);
MgDbSequence      *mg_database_get_sequence_to_field  (MgDatabase *mgdb, MgDbField *field);
void               mg_database_link_sequence          (MgDatabase *mgdb, 
						       MgDbSequence *seq, MgDbField *field);
void               mg_database_unlink_sequence        (MgDatabase *mgdb, 
						       MgDbSequence *seq, MgDbField *field);

GSList            *mg_database_get_all_constraints    (MgDatabase *mgdb);
GSList            *mg_database_get_fk_constraints     (MgDatabase *mgdb);

G_END_DECLS

#endif
