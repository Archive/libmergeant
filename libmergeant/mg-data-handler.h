/* mg-data-handler.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_DATA_HANDLER_H_
#define __MG_DATA_HANDLER_H_

#include <glib-object.h>
#include "mg-defs.h"
#include <libgda/libgda.h>

G_BEGIN_DECLS

#define MG_DATA_HANDLER_TYPE          (mg_data_handler_get_type())
#define MG_DATA_HANDLER(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_data_handler_get_type(), MgDataHandler)
#define IS_MG_DATA_HANDLER(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_data_handler_get_type ())
#define MG_DATA_HANDLER_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MG_DATA_HANDLER_TYPE, MgDataHandlerIface))


/* struct for the interface */
struct _MgDataHandlerIface
{
	GTypeInterface           g_iface;

	/* virtual table */
	MgDataEntry *(* get_entry_from_value) (MgDataHandler *dh, const GdaValue *value, GdaValueType type);
	gchar       *(* get_sql_from_value)   (MgDataHandler *dh, const GdaValue *value);
	gchar       *(* get_str_from_value)   (MgDataHandler *dh, const GdaValue *value);
	GdaValue    *(* get_value_from_sql)   (MgDataHandler *dh, const gchar *sql, GdaValueType type);
	GdaValue    *(* get_value_from_str)   (MgDataHandler *dh, const gchar *str, GdaValueType type);
	GdaValue    *(* get_sane_init_value)  (MgDataHandler *dh, GdaValueType type);

	guint        (* get_nb_gda_types)     (MgDataHandler * dh);
	GdaValueType (* get_gda_type_index)   (MgDataHandler * dh, guint index);
	gboolean     (* accepts_gda_type)     (MgDataHandler * dh, GdaValueType type);
	
	const gchar *(* get_descr)            (MgDataHandler * dh);
	const gchar *(* get_descr_detail)     (MgDataHandler * dh);
	const gchar *(* get_version)          (MgDataHandler * dh);
	gboolean     (* is_plugin)            (MgDataHandler * dh);
	const gchar *(* get_plugin_name)      (MgDataHandler * dh);
	const gchar *(* get_plugin_file)      (MgDataHandler * dh);
	gchar       *(* get_key)              (MgDataHandler * dh);
	
};




guint        mg_data_handler_get_type               (void);

MgDataEntry *mg_data_handler_get_entry_from_value   (MgDataHandler * dh, const GdaValue *value, GdaValueType type);
gchar       *mg_data_handler_get_sql_from_value     (MgDataHandler * dh, const GdaValue *value);
gchar       *mg_data_handler_get_str_from_value     (MgDataHandler * dh, const GdaValue *value);
GdaValue    *mg_data_handler_get_value_from_sql     (MgDataHandler * dh, const gchar *sql, GdaValueType type);
GdaValue    *mg_data_handler_get_value_from_str     (MgDataHandler * dh, const gchar *str, GdaValueType type);
GdaValue    *mg_data_handler_get_sane_init_value    (MgDataHandler * dh, GdaValueType type);

guint        mg_data_handler_get_nb_gda_types       (MgDataHandler * dh);
GdaValueType mg_data_handler_get_gda_type_index     (MgDataHandler * dh, guint index);
gboolean     mg_data_handler_accepts_gda_type       (MgDataHandler * dh, GdaValueType type);

const gchar *mg_data_handler_get_descr              (MgDataHandler * dh);
const gchar *mg_data_handler_get_descr_detail       (MgDataHandler * dh);
const gchar *mg_data_handler_get_version            (MgDataHandler * dh);
gboolean     mg_data_handler_is_plugin              (MgDataHandler * dh);
const gchar *mg_data_handler_get_plugin_name        (MgDataHandler * dh);
const gchar *mg_data_handler_get_plugin_file        (MgDataHandler * dh);
gchar       *mg_data_handler_get_key                (MgDataHandler * dh);


G_END_DECLS

#endif
