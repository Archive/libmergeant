/* mg-db-field.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-db-field.h"
#include "mg-db-table.h"
#include "mg-xml-storage.h"
#include "mg-field.h"
#include "mg-entity.h"
#include "mg-renderer.h"
#include "marshal.h"
#include "mg-data-handler.h"
#include "mg-server.h"
#include "mg-server-data-type.h"
#include "mg-db-constraint.h"
#include <string.h>

/* 
 * Main static functions 
 */
static void mg_db_field_class_init (MgDbFieldClass * class);
static void mg_db_field_init (MgDbField * srv);
static void mg_db_field_dispose (GObject   * object);
static void mg_db_field_finalize (GObject   * object);

static void mg_db_field_set_property (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec);
static void mg_db_field_get_property (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec);

/* XML storage interface */
static void        mg_db_field_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_db_field_get_xml_id (MgXmlStorage *iface);
static xmlNodePtr  mg_db_field_save_to_xml (MgXmlStorage *iface, GError **error);
static gboolean    mg_db_field_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error);

/* Field interface */
static void              mg_db_field_field_init      (MgFieldIface *iface);
static MgEntity         *mg_db_field_get_entity      (MgField *iface);
static MgServerDataType *mg_db_field_get_data_type   (MgField *iface);
static const gchar      *mg_db_field_get_name        (MgField *iface);
static const gchar      *mg_db_field_get_description (MgField *iface);

/* Renderer interface */
static void            mg_db_field_renderer_init      (MgRendererIface *iface);
static GdaXqlItem     *mg_db_field_render_as_xql   (MgRenderer *iface, MgContext *context, GError **error);
static gchar          *mg_db_field_render_as_sql   (MgRenderer *iface, MgContext *context, GError **error);
static gchar          *mg_db_field_render_as_str   (MgRenderer *iface, MgContext *context);

#ifdef debug
static void            mg_db_field_dump            (MgDbField *field, guint offset);
#endif

/* When the DbTable or MgServerDataType is nullified */
static void nullified_object_cb (GObject *obj, MgDbField *field);

/* Function to get a MgDataHandler from a MgDbField */
static MgDataHandler *mg_db_field_handler_func (MgServer *srv, GObject *obj);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	TEMPL_SIGNAL,
	LAST_SIGNAL
};

static gint mg_db_field_signals[LAST_SIGNAL] = { 0 };

/* properties */
enum
{
	PROP_0,
	PROP_DB_TABLE
};


/* private structure */
struct _MgDbFieldPrivate
{
	MgServerDataType *data_type;
	MgDbTable        *table;
	gint              length;     /* -1 if not applicable */
	gint              scale;      /* 0 if not applicable */
	GdaValue         *default_val;/* NULL if no default value */
};
#define FIELD_SRV(field) (mg_conf_get_server (mg_base_get_conf (MG_BASE (field))))


/* module error */
GQuark mg_db_field_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_db_field_error");
	return quark;
}


guint
mg_db_field_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgDbFieldClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_db_field_class_init,
			NULL,
			NULL,
			sizeof (MgDbField),
			0,
			(GInstanceInitFunc) mg_db_field_init
		};

		static const GInterfaceInfo xml_storage_info = {
			(GInterfaceInitFunc) mg_db_field_xml_storage_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo field_info = {
			(GInterfaceInitFunc) mg_db_field_field_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo renderer_info = {
			(GInterfaceInitFunc) mg_db_field_renderer_init,
			NULL,
			NULL
		};
		
		
		type = g_type_register_static (MG_BASE_TYPE, "MgDbField", &info, 0);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
		g_type_add_interface_static (type, MG_FIELD_TYPE, &field_info);
		g_type_add_interface_static (type, MG_RENDERER_TYPE, &renderer_info);
	}
	return type;
}

static void 
mg_db_field_xml_storage_init (MgXmlStorageIface *iface)
{
	iface->get_xml_id = mg_db_field_get_xml_id;
	iface->save_to_xml = mg_db_field_save_to_xml;
	iface->load_from_xml = mg_db_field_load_from_xml;
}

static void
mg_db_field_field_init (MgFieldIface *iface)
{
	iface->get_entity = mg_db_field_get_entity;
	iface->get_data_type = mg_db_field_get_data_type;
	iface->get_name = mg_db_field_get_name;
	iface->get_description = mg_db_field_get_description;
}

static void
mg_db_field_renderer_init (MgRendererIface *iface)
{
	iface->render_as_xql = mg_db_field_render_as_xql;
	iface->render_as_sql = mg_db_field_render_as_sql;
	iface->render_as_str = mg_db_field_render_as_str;
}

static void
mg_db_field_class_init (MgDbFieldClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_db_field_signals[TEMPL_SIGNAL] =
		g_signal_new ("templ_signal",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgDbFieldClass, templ_signal),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	class->templ_signal = NULL;

	object_class->dispose = mg_db_field_dispose;
	object_class->finalize = mg_db_field_finalize;

	/* Properties */
	object_class->set_property = mg_db_field_set_property;
	object_class->get_property = mg_db_field_get_property;
	g_object_class_install_property (object_class, PROP_DB_TABLE,
					 g_param_spec_pointer ("db_table", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_db_field_dump;
#endif
}


static void
mg_db_field_init (MgDbField * mg_db_field)
{
	mg_db_field->priv = g_new0 (MgDbFieldPrivate, 1);
	mg_db_field->priv->table = NULL;
	mg_db_field->priv->data_type = NULL;
	mg_db_field->priv->length = -1;
	mg_db_field->priv->scale = 0;
}


/**
 * mg_db_field_new
 * @conf: a #MgConf object
 * @type:  a #MgServerDataType object (the field's type)
 *
 * Creates a new MgDbField object
 *
 * Returns: the new object
 */
GObject*
mg_db_field_new (MgConf *conf, MgServerDataType *type)
{
	GObject   *obj;
	MgDbField *mg_db_field;

	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);
	if (type)
		g_return_val_if_fail (IS_MG_SERVER_DATA_TYPE (type), NULL);

	obj = g_object_new (MG_DB_FIELD_TYPE, "conf", conf, NULL);
	mg_db_field = MG_DB_FIELD (obj);
	mg_base_set_id (MG_BASE (mg_db_field), 0);

	if (type)
		mg_db_field_set_data_type (mg_db_field, type);

	/* Extra init */
	mg_server_set_object_func_handler (mg_conf_get_server (conf), mg_db_field_handler_func);
	
	return obj;
}

static MgDataHandler *
mg_db_field_handler_func (MgServer *srv, GObject *obj)
{
	if (IS_MG_DB_FIELD (obj)) 
		return mg_server_get_handler_by_gda (srv, 
						     mg_server_data_type_get_gda_type (MG_DB_FIELD (obj)->priv->data_type));
	else
		return NULL;
}

static void 
nullified_object_cb (GObject *obj, MgDbField *field)
{
	mg_base_nullify (MG_BASE (field));
}

static void
mg_db_field_dispose (GObject *object)
{
	MgDbField *mg_db_field;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_DB_FIELD (object));

	mg_db_field = MG_DB_FIELD (object);
	if (mg_db_field->priv) {
		mg_base_nullify_check (MG_BASE (object));

		if (mg_db_field->priv->table) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (mg_db_field->priv->table),
							      G_CALLBACK (nullified_object_cb), mg_db_field);
			mg_db_field->priv->table = NULL;
		}
		if (mg_db_field->priv->data_type) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (mg_db_field->priv->data_type),
							      G_CALLBACK (nullified_object_cb), mg_db_field);
			mg_db_field->priv->data_type = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_db_field_finalize (GObject   * object)
{
	MgDbField *mg_db_field;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_DB_FIELD (object));

	mg_db_field = MG_DB_FIELD (object);
	if (mg_db_field->priv) {

		g_free (mg_db_field->priv);
		mg_db_field->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_db_field_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	gpointer ptr;
	MgDbField *mg_db_field;

	mg_db_field = MG_DB_FIELD (object);
	if (mg_db_field->priv) {
		switch (param_id) {
		case PROP_DB_TABLE:
			if (mg_db_field->priv->table) {
				g_signal_handlers_disconnect_by_func (G_OBJECT (mg_db_field->priv->table),
								      G_CALLBACK (nullified_object_cb), mg_db_field);
				mg_db_field->priv->table = NULL;
			}

			ptr = g_value_get_pointer (value);
			if (ptr && IS_MG_DB_TABLE (ptr)) {
				mg_db_field->priv->table = MG_DB_TABLE (ptr);
				g_signal_connect (G_OBJECT (ptr), "nullified",
						  G_CALLBACK (nullified_object_cb), mg_db_field);
			}
			break;
		}
	}
}

static void
mg_db_field_get_property (GObject              *object,
			guint                 param_id,
			GValue               *value,
			GParamSpec           *pspec)
{
	MgDbField *mg_db_field;
	mg_db_field = MG_DB_FIELD (object);
	
	if (mg_db_field->priv) {
		switch (param_id) {
		case PROP_DB_TABLE:
			g_value_set_pointer (value, mg_db_field->priv->table);
			break;
		}	
	}
}


/**
 * mg_db_field_get_length
 * @field: a #MgDbField  object
 *
 * Set the length of a field.
 *
 */
void
mg_db_field_set_length (MgDbField *field, gint length)
{
	g_return_if_fail (field && IS_MG_DB_FIELD (field));
	g_return_if_fail (field->priv);
	if (length <= 0)
		field->priv->length = -1;
	else
		field->priv->length = length;

	/* signal the modification */
	mg_base_changed (MG_BASE (field));
}

/**
 * mg_db_field_get_length
 * @field: a #MgDbField  object
 *
 * Get the length of a field.
 *
 * Returns: the size of the corresponding data type has a fixed size, or -1
 */
gint
mg_db_field_get_length (MgDbField *field)
{
	g_return_val_if_fail (field && IS_MG_DB_FIELD (field), -1);
	g_return_val_if_fail (field->priv, -1);

	return field->priv->length;
}

/**
 * mg_db_field_get_scale
 * @field: a #MgDbField  object
 *
 * Set the scale of a field.
 *
 */
void
mg_db_field_set_scale (MgDbField *field, gint scale)
{
	g_return_if_fail (field && IS_MG_DB_FIELD (field));
	g_return_if_fail (field->priv);
	if (scale <= 0)
		field->priv->scale = 0;
	else
		field->priv->scale = scale;

	/* signal the modification */
	mg_base_changed (MG_BASE (field));
}

/**
 * mg_db_field_get_scale
 * @field: a #MgDbField  object
 *
 * Get the scale of a field.
 *
 * Returns: the size of the corresponding data type has a fixed size, or -1
 */
gint
mg_db_field_get_scale (MgDbField *field)
{
	g_return_val_if_fail (field && IS_MG_DB_FIELD (field), -1);
	g_return_val_if_fail (field->priv, -1);

	return field->priv->scale;
}

/**
 * mg_db_field_get_constraints
 * @field: a #MgDbField  object
 *
 * Get all the constraints which impact the given field. Constraints are of several type:
 * NOT NULL, primary key, foreign key, check constrains
 *
 * Returns: a new list of #MgDbConstraint objects
 */
GSList *
mg_db_field_get_constraints (MgDbField *field)
{
	GSList *retval = NULL;
	GSList *list, *table_cons;

	g_return_val_if_fail (field && IS_MG_DB_FIELD (field), NULL);
	g_return_val_if_fail (field->priv, NULL);
	g_return_val_if_fail (field->priv->table, NULL);

	table_cons = mg_db_table_get_constraints (field->priv->table);
	list = table_cons;
	while (list) {
		if (mg_db_constraint_uses_field (MG_DB_CONSTRAINT (list->data), field))
			retval = g_slist_append (retval, list->data);
		list = g_slist_next (list);
	}
	g_slist_free (table_cons);
	
	return retval;
}

/**
* mg_db_field_set_data_type
* @field: a #MgDbField  object
* @type: a #MgServerDataType object
*
* Sets the data type of the field
*/
void
mg_db_field_set_data_type (MgDbField *field, MgServerDataType *type)
{
	g_return_if_fail (field && IS_MG_DB_FIELD (field));
	g_return_if_fail (field->priv);
	g_return_if_fail (type && IS_MG_SERVER_DATA_TYPE (type));

	if (field->priv->data_type)
		g_signal_handlers_disconnect_by_func (G_OBJECT (field->priv->data_type),
						      G_CALLBACK (nullified_object_cb), field);
	field->priv->data_type = type;
	g_signal_connect (G_OBJECT (type), "nullified",
			  G_CALLBACK (nullified_object_cb), field);

	/* signal the modification */
	mg_base_changed (MG_BASE (field));
}


/**
 * mg_db_field_set_default_value
 * @field: a #MgDbField object
 * @value: a #GdaValue value or NULL
 *
 * Sets (or replace) the default value for the field. WARNING: the default value's data type can be
 * different from the field's data type (this is the case for example if the default value is a 
 * function like Postgres's default value for the SERIAL data type).
 */
void
mg_db_field_set_default_value (MgDbField *field, const GdaValue *value)
{
	g_return_if_fail (field && IS_MG_DB_FIELD (field));
	g_return_if_fail (field->priv);

	if (field->priv->default_val) {
		gda_value_free (field->priv->default_val);
		field->priv->default_val = NULL;
	}

	if (value)
		field->priv->default_val = gda_value_copy (value);

	/* signal the modification */
	mg_base_changed (MG_BASE (field));
}

/**
 * mg_db_field_get_default_value
 * @field: a #MgDbField object
 * 
 * Get the default value for the field if ne exists
 *
 * Returns: the default value
 */
const GdaValue *
mg_db_field_get_default_value (MgDbField *field)
{
	g_return_val_if_fail (field && IS_MG_DB_FIELD (field), NULL);
	g_return_val_if_fail (field->priv, NULL);

	return field->priv->default_val;
}

gboolean
mg_db_field_is_null_allowed (MgDbField *field)
{
	gboolean retval = TRUE;
	GSList *list, *table_cons;

	g_return_val_if_fail (field && IS_MG_DB_FIELD (field), FALSE);
	g_return_val_if_fail (field->priv, FALSE);
	g_return_val_if_fail (field->priv->table, FALSE);

	table_cons = mg_db_table_get_constraints (field->priv->table);
	list = table_cons;
	while (list && retval) {
		if ((mg_db_constraint_get_constraint_type (MG_DB_CONSTRAINT (list->data))==CONSTRAINT_NOT_NULL) &&
		    mg_db_constraint_uses_field (MG_DB_CONSTRAINT (list->data), field))
			retval = FALSE;
		list = g_slist_next (list);
	}
	g_slist_free (table_cons);
	
	return retval;
}

gboolean
mg_db_field_is_pkey_part (MgDbField *field)
{
	gboolean retval = FALSE;
	GSList *constraints, *list;

	g_return_val_if_fail (field && IS_MG_DB_FIELD (field), FALSE);
	g_return_val_if_fail (field->priv, FALSE);
	g_return_val_if_fail (field->priv->table, FALSE);

	constraints = mg_db_table_get_constraints (field->priv->table);
	list = constraints;
	while (list && !retval) {
		if ((mg_db_constraint_get_constraint_type (MG_DB_CONSTRAINT (list->data)) == CONSTRAINT_PRIMARY_KEY) &&
		    mg_db_constraint_uses_field (MG_DB_CONSTRAINT (list->data), field))
			retval = TRUE;
		list = g_slist_next (list);
	}
	g_slist_free (constraints);

	return retval;
}

gboolean
mg_db_field_is_pkey_alone (MgDbField *field)
{
	gboolean retval = FALSE;
	GSList *constraints, *list;

	g_return_val_if_fail (field && IS_MG_DB_FIELD (field), FALSE);
	g_return_val_if_fail (field->priv, FALSE);
	g_return_val_if_fail (field->priv->table, FALSE);

	constraints = mg_db_table_get_constraints (field->priv->table);
	list = constraints;
	while (list && !retval) {
		if ((mg_db_constraint_get_constraint_type (MG_DB_CONSTRAINT (list->data)) == CONSTRAINT_PRIMARY_KEY) &&
		    mg_db_constraint_uses_field (MG_DB_CONSTRAINT (list->data), field)) {
			GSList *fields = mg_db_constraint_pkey_get_fields (MG_DB_CONSTRAINT (list->data));
			retval = g_slist_length (fields) == 1 ? TRUE : FALSE;
			g_slist_free (fields);
		}
		list = g_slist_next (list);
	}
	g_slist_free (constraints);

	return retval;
}

gboolean
mg_db_field_is_fkey_part (MgDbField *field)
{
	gboolean retval = FALSE;
	GSList *constraints, *list;

	g_return_val_if_fail (field && IS_MG_DB_FIELD (field), FALSE);
	g_return_val_if_fail (field->priv, FALSE);
	g_return_val_if_fail (field->priv->table, FALSE);

	constraints = mg_db_table_get_constraints (field->priv->table);
	list = constraints;
	while (list && !retval) {
		if ((mg_db_constraint_get_constraint_type (MG_DB_CONSTRAINT (list->data)) == CONSTRAINT_FOREIGN_KEY) &&
		    mg_db_constraint_uses_field (MG_DB_CONSTRAINT (list->data), field))
			retval = TRUE;
		list = g_slist_next (list);
	}
	g_slist_free (constraints);

	return retval;
}

gboolean
mg_db_field_is_fkey_alone (MgDbField *field)
{
	gboolean retval = FALSE;
	GSList *constraints, *list;

	g_return_val_if_fail (field && IS_MG_DB_FIELD (field), FALSE);
	g_return_val_if_fail (field->priv, FALSE);
	g_return_val_if_fail (field->priv->table, FALSE);

	constraints = mg_db_table_get_constraints (field->priv->table);
	list = constraints;
	while (list && !retval) {
		if ((mg_db_constraint_get_constraint_type (MG_DB_CONSTRAINT (list->data)) == CONSTRAINT_FOREIGN_KEY) &&
		    mg_db_constraint_uses_field (MG_DB_CONSTRAINT (list->data), field)) {
			GSList *fields = mg_db_constraint_fkey_get_fields (MG_DB_CONSTRAINT (list->data));
			GSList *list2;
			retval = g_slist_length (fields) == 1 ? TRUE : FALSE;

			list2 = fields;
			while (list2) {
				g_free (list2->data);
				list2 = g_slist_next (list2);
			}
			g_slist_free (fields);
		}
		list = g_slist_next (list);
	}
	g_slist_free (constraints);

	return retval;	
}

#ifdef debug
static void
mg_db_field_dump (MgDbField *field, guint offset)
{
	gchar *str;
	gint i;

	g_return_if_fail (field && IS_MG_DB_FIELD (field));
	
        /* string for the offset */
        str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;

        /* dump */
        if (field->priv)
                g_print ("%s" D_COL_H1 "MgDbField" D_COL_NOR " %s (%p)\n",
                         str, mg_base_get_name (MG_BASE (field)), field);
        else
                g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, field);
}
#endif


/* 
 * MgField interface implementation
 */
static MgEntity *
mg_db_field_get_entity (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_DB_FIELD (iface), NULL);
	g_return_val_if_fail (MG_DB_FIELD (iface)->priv, NULL);

	return MG_ENTITY (MG_DB_FIELD (iface)->priv->table);
}

static MgServerDataType *
mg_db_field_get_data_type (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_DB_FIELD (iface), NULL);
	g_return_val_if_fail (MG_DB_FIELD (iface)->priv, NULL);

	return MG_DB_FIELD (iface)->priv->data_type;
}

static const gchar *
mg_db_field_get_name (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_DB_FIELD (iface), NULL);

	return mg_base_get_name (MG_BASE (iface));
}

static const gchar *
mg_db_field_get_description (MgField *iface)
{
	g_return_val_if_fail (iface && IS_MG_DB_FIELD (iface), NULL);
	
	return mg_base_get_description (MG_BASE (iface));	
}


/* 
 * MgXmlStorage interface implementation
 */
static gchar *
mg_db_field_get_xml_id (MgXmlStorage *iface)
{
	gchar *t_xml_id, *xml_id;

	g_return_val_if_fail (iface && IS_MG_DB_FIELD (iface), NULL);
	g_return_val_if_fail (MG_DB_FIELD (iface)->priv, NULL);

	t_xml_id = mg_xml_storage_get_xml_id (MG_XML_STORAGE (MG_DB_FIELD (iface)->priv->table));
	xml_id = g_strdup_printf ("%s:FI%s", t_xml_id, mg_base_get_name (MG_BASE (iface)));
	g_free (t_xml_id);
	
	return xml_id;
}

static xmlNodePtr
mg_db_field_save_to_xml (MgXmlStorage *iface, GError **error)
{
	xmlNodePtr node = NULL;
	MgDbField *field;
	gchar *str;

	g_return_val_if_fail (iface && IS_MG_DB_FIELD (iface), NULL);
	g_return_val_if_fail (MG_DB_FIELD (iface)->priv, NULL);

	field = MG_DB_FIELD (iface);

	node = xmlNewNode (NULL, "MG_FIELD");
	
	str = mg_db_field_get_xml_id (iface);
	xmlSetProp (node, "id", str);
	g_free (str);
	xmlSetProp (node, "name", mg_base_get_name (MG_BASE (field)));
	if (mg_base_get_owner (MG_BASE (field)))
		xmlSetProp (node, "owner", mg_base_get_owner (MG_BASE (field)));
	xmlSetProp (node, "descr", mg_base_get_description (MG_BASE (field)));
	str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (field->priv->data_type));
	xmlSetProp (node, "type", str);
	g_free (str);
	str = g_strdup_printf ("%d", field->priv->length);
	xmlSetProp (node, "length", str);
	g_free (str);
	str = g_strdup_printf ("%d", field->priv->scale);
	xmlSetProp (node, "scale", str);
	g_free (str);

	if (field->priv->default_val) {
		MgDataHandler *dh;
		GdaValueType vtype;
		
		vtype = gda_value_get_type (field->priv->default_val);
		xmlSetProp (node, "default_gda_type", gda_type_to_string (vtype));

		dh = mg_server_get_handler_by_gda (FIELD_SRV (field), vtype);
		str = mg_data_handler_get_str_from_value (dh, field->priv->default_val);
		xmlSetProp (node, "default", str);
		g_free (str);
	}

	if (mg_server_object_has_handler (FIELD_SRV (field), G_OBJECT (field))) {
		MgDataHandler *dh;
		dh = mg_server_get_object_handler (FIELD_SRV (field), G_OBJECT (field));
		xmlSetProp (node, "plugin", mg_data_handler_get_plugin_name (dh));
	}

	return node;
}

static gboolean
mg_db_field_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgDbField *field;
	gchar *prop;
	gboolean name = FALSE, type = FALSE;

	g_return_val_if_fail (iface && IS_MG_DB_FIELD (iface), FALSE);
	g_return_val_if_fail (MG_DB_FIELD (iface)->priv, FALSE);
	g_return_val_if_fail (node, FALSE);

	field = MG_DB_FIELD (iface);
	if (strcmp (node->name, "MG_FIELD")) {
		g_set_error (error,
			     MG_DB_FIELD_ERROR,
			     MG_DB_FIELD_XML_LOAD_ERROR,
			     _("XML Tag is not <MG_FIELD>"));
		return FALSE;
	}

	prop = xmlGetProp (node, "name");
	if (prop) {
		name = TRUE;
		mg_base_set_name (MG_BASE (field), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "descr");
	if (prop) {
		mg_base_set_description (MG_BASE (field), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "owner");
	if (prop) {
		mg_base_set_owner (MG_BASE (field), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "type");
	if (prop) {
		MgServerDataType *dt = mg_server_get_data_type_by_xml_id (FIELD_SRV (field), prop);
		if (dt) {
			mg_db_field_set_data_type (field, dt);
			type = TRUE;
		}
		g_free (prop);
	}

	prop = xmlGetProp (node, "length");
	if (prop) {
		field->priv->length = atoi (prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "scale");
	if (prop) {
		field->priv->scale = atoi (prop);
		g_free (prop);
	}
	
	prop = xmlGetProp (node, "plugin");
	if (prop) {
		MgDataHandler *dh;
		dh = mg_server_get_handler_by_name (FIELD_SRV (field), prop);
		if (dh) 
			mg_server_set_object_handler (FIELD_SRV (field), G_OBJECT (field), dh);
		g_free (prop);
	}

	prop = xmlGetProp (node, "default");
	if (prop) {
		gchar *str2;

		str2 = xmlGetProp (node, "default_gda_type");
		if (str2) {
			GdaValueType vtype;
			MgDataHandler *dh;
			GdaValue *value;
			
			vtype = gda_type_from_string (str2);			
			dh = mg_server_get_handler_by_gda (FIELD_SRV (field), vtype);
			value = mg_data_handler_get_value_from_str (dh, prop, vtype);
			mg_db_field_set_default_value (field, value);
			gda_value_free (value);

			g_free (str2);
		}
		g_free (prop);
	}



	if (name && type)
		return TRUE;
	else {
		g_set_error (error,
			     MG_DB_FIELD_ERROR,
			     MG_DB_FIELD_XML_LOAD_ERROR,
			     _("Missing required attributes for <MG_FIELD>"));
		return FALSE;
	}
}


/*
 * MgRenderer interface implementation
 */
static GdaXqlItem *
mg_db_field_render_as_xql (MgRenderer *iface, MgContext *context, GError **error)
{
	GdaXqlItem *node = NULL;

	g_return_val_if_fail (iface && IS_MG_DB_FIELD (iface), NULL);
	g_return_val_if_fail (MG_DB_FIELD (iface)->priv, NULL);
	
	TO_IMPLEMENT;
	return node;
}

static gchar *
mg_db_field_render_as_sql (MgRenderer *iface, MgContext *context, GError **error)
{
	gchar *str = NULL;

	g_return_val_if_fail (iface && IS_MG_DB_FIELD (iface), NULL);
	g_return_val_if_fail (MG_DB_FIELD (iface)->priv, NULL);
	
	TO_IMPLEMENT;
	return str;
}

static gchar *
mg_db_field_render_as_str (MgRenderer *iface, MgContext *context)
{
	gchar *str = NULL;

	g_return_val_if_fail (iface && IS_MG_DB_FIELD (iface), NULL);
	g_return_val_if_fail (MG_DB_FIELD (iface)->priv, NULL);
	
	TO_IMPLEMENT;
	return str;
}
