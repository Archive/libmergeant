/* mg-join.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_JOIN_H_
#define __MG_JOIN_H_

#include "mg-base.h"
#include "mg-defs.h"
#include <libgda/libgda.h>

G_BEGIN_DECLS

#define MG_JOIN_TYPE          (mg_join_get_type())
#define MG_JOIN(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_join_get_type(), MgJoin)
#define MG_JOIN_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_join_get_type (), MgJoinClass)
#define IS_MG_JOIN(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_join_get_type ())


/* Interfaces:
 * MgXmlStorage
 * MgRerefer
 */

/* error reporting */
extern GQuark mg_join_error_quark (void);
#define MG_JOIN_ERROR mg_join_error_quark ()

/* different possible types for a query */
typedef enum {
        MG_JOIN_TYPE_INNER,
	MG_JOIN_TYPE_LEFT_OUTER,
	MG_JOIN_TYPE_RIGHT_OUTER,
	MG_JOIN_TYPE_FULL_OUTER,
        MG_JOIN_TYPE_CROSS,
        MG_JOIN_TYPE_LAST
} MgJoinType;

enum
{
	MG_JOIN_XML_LOAD_ERROR,
	MG_JOIN_META_DATA_UPDATE,
	MG_JOIN_FIELDS_ERROR
};


/* struct for the object's data */
struct _MgJoin
{
	MgBase               object;
	MgJoinPrivate       *priv;
};

/* struct for the object's class */
struct _MgJoinClass
{
	MgBaseClass                    class;

	/* signals */
	void   (*type_changed)         (MgJoin *join);
	void   (*condition_changed)    (MgJoin *join);
};

guint           mg_join_get_type          (void);
GObject        *mg_join_new_with_targets  (MgQuery *query, MgTarget *target_1, MgTarget *target_2);
GObject        *mg_join_new_with_xml_ids  (MgQuery *query, const gchar *target_1_xml_id, const gchar *target_2_xml_id);
GObject        *mg_join_new_copy          (MgJoin *orig);

void            mg_join_set_join_type     (MgJoin *join, MgJoinType type);
MgJoinType      mg_join_get_join_type     (MgJoin *join);
MgQuery        *mg_join_get_query         (MgJoin *join);

MgTarget       *mg_join_get_target_1      (MgJoin *join);
MgTarget       *mg_join_get_target_2      (MgJoin *join);
void            mg_join_swap_targets      (MgJoin *join);

MgCondition    *mg_join_get_condition     (MgJoin *join);
const gchar    *mg_join_render_type       (MgJoin *join);

G_END_DECLS

#endif
