/* mg-defs.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "missing.h"

#ifndef __MG_DEFS_H_
#define __MG_DEFS_H_

#ifndef _
/* FIXME for this define... */
#define _(x) (x)
#endif

typedef struct _MgConf MgConf;
typedef struct _MgConfClass MgConfClass;
typedef struct _MgConfPrivate MgConfPrivate;

/*
 * Server part
 */
typedef struct _MgServer MgServer;
typedef struct _MgServerClass MgServerClass;
typedef struct _MgServerPrivate MgServerPrivate;

typedef struct _MgServerDataType MgServerDataType;
typedef struct _MgServerDataTypeClass MgServerDataTypeClass;
typedef struct _MgServerDataTypePrivate MgServerDataTypePrivate;

typedef struct _MgServerFunction MgServerFunction;
typedef struct _MgServerFunctionClass MgServerFunctionClass;
typedef struct _MgServerFunctionPrivate MgServerFunctionPrivate;

typedef struct _MgServerAggregate MgServerAggregate;
typedef struct _MgServerAggregateClass MgServerAggregateClass;
typedef struct _MgServerAggregatePrivate MgServerAggregatePrivate;

typedef struct _MgResultSet MgResultSet;
typedef struct _MgResultSetClass MgResultSetClass;
typedef struct _MgResultSetPrivate MgResultSetPrivate;

typedef struct _MgUser MgUser;
typedef struct _MgUserClass MgUserClass;
typedef struct _MgUserPrivate MgUserPrivate;

/*
 * Database part
 */
typedef struct _MgDatabase MgDatabase;
typedef struct _MgDatabaseClass MgDatabaseClass;
typedef struct _MgDatabasePrivate MgDatabasePrivate;

typedef struct _MgDbTable MgDbTable;
typedef struct _MgDbTableClass MgDbTableClass;
typedef struct _MgDbTablePrivate MgDbTablePrivate;

typedef struct _MgDbField MgDbField;
typedef struct _MgDbFieldClass MgDbFieldClass;
typedef struct _MgDbFieldPrivate MgDbFieldPrivate;

typedef struct _MgDbSequence MgDbSequence;
typedef struct _MgDbSequenceClass MgDbSequenceClass;
typedef struct _MgDbSequencePrivate MgDbSequencePrivate;

typedef struct _MgDbConstraint MgDbConstraint;
typedef struct _MgDbConstraintClass MgDbConstraintClass;
typedef struct _MgDbConstraintPrivate MgDbConstraintPrivate;


/*
 * Query part
*/
typedef struct _MgQuery MgQuery;
typedef struct _MgQueryClass MgQueryClass;
typedef struct _MgQueryPrivate MgQueryPrivate;

typedef struct _MgTarget MgTarget;
typedef struct _MgTargetClass MgTargetClass;
typedef struct _MgTargetPrivate MgTargetPrivate;

typedef struct _MgQfield MgQfield;
typedef struct _MgQfieldClass MgQfieldClass;
typedef struct _MgQfieldPrivate MgQfieldPrivate;

typedef struct _MgJoin MgJoin;
typedef struct _MgJoinClass MgJoinClass;
typedef struct _MgJoinPrivate MgJoinPrivate;

typedef struct _MgContext MgContext;
typedef struct _MgContextClass MgContextClass;
typedef struct _MgContextNode MgContextNode;
typedef struct _MgContextPrivate MgContextPrivate;

typedef struct _MgParameter MgParameter;
typedef struct _MgParameterClass MgParameterClass;
typedef struct _MgParameterPrivate MgParameterPrivate;

typedef struct _MgCondition MgCondition;
typedef struct _MgConditionClass MgConditionClass;
typedef struct _MgConditionPrivate MgConditionPrivate;

typedef struct _MgQfAll MgQfAll;
typedef struct _MgQfAllClass MgQfAllClass;
typedef struct _MgQfAllPrivate MgQfAllPrivate;

typedef struct _MgQfField MgQfField;
typedef struct _MgQfFieldClass MgQfFieldClass;
typedef struct _MgQfFieldPrivate MgQfFieldPrivate;

typedef struct _MgQfValue MgQfValue;
typedef struct _MgQfValueClass MgQfValueClass;
typedef struct _MgQfValuePrivate MgQfValuePrivate;

typedef struct _MgQfFunc MgQfFunc;
typedef struct _MgQfFuncClass MgQfFuncClass;
typedef struct _MgQfFuncPrivate MgQfFuncPrivate;

/* 
 * Interfaces 
 */
typedef struct _MgXmlStorage       MgXmlStorage;
typedef struct _MgXmlStorageIface  MgXmlStorageIface;

typedef struct _MgDataHandler      MgDataHandler;
typedef struct _MgDataHandlerIface MgDataHandlerIface;

typedef struct _MgDataEntry        MgDataEntry;
typedef struct _MgDataEntryIface   MgDataEntryIface;

typedef struct _MgRenderer         MgRenderer;
typedef struct _MgRendererIface    MgRendererIface;

typedef struct _MgReferer          MgReferer;
typedef struct _MgRefererIface     MgRefererIface;

typedef struct _MgEntity           MgEntity;
typedef struct _MgEntityIface      MgEntityIface;

typedef struct _MgField            MgField;
typedef struct _MgFieldIface       MgFieldIface;


#ifdef debug
#define D_COL_NOR "\033[0m"
#define D_COL_H0 "\033[;34;7m"
#define D_COL_H1 "\033[;36;7m"
#define D_COL_H2 "\033[;36;4m"
#define D_COL_OK "\033[;32m"
#define D_COL_ERR "\033[;31;1m"
#define AAA(X) g_print (D_COL_H1 "DEBUG MARK %d\n" D_COL_NOR, X)
#endif

#ifdef debug
#define TO_IMPLEMENT g_print (D_COL_ERR "Implementation missing:" D_COL_NOR " %s() in %s line %d\n", __FUNCTION__, __FILE__,__LINE__)
#else
#define TO_IMPLEMENT g_print ("Implementation missing: %s() in %s line %d\n", __FUNCTION__, __FILE__,__LINE__)
#endif

#endif
