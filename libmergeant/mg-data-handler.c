/* mg-data-handler.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-data-handler.h"
#include "marshal.h"


/* signals */
enum
{
	DUMMY,
	LAST_SIGNAL
};

static gint mg_data_handler_signals[LAST_SIGNAL] = { 0 };
static void mg_data_handler_iface_init (gpointer g_class);

guint
mg_data_handler_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgDataHandlerIface),
			(GBaseInitFunc) mg_data_handler_iface_init,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) NULL,
			NULL,
			NULL,
			0,
			0,
			(GInstanceInitFunc) NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, "MgDataHandler", &info, 0);
	}
	return type;
}


static void
mg_data_handler_iface_init (gpointer g_class)
{
	static gboolean initialized = FALSE;

	if (! initialized) {
		initialized = TRUE;
	}
}

/**
 * mg_data_handler_get_entry_from_value
 * @dh: an object which implements the #MgDataHandler interface
 * @value: the original value to display or %NULL
 * @type: the requested data type (if @value is not %NULL or of type NULL, then this parameter is ignored)
 *
 * Create a new MgDataEntry widget to edit the given value. If the value is NULL or of
 * type GDA_VALUE_TYPE_NULL, then the type argument is used and determines the real requested
 * type (it is otherwise ignored).
 *
 * Also, if the value is NULL or of type GDA_VALUE_TYPE_NULL, then the initial edited value in the
 * widget will be the sane initial value provided by the MgDataHandler object.
 *
 * Returns: the new widget
 */
MgDataEntry *
mg_data_handler_get_entry_from_value (MgDataHandler *dh, const GdaValue *value, GdaValueType type)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);

	if (!value || gda_value_is_null (value))
		g_return_val_if_fail (mg_data_handler_accepts_gda_type (MG_DATA_HANDLER (dh), type), NULL);

	if (MG_DATA_HANDLER_GET_IFACE (dh)->get_entry_from_value)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->get_entry_from_value) (dh, value, type);
	
	return NULL;
}

/**
 * mg_data_handler_get_sql_from_value
 * @dh: an object which implements the #MgDataHandler interface
 * @value: the value to be converted to a string
 *
 * Creates a new string which is an SQL representation of the given value. If the value is NULL or
 * is of type GDA_VALUE_TYPE_NULL, the returned string is NULL.
 *
 * Returns: the new string.
 */
gchar *
mg_data_handler_get_sql_from_value (MgDataHandler *dh, const GdaValue *value)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);
	
	if (! value || gda_value_is_null (value))
		return NULL;

	/* Calling the real function with value != NULL and not of type GDA_VALUE_TYPE_NULL */
	if (MG_DATA_HANDLER_GET_IFACE (dh)->get_sql_from_value)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->get_sql_from_value) (dh, value);
	
	return NULL;
}

/**
 * mg_data_handler_get_str_from_value
 * @dh: an object which implements the #MgDataHandler interface
 * @value: the value to be converted to a string
 *
 * Creates a new string which is a "user friendly" representation of the given value (usually
 * it will be in the users's locale, specially for the dates). If the value is 
 * NULL or is of type GDA_VALUE_TYPE_NULL, the returned string is a copy of "" (empty string).
 *
 * Returns: the new string.
 */
gchar *
mg_data_handler_get_str_from_value (MgDataHandler *dh, const GdaValue *value)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);

	if (! value || gda_value_is_null (value))
		return g_strdup ("");

	/* Calling the real function with value != NULL and not of type GDA_VALUE_TYPE_NULL */
	if (MG_DATA_HANDLER_GET_IFACE (dh)->get_str_from_value)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->get_str_from_value) (dh, value);
	
	return NULL;
}

/**
 * mg_data_handler_get_value_from_sql
 * @dh: an object which implements the #MgDataHandler interface
 * @sql:
 * @type: 
 *
 * Creates a new GdaValue which represents the SQL value given as argument. This is
 * the opposite of the function mg_data_handler_get_sql_from_value(). The type argument
 * is used to determine the real data type requested for the returned value.
 *
 * If the sql string is NULL, then the returned GdaValue is of type GDA_VALUE_TYPE_NULL;
 * if the sql string does not correspond to a valid SQL string for the requested type, then
 * NULL is returned.
 *
 * Returns: the new GdaValue or NULL on error
 */
GdaValue *
mg_data_handler_get_value_from_sql (MgDataHandler *dh, const gchar *sql, GdaValueType type)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (mg_data_handler_accepts_gda_type (MG_DATA_HANDLER (dh), type), NULL);

	if (!sql)
		return gda_value_new_null ();

	if (MG_DATA_HANDLER_GET_IFACE (dh)->get_value_from_sql)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->get_value_from_sql) (dh, sql, type);
	
	return NULL;
}


/**
 * mg_data_handler_get_value_from_str
 * @dh: an object which implements the #MgDataHandler interface
 * @str:
 * @type: 
 *
 * Creates a new GdaValue which represents the STR value given as argument. This is
 * the opposite of the function mg_data_handler_get_str_from_value(). The type argument
 * is used to determine the real data type requested for the returned value.
 *
 * If the str string is NULL, then the returned GdaValue is of type GDA_VALUE_TYPE_NULL;
 * if the str string does not correspond to a valid STR string for the requested type, then
 * NULL is returned.
 *
 * Returns: the new GdaValue or NULL on error
 */
GdaValue *
mg_data_handler_get_value_from_str (MgDataHandler *dh, const gchar *str, GdaValueType type)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (mg_data_handler_accepts_gda_type (MG_DATA_HANDLER (dh), type), NULL);

	if (!str)
		return gda_value_new_null ();

	if (MG_DATA_HANDLER_GET_IFACE (dh)->get_value_from_str)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->get_value_from_str) (dh, str, type);
	else {
		/* if the get_value_from_str() method is not implemented, then we try the
		   get_value_from_sql() method */
		if (MG_DATA_HANDLER_GET_IFACE (dh)->get_value_from_sql)
			return (MG_DATA_HANDLER_GET_IFACE (dh)->get_value_from_sql) (dh, str, type);
	}
	
	return NULL;
}


/**
 * mg_data_handler_get_sane_init_value
 * @dh: an object which implements the #MgDataHandler interface
 * @type: 
 *
 * Creates a new GdaValue which holds a sane initial value to be used if no value is specifically
 * provided. For example for a simple string, this would return gda_value_new_string ("").
 *
 * Returns: the new GdaValue.
 */
GdaValue *
mg_data_handler_get_sane_init_value (MgDataHandler *dh, GdaValueType type)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (mg_data_handler_accepts_gda_type (MG_DATA_HANDLER (dh), type), NULL);

	if (MG_DATA_HANDLER_GET_IFACE (dh)->get_sane_init_value)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->get_sane_init_value) (dh, type);
	
	return NULL;
}

/**
 * mg_data_handler_get_nb_gda_types
 * @dh: an object which implements the #MgDataHandler interface
 *
 * Get the number of GdaValueType types the MgDataHandler can handle correctly
 *
 * Returns: the number.
 */
guint
mg_data_handler_get_nb_gda_types (MgDataHandler *dh)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), 0);

	if (MG_DATA_HANDLER_GET_IFACE (dh)->get_nb_gda_types)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->get_nb_gda_types) (dh);
	
	return 0;
}

/**
 * mg_data_handler_accepts_gda_type
 * @dh: an object which implements the #MgDataHandler interface
 * @type:
 *
 * Checks wether the MgDataHandler is able to handle the gda type given as argument.
 *
 * Returns: TRUE if the gda type can be handled
 */
gboolean
mg_data_handler_accepts_gda_type (MgDataHandler *dh, GdaValueType type)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), FALSE);

	if (MG_DATA_HANDLER_GET_IFACE (dh)->accepts_gda_type)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->accepts_gda_type) (dh, type);
	
	return FALSE;
}

/**
 * mg_data_handler_get_gda_type_index
 * @dh: an object which implements the #MgDataHandler interface
 * @index: 
 *
 * Get the GdaValueType handled by the MgDataHandler, at the given position (starting at zero).
 *
 * Returns: the GdaValueType
 */
GdaValueType
mg_data_handler_get_gda_type_index (MgDataHandler *dh, guint index)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), GDA_VALUE_TYPE_UNKNOWN);
	g_return_val_if_fail (index < mg_data_handler_get_nb_gda_types (dh),
			      GDA_VALUE_TYPE_UNKNOWN);

	if (MG_DATA_HANDLER_GET_IFACE (dh)->get_gda_type_index)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->get_gda_type_index) (dh, index);
	
	return GDA_VALUE_TYPE_UNKNOWN;
}

/**
 * mg_data_handler_get_descr
 * @dh: an object which implements the #MgDataHandler interface
 *
 * Get a short description of the MgDataHandler
 *
 * Returns: the description
 */
const gchar *
mg_data_handler_get_descr (MgDataHandler *dh)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);

	if (MG_DATA_HANDLER_GET_IFACE (dh)->get_descr)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->get_descr) (dh);
	
	return NULL;
}

/**
 * mg_data_handler_get_descr_detail
 * @dh: an object which implements the #MgDataHandler interface
 *
 * Get a detailled description of the MgDataHandler
 *
 * Returns: the description
 */
const gchar *
mg_data_handler_get_descr_detail (MgDataHandler *dh)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);

	if (MG_DATA_HANDLER_GET_IFACE (dh)->get_descr_detail)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->get_descr_detail) (dh);
	
	return NULL;
}

/**
 * mg_data_handler_get_version
 * @dh: an object which implements the #MgDataHandler interface
 *
 * Get the MgDataHandler's version
 *
 * Returns: the version
 */
const gchar *
mg_data_handler_get_version (MgDataHandler *dh)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);

	if (MG_DATA_HANDLER_GET_IFACE (dh)->get_version)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->get_version) (dh);
	
	return NULL;
}

/**
 * mg_data_handler_is_plugin
 * @dh: an object which implements the #MgDataHandler interface
 *
 * Get wether the MgDataHandler is a plugin or not
 *
 * Returns: TRUE if the MgDataHandler is a plugin
 */
gboolean
mg_data_handler_is_plugin (MgDataHandler *dh)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), FALSE);

	if (MG_DATA_HANDLER_GET_IFACE (dh)->is_plugin)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->is_plugin) (dh);
	
	return FALSE;
}

/**
 * mg_data_handler_get_plugin_name
 * @dh: an object which implements the #MgDataHandler interface
 *
 * Get the name of the MgDataHandler if it is a plugin
 *
 * Returns: the name of the MgDataHandler if it is a plugin or NULL
 */
const gchar *mg_data_handler_get_plugin_name (MgDataHandler *dh)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);

	if (MG_DATA_HANDLER_GET_IFACE (dh)->get_plugin_name)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->get_plugin_name) (dh);
	
	return NULL;
}

/**
 * mg_data_handler_get_plugin_file
 * @dh: an object which implements the #MgDataHandler interface
 *
 * Get the file name (.so on Unix) of the MgDataHandler if it is a plugin
 *
 * Returns: the file name of the MgDataHandler if it is a plugin or NULL
 */
const gchar *
mg_data_handler_get_plugin_file (MgDataHandler *dh)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);

	if (MG_DATA_HANDLER_GET_IFACE (dh)->get_plugin_file)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->get_plugin_file) (dh);
	
	return NULL;
}

/**
 * mg_data_handler_get_key
 * @dh: an object which implements the #MgDataHandler interface
 *
 * Get a unique identifier for the MgDataHandler
 *
 * Returns: the identifier
 */
gchar *
mg_data_handler_get_key (MgDataHandler *dh)
{
	g_return_val_if_fail (dh && IS_MG_DATA_HANDLER (dh), NULL);

	if (MG_DATA_HANDLER_GET_IFACE (dh)->get_key)
		return (MG_DATA_HANDLER_GET_IFACE (dh)->get_key) (dh);
	
	return NULL;
}
