/* mg-base.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <string.h>
#include "mg-base.h"
#include "marshal.h"
#include "mg-conf.h"

/* 
 * Main static functions 
 */
static void mg_base_class_init (MgBaseClass * class);
static void mg_base_init (MgBase * srv);
static void mg_base_dispose (GObject   * object);
static void mg_base_finalize (GObject   * object);

static void mg_base_set_property    (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec);
static void mg_base_get_property    (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	CHANGED,
	ID_CHANGED,
	NAME_CHANGED,
	DESCR_CHANGED,
	OWNER_CHANGED,
	NULLIFIED,
	LAST_SIGNAL
};

static gint mg_base_signals[LAST_SIGNAL] = { 0, 0, 0, 0};

/* properties */
enum
{
	PROP_0,
	PROP_CONF,
};


struct _MgBasePrivate
{
	MgConf            *conf;     /* property: NOT NULL to use MgBase object */
	guint              id;	     /* 0 or UNIQUE, comes from ConfManager->id_serial */

	gchar             *name;
	gchar             *descr;
	gchar             *owner;
	
	gboolean           nullified;/* TRUE if the object has been "nullified" */
};

guint
mg_base_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgBaseClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_base_class_init,
			NULL,
			NULL,
			sizeof (MgBase),
			0,
			(GInstanceInitFunc) mg_base_init
		};
		
		type = g_type_register_static (G_TYPE_OBJECT, "MgBase", &info, 0);
	}
	return type;
}

static void
mg_base_class_init (MgBaseClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_base_signals[CHANGED] =
		g_signal_new ("changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgBaseClass, changed),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	mg_base_signals[NAME_CHANGED] =
		g_signal_new ("name_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgBaseClass, name_changed),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	mg_base_signals[ID_CHANGED] =
		g_signal_new ("id_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgBaseClass, id_changed),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	mg_base_signals[DESCR_CHANGED] =
		g_signal_new ("descr_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgBaseClass, descr_changed),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	mg_base_signals[OWNER_CHANGED] =
		g_signal_new ("owner_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgBaseClass, owner_changed),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	mg_base_signals[NULLIFIED] =
		g_signal_new ("nullified",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgBaseClass, nullified),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	class->changed = NULL;
	class->name_changed = mg_base_changed;
	class->id_changed = mg_base_changed;
	class->descr_changed = mg_base_changed;
	class->owner_changed = mg_base_changed;
	class->nullified = NULL;


	/* virtual functions */
	class->nullify = NULL;
#ifdef debug
	class->dump = NULL;
#endif

	object_class->dispose = mg_base_dispose;
	object_class->finalize = mg_base_finalize;

	/* Properties */
	object_class->set_property = mg_base_set_property;
	object_class->get_property = mg_base_get_property;
	g_object_class_install_property (object_class, PROP_CONF,
					 g_param_spec_pointer ("conf", NULL, NULL, (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static void
mg_base_init (MgBase * base)
{
	base->priv = g_new0 (MgBasePrivate, 1);
	base->priv->conf = NULL;
	base->priv->nullified = FALSE;
	base->priv->id = 0;

	base->priv->name = NULL;
	base->priv->descr = NULL;
	base->priv->owner = NULL;
}

/**
 * mg_base_new
 *
 * Creates a new MgBase object. This object class is normally not instantiated
 * directly but through child classes objects' intantiation
 * 
 * Returns: the newly created object
 */
GObject   *
mg_base_new ()
{
	GObject   *obj;

	MgBase *base;
	obj = g_object_new (MG_BASE_TYPE, NULL);
	base = MG_BASE (obj);

	return obj;
}


static void
mg_base_dispose (GObject   * object)
{
	MgBase *base;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_BASE (object));

	base = MG_BASE (object);
	if (base->priv) {
		if (! base->priv->nullified)
			mg_base_nullify (base);

		if (base->priv->conf) {
			g_object_remove_weak_pointer (G_OBJECT (base->priv->conf),
						      (gpointer) & (base->priv->conf));
			base->priv->conf = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_base_finalize (GObject   * object)
{
	MgBase *base;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_BASE (object));

	base = MG_BASE (object);
	if (base->priv) {
		if (! base->priv->nullified) {
			g_warning ("MgBase::finalize(%p) not nullified!\n", base);
		}

		if (base->priv->name)
			g_free (base->priv->name);
		if (base->priv->descr)
			g_free (base->priv->descr);
		if (base->priv->owner)
			g_free (base->priv->owner);

		g_free (base->priv);
		base->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_base_set_property    (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	gpointer ptr;
	MgBase *base;

	base = MG_BASE (object);
	if (base->priv) {
		switch (param_id) {
		case PROP_CONF:
			ptr = g_value_get_pointer (value);
			mg_base_set_conf (base, MG_CONF (ptr));
			break;
		}
	}
}

static void
mg_base_get_property    (GObject              *object,
			guint                 param_id,
			GValue               *value,
			GParamSpec           *pspec)
{
	TO_IMPLEMENT;
}

/**
 * mg_base_set_conf
 * @base: a #MgBase object
 * @conf: the #MgConf to which @base is attached
 * 
 * Sets the #MgConf to which @base is attached to. It has no other
 * action than to store it to easily be able to retreive it.
 */
void
mg_base_set_conf (MgBase *base, MgConf *conf)
{
	g_return_if_fail (base && IS_MG_BASE (base));
	g_return_if_fail (base->priv);
	g_return_if_fail (conf && IS_MG_CONF (conf));

	if (base->priv->conf) {
		g_object_remove_weak_pointer (G_OBJECT (base->priv->conf),
					      (gpointer) & (base->priv->conf));
		base->priv->conf = NULL;
	}

	base->priv->conf = conf;
	g_object_add_weak_pointer (G_OBJECT (base->priv->conf),
				   (gpointer) & (base->priv->conf));
}

/**
 * mg_base_get_conf
 * @base: a #MgBase object
 *
 * Fetch the corresponding #MgConf object.
 *
 * Returns: the #MgConf object to which @base is attached to
 */ 
MgConf *
mg_base_get_conf (MgBase *base)
{
	g_return_val_if_fail (base && IS_MG_BASE (base), NULL);
	g_return_val_if_fail (base->priv, NULL);

	return base->priv->conf;
}


/**
 * mg_base_set_id
 * @base: a #MgBase object
 * @id: 
 * 
 * Sets the id of the object. The id is set to 0 by default (which means the object does
 * not use the id at all).
 * WARNING: the id is not checked which means no search is
 * made to see if the id is already used
 */
void
mg_base_set_id (MgBase *base, guint id)
{
	g_return_if_fail (base && IS_MG_BASE (base));
	g_return_if_fail (base->priv);

	if (base->priv->id != id) {
		base->priv->id = id;
#ifdef debug_signal
		g_print (">> 'ID_CHANGED' from %s %p\n", G_OBJECT_TYPE_NAME (base), base);
#endif
		g_signal_emit (G_OBJECT (base), mg_base_signals[ID_CHANGED], 0);
#ifdef debug_signal
		g_print ("<< 'ID_CHANGED' from %s %p\n", G_OBJECT_TYPE_NAME (base), base);
#endif
	}
}

/**
 * mg_base_set_name
 * @base: a #MgBase object
 * @name: 
 *
 * Sets the name of the MgBase object. If the name is changed, then the 
 * "name_changed" signal is emitted.
 * 
 */
void
mg_base_set_name (MgBase *base, const gchar *name)
{
	gboolean changed = FALSE;

	g_return_if_fail (base && IS_MG_BASE (base));
	g_return_if_fail (base->priv);

	if (name) {
		if (base->priv->name) {
			changed = strcmp (base->priv->name, name) ? TRUE : FALSE;
			g_free (base->priv->name);
		}
		else
			changed = TRUE;
		base->priv->name = g_strdup (name);
	}

	if (changed) {
#ifdef debug_signal
		g_print (">> 'NAME_CHANGED' from %s %p\n", G_OBJECT_TYPE_NAME (base), base);
#endif
		g_signal_emit (G_OBJECT (base), mg_base_signals[NAME_CHANGED], 0);
#ifdef debug_signal
		g_print ("<< 'NAME_CHANGED' from %s %p\n", G_OBJECT_TYPE_NAME (base), base);
#endif
	}
}


/**
 * mg_base_set_description
 * @base: a #MgBase object
 * @descr: 
 *
 * Sets the description of the MgBase object. If the description is changed, then the 
 * "descr_changed" signal is emitted.
 * 
 */
void
mg_base_set_description (MgBase *base, const gchar *descr)
{
	gboolean changed = FALSE;

	g_return_if_fail (base && IS_MG_BASE (base));
	g_return_if_fail (base->priv);

	if (descr) {
		if (base->priv->descr) {
			changed = strcmp (base->priv->descr, descr) ? TRUE : FALSE;
			g_free (base->priv->descr);
		}
		else
			changed = TRUE;
		base->priv->descr = g_strdup (descr);
	}

	if (changed) {
#ifdef debug_signal
		g_print (">> 'DESCR_CHANGED' from mg_base_set_descr (%s)\n", base->priv->descr);
#endif
		g_signal_emit (G_OBJECT (base), mg_base_signals[DESCR_CHANGED], 0);
#ifdef debug_signal
		g_print ("<< 'DESCR_CHANGED' from mg_base_set_descr (%s)\n", base->priv->descr);
#endif
	}
}


/**
 * mg_base_set_owner
 * @base: a #MgBase object
 * @owner: 
 *
 * Sets the owner of the MgBase object. If the owner is changed, then the 
 * "owner_changed" signal is emitted.
 * 
 */
void
mg_base_set_owner (MgBase *base, const gchar *owner)
{
	gboolean changed = FALSE;

	g_return_if_fail (base && IS_MG_BASE (base));
	g_return_if_fail (base->priv);

	if (owner) {
		if (base->priv->owner) {
			changed = strcmp (base->priv->owner, owner) ? TRUE : FALSE;
			g_free (base->priv->owner);
		}
		else
			changed = TRUE;
		base->priv->owner = g_strdup (owner);
	}

	if (changed) {
#ifdef debug_signal
		g_print (">> 'OWNER_CHANGED' from mg_base_set_descr (%s)\n", base->priv->descr);
#endif
		g_signal_emit (G_OBJECT (base), mg_base_signals[OWNER_CHANGED], 0);
#ifdef debug_signal
		g_print ("<< 'OWNER_CHANGED' from mg_base_set_descr (%s)\n", base->priv->descr);
#endif
	}
}

/**
 * mg_base_get_id
 * @base: a #MgBase object
 * 
 * Fetch the id of the MgBase object.
 * 
 * Returns: the id.
 */
guint
mg_base_get_id (MgBase *base)
{
	g_return_val_if_fail (base && IS_MG_BASE (base), 0);
	g_return_val_if_fail (base->priv, 0);
		
	return base->priv->id;
}


/**
 * mg_base_get_name
 * @base: a #MgBase object
 * 
 * Fetch the name of the MgBase object.
 * 
 * Returns: the object's name.
 */
const gchar *
mg_base_get_name (MgBase *base)
{
	g_return_val_if_fail (base && IS_MG_BASE (base), NULL);
	g_return_val_if_fail (base->priv, NULL);

	return base->priv->name;
}


/**
 * mg_base_get_description
 * @base: a #MgBase object
 * 
 * Fetch the description of the MgBase object.
 * 
 * Returns: the object's description.
 */
const gchar *
mg_base_get_description (MgBase *base)
{
	g_return_val_if_fail (base && IS_MG_BASE (base), NULL);
	g_return_val_if_fail (base->priv, NULL);

	return base->priv->descr;
}

/**
 * mg_base_get_owner
 * @base: a #MgBase object
 * 
 * Fetch the owner of the MgBase object.
 * 
 * Returns: the object's owner.
 */
const gchar *
mg_base_get_owner (MgBase *base)
{
	g_return_val_if_fail (base && IS_MG_BASE (base), NULL);
	g_return_val_if_fail (base->priv, NULL);

	return base->priv->owner;
}



/**
 * mg_base_nullify
 * @base: a #MgBase object
 *
 * Force the object to completely clean itself (it will first emit the "nullified"
 * signal). Sometimes objects need to disappear even if they are referenced by other
 * objects; in this case they emit the "nullified" signal to tell the other objects
 * to leave their reference on the object (which is then destroyed).
 */
void
mg_base_nullify (MgBase *base)
{
	g_return_if_fail (base && IS_MG_BASE (base));

	if (base->priv) {
		if (!base->priv->nullified) {
			MgBaseClass *class;
			class = MG_BASE_CLASS (G_OBJECT_GET_CLASS (base));

			base->priv->nullified = TRUE;
#ifdef debug_signal
			g_print (">> 'NULLIFIED' from %s %p\n", G_OBJECT_TYPE_NAME (base), base);
#endif
			g_signal_emit (G_OBJECT (base), mg_base_signals[NULLIFIED], 0);
#ifdef debug_signal
			g_print ("<< 'NULLIFIED' from %s %p\n", G_OBJECT_TYPE_NAME (base), base);
#endif

			if (class->nullify)
				(*class->nullify) (base);
		}
		else 
			g_warning ("MgBase::nullify called on already nullified object %p, "
				   "of type %s\n", base, G_OBJECT_TYPE_NAME (base));
	}
}

/**
 * mg_base_nullify_check
 * @base: a #MgBase object
 *
 * Checks that the object has been nullified, and if not, then calls mg_base_nullify() on it.
 * This is usefull for objects inheriting the #MGBase object to be called first line in their
 * dispose() method.
 */
void
mg_base_nullify_check (MgBase *base)
{
	g_return_if_fail (base && IS_MG_BASE (base));

	if (base->priv && !base->priv->nullified)
		mg_base_nullify (base);
}

/**
 * mg_base_changed
 * @base: a #MgBase object
 *
 * Force emission of the "changed" signal.
 */
void
mg_base_changed (MgBase *base)
{
	g_return_if_fail (base && IS_MG_BASE (base));
	g_return_if_fail (base->priv);

#ifdef debug_signal
	g_print (">> 'CHANGED' from %s %p\n", G_OBJECT_TYPE_NAME (base), base);
#endif
	g_signal_emit (G_OBJECT (base), mg_base_signals[CHANGED], 0);
#ifdef debug_signal
	g_print ("<< 'CHANGED' from %s %p\n", G_OBJECT_TYPE_NAME (base), base);
#endif
}

#ifdef debug
/**
 * mg_base_dump
 * @base: a #MgBase object
 * @offset: the offset (in caracters) at which the dump will start
 *
 * Writes a textual description of the object to STDOUT. This function only
 * exists if libergeant is compiled with the "--enable-debug" option. This is
 * a virtual function.
 */
void
mg_base_dump (MgBase *base, guint offset)
{
	gchar *str;
	guint i;

	g_return_if_fail (base);
	g_return_if_fail (IS_MG_BASE (base));

	/* string for the offset */
	str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;
	
	/* dump */
	if (base->priv) {
		MgBaseClass *class;
		class = MG_BASE_CLASS (G_OBJECT_GET_CLASS (base));
		if (class->dump)
			(class->dump) (base, offset);
		else 
			g_print ("%s" D_COL_H1 "%s %p (name=%s, id=%d)\n" D_COL_NOR, 
				 str, G_OBJECT_TYPE_NAME (base), base, base->priv->name,
				 base->priv->id);
	}
	else
		g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, base);

	g_free (str);
}
#endif
