/* mg-qfield.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <string.h>
#include "mg-qfield.h"
#include "mg-query.h"
#include "mg-field.h"
#include "mg-qf-all.h"
#include "mg-qf-field.h"
#include "mg-qf-value.h"
#include "mg-qf-func.h"
#include "mg-xml-storage.h"
#include "mg-ref-base.h"
#include "mg-server.h"

/* 
 * Main static functions 
 */
static void mg_qfield_class_init (MgQfieldClass *class);
static void mg_qfield_init (MgQfield *qfield);
static void mg_qfield_dispose (GObject *object);
static void mg_qfield_finalize (GObject *object);

static void mg_qfield_set_property    (GObject              *object,
				       guint                 param_id,
				       const GValue         *value,
				       GParamSpec           *pspec);
static void mg_qfield_get_property    (GObject              *object,
				       guint                 param_id,
				       GValue               *value,
				       GParamSpec           *pspec);

static void attach_to_query (MgQfield *qfield, MgQuery *query);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	LAST_SIGNAL
};

static gint mg_qfield_signals[LAST_SIGNAL] = { };

/* properties */
enum
{
	PROP_0,
	PROP
};


struct _MgQfieldPrivate
{
	gchar     *alias;
	gboolean   visible;
	gboolean   internal;
};

/* module error */
GQuark mg_qfield_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_qfield_error");
	return quark;
}


guint
mg_qfield_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgQfieldClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_qfield_class_init,
			NULL,
			NULL,
			sizeof (MgQfield),
			0,
			(GInstanceInitFunc) mg_qfield_init
		};
		
		type = g_type_register_static (MG_BASE_TYPE, "MgQfield", &info, 0);
	}
	return type;
}

static void
mg_qfield_class_init (MgQfieldClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);



	/* virtual functions */
	class->copy = NULL;
	class->is_equal = NULL;

	object_class->dispose = mg_qfield_dispose;
	object_class->finalize = mg_qfield_finalize;

	/* Properties */
	object_class->set_property = mg_qfield_set_property;
	object_class->get_property = mg_qfield_get_property;
	g_object_class_install_property (object_class, PROP,
					 g_param_spec_pointer ("prop", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static void
mg_qfield_init (MgQfield *qfield)
{
	qfield->priv = g_new0 (MgQfieldPrivate, 1);
	qfield->priv->alias = NULL;
	qfield->priv->visible = TRUE;
	qfield->priv->internal = FALSE;
}

/**
 * mg_qfield_new_from_xml
 * @query: a #MgQuery object
 * @node: an XML node corresponding to a <MG_QFIELD> tag
 * @error: location to store error, or %NULL
 *
 * This is an object factory which does create instances of class inheritants of the #MgDfield class.
 * Ths #MgQfield object MUST then be attached to @query
 * 
 * Returns: the newly created object
 */
GObject   *
mg_qfield_new_from_xml (MgQuery *query, xmlNodePtr node, GError **error)
{
	GObject *obj = NULL;
	gchar *prop;

	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (node, NULL);
	g_return_val_if_fail (!strcmp (node->name, "MG_QF"), NULL);

	prop = xmlGetProp (node, "type");
	if (prop) {
		gboolean done = FALSE;
		if (!strcmp (prop, "ALL")) {
			gchar *target;

			done = TRUE;
			target = xmlGetProp (node, "target");
			if (target) {
				obj = mg_qf_all_new_with_xml_id (query, target);
				g_free (target);
			}
			else {
				g_set_error (error,
					     MG_QF_ALL_ERROR,
					     MG_QF_ALL_XML_LOAD_ERROR,
					     _("Missing 'target' attribute in <MG_QF>"));
				return NULL;
			}
		}

		if (!done && !strcmp (prop, "FIELD")) {
			gchar *target, *field;

			done = TRUE;
			target = xmlGetProp (node, "target");
			field = xmlGetProp (node, "object");
			if (target && field) 
				obj = mg_qf_field_new_with_xml_ids (query, target, field);

			if (target)
				g_free (target);
			if (field)
				g_free (field);

			if (!obj) {
				g_set_error (error,
					     MG_QF_ALL_ERROR,
					     MG_QF_ALL_XML_LOAD_ERROR,
					     _("Missing 'target' attribute in <MG_QF>"));
				return NULL;
			}

		}

		if (!done && !strcmp (prop, "AGG")) {
			TO_IMPLEMENT;
		}

		if (!done && !strcmp (prop, "FUNC")) {
			gchar *func;

			func = xmlGetProp (node, "object");
			if (func) {
				obj = mg_qf_func_new_with_xml_id (query, func);
				g_free (func);
			}
			
			if (!obj) {
				g_set_error (error,
					     MG_QF_ALL_ERROR,
					     MG_QF_ALL_XML_LOAD_ERROR,
					     _("Missing 'object' attribute in <MG_QF>"));
				return NULL;
			}
		}

		if (!done && !strcmp (prop, "VAL")) {
			gchar *srvt;

			done = TRUE;
			srvt = xmlGetProp (node, "srv_type");
			if (srvt) {
				MgServerDataType *dt;

				dt = mg_server_get_data_type_by_xml_id (mg_conf_get_server (mg_base_get_conf (MG_BASE (query))),
									srvt);
				if (dt)
					obj = mg_qf_value_new (query, dt);
				else {
					g_set_error (error,
						     MG_QF_ALL_ERROR,
						     MG_QF_ALL_XML_LOAD_ERROR,
						     _("Can't find data type %s for query field"), srvt);
					return NULL;
				}
				g_free (srvt);
			}
			else {
				g_set_error (error,
					     MG_QF_ALL_ERROR,
					     MG_QF_ALL_XML_LOAD_ERROR,
					     _("Missing 'srv_type' attribute for VALUE query field"));
				return NULL;
			}
					
		}

		g_free (prop);

		if (obj) {
			attach_to_query (MG_QFIELD (obj), query);
			if (!mg_xml_storage_load_from_xml (MG_XML_STORAGE (obj), node, error))
				return NULL;
		}
		else 
			g_set_error (error,
				     MG_QF_ALL_ERROR,
				     MG_QF_ALL_XML_LOAD_ERROR,
				     _("Missing Implementation in loading <MG_QF>"));
	}
	else {
		g_set_error (error,
			     MG_QFIELD_ERROR,
			     MG_QFIELD_XML_LOAD_ERROR,
			     _("Unknown value for 'type' attribute in <MG_QF>"));
		return NULL;
	}

	return obj;
}


/**
 * mg_qfield_new_copy
 * @orig: a #MgQfield to copy
 *
 * This is a copy constructor
 *
 * Returns: the new object
 */
GObject *
mg_qfield_new_copy (MgQfield *orig)
{
	MgQfieldClass *class;
	GObject *obj;
	MgQuery *query;
	MgQfield *newfield;

	g_return_val_if_fail (orig && IS_MG_QFIELD (orig), NULL);
	g_return_val_if_fail (orig->priv, NULL);
	g_object_get (G_OBJECT (orig), "query", &query, NULL);
	g_return_val_if_fail (query, NULL);

	class = MG_QFIELD_CLASS (G_OBJECT_GET_CLASS (orig));
	g_return_val_if_fail (class->copy, NULL);

	obj = (class->copy) (orig);
	newfield = MG_QFIELD (obj);
	newfield->priv->visible = orig->priv->visible;
	newfield->priv->internal = orig->priv->internal;

	attach_to_query (MG_QFIELD (obj), query);

	return obj;
}

static void
attach_to_query (MgQfield *qfield, MgQuery *query)
{
	g_object_set (G_OBJECT (qfield), "query", query, NULL);
}


static void
mg_qfield_dispose (GObject *object)
{
	MgQfield *qfield;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_QFIELD (object));

	qfield = MG_QFIELD (object);
	if (qfield->priv) {
		if (qfield->priv->alias) {
			g_free (qfield->priv->alias);
			qfield->priv->alias = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_qfield_finalize (GObject   * object)
{
	MgQfield *qfield;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_QFIELD (object));

	qfield = MG_QFIELD (object);
	if (qfield->priv) {
		g_free (qfield->priv);
		qfield->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_qfield_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	MgQfield *qfield;

	qfield = MG_QFIELD (object);
	if (qfield->priv) {
		switch (param_id) {
		case PROP:
			break;
		}
	}
}

static void
mg_qfield_get_property (GObject              *object,
			guint                 param_id,
			GValue               *value,
			GParamSpec           *pspec)
{
	MgQfield *qfield;

	qfield = MG_QFIELD (object);
        if (qfield->priv) {
                switch (param_id) {
                case PROP:
                        break;
                }
        }
}


/**
 * mg_qfield_set_alias
 * @qfield: a #MgQfield object
 * @alias: the alias to set @qfield to
 *
 * Sets @qfield's alias
 */
void
mg_qfield_set_alias (MgQfield *qfield, const gchar *alias)
{
	g_return_if_fail (qfield && IS_MG_QFIELD (qfield));
	g_return_if_fail (qfield->priv);

	if (qfield->priv->alias) {
		g_free (qfield->priv->alias);
		qfield->priv->alias = NULL;
	}
	
	if (alias)
		qfield->priv->alias = g_strdup (alias);
}

/**
 * mg_qfield_get_alias
 * @qfield: a #MgQfield object
 *
 * Get @qfield's alias
 *
 * Returns: the alias
 */
const gchar *
mg_qfield_get_alias (MgQfield *qfield)
{
	g_return_val_if_fail (qfield && IS_MG_QFIELD (qfield), NULL);
	g_return_val_if_fail (qfield->priv, NULL);

	
	if (!qfield->priv->alias)
		qfield->priv->alias = g_strdup_printf ("t%d", mg_base_get_id (MG_BASE (qfield)));

	return qfield->priv->alias;
}


/**
 * mg_qfield_set_visible
 * @qfield: a #MgQfield object
 * @visible:
 *
 * Sets the visibility of @qfield. A visible field will appear in the query's 
 * corresponding (virtual) entity, whereas a non visible one will be hidden (and
 * possibly not taking part in the query).
 */
void
mg_qfield_set_visible (MgQfield *qfield, gboolean visible)
{
	MgQuery *query;

	g_return_if_fail (qfield && IS_MG_QFIELD (qfield));
	g_return_if_fail (qfield->priv);
	g_object_get (G_OBJECT (qfield), "query", &query, NULL);
	g_return_if_fail (query);

	if (qfield->priv->visible != visible) {
		qfield->priv->visible = visible;
		if (visible)
			g_signal_emit_by_name (G_OBJECT (query), "field_added", MG_FIELD (qfield));
		else
			g_signal_emit_by_name (G_OBJECT (query), "field_removed", MG_FIELD (qfield));
	}
}


/**
 * mg_qfield_is_visible
 * @qfield: a #MgQfield object
 *
 * Returns: TRUE if @field is visible
 */
gboolean
mg_qfield_is_visible (MgQfield *qfield)
{
	g_return_val_if_fail (qfield && IS_MG_QFIELD (qfield), FALSE);
	g_return_val_if_fail (qfield->priv, FALSE);

	return qfield->priv->visible;
}

/**
 * mg_qfield_set_internal
 * @qfield: a #MgQfield object
 * @internal:
 *
 * Sets weather @qfield is internal or not. Internal fields in a query are fields added
 * by libmergeant itself, such fields may or may not be visible.
 */
void
mg_qfield_set_internal (MgQfield *qfield, gboolean internal)
{
	g_return_if_fail (qfield && IS_MG_QFIELD (qfield));
	g_return_if_fail (qfield->priv);

	qfield->priv->internal = internal;
}


/**
 * mg_qfield_is_internal
 * @qfield: a #MgQfield object
 *
 * Returns: TRUE if @field is internal
 */
gboolean
mg_qfield_is_internal (MgQfield *qfield)
{
	g_return_val_if_fail (qfield && IS_MG_QFIELD (qfield), FALSE);
	g_return_val_if_fail (qfield->priv, FALSE);

	return qfield->priv->internal;
}

/**
 * mg_qfield_get_parameters
 * @qfield: a #MgQfield object
 *
 * Get a list of all the parameters needed to @qfield to be
 * rendered as a valid statement
 *
 * Returns: a new list of parameters for @qfield
 */
GSList *
mg_qfield_get_parameters (MgQfield *qfield)
{
	MgQfieldClass *class;

	g_return_val_if_fail (qfield && IS_MG_QFIELD (qfield), NULL);
	g_return_val_if_fail (qfield->priv, NULL);
	class = MG_QFIELD_CLASS (G_OBJECT_GET_CLASS (qfield));

	if (class->get_params)
		return (class->get_params) (qfield);
	else
		return NULL;
}

/**
 * mg_qfield_is_equal
 * @qfield1: a #MgQfield object
 * @qfield2: a #MgQfield object
 *
 * Compares the @qfield1 and @qfield2. The name and aliases of the two fields are
 * not compared, only the contents of the fields are.
 *
 * Returns: TRUE if they are equal and FALSE otherwise
 */
gboolean
mg_qfield_is_equal (MgQfield *qfield1, MgQfield *qfield2)
{
	MgQfieldClass *class1, *class2;
	MgQuery *q1, *q2;

	g_return_val_if_fail (qfield1 && IS_MG_QFIELD (qfield1), FALSE);
	g_return_val_if_fail (qfield2 && IS_MG_QFIELD (qfield2), FALSE);
	g_return_val_if_fail (qfield1->priv, FALSE);
	g_return_val_if_fail (qfield2->priv, FALSE);
	g_object_get (G_OBJECT (qfield1), "query", &q1, NULL);
	g_object_get (G_OBJECT (qfield2), "query", &q2, NULL);
	g_return_val_if_fail (q1, FALSE);
	g_return_val_if_fail (q2, FALSE);

	if (q1 != q2)
		return FALSE;

	class1 = MG_QFIELD_CLASS (G_OBJECT_GET_CLASS (qfield1));
	class2 = MG_QFIELD_CLASS (G_OBJECT_GET_CLASS (qfield2));
	if (class1 != class2)
		return FALSE;

	g_return_val_if_fail (class1->is_equal, FALSE);

	return (class1->is_equal) (qfield1, qfield2);
}


/**
 * mg_qfield_is_list
 * @qfield: a #MgQfield object
 *
 * Tells if @qfield can potentially represent a list of values.
 *
 * Returns: TRUE if @field can be a list of values
 */
gboolean
mg_qfield_is_list (MgQfield *qfield)
{
	MgQfieldClass *class;

	g_return_val_if_fail (qfield && IS_MG_QFIELD (qfield), FALSE);
	g_return_val_if_fail (qfield->priv, FALSE);

	class = MG_QFIELD_CLASS (G_OBJECT_GET_CLASS (qfield));
	if (class->is_list)
		return (class->is_list) (qfield);
	else
		return FALSE;
}

