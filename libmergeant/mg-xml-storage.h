/* mg-xml-storage.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_XML_STORAGE_H_
#define __MG_XML_STORAGE_H_

#include <glib-object.h>
#include <libxml/tree.h>
#include "mg-defs.h"

G_BEGIN_DECLS

#define MG_XML_STORAGE_TYPE          (mg_xml_storage_get_type())
#define MG_XML_STORAGE(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_xml_storage_get_type(), MgXmlStorage)
#define IS_MG_XML_STORAGE(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_xml_storage_get_type ())
#define MG_XML_STORAGE_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MG_XML_STORAGE_TYPE, MgXmlStorageIface))



/* struct for the interface */
struct _MgXmlStorageIface
{
	GTypeInterface           g_iface;

	/* virtual table */
	gchar      *(* get_xml_id)      (MgXmlStorage *iface);
	xmlNodePtr  (* save_to_xml)     (MgXmlStorage *iface, GError **error);
	gboolean    (* load_from_xml)   (MgXmlStorage *iface, xmlNodePtr node, GError **error);
};

GType           mg_xml_storage_get_type        (void) G_GNUC_CONST;

gchar          *mg_xml_storage_get_xml_id      (MgXmlStorage *iface);
xmlNodePtr      mg_xml_storage_save_to_xml     (MgXmlStorage *iface, GError **error);
gboolean        mg_xml_storage_load_from_xml   (MgXmlStorage *iface, xmlNodePtr node, GError **error);


G_END_DECLS

#endif
