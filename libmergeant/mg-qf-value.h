/* mg-qf-value.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_QF_VALUE_H_
#define __MG_QF_VALUE_H_

#include <libgda/libgda.h>
#include "mg-base.h"
#include "mg-defs.h"
#include "mg-qfield.h"
#include "mg-server-data-type.h"

G_BEGIN_DECLS

#define MG_QF_VALUE_TYPE          (mg_qf_value_get_type())
#define MG_QF_VALUE(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_qf_value_get_type(), MgQfValue)
#define MG_QF_VALUE_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_qf_value_get_type (), MgQfValueClass)
#define IS_MG_QF_VALUE(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_qf_value_get_type ())


/* error reporting */
extern GQuark mg_qf_value_error_quark (void);
#define MG_QF_VALUE_ERROR mg_qf_value_error_quark ()

enum
{
	MG_QF_VALUE_XML_LOAD_ERROR,
	MG_QF_VALUE_RENDER_ERROR,
	MG_QF_VALUE_PARAM_ERROR
};


/* struct for the object's data */
struct _MgQfValue
{
	MgQfield              object;
	MgQfValuePrivate     *priv;
};

/* struct for the object's class */
struct _MgQfValueClass
{
	MgQfieldClass                  class;

	/* signals */
	void   (*templ_signal)        (MgQfValue *obj);
};

guint             mg_qf_value_get_type            (void);
GObject          *mg_qf_value_new                 (MgQuery *query, MgServerDataType *type);

const GdaValue   *mg_qf_value_get_value           (MgQfValue *field);
void              mg_qf_value_set_default_value   (MgQfValue *field, const GdaValue *default_val);
const GdaValue   *mg_qf_value_get_default_value   (MgQfValue *field);
GdaValueType      mg_qf_value_get_value_type      (MgQfValue *field);
MgServerDataType *mg_qf_value_get_server_data_type(MgQfValue *field);

void              mg_qf_value_set_is_parameter    (MgQfValue *field, gboolean is_param);
gboolean          mg_qf_value_is_parameter        (MgQfValue *field);

void              mg_qf_value_set_not_null        (MgQfValue *field, gboolean not_null);
gboolean          mg_qf_value_get_not_null        (MgQfValue *field);
gboolean          mg_qf_value_is_value_null       (MgQfValue *field, MgContext *context);

G_END_DECLS

#endif
