/* mg-qfield.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


/*
 * This object is the qfield object for most of Mergeant's objects, it provides
 * basic facilities:
 * - a reference to the MgConf object
 * - a unique id which is used to XML storing procedures
 * - some attributes such as name, description and owner of the object (only used
 *   for DBMS object which are derived from this class.
 */


#ifndef __MG_QFIELD_H_
#define __MG_QFIELD_H_

#include "mg-defs.h"
#include <libxml/tree.h>
#include "mg-base.h"

G_BEGIN_DECLS

#define MG_QFIELD_TYPE          (mg_qfield_get_type())
#define MG_QFIELD(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_qfield_get_type(), MgQfield)
#define MG_QFIELD_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_qfield_get_type (), MgQfieldClass)
#define IS_MG_QFIELD(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_qfield_get_type ())


/* error reporting */
extern GQuark mg_qfield_error_quark (void);
#define MG_QFIELD_ERROR mg_qfield_error_quark ()

enum
{
	MG_QFIELD_XML_LOAD_ERROR
};


/* struct for the object's data */
struct _MgQfield
{
	MgBase                 object;
	MgQfieldPrivate       *priv;
};

/* struct for the object's class */
struct _MgQfieldClass
{
	MgBaseClass            class;

	/* pure virtual functions */
	GObject    *(*copy)           (MgQfield *orig);
	GSList     *(*get_params)     (MgQfield *qfield);
	gboolean    (*is_equal)       (MgQfield *qfield1, MgQfield *qfield2);
	gboolean    (*is_list)        (MgQfield *qfield);
};

guint        mg_qfield_get_type        (void);
GObject     *mg_qfield_new_from_xml    (MgQuery *query, xmlNodePtr node, GError **error);
GObject     *mg_qfield_new_copy        (MgQfield *orig);

GSList      *mg_qfield_get_parameters  (MgQfield *qfield);
void         mg_qfield_set_alias       (MgQfield *qfield, const gchar *alias);
const gchar *mg_qfield_get_alias       (MgQfield *qfield);

void         mg_qfield_set_visible     (MgQfield *qfield, gboolean visible);
gboolean     mg_qfield_is_visible      (MgQfield *qfield);

void         mg_qfield_set_internal    (MgQfield *qfield, gboolean internal);
gboolean     mg_qfield_is_internal     (MgQfield *qfield);

gboolean     mg_qfield_is_equal        (MgQfield *qfield1, MgQfield *qfield2);

gboolean     mg_qfield_is_list         (MgQfield *qfield);

G_END_DECLS

#endif
