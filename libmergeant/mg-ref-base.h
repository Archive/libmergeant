/* mg-ref-base.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MG_REF_BASE_H_
#define __MG_REF_BASE_H_

#include "mg-base.h"
#include "mg-defs.h"
#include <libgda/libgda.h>

G_BEGIN_DECLS

#define MG_REF_BASE_TYPE          (mg_ref_base_get_type())
#define MG_REF_BASE(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_ref_base_get_type(), MgRefBase)
#define MG_REF_BASE_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_ref_base_get_type (), MgRefBaseClass)
#define IS_MG_REF_BASE(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_ref_base_get_type ())

typedef struct _MgRefBase MgRefBase;
typedef struct _MgRefBaseClass MgRefBaseClass;
typedef struct _MgRefBasePrivate MgRefBasePrivate;

/* error reporting */
extern GQuark mg_ref_base_error_quark (void);
#define MG_REF_BASE_ERROR mg_ref_base_error_quark ()

typedef enum
{
	REFERENCE_BY_XML_ID,
	REFERENCE_BY_NAME
} MgRefBaseType;

enum
{
	MG_REF_BASE_XML_LOAD_ERROR
};


/* struct for the object's data */
struct _MgRefBase
{
	MgBase                  object;
	MgRefBasePrivate       *priv;
};

/* struct for the object's class */
struct _MgRefBaseClass
{
	MgBaseClass                    class;

	/* signals */
	void   (*ref_found)           (MgRefBase *obj);
	void   (*ref_lost)            (MgRefBase *obj);
};

guint           mg_ref_base_get_type           (void);
GObject        *mg_ref_base_new                (MgConf *conf);
GObject        *mg_ref_base_new_copy           (MgRefBase *orig);

void            mg_ref_base_set_ref_name       (MgRefBase *ref, GType ref_type, 
					        MgRefBaseType type, const gchar *name);
const gchar    *mg_ref_base_get_ref_name       (MgRefBase *ref, GType *ref_type, MgRefBaseType *type);
GType           mg_ref_base_get_ref_type       (MgRefBase *ref);

void            mg_ref_base_set_ref_object     (MgRefBase *ref, MgBase *object);
void            mg_ref_base_set_ref_object_type(MgRefBase *ref, MgBase *object, GType type);
void            mg_ref_base_replace_ref_object (MgRefBase *ref, GHashTable *replacements);
MgBase         *mg_ref_base_get_ref_object     (MgRefBase *ref);

gboolean        mg_ref_base_activate           (MgRefBase *ref);
void            mg_ref_base_deactivate         (MgRefBase *ref);
gboolean        mg_ref_base_is_active          (MgRefBase *ref);

G_END_DECLS

#endif
