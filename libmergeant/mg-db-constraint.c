/* mg-db-constraint.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-db-constraint.h"
#include "mg-referer.h"
#include "mg-database.h"
#include "mg-db-table.h"
#include "mg-xml-storage.h"
#include "mg-entity.h"
#include "mg-db-field.h"
#include "mg-field.h"
#include "marshal.h"
#include <string.h>

/* 
 * Main static functions 
 */
static void mg_db_constraint_class_init (MgDbConstraintClass * class);
static void mg_db_constraint_init (MgDbConstraint * srv);
static void mg_db_constraint_dispose (GObject   * object);
static void mg_db_constraint_finalize (GObject   * object);

static void mg_db_constraint_set_property (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec);
static void mg_db_constraint_get_property (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec);

/* XML storage interface */
static void        mg_db_constraint_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_db_constraint_get_xml_id (MgXmlStorage *iface);
static xmlNodePtr  mg_db_constraint_save_to_xml (MgXmlStorage *iface, GError **error);
static gboolean    mg_db_constraint_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error);


/* MgReferer interface */
static void        mg_db_constraint_referer_init (MgRefererIface *iface);
static gboolean    mg_db_constraint_activate            (MgReferer *iface);
static void        mg_db_constraint_deactivate          (MgReferer *iface);
static gboolean    mg_db_constraint_is_active           (MgReferer *iface);
static GSList     *mg_db_constraint_get_ref_objects     (MgReferer *iface);
static void        mg_db_constraint_replace_refs        (MgReferer *iface, GHashTable *replacaments);

#ifdef debug
static void        mg_db_constraint_dump                (MgDbConstraint *cstr, guint offset);
#endif

/* When a DbTable or MgDbField is nullified */
static void nullified_object_cb (GObject *obj, MgDbConstraint *cstr);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	TEMPL_SIGNAL,
	LAST_SIGNAL
};

static gint mg_db_constraint_signals[LAST_SIGNAL] = { 0 };

/* properties */
enum
{
	PROP_0,
	PROP_USER_CSTR
};


/* private structure */
struct _MgDbConstraintPrivate
{
	MgDbConstraintType      type;
	MgDbTable              *table;
	gboolean                user_defined; /* FALSE if the constraint is hard coded into the database */
	
	/* Only used if single field constraint */
	MgDbField              *single_field;

	/* Only used if Primary key */
	GSList                 *multiple_fields; 

	/* Only used for foreign keys */
	MgDbTable              *ref_table; 
	GSList                 *fk_pairs;  
	MgDbConstraintFkAction  on_delete;
	MgDbConstraintFkAction  on_update;
	
};


/* module error */
GQuark mg_db_constraint_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_db_constraint_error");
	return quark;
}


guint
mg_db_constraint_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgDbConstraintClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_db_constraint_class_init,
			NULL,
			NULL,
			sizeof (MgDbConstraint),
			0,
			(GInstanceInitFunc) mg_db_constraint_init
		};

		static const GInterfaceInfo xml_storage_info = {
			(GInterfaceInitFunc) mg_db_constraint_xml_storage_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo referer_info = {
			(GInterfaceInitFunc) mg_db_constraint_referer_init,
			NULL,
			NULL
		};
		
		type = g_type_register_static (MG_BASE_TYPE, "MgDbConstraint", &info, 0);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
		g_type_add_interface_static (type, MG_REFERER_TYPE, &referer_info);
	}
	return type;
}

static void 
mg_db_constraint_xml_storage_init (MgXmlStorageIface *iface)
{
	iface->get_xml_id = mg_db_constraint_get_xml_id;
	iface->save_to_xml = mg_db_constraint_save_to_xml;
	iface->load_from_xml = mg_db_constraint_load_from_xml;
}

static void
mg_db_constraint_referer_init (MgRefererIface *iface)
{
	iface->activate = mg_db_constraint_activate;
	iface->deactivate = mg_db_constraint_deactivate;
	iface->is_active = mg_db_constraint_is_active;
	iface->get_ref_objects = mg_db_constraint_get_ref_objects;
	iface->replace_refs = mg_db_constraint_replace_refs;
}

static void
mg_db_constraint_class_init (MgDbConstraintClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_db_constraint_signals[TEMPL_SIGNAL] =
		g_signal_new ("templ_signal",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgDbConstraintClass, templ_signal),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	class->templ_signal = NULL;

	object_class->dispose = mg_db_constraint_dispose;
	object_class->finalize = mg_db_constraint_finalize;

	/* Properties */
	object_class->set_property = mg_db_constraint_set_property;
	object_class->get_property = mg_db_constraint_get_property;
	g_object_class_install_property (object_class, PROP_USER_CSTR,
					 g_param_spec_boolean ("user_constraint", NULL, NULL, FALSE,
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_db_constraint_dump;
#endif
}

static void
mg_db_constraint_init (MgDbConstraint * mg_db_constraint)
{
	mg_db_constraint->priv = g_new0 (MgDbConstraintPrivate, 1);
	mg_db_constraint->priv->type = CONSTRAINT_PRIMARY_KEY;
	mg_db_constraint->priv->table = NULL;
	mg_db_constraint->priv->user_defined = FALSE;
	mg_db_constraint->priv->single_field = NULL;
	mg_db_constraint->priv->multiple_fields = NULL;
	mg_db_constraint->priv->ref_table = NULL;
	mg_db_constraint->priv->fk_pairs = NULL;
	mg_db_constraint->priv->on_delete = CONSTRAINT_FK_ACTION_NO_ACTION;
	mg_db_constraint->priv->on_update = CONSTRAINT_FK_ACTION_NO_ACTION;
}


/**
 * mg_db_constraint_new
 * @table: the #MgDbTable to which the constraint is attached
 * @type: the type of constraint
 *
 * Creates a new MgDbConstraint object
 *
 * Returns: the new object
 */
GObject*
mg_db_constraint_new (MgDbTable *table, MgDbConstraintType type)
{
	GObject   *obj;
	MgDbConstraint *mg_db_constraint;
	MgConf *conf = NULL;

	g_return_val_if_fail (table && IS_MG_DB_TABLE (table), NULL);
	conf = mg_base_get_conf (MG_BASE (table));
	
	obj = g_object_new (MG_DB_CONSTRAINT_TYPE, "conf", conf, NULL);

	mg_db_constraint = MG_DB_CONSTRAINT (obj);
	mg_base_set_id (MG_BASE (mg_db_constraint), 0);

	mg_db_constraint->priv->type = type;
	mg_db_constraint->priv->table = table;

	g_signal_connect (G_OBJECT (table), "nullified",
			  G_CALLBACK (nullified_object_cb), mg_db_constraint);

	return obj;
}

/**
 * mg_db_constraint_new_with_db
 * @db: a #MgDatabase object
 * 
 * Creates a new #MgDbConstraint object without specifying anything about the
 * constraint except the database it is attached to. This is usefull only
 * when the object is going to be loaded from an XML node.
 *
 * Returns: the new uninitialized object
 */
GObject *
mg_db_constraint_new_with_db (MgDatabase *db)
{
	GObject   *obj;
	MgDbConstraint *mg_db_constraint;
	MgConf *conf = NULL;

	/* here priv->table will be NULL and a "db" data is attached to the object */

	g_return_val_if_fail (db && IS_MG_DATABASE (db), NULL);
	conf = mg_base_get_conf (MG_BASE (db));
	
	obj = g_object_new (MG_DB_CONSTRAINT_TYPE, "conf", conf, NULL);

	mg_db_constraint = MG_DB_CONSTRAINT (obj);
	mg_base_set_id (MG_BASE (mg_db_constraint), 0);
	
	g_object_set_data (obj, "db", db);

	g_signal_connect (G_OBJECT (db), "nullified",
			  G_CALLBACK (nullified_object_cb), mg_db_constraint);
	return obj;
}

static void 
nullified_object_cb (GObject *obj, MgDbConstraint *cstr)
{
	mg_base_nullify (MG_BASE (cstr));
}

static void
mg_db_constraint_dispose (GObject *object)
{
	MgDbConstraint *cstr;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_DB_CONSTRAINT (object));

	cstr = MG_DB_CONSTRAINT (object);
	if (cstr->priv) {
		GSList *list;

		mg_base_nullify_check (MG_BASE (object));

		switch (cstr->priv->type) {
		case CONSTRAINT_PRIMARY_KEY:
		case CONSTRAINT_UNIQUE:
			list = cstr->priv->multiple_fields;
			while (list) {
				g_signal_handlers_disconnect_by_func (G_OBJECT (list->data),
								      G_CALLBACK (nullified_object_cb), cstr);
				list = g_slist_next (list);
			}
			g_slist_free (cstr->priv->multiple_fields);
			cstr->priv->multiple_fields = NULL;
			break;
		case CONSTRAINT_FOREIGN_KEY:
			if (cstr->priv->ref_table)
				g_signal_handlers_disconnect_by_func (G_OBJECT (cstr->priv->ref_table),
								      G_CALLBACK (nullified_object_cb), cstr);
			cstr->priv->ref_table = NULL;

			list = cstr->priv->fk_pairs;
			while (list) {
				MgDbConstraintFkeyPair *pair = MG_DB_CONSTRAINT_FK_PAIR (list->data);

				g_signal_handlers_disconnect_by_func (G_OBJECT (pair->fkey),
								      G_CALLBACK (nullified_object_cb), cstr);
				if (pair->ref_pkey)
					g_signal_handlers_disconnect_by_func (G_OBJECT (pair->ref_pkey),
									      G_CALLBACK (nullified_object_cb), cstr);
				if (pair->ref_pkey_repl)
					g_object_unref (G_OBJECT (pair->ref_pkey_repl));
				g_free (list->data);
				list = g_slist_next (list);
			}
			g_slist_free (cstr->priv->fk_pairs);
			cstr->priv->fk_pairs = NULL;
			break;
		case CONSTRAINT_NOT_NULL:
			if (cstr->priv->single_field)
				g_signal_handlers_disconnect_by_func (G_OBJECT (cstr->priv->single_field),
								      G_CALLBACK (nullified_object_cb), cstr);
			cstr->priv->single_field = NULL;
			break;
		case CONSTRAINT_CHECK_EXPR:
		default:
			TO_IMPLEMENT;
		}

		if (g_object_get_data (G_OBJECT (cstr), "db")) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (g_object_get_data (G_OBJECT (cstr), "db")),
							      G_CALLBACK (nullified_object_cb), cstr);
			g_object_set_data (G_OBJECT (cstr), "db", NULL);
		}
		
		if (cstr->priv->table) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (cstr->priv->table),
							      G_CALLBACK (nullified_object_cb), cstr);
			cstr->priv->table = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_db_constraint_finalize (GObject   * object)
{
	MgDbConstraint *mg_db_constraint;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_DB_CONSTRAINT (object));

	mg_db_constraint = MG_DB_CONSTRAINT (object);
	if (mg_db_constraint->priv) {

		g_free (mg_db_constraint->priv);
		mg_db_constraint->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_db_constraint_set_property (GObject              *object,
			       guint                 param_id,
			       const GValue         *value,
			       GParamSpec           *pspec)
{
	MgDbConstraint *mg_db_constraint;

	mg_db_constraint = MG_DB_CONSTRAINT (object);
	if (mg_db_constraint->priv) {
		switch (param_id) {
		case PROP_USER_CSTR:
			mg_db_constraint->priv->user_defined = g_value_get_boolean (value);
			break;
		}
	}
}

static void
mg_db_constraint_get_property (GObject              *object,
			       guint                 param_id,
			       GValue               *value,
			       GParamSpec           *pspec)
{
	MgDbConstraint *mg_db_constraint;
	mg_db_constraint = MG_DB_CONSTRAINT (object);
	
	if (mg_db_constraint->priv) {
		switch (param_id) {
		case PROP_USER_CSTR:
			g_value_set_boolean (value, mg_db_constraint->priv->user_defined);
			break;
		}	
	}
}


/**
 * mg_db_constraint_get_constraint_type
 * @cstr: a #MgDbConstraint object
 *
 * Get the type of constraint the @cstr object represents
 *
 * Returns: the constraint type
 */
MgDbConstraintType
mg_db_constraint_get_constraint_type (MgDbConstraint *cstr)
{
	g_return_val_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr), CONSTRAINT_UNKNOWN);
	g_return_val_if_fail (cstr->priv, CONSTRAINT_UNKNOWN);
	g_return_val_if_fail (cstr->priv->table, CONSTRAINT_UNKNOWN);

	return cstr->priv->type;
}

/**
 * mg_db_constraint_equal
 * @cstr1: the first #MgDbConstraint to compare
 * @cstr2: the second #MgDbConstraint to compare
 *
 * Compares two #MgDbConstraint objects to see if they are equal, without taking into account the
 * name of the constraints or weather they are user or system defined
 *
 * Returns: TRUE if the two constraints are equal and FALSE otherwise
 */
gboolean
mg_db_constraint_equal (MgDbConstraint *cstr1, MgDbConstraint *cstr2)
{
	gboolean equal = TRUE;
	GSList *list1, *list2;

	g_return_val_if_fail (cstr1 && IS_MG_DB_CONSTRAINT (cstr1), FALSE);
	g_return_val_if_fail (cstr1->priv, FALSE);
	g_return_val_if_fail (cstr2 && IS_MG_DB_CONSTRAINT (cstr2), FALSE);
	g_return_val_if_fail (cstr2->priv, FALSE);
	g_return_val_if_fail (cstr1->priv->table, FALSE);
	g_return_val_if_fail (cstr2->priv->table, FALSE);

	if (cstr1->priv->type != cstr2->priv->type)
		return FALSE;

	if (cstr1->priv->table != cstr2->priv->table)
		return FALSE;
	
	mg_db_constraint_activate (MG_REFERER (cstr1));
	mg_db_constraint_activate (MG_REFERER (cstr2));

	switch (cstr1->priv->type) {
	case CONSTRAINT_PRIMARY_KEY:
	case CONSTRAINT_UNIQUE:
		list1 = cstr1->priv->multiple_fields;
		list2 = cstr2->priv->multiple_fields;
		while (list1 && list2 && equal) {
			if (list1->data != list2->data)
				equal = FALSE;
			list1 = g_slist_next (list1);
			list2 = g_slist_next (list2);
		}
		if (list1 || list2)
			equal = FALSE;
		break;
	case CONSTRAINT_FOREIGN_KEY:
		list1 = cstr1->priv->fk_pairs;
		list2 = cstr2->priv->fk_pairs;
		while (list1 && list2 && equal) {
			if (MG_DB_CONSTRAINT_FK_PAIR (list1->data)->fkey != 
			    MG_DB_CONSTRAINT_FK_PAIR (list2->data)->fkey)
				equal = FALSE;
			if (MG_DB_CONSTRAINT_FK_PAIR (list1->data)->ref_pkey != 
			    MG_DB_CONSTRAINT_FK_PAIR (list2->data)->ref_pkey)
				equal = FALSE;

			if (MG_DB_CONSTRAINT_FK_PAIR (list1->data)->ref_pkey_repl &&
			    MG_DB_CONSTRAINT_FK_PAIR (list2->data)->ref_pkey_repl) {
				GType type1, type2;
				MgRefBaseType itype1, itype2;
				const gchar *name1, *name2;
				
				name1 = mg_ref_base_get_ref_name (MG_DB_CONSTRAINT_FK_PAIR (list1->data)->ref_pkey_repl,
								  &type1, &itype1);
				name2 = mg_ref_base_get_ref_name (MG_DB_CONSTRAINT_FK_PAIR (list2->data)->ref_pkey_repl,
								  &type2, &itype2);
				if ((type1 != type2) || (itype1 != itype2) ||
				    strcmp (name1, name2))
					equal = FALSE;
			}
			else {
				if (MG_DB_CONSTRAINT_FK_PAIR (list1->data)->ref_pkey_repl ||
				    MG_DB_CONSTRAINT_FK_PAIR (list2->data)->ref_pkey_repl)
					equal = FALSE;
			}

			list1 = g_slist_next (list1);
			list2 = g_slist_next (list2);
		}
		if (list1 || list2)
			equal = FALSE;
		break;
	case CONSTRAINT_NOT_NULL:
		if (cstr1->priv->single_field != cstr2->priv->single_field)
			equal = FALSE;
		break;
	case CONSTRAINT_CHECK_EXPR:
		TO_IMPLEMENT;
		break;
	default:
		equal = FALSE;
		break;
	}

	return equal;
}


/**
 * mg_db_constraint_get_table
 * @cstr: a #MgDbConstraint object
 *
 * Get the table to which the constraint is attached
 *
 * Returns: the #MgDbTable
 */
MgDbTable *
mg_db_constraint_get_table (MgDbConstraint *cstr)
{
	g_return_val_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr), NULL);
	g_return_val_if_fail (cstr->priv, NULL);
	g_return_val_if_fail (cstr->priv->table, NULL);

	return cstr->priv->table;
}

/**
 * mg_db_constraint_uses_field
 * @cstr: a #MgDbConstraint object
 * @field: a #MgDbField object
 *
 * Tests if @field is part of the @cstr constraint
 *
 * Returns: TRUE if @cstr uses @field
 */
gboolean
mg_db_constraint_uses_field (MgDbConstraint *cstr, MgDbField *field)
{
	gboolean retval = FALSE;
	GSList *list;

	g_return_val_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr), FALSE);
	g_return_val_if_fail (cstr->priv, FALSE);
	g_return_val_if_fail (field && IS_MG_DB_FIELD (field), FALSE);

	switch (mg_db_constraint_get_constraint_type (cstr)) {
	case CONSTRAINT_PRIMARY_KEY:
	case CONSTRAINT_UNIQUE:
		if (g_slist_find (cstr->priv->multiple_fields, field))
			retval = TRUE;
		break;
	case CONSTRAINT_FOREIGN_KEY:
		list = cstr->priv->fk_pairs;
		while (list && !retval) {
			if (MG_DB_CONSTRAINT_FK_PAIR (list->data)->fkey == field)
				retval = TRUE;
			list = g_slist_next (list);
		}
		break;
	case CONSTRAINT_NOT_NULL:
		if (cstr->priv->single_field == field)
			retval = TRUE;
		break;
	case CONSTRAINT_CHECK_EXPR:
	default:
		TO_IMPLEMENT;
	}
	
	return retval;
}


static void mg_db_constraint_multiple_set_fields (MgDbConstraint *cstr, const GSList *fields);

/**
* mg_db_constraint_pkey_set_fields
* @cstr: a #MgDbConstraint object
* @fields: a list of #MgDbField objects
*
* Sets the fields which make the primary key represented by @cstr. All the fields
* must belong to the same #MgDbTable to which the constraint is attached
*/
void
mg_db_constraint_pkey_set_fields (MgDbConstraint *cstr, const GSList *fields)
{
	g_return_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr));
	g_return_if_fail (cstr->priv);
	g_return_if_fail (cstr->priv->type == CONSTRAINT_PRIMARY_KEY);
	g_return_if_fail (cstr->priv->table);
	g_return_if_fail (fields);

	mg_db_constraint_multiple_set_fields (cstr, fields);
}


static void
mg_db_constraint_multiple_set_fields (MgDbConstraint *cstr, const GSList *fields)
{
	const GSList *list;
	g_return_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr));

	/* testing for errors */
	list = fields;
	while (list) {
		if (!list->data) {
			g_warning ("List contains a NULL value, not a field");
			return;
		}
		if (!IS_MG_DB_FIELD (list->data)) {
			g_warning ("List item %p is not a field\n", list->data);
			return;
		}
		if (mg_field_get_entity (MG_FIELD (list->data)) != MG_ENTITY (cstr->priv->table)) {
			g_warning ("Field %p belongs to a table different from the constraint\n", list->data);
			return;
		}

		list = g_slist_next (list);
	}

	/* if a fields list is already here, get rid of it */
	if (cstr->priv->multiple_fields) {
		list = cstr->priv->multiple_fields;
		while (list) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (list->data),
							      G_CALLBACK (nullified_object_cb), cstr);
			list = g_slist_next (list);
		}
		g_slist_free (cstr->priv->multiple_fields);
		cstr->priv->multiple_fields = NULL;
	}

	/* make a copy of the new fields list */
	list = fields;
	while (list) {
		g_signal_connect (G_OBJECT (list->data), "nullified",
				  G_CALLBACK (nullified_object_cb), cstr);
		cstr->priv->multiple_fields = g_slist_append (cstr->priv->multiple_fields, list->data);
		list = g_slist_next (list);
	}
}

/**
 * mg_db_constraint_pkey_get_fields
 * @cstr: a #MgDbConstraint object
 *
 * Get the list of fields composing the primary key constraint which @cstr represents. The
 * returned list is allocated and must be de-allocated by the caller.
 *
 * Returns: a new list of fields
 */
GSList *
mg_db_constraint_pkey_get_fields (MgDbConstraint *cstr)
{
	g_return_val_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr), NULL);
	g_return_val_if_fail (cstr->priv, NULL);
	g_return_val_if_fail (cstr->priv->type == CONSTRAINT_PRIMARY_KEY, NULL);
	g_return_val_if_fail (cstr->priv->table, NULL);

	return g_slist_copy (cstr->priv->multiple_fields);
}


/** 
 * mg_db_constraint_fkey_set_fields
 * @cstr: a #MgDbConstraint object
 * @pairs: a list of #MgDbField objects
 *
 * Sets the field pairs which make the foreign key represented by @cstr. All the field pairs
 * must list a field which belong to the same #MgDbTable to which the constraint is attached
 * and a field which belongs to a #MgDbTable which is different from the one just mentionned and which
 * is within the same database.
 * The pairs are of type #MgDbConstraintFkeyPair.
 */
void
mg_db_constraint_fkey_set_fields (MgDbConstraint *cstr, const GSList *pairs)
{
	const GSList *list;
	MgDbTable *ref_table = NULL;
	GSList *oldlist;

	g_return_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr));
	g_return_if_fail (cstr->priv);
	g_return_if_fail (cstr->priv->type == CONSTRAINT_FOREIGN_KEY);
	g_return_if_fail (cstr->priv->table);

	/* testing for errors */
	list = pairs;
	while (list) {
		MgDbConstraintFkeyPair *pair;

		if (!list->data) {
			g_warning ("List contains a NULL value, not a pair of fields");
			return;
		}

		pair = MG_DB_CONSTRAINT_FK_PAIR (list->data);
		if (!IS_MG_DB_FIELD (pair->fkey)) {
			g_warning ("Pair item %p has fkey which is not a is not a field", list->data);
			return;
		}
		if (pair->ref_pkey_repl) { /* we have a MgRefBase object */
			if (!IS_MG_REF_BASE (pair->ref_pkey_repl)) {
				g_warning ("Pair item %p has ref_pkey_repl which is not a is not a MgRefBase", list->data);
				return;	
			}
			if (mg_ref_base_get_ref_type (pair->ref_pkey_repl) !=
			    MG_DB_FIELD_TYPE) {
				g_warning ("Pair item %p has ref_pkey_repl which does not reference a field", list->data);
				return;	
			}
		}
		else { /* we have a MgDbField object */
			if (!pair->ref_pkey || !IS_MG_DB_FIELD (pair->ref_pkey)) {
				g_warning ("Pair item %p has ref_pkey which is not a is not a field", list->data);
				return;
			}
			if (!ref_table)
				ref_table = MG_DB_TABLE (mg_field_get_entity (MG_FIELD (pair->ref_pkey)));
			else
				if (mg_field_get_entity (MG_FIELD (pair->ref_pkey)) != 
				    MG_ENTITY (ref_table)) {
					g_warning ("Referenced table is not the same for all pairs");
					return;
				}
		}

		if (mg_field_get_entity (MG_FIELD (pair->fkey)) != 
					 MG_ENTITY (cstr->priv->table)) {
			g_warning ("Field %p belongs to a table different from the constraint", 
				   pair->fkey);
			return;
		}

		list = g_slist_next (list);
	}

	/* if a fields list is already here, we treat in two parts: first (before the new list is analysed)
	 * we diocsonnect signals from objects of the old list, and then (after the new list has been analysed)
	 * we remove any reference to unused objects and clear the list */
	oldlist = cstr->priv->fk_pairs;
	list = cstr->priv->fk_pairs;
	while (list) {
		MgDbConstraintFkeyPair *pair = MG_DB_CONSTRAINT_FK_PAIR (list->data);
		
		g_signal_handlers_disconnect_by_func (G_OBJECT (pair->fkey),
						      G_CALLBACK (nullified_object_cb), cstr);
		if (pair->ref_pkey)
			g_signal_handlers_disconnect_by_func (G_OBJECT (pair->ref_pkey),
							      G_CALLBACK (nullified_object_cb), cstr);
		
		list = g_slist_next (list);
	}
	if (cstr->priv->ref_table) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (cstr->priv->ref_table),
						      G_CALLBACK (nullified_object_cb), cstr);
		cstr->priv->ref_table = NULL;
	}

	/* make a copy of the new fields list */
	cstr->priv->fk_pairs = NULL;
	list = pairs;
	while (list) {
		MgDbConstraintFkeyPair *pair;
		pair = g_new0 (MgDbConstraintFkeyPair, 1);
		*pair = *(MG_DB_CONSTRAINT_FK_PAIR (list->data));

		g_signal_connect (G_OBJECT (pair->fkey), "nullified",
				  G_CALLBACK (nullified_object_cb), cstr);
		
		if (!pair->ref_pkey_repl) 
			/* here we have tested that pair->ref_pkey is OK */
			g_signal_connect (G_OBJECT (pair->ref_pkey), "nullified",
					  G_CALLBACK (nullified_object_cb), cstr);
		else 
			g_object_ref (G_OBJECT (pair->ref_pkey_repl));

		cstr->priv->fk_pairs = g_slist_append (cstr->priv->fk_pairs, pair);
		list = g_slist_next (list);
	}
	cstr->priv->ref_table = ref_table;
	if (ref_table)
		g_signal_connect (G_OBJECT (ref_table), "nullified",
				  G_CALLBACK (nullified_object_cb), cstr);

	/* second part of getting rid of the ols list of fields */
	if (oldlist) {
		list = oldlist;
		while (list) {
			MgDbConstraintFkeyPair *pair = MG_DB_CONSTRAINT_FK_PAIR (list->data);

			if (pair->ref_pkey_repl)
				g_object_unref (G_OBJECT (pair->ref_pkey_repl));
				
			g_free (list->data);
			list = g_slist_next (list);
		}
		g_slist_free (oldlist);
	}


	mg_db_constraint_activate (MG_REFERER (cstr));
}


/**
 * mg_db_constraint_fkey_get_ref_table
 * @cstr: a #MgDbConstraint object
 *
 * Get the #MgDbTable at the other end of the foreign key relation represented by this
 * constraint
 *
 * Returns: the #MgDbTable
 */
MgDbTable *
mg_db_constraint_fkey_get_ref_table (MgDbConstraint *cstr)
{
	g_return_val_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr), NULL);
	g_return_val_if_fail (cstr->priv, NULL);
	g_return_val_if_fail (cstr->priv->type == CONSTRAINT_FOREIGN_KEY, NULL);
	g_return_val_if_fail (cstr->priv->table, NULL);
	
	mg_db_constraint_activate (MG_REFERER (cstr));

	return cstr->priv->ref_table;
}

/**
 * mg_db_constraint_fkey_get_fields
 * @cstr: a #MgDbConstraint object
 *
 * Get the list of field pairs composing the foreign key constraint which @cstr represents. In the returned
 * list, each pair item is allocated and it's up to the caller to free the list and each pair, and the
 * reference count for each pointer to GObjects in each pair is NOT INCREASED, which means the caller of this
 * function DOES NOT hold any reference on the mentionned GObjects (if he needs to, it has to call g_object_ref())
 *
 * Returns: a new list of #MgDbConstraintFkeyPair pairs
 */
GSList *
mg_db_constraint_fkey_get_fields (MgDbConstraint *cstr)
{
	GSList *list, *retval = NULL;

	g_return_val_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr), NULL);
	g_return_val_if_fail (cstr->priv, NULL);
	g_return_val_if_fail (cstr->priv->type == CONSTRAINT_FOREIGN_KEY, NULL);
	g_return_val_if_fail (cstr->priv->table, NULL);

	mg_db_constraint_activate (MG_REFERER (cstr));
	
	/* copy the list of pairs */
	list = cstr->priv->fk_pairs;
	while (list) {
		MgDbConstraintFkeyPair *pair;
		pair = g_new0 (MgDbConstraintFkeyPair, 1);
		*pair = * MG_DB_CONSTRAINT_FK_PAIR (list->data);
		retval = g_slist_append (retval, pair);
		list = g_slist_next (list);
	}

	return retval;
}

/**
 * mg_db_constraint_fkey_set_actions
 * @cstr: a #MgDbConstraint object
 * @on_update: the action undertaken when an UPDATE occurs
 * @on_delete: the action undertaken when a DELETE occurs
 *
 * Sets the actions undertaken by the DBMS when some actions occur on the referenced data
 */
void
mg_db_constraint_fkey_set_actions (MgDbConstraint *cstr, 
				   MgDbConstraintFkAction on_update, MgDbConstraintFkAction on_delete)
{
	g_return_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr));
	g_return_if_fail (cstr->priv);
	g_return_if_fail (cstr->priv->type == CONSTRAINT_FOREIGN_KEY);
	g_return_if_fail (cstr->priv->table);

	cstr->priv->on_update = on_update;
	cstr->priv->on_delete = on_delete;
}

/**
 * mg_db_constraint_fkey_get_actions
 * @cstr: a #MgDbConstraint object
 * @on_update: an address to store the action undertaken when an UPDATE occurs
 * @on_delete: an address to store the action undertaken when a DELETE occurs
 *
 * Get the actions undertaken by the DBMS when some actions occur on the referenced data
 */
void
mg_db_constraint_fkey_get_actions (MgDbConstraint *cstr, 
				   MgDbConstraintFkAction *on_update, MgDbConstraintFkAction *on_delete)
{
	g_return_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr));
	g_return_if_fail (cstr->priv);
	g_return_if_fail (cstr->priv->type == CONSTRAINT_FOREIGN_KEY);
	g_return_if_fail (cstr->priv->table);

	if (on_update)
		*on_update = cstr->priv->on_update;
	if (on_delete)
		*on_delete = cstr->priv->on_delete;
}


/**
 * mg_db_constraint_unique_set_fields
 * @cstr: a #MgDbConstraint object
 * @fields:
 *
 */
void
mg_db_constraint_unique_set_fields (MgDbConstraint *cstr, const GSList *fields)
{
	g_return_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr));
	g_return_if_fail (cstr->priv);
	g_return_if_fail (cstr->priv->type == CONSTRAINT_UNIQUE);
	g_return_if_fail (cstr->priv->table);
	g_return_if_fail (fields);

	mg_db_constraint_multiple_set_fields (cstr, fields);
}

/**
 * mg_db_constraint_unique_get_fields
 * @cstr: a #MgDbConstraint object
 *
 * Get the list of fields represented by this UNIQUE constraint. It's up to the caller to free the list.
 *
 * Returns: a new list of fields
 */
GSList *
mg_db_constraint_unique_get_fields (MgDbConstraint *cstr)
{
	g_return_val_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr), NULL);
	g_return_val_if_fail (cstr->priv, NULL);
	g_return_val_if_fail (cstr->priv->type == CONSTRAINT_UNIQUE, NULL);
	g_return_val_if_fail (cstr->priv->table, NULL);

	return g_slist_copy (cstr->priv->multiple_fields);
}

/**
 * mg_db_constraint_non_null_set_field
 * @cstr: a #MgDbConstraint object
 * @field:
 *
 */
void
mg_db_constraint_not_null_set_field (MgDbConstraint *cstr, MgDbField *field)
{
	g_return_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr));
	g_return_if_fail (cstr->priv);
	g_return_if_fail (cstr->priv->type == CONSTRAINT_NOT_NULL);
	g_return_if_fail (cstr->priv->table);
	g_return_if_fail (field && IS_MG_DB_FIELD (field));
	g_return_if_fail (mg_field_get_entity (MG_FIELD (field)) == MG_ENTITY (cstr->priv->table));


	/* if a field  is already here, get rid of it */
	if (cstr->priv->single_field) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (cstr->priv->single_field),
						      G_CALLBACK (nullified_object_cb), cstr);
		cstr->priv->single_field = NULL;
	}

	g_signal_connect (G_OBJECT (field), "nullified",
			  G_CALLBACK (nullified_object_cb), cstr);
	cstr->priv->single_field = field;
}

/**
 * mg_db_constraint_non_null_get_field
 * @cstr: a #MgDbConstraint object
 *
 */
MgDbField *
mg_db_constraint_not_null_get_field (MgDbConstraint *cstr)
{
	g_return_val_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr), NULL);
	g_return_val_if_fail (cstr->priv, NULL);
	g_return_val_if_fail (cstr->priv->type == CONSTRAINT_NOT_NULL, NULL);
	g_return_val_if_fail (cstr->priv->table, NULL);

	return cstr->priv->single_field;
}



#ifdef debug
static void
mg_db_constraint_dump (MgDbConstraint *cstr, guint offset)
{
	gchar *str;
	gint i;

	g_return_if_fail (cstr && IS_MG_DB_CONSTRAINT (cstr));
	
        /* string for the offset */
        str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;

        /* dump */
        if (cstr->priv && cstr->priv->table)
		g_print ("%s" D_COL_H1 "MgDbConstraint" D_COL_NOR " %p type=%d name=%s\n",
			 str, cstr, cstr->priv->type, mg_base_get_description (MG_BASE (cstr)));
        else
                g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, cstr);
}
#endif








/* 
 * MgXmlStorage interface implementation
 */
static gchar *
mg_db_constraint_get_xml_id (MgXmlStorage *iface)
{
	gchar *t_xml_id, *xml_id;

	g_return_val_if_fail (iface && IS_MG_DB_CONSTRAINT (iface), NULL);
	g_return_val_if_fail (MG_DB_CONSTRAINT (iface)->priv, NULL);
	g_return_val_if_fail (MG_DB_CONSTRAINT (iface)->priv->table, NULL);

	t_xml_id = mg_xml_storage_get_xml_id (MG_XML_STORAGE (MG_DB_CONSTRAINT (iface)->priv->table));
	xml_id = g_strdup_printf ("%s:FI%s", t_xml_id, mg_base_get_name (MG_BASE (iface)));
	g_free (t_xml_id);
	
	return xml_id;
}

static const gchar *
constraint_type_to_str (MgDbConstraintType type)
{
	switch (type) {
	case CONSTRAINT_PRIMARY_KEY:
		return "PKEY";
		break;
	case CONSTRAINT_FOREIGN_KEY:
		return "FKEY";
		break;
	case CONSTRAINT_UNIQUE:
		return "UNIQ";
		break;
	case CONSTRAINT_NOT_NULL:
		return "NNUL";
		break;
	case CONSTRAINT_CHECK_EXPR:
		return "CHECK";
		break;
	default:
		return "???";
		break;
	}
}

static MgDbConstraintType
constraint_str_to_type (const gchar *str)
{
	g_return_val_if_fail (str, CONSTRAINT_UNKNOWN);

	switch (*str) {
	case 'P':
		return CONSTRAINT_PRIMARY_KEY;
		break;
	case 'F':
		return CONSTRAINT_FOREIGN_KEY;
		break;
	case 'U':
		return CONSTRAINT_UNIQUE;
		break;
	case 'N':
		return CONSTRAINT_NOT_NULL;
		break;
	case 'C':
		return CONSTRAINT_CHECK_EXPR;
		break;
	default:
		return CONSTRAINT_UNKNOWN;
		break;
	}
}


static const gchar *
constraint_action_to_str (MgDbConstraintFkAction action)
{
	switch (action) {
	case CONSTRAINT_FK_ACTION_CASCADE:
		return "CAS";
		break;
	case CONSTRAINT_FK_ACTION_SET_NULL:
		return "NULL";
		break;
	case CONSTRAINT_FK_ACTION_SET_DEFAULT:
		return "DEF";
		break;
	case CONSTRAINT_FK_ACTION_SET_VALUE:
		return "VAL";
		break;
	case CONSTRAINT_FK_ACTION_NO_ACTION:
		return "RESTRICT";
		break;
	default:
		return "???";
		break;	
	}
}

static MgDbConstraintFkAction
constraint_str_to_action (const gchar *str)
{
	g_return_val_if_fail (str, CONSTRAINT_FK_ACTION_NO_ACTION);
	switch (*str) {
	case 'C':
		return CONSTRAINT_FK_ACTION_CASCADE;
		break;
	case 'N':
		return CONSTRAINT_FK_ACTION_SET_NULL;
		break;
	case 'D':
		return CONSTRAINT_FK_ACTION_SET_DEFAULT;
		break;
	case 'V':
		return CONSTRAINT_FK_ACTION_SET_VALUE;
		break;
	case 'R':
		return CONSTRAINT_FK_ACTION_NO_ACTION;
		break;
	default:
		return CONSTRAINT_FK_ACTION_NO_ACTION;
		break;	
	}
}

static xmlNodePtr
mg_db_constraint_save_to_xml (MgXmlStorage *iface, GError **error)
{
	xmlNodePtr node = NULL;
	xmlNodePtr child;
	MgDbConstraint *cstr;
	gchar *str;
	GSList *list;

	g_return_val_if_fail (iface && IS_MG_DB_CONSTRAINT (iface), NULL);
	g_return_val_if_fail (MG_DB_CONSTRAINT (iface)->priv, NULL);
	g_return_val_if_fail (MG_DB_CONSTRAINT (iface)->priv->table, NULL);

	cstr = MG_DB_CONSTRAINT (iface);
	mg_db_constraint_activate (MG_REFERER (cstr));

	if (! mg_db_constraint_is_active (MG_REFERER (cstr))) {
		g_set_error (error,
			     MG_DB_CONSTRAINT_ERROR,
			     MG_DB_CONSTRAINT_XML_SAVE_ERROR,
			     _("Constraint cannot be activated!"));
		return NULL;
	}
	node = xmlNewNode (NULL, "MG_CONSTRAINT");
	
	xmlSetProp (node, "name", mg_base_get_name (MG_BASE (cstr)));
	xmlSetProp (node, "user_defined", cstr->priv->user_defined ? "t" : "f");
	xmlSetProp (node, "type", constraint_type_to_str (cstr->priv->type));

	str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (cstr->priv->table));
	xmlSetProp (node, "table", str);
	g_free (str);

	switch (cstr->priv->type) {
	case CONSTRAINT_UNIQUE:
	case CONSTRAINT_PRIMARY_KEY:
		list = cstr->priv->multiple_fields;
		while (list) {
			child = xmlNewChild (node, NULL, "MG_CONSTRAINT_FIELD", NULL);
			str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (list->data));
			xmlSetProp (child, "field", str);
			g_free (str);
			list = g_slist_next (list);
		}
		break;
	case CONSTRAINT_FOREIGN_KEY:
		list = cstr->priv->fk_pairs;
		while (list) {
			child = xmlNewChild (node, NULL, "MG_CONSTRAINT_PAIR", NULL);
			str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (MG_DB_CONSTRAINT_FK_PAIR (list->data)->fkey));
			xmlSetProp (child, "field", str);
			g_free (str);
			str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (MG_DB_CONSTRAINT_FK_PAIR (list->data)->ref_pkey));
			xmlSetProp (child, "ref", str);
			g_free (str);
			list = g_slist_next (list);
		}

		xmlSetProp (node, "on_update", constraint_action_to_str (cstr->priv->on_update));
		xmlSetProp (node, "on_delete", constraint_action_to_str (cstr->priv->on_delete));
		break;
	case CONSTRAINT_NOT_NULL:
		child = xmlNewChild (node, NULL, "MG_CONSTRAINT_FIELD", NULL);
		if (cstr->priv->single_field)
			str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (cstr->priv->single_field));
		xmlSetProp (child, "field", str);
		g_free (str);
		break;
	case CONSTRAINT_CHECK_EXPR:
		TO_IMPLEMENT;
		break;
	default:
		break;
	}

	return node;
}

static gboolean
mg_db_constraint_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgDbConstraint *cstr;
	gchar *prop;
	gboolean type = FALSE, field_error = FALSE;
	MgDbTable *table = NULL;
	MgDatabase *db;
	gchar *ref_pb = NULL;
	xmlNodePtr subnode;
	GSList *contents, *list;

	g_return_val_if_fail (iface && IS_MG_DB_CONSTRAINT (iface), FALSE);
	g_return_val_if_fail (MG_DB_CONSTRAINT (iface)->priv, FALSE);
	g_return_val_if_fail (node, FALSE);
	g_return_val_if_fail (!MG_DB_CONSTRAINT (iface)->priv->table, FALSE);
	db = g_object_get_data (G_OBJECT (iface), "db");
	g_return_val_if_fail (db, FALSE);

	cstr = MG_DB_CONSTRAINT (iface);
	if (strcmp (node->name, "MG_CONSTRAINT")) {
		g_set_error (error,
			     MG_DB_CONSTRAINT_ERROR,
			     MG_DB_CONSTRAINT_XML_LOAD_ERROR,
			     _("XML Tag is not <MG_CONSTRAINT>"));
		return FALSE;
	}

	prop = xmlGetProp (node, "name");
	if (prop) {
		mg_base_set_name (MG_BASE (cstr), prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "table");
	if (prop) {
		table = mg_database_get_table_by_xml_id (db, prop);
		if (table) {
			g_object_set_data (G_OBJECT (iface), "db", NULL);
			g_signal_handlers_disconnect_by_func (G_OBJECT (db),
							      G_CALLBACK (nullified_object_cb), cstr);
			cstr->priv->table = table;
			g_signal_connect (G_OBJECT (table), "nullified",
					  G_CALLBACK (nullified_object_cb), cstr);
			g_free (prop);
		}
		else 
			ref_pb = prop;
	}
	
	prop = xmlGetProp (node, "user_defined");
	if (prop) {
		cstr->priv->user_defined = (*prop && (*prop == 't')) ? TRUE : FALSE;
		g_free (prop);
	}

	prop = xmlGetProp (node, "type");
	if (prop) {
		cstr->priv->type = constraint_str_to_type (prop);
		g_free (prop);
		type = TRUE;
	}
	
	prop = xmlGetProp (node, "on_update");
	if (prop) {
		cstr->priv->on_update = constraint_str_to_action (prop);
		g_free (prop);
		type = TRUE;
	}

	prop = xmlGetProp (node, "on_delete");
	if (prop) {
		cstr->priv->on_delete = constraint_str_to_action (prop);
		g_free (prop);
		type = TRUE;
	}

	/* contents of the constraint if applicable */
	contents = NULL;
	subnode = node->children;
	while (subnode) {
		if (!strcmp (subnode->name, "MG_CONSTRAINT_FIELD")) {
			MgDbField *field = NULL;
			prop = xmlGetProp (subnode, "field");
			if (prop) {
				field = mg_database_get_field_by_xml_id (db, prop);
				g_free (prop);
			}
			if (field)
				contents = g_slist_append (contents, field);
			else {
				field_error = TRUE;
				ref_pb = xmlGetProp (subnode, "field");
			}
		}
		
		if (!strcmp (subnode->name, "MG_CONSTRAINT_PAIR")) {
			MgDbField *field = NULL, *ref = NULL;
			prop = xmlGetProp (subnode, "field");
			if (prop) {
				field = mg_database_get_field_by_xml_id (db, prop);
				g_free (prop);
			}
			prop = xmlGetProp (subnode, "ref");
			if (prop) {
				ref = mg_database_get_field_by_xml_id (db, prop);
				g_free (prop);
			}
			if (field && ref) {
				MgDbConstraintFkeyPair *pair;

				pair = g_new0 (MgDbConstraintFkeyPair, 1);
				pair->fkey = field;
				pair->ref_pkey = ref;
				pair->ref_pkey_repl = NULL;
				contents = g_slist_append (contents, pair);
			}
			else {
				field_error = TRUE;
				if (!field)
					ref_pb = xmlGetProp (subnode, "field");
				else
					ref_pb = xmlGetProp (subnode, "ref");
			}
		}
		subnode = subnode->next;
	}

	switch (cstr->priv->type) {
	case CONSTRAINT_PRIMARY_KEY:
		mg_db_constraint_pkey_set_fields (cstr, contents);
		g_slist_free (contents);
		break;
        case CONSTRAINT_FOREIGN_KEY:
		mg_db_constraint_fkey_set_fields (cstr, contents);
		list = contents;
		while (list) {
			g_free (list->data);
			list = g_slist_next (list);
		}
		g_slist_free (contents);
		break;
        case CONSTRAINT_UNIQUE:
		mg_db_constraint_unique_set_fields (cstr, contents);
		g_slist_free (contents);
		break;
        case CONSTRAINT_NOT_NULL:
		if (contents) {
			mg_db_constraint_not_null_set_field (cstr, contents->data);
			g_slist_free (contents);
		}
		else
			field_error = TRUE;
		break;
        case CONSTRAINT_CHECK_EXPR:
		TO_IMPLEMENT;
		break;
	default:
		break;
	}

	if (type && table && !field_error)
		return TRUE;
	else {
		if (!type)
			g_set_error (error,
				     MG_DB_CONSTRAINT_ERROR,
				     MG_DB_CONSTRAINT_XML_LOAD_ERROR,
				     _("Missing required attributes for <MG_CONSTRAINT>"));
		if (!table) {
			g_set_error (error,
				     MG_DB_CONSTRAINT_ERROR,
				     MG_DB_CONSTRAINT_XML_LOAD_ERROR,
				     _("Referenced table (%s) not found"), ref_pb);
			if (ref_pb)
				g_free (ref_pb);
		}
		if (field_error) {
			g_set_error (error,
				     MG_DB_CONSTRAINT_ERROR,
				     MG_DB_CONSTRAINT_XML_LOAD_ERROR,
				     _("Referenced field in constraint (%s) not found"), ref_pb);
			if (ref_pb)
				g_free (ref_pb);
		}
		return FALSE;
	}
}




/*
 * MgReferer interface implementation
 */

/*
 * mg_db_constraint_activate
 *
 * Tries to convert any MgRefBase object to the pointed MgDbField instead
 */
static gboolean
mg_db_constraint_activate (MgReferer *iface)
{
	gboolean activated = TRUE;
	MgDbConstraint *cstr;
	
	g_return_val_if_fail (iface && IS_MG_DB_CONSTRAINT (iface), FALSE);
	cstr = MG_DB_CONSTRAINT (iface);
	g_return_val_if_fail (cstr->priv, FALSE);
	g_return_val_if_fail (cstr->priv->table, FALSE);

	if (mg_db_constraint_is_active (MG_REFERER (cstr)))
		return TRUE;

	if (cstr->priv->type == CONSTRAINT_FOREIGN_KEY) {
		GSList *list;
		MgDbConstraintFkeyPair *pair;
		MgBase *ref;

		list = cstr->priv->fk_pairs;
		while (list) {
			pair = MG_DB_CONSTRAINT_FK_PAIR (list->data);
			if (!pair->ref_pkey) {
				g_assert (pair->ref_pkey_repl);
				ref = mg_ref_base_get_ref_object (pair->ref_pkey_repl);
				if (ref) {
					pair->ref_pkey = MG_DB_FIELD (ref);
					g_object_unref (G_OBJECT (pair->ref_pkey_repl));
					pair->ref_pkey_repl = NULL;
					g_signal_connect (G_OBJECT (pair->ref_pkey), "nullified",
							  G_CALLBACK (nullified_object_cb), cstr);
				}
			}

			if (!pair->ref_pkey)
				activated = FALSE;
			
			list = g_slist_next (list);
		}
	}

	return activated;
}

static void
mg_db_constraint_deactivate (MgReferer *iface)
{
	g_return_if_fail (iface && IS_MG_DB_CONSTRAINT (iface));
	/* do nothing */
}

/*
 * mg_db_constraint_is_active
 *
 * Tells wether the MgDbConstraint object has found all the
 * MgDbField objects it needs
 */
static gboolean
mg_db_constraint_is_active (MgReferer *iface)
{
	gboolean activated = TRUE;
	MgDbConstraint *cstr;
	
	g_return_val_if_fail (iface && IS_MG_DB_CONSTRAINT (iface), FALSE);
	cstr = MG_DB_CONSTRAINT (iface);
	g_return_val_if_fail (cstr->priv, FALSE);
	g_return_val_if_fail (cstr->priv->table, FALSE);

	if (cstr->priv->type == CONSTRAINT_FOREIGN_KEY) {
		GSList *list = cstr->priv->fk_pairs;
		while (list && activated) {
			MgDbConstraintFkeyPair *pair = MG_DB_CONSTRAINT_FK_PAIR (list->data);
			if (pair->ref_pkey_repl)
				activated = FALSE;
			list = g_slist_next (list);
		}
	}

	return activated;
}

static GSList *
mg_db_constraint_get_ref_objects (MgReferer *iface)
{
	g_return_val_if_fail (iface && IS_MG_DB_CONSTRAINT (iface), NULL);
	/* do nothing */
	return NULL;
}

static void mg_db_constraint_replace_refs (MgReferer *iface, GHashTable *replacements)
{
	g_return_if_fail (iface && IS_MG_DB_CONSTRAINT (iface));
	/* do nothing */
}
