/* mg-dbms-update-viewer.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <string.h>
#include "mg-dbms-update-viewer.h"
#include "mg-server.h"

static void mg_dbms_update_viewer_class_init (MgDbmsUpdateViewerClass * class);
static void mg_dbms_update_viewer_init (MgDbmsUpdateViewer * wid);
static void mg_dbms_update_viewer_dispose (GObject   * object);


struct _MgDbmsUpdateViewerPriv
{
	MgConf      *conf;
	GtkWidget   *table;
	GtkWidget   *pbar;

	GSList      *signal_keys; /* pointer to string keys */
	gchar       *current_key;
};


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

guint
mg_dbms_update_viewer_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgDbmsUpdateViewerClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_dbms_update_viewer_class_init,
			NULL,
			NULL,
			sizeof (MgDbmsUpdateViewer),
			0,
			(GInstanceInitFunc) mg_dbms_update_viewer_init
		};		
		
		type = g_type_register_static (GTK_TYPE_VBOX, "MgDbmsUpdateViewer", &info, 0);
	}

	return type;
}

static void
mg_dbms_update_viewer_class_init (MgDbmsUpdateViewerClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_dbms_update_viewer_dispose;
}

static void
mg_dbms_update_viewer_init (MgDbmsUpdateViewer * wid)
{
	wid->priv = g_new0 (MgDbmsUpdateViewerPriv, 1);
	wid->priv->conf = NULL;
	wid->priv->table = NULL;
	wid->priv->pbar = NULL;
	wid->priv->signal_keys = NULL;
	wid->priv->current_key = NULL;
}

static void mg_dbms_update_viewer_do_init (MgDbmsUpdateViewer *mgv);
static void mg_conf_weak_notify (MgDbmsUpdateViewer *mgv, MgConf *conf);
/**
 * mg_dbms_update_viewer_new
 * @conf: a #MgConf object
 *
 * Creates a new #MgDbmsUpdateViewer widget.
 *
 * Returns: the new widget
 */
GtkWidget *
mg_dbms_update_viewer_new (MgConf *conf)
{
	GObject    *obj;
	MgDbmsUpdateViewer *mgv;

	g_return_val_if_fail (conf && IS_MG_CONF (conf), NULL);

	obj = g_object_new (MG_DBMS_UPDATE_VIEWER_TYPE, NULL);
	mgv = MG_DBMS_UPDATE_VIEWER (obj);

	mgv->priv->conf = conf;

	g_object_weak_ref (G_OBJECT (mgv->priv->conf),
			   (GWeakNotify) mg_conf_weak_notify, mgv);

	mg_dbms_update_viewer_do_init (mgv);

	return GTK_WIDGET (obj);
}

static void
mg_conf_weak_notify (MgDbmsUpdateViewer *mgv, MgConf *conf)
{
	/* Tell that we don't need to weak unref the MgConf */
	mgv->priv->conf = NULL;
}

static void update_started_cb (MgServer *srv, MgDbmsUpdateViewer *mgv);
static void update_finished_cb (MgServer *srv, MgDbmsUpdateViewer *mgv);
static void update_progress_cb (MgServer *srv, gchar *key, guint now, guint total, 
				MgDbmsUpdateViewer *mgv);

static void
mg_dbms_update_viewer_dispose (GObject *object)
{
	MgDbmsUpdateViewer *mgv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_DBMS_UPDATE_VIEWER (object));
	mgv = MG_DBMS_UPDATE_VIEWER (object);

	if (mgv->priv) {
		/* Weak unref the MgConf is necessary */
		if (mgv->priv->conf) {
			MgServer *srv = mg_conf_get_server (mgv->priv->conf);
			MgDatabase *db = mg_conf_get_database (mgv->priv->conf);
			g_object_weak_unref (G_OBJECT (mgv->priv->conf),
					     (GWeakNotify) mg_conf_weak_notify, mgv);

			g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
							      G_CALLBACK (update_started_cb), mgv);
			g_signal_handlers_disconnect_by_func (G_OBJECT (srv), 
							      G_CALLBACK (update_finished_cb), mgv);
			g_signal_handlers_disconnect_by_func (G_OBJECT (srv),
							      G_CALLBACK (update_progress_cb), mgv);
			g_signal_handlers_disconnect_by_func (G_OBJECT (db),
							      G_CALLBACK (update_started_cb), mgv);
			g_signal_handlers_disconnect_by_func (G_OBJECT (db), 
							      G_CALLBACK (update_finished_cb), mgv);
			g_signal_handlers_disconnect_by_func (G_OBJECT (db),
							      G_CALLBACK (update_progress_cb), mgv);
		}
		
		/* keys list */
		if (mgv->priv->signal_keys) {
			GSList *list = mgv->priv->signal_keys;
			while (list) {
				g_free (list->data);
				list = g_slist_next (list);
			}

			g_slist_free (mgv->priv->signal_keys);
			mgv->priv->signal_keys = NULL;
		}

		/* the private area itself */
		g_free (mgv->priv);
		mgv->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}


static void append_refresh_element_table (MgDbmsUpdateViewer *mgv, guint order,
                                          const gchar * signal_key, const gchar *descr);
static void
mg_dbms_update_viewer_do_init (MgDbmsUpdateViewer *mgv)
{
	GtkWidget *table, *pbar;
	MgServer *srv;
	MgDatabase *db;

	gtk_container_set_border_width (GTK_CONTAINER (mgv), 5);

	/* table and status items */
	table = gtk_table_new (7, 2, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 0);
	gtk_table_set_col_spacings (GTK_TABLE (table), 6);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	gtk_box_pack_start (GTK_BOX (mgv), table, TRUE, TRUE, 6);
	gtk_widget_show (table);
	mgv->priv->table = table;

	/* adding status items */
	append_refresh_element_table (mgv, 0, "DATA_TYPES", _("Data types analysis"));
	append_refresh_element_table (mgv, 1, "FUNCTIONS", _("Functions analysis"));
	append_refresh_element_table (mgv, 2, "AGGREGATES", _("Aggregates analysis"));
	append_refresh_element_table (mgv, 3, NULL, NULL);
	append_refresh_element_table (mgv, 4, "TABLES", _("Tables analysis"));
	append_refresh_element_table (mgv, 5, "CONSTRAINTS", _("Database constraints analysis"));
	append_refresh_element_table (mgv, 6, "SEQUENCES", _("Sequences analysis"));

	/* progress bar */
	pbar = gtk_progress_bar_new ();
	gtk_progress_bar_set_orientation (GTK_PROGRESS_BAR (pbar), GTK_PROGRESS_LEFT_TO_RIGHT);
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (pbar), .0);
	gtk_box_pack_start (GTK_BOX (mgv), pbar, FALSE, FALSE, 0);
	gtk_widget_show (pbar);
	mgv->priv->pbar = pbar;

	mg_dbms_update_viewer_reset (mgv);

	srv = mg_conf_get_server (mgv->priv->conf);
	g_signal_connect (G_OBJECT (srv), "data_update_started",
			  G_CALLBACK (update_started_cb), mgv);
	g_signal_connect (G_OBJECT (srv), "data_update_finished",
			  G_CALLBACK (update_finished_cb), mgv);
	g_signal_connect (G_OBJECT (srv), "update_progress",
			  G_CALLBACK (update_progress_cb), mgv);

	db = mg_conf_get_database (mgv->priv->conf);
	g_signal_connect (G_OBJECT (db), "data_update_started",
			  G_CALLBACK (update_started_cb), mgv);
	g_signal_connect (G_OBJECT (db), "data_update_finished",
			  G_CALLBACK (update_finished_cb), mgv);
	g_signal_connect (G_OBJECT (db), "update_progress",
			  G_CALLBACK (update_progress_cb), mgv);
}

static void
append_refresh_element_table (MgDbmsUpdateViewer *mgv, guint order,
			      const gchar *signal_key, const gchar *descr)
{
	if (signal_key) {
		gchar *str;
		GtkWidget *vbox, *label, *wid;

		/* adding the key to the list */
		mgv->priv->signal_keys = g_slist_append (mgv->priv->signal_keys, g_strdup (signal_key));

		/* icons */
		vbox = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox);
		gtk_table_attach (GTK_TABLE (mgv->priv->table), vbox, 0, 1, order, order+1, 0, 0, 0, 0);
		
		wid = gtk_image_new_from_stock (GTK_STOCK_APPLY, GTK_ICON_SIZE_MENU);
		gtk_box_pack_start (GTK_BOX (vbox), wid, FALSE, FALSE, 0);
		str = g_strdup_printf ("%sD", signal_key);
		g_object_set_data (G_OBJECT (mgv->priv->table), str, wid);
		g_free (str);
		gtk_widget_hide (wid);
		
		wid = gtk_image_new_from_stock (GTK_STOCK_GO_FORWARD, GTK_ICON_SIZE_MENU);
		gtk_box_pack_start (GTK_BOX (vbox), wid, FALSE, FALSE, 0);
		str = g_strdup_printf ("%sN", signal_key);
		g_object_set_data (G_OBJECT (mgv->priv->table), str, wid);
		g_free (str);
		gtk_widget_hide (wid);
		
		/* description */
		vbox = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox);
		gtk_table_attach_defaults (GTK_TABLE (mgv->priv->table), vbox, 1, 2, order, order+1);
		
		label = gtk_label_new (descr);
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
		gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);
		str = g_strdup_printf ("%sL", signal_key);
		g_object_set_data (G_OBJECT (mgv->priv->table), str, label);
		gtk_widget_show (label);
		g_free (str);
		
		label = gtk_label_new (NULL);
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
		str = g_strdup_printf ("<b>%s</b>", descr);
		gtk_label_set_markup (GTK_LABEL (label), str);
		g_free (str);
		gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);
		str = g_strdup_printf ("%sB", signal_key);
		g_object_set_data (G_OBJECT (mgv->priv->table), str, label);
		g_free (str);
	}
	else {
		GtkWidget *hsep;

		hsep = gtk_hseparator_new ();
		gtk_widget_show (hsep);
		gtk_table_attach (GTK_TABLE (mgv->priv->table), hsep, 0, 2, order, order+1, GTK_FILL, 0, 0, 0);
	}
}


static void mg_dbms_update_viewer_do_reset (MgDbmsUpdateViewer *mgv, gboolean show_first);


static void
update_started_cb (MgServer *srv, MgDbmsUpdateViewer *mgv)
{
	mg_dbms_update_viewer_do_reset (mgv, FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (mgv), TRUE);
}

static void
update_finished_cb (MgServer *srv, MgDbmsUpdateViewer *mgv)
{
	if (mgv->priv->current_key) {
		g_free (mgv->priv->current_key);
		mgv->priv->current_key = NULL;
	}
	mg_dbms_update_viewer_reset (mgv);
}

static void
update_progress_cb (MgServer *srv, gchar *key, guint now, guint total, MgDbmsUpdateViewer *mgv)
{
	GtkWidget *table = mgv->priv->table;
	GtkWidget *pbar = mgv->priv->pbar;

	if (key) {
		if (! mgv->priv->current_key || strcmp (mgv->priv->current_key, key)) {
			/* set to "in process" status */
			gchar *str;
			GtkWidget *wid;
			
			/* icon */
			str = g_strdup_printf ("%sN", key);
			wid = g_object_get_data (G_OBJECT (table), str);
			g_free (str);
			gtk_widget_show (wid);
			
			/* description */
			str = g_strdup_printf ("%sL", key);
			wid = g_object_get_data (G_OBJECT (table), str);
			g_free (str);
			gtk_widget_hide (wid);
			
			str = g_strdup_printf ("%sB", key);
			wid = g_object_get_data (G_OBJECT (table), str);
			g_free (str);
			gtk_widget_show (wid);
			
			if (mgv->priv->current_key)
				g_free (mgv->priv->current_key);
			mgv->priv->current_key = g_strdup (key);
		}
		if (total)
			gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (pbar),
						       (gdouble) now / (gdouble) total);
		else
			gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (pbar), 0.);
	}
	else {
		if (mgv->priv->current_key) {
			/* set to done state */
			gchar *str;
			GtkWidget *wid;
			
			/* icon */
			str = g_strdup_printf ("%sN", mgv->priv->current_key);
			wid = g_object_get_data (G_OBJECT (table), str);
			g_free (str);
			gtk_widget_hide (wid);
			
			str = g_strdup_printf ("%sD", mgv->priv->current_key);
			wid = g_object_get_data (G_OBJECT (table), str);
			g_free (str);
			gtk_widget_show (wid);
			
			/* description */
			str = g_strdup_printf ("%sL", mgv->priv->current_key);
			wid = g_object_get_data (G_OBJECT (table), str);
			g_free (str);
			gtk_widget_show (wid);
			
			str = g_strdup_printf ("%sB", mgv->priv->current_key);
			wid = g_object_get_data (G_OBJECT (table), str);
			g_free (str);
			gtk_widget_hide (wid);
			
			/* current key is not valid anymore */
			g_free (mgv->priv->current_key);
			mgv->priv->current_key = NULL;
		}
	}

	/* We'd better do this if we want to display anything... */
	while (gtk_events_pending ())
		gtk_main_iteration ();
}


/**
 * mg_dbms_update_viewer_reset
 * @mgv: An #MgDbmsUpdateViewer widget
 *
 * Reset the widget to the default (clears all the items and sets the progress bar to 0%)
 */
void
mg_dbms_update_viewer_reset (MgDbmsUpdateViewer *mgv)
{
	g_return_if_fail (mgv && IS_MG_DBMS_UPDATE_VIEWER (mgv));
	g_return_if_fail (mgv->priv);
	
	mg_dbms_update_viewer_do_reset (mgv, TRUE);
}


/*
 * mg_dbms_update_viewer_do_reset
 */
static void
mg_dbms_update_viewer_do_reset (MgDbmsUpdateViewer *mgv, gboolean show_first)
{
	GtkWidget *wid;
	GSList *list;
	gchar *str;

	/* hide all the icons and set all the text to be normal and not bold */
	list = mgv->priv->signal_keys;
	while (list) {
		str = g_strdup_printf ("%sD", (gchar *) list->data);
		wid = g_object_get_data (G_OBJECT (mgv->priv->table), str);
		gtk_widget_hide (wid);
		g_free (str);

		str = g_strdup_printf ("%sN", (gchar *) list->data);
		wid = g_object_get_data (G_OBJECT (mgv->priv->table), str);
		gtk_widget_hide (wid);
		g_free (str);

		str = g_strdup_printf ("%sL", (gchar *) list->data);
		wid = g_object_get_data (G_OBJECT (mgv->priv->table), str);
		g_free (str);
		gtk_widget_show (wid);

		str = g_strdup_printf ("%sB", (gchar *) list->data);
		wid = g_object_get_data (G_OBJECT (mgv->priv->table), str);
		g_free (str);
		gtk_widget_hide (wid);

		list = g_slist_next (list);
	}

	if (show_first) {
		/* show first icon */
		list = mgv->priv->signal_keys;
		str = g_strdup_printf ("%sN", (gchar *) list->data);
		wid = g_object_get_data (G_OBJECT (mgv->priv->table), str);
		gtk_widget_show (wid);
		g_free (str);
	}

	/* reset progress bar */
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (mgv->priv->pbar), 0.);

	/* current key reset */
	if (mgv->priv->current_key) {
		g_free (mgv->priv->current_key);
		mgv->priv->current_key = NULL;
	}
	
	/* set the whole thing to non sensitive */
	gtk_widget_set_sensitive (GTK_WIDGET (mgv), mgv->priv->current_key ? TRUE : FALSE);
}
