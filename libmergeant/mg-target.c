/* mg-target.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-target.h"
#include "mg-entity.h"
#include <string.h>
#include "mg-ref-base.h"
#include "mg-xml-storage.h"
#include "mg-referer.h"
#include "mg-renderer.h"
#include "mg-query.h"
#include "mg-db-table.h"

/* 
 * Main static functions 
 */
static void mg_target_class_init (MgTargetClass * class);
static void mg_target_init (MgTarget * srv);
static void mg_target_dispose (GObject   * object);
static void mg_target_finalize (GObject   * object);

static void mg_target_set_property (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec);
static void mg_target_get_property (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec);

/* XML storage interface */
static void        mg_target_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_target_get_xml_id       (MgXmlStorage *iface);
static xmlNodePtr  mg_target_save_to_xml      (MgXmlStorage *iface, GError **error);
static gboolean    mg_target_load_from_xml    (MgXmlStorage *iface, xmlNodePtr node, GError **error);

/* Referer interface */
static void        mg_target_referer_init        (MgRefererIface *iface);
static gboolean    mg_target_activate            (MgReferer *iface);
static void        mg_target_deactivate          (MgReferer *iface);
static gboolean    mg_target_is_active           (MgReferer *iface);
static GSList     *mg_target_get_ref_objects     (MgReferer *iface);
static void        mg_target_replace_refs        (MgReferer *iface, GHashTable *replacements);

/* Renderer interface */
static void        mg_target_renderer_init   (MgRendererIface *iface);
static GdaXqlItem *mg_target_render_as_xql   (MgRenderer *iface, MgContext *context, GError **error);
static gchar      *mg_target_render_as_sql   (MgRenderer *iface, MgContext *context, GError **error);
static gchar      *mg_target_render_as_str   (MgRenderer *iface, MgContext *context);

/* Alias interface */
/* static void        mg_target_alias_init        (MgAlias *iface); */


/* When the MgQuery is nullified */
static void        nullified_object_cb (GObject *obj, MgTarget *target);

#ifdef debug
static void        mg_target_dump (MgTarget *table, guint offset);
#endif

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;


/* properties */
enum
{
	PROP_0,
	PROP
};


/* private structure */
struct _MgTargetPrivate
{
	MgQuery    *query;
	MgRefBase  *entity_ref;
	gchar      *alias;
};


/* module error */
GQuark mg_target_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_target_error");
	return quark;
}


guint
mg_target_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgTargetClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_target_class_init,
			NULL,
			NULL,
			sizeof (MgTarget),
			0,
			(GInstanceInitFunc) mg_target_init
		};

		static const GInterfaceInfo xml_storage_info = {
			(GInterfaceInitFunc) mg_target_xml_storage_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo referer_info = {
			(GInterfaceInitFunc) mg_target_referer_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo renderer_info = {
			(GInterfaceInitFunc) mg_target_renderer_init,
			NULL,
			NULL
		};
		
		type = g_type_register_static (MG_BASE_TYPE, "MgTarget", &info, 0);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
		g_type_add_interface_static (type, MG_REFERER_TYPE, &referer_info);
		g_type_add_interface_static (type, MG_RENDERER_TYPE, &renderer_info);
	}
	return type;
}

static void 
mg_target_xml_storage_init (MgXmlStorageIface *iface)
{
	iface->get_xml_id = mg_target_get_xml_id;
	iface->save_to_xml = mg_target_save_to_xml;
	iface->load_from_xml = mg_target_load_from_xml;
}

static void
mg_target_referer_init (MgRefererIface *iface)
{
	iface->activate = mg_target_activate;
	iface->deactivate = mg_target_deactivate;
	iface->is_active = mg_target_is_active;
	iface->get_ref_objects = mg_target_get_ref_objects;
	iface->replace_refs = mg_target_replace_refs;
}

static void
mg_target_renderer_init (MgRendererIface *iface)
{
	iface->render_as_xql = mg_target_render_as_xql;
	iface->render_as_sql = mg_target_render_as_sql;
	iface->render_as_str = mg_target_render_as_str;
}

static void
mg_target_class_init (MgTargetClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_target_dispose;
	object_class->finalize = mg_target_finalize;

	/* Properties */
	object_class->set_property = mg_target_set_property;
	object_class->get_property = mg_target_get_property;
	g_object_class_install_property (object_class, PROP,
					 g_param_spec_pointer ("prop", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_target_dump;
#endif

}

static void
mg_target_init (MgTarget *mg_target)
{
	mg_target->priv = g_new0 (MgTargetPrivate, 1);
	mg_target->priv->query = NULL;
	mg_target->priv->entity_ref = NULL;
	mg_target->priv->alias = NULL;
}

/**
 * mg_target_new_with_entity
 * @query: a #MgQuery object
 * @entity: an object implementing the #MgEntity interface
 *
 * Creates a new #MgTarget object, specifying the #MgEntity to represent
 *
 * Returns: the new object
 */
GObject*
mg_target_new_with_entity (MgQuery *query, MgEntity *entity)
{
	GObject *obj;
	MgTarget *mg_target;
	MgConf *conf;
	guint id;

	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (entity && IS_MG_ENTITY (entity), NULL);

	conf = mg_base_get_conf (MG_BASE (query));
	obj = g_object_new (MG_TARGET_TYPE, "conf", conf, NULL);
	mg_target = MG_TARGET (obj);
	g_object_get (G_OBJECT (query), "target_serial", &id, NULL);
	mg_base_set_id (MG_BASE (mg_target), id);

	mg_target->priv->query = query;
	g_signal_connect (G_OBJECT (query), "nullified",
			  G_CALLBACK (nullified_object_cb), mg_target);

	mg_target->priv->entity_ref = MG_REF_BASE (mg_ref_base_new (conf));
	mg_ref_base_set_ref_object (mg_target->priv->entity_ref, MG_BASE (entity));

	return obj;
}

/**
 * mg_target_new_with_xml_id
 * @query: a #MgQuery object
 * @entity_xml_id: the XML Id of an object implementing the #MgEntity interface
 *
 * Creates a new #MgTarget object, specifying the XML id of the #MgEntity to represent
 *
 * Returns: the new object
 */
GObject *
mg_target_new_with_xml_id (MgQuery *query, const gchar *entity_xml_id)
{
	GObject   *obj;
	MgTarget *mg_target;
	MgConf *conf;
	GType target_ref;
	guint id;

	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (entity_xml_id && *entity_xml_id, NULL);

	conf = mg_base_get_conf (MG_BASE (query));
	obj = g_object_new (MG_TARGET_TYPE, "conf", conf, NULL);
	mg_target = MG_TARGET (obj);
	g_object_get (G_OBJECT (query), "target_serial", &id, NULL);
	mg_base_set_id (MG_BASE (mg_target), id);

	mg_target->priv->query = query;
	g_signal_connect (G_OBJECT (query), "nullified",
			  G_CALLBACK (nullified_object_cb), mg_target);

	mg_target->priv->entity_ref = MG_REF_BASE (mg_ref_base_new (conf));
	if (*entity_xml_id == 'T') 
		target_ref = MG_DB_TABLE_TYPE;
	else
		target_ref = MG_QUERY_TYPE;
	mg_ref_base_set_ref_name (mg_target->priv->entity_ref, target_ref, REFERENCE_BY_XML_ID, entity_xml_id);

	return obj;
}


/**
 * mg_target_new_copy
 * @orig: a #MgTarget object to copy
 *
 * Makes a copy of an existing object (copy constructor)
 *
 * Returns: the new object
 */
GObject *
mg_target_new_copy (MgTarget *orig)
{
	GObject   *obj;
	MgTarget *mg_target;
	MgConf *conf;

	g_return_val_if_fail (orig && IS_MG_TARGET (orig), NULL);

	conf = mg_base_get_conf (MG_BASE (orig));
	obj = g_object_new (MG_TARGET_TYPE, "conf", conf, NULL);
	mg_target = MG_TARGET (obj);

	mg_target->priv->query = orig->priv->query;
	g_signal_connect (G_OBJECT (orig->priv->query), "nullified",
			  G_CALLBACK (nullified_object_cb), mg_target);

	mg_target->priv->entity_ref = MG_REF_BASE (mg_ref_base_new_copy (orig->priv->entity_ref));

	return obj;	
}


static void
nullified_object_cb (GObject *obj, MgTarget *target)
{
	mg_base_nullify (MG_BASE (target));
}


static void
mg_target_dispose (GObject *object)
{
	MgTarget *mg_target;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_TARGET (object));

	mg_target = MG_TARGET (object);
	if (mg_target->priv) {
		mg_base_nullify_check (MG_BASE (object));
		
		if (mg_target->priv->query) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (mg_target->priv->query),
							      G_CALLBACK (nullified_object_cb), mg_target);
			mg_target->priv->query = NULL;
		}
		if (mg_target->priv->entity_ref) {
			g_object_unref (G_OBJECT (mg_target->priv->entity_ref));
			mg_target->priv->entity_ref = NULL;
		}

		if (mg_target->priv->alias) {
			g_free (mg_target->priv->alias);
			mg_target->priv->alias = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_target_finalize (GObject   * object)
{
	MgTarget *mg_target;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_TARGET (object));

	mg_target = MG_TARGET (object);
	if (mg_target->priv) {
		g_free (mg_target->priv);
		mg_target->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_target_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	gpointer ptr;
	MgTarget *mg_target;

	mg_target = MG_TARGET (object);
	if (mg_target->priv) {
		switch (param_id) {
		case PROP:
			ptr = g_value_get_pointer (value);
			break;
		}
	}
}

static void
mg_target_get_property (GObject              *object,
			  guint                 param_id,
			  GValue               *value,
			  GParamSpec           *pspec)
{
	MgTarget *mg_target;
	mg_target = MG_TARGET (object);
	
	if (mg_target->priv) {
		switch (param_id) {
		case PROP:
			break;
		}	
	}
}


/**
 * mg_target_get_query
 * @target: a #MgTarget object
 *
 * Get the #MgQuery in which @target is
 *
 * Returns: the #MgQuery object
 */
MgQuery *
mg_target_get_query (MgTarget *target)
{
	g_return_val_if_fail (target && IS_MG_TARGET (target), NULL);
	g_return_val_if_fail (target->priv, NULL);

	return target->priv->query;
}

/**
 * mg_target_get_represented_entity
 * @target: a #MgTarget object
 *
 * Get the #MgEntity object which is represented by @target
 *
 * Returns: the #MgEntity object or NULL if @target is not active
 */
MgEntity *
mg_target_get_represented_entity (MgTarget *target)
{
	MgBase *ent;
	MgEntity *entity = NULL;

	g_return_val_if_fail (target && IS_MG_TARGET (target), NULL);
	g_return_val_if_fail (target->priv, NULL);

	ent = mg_ref_base_get_ref_object (target->priv->entity_ref);
	if (ent)
		entity = MG_ENTITY (ent);

	return entity;
}

/**
 * mg_target_set_alias
 * @target: a #MgTarget object
 * @alias: the alias
 *
 * Sets @target's alias to @alias
 */
void
mg_target_set_alias (MgTarget *target, const gchar *alias)
{
	g_return_if_fail (target && IS_MG_TARGET (target));
	g_return_if_fail (target->priv);

	if (target->priv->alias) {
		g_free (target->priv->alias);
		target->priv->alias = NULL;
	}
	
	if (alias)
		target->priv->alias = g_strdup (alias);
}

/**
 * mg_target_get_alias
 * @target: a #MgTarget object
 *
 * Get @target's alias
 *
 * Returns: the alias
 */
const gchar *
mg_target_get_alias (MgTarget *target)
{
	g_return_val_if_fail (target && IS_MG_TARGET (target), NULL);
	g_return_val_if_fail (target->priv, NULL);

	
	if (!target->priv->alias)
		target->priv->alias = g_strdup_printf ("t%d", mg_base_get_id (MG_BASE (target)));

	return target->priv->alias;
}


#ifdef debug
static void
mg_target_dump (MgTarget *target, guint offset)
{
	gchar *str;
        guint i;
	
	g_return_if_fail (target && IS_MG_TARGET (target));

        /* string for the offset */
        str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;

        /* dump */
        if (target->priv) {
                g_print ("%s" D_COL_H1 "MgTarget" D_COL_NOR " %p (id=%d) ",
                         str, target, mg_base_get_id (MG_BASE (target)));
		if (mg_target_is_active (MG_REFERER (target)))
			g_print ("Active, references %p ", mg_ref_base_get_ref_object (target->priv->entity_ref));
		else
			g_print (D_COL_ERR "Non active" D_COL_NOR ", ");
		g_print ("requested name: %s\n", mg_ref_base_get_ref_name (target->priv->entity_ref, NULL, NULL));
	}
        else
                g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, target);
}
#endif



/* 
 * MgReferer interface implementation
 */
static gboolean
mg_target_activate (MgReferer *iface)
{
	g_return_val_if_fail (iface && IS_MG_TARGET (iface), FALSE);
	g_return_val_if_fail (MG_TARGET (iface)->priv, FALSE);

	return mg_ref_base_activate (MG_TARGET (iface)->priv->entity_ref);
}

static void
mg_target_deactivate (MgReferer *iface)
{
	g_return_if_fail (iface && IS_MG_TARGET (iface));
	g_return_if_fail (MG_TARGET (iface)->priv);

	mg_ref_base_deactivate (MG_TARGET (iface)->priv->entity_ref);
}

static gboolean
mg_target_is_active (MgReferer *iface)
{
	g_return_val_if_fail (iface && IS_MG_TARGET (iface), FALSE);
	g_return_val_if_fail (MG_TARGET (iface)->priv, FALSE);

	return mg_ref_base_is_active (MG_TARGET (iface)->priv->entity_ref);
}

static GSList *
mg_target_get_ref_objects (MgReferer *iface)
{
	GSList *list = NULL;
	MgBase *base;
	g_return_val_if_fail (iface && IS_MG_TARGET (iface), NULL);
	g_return_val_if_fail (MG_TARGET (iface)->priv, NULL);

	base = mg_ref_base_get_ref_object (MG_TARGET (iface)->priv->entity_ref);
	if (base)
		list = g_slist_append (list, base);

	return list;
}

static void
mg_target_replace_refs (MgReferer *iface, GHashTable *replacements)
{
	MgTarget *target;

	g_return_if_fail (iface && IS_MG_TARGET (iface));
	g_return_if_fail (MG_TARGET (iface)->priv);

	target = MG_TARGET (iface);
	if (target->priv->query) {
		MgQuery *query = g_hash_table_lookup (replacements, target->priv->query);
		if (query) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (target->priv->query),
							      G_CALLBACK (nullified_object_cb), target);
			target->priv->query = query;
			g_signal_connect (G_OBJECT (query), "nullified",
					  G_CALLBACK (nullified_object_cb), target);
		}
	}

	mg_ref_base_replace_ref_object (target->priv->entity_ref, replacements);
}



/* 
 * MgXmlStorage interface implementation
 */
static gchar *
mg_target_get_xml_id (MgXmlStorage *iface)
{
	gchar *str, *retval;
	MgTarget *target;

	g_return_val_if_fail (iface && IS_MG_TARGET (iface), NULL);
	g_return_val_if_fail (MG_TARGET (iface)->priv, NULL);
	target = MG_TARGET (iface);

	str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (target->priv->query));
	retval = g_strdup_printf ("%s:T%d", str, mg_base_get_id (MG_BASE (target)));
	g_free (str);

	return retval;
}

static xmlNodePtr
mg_target_save_to_xml (MgXmlStorage *iface, GError **error)
{
	xmlNodePtr node = NULL;
	MgTarget *target;
	gchar *str;

	g_return_val_if_fail (iface && IS_MG_TARGET (iface), NULL);
	g_return_val_if_fail (MG_TARGET (iface)->priv, NULL);

	target = MG_TARGET (iface);

	node = xmlNewNode (NULL, "MG_TARGET");
	
	str = mg_target_get_xml_id (iface);
	xmlSetProp (node, "id", str);
	g_free (str);
	
	if (target->priv->entity_ref) {
		str = NULL;
		if (mg_ref_base_is_active (target->priv->entity_ref)) {
			MgBase *base = mg_ref_base_get_ref_object (target->priv->entity_ref);
			g_assert (base);
			str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (base));
		}
		else 
			str = g_strdup (mg_ref_base_get_ref_name (target->priv->entity_ref, NULL, NULL));
		
		if (str) {
			xmlSetProp (node, "entity_ref", str);
			g_free (str);
		}
	}

	return node;
}

static gboolean
mg_target_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgTarget *target;
	gboolean name=FALSE, id=FALSE;
	gchar *str;

	g_return_val_if_fail (iface && IS_MG_TARGET (iface), FALSE);
	g_return_val_if_fail (MG_TARGET (iface)->priv, FALSE);
	g_return_val_if_fail (node, FALSE);

	target = MG_TARGET (iface);
	if (strcmp (node->name, "MG_TARGET")) {
		g_set_error (error,
			     MG_TARGET_ERROR,
			     MG_TARGET_XML_LOAD_ERROR,
			     _("XML Tag is not <MG_TARGET>"));
		return FALSE;
	}

	str = xmlGetProp (node, "id");
	if (str) {
		gchar *tok, *ptr;

		ptr = strtok_r (str, ":", &tok);
		ptr = strtok_r (NULL, ":", &tok);
		if (*ptr && (*ptr == 'T')) {
			guint bid;
			
			bid = atoi (ptr + 1);
			mg_base_set_id (MG_BASE (target), bid);
			id = TRUE;
		}
		g_free (str);
	}

	str = xmlGetProp (node, "entity_ref");
	if (str) {
		if (target->priv->entity_ref) {
			GType target_ref;
			if (*str == 'T') 
				target_ref = MG_DB_TABLE_TYPE;
			else
				target_ref = MG_QUERY_TYPE;
			mg_ref_base_set_ref_name (target->priv->entity_ref, target_ref, 
						  REFERENCE_BY_XML_ID, str);
			name = TRUE;
		}
		g_free (str);
	}

	if (name)
		return TRUE;
	else {
		g_set_error (error,
			     MG_TARGET_ERROR,
			     MG_TARGET_XML_LOAD_ERROR,
			     _("Error loading data from <MG_TARGET> node"));
		return FALSE;
	}
}

/* 
 * MgRenderer interface implementation
 */
static GdaXqlItem *
mg_target_render_as_xql (MgRenderer *iface, MgContext *context, GError **error)
{
	g_return_val_if_fail (iface && IS_MG_TARGET (iface), NULL);
	g_return_val_if_fail (MG_TARGET (iface)->priv, NULL);

	TO_IMPLEMENT;
	return NULL;
}

static gchar *
mg_target_render_as_sql (MgRenderer *iface, MgContext *context, GError **error)
{
	gchar *str;
	GString *string = NULL;
	MgTarget *target;
	MgEntity *entity;
	gboolean done = FALSE;
	gboolean err = FALSE;

	g_return_val_if_fail (iface && IS_MG_TARGET (iface), NULL);
	g_return_val_if_fail (MG_TARGET (iface)->priv, NULL);
	g_return_val_if_fail (mg_referer_activate (MG_REFERER (iface)), NULL);
	target = MG_TARGET (iface);

	entity = mg_target_get_represented_entity (target);
	if (IS_MG_DB_TABLE (entity)) {
		string = g_string_new (mg_base_get_name (MG_BASE (entity)));
		done = TRUE;
	}

	if (IS_MG_QUERY (entity)) {
		string = g_string_new ("(");
		str = mg_renderer_render_as_sql (MG_RENDERER (entity), context, error);
		if (str) {
			g_string_append (string, str);
			g_free (str);
		}
		else
			err = TRUE;

		g_string_append (string, ")");
		done = TRUE;
	}

	g_assert (done);

	if (string) {
		if (!err) {
			/* adding alias */
			g_string_append (string, " AS ");
			g_string_append (string, mg_target_get_alias (target));
			
			str = string->str;
		}
		else
			str = NULL;
		g_string_free (string, err);
	}
	else
		str = NULL;

	return str;
}

static gchar *
mg_target_render_as_str (MgRenderer *iface, MgContext *context)
{
	g_return_val_if_fail (iface && IS_MG_TARGET (iface), NULL);
	g_return_val_if_fail (MG_TARGET (iface)->priv, NULL);

	TO_IMPLEMENT;
	return NULL;
}
