/* mg-join.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-join.h"
#include "mg-query.h"
#include "mg-ref-base.h"
#include "mg-target.h"
#include "mg-xml-storage.h"
#include "mg-referer.h"
#include "marshal.h"
#include <string.h>

/* 
 * Main static functions 
 */
static void mg_join_class_init (MgJoinClass * class);
static void mg_join_init (MgJoin * srv);
static void mg_join_dispose (GObject   * object);
static void mg_join_finalize (GObject   * object);

static void mg_join_set_property (GObject              *object,
				    guint                 param_id,
				    const GValue         *value,
				    GParamSpec           *pspec);
static void mg_join_get_property (GObject              *object,
				    guint                 param_id,
				    GValue               *value,
				    GParamSpec           *pspec);

/* XML storage interface */
static void        mg_join_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_join_get_xml_id       (MgXmlStorage *iface);
static xmlNodePtr  mg_join_save_to_xml      (MgXmlStorage *iface, GError **error);
static gboolean    mg_join_load_from_xml    (MgXmlStorage *iface, xmlNodePtr node, GError **error);

/* Referer interface */
static void        mg_join_referer_init        (MgRefererIface *iface);
static gboolean    mg_join_activate            (MgReferer *iface);
static void        mg_join_deactivate          (MgReferer *iface);
static gboolean    mg_join_is_active           (MgReferer *iface);
static GSList     *mg_join_get_ref_objects     (MgReferer *iface);
static void        mg_join_replace_refs        (MgReferer *iface, GHashTable *replacements);

/* When the Query or any of the refering MgTarget is nullified */
static void        nullified_object_cb (GObject *obj, MgJoin *join);
static void        target_ref_lost_cb (MgRefBase *ref, MgJoin *join);

#ifdef debug
static void        mg_join_dump (MgJoin *join, guint offset);
#endif

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	TYPE_CHANGED,
	CONDITION_CHANGED,
	LAST_SIGNAL
};

static gint mg_join_signals[LAST_SIGNAL] = { 0, 0 };

/* properties */
enum
{
	PROP_0,
	PROP
};


/* private structure */
struct _MgJoinPrivate
{
	MgJoinType   join_type;
	MgQuery     *query;
	MgRefBase   *target1;
	MgRefBase   *target2;
	MgCondition *cond;
};



/* module error */
GQuark mg_join_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_join_error");
	return quark;
}


guint
mg_join_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgJoinClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_join_class_init,
			NULL,
			NULL,
			sizeof (MgJoin),
			0,
			(GInstanceInitFunc) mg_join_init
		};

		static const GInterfaceInfo xml_storage_info = {
			(GInterfaceInitFunc) mg_join_xml_storage_init,
			NULL,
			NULL
		};

		static const GInterfaceInfo referer_info = {
			(GInterfaceInitFunc) mg_join_referer_init,
			NULL,
			NULL
		};
		
		type = g_type_register_static (MG_BASE_TYPE, "MgJoin", &info, 0);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
		g_type_add_interface_static (type, MG_REFERER_TYPE, &referer_info);
	}
	return type;
}

static void 
mg_join_xml_storage_init (MgXmlStorageIface *iface)
{
	iface->get_xml_id = mg_join_get_xml_id;
	iface->save_to_xml = mg_join_save_to_xml;
	iface->load_from_xml = mg_join_load_from_xml;
}

static void
mg_join_referer_init (MgRefererIface *iface)
{
	iface->activate = mg_join_activate;
	iface->deactivate = mg_join_deactivate;
	iface->is_active = mg_join_is_active;
	iface->get_ref_objects = mg_join_get_ref_objects;
	iface->replace_refs = mg_join_replace_refs;
}


static void
mg_join_class_init (MgJoinClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	mg_join_signals[TYPE_CHANGED] =
		g_signal_new ("type_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgJoinClass, type_changed),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	mg_join_signals[CONDITION_CHANGED] =
		g_signal_new ("condition_changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MgJoinClass, condition_changed),
			      NULL, NULL,
			      marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	class->type_changed = NULL;
	class->condition_changed = NULL;

	object_class->dispose = mg_join_dispose;
	object_class->finalize = mg_join_finalize;

	/* Properties */
	object_class->set_property = mg_join_set_property;
	object_class->get_property = mg_join_get_property;
	g_object_class_install_property (object_class, PROP,
					 g_param_spec_pointer ("prop", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_join_dump;
#endif

}

static void
mg_join_init (MgJoin * mg_join)
{
	mg_join->priv = g_new0 (MgJoinPrivate, 1);
	mg_join->priv->join_type = MG_JOIN_TYPE_INNER;
	mg_join->priv->query = NULL;
	mg_join->priv->target1 = NULL;
	mg_join->priv->target2 = NULL;
	mg_join->priv->cond = NULL;
}

/**
 * mg_join_new_with_targets
 * @query: a #MgQuery object in which the join will occur
 * @target_1: the 1st #MgTarget object participating in the join
 * @target_2: the 2nd #MgTarget object participating in the join
 *
 * Creates a new MgJoin object. Note: the #MgTarget ranks (1st and 2nd) does not matter, but
 * is necessary since the join may not be symetrical (LEFT or RIGHT join). Also, the #MgJoin object
 * may decide to swap the two if necessary.
 *
 * Returns: the new object
 */
GObject*
mg_join_new_with_targets (MgQuery *query, MgTarget *target_1, MgTarget *target_2)
{
	GObject   *obj;
	MgJoin *mg_join;
	MgConf *conf;

	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (target_1 && IS_MG_TARGET (target_1), NULL);
	g_return_val_if_fail (target_2 && IS_MG_TARGET (target_2), NULL);
	g_return_val_if_fail (mg_target_get_query (target_1) == query, NULL);
	g_return_val_if_fail (mg_target_get_query (target_2) == query, NULL);
	g_return_val_if_fail (target_1 != target_2, NULL);

	conf = mg_base_get_conf (MG_BASE (query));
	obj = g_object_new (MG_JOIN_TYPE, "conf", conf, NULL);
	mg_join = MG_JOIN (obj);
	mg_base_set_id (MG_BASE (mg_join), 0);

	mg_join->priv->query = query;
	mg_join->priv->target1 = MG_REF_BASE (mg_ref_base_new (conf));
	mg_ref_base_set_ref_object (mg_join->priv->target1, MG_BASE (target_1));

	mg_join->priv->target2 = MG_REF_BASE (mg_ref_base_new (conf));
	mg_ref_base_set_ref_object (mg_join->priv->target2, MG_BASE (target_2));
	
	g_signal_connect (G_OBJECT (query), "nullified",
			  G_CALLBACK (nullified_object_cb), mg_join);

	/* if the references to the any target is lost then we want to nullify the join */
	g_signal_connect (G_OBJECT (mg_join->priv->target1), "ref_lost",
			  G_CALLBACK (target_ref_lost_cb), mg_join);
	g_signal_connect (G_OBJECT (mg_join->priv->target2), "ref_lost",
			  G_CALLBACK (target_ref_lost_cb), mg_join);

	return obj;
}

/**
 * mg_join_new_with_xml_ids
 * @query: a #MgQuery object in which the join will occur
 * @target_1_xml_id: the 1st #MgTarget object's XML id participating in the join
 * @target_2_xml_id: the 2nd #MgTarget object's XML id participating in the join
 *
 * Creates a new MgJoin object. Note: the #MgTarget ranks (1st and 2nd) does not matter, but
 * is necessary since the join may not be symetrical (LEFT or RIGHT join). Also, the #MgJoin object
 * may decide to swap the two if necessary.
 *
 * Returns: the new object
 */
GObject *
mg_join_new_with_xml_ids (MgQuery *query, const gchar *target_1_xml_id, const gchar *target_2_xml_id)
{
	GObject   *obj;
	MgJoin *mg_join;
	MgConf *conf;
	gchar *qid, *ptr, *tok;
	gchar *tid;
	
	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	g_return_val_if_fail (target_1_xml_id && *target_1_xml_id, NULL);
	g_return_val_if_fail (target_2_xml_id && *target_2_xml_id, NULL);
	g_return_val_if_fail (strcmp (target_1_xml_id, target_2_xml_id), NULL);

	/* check that the XML Ids start with the query's XML Id */
	qid = mg_xml_storage_get_xml_id (MG_XML_STORAGE (query));
	tid = g_strdup (target_1_xml_id);
	ptr = strtok_r (tid, ":", &tok);
	g_return_val_if_fail (!strcmp (ptr, qid), NULL);
	g_free (tid);
	tid = g_strdup (target_2_xml_id);
	ptr = strtok_r (tid, ":", &tok);
	g_return_val_if_fail (!strcmp (ptr, qid), NULL);
	g_free (tid);
	g_free (qid);
	

	conf = mg_base_get_conf (MG_BASE (query));
	obj = g_object_new (MG_JOIN_TYPE, "conf", conf, NULL);
	mg_join = MG_JOIN (obj);
	mg_base_set_id (MG_BASE (mg_join), 0);

	mg_join->priv->query = query;
	mg_join->priv->target1 = MG_REF_BASE (mg_ref_base_new (conf));
	mg_ref_base_set_ref_name (mg_join->priv->target1, MG_TARGET_TYPE, REFERENCE_BY_XML_ID, target_1_xml_id);

	mg_join->priv->target2 = MG_REF_BASE (mg_ref_base_new (conf));
	mg_ref_base_set_ref_name (mg_join->priv->target2, MG_TARGET_TYPE, REFERENCE_BY_XML_ID, target_2_xml_id);
	
	g_signal_connect (G_OBJECT (query), "nullified",
			  G_CALLBACK (nullified_object_cb), mg_join);

	/* if the references to the any target is lost then we want to nullify the join */
	g_signal_connect (G_OBJECT (mg_join->priv->target1), "ref_lost",
			  G_CALLBACK (target_ref_lost_cb), mg_join);
	g_signal_connect (G_OBJECT (mg_join->priv->target2), "ref_lost",
			  G_CALLBACK (target_ref_lost_cb), mg_join);

	return obj;
}

/**
 * mg_join_new_copy
 * @orig: a #MgJoin to make a copy of
 * 
 * Copy constructor
 *
 * Returns: a the new copy of @orig
 */
GObject *
mg_join_new_copy (MgJoin *orig)
{
	GObject   *obj;
	MgJoin *mg_join;
	MgConf *conf;

	g_return_val_if_fail (orig && IS_MG_JOIN (orig), NULL);

	conf = mg_base_get_conf (MG_BASE (orig));
	obj = g_object_new (MG_JOIN_TYPE, "conf", conf, NULL);
	mg_join = MG_JOIN (obj);
	mg_base_set_id (MG_BASE (mg_join), 0);

	mg_join->priv->query = orig->priv->query;
	g_signal_connect (G_OBJECT (orig->priv->query), "nullified",
			  G_CALLBACK (nullified_object_cb), mg_join);

	mg_join->priv->target1 = MG_REF_BASE (mg_ref_base_new_copy (orig->priv->target1));
	mg_join->priv->target2 = MG_REF_BASE (mg_ref_base_new_copy (orig->priv->target2));
	mg_join->priv->join_type = orig->priv->join_type;

	/* if the references to the any target is lost then we want to nullify the join */
	g_signal_connect (G_OBJECT (mg_join->priv->target1), "ref_lost",
			  G_CALLBACK (target_ref_lost_cb), mg_join);
	g_signal_connect (G_OBJECT (mg_join->priv->target2), "ref_lost",
			  G_CALLBACK (target_ref_lost_cb), mg_join);

	return obj;	
}

static void
nullified_object_cb (GObject *obj, MgJoin *join)
{
	mg_base_nullify (MG_BASE (join));
}

static void
target_ref_lost_cb (MgRefBase *ref, MgJoin *join)
{
	mg_base_nullify (MG_BASE (join));
}

static void
mg_join_dispose (GObject *object)
{
	MgJoin *mg_join;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_JOIN (object));

	mg_join = MG_JOIN (object);
	if (mg_join->priv) {
		mg_base_nullify_check (MG_BASE (object));
		
		if (mg_join->priv->query) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (mg_join->priv->query),
							      G_CALLBACK (nullified_object_cb), mg_join);
			mg_join->priv->query = NULL;
		}
		if (mg_join->priv->target1) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (mg_join->priv->target1),
							      G_CALLBACK (target_ref_lost_cb), mg_join);
			g_object_unref (G_OBJECT (mg_join->priv->target1));
			mg_join->priv->target1 = NULL;
		}
		if (mg_join->priv->target2) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (mg_join->priv->target2),
							      G_CALLBACK (target_ref_lost_cb), mg_join);
			g_object_unref (G_OBJECT (mg_join->priv->target2));
			mg_join->priv->target2 = NULL;
		}
		if (mg_join->priv->cond) {
			g_object_unref (mg_join->priv->cond);
			mg_join->priv->cond = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
mg_join_finalize (GObject   * object)
{
	MgJoin *mg_join;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_JOIN (object));

	mg_join = MG_JOIN (object);
	if (mg_join->priv) {
		g_free (mg_join->priv);
		mg_join->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_join_set_property (GObject              *object,
			guint                 param_id,
			const GValue         *value,
			GParamSpec           *pspec)
{
	gpointer ptr;
	MgJoin *mg_join;

	mg_join = MG_JOIN (object);
	if (mg_join->priv) {
		switch (param_id) {
		case PROP:
			ptr = g_value_get_pointer (value);
			break;
		}
	}
}

static void
mg_join_get_property (GObject              *object,
			  guint                 param_id,
			  GValue               *value,
			  GParamSpec           *pspec)
{
	MgJoin *mg_join;
	mg_join = MG_JOIN (object);
	
	if (mg_join->priv) {
		switch (param_id) {
		case PROP:
			break;
		}	
	}
}

/**
 * mg_join_set_join_type
 * @join: a #MgJoin object
 * @type: the new type of join
 *
 * Sets the type of @join
 */
void
mg_join_set_join_type (MgJoin *join, MgJoinType type)
{
	g_return_if_fail (join && IS_MG_JOIN (join));
	g_return_if_fail (join->priv);
	
	if (join->priv->join_type != type) {
		join->priv->join_type = type;
		mg_base_changed (MG_BASE (join));
	}
}

/**
 * mg_join_get_join_type
 * @join: a #MgJoin object
 *
 * Get the type of a join
 *
 * Returns: the type of @join
 */
MgJoinType
mg_join_get_join_type (MgJoin *join)
{
	g_return_val_if_fail (join && IS_MG_JOIN (join), MG_JOIN_TYPE_CROSS);
	g_return_val_if_fail (join->priv, MG_JOIN_TYPE_CROSS);
	
	return join->priv->join_type;
}


/**
 * mg_join_get_query
 * @join: a #MgJoin object
 * 
 * Get the #MgQuery to which @join is attached to
 *
 * Returns: the #MgQuery
 */
MgQuery *
mg_join_get_query (MgJoin *join)
{
	g_return_val_if_fail (join && IS_MG_JOIN (join), NULL);
	g_return_val_if_fail (join->priv, NULL);
	
	return join->priv->query;
}

/**
 * mg_join_get_target_1
 * @join: a #MgJoin object
 *
 * Get the 1st #MgTarget participating in the join
 *
 * Returns: the #MgTarget
 */
MgTarget *
mg_join_get_target_1 (MgJoin *join)
{
	MgBase *base;

	g_return_val_if_fail (join && IS_MG_JOIN (join), NULL);
	g_return_val_if_fail (join->priv, NULL);
	
	base = mg_ref_base_get_ref_object (join->priv->target1);
	if (base)
		return MG_TARGET (base);
	else
		return NULL;
}

/**
 * mg_join_get_target_2
 * @join: a #MgJoin object
 *
 * Get the 2nd #MgTarget participating in the join
 *
 * Returns: the #MgTarget
 */
MgTarget *
mg_join_get_target_2 (MgJoin *join)
{
	MgBase *base;

	g_return_val_if_fail (join && IS_MG_JOIN (join), NULL);
	g_return_val_if_fail (join->priv, NULL);
	
	base = mg_ref_base_get_ref_object (join->priv->target2);
	if (base)
		return MG_TARGET (base);
	else
		return NULL;
}


/**
 * mg_join_swap_targets
 * @join: a #MgJoin object
 *
 * Changes the relative roles of the two #MgTarget objects. It does not
 * change the join condition itself, and is usefull only for the internals
 * of the #MgQuery object
 */
void
mg_join_swap_targets (MgJoin *join)
{
	MgRefBase *ref;
	g_return_if_fail (join && IS_MG_JOIN (join));
	g_return_if_fail (join->priv);

	ref = join->priv->target1;
	join->priv->target1 = join->priv->target2;
	join->priv->target2 = ref;

	switch (join->priv->join_type) {
	case MG_JOIN_TYPE_LEFT_OUTER:
		join->priv->join_type = MG_JOIN_TYPE_RIGHT_OUTER;
		break;
	case MG_JOIN_TYPE_RIGHT_OUTER:
		join->priv->join_type = MG_JOIN_TYPE_LEFT_OUTER;
		break;
	default:
		break;
	}
}

/**
 * mg_join_get_condition
 * @join: a #MgJoin object
 *
 * Get the join's associated condition
 *
 * Returns: the #MgCondition object
 */
MgCondition *
mg_join_get_condition (MgJoin *join)
{
	g_return_val_if_fail (join && IS_MG_JOIN (join), NULL);
	g_return_val_if_fail (join->priv, NULL);
	
	return join->priv->cond;
}


/**
 * mg_join_render_type
 * @join: a #MgJoin object
 *
 * Get the SQL version of the join type ("INNER JOIN", "LEFT JOIN", etc)
 *
 * Returns: the type as a const string
 */
const gchar *
mg_join_render_type (MgJoin *join)
{
	g_return_val_if_fail (join && IS_MG_JOIN (join), NULL);
	g_return_val_if_fail (join->priv, NULL);

	switch (join->priv->join_type) {
	case MG_JOIN_TYPE_INNER:
		return "INNER JOIN";
	case MG_JOIN_TYPE_LEFT_OUTER:
		return "LEFT JOIN";
	case MG_JOIN_TYPE_RIGHT_OUTER:
		return "RIGHT JOIN";
	case MG_JOIN_TYPE_FULL_OUTER:
		return "FULL JOIN";
        case MG_JOIN_TYPE_CROSS:
		return "CROSS JOIN";
	default:
		g_assert_not_reached ();
		return NULL;
	}
}

#ifdef debug
static void
mg_join_dump (MgJoin *join, guint offset)
{
	gchar *str;
        guint i;
	
	g_return_if_fail (join && IS_MG_JOIN (join));

        /* string for the offset */
        str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;

        /* dump */
        if (join->priv) {
                g_print ("%s" D_COL_H1 "MgJoin" D_COL_NOR " %p ", str, join);
		if (mg_join_is_active (MG_REFERER (join)))
			g_print ("Active, Targets: %p -> %p ", mg_ref_base_get_ref_object (join->priv->target1),
				 mg_ref_base_get_ref_object (join->priv->target2));
		else
			g_print (D_COL_ERR "Non active" D_COL_NOR ", ");
		g_print ("requested targets names: %s & %s\n", mg_ref_base_get_ref_name (join->priv->target1, NULL, NULL),
			 mg_ref_base_get_ref_name (join->priv->target2, NULL, NULL));
	}
        else
                g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, join);
}
#endif



/* 
 * MgReferer interface implementation
 */
static gboolean
mg_join_activate (MgReferer *iface)
{
	gboolean retval;
	g_return_val_if_fail (iface && IS_MG_JOIN (iface), FALSE);
	g_return_val_if_fail (MG_JOIN (iface)->priv, FALSE);

	retval = mg_ref_base_activate (MG_JOIN (iface)->priv->target1);
	retval = mg_ref_base_activate (MG_JOIN (iface)->priv->target2) && retval;

	return retval;
}

static void
mg_join_deactivate (MgReferer *iface)
{
	g_return_if_fail (iface && IS_MG_JOIN (iface));
	g_return_if_fail (MG_JOIN (iface)->priv);

	mg_ref_base_deactivate (MG_JOIN (iface)->priv->target1);
	mg_ref_base_deactivate (MG_JOIN (iface)->priv->target2);
}

static gboolean
mg_join_is_active (MgReferer *iface)
{
	gboolean retval;

	g_return_val_if_fail (iface && IS_MG_JOIN (iface), FALSE);
	g_return_val_if_fail (MG_JOIN (iface)->priv, FALSE);

	retval = mg_ref_base_is_active (MG_JOIN (iface)->priv->target1);
	retval = retval && mg_ref_base_is_active (MG_JOIN (iface)->priv->target2);
	
	return retval;
}

static GSList *
mg_join_get_ref_objects (MgReferer *iface)
{
	GSList *list = NULL;
	MgBase *base;
	g_return_val_if_fail (iface && IS_MG_JOIN (iface), NULL);
	g_return_val_if_fail (MG_JOIN (iface)->priv, NULL);

	base = mg_ref_base_get_ref_object (MG_JOIN (iface)->priv->target1);
	if (base)
		list = g_slist_append (list, base);
	base = mg_ref_base_get_ref_object (MG_JOIN (iface)->priv->target2);
	if (base)
		list = g_slist_append (list, base);

	return list;
}

static void
mg_join_replace_refs (MgReferer *iface, GHashTable *replacements)
{
	MgJoin *join;
	g_return_if_fail (iface && IS_MG_JOIN (iface));
	g_return_if_fail (MG_JOIN (iface)->priv);

	join = MG_JOIN (iface);
	if (join->priv->query) {
		MgQuery *query = g_hash_table_lookup (replacements, join->priv->query);
		if (query) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (join->priv->query),
							      G_CALLBACK (nullified_object_cb), join);
			join->priv->query = query;
			g_signal_connect (G_OBJECT (query), "nullified",
					  G_CALLBACK (nullified_object_cb), join);
		}
	}

	mg_ref_base_replace_ref_object (join->priv->target1, replacements);
	mg_ref_base_replace_ref_object (join->priv->target2, replacements);
}

/* 
 * MgXmlStorage interface implementation
 */

static const gchar *convert_join_type_to_str (MgJoinType type);
static MgJoinType   convert_str_to_join_type (const gchar *str);

static gchar *
mg_join_get_xml_id (MgXmlStorage *iface)
{
	g_return_val_if_fail (iface && IS_MG_JOIN (iface), NULL);
	g_return_val_if_fail (MG_JOIN (iface)->priv, NULL);

	return NULL;
}

static xmlNodePtr
mg_join_save_to_xml (MgXmlStorage *iface, GError **error)
{
	xmlNodePtr node = NULL;
	MgJoin *join;
	gchar *str;
	const gchar *type;

	g_return_val_if_fail (iface && IS_MG_JOIN (iface), NULL);
	g_return_val_if_fail (MG_JOIN (iface)->priv, NULL);

	join = MG_JOIN (iface);

	node = xmlNewNode (NULL, "MG_JOIN");
	
	/* targets */
	if (join->priv->target1) {
		str = NULL;
		if (mg_ref_base_is_active (join->priv->target1)) {
			MgBase *base = mg_ref_base_get_ref_object (join->priv->target1);
			g_assert (base);
			str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (base));
		}
		else 
			str = g_strdup (mg_ref_base_get_ref_name (join->priv->target1, NULL, NULL));
		
		if (str) {
			xmlSetProp (node, "target1", str);
			g_free (str);
		}
	}

	if (join->priv->target2) {
		str = NULL;
		if (mg_ref_base_is_active (join->priv->target2)) {
			MgBase *base = mg_ref_base_get_ref_object (join->priv->target2);
			g_assert (base);
			str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (base));
		}
		else 
			str = g_strdup (mg_ref_base_get_ref_name (join->priv->target2, NULL, NULL));
		
		if (str) {
			xmlSetProp (node, "target2", str);
			g_free (str);
		}
	}

	/* join type */
	type = convert_join_type_to_str (join->priv->join_type);
	xmlSetProp (node, "join_type", type);

	return node;
}


static gboolean
mg_join_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgJoin *join;
	gchar *prop;
	gboolean t1 = FALSE, t2 = FALSE;

	g_return_val_if_fail (iface && IS_MG_JOIN (iface), FALSE);
	g_return_val_if_fail (MG_JOIN (iface)->priv, FALSE);
	g_return_val_if_fail (node, FALSE);

	join = MG_JOIN (iface);
	if (strcmp (node->name, "MG_JOIN")) {
		g_set_error (error,
			     MG_JOIN_ERROR,
			     MG_JOIN_XML_LOAD_ERROR,
			     _("XML Tag is not <MG_JOIN>"));
		return FALSE;
	}

	prop = xmlGetProp (node, "target1");
	if (prop) {
		if (join->priv->target1) {
			mg_ref_base_set_ref_name (join->priv->target1, MG_TARGET_TYPE, 
						  REFERENCE_BY_XML_ID, prop);
			t1 = TRUE;
		}
		g_free (prop);
	}
	prop = xmlGetProp (node, "target2");
	if (prop) {
		if (join->priv->target2) {
			mg_ref_base_set_ref_name (join->priv->target2, MG_TARGET_TYPE, 
						  REFERENCE_BY_XML_ID, prop);
			t2 = TRUE;
		}
		g_free (prop);
	}
	prop = xmlGetProp (node, "join_type");
	if (prop) {
		join->priv->join_type = convert_str_to_join_type (prop);
		g_free (prop);
	}


	if (t1 && t2)
		return TRUE;
	else {
		g_set_error (error,
			     MG_JOIN_ERROR,
			     MG_JOIN_XML_LOAD_ERROR,
			     _("Problem loading <MG_JOIN>"));
		return FALSE;
	}
}

static const gchar *
convert_join_type_to_str (MgJoinType type)
{
	switch (type) {
	default:
	case MG_JOIN_TYPE_INNER:
		return "INNER";
	case MG_JOIN_TYPE_LEFT_OUTER:
		return "LEFT";
	case MG_JOIN_TYPE_RIGHT_OUTER:
		return "RIGHT";
	case MG_JOIN_TYPE_FULL_OUTER:
		return "FULL";
        case MG_JOIN_TYPE_CROSS:
		return "CROSS";
	}
}

static MgJoinType
convert_str_to_join_type (const gchar *str)
{
	switch (*str) {
	case 'I':
	default:
		return MG_JOIN_TYPE_INNER;
	case 'L':
		return MG_JOIN_TYPE_LEFT_OUTER;
	case 'R':
		return MG_JOIN_TYPE_RIGHT_OUTER;
	case 'F':
		return MG_JOIN_TYPE_FULL_OUTER;
	case 'C':
		return MG_JOIN_TYPE_CROSS;
	}
}
