/* mg-work-core.h
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __MG_WORK_CORE__
#define __MG_WORK_CORE__

#include "mg-base.h"
#include "mg-defs.h"

G_BEGIN_DECLS

#define MG_WORK_CORE_TYPE          (mg_work_core_get_type())
#define MG_WORK_CORE(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_work_core_get_type(), MgWorkCore)
#define MG_WORK_CORE_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_work_core_get_type (), MgWorkCoreClass)
#define IS_MG_WORK_CORE(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_work_core_get_type ())


typedef struct _MgWorkCore      MgWorkCore;
typedef struct _MgWorkCoreClass MgWorkCoreClass;
typedef struct _MgWorkCorePriv  MgWorkCorePriv;
typedef struct _MgWorkCoreNode  MgWorkCoreNode;


/* struct for the object's data */
struct _MgWorkCore
{
	MgBase              object;

	MgContext         *args_context;   /* for the parameters required to run the SELECT query */
        gboolean           args_context_changed_connected;
        MgContext         *work_context;   /* for the parameters required to run the modif queries */
        MgTarget          *modif_target;   /* target which gets the modifications, or NULL */

        MgQuery           *query_select;   /* All the queries are owned by the object */
        MgQuery           *query_select_improved;
        MgQuery           *query_update;
        MgQuery           *query_delete;
        MgQuery           *query_insert;

        GHashTable        *query_select_repl; /* created in _new() when the argument query is copied */

        GSList            *nodes;             /* list of WorkCoreNode structures */
};

struct _MgWorkCoreNode
{
        MgParameter *param;    /* each param is also present in the 'work_context' and is only referenced there */
        gint         position; /* field position in query_select (and in the 'data' MgResultSet) */
};
#define MG_WORK_CORE_NODE(x) ((MgWorkCoreNode *)x)


/* struct for the object's class */
struct _MgWorkCoreClass
{
	MgBaseClass         parent_class;
};

/* 
 * Generic object's methods 
 */
guint        mg_work_core_get_type            (void);
GObject     *mg_work_core_new                 (MgQuery *query, MgTarget *modified);

G_END_DECLS

#endif



