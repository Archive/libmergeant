/* mg-condition.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <string.h>
#include "mg-condition.h"
#include "mg-query.h"
#include "mg-join.h"
#include "mg-xml-storage.h"
#include "mg-renderer.h"
#include "mg-referer.h"
#include "mg-ref-base.h"
#include "mg-qfield.h"
#include "mg-qf-value.h"

/* 
 * Main static functions 
 */
static void mg_condition_class_init (MgConditionClass *class);
static void mg_condition_init (MgCondition *condition);
static void mg_condition_dispose (GObject *object);
static void mg_condition_finalize (GObject *object);

static void mg_condition_set_property (GObject              *object,
				       guint                 param_id,
				       const GValue         *value,
				       GParamSpec           *pspec);
static void mg_condition_get_property (GObject              *object,
				       guint                 param_id,
				       GValue               *value,
				       GParamSpec           *pspec);

static gboolean condition_type_is_node (MgConditionType type);

static void nullified_object_cb (GObject *obj, MgCondition *cond);
static void nullified_parent_cb (MgCondition *parent, MgCondition *cond);
static void nullified_child_cb (MgCondition *child, MgCondition *cond);

static void child_cond_changed_cb (MgCondition *child, MgCondition *cond);


/* XML storage interface */
static void        mg_condition_xml_storage_init (MgXmlStorageIface *iface);
static gchar      *mg_condition_get_xml_id (MgXmlStorage *iface);
static xmlNodePtr  mg_condition_save_to_xml (MgXmlStorage *iface, GError **error);
static gboolean    mg_condition_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error);

/* Renderer interface */
static void            mg_condition_renderer_init      (MgRendererIface *iface);
static GdaXqlItem     *mg_condition_render_as_xql   (MgRenderer *iface, MgContext *context, GError **error);
static gchar          *mg_condition_render_as_sql   (MgRenderer *iface, MgContext *context, GError **error);
static gchar          *mg_condition_render_as_str   (MgRenderer *iface, MgContext *context);

/* Referer interface */
static void        mg_condition_referer_init        (MgRefererIface *iface);
static gboolean    mg_condition_activate            (MgReferer *iface);
static void        mg_condition_deactivate          (MgReferer *iface);
static gboolean    mg_condition_is_active           (MgReferer *iface);
static GSList     *mg_condition_get_ref_objects     (MgReferer *iface);
static void        mg_condition_replace_refs        (MgReferer *iface, GHashTable *replacements);

#ifdef debug
static void        mg_condition_dump                (MgCondition *condition, guint offset);
#endif

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	LAST_SIGNAL
};

static gint mg_condition_signals[LAST_SIGNAL] = { };

/* properties */
enum
{
	PROP_0,
	PROP_QUERY,
	PROP_JOIN
};


struct _MgConditionPrivate
{
	MgQuery         *query;
	MgJoin          *join; /* not NULL if cond is a join cond => more specific tests */
	MgConditionType  type;
	MgCondition     *cond_parent;
	GSList          *cond_children;
	MgRefBase       *ops[3]; /* references on MgQfield objects */
};

/* module error */
GQuark mg_condition_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("mg_condition_error");
	return quark;
}


guint
mg_condition_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgConditionClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_condition_class_init,
			NULL,
			NULL,
			sizeof (MgCondition),
			0,
			(GInstanceInitFunc) mg_condition_init
		};
		
		static const GInterfaceInfo xml_storage_info = {
                        (GInterfaceInitFunc) mg_condition_xml_storage_init,
                        NULL,
                        NULL
                };

                static const GInterfaceInfo renderer_info = {
                        (GInterfaceInitFunc) mg_condition_renderer_init,
                        NULL,
                        NULL
                };

                static const GInterfaceInfo referer_info = {
                        (GInterfaceInitFunc) mg_condition_referer_init,
                        NULL,
                        NULL
                };

		type = g_type_register_static (MG_BASE_TYPE, "MgCondition", &info, 0);
		g_type_add_interface_static (type, MG_XML_STORAGE_TYPE, &xml_storage_info);
                g_type_add_interface_static (type, MG_RENDERER_TYPE, &renderer_info);
                g_type_add_interface_static (type, MG_REFERER_TYPE, &referer_info);
	}
	return type;
}

static void
mg_condition_xml_storage_init (MgXmlStorageIface *iface)
{
        iface->get_xml_id = mg_condition_get_xml_id;
        iface->save_to_xml = mg_condition_save_to_xml;
        iface->load_from_xml = mg_condition_load_from_xml;
}

static void
mg_condition_renderer_init (MgRendererIface *iface)
{
        iface->render_as_xql = mg_condition_render_as_xql;
        iface->render_as_sql = mg_condition_render_as_sql;
        iface->render_as_str = mg_condition_render_as_str;
}

static void
mg_condition_referer_init (MgRefererIface *iface)
{
        iface->activate = mg_condition_activate;
        iface->deactivate = mg_condition_deactivate;
        iface->is_active = mg_condition_is_active;
        iface->get_ref_objects = mg_condition_get_ref_objects;
        iface->replace_refs = mg_condition_replace_refs;
}

static void
mg_condition_class_init (MgConditionClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = mg_condition_dispose;
	object_class->finalize = mg_condition_finalize;

	/* Properties */
	object_class->set_property = mg_condition_set_property;
	object_class->get_property = mg_condition_get_property;
	g_object_class_install_property (object_class, PROP_QUERY,
					 g_param_spec_pointer ("query", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property (object_class, PROP_JOIN,
					 g_param_spec_pointer ("join", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	/* virtual functions */
#ifdef debug
        MG_BASE_CLASS (class)->dump = (void (*)(MgBase *, guint)) mg_condition_dump;
#endif
}

static void
mg_condition_init (MgCondition *condition)
{
	gint i;
	condition->priv = g_new0 (MgConditionPrivate, 1);
	condition->priv->query = NULL;
	condition->priv->join = NULL;
	condition->priv->type = MG_CONDITION_TYPE_UNKNOWN;
	condition->priv->cond_parent = NULL;
	condition->priv->cond_children = NULL;
	for (i=0; i<3; i++)
		condition->priv->ops[i] = NULL;
}

/**
 * mg_condition_new
 * @query: a #MgQuery object
 * @type: the condition type
 *
 * Creates a new #MgCondition object
 *
 * Returns: the newly created object
 */
GObject   *
mg_condition_new (MgQuery *query, MgConditionType type)
{
	GObject *obj = NULL;
	MgConf *conf;
	gint i;
	MgCondition *cond;
	guint id;

	g_return_val_if_fail (query && IS_MG_QUERY (query), NULL);
	conf = mg_base_get_conf (MG_BASE (query));

	obj = g_object_new (MG_CONDITION_TYPE, "conf", conf, NULL);
	g_object_get (G_OBJECT (query), "cond_serial", &id, NULL);
	mg_base_set_id (MG_BASE (obj), id);

	cond = MG_CONDITION (obj);
	for (i=0; i<3; i++)
		cond->priv->ops[i] = MG_REF_BASE (mg_ref_base_new (conf));
	cond->priv->type = type;

	g_object_set (G_OBJECT (cond), "query", query, NULL);

	return obj;
}


/**
 * mg_condition_new_copy
 * @orig: a #MgCondition to copy
 * @replacements: a hash table to store replacements, or %NULL
 *
 * This is a copy constructor
 *
 * Returns: the new object
 */
GObject *
mg_condition_new_copy (MgCondition *orig, GHashTable *replacements)
{
	GObject *obj;
	MgQuery *query;
	MgCondition *newcond;
	MgConf *conf;
	GSList *list;
	gint i;

	g_return_val_if_fail (orig && IS_MG_CONDITION (orig), NULL);
	g_return_val_if_fail (orig->priv, NULL);
	g_object_get (G_OBJECT (orig), "query", &query, NULL);
	g_return_val_if_fail (query, NULL);
	conf = mg_base_get_conf (MG_BASE (query));

	obj = g_object_new (MG_CONDITION_TYPE, "conf", conf, NULL);
	newcond = MG_CONDITION (obj);
	if (replacements)
		g_hash_table_insert (replacements, orig, newcond);

	for (i=0; i<3; i++)
		newcond->priv->ops[i] = MG_REF_BASE (mg_ref_base_new_copy (orig->priv->ops[i]));
	newcond->priv->type = orig->priv->type;

	g_object_set (G_OBJECT (newcond), "query", query, NULL);

	list = orig->priv->cond_children;
	while (list) {
		GObject *ccond;

		ccond = mg_condition_new_copy (MG_CONDITION (list->data), replacements);
		mg_condition_node_add_child (newcond, MG_CONDITION (ccond), NULL);
		g_object_unref (ccond);
		list = g_slist_next (list);
	}

	return obj;
}

static void
mg_condition_dispose (GObject *object)
{
	MgCondition *condition;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_CONDITION (object));

	condition = MG_CONDITION (object);
	if (condition->priv) {
		gint i;

		if (condition->priv->cond_parent) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (condition->priv->cond_parent),
							      G_CALLBACK (nullified_parent_cb), condition);
			condition->priv->cond_parent = NULL;
		}


		if (condition->priv->query) {
                        g_signal_handlers_disconnect_by_func (G_OBJECT (condition->priv->query),
                                                              G_CALLBACK (nullified_object_cb), condition);
                        condition->priv->query = NULL;
                }


		if (condition->priv->join) {
                        g_signal_handlers_disconnect_by_func (G_OBJECT (condition->priv->join),
                                                              G_CALLBACK (nullified_object_cb), condition);
                        condition->priv->join = NULL;
                }

		for (i=0; i<3; i++)
			if (condition->priv->ops[i]) {
				g_object_unref (condition->priv->ops[i]);
				condition->priv->ops[i] = NULL;
			}

		while (condition->priv->cond_children)
			nullified_child_cb (MG_CONDITION (condition->priv->cond_children->data), condition);
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
nullified_object_cb (GObject *obj, MgCondition *cond)
{
	mg_base_nullify (MG_BASE (cond));
}

static void
nullified_parent_cb (MgCondition *parent, MgCondition *cond)
{
	g_assert (cond->priv->cond_parent == parent);
	g_signal_handlers_disconnect_by_func (G_OBJECT (parent),
					      G_CALLBACK (nullified_parent_cb) , cond);
	cond->priv->cond_parent = NULL;
	mg_base_nullify (MG_BASE (cond));
}

static void
nullified_child_cb (MgCondition *child, MgCondition *cond)
{
	g_assert (g_slist_find (cond->priv->cond_children, child));
	g_signal_handlers_disconnect_by_func (G_OBJECT (child),
					      G_CALLBACK (nullified_child_cb) , cond);
	g_signal_handlers_disconnect_by_func (G_OBJECT (child),
					      G_CALLBACK (child_cond_changed_cb) , cond);
	g_object_unref (G_OBJECT (child));
	cond->priv->cond_children = g_slist_remove (cond->priv->cond_children, child);
	mg_base_changed (MG_BASE (cond));
}


static void
mg_condition_finalize (GObject   * object)
{
	MgCondition *condition;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_MG_CONDITION (object));

	condition = MG_CONDITION (object);
	if (condition->priv) {
		g_free (condition->priv);
		condition->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
mg_condition_set_property (GObject              *object,
			   guint                 param_id,
			   const GValue         *value,
			   GParamSpec           *pspec)
{
	MgCondition *condition;
	gpointer ptr;

	condition = MG_CONDITION (object);
	if (condition->priv) {
		switch (param_id) {
		case PROP_QUERY:
			ptr = g_value_get_pointer (value);
                        g_return_if_fail (ptr && IS_MG_QUERY (ptr));

                        if (condition->priv->query) {
                                if (condition->priv->query == MG_QUERY (ptr))
                                        return;

                                g_signal_handlers_disconnect_by_func (G_OBJECT (condition->priv->query),
                                                                      G_CALLBACK (nullified_object_cb), 
								      condition);
                        }

                        condition->priv->query = MG_QUERY (ptr);
                        g_signal_connect (G_OBJECT (ptr), "nullified",
                                          G_CALLBACK (nullified_object_cb), condition);
			break;
		case PROP_JOIN:
			ptr = g_value_get_pointer (value);
                        g_return_if_fail (ptr && IS_MG_JOIN (ptr));
			g_return_if_fail (mg_join_get_query (MG_JOIN (ptr)) == condition->priv->query);

                        if (condition->priv->join) {
                                if (condition->priv->join == MG_JOIN (ptr))
                                        return;

                                g_signal_handlers_disconnect_by_func (G_OBJECT (condition->priv->join),
                                                                      G_CALLBACK (nullified_object_cb), 
								      condition);
                        }

                        condition->priv->join = MG_JOIN (ptr);
                        g_signal_connect (G_OBJECT (ptr), "nullified",
                                          G_CALLBACK (nullified_object_cb), condition);
			break;
		}
	}
}

static void
mg_condition_get_property (GObject              *object,
			   guint                 param_id,
			   GValue               *value,
			   GParamSpec           *pspec)
{
	MgCondition *condition;

	condition = MG_CONDITION (object);
        if (condition->priv) {
                switch (param_id) {
                case PROP_QUERY:
			g_value_set_pointer (value, condition->priv->query);
                        break;
		case PROP_JOIN:
			g_value_set_pointer (value, condition->priv->join);
                        break;
                }
        }
}

static gboolean
condition_type_is_node (MgConditionType type)
{
	gboolean retval;

	switch (type) {
	case MG_CONDITION_NODE_AND:
	case MG_CONDITION_NODE_OR:
	case MG_CONDITION_NODE_NOT:
		retval = TRUE;
		break;
	default:
		retval = FALSE;
		break;
	}
	return retval;
}

/**
 * mg_condition_set_cond_type
 * @condition: a #MgCondition object
 * @type:
 *
 * Sets the kind of condition @condition represents. If @type implies a node condition and
 * @condition currently represents a leaf, or if @type implies a leaf condition and
 * @condition currently represents a node, then @condition is changed without any error.
 */
void
mg_condition_set_cond_type (MgCondition *condition, MgConditionType type)
{
	g_return_if_fail (condition && IS_MG_CONDITION (condition));
	g_return_if_fail (condition->priv);
	if (condition->priv->type == type)
		return;

	if (condition_type_is_node (condition->priv->type) != condition_type_is_node (type)) {
		/* FIXME: remove all the children and all the operators */
		TO_IMPLEMENT;
	}

	condition->priv->type = type;
	mg_base_changed (MG_BASE (condition));
}

/**
 * mg_condition_get_cond_type
 * @condition: a #MgCondition object
 *
 * Get the type of @condition
 *
 * Returns: the type
 */
MgConditionType
mg_condition_get_cond_type (MgCondition *condition)
{
	g_return_val_if_fail (condition && IS_MG_CONDITION (condition), MG_CONDITION_TYPE_UNKNOWN);
	g_return_val_if_fail (condition->priv, MG_CONDITION_TYPE_UNKNOWN);

	return condition->priv->type;
}

/**
 * mg_condition_get_children
 * @condition: a #MgCondition object
 *
 * Get a list of #MgCondition objects which are children of @condition
 *
 * Returns: a new list of #MgCondition objects
 */
GSList *
mg_condition_get_children (MgCondition *condition)
{
	g_return_val_if_fail (condition && IS_MG_CONDITION (condition), NULL);
	g_return_val_if_fail (condition->priv, NULL);

	if (condition->priv->cond_children)
		return g_slist_copy (condition->priv->cond_children);
	else
		return NULL;
}

/**
 * mg_condition_get_parent
 * @condition: a #MgCondition object
 *
 * Get the #MgCondition object which is parent of @condition
 *
 * Returns: the parent object, or %NULL
 */
MgCondition *
mg_condition_get_parent (MgCondition *condition)
{
	g_return_val_if_fail (condition && IS_MG_CONDITION (condition), NULL);
	g_return_val_if_fail (condition->priv, NULL);

	return condition->priv->cond_parent;
}


/**
 * mg_condition_get_child_by_xml_id
 * @condition: a #MgCondition object
 * @xml_id: the XML Id of the requested #MgCondition child
 *
 * Get a pointer to a #MgCondition child from its XML Id
 *
 * Returns: the #MgCondition object, or %NULL if not found
 */
MgCondition *
mg_condition_get_child_by_xml_id (MgCondition *condition, const gchar *xml_id)
{
	TO_IMPLEMENT;
	return NULL;
}

/**
 * mg_condition_is_ancestor
 * @condition: a #MgCondition object
 * @ancestor: a #MgCondition object
 *
 * Tests if @ancestor is an ancestor of @condition
 *
 * Returns: TRUE if @ancestor is an ancestor of @condition
 */
gboolean
mg_condition_is_ancestor (MgCondition *condition, MgCondition *ancestor)
{
	g_return_val_if_fail (condition && IS_MG_CONDITION (condition), FALSE);
	g_return_val_if_fail (condition->priv, FALSE);
	g_return_val_if_fail (ancestor && IS_MG_CONDITION (ancestor), FALSE);
	g_return_val_if_fail (ancestor->priv, FALSE);
	
	if (condition->priv->cond_parent == ancestor)
		return TRUE;
	if (condition->priv->cond_parent)
		return mg_condition_is_ancestor (condition->priv->cond_parent, ancestor);

	return FALSE;
}

/**
 * mg_condition_is_leaf
 * @condition: a #MgCondition object
 *
 * Tells if @condition is a leaf condition (not AND, OR, NOT, etc)
 *
 * Returns: TRUE if @condition is a leaf condition
 */
gboolean
mg_condition_is_leaf (MgCondition *condition)
{
	g_return_val_if_fail (condition && IS_MG_CONDITION (condition), FALSE);
	g_return_val_if_fail (condition->priv, FALSE);

	switch (condition->priv->type) {
	case MG_CONDITION_NODE_AND:
	case MG_CONDITION_NODE_OR:
	case MG_CONDITION_NODE_NOT:
		return FALSE;
	default:
		return TRUE;
	}
}

/**
 * mg_condition_node_add_child
 * @condition: a #MgCondition object
 * @child: a #MgCondition object
 * @error: location to store error, or %NULL
 *
 * Adds a child to @condition; this is possible only if @condition is a node type (AND, OR, etc)
 *
 * Returns: TRUE if no error occured
 */
gboolean
mg_condition_node_add_child (MgCondition *condition, MgCondition *child, GError **error)
{
	g_return_val_if_fail (condition && IS_MG_CONDITION (condition), FALSE);
	g_return_val_if_fail (condition->priv, FALSE);
	g_return_val_if_fail (child && IS_MG_CONDITION (child), FALSE);
	g_return_val_if_fail (child->priv, FALSE);
	g_return_val_if_fail (!mg_condition_is_leaf (condition), FALSE);

	if (child->priv->cond_parent == condition)
		return TRUE;

	g_object_ref (G_OBJECT (child));

	if (child->priv->cond_parent) 
		mg_condition_node_del_child (child->priv->cond_parent, child);

	if (mg_condition_is_ancestor (condition, child)) {
		g_set_error (error,
                             MG_CONDITION_ERROR,
                             MG_CONDITION_PARENT_ERROR,
			     _("Conditions hierarchy error"));
		return FALSE;
	}

	/* child part */
	child->priv->cond_parent = condition;
	g_signal_connect (G_OBJECT (condition), "nullified",
			  G_CALLBACK (nullified_parent_cb), child);

	/* parent part */
	condition->priv->cond_children = g_slist_append (condition->priv->cond_children, child);
	g_signal_connect (G_OBJECT (child), "nullified",
			  G_CALLBACK (nullified_child_cb), condition);
	g_signal_connect (G_OBJECT (child), "changed",
			  G_CALLBACK (child_cond_changed_cb), condition);
	mg_base_changed (MG_BASE (condition));
	return TRUE;
}

/* Forwards the "changed" signal from children to parent condition */
static void
child_cond_changed_cb (MgCondition *child, MgCondition *cond)
{
	mg_base_changed (MG_BASE (cond));
}

/**
 * mg_condition_node_del_child
 * @condition: a #MgCondition object
 * @child: a #MgCondition object
 *
 * Removes a child from @condition; this is possible only if @condition is a node type (AND, OR, etc)
 */
void
mg_condition_node_del_child (MgCondition *condition, MgCondition *child)
{
	g_return_if_fail (condition && IS_MG_CONDITION (condition));
	g_return_if_fail (condition->priv);
	g_return_if_fail (child && IS_MG_CONDITION (child));
	g_return_if_fail (child->priv);
	g_return_if_fail (child->priv->cond_parent != condition);
	g_return_if_fail (!mg_condition_is_leaf (condition));

	/* child part */
	g_signal_handlers_disconnect_by_func (G_OBJECT (condition),
					      G_CALLBACK (nullified_parent_cb) , condition);
	child->priv->cond_parent = NULL;

	/* parent part */
	nullified_child_cb (child, condition);
}

/**
 * mg_condition_leaf_set_left_op
 * @condition: a #MgCondition object
 * @op: which oparetor is concerned
 * @field: a # MgQfield object
 *
 * Sets one of @condition's operators
 */
void
mg_condition_leaf_set_operator (MgCondition *condition, MgConditionOperator op, MgQfield *field)
{
	MgQuery *query1, *query2;

	g_return_if_fail (condition && IS_MG_CONDITION (condition));
	g_return_if_fail (condition->priv);
	g_return_if_fail (field && IS_MG_QFIELD (field));
	g_return_if_fail (mg_condition_is_leaf (condition));

	g_object_get (G_OBJECT (condition), "query", &query1, NULL);
	g_object_get (G_OBJECT (field), "query", &query2, NULL);
	g_return_if_fail (query1);
	g_return_if_fail (query1 == query2);

	mg_ref_base_set_ref_object_type (condition->priv->ops[op], MG_BASE (field), MG_QFIELD_TYPE);
}

/**
 * mg_condition_leaf_get_operator
 * @condition: a #MgCondition object
 * @op: which oparetor is concerned
 *
 * Get one of @condition's operators.
 *
 * Returns: the requested #MgQfield object
 */
MgQfield *
mg_condition_leaf_get_operator  (MgCondition *condition, MgConditionOperator op)
{
	MgBase *base;
	g_return_val_if_fail (condition && IS_MG_CONDITION (condition), NULL);
	g_return_val_if_fail (condition->priv, NULL);
	g_return_val_if_fail (mg_condition_is_leaf (condition), NULL);

	mg_ref_base_activate (condition->priv->ops[op]);
	base = mg_ref_base_get_ref_object (condition->priv->ops[op]);
	if (base)
		return MG_QFIELD (base);
	else
		return NULL;
}



#ifdef debug
static void
mg_condition_dump (MgCondition *condition, guint offset)
{
	gchar *str;
	gint i;

	g_return_if_fail (condition && IS_MG_CONDITION (condition));
	
        /* string for the offset */
        str = g_new0 (gchar, offset+1);
        for (i=0; i<offset; i++)
                str[i] = ' ';
        str[offset] = 0;

        /* dump */
        if (condition->priv) {
		GSList *children;
                g_print ("%s" D_COL_H1 "MgCondition" D_COL_NOR " \"%s\" (%p, id=%d) ",
                         str, mg_base_get_name (MG_BASE (condition)), condition, mg_base_get_id (MG_BASE (condition)));
		g_print ("\n");
		children = condition->priv->cond_children;
		while (children) {
			mg_condition_dump (MG_CONDITION (children->data), offset+5);
			children = g_slist_next (children);
		}
	}
        else
                g_print ("%s" D_COL_ERR "Using finalized object %p" D_COL_NOR, str, condition);
}
#endif

/* 
 * MgXmlStorage interface implementation
 */

static const gchar *
condition_type_to_str (MgConditionType type)
{
	switch (type) {
	case MG_CONDITION_NODE_AND:
		return "AND";
	case MG_CONDITION_NODE_OR:
		return "OR";
        case MG_CONDITION_NODE_NOT:
		return "NOT";
        case MG_CONDITION_LEAF_EQUAL:
		return "EQ";
        case MG_CONDITION_LEAF_DIFF:
		return "NE";
        case MG_CONDITION_LEAF_SUP:
		return "SUP";
        case MG_CONDITION_LEAF_SUPEQUAL:
		return "ESUP";
        case MG_CONDITION_LEAF_INF:
		return "INF";
        case MG_CONDITION_LEAF_INFEQUAL:
		return "EINF";
        case MG_CONDITION_LEAF_LIKE:
		return "LIKE";
        case MG_CONDITION_LEAF_REGEX:
		return "REG";
        case MG_CONDITION_LEAF_IN:
		return "IN";
        case MG_CONDITION_LEAF_BETWEEN:
		return "BTW";
	default:
		return "???";
	}
}

static MgConditionType
condition_str_to_type (const gchar *type)
{
	switch (*type) {
	case 'A':
		return MG_CONDITION_NODE_AND;
	case 'O':
		return MG_CONDITION_NODE_OR;
	case 'N':
		if (!strcmp (type, "NOT"))
			return MG_CONDITION_NODE_NOT;
		else
			return MG_CONDITION_LEAF_DIFF;
	case 'E':
		if (!strcmp (type, "EQ"))
			return MG_CONDITION_LEAF_EQUAL;
		else if (strcmp (type, "ESUP"))
			return MG_CONDITION_LEAF_SUPEQUAL;
		else
			return MG_CONDITION_LEAF_INFEQUAL;
	case 'S':
		return MG_CONDITION_LEAF_SUP;

	case 'I':
		if (!strcmp (type, "INF"))
			return MG_CONDITION_LEAF_INF;
		else
			return MG_CONDITION_LEAF_IN;
	case 'L':
		return MG_CONDITION_LEAF_LIKE;
	case 'R':
		return MG_CONDITION_LEAF_REGEX;
	case 'B':
		return MG_CONDITION_LEAF_BETWEEN;
	default:
		return MG_CONDITION_TYPE_UNKNOWN;
	}
}

static gchar *
mg_condition_get_xml_id (MgXmlStorage *iface)
{
        gchar *q_xml_id, *xml_id;

        g_return_val_if_fail (iface && IS_MG_CONDITION (iface), NULL);
        g_return_val_if_fail (MG_CONDITION (iface)->priv, NULL);

        q_xml_id = mg_xml_storage_get_xml_id (MG_XML_STORAGE (MG_CONDITION (iface)->priv->query));
        xml_id = g_strdup_printf ("%s:C%d", q_xml_id, mg_base_get_id (MG_BASE (iface)));
        g_free (q_xml_id);

        return xml_id;
}


static xmlNodePtr
mg_condition_save_to_xml (MgXmlStorage *iface, GError **error)
{
        xmlNodePtr node = NULL;
	MgCondition *cond;
        gchar *str;
	MgBase *base;
	GSList *list;

        g_return_val_if_fail (iface && IS_MG_CONDITION (iface), NULL);
        g_return_val_if_fail (MG_CONDITION (iface)->priv, NULL);

        cond = MG_CONDITION (iface);

        node = xmlNewNode (NULL, "MG_COND");

        str = mg_condition_get_xml_id (iface);
        xmlSetProp (node, "id", str);
        g_free (str);

        xmlSetProp (node, "type", condition_type_to_str (cond->priv->type));

	base = mg_ref_base_get_ref_object (cond->priv->ops[MG_CONDITION_OP_LEFT]);
	if (base) {
		str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (base));
		xmlSetProp (node, "l_op", str);
		g_free (str);
	}

	base = mg_ref_base_get_ref_object (cond->priv->ops[MG_CONDITION_OP_RIGHT]);
	if (base) {
		str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (base));
		xmlSetProp (node, "r_op", str);
		g_free (str);
	}

	base = mg_ref_base_get_ref_object (cond->priv->ops[MG_CONDITION_OP_RIGHT2]);
	if (base) {
		str = mg_xml_storage_get_xml_id (MG_XML_STORAGE (base));
		xmlSetProp (node, "r_op2", str);
		g_free (str);
	}

	/* sub conditions */
	list = cond->priv->cond_children;
	while (list) {
		xmlNodePtr sub = mg_xml_storage_save_to_xml (MG_XML_STORAGE (list->data), error);
		if (sub)
                        xmlAddChild (node, sub);
                else {
                        xmlFreeNode (node);
                        return NULL;
                }
		list = g_slist_next (list);
	}

        return node;
}

static gboolean
mg_condition_load_from_xml (MgXmlStorage *iface, xmlNodePtr node, GError **error)
{
	MgCondition *cond;
        gchar *prop;
        gboolean id = FALSE;
	xmlNodePtr children;

        g_return_val_if_fail (iface && IS_MG_CONDITION (iface), FALSE);
        g_return_val_if_fail (MG_CONDITION (iface)->priv, FALSE);
        g_return_val_if_fail (node, FALSE);

	cond = MG_CONDITION (iface);

	if (strcmp (node->name, "MG_COND")) {
                g_set_error (error,
                             MG_CONDITION_ERROR,
                             MG_CONDITION_XML_LOAD_ERROR,
                             _("XML Tag is not <MG_COND>"));
                return FALSE;
        }

	prop = xmlGetProp (node, "id");
        if (prop) {
                gchar *ptr, *tok;
                ptr = strtok_r (prop, ":", &tok);
                ptr = strtok_r (NULL, ":", &tok);
                if (strlen (ptr) < 2) {
                        g_set_error (error,
                                     MG_CONDITION_ERROR,
                                     MG_CONDITION_XML_LOAD_ERROR,
                                     _("Wrong 'id' attribute in <MG_COND>"));
                        return FALSE;
                }
                mg_base_set_id (MG_BASE (cond), atoi (ptr+1));
		id = TRUE;
                g_free (prop);
        }

	prop = xmlGetProp (node, "type");
        if (prop) {
		cond->priv->type = condition_str_to_type (prop);
		if (cond->priv->type == MG_CONDITION_TYPE_UNKNOWN) {
			g_set_error (error,
                                     MG_CONDITION_ERROR,
                                     MG_CONDITION_XML_LOAD_ERROR,
                                     _("Wrong 'type' attribute in <MG_COND>"));
                        return FALSE;
		}
                g_free (prop);
        }

	prop = xmlGetProp (node, "l_op");
	if (prop) {
		mg_ref_base_set_ref_name (cond->priv->ops[MG_CONDITION_OP_LEFT], MG_QFIELD_TYPE,
					  REFERENCE_BY_XML_ID, prop);
		g_free (prop);
	}


	prop = xmlGetProp (node, "r_op");
	if (prop) {
		mg_ref_base_set_ref_name (cond->priv->ops[MG_CONDITION_OP_RIGHT], MG_QFIELD_TYPE,
					  REFERENCE_BY_XML_ID, prop);
		g_free (prop);
	}

	prop = xmlGetProp (node, "r_op2");
	if (prop) {
		mg_ref_base_set_ref_name (cond->priv->ops[MG_CONDITION_OP_RIGHT2], MG_QFIELD_TYPE,
					  REFERENCE_BY_XML_ID, prop);
		g_free (prop);
	}

	/* children nodes */
	children = node->children;
	while (children) {
		if (!strcmp (children->name, "MG_COND")) {
			MgCondition *scond;

			scond = MG_CONDITION (mg_condition_new (cond->priv->query, MG_CONDITION_NODE_AND));
			if (mg_xml_storage_load_from_xml (MG_XML_STORAGE (scond), children, error)) {
				mg_condition_node_add_child (cond, scond, NULL);
				g_object_unref (G_OBJECT (scond));
			}
			else
				return FALSE;
                }

		children = children->next;
	}

	if (!id) {
		g_set_error (error,
			     MG_CONDITION_ERROR,
			     MG_CONDITION_XML_LOAD_ERROR,
			     _("Missing Id attribute in <MG_COND>"));
		return FALSE;
        }

        return TRUE;
}


/*
 * MgRenderer interface implementation
 */
static GdaXqlItem *
mg_condition_render_as_xql (MgRenderer *iface, MgContext *context, GError **error)
{
        GdaXqlItem *node = NULL;

        g_return_val_if_fail (iface && IS_MG_CONDITION (iface), NULL);
        g_return_val_if_fail (MG_CONDITION (iface)->priv, NULL);

        TO_IMPLEMENT;
        return node;
}

static gchar *
mg_condition_render_as_sql (MgRenderer *iface, MgContext *context, GError **error)
{
        gchar *retval = NULL, *str;
	GString *string;
        MgCondition *cond;
	gboolean is_node = FALSE;
	gchar *link = NULL;
	MgBase *ops[3];
	gint i;

        g_return_val_if_fail (iface && IS_MG_CONDITION (iface), NULL);
        g_return_val_if_fail (MG_CONDITION (iface)->priv, NULL);
        cond = MG_CONDITION (iface);
	if (!mg_condition_activate (MG_REFERER (cond))) {
		g_set_error (error,
			     MG_CONDITION_ERROR,
			     MG_CONDITION_RENDERER_ERROR,
			     _("Condition is not active"));
		return NULL;
	}

	for (i=0; i<3; i++)
		ops[i] = mg_ref_base_get_ref_object (cond->priv->ops[i]);

	/* testing for completeness */
	switch (cond->priv->type) {
	case MG_CONDITION_LEAF_EQUAL:
	case MG_CONDITION_LEAF_DIFF:
	case MG_CONDITION_LEAF_SUP:
        case MG_CONDITION_LEAF_SUPEQUAL:
        case MG_CONDITION_LEAF_INF:
        case MG_CONDITION_LEAF_INFEQUAL:
        case MG_CONDITION_LEAF_LIKE:
        case MG_CONDITION_LEAF_REGEX:
	case MG_CONDITION_LEAF_IN:
		if (!ops[MG_CONDITION_OP_LEFT] || !ops[MG_CONDITION_OP_RIGHT]) {
			g_set_error (error,
				     MG_CONDITION_ERROR,
				     MG_CONDITION_RENDERER_ERROR,
				     _("Condition must have two arguments"));
			return NULL;
		}
		break;
        case MG_CONDITION_LEAF_BETWEEN:
		if (!ops[MG_CONDITION_OP_LEFT] || !ops[MG_CONDITION_OP_RIGHT] ||
		    !ops[MG_CONDITION_OP_RIGHT2]) {
			g_set_error (error,
				     MG_CONDITION_ERROR,
				     MG_CONDITION_RENDERER_ERROR,
				     _("Condition must have three arguments"));
			return NULL;
		}
	default:
		break;
	}

	/* actual rendering */
	string = g_string_new ("");
	switch (cond->priv->type) {
	case MG_CONDITION_NODE_AND:
		is_node = TRUE;
		link = " AND ";
		break;
	case MG_CONDITION_NODE_OR:
		is_node = TRUE;
		link = " OR ";
		break;
	case MG_CONDITION_NODE_NOT:
		is_node = TRUE;
		link = " NOT ";
		break;
	case MG_CONDITION_LEAF_EQUAL:
		link = "=";
		if (IS_MG_QF_VALUE (ops[MG_CONDITION_OP_RIGHT]) &&
		    mg_qf_value_is_value_null (MG_QF_VALUE (ops[MG_CONDITION_OP_RIGHT]), context))
			link = " IS ";
		break;
	case MG_CONDITION_LEAF_DIFF:
		link = "!=";
		if (IS_MG_QF_VALUE (ops[MG_CONDITION_OP_RIGHT]) &&
		    mg_qf_value_is_value_null (MG_QF_VALUE (ops[MG_CONDITION_OP_RIGHT]), context))
			link = " IS NOT ";
		break;
	case MG_CONDITION_LEAF_SUP:
		link = ">";
		break;
        case MG_CONDITION_LEAF_SUPEQUAL:
		link = ">=";
		break;
        case MG_CONDITION_LEAF_INF:
		link = "<";
		break;
        case MG_CONDITION_LEAF_INFEQUAL:
		link = "<=";
		break;
        case MG_CONDITION_LEAF_LIKE:
		link = " LIKE ";
		break;
        case MG_CONDITION_LEAF_REGEX:
		link = "~";
		break;
	case MG_CONDITION_LEAF_IN:
		link = " IN ";
		break;
        case MG_CONDITION_LEAF_BETWEEN:
		link = " BETWEEN ";
		break;
	default:
		break;
	}

	if (link) {
		if (is_node) {
			gboolean first = TRUE;
			GSList *list;
			list = cond->priv->cond_children;
			while (list) {
				if (first)
					first = FALSE;
				else
					g_string_append_printf (string, "%s", link);
				str = mg_renderer_render_as_sql (MG_RENDERER (list->data), context, error);
				if (!str) {
					g_string_free (string, TRUE);
					return NULL;
				}
				g_string_append (string, str);
				g_free (str);

				list = g_slist_next (list);
			}
		}
		else {
			str = mg_renderer_render_as_sql (MG_RENDERER (ops[MG_CONDITION_OP_LEFT]), context, error);
			if (!str) {
				g_string_free (string, TRUE);
				return NULL;
			}
			g_string_append (string, str);
			g_free (str);
			g_string_append_printf (string, "%s", link);
			str = mg_renderer_render_as_sql (MG_RENDERER (ops[MG_CONDITION_OP_RIGHT]), context, error);
			if (!str) {
				g_string_free (string, TRUE);
				return NULL;
			}
			g_string_append (string, str);
			g_free (str);
		}
	}

	if (cond->priv->type == MG_CONDITION_LEAF_BETWEEN) {
		str = mg_renderer_render_as_sql (MG_RENDERER (ops[MG_CONDITION_OP_RIGHT2]), context, error);
		if (!str) {
			g_string_free (string, TRUE);
			return NULL;
		}
		g_string_append_printf (string, " AND %s", str);
		g_free (str);
	}

	retval = string->str;
	g_string_free (string, FALSE);

        return retval;
}

static gchar *
mg_condition_render_as_str (MgRenderer *iface, MgContext *context)
{
        gchar *str = NULL;

        g_return_val_if_fail (iface && IS_MG_CONDITION (iface), NULL);
        g_return_val_if_fail (MG_CONDITION (iface)->priv, NULL);

        str = mg_condition_render_as_sql (iface, context, NULL);
        if (!str)
                str = g_strdup ("???");
        return str;
}


/*
 * MgReferer interface implementation
 */
static gboolean
mg_condition_activate (MgReferer *iface)
{
	gboolean activated = TRUE;
	gint i;

        g_return_val_if_fail (iface && IS_MG_CONDITION (iface), FALSE);
        g_return_val_if_fail (MG_CONDITION (iface)->priv, FALSE);

	for (i=0; i<3; i++) {
		if (!mg_ref_base_activate (MG_CONDITION (iface)->priv->ops[i]))
			activated = FALSE;
	}

	return activated;
}

static void
mg_condition_deactivate (MgReferer *iface)
{
	gint i;
        g_return_if_fail (iface && IS_MG_CONDITION (iface));
        g_return_if_fail (MG_CONDITION (iface)->priv);

	for (i=0; i<3; i++)
		mg_ref_base_deactivate (MG_CONDITION (iface)->priv->ops[i]);
}

static gboolean
mg_condition_is_active (MgReferer *iface)
{
	gboolean activated = TRUE;
	gint i;

        g_return_val_if_fail (iface && IS_MG_CONDITION (iface), FALSE);
        g_return_val_if_fail (MG_CONDITION (iface)->priv, FALSE);

	for (i=0; i<3; i++) {
		if (!mg_ref_base_is_active (MG_CONDITION (iface)->priv->ops[i]))
			activated = FALSE;
	}

	return activated;
}

static GSList *
mg_condition_get_ref_objects (MgReferer *iface)
{
        GSList *list = NULL;
	gint i;
	
        g_return_val_if_fail (iface && IS_MG_CONDITION (iface), NULL);
        g_return_val_if_fail (MG_CONDITION (iface)->priv, NULL);

	for (i=0; i<3; i++) {
                MgBase *base = mg_ref_base_get_ref_object (MG_CONDITION (iface)->priv->ops[i]);
                if (base)
                        list = g_slist_append (list, base);
        }

        return list;
}

/**
 * mg_condition_get_ref_objects_all
 * @cond: a #MgCondition object
 *
 * Get a complete list of the objects referenced by @cond, including its descendants (unlike the mg_referer_get_ref_objects()
 * function applied to @cond).
 *
 * Returns: a new list of referenced objects
 */
GSList *
mg_condition_get_ref_objects_all (MgCondition *cond)
{
        GSList *list = NULL, *children;
	gint i;
	
        g_return_val_if_fail (cond && IS_MG_CONDITION (cond), NULL);
        g_return_val_if_fail (cond->priv, NULL);

	for (i=0; i<3; i++) {
		if (cond->priv->ops[i]) {
			MgBase *base = mg_ref_base_get_ref_object (cond->priv->ops[i]);
			if (base)
				list = g_slist_append (list, base);
		}
        }

	children = cond->priv->cond_children;
	while (children) {
		GSList *clist = mg_condition_get_ref_objects_all (MG_CONDITION (children->data));
		if (clist)
			list = g_slist_concat (list, clist);
		children = g_slist_next (children);
	}

        return list;
}

static void
mg_condition_replace_refs (MgReferer *iface, GHashTable *replacements)
{
	gint i;
	MgCondition *cond;
	GSList *list;

        g_return_if_fail (iface && IS_MG_CONDITION (iface));
        g_return_if_fail (MG_CONDITION (iface)->priv);

        cond = MG_CONDITION (iface);
        if (cond->priv->query) {
                MgQuery *query = g_hash_table_lookup (replacements, cond->priv->query);
                if (query) {
                        g_signal_handlers_disconnect_by_func (G_OBJECT (cond->priv->query),
                                                              G_CALLBACK (nullified_object_cb), cond);
                        cond->priv->query = query;
                        g_signal_connect (G_OBJECT (query), "nullified",
                                          G_CALLBACK (nullified_object_cb), cond);
                }
        }

	if (cond->priv->join) {
                MgJoin *join = g_hash_table_lookup (replacements, cond->priv->join);
                if (join) {
                        g_signal_handlers_disconnect_by_func (G_OBJECT (cond->priv->join),
                                                              G_CALLBACK (nullified_object_cb), cond);
                        cond->priv->join = join;
                        g_signal_connect (G_OBJECT (join), "nullified",
                                          G_CALLBACK (nullified_object_cb), cond);
                }
        }

	for (i=0; i<3; i++)
                mg_ref_base_replace_ref_object (cond->priv->ops[i], replacements);

	list = cond->priv->cond_children;
	while (list) {
		mg_condition_replace_refs (MG_REFERER (list->data), replacements);
		list = g_slist_next (list);
	}
}
