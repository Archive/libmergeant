/* mg-entity.c
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mg-entity.h"
#include "mg-base.h"
#include "mg-context.h"
#include "marshal.h"


/* signals */
enum
{
	FIELD_ADDED,
	FIELD_REMOVED,
	FIELD_UPDATED,
	FIELDS_ORDER_CHANGED,
	LAST_SIGNAL
};

static gint mg_entity_signals[LAST_SIGNAL] = { 0, 0, 0, 0 };

static void mg_entity_iface_init (gpointer g_class);

GType
mg_entity_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgEntityIface),
			(GBaseInitFunc) mg_entity_iface_init,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) NULL,
			NULL,
			NULL,
			0,
			0,
			(GInstanceInitFunc) NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, "MgEntity", &info, 0);
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
	}
	return type;
}


static void
mg_entity_iface_init (gpointer g_class)
{
	static gboolean initialized = FALSE;

	if (! initialized) {
		mg_entity_signals[FIELD_ADDED] =
			g_signal_new ("field_added",
				      MG_ENTITY_TYPE,
				      G_SIGNAL_RUN_FIRST,
				      G_STRUCT_OFFSET (MgEntityIface, field_added),
				      NULL, NULL,
				      marshal_VOID__POINTER, G_TYPE_NONE,
				      1, G_TYPE_POINTER);
		mg_entity_signals[FIELD_REMOVED] =
			g_signal_new ("field_removed",
				      MG_ENTITY_TYPE,
				      G_SIGNAL_RUN_FIRST,
				      G_STRUCT_OFFSET (MgEntityIface, field_removed),
				      NULL, NULL,
				      marshal_VOID__POINTER, G_TYPE_NONE,
				      1, G_TYPE_POINTER);
		mg_entity_signals[FIELD_UPDATED] =
			g_signal_new ("field_updated",
				      MG_ENTITY_TYPE,
				      G_SIGNAL_RUN_FIRST,
				      G_STRUCT_OFFSET (MgEntityIface, field_updated),
				      NULL, NULL,
				      marshal_VOID__POINTER, G_TYPE_NONE,
				      1, G_TYPE_POINTER);
		mg_entity_signals[FIELDS_ORDER_CHANGED] =
			g_signal_new ("fields_order_changed",
				      MG_ENTITY_TYPE,
				      G_SIGNAL_RUN_FIRST,
				      G_STRUCT_OFFSET (MgEntityIface, fields_order_changed),
				      NULL, NULL,
				      marshal_VOID__VOID, G_TYPE_NONE,
				      0);		
		initialized = TRUE;
	}
}

/**
 * mg_entity_get_all_fields
 * @iface: an object implementing the #MgEntity interface
 *
 * Get a new list containing all the #DbField objects held within the object
 * implementing the #MgEntity interface.
 *
 * Returns: the new list.
 */
GSList *
mg_entity_get_all_fields (MgEntity *iface)
{
	g_return_val_if_fail (iface && IS_MG_ENTITY (iface), NULL);

	if (MG_ENTITY_GET_IFACE (iface)->get_all_fields)
		return (MG_ENTITY_GET_IFACE (iface)->get_all_fields) (iface);
	
	return NULL;	
}

/**
 * mg_entity_get_visible_fields
 * @iface: an object implementing the #MgEntity interface
 *
 * Same as mg_entity_get_all_fields() but returns only the visible fields, the
 * ones which are not hidden (this makes sense only for queries, not for DBMS tables
 * where there are no hidden fields).
 *
 * Returns: the new list.
 */
GSList *
mg_entity_get_visible_fields (MgEntity *iface)
{
	g_return_val_if_fail (iface && IS_MG_ENTITY (iface), NULL);

	if (MG_ENTITY_GET_IFACE (iface)->get_visible_fields)
		return (MG_ENTITY_GET_IFACE (iface)->get_visible_fields) (iface);
	
	return NULL;
}

/**
 * mg_entity_get_field_by_name
 * @iface: an object implementing the #MgEntity interface
 * @name:
 *
 * Get a #MgField using its name
 *
 * Returns: the requested #MgField
 */
MgField *
mg_entity_get_field_by_name (MgEntity *iface, const gchar *name)
{
	g_return_val_if_fail (iface && IS_MG_ENTITY (iface), NULL);

	if (MG_ENTITY_GET_IFACE (iface)->get_field_by_name)
		return (MG_ENTITY_GET_IFACE (iface)->get_field_by_name) (iface, name);
	
	return NULL;
}


/**
 * mg_entity_get_field_by_xml_id
 * @iface: an object implementing the #MgEntity interface
 * @xml_id:
 *
 *
 * Returns: the requested MgField
 */
MgField *
mg_entity_get_field_by_xml_id (MgEntity *iface, const gchar *xml_id)
{
	g_return_val_if_fail (iface && IS_MG_ENTITY (iface), NULL);

	if (MG_ENTITY_GET_IFACE (iface)->get_field_by_xml_id)
		return (MG_ENTITY_GET_IFACE (iface)->get_field_by_xml_id) (iface, xml_id);
	
	return NULL;
}

/**
 * mg_entity_get_field_by_index
 * @iface: an object implementing the #MgEntity interface
 * @index:
 *
 *
 * Returns: the requested MgField or NULL if the index is out of bounds
 */
MgField *
mg_entity_get_field_by_index (MgEntity *iface, gint index)
{
	g_return_val_if_fail (iface && IS_MG_ENTITY (iface), NULL);

	if (MG_ENTITY_GET_IFACE (iface)->get_field_by_index)
		return (MG_ENTITY_GET_IFACE (iface)->get_field_by_index) (iface, index);
	
	return NULL;
}

/**
 * mg_entity_get_field_index
 * @iface: an object implementing the #MgEntity interface
 * @field: an object implementing the #MgField interface
 *
 * Get the position of the field in the given entity. Positions start at 0.
 * @field MUST be a visible field.
 *
 * Returns: the position or -1 if the field is not in the entity
 */
gint
mg_entity_get_field_index (MgEntity *iface, MgField *field)
{
	g_return_val_if_fail (iface && IS_MG_ENTITY (iface), FALSE);

	if (MG_ENTITY_GET_IFACE (iface)->get_field_index)
		return (MG_ENTITY_GET_IFACE (iface)->get_field_index) (iface, field);	

	return FALSE;
}

/**
 * mg_entity_add_field
 * @iface: an object implementing the #MgEntity interface
 * @field: an object implementing the #MgField interface to add
 *
 * Add @field to @iface's fields (at the end of the list)
 */
void
mg_entity_add_field (MgEntity *iface, MgField *field)
{
	g_return_if_fail (iface && IS_MG_ENTITY (iface));

	if (MG_ENTITY_GET_IFACE (iface)->add_field)
		(MG_ENTITY_GET_IFACE (iface)->add_field) (iface, field);
}

/**
 * mg_entity_add_field_before
 * @iface: an object implementing the #MgEntity interface
 * @field: an object implementing the #MgField interface to add
 * @field_before: an object implementing the #MgField interface before which @field will be added, or %NULL
 *
 * Add @field to @iface's fields, before @field_before if it is not %NULL, 
 * or at the end if @field_before is %NULL.
 */
void
mg_entity_add_field_before (MgEntity *iface, MgField *field, MgField *field_before)
{
	g_return_if_fail (iface && IS_MG_ENTITY (iface));

	if (MG_ENTITY_GET_IFACE (iface)->add_field_before)
		(MG_ENTITY_GET_IFACE (iface)->add_field_before) (iface, field, field_before);
}

/**
 * mg_entity_swap_fields
 * @iface: an object implementing the #MgEntity interface
 * @field1: an object implementing the #MgField interface
 * @field2: an object implementing the #MgField interface
 */
void
mg_entity_swap_fields (MgEntity *iface, MgField *field1, MgField *field2)
{
	g_return_if_fail (iface && IS_MG_ENTITY (iface));

	if (MG_ENTITY_GET_IFACE (iface)->swap_fields)
		(MG_ENTITY_GET_IFACE (iface)->swap_fields) (iface, field1, field2);
}

/**
 * mg_entity_remove_field
 * @iface: an object implementing the #MgEntity interface
 * @field: an object implementing the #MgField interface to remove
 */
void
mg_entity_remove_field (MgEntity *iface, MgField *field)
{
	g_return_if_fail (iface && IS_MG_ENTITY (iface));

	if (MG_ENTITY_GET_IFACE (iface)->remove_field)
		(MG_ENTITY_GET_IFACE (iface)->remove_field) (iface, field);
}

/**
 * mg_entity_is_writable
 * @iface: an object implementing the #MgEntity interface
 *
 * Tells if the real entity (the corresponding DBMS object) represented by @iface can be written to.
 *
 * Returns: TRUE if it is possible to write to @iface
 */
gboolean
mg_entity_is_writable (MgEntity *iface)
{
	g_return_val_if_fail (iface && IS_MG_ENTITY (iface), FALSE);

	if (MG_ENTITY_GET_IFACE (iface)->is_writable)
		return (MG_ENTITY_GET_IFACE (iface)->is_writable) (iface);	

	return FALSE;
}

/**
 * mg_entity_get_parameters
 * @iface: an object implementing the #MgEntity interface
 *
 * Get a list of parameters required before @iface can be used. Usualy this function is used with queries
 * to grab the required parameters before executing a query.
 *
 * Returns: a new list of #MgParameter objects. @iface DOES NOT hold any reference to any of the listed parameter.
 */
GSList *
mg_entity_get_parameters (MgEntity *iface)
{
	g_return_val_if_fail (iface && IS_MG_ENTITY (iface), NULL);

	if (MG_ENTITY_GET_IFACE (iface)->get_parameters)
		return (MG_ENTITY_GET_IFACE (iface)->get_parameters) (iface);	

	return FALSE;
}



/**
 * mg_entity_get_exec_context
 * @iface: an object implementing the #MgEntity interface
 *
 * Creates a new #MgContext object which contains all the parameters required to use @iface.
 *
 * Returns: a new #MgContext object
 */
MgContext *
mg_entity_get_exec_context (MgEntity *iface)
{
	MgContext *context;
	GSList *list, *params;
	MgConf *conf;

	g_return_val_if_fail (iface && IS_MG_ENTITY (iface), NULL);	

	conf = mg_base_get_conf (MG_BASE (iface));
	params = mg_entity_get_parameters (iface);
	context = MG_CONTEXT (mg_context_new (conf, params));
 
	/* get rid of the params list since we don't use them anymore */
	list = params;
	while (list) {
		g_object_unref (G_OBJECT (list->data));
		list = g_slist_next (list);
	}
	g_slist_free (params);

	return context;
}
