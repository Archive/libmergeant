#include "../libmergeant/libmergeant.h"
#include "mg-test-common.h"

#define MAKE_DUMPS 1
#define SHOW_STEPS
/*#undef SHOW_STEPS*/

#define STEP_SEPARATOR "------------------"

gint main (int argc, char **argv) {
	MgConf *conf;
	GError *error = NULL;
	GSList *queries, *list;

	gtk_init (&argc, &argv);

	g_print ("############################ TEST 1 ################################\n"); 
	g_print ("# Loading DATA3.xml                                                #\n");
	g_print ("####################################################################\n"); 

	conf = MG_CONF (mg_conf_new ());
	if (!mg_conf_load_xml_file (conf, "DATA3.xml", &error)) {
		g_print ("Error occured:\n\t%s\n", error->message);
		g_error_free (error);
		error = NULL;
	}
#ifdef SHOW_STEPS
	else
		g_print ("DATA3.xml file loaded.\n");
#endif
#ifdef debug
/* 	if (MAKE_DUMPS) */
/* 		mg_conf_dump (conf); */
#endif	
	if (!mg_conf_save_xml_file (conf, "DATA3_OUT.xml", &error)) {
		g_print ("Error occured:\n\t%s\n", error->message);
		g_error_free (error);
		error = NULL;
	}
	else {
		gboolean cmp;
		gint status;
#ifdef SHOW_STEPS
		g_print ("File written to DATA3_OUT.xml.\n");
#endif
		g_print ("Comparing DATA3.xml and DATA3_OUT.xml... ");

		cmp = g_spawn_command_line_sync ("/usr/bin/cmp -s DATA3.xml DATA3_OUT.xml",
						 NULL, NULL, &status, &error);
		if (cmp) {
			if (!status)
				g_print ("Equal => TEST OK\n");
			else
				g_print ("Different => TEST FAILED\n");
		}
		else {
			g_print ("Error occured:\n\t%s\n", error->message);
			g_error_free (error);
		}
	}

	queries = mg_conf_get_queries (conf);
	list = queries;
	while (list) {
		MgQuery *query = MG_QUERY (list->data);
		gchar *str;
		MgQuery *copy;
		GSList *plist, *params;

		/* rendering that query */
#ifdef debug
		g_print ("\n" STEP_SEPARATOR " Query: %s " STEP_SEPARATOR "\n", mg_base_get_name (MG_BASE (query)));
		mg_base_dump (MG_BASE (query), 0);
#else
		str = mg_renderer_render_as_sql (MG_RENDERER (query), NULL, NULL);
		g_print ("\n" STEP_SEPARATOR " Query: %s " STEP_SEPARATOR "\nDES=%s\nSQL=%s\n", mg_base_get_name (MG_BASE (query)),
			 mg_base_get_description (MG_BASE (query)),
			 str);
		g_free (str);
#endif
		/* list the parameters */
		params = mg_entity_get_parameters (MG_ENTITY (query));
		plist = params;
		while (plist) {
			if (plist == params)
				g_print ("PARAMETERS:\n");
#ifdef debug
			mg_base_dump (MG_BASE (plist->data), 5);
#endif
			g_object_unref (G_OBJECT (plist->data));
			plist = g_slist_next (plist);
		}
		g_slist_free (params);

		/* copy the query */
		copy = MG_QUERY (mg_query_new_copy (query, NULL));
#ifdef debug
		g_print ("\n" STEP_SEPARATOR " Query: %s " STEP_SEPARATOR "\n", mg_base_get_name (MG_BASE (copy)));
		mg_base_dump (MG_BASE (copy), 0);
#else
		str = mg_renderer_render_as_sql (MG_RENDERER (copy), NULL, NULL);
		g_print ("\n" STEP_SEPARATOR " COPY: %s " STEP_SEPARATOR "\nDES=%s\nSQL=%s\n", mg_base_get_name (MG_BASE (copy)),
			 mg_base_get_description (MG_BASE (copy)),
			 str);
		g_free (str);
#endif
		/* list the parameters */
		params = mg_entity_get_parameters (MG_ENTITY (copy));
		plist = params;
		while (plist) {
			if (plist == params)
				g_print ("PARAMETERS:\n");
#ifdef debug
			mg_base_dump (MG_BASE (plist->data), 5);
#endif
			g_object_unref (G_OBJECT (plist->data));
			plist = g_slist_next (plist);
		}
		g_slist_free (params);


		g_object_unref (G_OBJECT (copy));

		list = g_slist_next (list);
	}
	g_slist_free (queries);

	g_object_unref (G_OBJECT (conf));


	return 0;
}
