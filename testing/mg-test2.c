#include "../libmergeant/libmergeant.h"
#include "mg-test-common.h"

#define MAKE_DUMPS 0
#define SHOW_STEPS
/*#undef SHOW_STEPS*/

#define STEP_SEPARATOR "------------------"

gint main (int argc, char **argv) {
	MgConf *conf;
	MgServer *srv;
	MgDatabase *db;
	GError *error = NULL;
	gboolean testerror;
	gchar *source, *user;

	gtk_init (&argc, &argv);

	source = get_first_datasource ();
	if (!source) {
		g_print ("Can't execute tests, no datasource available\n");
		exit (1);
	}
	user = get_datasource_user (source);


	g_print ("############################ TEST 1 ################################\n"); 
	g_print ("# MgConf creation, connection to DBMS and writing TEST1.xml        #\n");
	g_print ("####################################################################\n"); 
	conf = MG_CONF (mg_conf_new ());

#ifdef debug
	if (MAKE_DUMPS)
		mg_conf_dump (conf);
#endif
#ifdef SHOW_STEPS
	g_print (STEP_SEPARATOR "MgServer connection\n"); 
#endif
	srv = mg_conf_get_server (conf);

	mg_server_set_datasource (srv, source);
	mg_server_set_user_name (srv, user);
	mg_server_set_user_password (srv, "");
	g_object_set (G_OBJECT (srv), "with_functions", TRUE, NULL);

	if (!mg_server_open_connect (srv, &error)) {
		g_print ("CONNECTION ERROR: %s\n", error->message);
		g_error_free (error);
		error = NULL;
	}

	/* DBMS data management */
#ifdef SHOW_STEPS
	g_print (STEP_SEPARATOR "Loading meta data from DBMS\n"); 
#endif
	if (!mg_server_update_dbms_data (srv, &error)) {
#ifdef SHOW_STEPS
		g_print ("SERVER DBMS DATA UPDATE: %s\n", error->message);
#endif
		g_error_free (error);
		error = NULL;
	}
#ifdef SHOW_STEPS
	else
		g_print ("Dbms data loaded.\n");
#endif

	/* Database data */
	db = mg_conf_get_database (conf);
#ifdef SHOW_STEPS
	g_print (STEP_SEPARATOR "Loading meta data from Database\n"); 
#endif
	if (!mg_database_update_dbms_data (db, &error)) {
#ifdef SHOW_STEPS
		g_print ("Database DBMS DATA UPDATE: %s\n", error->message);
#endif
		g_error_free (error);
		error = NULL;
	}
#ifdef SHOW_STEPS
	else
		g_print ("Database DBMS data loaded.\n");
#endif	


#ifdef debug
	if (MAKE_DUMPS)
		mg_conf_dump (conf);
#endif	
#ifdef SHOW_STEPS
	g_print (STEP_SEPARATOR "Saving data to TEST1.xml\n"); 
#endif
	if (!mg_conf_save_xml_file (conf, "TEST1.xml", &error)) {
		g_print ("Error occured:\n\t%s\n", error->message);
		g_error_free (error);
		error = NULL;
	}
#ifdef SHOW_STEPS
	else
		g_print ("File written to TEST1.xml.\n");

	g_print (STEP_SEPARATOR "Memory liberation\n"); 
#endif
	mg_server_close_connect (srv);
#ifdef SHOW_STEPS
	g_print ("CONNECTION to server closed.\n");
#endif
	g_object_unref (G_OBJECT (conf));

	g_print ("\n\n");
	g_print ("############################ TEST 2 ################################\n"); 
	g_print ("# loading TEST1.xml and writing TEST2.xml                          #\n");
	g_print ("####################################################################\n"); 

	conf = MG_CONF (mg_conf_new ());
	if (!mg_conf_load_xml_file (conf, "TEST1.xml", &error)) {
		g_print ("Error occured:\n\t%s\n", error->message);
		g_error_free (error);
		error = NULL;
	}
#ifdef SHOW_STEPS
	else
		g_print ("TEST1.xml file loaded.\n");
#endif
#ifdef debug
	if (MAKE_DUMPS)
		mg_conf_dump (conf);
#endif	
	if (!mg_conf_save_xml_file (conf, "TEST2.xml", &error)) {
		g_print ("Error occured:\n\t%s\n", error->message);
		g_error_free (error);
		error = NULL;
	}
	else {
		gboolean cmp;
		gint status;
#ifdef SHOW_STEPS
		g_print ("File written to TEST2.xml.\n");
#endif
		g_print ("Comparing TEST1.xml and TEST2.xml... ");

		cmp = g_spawn_command_line_sync ("/usr/bin/cmp -s TEST1.xml TEST2.xml",
						 NULL, NULL, &status, &error);
		if (cmp) {
			if (!status)
				g_print ("Equal => TEST OK\n");
			else
				g_print ("Different => TEST FAILED\n");
		}
		else {
			g_print ("Error occured:\n\t%s\n", error->message);
			g_error_free (error);
		}
	}
#ifdef SHOW_STEPS
	g_print (STEP_SEPARATOR "Memory liberation\n"); 
#endif
	g_object_unref (G_OBJECT (conf));

	return 0;

	g_print ("\n\n");
	g_print ("############################ TEST 3 ################################\n"); 
	g_print ("# loading TEST1.xml, data supression propagation and               #\n");
	g_print ("# re-synchronazition of meta date from the DBMS                    #\n");
	g_print ("####################################################################\n"); 
	testerror = FALSE;
	conf = MG_CONF (mg_conf_new ());
	if (!mg_conf_load_xml_file (conf, "TEST1.xml", &error)) {
		g_print ("Error occured:\n\t%s\n", error->message);
		g_error_free (error);
		error = NULL;
		testerror =TRUE;
	}
#ifdef SHOW_STEPS
	else
		g_print ("TEST1.xml file loaded.\n");
#endif

	if (!testerror) {
		MgServerDataType *dt;

		dt = mg_server_get_data_type_by_name (mg_conf_get_server (conf), "float4");
#ifdef SHOW_STEPS
		g_print ("MgServerDataType for 'float4' is %p\n", dt);
		g_print (STEP_SEPARATOR "Calling mg_base_nullify on %p\n", dt); 
#endif
		mg_base_nullify (MG_BASE (dt));
#ifdef debug
		if (MAKE_DUMPS)
			mg_conf_dump (conf);
#endif
#ifdef SHOW_STEPS	
		g_print (STEP_SEPARATOR "Saving to TEST3.xml\n"); 
#endif
		if (!mg_conf_save_xml_file (conf, "TEST3.xml", &error)) {
			g_print ("Error occured:\n\t%s\n", error->message);
			g_error_free (error);
			error = NULL;
		}

#ifdef SHOW_STEPS
		g_print (STEP_SEPARATOR "Update meta data from DBMS\n"); 
#endif
		srv = mg_conf_get_server (conf);

		mg_server_set_datasource (srv, source);
		mg_server_set_user_name (srv, user);
		mg_server_set_user_password (srv, "");
		
		if (!mg_server_open_connect (srv, &error)) {
			g_print ("CONNECTION ERROR: %s\n", error->message);
			g_error_free (error);
			error = NULL;
		}
		else {
			if (!mg_server_update_dbms_data (srv, &error)) {
#ifdef SHOW_STEPS
				g_print ("SERVER DBMS DATA UPDATE: %s\n", error->message);
#endif
				g_error_free (error);
				error = NULL;
			}
			else {
#ifdef SHOW_STEPS
				g_print (STEP_SEPARATOR "Saving to TEST4.xml\n"); 
#endif
				if (!mg_conf_save_xml_file (conf, "TEST4.xml", &error)) {
					g_print ("Error occured:\n\t%s\n", error->message);
					g_error_free (error);
					error = NULL;
				}
				else {
					gboolean cmp;
					gint status;
					g_print ("Comparing TEST1.xml and TEST4.xml... ");
					
					cmp = g_spawn_command_line_sync ("/usr/bin/cmp -s TEST1.xml TEST4.xml",
									 NULL, NULL, &status, &error);
					if (cmp) {
						if (!status)
							g_print ("Equal => TEST OK\n");
						else
							g_print ("Different => TEST FAILED\n");
					}
					else {
						g_print ("Error occured:\n\t%s\n", error->message);
						g_error_free (error);
					}	
				}
			}
		}
	}
#ifdef SHOW_STEPS
	g_print (STEP_SEPARATOR "Memory liberation\n"); 
#endif
	g_object_unref (G_OBJECT (conf));

	g_free (source);
	g_free (user);

	return 0;
}
