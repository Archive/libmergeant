#include "mg-test-common.h"
#include <string.h>

gchar *
get_first_datasource (void)
{
	gchar *str = NULL;
	GList *ds_list;
	GList *node;
	GdaDataSourceInfo *info;
              
	ds_list = gda_config_get_data_source_list ();
        node = g_list_first (ds_list);
	if (node) {
		info = (GdaDataSourceInfo *) node->data;
		str = g_strdup (info->name);
		g_print ("Using source:\n\tNAME: %s\n\tPROVIDER: %s\n\tCNC: %s\n\tDESC: %s\n\tUSER: %s\n",
			 info->name, info->provider, info->cnc_string, info->description,
			 info->username);
	}
	gda_config_free_data_source_list (ds_list);
	
	return str;
}

gchar *
get_datasource_user (const gchar *ds)
{
	gchar *str = NULL;

	GList *ds_list;
	GList *node;
	GdaDataSourceInfo *info;
              
	ds_list = gda_config_get_data_source_list ();
        node = g_list_first (ds_list);
	while (node && !str) {
		info = (GdaDataSourceInfo *) node->data;
		if (!strcmp (info->name, ds))
			str = g_strdup (info->username);
		node = g_list_next (node);
	}
	gda_config_free_data_source_list (ds_list);

	return str;
}
