#include "../libmergeant/libmergeant.h"
#include "mg-test-common.h"

gint main (int argc, char **argv) {
	MgConf *conf;
	GError *error = NULL;
	MgGraphviz *graph;
	GSList *list, *tmplist;

	gtk_init (&argc, &argv);

	conf = MG_CONF (mg_conf_new ());
	g_print ("############################ GRAPHER ###############################\n"); 
	g_print ("# Loading DATA_GRAPH.xml                                           #\n");


	if (!mg_conf_load_xml_file (conf, "DATA_GRAPH.xml", &error)) {
		g_print ("Error occured:\n\t%s\n", error->message);
		g_error_free (error);
		error = NULL;
		exit (1);
	}

	graph = MG_GRAPHVIZ (mg_graphviz_new (conf));
	list = mg_conf_get_queries (conf);
	tmplist = list;
	while (list) {
		mg_graphviz_add_to_graph (graph, list->data);
		list = g_slist_next (list);
	}
	g_slist_free (tmplist);
	g_print ("# Saving DATA_GRAPH.dot                                            #\n");
	if (!mg_graphviz_save_file (graph, "DATA_GRAPH.dot", &error)) {
		g_print ("Error occured:\n\t%s\n", error->message);
		g_error_free (error);
		error = NULL;
		exit (1);
	}
	g_print ("####################################################################\n"); 
	g_object_unref (G_OBJECT (graph));
	g_object_unref (G_OBJECT (conf));

	return 0;
}
