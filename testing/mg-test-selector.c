#include <libmergeant/libmergeant.h>
#include <gtk/gtk.h>
#include "mg-test-common.h"

gchar *source = NULL, *user = NULL;
GtkWidget *mainwin = NULL;

static gboolean delete_event( GtkWidget *widget,
                              GdkEvent  *event,
                              gpointer   data )
{
    g_print ("Leaving test...\n");

    return FALSE;
}

static void destroy( GtkWidget *widget,
                     gpointer   data )
{
    gtk_main_quit ();
}

GtkWidget *build_menu (GtkWidget *mainwin);
GtkWidget *build_main (MgConf *conf, MgDataHandler *dh, GdaValueType type);

static void dt_type_delete_cb (GtkButton *button, MgConf *conf);
static void sync_dbms_cb      (GtkButton *button, MgConf *conf);
static void stop_sync_dbms_cb       (GtkButton *button, MgConf *conf);
static void query_action_cb         (GtkButton *button, MgConf *conf);
static void query_field_action_cb   (GtkButton *button, MgConf *conf);
static void query_render_sql_cb     (GtkButton *button, MgConf *conf);
static void query_exec_cb           (GtkButton *button, MgConf *conf);
static void selector_selection_changed_cb (MgSelector *mgsel, GObject *selection, gpointer data);
int 
main (int argc, char **argv)
{
	GtkWidget *vbox, *wid, *menu, *table, *button, *label, *exec1_button, *exec2_button;
	MgConf *conf;
	GError *error = NULL;

	/* Initialize i18n support */
	gtk_set_locale ();
	
	/* Initialize the widget set */
	gtk_init (&argc, &argv);

	/* tested objects */
	conf = MG_CONF (mg_conf_new ());

	/* Create the main window */
	mainwin = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width (GTK_CONTAINER (mainwin), 0);
	g_signal_connect (G_OBJECT (mainwin), "delete_event",
			  G_CALLBACK (delete_event), NULL);
	g_signal_connect (G_OBJECT (mainwin), "destroy",
			  G_CALLBACK (destroy), NULL);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (mainwin), vbox);
	gtk_widget_show (vbox);

	/* menu */
	menu = build_menu (mainwin);
	gtk_widget_show (menu);
	gtk_box_pack_start (GTK_BOX (vbox), menu, FALSE, FALSE, 0);

	/* Table layout for buttons */
	label = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (label), "<b>Actions:</b>");
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);
	gtk_misc_set_padding (GTK_MISC (label), 15, 15);
	gtk_widget_show (label);

	table = gtk_table_new (3, 4, FALSE);
	gtk_box_pack_start (GTK_BOX (vbox), table, FALSE, TRUE, 0);
	gtk_widget_show (table);
	gtk_table_set_row_spacings (GTK_TABLE (table), 5);
	gtk_table_set_col_spacings (GTK_TABLE (table), 5);

	/* operations on data types */
	label = gtk_label_new ("On data types:");
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);
	gtk_widget_show (label);
	button = gtk_button_new_with_label ("Delete int4");
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (dt_type_delete_cb), conf);
	gtk_table_attach_defaults (GTK_TABLE (table), button, 1, 2, 0, 1);
	g_object_set_data (G_OBJECT (button), "dt", "int4");
	gtk_widget_show (button);

	button = gtk_button_new_with_label ("Delete inet");
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (dt_type_delete_cb), conf);
	gtk_table_attach_defaults (GTK_TABLE (table), button, 2, 3, 0, 1);
	g_object_set_data (G_OBJECT (button), "dt", "inet");
	gtk_widget_show (button);

	/* operations on Database */
	label = gtk_label_new ("On database:");
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);
	gtk_widget_show (label);
	button = gtk_button_new_with_label ("Start DBMS update");
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (sync_dbms_cb), conf);
	gtk_table_attach_defaults (GTK_TABLE (table), button, 1, 2, 1, 2);
	gtk_widget_show (button);

	button = gtk_button_new_with_label ("Stop DBMS update");
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (stop_sync_dbms_cb), conf);
	gtk_table_attach_defaults (GTK_TABLE (table), button, 2, 3, 1, 2);
	gtk_widget_show (button);

	/* operations on queries */
	label = gtk_label_new ("On queries:");
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 2, 3);
	gtk_widget_show (label);

	/* button = gtk_button_new_with_label ("Add/Remove query 'QU6'"); */
/* 	g_signal_connect (G_OBJECT (button), "clicked", */
/* 			  G_CALLBACK (query_action_cb), conf); */
/* 	gtk_table_attach_defaults (GTK_TABLE (table), button, 1, 2, 2, 3); */
/* 	gtk_widget_show (button); */

	/* button = gtk_button_new_with_label ("Add/Remove field 'QU14:QF16'"); */
/* 	g_signal_connect (G_OBJECT (button), "clicked", */
/* 			  G_CALLBACK (query_field_action_cb), conf); */
/* 	gtk_table_attach_defaults (GTK_TABLE (table), button, 2, 3, 2, 3); */
/* 	gtk_widget_show (button); */

	button = gtk_button_new_with_label ("Render SQL");
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (query_render_sql_cb), conf);
	gtk_table_attach_defaults (GTK_TABLE (table), button, 1, 2, 2, 3);
	exec1_button = button;
	gtk_widget_show (button);

	button = gtk_button_new_with_label ("Execute Query");
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (query_exec_cb), conf);
	gtk_table_attach_defaults (GTK_TABLE (table), button, 2, 3, 2, 3);
	exec2_button = button;
	gtk_widget_show (button);


	/* Data loading if available */
	if (!mg_conf_load_xml_file (conf, "TEST_SELECTOR.xml", &error)) {
		source = get_first_datasource ();
		
		if (!source) {
			g_print ("Can't execute tests, no datasource available\n");
			exit (1);
		}
		user = get_datasource_user (source);
		
		g_error_free (error);
		error = NULL;
	}
	else {
		g_print ("Using TEST_SELECTOR.xml file\n\n");
		source = mg_server_get_datasource (mg_conf_get_server (conf));
		user = mg_server_get_user_name (mg_conf_get_server (conf));
	}

	if (!mg_server_open_connect (mg_conf_get_server (conf), &error)) {
		g_print ("Can't open connection: %s\n", error->message);
		g_error_free (error);
	}

	/* MgSelector widget */
	label = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (label), "<b>MgSelector widget:</b>");
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);
	gtk_misc_set_padding (GTK_MISC (label), 15, 15);
	gtk_widget_show (label);
	wid = mg_selector_new (conf, 
			       /* MG_SELECTOR_DATA_TYPES | MG_SELECTOR_FUNCTIONS | MG_SELECTOR_AGGREGATES |  */
 			       /*MG_SELECTOR_TABLES | MG_SELECTOR_FIELDS | */
			       MG_SELECTOR_QUERIES | MG_SELECTOR_SUB_QUERIES | MG_SELECTOR_QVIS_FIELDS,
			       MG_SELECTOR_COLUMN_COMMENTS | 
			       /* MG_SELECTOR_COLUMN_OWNER | */ 
			       MG_SELECTOR_COLUMN_TYPE | 
			       /* MG_SELECTOR_COLUMN_FIELD_LENGTH |  */
/* 			       MG_SELECTOR_COLUMN_FIELD_NNUL |  */
/* 			       MG_SELECTOR_COLUMN_FIELD_DEFAULT | */
			       MG_SELECTOR_COLUMN_QFIELD_VALUE |
			       MG_SELECTOR_COLUMN_QFIELD_TYPE);
	gtk_box_pack_start (GTK_BOX (vbox), wid, TRUE, TRUE, 0);
	gtk_widget_set_size_request (wid, 600, 300);
	gtk_widget_show (wid);
	g_signal_connect (G_OBJECT (wid), "selection_changed",
			  G_CALLBACK (selector_selection_changed_cb), wid);
	g_object_set_data (G_OBJECT (exec1_button), "mgsel", wid);
	g_object_set_data (G_OBJECT (exec2_button), "mgsel", wid);
	

	/* DBMS update viewer */
	label = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (label), "<b>MgDbmsUpdate widget:</b>");
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);
	gtk_misc_set_padding (GTK_MISC (label), 15, 15);
	gtk_widget_show (label);

	wid = mg_dbms_update_viewer_new (conf);
	gtk_box_pack_start (GTK_BOX (vbox), wid, FALSE, TRUE, 0);
	gtk_widget_show (wid);

	/* Show the application window */
	gtk_widget_show (mainwin);
	
	gtk_main ();
	g_object_unref (G_OBJECT (conf));

	g_free (source);
	g_free (user);

	return 0;
}

static void form_param_changed (MgForm *form, MgParameter *param, GtkDialog *dlg);
static void
query_render_sql_cb (GtkButton *button, MgConf *conf)
{
	MgSelector *mgsel;
	GObject *obj;

	mgsel = g_object_get_data (G_OBJECT (button), "mgsel");
	obj = mg_selector_get_selected_object (mgsel);
	if (obj && IS_MG_QUERY (obj)) {
		GError *error = NULL;
		MgQuery *query = MG_QUERY (obj);
		gchar *sql;
		MgGraphviz *graph;

#ifdef debug
		mg_base_dump (MG_BASE (query), 0);
#endif
		sql = mg_renderer_render_as_sql (MG_RENDERER (query), NULL, &error);
		if (sql) {
			GtkWidget *dlg;
			gint result;
			MgContext *context;

			context = mg_entity_get_exec_context (MG_ENTITY (query));
#ifdef debug
			g_print ("=============== CONTEXT DUMP ==========================\n");
			mg_base_dump (MG_BASE (context), 0);
			g_print ("=======================================================\n");
#endif
			dlg = mg_form_new_in_dialog (conf, context, GTK_WINDOW (mainwin),
						     "Missing parameters", "Fill in the missing "
						     "parameters");
			mg_form_set_entries_auto_default (g_object_get_data (G_OBJECT (dlg), "form"), TRUE);

			gtk_widget_show (dlg);
			result = gtk_dialog_run (GTK_DIALOG (dlg));
			switch (result)
				{
				case GTK_RESPONSE_ACCEPT:
					graph = mg_graphviz_new (conf);
					mg_graphviz_add_to_graph (graph, G_OBJECT (query));
					if (! mg_graphviz_save_file (graph, "QUERY.dot", &error)) {
						if (error) {
							g_print ("Graph=> ERROR = %s\n", error->message);
							g_error_free (error);
							error = NULL;
						}
						else
							g_print ("Graph=> Unknown error\n");
	
					}
					g_object_unref (G_OBJECT (graph));

					sql = mg_renderer_render_as_sql (MG_RENDERER (query), context, &error);
					if (sql) {
						g_print ("SQL: %s\n", sql);
						g_free (sql);
					}
					else {
						if (error) {
							g_print ("Exec=> ERROR = %s\n", error->message);
							g_error_free (error);
						}
						else
							g_print ("Exec=> Unknown error\n");
					}
						
					break;
				default:
					
					break;
				}
			gtk_widget_destroy (dlg);
			g_object_unref (G_OBJECT (context));
		}
		else {
			g_print ("Exec=> ERROR = %s\n", error->message);
			g_error_free (error);
		}
	}
	else {
		g_print ("Can only execute a MgQuery!\n");
	}
}

static void
query_exec_cb (GtkButton *button, MgConf *conf)
{
	MgSelector *mgsel;
	GObject *obj;

	mgsel = g_object_get_data (G_OBJECT (button), "mgsel");
	obj = mg_selector_get_selected_object (mgsel);
	if (obj && IS_MG_QUERY (obj)) {
		GError *error = NULL;
		MgQuery *query = MG_QUERY (obj);
		gchar *sql;

#ifdef debug
		MgGraphviz *graph;
		graph = mg_graphviz_new (conf);
		mg_graphviz_add_to_graph (graph, G_OBJECT (query));
		mg_graphviz_save_file (graph, "_initial_query.dot", NULL);
		g_object_unref (G_OBJECT (graph));
#endif
		sql = mg_renderer_render_as_sql (MG_RENDERER (query), NULL, &error);
		if (sql) {
			GtkWidget *form, *dlg;
			MgTarget *target = NULL;
			GSList *targets;
			
			targets = mg_query_get_targets (query);
			if (targets) {
				target = MG_TARGET (targets->data);
				g_slist_free (targets);
			}

			form = mg_work_form_new (query, target);
			dlg = gtk_dialog_new_with_buttons ("Execution", GTK_WINDOW (mainwin),
							   GTK_DIALOG_MODAL,
							   GTK_STOCK_CLOSE,
							   GTK_RESPONSE_ACCEPT,
							   NULL);
			gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dlg)->vbox), form);
			gtk_widget_show_all (dlg);
			mg_work_form_run (MG_WORK_FORM (form), 
					  MG_WORK_FORM_NAVIGATION_ARROWS | MG_WORK_FORM_NAVIGATION_SCROLL | 
					  MG_WORK_FORM_MODIF_BUTTONS |
					  MG_WORK_FORM_ASK_CONFIRM_DELETE | MG_WORK_FORM_ASK_CONFIRM_UPDATE | 
					  MG_WORK_FORM_ASK_CONFIRM_INSERT);
			gtk_dialog_run (GTK_DIALOG (dlg));
			gtk_widget_destroy (dlg);
		}
		else {
			g_print ("Exec=> ERROR = %s\n", error->message);
			g_error_free (error);
		}
	}
	else {
		g_print ("Can only execute a MgQuery!\n");
	}
}

static void
form_param_changed (MgForm *form, MgParameter *param, GtkDialog *dlg)
{
	gboolean valid;

	valid = mg_form_is_valid (form);

	gtk_dialog_set_response_sensitive (dlg, GTK_RESPONSE_ACCEPT, valid);
}

static void
query_action_cb (GtkButton *button, MgConf *conf)
{
	static MgQuery *query = NULL;
	static gboolean in_conf = TRUE;

	if (!query) 
		query = mg_conf_get_query_by_xml_id (conf, "QU6");

	if (in_conf) {
		g_object_ref (G_OBJECT (query));
		mg_conf_unassume_query (conf, query);
		in_conf = FALSE;
	}
	else {
		mg_conf_assume_query (conf, query);
		g_object_unref (G_OBJECT (query));
		in_conf = TRUE;
	}
	
}

static void
query_field_action_cb (GtkButton *button, MgConf *conf)
{
	static MgQuery *fquery = NULL;
	static gboolean in_conf = TRUE;
	static MgQfield *field = NULL;

	if (!field) {
		fquery = mg_conf_get_query_by_xml_id (conf, "QU14");
		field = MG_QFIELD (mg_entity_get_field_by_xml_id (MG_ENTITY (fquery), "QU14:QF16"));
	}

	if (in_conf) {
		in_conf = FALSE;
		g_object_ref (G_OBJECT (field));
		mg_entity_remove_field (MG_ENTITY (fquery), MG_FIELD (field));
	}
	else {
		in_conf = TRUE;
		mg_entity_add_field (MG_ENTITY (fquery), MG_FIELD (field));
		g_object_unref (G_OBJECT (field));
	}
	
}

static void
sync_dbms_cb (GtkButton *button, MgConf *conf)
{
	GError *error=NULL;
	MgServer *srv = mg_conf_get_server (conf);
	MgDatabase *db = mg_conf_get_database (conf);
	
	if (! mg_server_conn_is_opened (srv)) {
		GError *error = NULL;
		/* Opening the connection */
		mg_server_set_datasource (srv, source);
		mg_server_set_user_name (srv, user);
		mg_server_set_user_password (srv, "");
		
		if (!mg_server_open_connect (srv, &error)) {
			g_print ("CONNECTION ERROR: %s\n", error->message);
			g_error_free (error);
			error = NULL;
		}
	}

	if (!mg_server_update_dbms_data (srv, &error)) {
		g_print ("SERVER DBMS DATA UPDATE: %s\n", error->message);
		g_error_free (error);
		error = NULL;
	}
	else
		g_print ("Dbms SRV data loaded.\n");

	if (!mg_database_update_dbms_data (db, &error)) {
		g_print ("DATABASE DBMS DATA UPDATE: %s\n", error->message);
		g_error_free (error);
		error = NULL;
	}
	else
		g_print ("Dbms DATABASE data loaded.\n");
				
}

static void 
dt_type_delete_cb (GtkButton *button, MgConf *conf)
{
	MgServerDataType *dt;
	gchar *strdt;

	strdt = g_object_get_data (G_OBJECT (button), "dt");

	dt = mg_server_get_data_type_by_name (mg_conf_get_server (conf), strdt);
	g_print ("MgServerDataType for '%s' is %p\n", strdt, dt);

	if (dt) {
		g_print ("Calling mg_base_nullify on %p\n", dt); 
		/*g_signal_emit_by_name (G_OBJECT (mg_conf_get_server (conf)), "data_update_started");*/
		mg_base_nullify (MG_BASE (dt));
		/*g_signal_emit_by_name (G_OBJECT (mg_conf_get_server (conf)), "data_update_finished");*/
	}
	else 
		g_print ("ERROR: can't find data type '%s'\n", strdt);
}

static void
stop_sync_dbms_cb (GtkButton *button, MgConf *conf)
{
	MgServer *srv = mg_conf_get_server (conf);
	mg_server_stop_update_dbms_data (srv);
}


static void
selector_selection_changed_cb (MgSelector *mgsel, GObject *selection, gpointer data)
{
	GObject *obj;
	obj = mg_selector_get_selected_object (mgsel);
	g_print ("SELECTION: %p, fetched: %p\n", selection, obj);
}

/*
 * Menu
 */
GtkWidget *
build_menu (GtkWidget *mainwin)
{
	GtkWidget *menubar1, *menuitem1, *menuitem1_menu, *quit1;
	GtkAccelGroup *accel_group;

	accel_group = gtk_accel_group_new ();

	menubar1 = gtk_menu_bar_new ();
	gtk_widget_show (menubar1);

	menuitem1 = gtk_menu_item_new_with_mnemonic (_("_Fichier"));
	gtk_widget_show (menuitem1);
	gtk_container_add (GTK_CONTAINER (menubar1), menuitem1);
	
	menuitem1_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem1), menuitem1_menu);
	
	quit1 = gtk_image_menu_item_new_from_stock ("gtk-quit", accel_group);
	gtk_widget_show (quit1);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), quit1);

	g_signal_connect ((gpointer) quit1, "activate",
			  G_CALLBACK (destroy), NULL);

	gtk_window_add_accel_group (GTK_WINDOW (mainwin), accel_group);

	return menubar1;
}
