#include <libmergeant/libmergeant.h>
#include <gtk/gtk.h>

static const gchar *my_gda_type_to_string (GdaValueType type)
{
	switch (type) {
        case GDA_VALUE_TYPE_NULL : return "Null";
        case GDA_VALUE_TYPE_BIGINT : return "Bigint";
        case GDA_VALUE_TYPE_BINARY : return "Binary";
        case GDA_VALUE_TYPE_BOOLEAN : return "Boolean";
        case GDA_VALUE_TYPE_DATE : return "Date";
        case GDA_VALUE_TYPE_DOUBLE : return "Double";
        case GDA_VALUE_TYPE_GEOMETRIC_POINT : return "Point";
	case GDA_VALUE_TYPE_GOBJECT : return "GObject";
        case GDA_VALUE_TYPE_INTEGER : return "Integer";
        case GDA_VALUE_TYPE_LIST : return "List";
        case GDA_VALUE_TYPE_MONEY : return "Money";
        case GDA_VALUE_TYPE_NUMERIC : return "Numeric";
        case GDA_VALUE_TYPE_SINGLE : return "Single";
        case GDA_VALUE_TYPE_SMALLINT : return "Smallint";
        case GDA_VALUE_TYPE_STRING : return "String";
        case GDA_VALUE_TYPE_TIME : return "Time";
        case GDA_VALUE_TYPE_TIMESTAMP : return "Timestamp";
        case GDA_VALUE_TYPE_TINYINT : return "Tinyint";
        case GDA_VALUE_TYPE_TYPE : return "GdaType";
        default:
		break;
        }

        return "???";
}

static gboolean delete_event( GtkWidget *widget,
                              GdkEvent  *event,
                              gpointer   data )
{
    g_print ("Leaving test...\n");

    return FALSE;
}

static void destroy( GtkWidget *widget,
                     gpointer   data )
{
    gtk_main_quit ();
}

GtkWidget *build_menu (GtkWidget *mainwin, GtkWidget *top_nb);
GtkWidget *build_main (MgConf *conf, MgDataHandler *dh, GdaValueType type);


int 
main (int argc, char **argv)
{
	GtkWidget *mainwin, *vbox, *wid, *nb, *label, *menu, *top_nb;
	MgConf *conf;
	MgServer *srv;
	GSList *plugins, *list;
	GdaValueType type;
	MgDataHandler *dh;

	/* Initialize i18n support */
	gtk_set_locale ();
	
	/* Initialize the widget set */
	gtk_init (&argc, &argv);

	/* tested objects */
	conf = MG_CONF (mg_conf_new ());

	/* Create the main window */
	mainwin = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width (GTK_CONTAINER (mainwin), 0);
	g_signal_connect (G_OBJECT (mainwin), "delete_event",
			  G_CALLBACK (delete_event), NULL);
	g_signal_connect (G_OBJECT (mainwin), "destroy",
			  G_CALLBACK (destroy), NULL);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (mainwin), vbox);
	gtk_widget_show (vbox);

	/* top notebook */
	top_nb = gtk_notebook_new ();
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (top_nb), FALSE);
	gtk_widget_show (top_nb);
	menu = build_menu (mainwin, top_nb);
	gtk_widget_show (menu);
	gtk_box_pack_start (GTK_BOX (vbox), menu, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), top_nb, TRUE, TRUE, 0);



	/*
	 * Default handlers
	 */
	vbox = gtk_vbox_new (FALSE, 10);
	gtk_notebook_append_page (GTK_NOTEBOOK (top_nb), vbox, NULL);
	gtk_widget_show (vbox);

	label = gtk_label_new("");
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_label_set_markup (GTK_LABEL (label), "<b>Default data handlers:</b>\n"
			      "This test displays MgDataEntry widgets and helpers to test "
			      "them in pages of a notebook. Each page presents the default "
			      "data handler for the corresponding gda type.");
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 5);
	gtk_widget_show (label);

	nb = gtk_notebook_new ();
	gtk_notebook_set_tab_pos (GTK_NOTEBOOK (nb), GTK_POS_LEFT);
	gtk_box_pack_start (GTK_BOX (vbox), nb, TRUE, TRUE, 0);
	gtk_widget_show (nb);

	/* populating the notebook */
	srv = mg_conf_get_server (conf);
	for (type = GDA_VALUE_TYPE_BIGINT; type < GDA_VALUE_TYPE_TYPE; type ++) {
		dh = mg_server_get_handler_by_gda (srv, type);
		wid = build_main (conf, dh, type);
		gtk_widget_show (wid);
		label = gtk_label_new (my_gda_type_to_string (type));
		gtk_notebook_append_page (GTK_NOTEBOOK (nb), wid, label);
	}


	/*
	 * Plugins page 
	 */
	vbox = gtk_vbox_new (FALSE, 10);
	gtk_notebook_append_page (GTK_NOTEBOOK (top_nb), vbox, NULL);
	gtk_widget_show (vbox);

	label = gtk_label_new("");
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_label_set_markup (GTK_LABEL (label), "<b>Plugin data handlers:</b>\n"
			      "This test displays MgDataEntry widgets and helpers to test "
			      "them in pages of a notebook. Each page tests a plugin for a given "
			      "gda type (there can be several pages for a plugin if the plugin "
			      "data handler accepts more than one gda type.");
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 5);
	gtk_widget_show (label);

	nb = gtk_notebook_new ();
	gtk_notebook_set_tab_pos (GTK_NOTEBOOK (nb), GTK_POS_LEFT);
	gtk_box_pack_start (GTK_BOX (vbox), nb, TRUE, TRUE, 0);
	gtk_widget_show (nb);

	/* populating the notebook */
	srv = mg_conf_get_server (conf);
	plugins = mg_server_get_plugin_handlers (srv);
	list = plugins;
	while (list) {
		gint i, nbt;
		gchar *str;

		dh = MG_DATA_HANDLER (list->data);
		nbt = mg_data_handler_get_nb_gda_types (dh);
		for (i=0; i<nbt; i++) {
			type = mg_data_handler_get_gda_type_index (dh, i);
			wid = build_main (conf, dh, type);
			gtk_widget_show (wid);
			str = g_strdup_printf ("%s\n(%s)", mg_data_handler_get_plugin_name (dh),
					       my_gda_type_to_string (type));
			label = gtk_label_new (str);
			gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
			g_free (str);
			gtk_notebook_append_page (GTK_NOTEBOOK (nb), wid, label);
		}
		list = g_slist_next (list);
	}
	g_slist_free (plugins);
		




	/* Show the application window */
	gtk_widget_show (mainwin);
	
	gtk_main ();
	g_object_unref (G_OBJECT (conf));

	return 0;
}

void entry_contents_modified (GtkWidget *entry, gpointer data);
void null_toggled_cb (GtkToggleButton *button, GtkWidget *entry);
void default_toggled_cb (GtkToggleButton *button, GtkWidget *entry);
void actions_toggled_cb (GtkToggleButton *button, GtkWidget *entry);
void orig_clicked_cb (GtkButton *button, GtkWidget *entry);
void default_clicked_cb (GtkButton *button, GtkWidget *entry);
GtkWidget *
build_main (MgConf *conf, MgDataHandler *dh, GdaValueType type)
{
	MgServer *srv;
	GtkWidget *wid, *label, *table, *button, *bbox, *entry, *vbox;
	GString *string;
	
	vbox = gtk_vbox_new (FALSE, 0);

	srv = mg_conf_get_server (conf);
	table = gtk_table_new (7, 2, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 5);
	gtk_table_set_row_spacings (GTK_TABLE (table), 5);
	gtk_box_pack_start (GTK_BOX (vbox), table, FALSE, FALSE, 0);

	/* explain top label */
	string = g_string_new ("");
	g_string_append_printf (string, "<b>Description:</b> %s\n",
				mg_data_handler_get_descr (dh));
	g_string_append_printf (string, "<b>Details:</b> %s\n",
				mg_data_handler_get_descr_detail (dh));
	g_string_append_printf (string, "<b>Version:</b> %s\n",
				mg_data_handler_get_version (dh));
	g_string_append_printf (string, "<b>Key:</b> %s\n",
				mg_data_handler_get_key (dh));
	if (mg_data_handler_is_plugin (dh)) {
		g_string_append_printf (string, "<b>Plugin Name:</b> %s\n"
					"<b>Plugin File:</b> %s\n",
					mg_data_handler_get_plugin_name (dh),
					mg_data_handler_get_plugin_file (dh));
	}
	label = gtk_label_new ("");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_label_set_markup (GTK_LABEL (label), string->str);
	g_string_free (string, TRUE);
	gtk_table_attach (GTK_TABLE (table), label, 0, 2, 0, 1, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	gtk_widget_show (label);

	/* widget being tested */
	wid = GTK_WIDGET (mg_data_handler_get_entry_from_value (dh, NULL, type));
	if (mg_data_entry_expand_in_layout (MG_DATA_ENTRY (wid)))
		gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 2, 1, 2);
	else
		gtk_table_attach (GTK_TABLE (table), wid, 0, 2, 1, 2, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	gtk_widget_show (wid);

	/* Other widgets */
	label = gtk_label_new (_("Current flags: "));
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3, GTK_FILL, 0, 0, 0);
	gtk_widget_show (label);	

	label = gtk_label_new (_("--"));
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 1, 2, 2, 3);
	g_object_set_data (G_OBJECT (wid), "flags", label);
	gtk_widget_show (label);

	label = gtk_label_new (_("Current value: "));
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 3, 4, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (label);
	
	label = gtk_label_new ("");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 1, 2, 3, 4);
	g_object_set_data (G_OBJECT (wid), "value", label);
	g_signal_connect (G_OBJECT (wid), "contents_modified",
			  G_CALLBACK (entry_contents_modified), NULL);
	gtk_widget_show (label);

	entry_contents_modified (wid, GTK_LABEL (label));

	bbox = gtk_hbutton_box_new ();
	gtk_table_attach (GTK_TABLE (table), bbox, 0, 2, 4, 5, 0, 0, 0, 0);
		
	button = gtk_toggle_button_new_with_label (_("NULL ok"));
	g_signal_connect (G_OBJECT (button), "toggled",
			  G_CALLBACK (null_toggled_cb), wid);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button),
				      mg_data_entry_get_attributes (MG_DATA_ENTRY (wid)) & 
				      MG_DATA_ENTRY_CAN_BE_NULL);

	button = gtk_toggle_button_new_with_label (_("DEFAULT ok"));
	g_signal_connect (G_OBJECT (button), "toggled",
			  G_CALLBACK (default_toggled_cb), wid);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button),
				      mg_data_entry_get_attributes (MG_DATA_ENTRY (wid)) & 
				      MG_DATA_ENTRY_CAN_BE_DEFAULT);

	button = gtk_toggle_button_new_with_label (_("Actions?"));
	g_signal_connect (G_OBJECT (button), "toggled",
			  G_CALLBACK (actions_toggled_cb), wid);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button),
				      mg_data_entry_get_attributes (MG_DATA_ENTRY (wid)) & 
				      MG_DATA_ENTRY_ACTIONS_SHOWN);
	gtk_widget_show_all (bbox);

	/* to set the original value */
	entry = GTK_WIDGET (mg_data_handler_get_entry_from_value (dh, NULL, type));
	gtk_table_attach (GTK_TABLE (table), entry, 0, 1, 5, 6, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	gtk_widget_show (entry);
	button = gtk_button_new_with_label (_("Set as original"));
	g_object_set_data (G_OBJECT (button), "entry", entry);
	gtk_table_attach (GTK_TABLE (table), button, 1, 2, 5, 6, 0, 0, 0, 0);
	gtk_widget_show (button);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (orig_clicked_cb), wid);

	/* to set the default value */
	entry = GTK_WIDGET (mg_data_handler_get_entry_from_value (dh, NULL, type));
	gtk_table_attach (GTK_TABLE (table), entry, 0, 1, 6, 7, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	gtk_widget_show (entry);
	button = gtk_button_new_with_label (_("Set as default"));
	g_object_set_data (G_OBJECT (button), "entry", entry);
	gtk_table_attach (GTK_TABLE (table), button, 1, 2, 6, 7, 0, 0, 0, 0);
	gtk_widget_show (button);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (default_clicked_cb), wid);

	gtk_widget_show (table);

	label = gtk_label_new("");
	gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);
	gtk_widget_show (label);

	gtk_widget_show (vbox);

	return vbox;
}

void 
null_toggled_cb (GtkToggleButton *button, GtkWidget *entry)
{
	guint action = 0;
	if (gtk_toggle_button_get_active (button))
		action = MG_DATA_ENTRY_CAN_BE_NULL;

	mg_data_entry_set_attributes (MG_DATA_ENTRY (entry),
				      action, MG_DATA_ENTRY_CAN_BE_NULL);
	gtk_toggle_button_set_active (button,
				      mg_data_entry_get_attributes (MG_DATA_ENTRY (entry)) & 
				      MG_DATA_ENTRY_CAN_BE_NULL);
}

void
default_toggled_cb (GtkToggleButton *button, GtkWidget *entry)
{
	guint action = 0;
	if (gtk_toggle_button_get_active (button))
		action = MG_DATA_ENTRY_CAN_BE_DEFAULT;

	mg_data_entry_set_attributes (MG_DATA_ENTRY (entry),
				      action, MG_DATA_ENTRY_CAN_BE_DEFAULT);
	gtk_toggle_button_set_active (button,
				      mg_data_entry_get_attributes (MG_DATA_ENTRY (entry)) & 
				      MG_DATA_ENTRY_CAN_BE_DEFAULT);
}

void
actions_toggled_cb (GtkToggleButton *button, GtkWidget *entry)
{
	guint action = 0;
	if (gtk_toggle_button_get_active (button))
		action = MG_DATA_ENTRY_ACTIONS_SHOWN;

	mg_data_entry_set_attributes (MG_DATA_ENTRY (entry),
				      action, MG_DATA_ENTRY_ACTIONS_SHOWN);
	gtk_toggle_button_set_active (button,
				      mg_data_entry_get_attributes (MG_DATA_ENTRY (entry)) & 
				      MG_DATA_ENTRY_ACTIONS_SHOWN);
}

void 
entry_contents_modified (GtkWidget *entry, gpointer data)
{
	guint attrs;
	GtkLabel *label;
	GString *str = g_string_new ("");
	gchar *strval;
	GdaValue *value;
	MgDataHandler *dh;

	/* flags */
	label = g_object_get_data (G_OBJECT (entry), "flags");
	attrs = mg_data_entry_get_attributes (MG_DATA_ENTRY (entry));
	if (attrs & MG_DATA_ENTRY_IS_NULL)
		g_string_append (str, "N ");
	if (attrs & MG_DATA_ENTRY_IS_DEFAULT)
		g_string_append (str, "D ");
	if (!(attrs & MG_DATA_ENTRY_IS_UNCHANGED))
		g_string_append (str, "M ");
	if (attrs & MG_DATA_ENTRY_DATA_NON_VALID)
		g_string_append (str, "U ");

	gtk_label_set_text (label, str->str);
	g_string_free (str, TRUE);

	/* value */
	label = g_object_get_data (G_OBJECT (entry), "value");
	value = mg_data_entry_get_value (MG_DATA_ENTRY (entry));
	dh = mg_data_entry_get_handler (MG_DATA_ENTRY (entry));
	strval = mg_data_handler_get_sql_from_value (dh, value);
	gda_value_free (value);
	gtk_label_set_text (label, strval);
	g_free (strval);
}

void
orig_clicked_cb (GtkButton *button, GtkWidget *entry)
{
	GtkWidget *wid;
	GdaValue *value;

	wid = g_object_get_data (G_OBJECT (button), "entry");
	value = mg_data_entry_get_value (MG_DATA_ENTRY (wid));
	g_print ("Setting to original Value %p ", value);
	if (value) {
		if (gda_value_get_type (value) == GDA_VALUE_TYPE_NULL)
			g_print ("GDA_VALUE_TYPE_NULL\n");
		else
			g_print ("type = %d\n", gda_value_get_type (value));
	}
	
	mg_data_entry_set_value_orig (MG_DATA_ENTRY (entry), value);
	gda_value_free (value);
}

void
default_clicked_cb (GtkButton *button, GtkWidget *entry)
{
	GtkWidget *wid;
	GdaValue *value;

	wid = g_object_get_data (G_OBJECT (button), "entry");
	value = mg_data_entry_get_value (MG_DATA_ENTRY (wid));
	mg_data_entry_set_value_default (MG_DATA_ENTRY (entry), value);
	gda_value_free (value);
}



/*
 * Menu
 */
static void on_default_handlers1_activate (GtkMenuItem *item, GtkWidget *top_nb);
static void on_plugins1_activate (GtkMenuItem *item, GtkWidget *top_nb);
GtkWidget *
build_menu (GtkWidget *mainwin, GtkWidget *top_nb)
{
	GtkWidget *menubar1, *menuitem1, *menuitem1_menu, *quit1, *menuitem2, *menuitem2_menu;
	GtkWidget *default_handlers1, *plugins1;
	GtkAccelGroup *accel_group;

	accel_group = gtk_accel_group_new ();

	menubar1 = gtk_menu_bar_new ();
	gtk_widget_show (menubar1);

	menuitem1 = gtk_menu_item_new_with_mnemonic (_("_Fichier"));
	gtk_widget_show (menuitem1);
	gtk_container_add (GTK_CONTAINER (menubar1), menuitem1);
	
	menuitem1_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem1), menuitem1_menu);
	
	quit1 = gtk_image_menu_item_new_from_stock ("gtk-quit", accel_group);
	gtk_widget_show (quit1);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), quit1);

	menuitem2 = gtk_menu_item_new_with_mnemonic (_("_Page"));
	gtk_widget_show (menuitem2);
	gtk_container_add (GTK_CONTAINER (menubar1), menuitem2);
	
	menuitem2_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem2), menuitem2_menu);
	
	default_handlers1 = gtk_menu_item_new_with_mnemonic (_("Default handlers"));
	gtk_widget_show (default_handlers1);
	gtk_container_add (GTK_CONTAINER (menuitem2_menu), default_handlers1);
	
	plugins1 = gtk_menu_item_new_with_mnemonic (_("Plugins"));
	gtk_widget_show (plugins1);
	gtk_container_add (GTK_CONTAINER (menuitem2_menu), plugins1);
	
	g_signal_connect ((gpointer) quit1, "activate",
			  G_CALLBACK (destroy), NULL);
	g_signal_connect ((gpointer) default_handlers1, "activate",
			  G_CALLBACK (on_default_handlers1_activate), top_nb);
	g_signal_connect ((gpointer) plugins1, "activate",
			  G_CALLBACK (on_plugins1_activate), top_nb);

	gtk_window_add_accel_group (GTK_WINDOW (mainwin), accel_group);

	return menubar1;
}

static void
on_default_handlers1_activate (GtkMenuItem *item, GtkWidget *top_nb)
{
	gtk_notebook_set_current_page (GTK_NOTEBOOK (top_nb), 0);
}

static void
on_plugins1_activate (GtkMenuItem *item, GtkWidget *top_nb)
{
	gtk_notebook_set_current_page (GTK_NOTEBOOK (top_nb), 1);
}
