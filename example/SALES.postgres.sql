--
-- PostgreSQL database dump
--

SET search_path = public, pg_catalog;

--
-- TOC entry 2 (OID 521424)
-- Name: salesrep; Type: TABLE; Schema: public
--

CREATE TABLE salesrep (
    id serial NOT NULL,
    name character varying(30) NOT NULL,
    year_salary double precision NOT NULL,
    date_empl date NOT NULL
);


--
-- TOC entry 3 (OID 521429)
-- Name: customers; Type: TABLE; Schema: public
--

CREATE TABLE customers (
    id serial NOT NULL,
    name character varying(35) NOT NULL,
    default_served_by integer,
    country character varying(20),
    city character varying(30)
);


--
-- TOC entry 4 (OID 521434)
-- Name: warehouses; Type: TABLE; Schema: public
--

CREATE TABLE warehouses (
    id serial NOT NULL,
    name character varying(30) NOT NULL,
    country character varying(20) NOT NULL,
    city character varying(30) NOT NULL
);


--
-- TOC entry 5 (OID 521437)
-- Name: products; Type: TABLE; Schema: public
--

CREATE TABLE products (
    ref character varying(15) NOT NULL,
    name character varying(20) NOT NULL,
    price double precision,
    wh_stored integer
);


--
-- TOC entry 6 (OID 521439)
-- Name: locations; Type: TABLE; Schema: public
--

CREATE TABLE locations (
    country character varying(20) NOT NULL,
    city character varying(30) NOT NULL,
    shortcut character varying(50) NOT NULL
);


--
-- TOC entry 7 (OID 521443)
-- Name: orders; Type: TABLE; Schema: public
--

CREATE TABLE orders (
    id serial NOT NULL,
    customer integer NOT NULL,
    creation_date date DEFAULT now() NOT NULL,
    delivery_before date,
    delivery_date date
);


--
-- TOC entry 8 (OID 521447)
-- Name: order_contents; Type: TABLE; Schema: public
--

CREATE TABLE order_contents (
    order_id integer NOT NULL,
    product_ref character varying(15) NOT NULL,
    quantity integer DEFAULT 1 NOT NULL,
    discount double precision DEFAULT 0 NOT NULL
);


--
-- Data for TOC entry 19 (OID 521424)
-- Name: salesrep; Type: TABLE DATA; Schema: public
--

INSERT INTO salesrep VALUES (1, 'Chris Johnson', 20, '2002-10-03');
INSERT INTO salesrep VALUES (2, 'Marc Swayn', 25, '2003-05-08');
INSERT INTO salesrep VALUES (3, 'John Tremor', 23, '2003-09-18');


--
-- Data for TOC entry 20 (OID 521429)
-- Name: customers; Type: TABLE DATA; Schema: public
--

INSERT INTO customers VALUES (2, 'Ed Lamton', 1, NULL, NULL);
INSERT INTO customers VALUES (1, 'Franck Pierce', 2, 'FR', 'TLS');


--
-- Data for TOC entry 21 (OID 521434)
-- Name: warehouses; Type: TABLE DATA; Schema: public
--



--
-- Data for TOC entry 22 (OID 521437)
-- Name: products; Type: TABLE DATA; Schema: public
--



--
-- Data for TOC entry 23 (OID 521439)
-- Name: locations; Type: TABLE DATA; Schema: public
--

INSERT INTO locations VALUES ('FR', 'TLS', 'Toulouse, France');
INSERT INTO locations VALUES ('SP', 'MDR', 'Madrid, Spain');


--
-- Data for TOC entry 24 (OID 521443)
-- Name: orders; Type: TABLE DATA; Schema: public
--



--
-- Data for TOC entry 25 (OID 521447)
-- Name: order_contents; Type: TABLE DATA; Schema: public
--



--
-- TOC entry 13 (OID 521455)
-- Name: salesrep_pkey; Type: CONSTRAINT; Schema: public
--

ALTER TABLE ONLY salesrep
    ADD CONSTRAINT salesrep_pkey PRIMARY KEY (id);


--
-- TOC entry 14 (OID 521457)
-- Name: customers_pkey; Type: CONSTRAINT; Schema: public
--

ALTER TABLE ONLY customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (id);


--
-- TOC entry 26 (OID 521459)
-- Name: $1; Type: CONSTRAINT; Schema: public
--

ALTER TABLE ONLY customers
    ADD CONSTRAINT "$1" FOREIGN KEY (default_served_by) REFERENCES salesrep(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 15 (OID 521463)
-- Name: warehouses_pkey; Type: CONSTRAINT; Schema: public
--

ALTER TABLE ONLY warehouses
    ADD CONSTRAINT warehouses_pkey PRIMARY KEY (id);


--
-- TOC entry 16 (OID 521465)
-- Name: products_pkey; Type: CONSTRAINT; Schema: public
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (ref);


--
-- TOC entry 29 (OID 521467)
-- Name: $1; Type: CONSTRAINT; Schema: public
--

ALTER TABLE ONLY products
    ADD CONSTRAINT "$1" FOREIGN KEY (wh_stored) REFERENCES warehouses(id) ON UPDATE CASCADE ON DELETE NO ACTION;


--
-- TOC entry 17 (OID 521471)
-- Name: locations_pkey; Type: CONSTRAINT; Schema: public
--

ALTER TABLE ONLY locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (country, city);


--
-- TOC entry 27 (OID 521473)
-- Name: customers_locations_fk; Type: CONSTRAINT; Schema: public
--

ALTER TABLE ONLY customers
    ADD CONSTRAINT customers_locations_fk FOREIGN KEY (country, city) REFERENCES locations(country, city) ON UPDATE NO ACTION ON DELETE NO ACTION;


--
-- TOC entry 28 (OID 521477)
-- Name: warehouses_locations_fk; Type: CONSTRAINT; Schema: public
--

ALTER TABLE ONLY warehouses
    ADD CONSTRAINT warehouses_locations_fk FOREIGN KEY (country, city) REFERENCES locations(country, city) ON UPDATE NO ACTION ON DELETE NO ACTION;


--
-- TOC entry 18 (OID 521481)
-- Name: orders_pkey; Type: CONSTRAINT; Schema: public
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- TOC entry 30 (OID 521483)
-- Name: $1; Type: CONSTRAINT; Schema: public
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT "$1" FOREIGN KEY (customer) REFERENCES customers(id) ON UPDATE CASCADE ON DELETE NO ACTION;


--
-- TOC entry 31 (OID 521487)
-- Name: $1; Type: CONSTRAINT; Schema: public
--

ALTER TABLE ONLY order_contents
    ADD CONSTRAINT "$1" FOREIGN KEY (order_id) REFERENCES orders(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 32 (OID 521491)
-- Name: $2; Type: CONSTRAINT; Schema: public
--

ALTER TABLE ONLY order_contents
    ADD CONSTRAINT "$2" FOREIGN KEY (product_ref) REFERENCES products(ref) ON UPDATE CASCADE ON DELETE NO ACTION;


--
-- TOC entry 9 (OID 521422)
-- Name: salesrep_id_seq; Type: SEQUENCE SET; Schema: public
--

SELECT pg_catalog.setval ('salesrep_id_seq', 3, true);


--
-- TOC entry 10 (OID 521427)
-- Name: customers_id_seq; Type: SEQUENCE SET; Schema: public
--

SELECT pg_catalog.setval ('customers_id_seq', 2, true);


--
-- TOC entry 11 (OID 521432)
-- Name: warehouses_id_seq; Type: SEQUENCE SET; Schema: public
--

SELECT pg_catalog.setval ('warehouses_id_seq', 1, false);


--
-- TOC entry 12 (OID 521441)
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public
--

SELECT pg_catalog.setval ('orders_id_seq', 1, false);


