# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#: libmergeant/handlers/mg-handler-boolean.c:139
#: libmergeant/handlers/mg-handler-none.c:137
#: libmergeant/handlers/mg-handler-numerical.c:139
#: libmergeant/handlers/mg-handler-string.c:139
#: libmergeant/handlers/mg-handler-time.c:167 testing/mg-test-handlers.c:255
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2003-09-14 18:07+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: extra/mg-db-browser.c:100
msgid "Tables and Views"
msgstr ""

#: extra/mg-db-browser.c:105
msgid "Data types"
msgstr ""

#: extra/mg-db-browser.c:110 libmergeant/mg-selector.c:2845
msgid "Queries"
msgstr ""

#: extra/mg-db-browser.c:142
msgid "No File"
msgstr ""

#: extra/mg-db-browser.c:160 extra/mg-verify-file.c:32
#, c-format
msgid ""
"Error loading file '%s':\n"
"%s\n"
msgstr ""

#. File menu
#: extra/mg-db-browser.c:190
msgid "_File"
msgstr ""

#. Database menu
#: extra/mg-db-browser.c:226
msgid "_Database"
msgstr ""

#: extra/mg-db-browser.c:233
msgid "Select datasource"
msgstr ""

#: extra/mg-db-browser.c:240
msgid "Synchronise metadata with DBMS"
msgstr ""

#. Create the selector
#: extra/mg-db-browser.c:260
msgid "Select a file to load"
msgstr ""

#: extra/mg-db-browser.c:281
#, c-format
msgid ""
"Error saving file '%s':\n"
"%s\n"
msgstr ""

#. Create the selector
#: extra/mg-db-browser.c:300
msgid "Select a file to save to"
msgstr ""

#: extra/mg-db-browser.c:324
msgid "Medatata synchronisation"
msgstr ""

#: extra/mg-db-browser.c:342
#, c-format
msgid ""
"Error updating Server metadata '%s':\n"
"%s\n"
msgstr ""

#: extra/mg-db-browser.c:358
#, c-format
msgid ""
"Error updating Database metadata '%s':\n"
"%s\n"
msgstr ""

#: extra/mg-db-browser.c:382
msgid "Connection's configuration"
msgstr ""

#: extra/mg-db-browser.c:402 extra/mg-db-browser.c:995
msgid ""
"This feature is disabled since\n"
"the libgnomedb library  has been compiled without the\n"
"Gnome library support"
msgstr ""

#: extra/mg-db-browser.c:424
#, c-format
msgid ""
"Error opening the connection '%s':\n"
"%s\n"
msgstr ""

#: extra/mg-db-browser.c:461
msgid "Select a data type:"
msgstr ""

#: extra/mg-db-browser.c:473
msgid "Select a mode:"
msgstr ""

#: extra/mg-db-browser.c:478
msgid "Functions returning this data type"
msgstr ""

#: extra/mg-db-browser.c:485
msgid "Functions using this data type"
msgstr ""

#: extra/mg-db-browser.c:492
msgid "Aggs. using this data type"
msgstr ""

#: extra/mg-db-browser.c:498
msgid "Result:"
msgstr ""

#: extra/mg-db-browser.c:511 extra/mg-db-browser.c:574
msgid "Select a data type..."
msgstr ""

#: extra/mg-db-browser.c:683
msgid "Select a table or field:"
msgstr ""

#: extra/mg-db-browser.c:706 extra/mg-db-browser.c:730
msgid "Select a table or a table's field..."
msgstr ""

#: extra/mg-db-browser.c:739
msgid "View: "
msgstr ""

#: extra/mg-db-browser.c:741
msgid "Table: "
msgstr ""

#: extra/mg-db-browser.c:769 extra/mg-db-browser.c:915
msgid "Primary key"
msgstr ""

#: extra/mg-db-browser.c:806 extra/mg-db-browser.c:924
msgid "Foreign key"
msgstr ""

#: extra/mg-db-browser.c:846
msgid "UNIQUE constraint"
msgstr ""

#: extra/mg-db-browser.c:876
msgid "Field: "
msgstr ""

#: extra/mg-db-browser.c:882
msgid "Data type: "
msgstr ""

#: extra/mg-db-browser.c:886
msgid "Description: "
msgstr ""

#: extra/mg-db-browser.c:890
msgid "Length: "
msgstr ""

#: extra/mg-db-browser.c:898
msgid "Scale: "
msgstr ""

#: extra/mg-db-browser.c:907
msgid "NULL allowed: "
msgstr ""

#: extra/mg-db-browser.c:908 libmergeant/handlers/mg-entry-boolean.c:189
msgid "Yes"
msgstr ""

#: extra/mg-db-browser.c:908 extra/mg-db-browser.c:918
#: extra/mg-db-browser.c:927 libmergeant/handlers/mg-entry-boolean.c:196
msgid "No"
msgstr ""

#: extra/mg-db-browser.c:913
msgid "Primary key: "
msgstr ""

#: extra/mg-db-browser.c:916
msgid "Part of primary key"
msgstr ""

#: extra/mg-db-browser.c:922
msgid "Foreign key: "
msgstr ""

#: extra/mg-db-browser.c:925
msgid "Part of foreign key"
msgstr ""

#: extra/mg-db-browser.c:955
msgid "Select a query:"
msgstr ""

#: extra/mg-db-browser.c:970
msgid ""
"Raw query rendered as SQL\n"
"(no parameter taken into account):"
msgstr ""

#: extra/mg-db-browser.c:979
msgid ""
"Query rendered as SQL\n"
"(optionally with parameters):"
msgstr ""

#: extra/mg-db-browser.c:988
msgid "Render as SQL"
msgstr ""

#: extra/mg-db-browser.c:1031 extra/mg-db-browser.c:1085
msgid "Non reported error"
msgstr ""

#: extra/mg-verify-file.c:14
#, c-format
msgid "Usage: %s <filename.xml>\n"
msgstr ""

#: extra/mg-verify-file.c:20
#, c-format
msgid "File '%s' does not exist or can't be read!\n"
msgstr ""

#: extra/mg-verify-file.c:25
#, c-format
msgid "File '%s' is a directory!\n"
msgstr ""

#: extra/mg-verify-file.c:42
msgid "List of queries in this file:\n"
msgstr ""

#: extra/mg-verify-file.c:44
msgid "There is no query in this file.\n"
msgstr ""

#: extra/mg-verify-file.c:49
#, c-format
msgid "------> Query \"%s\" <------\n"
msgstr ""

#: extra/mg-verify-file.c:56
#, c-format
msgid "SQL ERROR: %s\n"
msgstr ""

#: extra/mg-verify-file.c:61
#, c-format
msgid "Query_%02d.dot"
msgstr ""

#: extra/mg-verify-file.c:65
#, c-format
msgid ""
"Could not write graph to '%s' (%s)\n"
"\n"
msgstr ""

#: extra/mg-verify-file.c:70
#, c-format
msgid ""
"Written graph to '%s'\n"
"\n"
msgstr ""

#: libmergeant/mg-form.c:401 libmergeant/mg-form.c:428
#: libmergeant/mg-selector.c:474
msgid "Value"
msgstr ""

#: libmergeant/mg-form.c:840
msgid "Required parameters"
msgstr ""

#: libmergeant/mg-condition.c:675
msgid "Conditions hierarchy error"
msgstr ""

#: libmergeant/mg-condition.c:983
msgid "XML Tag is not <MG_COND>"
msgstr ""

#: libmergeant/mg-condition.c:996
msgid "Wrong 'id' attribute in <MG_COND>"
msgstr ""

#: libmergeant/mg-condition.c:1011
msgid "Wrong 'type' attribute in <MG_COND>"
msgstr ""

#: libmergeant/mg-condition.c:1061
msgid "Missing Id attribute in <MG_COND>"
msgstr ""

#: libmergeant/mg-condition.c:1102
msgid "Condition is not active"
msgstr ""

#: libmergeant/mg-condition.c:1124
msgid "Condition must have two arguments"
msgstr ""

#: libmergeant/mg-condition.c:1134
msgid "Condition must have three arguments"
msgstr ""

#: libmergeant/mg-conf.c:566
#, c-format
msgid "Error writing XML file %s"
msgstr ""

#: libmergeant/mg-conf.c:577
msgid "Can't allocate memory for XML structure."
msgstr ""

#: libmergeant/mg-database.c:572
msgid "Database already contains data"
msgstr ""

#: libmergeant/mg-database.c:579
msgid "XML Tag is not <MG_DATABASE>"
msgstr ""

#: libmergeant/mg-database.c:633
msgid "XML Tag below <MG_TABLES> is not <MG_TABLE>"
msgstr ""

#: libmergeant/mg-database.c:664
msgid "XML Tag below <MG_CONSTRAINTS> is not <MG_CONSTRAINT>"
msgstr ""

#: libmergeant/mg-database.c:893 libmergeant/mg-server.c:1425
msgid "Update already started!"
msgstr ""

#: libmergeant/mg-database.c:901 libmergeant/mg-db-table.c:539
#: libmergeant/mg-server.c:1431
msgid "Connection is not opened!"
msgstr ""

#: libmergeant/mg-database.c:933 libmergeant/mg-server.c:1463
msgid "Update stopped!"
msgstr ""

#: libmergeant/mg-database.c:973
msgid "Can't get list of tables"
msgstr ""

#: libmergeant/mg-database.c:983
msgid "Schema for list of tables is wrong"
msgstr ""

#: libmergeant/mg-db-constraint.c:1206
msgid "Constraint cannot be activated!"
msgstr ""

#: libmergeant/mg-db-constraint.c:1288
msgid "XML Tag is not <MG_CONSTRAINT>"
msgstr ""

#: libmergeant/mg-db-constraint.c:1432
msgid "Missing required attributes for <MG_CONSTRAINT>"
msgstr ""

#: libmergeant/mg-db-constraint.c:1437
#, c-format
msgid "Referenced table (%s) not found"
msgstr ""

#: libmergeant/mg-db-constraint.c:1445
#, c-format
msgid "Referenced field in constraint (%s) not found"
msgstr ""

#: libmergeant/mg-db-field.c:834
msgid "XML Tag is not <MG_FIELD>"
msgstr ""

#: libmergeant/mg-db-field.c:917
msgid "Missing required attributes for <MG_FIELD>"
msgstr ""

#: libmergeant/mg-db-table.c:580
msgid "Schema for list of fields is wrong"
msgstr ""

#: libmergeant/mg-db-table.c:626
msgid "Can't find data type"
msgstr ""

#: libmergeant/mg-db-table.c:1140
msgid "XML Tag is not <MG_TABLE>"
msgstr ""

#: libmergeant/mg-db-table.c:1201
msgid "Missing required attributes for <MG_TABLE>"
msgstr ""

#. adding status items
#: libmergeant/mg-dbms-update-viewer.c:207
msgid "Data types analysis"
msgstr ""

#: libmergeant/mg-dbms-update-viewer.c:208
msgid "Functions analysis"
msgstr ""

#: libmergeant/mg-dbms-update-viewer.c:209
msgid "Aggregates analysis"
msgstr ""

#: libmergeant/mg-dbms-update-viewer.c:211
msgid "Tables analysis"
msgstr ""

#: libmergeant/mg-dbms-update-viewer.c:212
msgid "Database constraints analysis"
msgstr ""

#: libmergeant/mg-dbms-update-viewer.c:213
msgid "Sequences analysis"
msgstr ""

#: libmergeant/mg-join.c:859
msgid "XML Tag is not <MG_JOIN>"
msgstr ""

#: libmergeant/mg-join.c:894
msgid "Problem loading <MG_JOIN>"
msgstr ""

#: libmergeant/mg-parameter.c:712
msgid "A parameter can only get its value within a query"
msgstr ""

#: libmergeant/mg-parameter.c:724
msgid "Parameter: query to limit range is not a selection query"
msgstr ""

#: libmergeant/mg-qf-all.c:632 libmergeant/mg-qf-field.c:826
#: libmergeant/mg-qf-value.c:1133 libmergeant/mg-qf-func.c:708
msgid "XML Tag is not <MG_QF>"
msgstr ""

#: libmergeant/mg-qf-all.c:642 libmergeant/mg-qf-field.c:836
#: libmergeant/mg-qf-value.c:1143
msgid "Wrong type of field in <MG_QF>"
msgstr ""

#: libmergeant/mg-qf-all.c:657 libmergeant/mg-qf-field.c:851
#: libmergeant/mg-qf-value.c:1158 libmergeant/mg-qf-func.c:733
msgid "Wrong 'id' attribute in <MG_QF>"
msgstr ""

#: libmergeant/mg-qf-all.c:695 libmergeant/mg-qf-field.c:912
#: libmergeant/mg-qf-func.c:812
msgid "Missing required attributes for <MG_QF>"
msgstr ""

#: libmergeant/mg-qf-all.c:734 libmergeant/mg-qf-field.c:967
#, c-format
msgid "Can't find target '%s'"
msgstr ""

#: libmergeant/mg-qf-all.c:758 libmergeant/mg-qf-field.c:1002
msgid "Non activated field"
msgstr ""

#: libmergeant/mg-qf-field.c:973 libmergeant/mg-query.c:2763
#, c-format
msgid "Can't find field '%s'"
msgstr ""

#: libmergeant/mg-qf-value.c:828
msgid "A field providing a parameter's value must be visible"
msgstr ""

#: libmergeant/mg-qf-value.c:845
msgid "A query providing a parameter must be a selection query"
msgstr ""

#: libmergeant/mg-qf-value.c:854
#, c-format
msgid "Incompatible field type for a parameter's provider (%s / %s)"
msgstr ""

#: libmergeant/mg-qf-value.c:1258
msgid "Missing required gda_type for <MG_QF>"
msgstr ""

#: libmergeant/mg-qf-value.c:1268
#, c-format
msgid "Value field '%s' does not have a value!"
msgstr ""

#: libmergeant/mg-qf-value.c:1355
msgid "Using invalid parameter data"
msgstr ""

#: libmergeant/mg-qf-value.c:1383 libmergeant/mg-qf-value.c:1392
msgid "<VALUE>"
msgstr ""

#: libmergeant/mg-qf-value.c:1389
msgid "No specified value"
msgstr ""

#: libmergeant/mg-qfield.c:182 libmergeant/mg-qfield.c:205
msgid "Missing 'target' attribute in <MG_QF>"
msgstr ""

#: libmergeant/mg-qfield.c:228
msgid "Missing 'object' attribute in <MG_QF>"
msgstr ""

#: libmergeant/mg-qfield.c:249
#, c-format
msgid "Can't find data type %s for query field"
msgstr ""

#: libmergeant/mg-qfield.c:258
msgid "Missing 'srv_type' attribute for VALUE query field"
msgstr ""

#: libmergeant/mg-qfield.c:275
msgid "Missing Implementation in loading <MG_QF>"
msgstr ""

#: libmergeant/mg-qfield.c:281
msgid "Unknown value for 'type' attribute in <MG_QF>"
msgstr ""

#: libmergeant/mg-query.c:1166
msgid ""
"The query represented by a target must be a sub query of the current query"
msgstr ""

#: libmergeant/mg-query.c:1181
msgid "Queries which update data can only have one target"
msgstr ""

#: libmergeant/mg-query.c:1191
msgid "Aggregation queries can't have any target, only sub queries"
msgstr ""

#: libmergeant/mg-query.c:2613
msgid "XML Tag is not <MG_QUERY>"
msgstr ""

#: libmergeant/mg-query.c:2818
msgid "Problem loading <MG_QUERY>"
msgstr ""

#: libmergeant/mg-query.c:2917
msgid "Can't resolve some references in the query"
msgstr ""

#: libmergeant/mg-query.c:2958
msgid "More than two sub queries for an EXCEPT query"
msgstr ""

#: libmergeant/mg-query.c:2970
msgid "Query without any SQL code"
msgstr ""

#: libmergeant/mg-query.c:3018
msgid "Missing parameters"
msgstr ""

#: libmergeant/mg-query.c:3082
#, c-format
msgid "Query %s is not a selection query"
msgstr ""

#: libmergeant/mg-query.c:3114
msgid "No target defined to apply modifications"
msgstr ""

#: libmergeant/mg-query.c:3122
msgid "More than one target defined to apply modifications"
msgstr ""

#: libmergeant/mg-query.c:3133
#, c-format
msgid "Entity %s is not writable"
msgstr ""

#: libmergeant/mg-query.c:3152
msgid "Modification query field has incompatible type"
msgstr ""

#: libmergeant/mg-query.c:3171
msgid "An insertion query can only have one sub-query"
msgstr ""

#: libmergeant/mg-query.c:3179
msgid "Insertion query fields incompatible with sub query's fields"
msgstr ""

#: libmergeant/mg-query.c:3195
msgid "Insertion query field has incompatible value assignment"
msgstr ""

#: libmergeant/mg-query.c:3208
msgid "Insertion query can't have any condition"
msgstr ""

#: libmergeant/mg-query.c:3223
msgid "Deletion query can't have any visible field"
msgstr ""

#: libmergeant/mg-query.c:3242
msgid "Update query field has incompatible value assignment"
msgstr ""

#: libmergeant/mg-query.c:3269
msgid "An aggregate type (UNION, etc) of query can't have any target"
msgstr ""

#: libmergeant/mg-query.c:3277
msgid "An aggregate type (UNION, etc) of query can't have any condition"
msgstr ""

#: libmergeant/mg-query.c:3617
msgid "Join has no joining condition"
msgstr ""

#: libmergeant/mg-query.c:3769
msgid "Missing values"
msgstr ""

#: libmergeant/mg-query.c:3955
#, c-format
msgid "Query '%s'"
msgstr ""

#: libmergeant/mg-query.c:3957
msgid "Unnamed Query"
msgstr ""

#: libmergeant/mg-selector.c:432
msgid "Name"
msgstr ""

#: libmergeant/mg-selector.c:450
msgid "Owner"
msgstr ""

#: libmergeant/mg-selector.c:454
msgid "Description"
msgstr ""

#: libmergeant/mg-selector.c:458 libmergeant/mg-selector.c:2487
#: libmergeant/mg-selector.c:2723 libmergeant/mg-selector.c:2931
#: libmergeant/mg-selector.c:3200
msgid "Type"
msgstr ""

#: libmergeant/mg-selector.c:462 libmergeant/mg-selector.c:2726
msgid "Length"
msgstr ""

#: libmergeant/mg-selector.c:466 libmergeant/mg-selector.c:2729
msgid "Not NULL?"
msgstr ""

#: libmergeant/mg-selector.c:470 libmergeant/mg-selector.c:2732
msgid "Default value"
msgstr ""

#: libmergeant/mg-selector.c:478
msgid "Kind of field"
msgstr ""

#: libmergeant/mg-selector.c:1895
msgid "Data Types"
msgstr ""

#: libmergeant/mg-selector.c:1996
msgid "Data Type"
msgstr ""

#: libmergeant/mg-selector.c:2043
msgid "Functions"
msgstr ""

#: libmergeant/mg-selector.c:2164
msgid "Function"
msgstr ""

#: libmergeant/mg-selector.c:2211
msgid "Aggregates"
msgstr ""

#: libmergeant/mg-selector.c:2327
msgid "Aggregate"
msgstr ""

#: libmergeant/mg-selector.c:2374
msgid "Tables & views"
msgstr ""

#: libmergeant/mg-selector.c:2484
msgid "Table or View"
msgstr ""

#: libmergeant/mg-selector.c:2532
msgid "View"
msgstr ""

#: libmergeant/mg-selector.c:2534
msgid "Table"
msgstr ""

#: libmergeant/mg-selector.c:2720 libmergeant/mg-selector.c:3197
msgid "Field"
msgstr ""

#: libmergeant/mg-selector.c:2765
msgid "Unknown"
msgstr ""

#: libmergeant/mg-selector.c:2928
msgid "Query"
msgstr ""

#: libmergeant/mg-selector.c:2975
msgid "Query 2<no name>"
msgstr ""

#: libmergeant/mg-selector.c:2986
msgid "Select"
msgstr ""

#: libmergeant/mg-selector.c:2988
msgid "Insert"
msgstr ""

#: libmergeant/mg-selector.c:2990
msgid "Update"
msgstr ""

#: libmergeant/mg-selector.c:2992
msgid "Delete"
msgstr ""

#: libmergeant/mg-selector.c:2994
msgid "Select (union)"
msgstr ""

#: libmergeant/mg-selector.c:2996
msgid "Select (intersection)"
msgstr ""

#: libmergeant/mg-selector.c:2998
msgid "Select (exception)"
msgstr ""

#: libmergeant/mg-selector.c:3000
msgid "SQL text"
msgstr ""

#: libmergeant/mg-selector.c:3014
msgid "entity.*"
msgstr ""

#: libmergeant/mg-selector.c:3016
msgid "entity.field"
msgstr ""

#: libmergeant/mg-selector.c:3019
msgid "parameter"
msgstr ""

#: libmergeant/mg-selector.c:3021
msgid "value"
msgstr ""

#: libmergeant/mg-selector.c:3252
msgid "-"
msgstr ""

#: libmergeant/mg-selector.c:3269
msgid "Field <no name>"
msgstr ""

#: libmergeant/mg-selector.c:3282
msgid "Query <no name>"
msgstr ""

#: libmergeant/mg-server-aggregate.c:395
msgid "XML Tag is not <MG_AGGREGATE>"
msgstr ""

#: libmergeant/mg-server-aggregate.c:445
#, c-format
msgid "Can't find data type for aggregate '%s'"
msgstr ""

#: libmergeant/mg-server-aggregate.c:457
#, c-format
msgid "More than one return type for aggregate '%s'"
msgstr ""

#: libmergeant/mg-server-aggregate.c:468
#, c-format
msgid "More than one argument type for aggregate '%s'"
msgstr ""

#: libmergeant/mg-server-aggregate.c:486
msgid "Missing required attributes for <MG_AGGREGATE>"
msgstr ""

#: libmergeant/mg-server-data-type.c:327
msgid "XML Tag is not <MG_DATATYPE>"
msgstr ""

#: libmergeant/mg-server-data-type.c:370
msgid "Missing required attributes for <MG_DATATYPE>"
msgstr ""

#: libmergeant/mg-server-function.c:406
msgid "XML Tag is not <MG_FUNCTION>"
msgstr ""

#: libmergeant/mg-server-function.c:456
#, c-format
msgid "Can't find data type for function '%s'"
msgstr ""

#: libmergeant/mg-server-function.c:468
#, c-format
msgid "More than one return type for function '%s'"
msgstr ""

#: libmergeant/mg-server-function.c:493
msgid "Missing required attributes for <MG_FUNCTION>"
msgstr ""

#: libmergeant/mg-server.c:692
#, c-format
msgid "Error saving data type %s"
msgstr ""

#: libmergeant/mg-server.c:717
#, c-format
msgid "Error saving function %s"
msgstr ""

#: libmergeant/mg-server.c:741
#, c-format
msgid "Error saving aggregate %s"
msgstr ""

#: libmergeant/mg-server.c:772
msgid "Connection is already opened"
msgstr ""

#: libmergeant/mg-server.c:779
msgid "Server already contains data"
msgstr ""

#: libmergeant/mg-server.c:786
msgid "XML Tag is not <MG_SERVER>"
msgstr ""

#: libmergeant/mg-server.c:1176
#, c-format
msgid "No datasource '%s' defined in your GDA configuration"
msgstr ""

#: libmergeant/mg-server.c:1184
msgid "[LibMergeant]"
msgstr ""

#: libmergeant/mg-server.c:1217
#, c-format
msgid "Could not open the connection to the DBMS for datasource '%s'"
msgstr ""

#: libmergeant/mg-server.c:1325
msgid "Connection is not opened"
msgstr ""

#: libmergeant/mg-server.c:1342
msgid "Unknown mode of operation for this query"
msgstr ""

#: libmergeant/mg-server.c:1360
#, c-format
msgid "Error during execution of this query (%s)"
msgstr ""

#: libmergeant/mg-server.c:1504
msgid "Can't get list of data types"
msgstr ""

#: libmergeant/mg-server.c:1515
msgid "Schema for list of data types is wrong"
msgstr ""

#: libmergeant/mg-server.c:1681
msgid "Can't get list of functions"
msgstr ""

#: libmergeant/mg-server.c:1696
msgid "Schema for list of functions is wrong"
msgstr ""

#: libmergeant/mg-server.c:1950
msgid "Can't get list of aggregates"
msgstr ""

#: libmergeant/mg-server.c:1964
msgid "Schema for list of aggregates is wrong"
msgstr ""

#: libmergeant/mg-target.c:674
msgid "XML Tag is not <MG_TARGET>"
msgstr ""

#: libmergeant/mg-target.c:715
msgid "Error loading data from <MG_TARGET> node"
msgstr ""

#: libmergeant/handlers/mg-entry-boolean.c:203
msgid "Unset"
msgstr ""

#: libmergeant/handlers/mg-entry-combo.c:395
#: libmergeant/handlers/mg-entry-combo.c:409
msgid "No value available"
msgstr ""

#: libmergeant/handlers/mg-entry-combo.c:424
msgid "Connexion not opened"
msgstr ""

#: libmergeant/handlers/mg-entry-none.c:173
msgid ""
"This data cannot be displayed or edited\n"
"because of its data type, a plugin needs to be\n"
"affected to that data type (or develop if none exists!)"
msgstr ""

#. set to NULL item
#: libmergeant/handlers/mg-entry-shell.c:393
msgid "Set to NULL"
msgstr ""

#. default value item
#: libmergeant/handlers/mg-entry-shell.c:405
msgid "Set to default value"
msgstr ""

#. reset to original value item
#: libmergeant/handlers/mg-entry-shell.c:417
msgid "Reset to original value"
msgstr ""

#. button to pop up the calendar
#: libmergeant/handlers/mg-entry-time.c:488
msgid "Choose"
msgstr ""

#. small label
#: libmergeant/handlers/mg-entry-time.c:717
msgid "hh:mm:ss"
msgstr ""

#: libmergeant/handlers/mg-handler-boolean.c:145
msgid "InternalBoolean"
msgstr ""

#: libmergeant/handlers/mg-handler-boolean.c:146
msgid "Booleans representation"
msgstr ""

#: libmergeant/handlers/mg-handler-none.c:140
msgid "InternalNone"
msgstr ""

#: libmergeant/handlers/mg-handler-none.c:141
msgid "Default representation for unhandled GDA data types"
msgstr ""

#: libmergeant/handlers/mg-handler-numerical.c:151
msgid "InternalNumerical"
msgstr ""

#: libmergeant/handlers/mg-handler-numerical.c:152
msgid "Numericals representation"
msgstr ""

#: libmergeant/handlers/mg-handler-string.c:145
msgid "InternalString"
msgstr ""

#: libmergeant/handlers/mg-handler-string.c:146
msgid "Strings representation"
msgstr ""

#: libmergeant/handlers/mg-handler-time.c:192
msgid "InternalTime"
msgstr ""

#: libmergeant/handlers/mg-handler-time.c:193
msgid "Time, Date and TimeStamp representation"
msgstr ""

#: libmergeant/handlers/plugins/mg-entry-cidr.c:273
msgid "Mask settings"
msgstr ""

#: libmergeant/handlers/plugins/mg-entry-cidr.c:279
msgid "Set to host mask"
msgstr ""

#: libmergeant/handlers/plugins/mg-entry-cidr.c:288
#, c-format
msgid "Set to class %c network"
msgstr ""

#: libmergeant/handlers/plugins/mg-handler-cidr.c:152
msgid "Data handler for Postgres 'cidr' and 'inet' data types"
msgstr ""

#: libmergeant/handlers/plugins/mg-handler-cidr.c:161
msgid "IP adresses representation"
msgstr ""

#: libmergeant/handlers/plugins/mg-handler-text.c:145
msgid "Data handler for string data types, presented as a text area"
msgstr ""

#: libmergeant/handlers/plugins/mg-handler-text.c:154
msgid "Text area representation"
msgstr ""

#: libmergeant/handlers/plugins/mg-handler-passmd5.c:145
msgid "Data handler for passwords stored as MD5 strings"
msgstr ""

#: libmergeant/handlers/plugins/mg-handler-passmd5.c:154
msgid "MD5 password handling"
msgstr ""

#: libmergeant/mg-qf-func.c:718
msgid "Wrong type of func in <MG_QF>"
msgstr ""

#: libmergeant/mg-qf-func.c:803
#, c-format
msgid "Wrong number of arguments for function %s"
msgstr ""

#: libmergeant/mg-qf-func.c:877
#, c-format
msgid "Can't find referenced field '%s'"
msgstr ""

#: libmergeant/mg-qf-func.c:891
#, c-format
msgid "Can't find function '%s'"
msgstr ""

#: libmergeant/mg-qf-func.c:952
msgid "Non activated function"
msgstr ""

#: libmergeant/mg-work-form.c:592
msgid "An unknown error occured while executing the query."
msgstr ""

#: libmergeant/mg-work-form.c:729
msgid "No data to be displayed"
msgstr ""

#: libmergeant/mg-work-form.c:1446
#, c-format
msgid ""
"Execute the following insertion query ?\n"
"\n"
"%s"
msgstr ""

#: libmergeant/mg-work-form.c:1450
#, c-format
msgid ""
"Execute the following update query ?\n"
"\n"
"%s"
msgstr ""

#: libmergeant/mg-work-form.c:1454
#, c-format
msgid ""
"Execute the following deletion query ?\n"
"\n"
"%s"
msgstr ""

#: libmergeant/mg-work-form.c:1508
#, c-format
msgid ""
"The following error occured while preparing the query:\n"
"%s"
msgstr ""

#: libmergeant/mg-work-form.c:1513
msgid "An unknown error occured while preparing the query."
msgstr ""

#. Other widgets
#: testing/mg-test-handlers.c:239
msgid "Current flags: "
msgstr ""

#: testing/mg-test-handlers.c:244
msgid "--"
msgstr ""

#: testing/mg-test-handlers.c:250
msgid "Current value: "
msgstr ""

#: testing/mg-test-handlers.c:268
msgid "NULL ok"
msgstr ""

#: testing/mg-test-handlers.c:276
msgid "DEFAULT ok"
msgstr ""

#: testing/mg-test-handlers.c:284
msgid "Actions?"
msgstr ""

#: testing/mg-test-handlers.c:297
msgid "Set as original"
msgstr ""

#: testing/mg-test-handlers.c:308
msgid "Set as default"
msgstr ""

#: testing/mg-test-handlers.c:454 testing/mg-test-selector.c:511
msgid "_Fichier"
msgstr ""

#: testing/mg-test-handlers.c:465
msgid "_Page"
msgstr ""

#: testing/mg-test-handlers.c:472
msgid "Default handlers"
msgstr ""

#: testing/mg-test-handlers.c:476
msgid "Plugins"
msgstr ""
