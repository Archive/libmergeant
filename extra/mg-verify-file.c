#include <config.h>

#include <libmergeant/libmergeant.h>

int 
main (int argc, char **argv)
{
	MgConf *conf;
	gchar *filename;
	GError *error = NULL;
	GSList *list, *lptr;
	
	if (argc <= 1) {
		g_print (_("Usage: %s <filename.xml>\n"), argv[0]);
		exit (1);
	}

	filename = argv[1];
	if (!g_file_test (filename, G_FILE_TEST_EXISTS)) {
		g_print (_("File '%s' does not exist or can't be read!\n"), filename);
		exit (2);
	}

	if (g_file_test (filename, G_FILE_TEST_IS_DIR)) {
		g_print (_("File '%s' is a directory!\n"), filename);
		exit (2);
	}
	
	g_type_init ();
	conf = MG_CONF (mg_conf_new ());
	if (!mg_conf_load_xml_file (conf, filename, &error)) {
		g_print (_("Error loading file '%s':\n%s\n"), filename, error->message);
		g_error_free (error);
		exit (3);
	}

	g_print ("File '%s' has correctly been loaded.\n", filename);

	/* queries listing */
	list = mg_conf_get_queries (conf);
	if (list) 
		g_print (_("List of queries in this file:\n"));
	else
		g_print (_("There is no query in this file.\n"));
	lptr = list;
	while (lptr) {
		MgGraphviz *graph;
		gchar *str;
		g_print (_("------> Query \"%s\" <------\n"), mg_base_get_name (MG_BASE (lptr->data)));
		str = mg_renderer_render_as_sql (MG_RENDERER (lptr->data), NULL, &error);
		if (str) {
			g_print ("%s\n", str);
			g_free (str);
		}
		else {
			g_print (_("SQL ERROR: %s\n"), error->message);
			g_error_free (error);
			error = NULL;
		}
		
		str = g_strdup_printf (_("Query_%02d.dot"), g_slist_position (list, lptr) + 1);
		graph = MG_GRAPHVIZ (mg_graphviz_new (conf));
		mg_graphviz_add_to_graph (graph, G_OBJECT (lptr->data));
		if (!mg_graphviz_save_file (graph, str, &error)) {
			g_print (_("Could not write graph to '%s' (%s)\n\n"), str, error->message);
			g_error_free (error);
			error = NULL;
		}
		else
			g_print (_("Written graph to '%s'\n\n"), str);
		g_free (str);
		
		lptr = g_slist_next (lptr);
	}
	g_slist_free (list);
	
	g_object_unref (G_OBJECT (conf));

	return 0;
}
