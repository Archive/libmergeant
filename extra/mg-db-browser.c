#include <config.h>

#include <libgnomedb/libgnomedb.h>
#include <libmergeant/libmergeant.h>
#include <gtk/gtk.h>


typedef struct {
	/* General section */
	MgConf        *conf;
	gchar         *filename;
	gchar         *dsn;
	gchar         *username;
	gchar         *password;
	GtkWidget     *mainwin;

	/* Menu */
	GtkWidget     *menu_save;
	GtkWidget     *menu_db_selectds;
	GtkWidget     *menu_db_action;

	/* Data types page */
	gint           dt_mode;
	GtkTextBuffer *dt_textbuffer;
	GtkWidget     *dt_selector;

	/* Database page */
	GtkTextBuffer *db_textbuffer;
	GtkWidget     *db_selector;
	GtkWidget     *db_view_contents;

	/* Queries page */
	GtkWidget     *qu_selector;
	GtkWidget     *qu_editor;
	GtkWidget     *qu_exec_button;
	GtkWidget     *qu_modif_button;
} BrowserConfig;

static gboolean conn_open (BrowserConfig *config);
static void set_filename (BrowserConfig *conf, const gchar *filename);
static gboolean delete_event (GtkWidget *widget, GdkEvent  *event, gpointer   data )
{
    g_print ("Leaving DB browser...\n");

    return FALSE;
}

static void destroy (GtkWidget *widget, gpointer   data )
{
    gtk_main_quit ();
}

static GtkWidget *build_menu (BrowserConfig *conf, GtkWidget *mainwin);
static GtkWidget *build_data_types_page (BrowserConfig *conf);
static GtkWidget *build_tables_views_page (BrowserConfig *conf);
static GtkWidget *build_queries_page (BrowserConfig *conf);
static void       load_file (BrowserConfig *conf);
int 
main (int argc, char **argv)
{
	GtkWidget *mainwin, *vbox, *menu, *nb, *label, *page;
	MgConf *conf;
	BrowserConfig *config;
	
	/* Initialize i18n support */
	gtk_set_locale ();
	
	/* Initialize the widget set */
	gtk_init (&argc, &argv);

	/* Browser Configuration */
	config = g_new0 (BrowserConfig, 1);
	conf = MG_CONF (mg_conf_new ());
	config->conf = conf;
	g_object_set (G_OBJECT (mg_conf_get_server (conf)), "with_functions", TRUE, NULL);
	
	/* Create the main window */
	mainwin = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width (GTK_CONTAINER (mainwin), 0);
	g_signal_connect (G_OBJECT (mainwin), "delete_event",
			  G_CALLBACK (delete_event), NULL);
	g_signal_connect (G_OBJECT (mainwin), "destroy",
			  G_CALLBACK (destroy), NULL);
	config->mainwin = mainwin;

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (mainwin), vbox);
	gtk_widget_show (vbox);

	/* menu */
	menu = build_menu (config, mainwin);
	gtk_widget_show (menu);
	gtk_box_pack_start (GTK_BOX (vbox), menu, FALSE, FALSE, 0);

	/* main area */
	nb = gtk_notebook_new ();
	gtk_box_pack_start (GTK_BOX (vbox), nb, TRUE, TRUE, 0);

	/* tables and views page */
	page = build_tables_views_page (config);
	label = gtk_label_new (_("Tables and Views"));
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), page, label);

	/* data types page */
	page = build_data_types_page (config);
	label = gtk_label_new (_("Data types"));
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), page, label);

	/* queries page */
	page = build_queries_page (config);
	label = gtk_label_new (_("Queries"));
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), page, label);

	/* Show the application window */
	gtk_widget_set_size_request (mainwin, 700, 600);
	gtk_widget_show_all (mainwin);

	/* Application init */
	if (argc > 1)
		set_filename (config, argv[1]);

	load_file (config);
	
	gtk_main ();
	g_object_unref (G_OBJECT (conf));

	return 0;
}

static void
set_filename (BrowserConfig *conf, const gchar *filename)
{
	if (conf->filename) {
		g_free (conf->filename);
		conf->filename = NULL;
	}
	if (filename)
		conf->filename = g_strdup (filename);

	if (filename)
		gtk_window_set_title (GTK_WINDOW (conf->mainwin), filename);
	else
		gtk_window_set_title (GTK_WINDOW (conf->mainwin), _("No File"));
}

static void
load_file (BrowserConfig *config)
{
	GError *error = NULL;
	if (!config->filename)
		return;

	mg_server_reset (mg_conf_get_server (config->conf));
	/* actual file loading */
	if (!mg_conf_load_xml_file (config->conf, config->filename, &error)) {
		GtkWidget *msg;

		msg = gtk_message_dialog_new (GTK_WINDOW (config->mainwin), 
					      GTK_DIALOG_DESTROY_WITH_PARENT,
					      GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, 
					      _("Error loading file '%s':\n%s\n"), config->filename,
					      error->message);
		gtk_dialog_run (GTK_DIALOG (msg));
		gtk_widget_destroy (msg);
		g_error_free (error);
		error = NULL;
	}
}


/*
 * Menu
 */
static void open_file_cb (GtkWidget *wid, BrowserConfig *conf);
static void save_file_cb (GtkWidget *wid, BrowserConfig *conf);
static void save_as_file_cb (GtkWidget *wid, BrowserConfig *conf);
static void select_ds_cb (GtkWidget *wid, BrowserConfig *conf);
static void conn_action_cb (GtkWidget *wid, BrowserConfig *conf);
GtkWidget *
build_menu (BrowserConfig *conf, GtkWidget *mainwin)
{
	GtkWidget *menubar1, *menuitem1, *menuitem1_menu, *entry;
	GtkAccelGroup *accel_group;

	accel_group = gtk_accel_group_new ();

	menubar1 = gtk_menu_bar_new ();
	gtk_widget_show (menubar1);

	/* File menu */
	menuitem1 = gtk_menu_item_new_with_mnemonic (_("_File"));
	gtk_widget_show (menuitem1);
	gtk_container_add (GTK_CONTAINER (menubar1), menuitem1);
	
	menuitem1_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem1), menuitem1_menu);
	
	entry = gtk_image_menu_item_new_from_stock (GTK_STOCK_OPEN, accel_group);
	gtk_widget_show (entry);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), entry);

	g_signal_connect (G_OBJECT (entry), "activate",
			  G_CALLBACK (open_file_cb), conf);

	entry = gtk_image_menu_item_new_from_stock (GTK_STOCK_SAVE, accel_group);
	gtk_widget_show (entry);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), entry);

	g_signal_connect (G_OBJECT (entry), "activate",
			  G_CALLBACK (save_file_cb), conf);

	entry = gtk_image_menu_item_new_from_stock (GTK_STOCK_SAVE_AS, accel_group);
	gtk_widget_show (entry);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), entry);

	g_signal_connect (G_OBJECT (entry), "activate",
			  G_CALLBACK (save_as_file_cb), conf);

	entry = gtk_image_menu_item_new_from_stock (GTK_STOCK_QUIT, accel_group);
	gtk_widget_show (entry);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), entry);

	g_signal_connect (G_OBJECT (entry), "activate",
			  G_CALLBACK (destroy), conf);

	/* Database menu */
	menuitem1 = gtk_menu_item_new_with_mnemonic (_("_Database"));
	gtk_widget_show (menuitem1);
	gtk_container_add (GTK_CONTAINER (menubar1), menuitem1);
	
	menuitem1_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem1), menuitem1_menu);
	
	entry = gtk_menu_item_new_with_mnemonic (_("Select datasource"));
	gtk_widget_show (entry);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), entry);

	g_signal_connect (G_OBJECT (entry), "activate",
			  G_CALLBACK (select_ds_cb), conf);

	entry = gtk_menu_item_new_with_mnemonic (_("Synchronise metadata with DBMS"));
	gtk_widget_show (entry);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), entry);

	g_signal_connect (G_OBJECT (entry), "activate",
			  G_CALLBACK (conn_action_cb), conf);


	gtk_window_add_accel_group (GTK_WINDOW (mainwin), accel_group);

	return menubar1;
}

static void
open_file_cb (GtkWidget *wid, BrowserConfig *conf)
{
	GtkWidget *file_selector;
	gint response;

	/* Create the selector */
	file_selector = gtk_file_selection_new (_("Select a file to load"));
	response = gtk_dialog_run (GTK_DIALOG (file_selector));
	if (response == GTK_RESPONSE_OK) {
		set_filename (conf, gtk_file_selection_get_filename (GTK_FILE_SELECTION (file_selector)));
		load_file (conf);
	}
	gtk_widget_destroy (file_selector);
}

static void
save_file_cb (GtkWidget *wid, BrowserConfig *conf)
{
	if (conf->filename) {
		GError *error = NULL;

		if (!mg_conf_save_xml_file (conf->conf, conf->filename, &error)) {
			GtkWidget *msg;
			
			msg = gtk_message_dialog_new (GTK_WINDOW (conf->mainwin), 
						      GTK_DIALOG_DESTROY_WITH_PARENT,
						      GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, 
						      _("Error saving file '%s':\n%s\n"), conf->filename,
						      error->message);
			gtk_dialog_run (GTK_DIALOG (msg));
			gtk_widget_destroy (msg);
			g_error_free (error);
			error = NULL;
		}
	}
	else
		save_as_file_cb (NULL, conf);	
}

static void
save_as_file_cb (GtkWidget *wid, BrowserConfig *conf)
{
	GtkWidget *file_selector;
	gint response;

	/* Create the selector */
	file_selector = gtk_file_selection_new (_("Select a file to save to"));
	response = gtk_dialog_run (GTK_DIALOG (file_selector));
	if (response == GTK_RESPONSE_OK) {
		set_filename (conf, gtk_file_selection_get_filename (GTK_FILE_SELECTION (file_selector)));
		save_file_cb (NULL, conf);
	}
	gtk_widget_destroy (file_selector);	
}


static void stop_dbms_update_cb (GtkWidget *dlg, gint response, BrowserConfig *config);
static void
conn_action_cb (GtkWidget *wid, BrowserConfig *config)
{
	GtkWidget *dlg, *updview;
	MgDatabase *db;
	MgServer *srv;
	GError *error = NULL;

	db = mg_conf_get_database (config->conf);
	srv = mg_conf_get_server (config->conf);
	if (!conn_open (config))
		return;

	dlg = gtk_dialog_new_with_buttons (_("Medatata synchronisation"),
					   GTK_WINDOW (config->mainwin), 
					   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR,
					   GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT, NULL);
	g_signal_connect (G_OBJECT (dlg), "response",
			  G_CALLBACK (stop_dbms_update_cb), config);
	updview = mg_dbms_update_viewer_new (config->conf);
	gtk_widget_show (updview);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG(dlg)->vbox), updview, TRUE, TRUE, 0);
	gtk_widget_show (dlg);
	/* data types, functions and aggregates */
	if (!mg_server_update_dbms_data (srv, &error)) {
		if (error->code != MG_SERVER_META_DATA_UPDATE_USER_STOPPED) {
			GtkWidget *msg;
			
			msg = gtk_message_dialog_new (GTK_WINDOW (config->mainwin), 
						      GTK_DIALOG_DESTROY_WITH_PARENT,
						      GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, 
						      _("Error updating Server metadata '%s':\n%s"), config->filename,
						      error->message);
			gtk_dialog_run (GTK_DIALOG (msg));
			gtk_widget_destroy (msg);
		}
		g_error_free (error);
		error = NULL;	
	}
	/* database */
	if (!mg_database_update_dbms_data (db, &error)) {
		if (error->code != MG_DATABASE_META_DATA_UPDATE_USER_STOPPED) {
			GtkWidget *msg;
			
			msg = gtk_message_dialog_new (GTK_WINDOW (config->mainwin), 
						      GTK_DIALOG_DESTROY_WITH_PARENT,
						      GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, 
						      _("Error updating Database metadata '%s':\n%s"), config->filename,
						      error->message);
			gtk_dialog_run (GTK_DIALOG (msg));
			gtk_widget_destroy (msg);
		}
		g_error_free (error);
		error = NULL;	
	}

	gtk_widget_destroy (dlg);
}

static void
stop_dbms_update_cb (GtkWidget *dlg, gint response, BrowserConfig *config)
{
	mg_server_stop_update_dbms_data (mg_conf_get_server (config->conf));
}

static void
select_ds_cb (GtkWidget *wid, BrowserConfig *conf)
{
#ifdef BUILD_WITH_GNOME
	GtkWidget *props;
                                                                                                                    
        props = gnome_db_login_dialog_new (_("Connection's configuration"));
        if (gnome_db_login_dialog_run (GNOME_DB_LOGIN_DIALOG (props))) {
		MgServer *srv;
		
		srv = mg_conf_get_server (conf->conf);
		mg_server_set_datasource (srv, gnome_db_login_dialog_get_dsn (GNOME_DB_LOGIN_DIALOG (props)));
		if (!gnome_db_login_dialog_get_username (GNOME_DB_LOGIN_DIALOG (props)))
			mg_server_set_user_name (srv, "");
		else
			mg_server_set_user_name (srv, gnome_db_login_dialog_get_username (GNOME_DB_LOGIN_DIALOG (props)));
		mg_server_set_user_password (srv, gnome_db_login_dialog_get_password (GNOME_DB_LOGIN_DIALOG (props)));
		gtk_widget_destroy (props);
		conn_action_cb (NULL, conf);
	}
	else
		gtk_widget_destroy (props);
#else
	GtkWidget *msg;

	msg = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, 
				      _("This feature is disabled since\nthe libgnomedb library  has been compiled without the\n"
					"Gnome library support"));
	gtk_dialog_run (GTK_DIALOG (msg));
	gtk_widget_destroy (msg);
#endif
}


static gboolean 
conn_open (BrowserConfig *config)
{
	MgServer *srv;
	GError *error = NULL;

	srv = mg_conf_get_server (config->conf);
	if (!mg_server_conn_is_opened (srv)) {
		if (!mg_server_open_connect (srv, &error)) {
			GtkWidget *msg;
			
			msg = gtk_message_dialog_new (GTK_WINDOW (config->mainwin), 
						      GTK_DIALOG_DESTROY_WITH_PARENT,
						      GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, 
						      _("Error opening the connection '%s':\n%s\n"), config->filename,
						      error->message);
			gtk_dialog_run (GTK_DIALOG (msg));
			gtk_widget_destroy (msg);
			g_error_free (error);
			error = NULL;	
			return FALSE;
		}
	}

	return TRUE;
}





/*
 * Data types page
 */
static void dt_selection_changed_cb (MgSelector *mgsel, GObject *dt, BrowserConfig *config);
static void dt_mode_changed_cb (GtkWidget *wid, BrowserConfig *config);
static GtkWidget *
build_data_types_page (BrowserConfig *conf)
{
	GtkWidget *wid, *bbox, *label, *paned, *vb;
	GtkWidget *sw, *textview;
	GtkWidget *rd1, *rd2, *rd3;
	GtkTextBuffer *buffer;
	gchar *str;

	paned = gtk_hpaned_new ();
	gtk_container_set_border_width (GTK_CONTAINER (paned), 5);

	/* left part */
	vb = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (vb), 5);
	gtk_widget_set_size_request (vb, 200, 150);
	gtk_paned_add1 (GTK_PANED (paned), vb);

	label = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Select a data type:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);

	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, FALSE, 0);
	wid = mg_selector_new (conf->conf, MG_SELECTOR_DATA_TYPES, 
			       MG_SELECTOR_COLUMN_OWNER | MG_SELECTOR_COLUMN_COMMENTS);
	gtk_box_pack_start (GTK_BOX (vb), wid, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (wid), "selection_changed",
			  G_CALLBACK (dt_selection_changed_cb), conf);
	conf->dt_selector = wid;

	/* right part */
	vb = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (vb), 5);
	gtk_paned_add2 (GTK_PANED (paned), vb);
	label = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Select a filter option:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);

	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, FALSE, 0);
	bbox = gtk_vbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (vb), bbox, FALSE, FALSE, 0);

	rd1 = gtk_radio_button_new_with_label (NULL, _("Functions returning this data type"));
	gtk_box_pack_start (GTK_BOX (bbox), rd1, FALSE, FALSE, 0);
	g_object_set_data (G_OBJECT (rd1), "mode", GINT_TO_POINTER (0));
	g_signal_connect (G_OBJECT (rd1), "toggled",
			  G_CALLBACK (dt_mode_changed_cb), conf);

	rd2 = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (rd1),
							   _("Functions using this data type"));
	gtk_box_pack_start (GTK_BOX (bbox), rd2, FALSE, FALSE, 0);
	g_object_set_data (G_OBJECT (rd2), "mode", GINT_TO_POINTER (1));
	g_signal_connect (G_OBJECT (rd2), "toggled",
			  G_CALLBACK (dt_mode_changed_cb), conf);

	rd3 = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (rd1),
							   _("Aggregates using this data type"));
	gtk_box_pack_start (GTK_BOX (bbox), rd3, FALSE, FALSE, 0);
	g_object_set_data (G_OBJECT (rd3), "mode", GINT_TO_POINTER (2));
	g_signal_connect (G_OBJECT (rd3), "toggled",
			  G_CALLBACK (dt_mode_changed_cb), conf);

	label = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Result of filter:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);

	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, FALSE, 0);
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_box_pack_start (GTK_BOX (vb), sw, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	textview = gtk_text_view_new ();
	gtk_container_add (GTK_CONTAINER (sw), textview);
	gtk_text_view_set_left_margin (GTK_TEXT_VIEW (textview), 5);
	gtk_text_view_set_right_margin (GTK_TEXT_VIEW (textview), 5);
	gtk_text_view_set_editable (GTK_TEXT_VIEW (textview), FALSE);
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview));
	conf->dt_textbuffer = buffer;
	gtk_text_buffer_set_text (buffer, _("Select a data type..."), -1);

	gtk_text_buffer_create_tag (buffer, "funcname",
				    "weight", PANGO_WEIGHT_BOLD,
				    "foreground", "red", NULL);

	gtk_text_buffer_create_tag (buffer, "descr",
				    "style", PANGO_STYLE_ITALIC, NULL);

	return paned;
}

static void
dt_mode_changed_cb (GtkWidget *wid, BrowserConfig *config)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (wid))) {
		config->dt_mode = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (wid), "mode"));
		dt_selection_changed_cb (MG_SELECTOR (config->dt_selector), NULL, config);
	}
}

static gchar *
function_get_args (MgServerFunction *func)
{
	GString *string;
	const GSList *args;
	gchar *retval;
	gboolean firstarg = TRUE;

	string = g_string_new ("");
	args = mg_server_function_get_arg_types (func);
	g_string_append (string, " (");
	while (args) {
		if (firstarg)
			firstarg = FALSE;
		else
			g_string_append (string, ", ");
		g_string_append (string,
				 mg_server_data_type_get_sqlname (MG_SERVER_DATA_TYPE (args->data)));
		args = g_slist_next (args);
	}
	g_string_append (string, ")");
	retval = string->str;
	g_string_free (string, FALSE);

	return retval;
}


static void
dt_selection_changed_cb (MgSelector *mgsel, GObject *dt, BrowserConfig *config)
{
	GtkTextIter start, end;

	/* empty the text buffer */
	gtk_text_buffer_get_start_iter (config->dt_textbuffer, &start);
	gtk_text_buffer_get_end_iter (config->dt_textbuffer, &end);
	gtk_text_buffer_delete (config->dt_textbuffer, &start, &end);

	if (!dt)
		dt = mg_selector_get_selected_object (MG_SELECTOR (config->dt_selector));

	if (!dt)
		gtk_text_buffer_set_text (config->dt_textbuffer, _("Select a data type..."), -1);
	else {
		GSList *list, *funcs, *args, *aggs;
		gboolean getfunc;
		MgServerDataType *ldt;
		
		funcs = mg_server_get_functions (mg_conf_get_server (config->conf));
		aggs = mg_server_get_aggregates (mg_conf_get_server (config->conf));
		list = g_slist_concat (funcs, aggs);
		funcs = list;
		while (list) {
			getfunc = FALSE;
			switch (config->dt_mode) {
			case 0:
				if (IS_MG_SERVER_FUNCTION (list->data)) {
					ldt = mg_server_function_get_ret_type (MG_SERVER_FUNCTION (list->data));
					getfunc = (G_OBJECT (ldt) == dt) ? TRUE : FALSE;
				}
				break;
			case 1:
				if (IS_MG_SERVER_FUNCTION (list->data)) {
					args = mg_server_function_get_arg_types (MG_SERVER_FUNCTION (list->data));
					while (args && !getfunc) {
						if (args->data == (gpointer) dt)
							getfunc = TRUE;
						args = g_slist_next (args);
					}
				}
				break;
			case 2:
				if (IS_MG_SERVER_AGGREGATE (list->data)) {
					ldt = mg_server_aggregate_get_arg_type (MG_SERVER_AGGREGATE (list->data));
					getfunc = !ldt || (G_OBJECT (ldt) == dt) ? TRUE : FALSE;
				}
				break;
			}

			if (getfunc) {
				gchar *str;

				str = mg_base_get_name (MG_BASE (list->data));
				gtk_text_buffer_get_end_iter (config->dt_textbuffer, &end);
				gtk_text_buffer_insert_with_tags_by_name (config->dt_textbuffer, 
									  &end, str, -1, 
									  "funcname", NULL);

				if (IS_MG_SERVER_FUNCTION (list->data))
					str = function_get_args (MG_SERVER_FUNCTION (list->data));
				else {
					if ((ldt = mg_server_aggregate_get_arg_type (MG_SERVER_AGGREGATE (list->data)))) {
						str = g_strdup_printf (" (%s)", mg_base_get_name (MG_BASE (ldt)));
					}
					else
						str = g_strdup (" (*)");
				}
				gtk_text_buffer_get_end_iter (config->dt_textbuffer, &end);
				gtk_text_buffer_insert (config->dt_textbuffer, &end, str, -1);
				g_free (str);

				str = mg_base_get_description (MG_BASE (list->data));
				if (str && *str) {
					gchar *str2 = g_strdup_printf (" -- %s\n", str);
					gtk_text_buffer_get_end_iter (config->dt_textbuffer, &end);
					gtk_text_buffer_insert_with_tags_by_name (config->dt_textbuffer, &end, str2, -1, 
										  "descr", NULL);
					g_free (str2);
				}
				else {
					gtk_text_buffer_get_end_iter (config->dt_textbuffer, &end);
					gtk_text_buffer_insert (config->dt_textbuffer, &end, "\n", -1);
				}
			}
			list = g_slist_next (list);
		}
		g_slist_free (funcs);
	}
}












/**
 * build_tables_views_page
 *
 * Build the Tables and Views page
 */
static void tables_selection_changed_cb (MgSelector *mgsel, GObject *obj, BrowserConfig *config);
static void table_view_contents (GtkWidget *button, BrowserConfig *config);
static GtkWidget *
build_tables_views_page (BrowserConfig *conf)
{
	GtkWidget *wid, *label, *paned, *vb;
	GtkWidget *sw, *textview;
	GtkTextBuffer *buffer;
	gchar *str;

	paned = gtk_hpaned_new ();
	gtk_container_set_border_width (GTK_CONTAINER (paned), 5);

	/* left part */
	vb = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (vb), 5);
	gtk_widget_set_size_request (vb, 200, 150);
	gtk_paned_add1 (GTK_PANED (paned), vb);

	label = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Select a table or field:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, FALSE, 0);

	wid = mg_selector_new (conf->conf, MG_SELECTOR_TABLES | MG_SELECTOR_FIELDS, 
			       MG_SELECTOR_COLUMN_OWNER | MG_SELECTOR_COLUMN_TYPE | MG_SELECTOR_COLUMN_FIELD_LENGTH |
			       MG_SELECTOR_COLUMN_FIELD_NNUL | MG_SELECTOR_COLUMN_FIELD_DEFAULT);

	gtk_box_pack_start (GTK_BOX (vb), wid, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (wid), "selection_changed",
			  G_CALLBACK (tables_selection_changed_cb), conf);
	conf->db_selector = wid;

	/* right part */
	vb = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (vb), 5);
	gtk_paned_add2 (GTK_PANED (paned), vb);

	label = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Table's operations:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, FALSE, 0);

	wid = gtk_button_new_with_label (_("Edit contents"));
	gtk_box_pack_start (GTK_BOX (vb), wid, FALSE, TRUE, 0);
	g_signal_connect (G_OBJECT (wid), "clicked",
			  G_CALLBACK (table_view_contents), conf);
	gtk_widget_set_sensitive (wid, FALSE);
	conf->db_view_contents = wid;

	label = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Table of field's properties:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, FALSE, 0);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_box_pack_start (GTK_BOX (vb), sw, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	textview = gtk_text_view_new ();
	gtk_container_add (GTK_CONTAINER (sw), textview);
	gtk_text_view_set_left_margin (GTK_TEXT_VIEW (textview), 5);
	gtk_text_view_set_right_margin (GTK_TEXT_VIEW (textview), 5);
	gtk_text_view_set_editable (GTK_TEXT_VIEW (textview), FALSE);
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview));
	conf->db_textbuffer = buffer;
	gtk_text_buffer_set_text (buffer, _("Select a table or a table's field..."), -1);

	gtk_text_buffer_create_tag (buffer, "header",
				    "weight", PANGO_WEIGHT_BOLD,
				    "foreground", "red", NULL);

	gtk_text_buffer_create_tag (buffer, "section",
				    "weight", PANGO_WEIGHT_BOLD,
				    "foreground", "blue", NULL);
	
	return paned;
}

static void
tables_selection_changed_cb (MgSelector *mgsel, GObject *obj, BrowserConfig *config)
{
	GtkTextIter start, end, current;

	/* empty the text buffer */
	gtk_text_buffer_get_start_iter (config->db_textbuffer, &start);
	gtk_text_buffer_get_end_iter (config->db_textbuffer, &end);
	gtk_text_buffer_delete (config->db_textbuffer, &start, &end);

	if (!obj)
		gtk_text_buffer_set_text (config->db_textbuffer, _("Select a table or a table's field..."), -1);
	else {
		gtk_text_buffer_get_start_iter (config->db_textbuffer, &current);
		if (IS_MG_DB_TABLE (obj)) {
			gchar *str;
			MgDbTable *table = MG_DB_TABLE (obj);
			GSList *constraints, *list;

			if (mg_db_table_is_view (table))
				str = _("View: ");
			else
				str = _("Table: ");

			gtk_text_buffer_insert_with_tags_by_name (config->db_textbuffer, 
								  &current, str, -1, 
								  "section", NULL);
			gtk_text_buffer_insert (config->db_textbuffer, &current, mg_base_get_name (MG_BASE (obj)), -1);
			gtk_text_buffer_insert (config->db_textbuffer, &current, "\n\n", -1);
			
			/* constraints list */
			constraints = mg_db_table_get_constraints (table);

			/* PKey */
			list = constraints;
			while (list) {
				gboolean first = TRUE;
				if (mg_db_constraint_get_constraint_type (MG_DB_CONSTRAINT (list->data)) == 
				    CONSTRAINT_PRIMARY_KEY) {
					MgDbConstraint *cstr = MG_DB_CONSTRAINT (list->data);
					GSList *fields, *list2;
					gboolean header = FALSE;

					fields = mg_db_constraint_pkey_get_fields (cstr);
					list2 = fields;
					while (list2) {
						if (!header) {
							header = TRUE;
							gtk_text_buffer_insert_with_tags_by_name (config->db_textbuffer,
												  &current, 
												  _("Primary key"), -1,
												  "section", NULL);
							gtk_text_buffer_insert (config->db_textbuffer, &current, "\n", -1);
						}

						if (first) 
							first = FALSE;
						else
							gtk_text_buffer_insert (config->db_textbuffer, &current, ", ", -1);

						gtk_text_buffer_insert (config->db_textbuffer, &current, 
									mg_base_get_name (MG_BASE (list2->data)), -1);
						list2 = g_slist_next (list2);
					}
					g_slist_free (fields);
					gtk_text_buffer_insert (config->db_textbuffer, &current, "\n\n", -1);
				}
				list = g_slist_next (list);
			}

			/* FKey */
			list = constraints;
			while (list) {
				if (mg_db_constraint_get_constraint_type (MG_DB_CONSTRAINT (list->data)) == 
				    CONSTRAINT_FOREIGN_KEY) {
					MgDbConstraint *cstr = MG_DB_CONSTRAINT (list->data);
					GSList *fields, *list2;
					gboolean header = FALSE;

					fields = mg_db_constraint_fkey_get_fields (cstr);
					list2 = fields;
					while (list2) {
						MgEntity *ent;
						if (!header) {
							header = TRUE;
							gtk_text_buffer_insert_with_tags_by_name (config->db_textbuffer,
												  &current, 
												  _("Foreign key"), -1,
												  "section", NULL);
							gtk_text_buffer_insert (config->db_textbuffer, &current, "\n", -1);
						}
						
						str = mg_base_get_name (MG_BASE (MG_DB_CONSTRAINT_FK_PAIR (list2->data)->fkey));
						gtk_text_buffer_insert (config->db_textbuffer, &current, str, -1);
						gtk_text_buffer_insert (config->db_textbuffer, &current, " --> ", -1);
						ent = mg_field_get_entity (MG_FIELD (MG_DB_CONSTRAINT_FK_PAIR (list2->data)->ref_pkey));
						str = mg_base_get_name (MG_BASE (ent));
						gtk_text_buffer_insert (config->db_textbuffer, &current, str, -1);
						gtk_text_buffer_insert (config->db_textbuffer, &current, ".", -1);
						str = mg_base_get_name (MG_BASE (MG_DB_CONSTRAINT_FK_PAIR (list2->data)->ref_pkey));
						gtk_text_buffer_insert (config->db_textbuffer, &current, str, -1);
						gtk_text_buffer_insert (config->db_textbuffer, &current, "\n", -1);
						
						list2 = g_slist_next (list2);
					}
					g_slist_free (fields);
					gtk_text_buffer_insert (config->db_textbuffer, &current, "\n", -1);
				}
				list = g_slist_next (list);
			}

			/* Unique */
			list = constraints;
			while (list) {
				if (mg_db_constraint_get_constraint_type (MG_DB_CONSTRAINT (list->data)) == 
				    CONSTRAINT_UNIQUE) {
					MgDbConstraint *cstr = MG_DB_CONSTRAINT (list->data);
					GSList *fields, *list2;
					gboolean header = FALSE;

					fields = mg_db_constraint_unique_get_fields (cstr);
					list2 = fields;
					while (list2) {
						if (!header) {
							header = TRUE;
							gtk_text_buffer_insert_with_tags_by_name (config->db_textbuffer,
												  &current, 
												  _("UNIQUE constraint"), -1,
												  "section", NULL);
							gtk_text_buffer_insert (config->db_textbuffer, &current, "\n", -1);
						}
						else
							gtk_text_buffer_insert (config->db_textbuffer, &current, ", ", -1);

						gtk_text_buffer_insert (config->db_textbuffer, &current, 
									mg_base_get_name (MG_BASE (list2->data)), -1);
						
						list2 = g_slist_next (list2);
					}
					g_slist_free (fields);
					gtk_text_buffer_insert (config->db_textbuffer, &current, "\n\n", -1);
				}
				list = g_slist_next (list);
			}

			/* check constraint: FIXME */

			g_slist_free (constraints);
			
		}

		if (IS_MG_DB_FIELD (obj)) {
			gchar *str;
			MgServerDataType *type;
			MgDbField *field = MG_DB_FIELD (obj);

			gtk_text_buffer_insert_with_tags_by_name (config->db_textbuffer, 
								  &current, _("Field: "), -1, 
								  "section", NULL);
			gtk_text_buffer_insert (config->db_textbuffer, &current, mg_base_get_name (MG_BASE (obj)), -1);
			gtk_text_buffer_insert (config->db_textbuffer, &current, "\n\n", -1);

			type = mg_field_get_data_type (MG_FIELD (field));
			gtk_text_buffer_insert (config->db_textbuffer, &current, _("Data type: "), -1);
			gtk_text_buffer_insert (config->db_textbuffer, &current, mg_base_get_name (MG_BASE (type)), -1);
			gtk_text_buffer_insert (config->db_textbuffer, &current, "\n", -1);

			gtk_text_buffer_insert (config->db_textbuffer, &current, _("Description: "), -1);
			gtk_text_buffer_insert (config->db_textbuffer, &current, mg_base_get_description (MG_BASE (obj)), -1);
			gtk_text_buffer_insert (config->db_textbuffer, &current, "\n", -1);

			gtk_text_buffer_insert (config->db_textbuffer, &current, _("Length: "), -1);
			if (mg_db_field_get_length (field) != -1) {
				str = g_strdup_printf ("%d", mg_db_field_get_length (field));
				gtk_text_buffer_insert (config->db_textbuffer, &current, str, -1);
				g_free (str);
			}
			gtk_text_buffer_insert (config->db_textbuffer, &current, "\n", -1);

			gtk_text_buffer_insert (config->db_textbuffer, &current, _("Scale: "), -1);
			if (mg_db_field_get_scale (field) != -1) {
				str = g_strdup_printf ("%d", mg_db_field_get_scale (field));
				gtk_text_buffer_insert (config->db_textbuffer, &current, str, -1);
				g_free (str);
			}
			gtk_text_buffer_insert (config->db_textbuffer, &current, "\n", -1);

			
			gtk_text_buffer_insert (config->db_textbuffer, &current, _("NULL allowed: "), -1);
			str = mg_db_field_is_null_allowed (field) ? _("Yes") : _("No");
			gtk_text_buffer_insert (config->db_textbuffer, &current, str, -1);
			gtk_text_buffer_insert (config->db_textbuffer, &current, "\n", -1);
			gtk_text_buffer_insert (config->db_textbuffer, &current, "\n", -1);

			gtk_text_buffer_insert (config->db_textbuffer, &current, _("Primary key: "), -1);
			if (mg_db_field_is_pkey_part (field)) 
				str = mg_db_field_is_pkey_alone (field) ? _("Primary key") : 
					_("Part of primary key");
			else
				str = _("No");
			gtk_text_buffer_insert (config->db_textbuffer, &current, str, -1);
			gtk_text_buffer_insert (config->db_textbuffer, &current, "\n", -1);

			gtk_text_buffer_insert (config->db_textbuffer, &current, _("Foreign key: "), -1);
			if (mg_db_field_is_fkey_part (field)) 
				str = mg_db_field_is_fkey_alone (field) ? _("Foreign key") : 
					_("Part of foreign key");
			else
				str = _("No");
			gtk_text_buffer_insert (config->db_textbuffer, &current, str, -1);
			gtk_text_buffer_insert (config->db_textbuffer, &current, "\n", -1);
		}
	}


	gtk_widget_set_sensitive (config->db_view_contents, obj && IS_MG_DB_TABLE (obj));
}

static void
table_view_contents (GtkWidget *button, BrowserConfig *config)
{
	MgQuery *query;
	MgDbTable *table;
	MgTarget *target;
	MgQfield *field;
	GObject *sel;
	GtkWidget *form, *dlg;
	
	sel = mg_selector_get_selected_object (MG_SELECTOR (config->db_selector));
	if (!sel || !IS_MG_DB_TABLE (sel))
		return;

	conn_open (config);

	/* Query preparation: "SELECT * FROM table" */
	table = MG_DB_TABLE (sel);
	query = MG_QUERY (mg_query_new (config->conf));
	mg_query_set_query_type (query, MG_QUERY_TYPE_SELECT);

	target = MG_TARGET (mg_target_new_with_entity (query, MG_ENTITY (table)));
	mg_query_add_target (query, target, NULL);
	g_object_unref (G_OBJECT (target));
	
	field = MG_QFIELD (mg_qf_all_new_with_target (query, target));;
	mg_entity_add_field (MG_ENTITY (query), MG_FIELD (field));
	g_object_unref (G_OBJECT (field));


	/* Form */
	form = mg_work_form_new (query, target);
	dlg = gtk_dialog_new_with_buttons ("Execution", GTK_WINDOW (config->mainwin),
					   GTK_DIALOG_MODAL,
					   GTK_STOCK_CLOSE,
					   GTK_RESPONSE_ACCEPT,
					   NULL);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dlg)->vbox), form);
	gtk_widget_show_all (dlg);
	mg_work_form_run (MG_WORK_FORM (form), 
			  MG_WORK_FORM_NAVIGATION_ARROWS | MG_WORK_FORM_NAVIGATION_SCROLL  | 
			  MG_WORK_FORM_MODIF_BUTTONS |
			  MG_WORK_FORM_ASK_CONFIRM_DELETE | MG_WORK_FORM_ASK_CONFIRM_UPDATE | 
			  MG_WORK_FORM_ASK_CONFIRM_INSERT);
	gtk_dialog_run (GTK_DIALOG (dlg));
	gtk_widget_destroy (dlg);

	g_object_unref (G_OBJECT (query));
}



/**
 * build_queries_page
 *
 * Build the Queries page
 */
static void queries_selection_changed_cb (MgSelector *mgsel, GObject *obj, BrowserConfig *config);
static void query_execute (GtkButton *button, BrowserConfig *config);
static GtkWidget *
build_queries_page (BrowserConfig *conf)
{
	GtkWidget *wid, *label, *paned, *vb;
	gchar *str;

	paned = gtk_hpaned_new ();
	gtk_container_set_border_width (GTK_CONTAINER (paned), 5);

	/* left part */
	vb = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (vb), 5);
	gtk_widget_set_size_request (vb, 200, 150);
	gtk_paned_add1 (GTK_PANED (paned), vb);
	label = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Select a query:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);

	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, FALSE, 0);
	wid = mg_selector_new (conf->conf, MG_SELECTOR_QUERIES | MG_SELECTOR_QVIS_FIELDS/* | MG_SELECTOR_SUB_QUERIES*/,
			       MG_SELECTOR_COLUMN_COMMENTS | MG_SELECTOR_COLUMN_TYPE | MG_SELECTOR_COLUMN_QFIELD_VALUE |
			       MG_SELECTOR_COLUMN_QFIELD_TYPE);

	gtk_box_pack_start (GTK_BOX (vb), wid, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (wid), "selection_changed",
			  G_CALLBACK (queries_selection_changed_cb), conf);
	conf->qu_selector = wid;

	/* right part */
	vb = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (vb), 5);
	gtk_paned_add2 (GTK_PANED (paned), vb);
#define BUILD_WITH_GNOME
#ifdef BUILD_WITH_GNOME
	label = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Query operations:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, TRUE, 0);

	wid = gtk_button_new_with_label (_("Execute"));
	gtk_box_pack_start (GTK_BOX (vb), wid, FALSE, TRUE, 0);
	g_signal_connect (G_OBJECT (wid), "clicked",
			  G_CALLBACK (query_execute), conf);
	gtk_widget_set_sensitive (wid, FALSE);
	conf->qu_exec_button = wid;

	wid = gtk_button_new_with_label (_("Use to modify"));
	gtk_box_pack_start (GTK_BOX (vb), wid, FALSE, TRUE, 0);
	g_signal_connect (G_OBJECT (wid), "clicked",
			  G_CALLBACK (query_execute), conf);
	gtk_widget_set_sensitive (wid, FALSE);
	conf->qu_modif_button = wid;

	label = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Query rendered as SQL:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, TRUE, 0);

	wid = gnome_db_editor_new ();
	gnome_db_editor_set_editable (GNOME_DB_EDITOR (wid), FALSE);
	gnome_db_editor_set_highlight (GNOME_DB_EDITOR (wid), TRUE);
	gtk_box_pack_start (GTK_BOX (vb), wid, TRUE, TRUE, 0);
	conf->qu_editor = wid;
#else
	label = gtk_label_new (_("This feature is disabled since\nthe libgnomedb library  has been compiled without the\n"
				 "Gnome library support"));
	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, TRUE, 20);
#endif	

	return paned;
}

static void
queries_selection_changed_cb (MgSelector *mgsel, GObject *obj, BrowserConfig *config)
{	
	MgTarget *target = NULL;

	if (!obj)
#ifdef BUILD_WITH_GNOME
		gnome_db_editor_set_text (GNOME_DB_EDITOR (config->qu_editor), "", -1);
#endif
	else {
		if (IS_MG_QUERY (obj)) {
			gchar *str;
			MgQuery *query = MG_QUERY (obj);
			GError *error = NULL;

			if (mg_query_is_select_query (query)) {
				GSList *targets;
				
				targets = mg_query_get_targets (query);
				if (targets) {
					target = MG_TARGET (targets->data);
					g_slist_free (targets);
				}
			}

			str = mg_renderer_render_as_sql (MG_RENDERER (query), NULL, &error);
			if (str) {
#ifdef BUILD_WITH_GNOME
				gnome_db_editor_set_text (GNOME_DB_EDITOR (config->qu_editor), str, -1);
#endif
				g_free (str);
			}
			else {
				if (error) {
					str = error->message;
#ifdef BUILD_WITH_GNOME
					gnome_db_editor_set_text (GNOME_DB_EDITOR (config->qu_editor), str, -1);
#endif
					g_error_free (error);
				}
				else
#ifdef BUILD_WITH_GNOME
					gnome_db_editor_set_text (GNOME_DB_EDITOR (config->qu_editor),
								  _("Non reported error"), -1);
#endif
			}
		}
		else 
#ifdef BUILD_WITH_GNOME
			gnome_db_editor_set_text (GNOME_DB_EDITOR (config->qu_editor), "", -1);
#endif
	}

	gtk_widget_set_sensitive (config->qu_exec_button, obj && IS_MG_QUERY (obj));
	gtk_widget_set_sensitive (config->qu_modif_button, obj && IS_MG_QUERY (obj) && target);
	g_object_set_data (G_OBJECT (config->qu_modif_button), "target", target);
}

static void
query_execute (GtkButton *button, BrowserConfig *config)
{
	GError *error = NULL;
	GObject *obj;
	MgQuery *query;

	gint result = GTK_RESPONSE_ACCEPT;
	MgContext *context;

	conn_open (config);

	gnome_db_editor_set_text (GNOME_DB_EDITOR (config->qu_editor), "", -1);
	obj = mg_selector_get_selected_object (MG_SELECTOR (config->qu_selector));
	if (!obj)
		return;

	query = MG_QUERY (obj);
	context = mg_entity_get_exec_context (MG_ENTITY (query));

	if (mg_context_needs_user_input (context)) {
		GtkWidget *dlg;
		MgForm *sform;

		dlg = mg_form_new_in_dialog (config->conf, context, GTK_WINDOW (config->mainwin),
					     "Missing parameters", "Fill in the missing "
					     "parameters");
		sform = g_object_get_data (G_OBJECT (dlg), "form");
		mg_form_set_entries_auto_default (sform, TRUE);

		gtk_widget_show (dlg);
		result = gtk_dialog_run (GTK_DIALOG (dlg));
		gtk_widget_destroy (dlg);
	}

	if (result == GTK_RESPONSE_ACCEPT) {
		gchar *sql;
		sql = mg_renderer_render_as_sql (MG_RENDERER (query), context, &error);
		if (sql) {
			gnome_db_editor_set_text (GNOME_DB_EDITOR (config->qu_editor), sql, -1);

			if (mg_query_is_modif_query (query)) {
				GtkWidget *dlg;
				
				dlg = gtk_message_dialog_new (GTK_WINDOW (config->mainwin), 0,
							      GTK_MESSAGE_QUESTION,
							      GTK_BUTTONS_YES_NO,
							      _("Execute the following modification query:\n%s"), sql);
				
				gtk_widget_show (dlg);
				result = gtk_dialog_run (GTK_DIALOG (dlg));
				gtk_widget_destroy (dlg);
			}
			
			if (result == GTK_RESPONSE_ACCEPT) {
				if (mg_query_is_modif_query (query)) { /* simply run that query */
					MgConf *conf = mg_base_get_conf (MG_BASE (query));

					mg_server_do_query (mg_conf_get_server (conf), sql, MG_SERVER_QUERY_SQL, &error);
					if (error) {
						GtkWidget *dlg;
						gchar *message;
						
						message = g_strdup (error->message);
						g_error_free (error);
						dlg = gtk_message_dialog_new (GTK_WINDOW (config->mainwin), 0,
									      GTK_MESSAGE_ERROR,
									      GTK_BUTTONS_CLOSE,
									      message);
						g_free (message);
						gtk_dialog_run (GTK_DIALOG (dlg));
						gtk_widget_destroy (dlg);
					}
				}
				else { /* open a form to display the query */
					GtkWidget *form, *dlg;
					MgTarget *target;
					guint mode;

					target = g_object_get_data (G_OBJECT (button), "target");

					form = mg_work_form_new (query, target);
					dlg = gtk_dialog_new_with_buttons ("Execution", GTK_WINDOW (config->mainwin),
									   GTK_DIALOG_MODAL,
									   GTK_STOCK_CLOSE,
									   GTK_RESPONSE_ACCEPT,
									   NULL);
					gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dlg)->vbox), form);
					gtk_widget_show_all (dlg);
					mode = MG_WORK_FORM_NAVIGATION_ARROWS | MG_WORK_FORM_NAVIGATION_SCROLL;
					if (target)
						mode = mode | MG_WORK_FORM_MODIF_BUTTONS |
							MG_WORK_FORM_ASK_CONFIRM_DELETE | MG_WORK_FORM_ASK_CONFIRM_UPDATE | 
							MG_WORK_FORM_ASK_CONFIRM_INSERT;

					mg_work_form_run (MG_WORK_FORM (form), mode);
					gtk_dialog_run (GTK_DIALOG (dlg));
					gtk_widget_destroy (dlg);
				}
			}

			g_free (sql);
		}
		else {
			if (error) {
				gnome_db_editor_set_text (GNOME_DB_EDITOR (config->qu_editor), 
							  error->message, -1);
				g_error_free (error);
			}
			else
				gnome_db_editor_set_text (GNOME_DB_EDITOR (config->qu_editor),
							  _("Non reported error"), -1); 
		}
	}
	g_object_unref (G_OBJECT (context));
}
